<?php

return [

    'base_url' => env('SIPLAH_BASEURL', null),
    'app_id' => env('SIPLAH_APP_ID', null),
    'client_secret' => env('SIPLAH_CLIENT_SECRET', null),

    'satdik_jabatan_yang_diizinkan' => [
        'Kepala Sekolah', 'PLT Kepala Sekolah', 'Pelaksana PBJ', 'Bendahara'
    ],

    'satdik_jabatan_yang_bisa_checkout' => [
        'Kepala Sekolah', 'PLT Kepala Sekolah', 'Pelaksana PBJ',
    ],
];
