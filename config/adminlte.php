<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Template Skin
    |--------------------------------------------------------------------------
    |
    | 'skin-blue', 'skin-blue-light',
    | 'skin-yellow', 'skin-yellow-light',
    | 'skin-green', 'skin-green-light',
    | 'skin-purple', 'skin-purple-light',
    | 'skin-red', 'skin-red-light',
    | 'skin-black', 'skin-black-light',
    |
    */
    'skin' => 'skin-blue-light',

    'layout' => [
        'styles_location' => 'examples.themes.adminlte.default.styles',
        'scripts_location' => 'examples.themes.adminlte.default.scripts',

        'header_location' => 'examples.themes.adminlte.default.header',
        'sidebar_location' => 'examples.themes.adminlte.default.sidebar',
        'footer_location' => 'examples.themes.adminlte.default.footer',
    ],

    'logo_mini' => 'L',
    'logo_lg' => 'Laravel',
    'logo_url' => '/home',




];
