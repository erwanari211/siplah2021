<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LaratrustSeeder::class);

        $this->call(ZoneOngkirProvincesTableSeeder::class);
        $this->call(ZoneOngkirCitiesTableSeeder::class);
        $this->call(OrderStatusesTableSeeder::class);
    }
}
