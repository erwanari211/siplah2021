<?php

namespace database\seeds;

use Illuminate\Database\Seeder;

class KemdikbudSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('database\seeds\kemdikbud\KemdikbudProvincesSeeder');
        $this->call('database\seeds\kemdikbud\KemdikbudCitiesSeeder');
        $this->call('database\seeds\kemdikbud\KemdikbudDistrictsSeeder');

        $this->call('database\seeds\kemdikbud\KemdikbudIndonesiaProvincesSeeder');
        $this->call('database\seeds\kemdikbud\KemdikbudIndonesiaCitiesSeeder');
        $this->call('database\seeds\kemdikbud\KemdikbudIndonesiaDistrictsSeeder');
    }
}
