<?php

namespace database\seeds\siplah;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\User;
use DB;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceBanner;
use App\Models\Modules\Marketplaces\MarketplaceBannerGroup;
use App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail;
use Carbon\Carbon;

class MarketplacesBannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $marketplace = Marketplace::first();
        $this->data['marketplace'] = $marketplace;

        $this->createBanners();
        $this->createBannerGroups();
        $this->addBannerToGroup();
    }

    public function createBanners()
    {
        $marketplace = $this->data['marketplace'];
        $location = 'images/dummy-images/banners/';
        $expiredDate = Carbon::now()->addMonths(2)->format('Y-m-d H:i:s');

        for ($i=1; $i <= 10; $i++) {
            $image = $i < 10 ? '0' . $i : $i;

            $banner = new MarketplaceBanner;
            $banner->marketplace_id = $marketplace->id;
            $banner->image = $location . $image . '.jpg';
            $banner->active = true;
            $banner->expired_at = $expiredDate;
            $banner->sort_order = 1;
            $banner->type = 'banner';
            $banner->save();

            $this->data['banners'][] = $banner;
        }

    }

    public function createBannerGroups()
    {
        $marketplace = $this->data['marketplace'];

        $groupSlider = new MarketplaceBannerGroup;
        $groupSlider->marketplace_id = $marketplace->id;
        $groupSlider->name = 'Banner Group Slider';
        $groupSlider->active = true;
        $groupSlider->type = 'slider';
        $groupSlider->sort_order = 1;
        $groupSlider->display_name = null;
        $groupSlider->save();
        $this->data['bannerGroups']['slider'] = $groupSlider;

        $groupNormal = new MarketplaceBannerGroup;
        $groupNormal->marketplace_id = $marketplace->id;
        $groupNormal->name = 'Banner Group Normal';
        $groupNormal->active = true;
        $groupNormal->type = 'normal';
        $groupNormal->sort_order = 2;
        $groupNormal->display_name = 'Rekomendasi';
        $groupNormal->save();
        $this->data['bannerGroups']['normal'] = $groupNormal;
    }

    public function addBannerToGroup()
    {
        $faker = Factory::create();
        $banners = $this->data['banners'];

        $groupSlider = $this->data['bannerGroups']['slider'];
        $selectedBanners = $faker->randomElements($banners, 4);
        $sortOrder = 1;
        foreach ($selectedBanners as $banner) {
            $detail = new MarketplaceBannerGroupDetail;
            $detail->marketplace_banner_group_id = $groupSlider->id;
            $detail->marketplace_banner_id = $banner->id;
            $detail->sort_order = $sortOrder;
            $detail->save();

            $sortOrder++;
        }

        $groupNormal = $this->data['bannerGroups']['normal'];
        $selectedBanners = $faker->randomElements($banners, 6);
        $sortOrder = 1;
        foreach ($selectedBanners as $banner) {
            $detail = new MarketplaceBannerGroupDetail;
            $detail->marketplace_banner_group_id = $groupNormal->id;
            $detail->marketplace_banner_id = $banner->id;
            $detail->sort_order = $sortOrder;
            $detail->save();

            $sortOrder++;
        }
    }

}
