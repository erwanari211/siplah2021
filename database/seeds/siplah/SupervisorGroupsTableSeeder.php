<?php

namespace database\seeds\siplah;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\User;
use DB;
use Illuminate\Support\Str;
use App\Models\Modules\Marketplaces\SupervisorGroup;
use App\Models\Modules\Marketplaces\SupervisorStaff;

class SupervisorGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i=1; $i <= 3; $i++) {
            $group = $this->createGroup($i);
            $this->creteStaffs($group);
        }
    }

    public function createGroup($index)
    {
        $alpha = range('A', 'Z');
        $index = $index ?? strtolower(Str::random(8));
        $char = $alpha[$index - 1] ?? $index;
        $name = 'Group ' . $char;
        $group = new SupervisorGroup;
        $group->unique_code = $group->randomUniqueCode();
        $group->name = $name;
        $group->slug = str_slug($name);

        $imageIdx = rand(1, 10);
        $imageIdx = ($imageIdx < 10) ? ('0' . $imageIdx) : $imageIdx;
        $imageFolder = 'images/dummy-images/abstracts-300x300/';
        $filepath = $imageFolder .  $imageIdx . '.jpg';
        $group->image = $filepath;
        $group->save();

        return $group;
    }

    public function creteStaffs($group)
    {
        for ($i=1; $i <= 3; $i++) {
            $groupName = $group->name;
            $name = $groupName . ' staff ' . $i;
            $name = Str::snake($name);
            $email = $name . '@app.com';
            $password = 'password';
            $role = $i % 3 == 1 ? 'admin' : 'staff';

            $userExists = User::where('email', $email)->exists();
            if (!$userExists) {
                $user = User::create([
                    'name' => $name,
                    'username' => Str::random(11),
                    'email' => $email,
                    'password' => bcrypt($password),
                ]);
            } else {
                $user = User::where('email', $email)->first();
            }

            $staffExists = SupervisorStaff::where([
                'user_id' => $user->id,
                'supervisor_group_id' => $group->id,
            ])->exists();

            if (!$staffExists) {
                $staff = new SupervisorStaff;
                $staff->user_id = $user->id;
                $staff->supervisor_group_id = $group->id;
                $staff->role = $role;
                $staff->save();
            }

        }
    }

}
