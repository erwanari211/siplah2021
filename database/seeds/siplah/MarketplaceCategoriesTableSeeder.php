<?php

namespace database\seeds\siplah;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceCategory;
use DB;

class MarketplaceCategoriesTableSeeder extends Seeder
{
    public $useCompleteCategory = true;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = $this->readCsv();
        $this->data['categories'] = $categories;
        $this->createCategories();
        $this->createUncategorizedCategory();
    }

    public function readCsv()
    {
        $file = base_path().'/database/seeds/siplah/csv/categories.csv';

        if (!$this->useCompleteCategory) {
            $file = base_path().'/database/seeds/siplah/csv/categories2.csv';
        }

        $header = array('category_id', 'parent_category_id', 'name');

        $Csv = new CsvtoArray;
        $data = $Csv->csv_to_array($file, $header);

        $collection = collect($data);
        $collection->shift(); // remove header

        return $collection;
    }

    public function createCategories()
    {
        $marketplace = Marketplace::first();
        $table = 'marketplace_categories';

        $collection = $this->data['categories'];
        foreach($collection->chunk(50) as $chunk) {
            $chunkArray = $chunk->toArray();
            foreach ($chunkArray as $item) {
                $parentId = $item['parent_category_id'];
                if ($parentId == '') {
                    $parentId = null;
                }

                if ($item['category_id'] == 0) {
                    continue;
                }

                $category = [
                    'id' => $item['category_id'],
                    'marketplace_id' => $marketplace->id,
                    'parent_id' => $parentId,
                    'name' => $item['name'],
                    'slug' => str_slug($item['name']),
                ];

                if ($item['name'] == 'Barang') {
                    $category['image'] = 'images/icon/categories/product.png';
                }

                if ($item['name'] == 'Jasa') {
                    $category['image'] = 'images/icon/categories/service.png';
                }



                DB::table($table)->insert($category);
            }
        }
    }

    public function createUncategorizedCategory()
    {
        $marketplace = Marketplace::first();
        $table = 'marketplace_categories';

        $categoryData = [
            'marketplace_id' => $marketplace->id,
            'parent_id' => null,
            'name' => 'Uncategorized',
            'slug' => str_slug('Uncategorized'),
            'image' => 'images/icon/categories/opened-folder.png',
        ];

        $categoryId = DB::table($table)->insertGetId($categoryData);
        $category = MarketplaceCategory::find($categoryId);
        $category->id = 0;
        $category->save();
    }
}
