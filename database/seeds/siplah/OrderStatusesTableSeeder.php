<?php

namespace database\seeds\siplah;

use Illuminate\Database\Seeder;
use App\Models\Modules\Marketplaces\OrderStatus;
use DB;

class OrderStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::truncate();

        $statuses = $this->getSiplahStatuses();

        $index = 1;
        foreach ($statuses as $status) {
            $orderStatus = new OrderStatus;
            $orderStatus->group = $status['group'];
            $orderStatus->key = $status['key'];
            $orderStatus->value = $status['value'];
            $orderStatus->sort_order = $index;
            $orderStatus->save();

            $index++;
        }
    }

    public function getStatuses()
    {
        $statuses = [
            ['group' => 'order', 'key' => 'pending', 'value' => 'Pending', ],
            ['group' => 'payment', 'key' => 'paid', 'value' => 'Paid', ],
            ['group' => 'shipping', 'key' => 'processing', 'value' => 'Processing', ],
            ['group' => 'shipping', 'key' => 'processed', 'value' => 'Processed', ],
            ['group' => 'shipping', 'key' => 'shipped', 'value' => 'Shipped', ],
            ['group' => 'order', 'key' => 'completed', 'value' => 'Completed', ],
            ['group' => 'order', 'key' => 'canceled', 'value' => 'Canceled', ],
            ['group' => 'order', 'key' => 'denied', 'value' => 'Denied', ],
            ['group' => 'shipping', 'key' => 'canceled reversal', 'value' => 'Canceled Reversal', ],
            ['group' => 'order', 'key' => 'failed', 'value' => 'Failed', ],
            ['group' => 'payment', 'key' => 'refunded', 'value' => 'Refunded', ],
            ['group' => 'shipping', 'key' => 'reversed', 'value' => 'Reversed', ],
            ['group' => 'payment', 'key' => 'charged back', 'value' => 'Charged Back', ],
            ['group' => 'order', 'key' => 'expired', 'value' => 'Expired', ],
            ['group' => 'order', 'key' => 'voided', 'value' => 'Voided', ],
            ['group' => 'payment', 'key' => 'installment', 'value' => 'Installment', ],
            ['group' => 'payment', 'key' => 'unpaid', 'value' => 'Unpaid', ],
            ['group' => 'payment', 'key' => 'payment confirmation', 'value' => 'Payment Confirmation', ],
        ];

        return $statuses;
    }

    public function getSiplahStatuses()
    {
        $statuses = [
            ['group' => 'order_process', 'key' => 'pending', 'value' => 'Pending', ],
            ['group' => 'order_process', 'key' => 'processing', 'value' => 'Processing', ],
            ['group' => 'order_process', 'key' => 'completed', 'value' => 'Completed', ],
            ['group' => 'order_process', 'key' => 'complain', 'value' => 'Complain', ],
            ['group' => 'order_process', 'key' => 'canceled', 'value' => 'Canceled', ],
            ['group' => 'order_process', 'key' => 'rejected', 'value' => 'Rejected', ],

            ['group' => 'order_confirmation', 'key' => 'pending', 'value' => 'Pending (Confirmation)', ],
            ['group' => 'order_confirmation', 'key' => 'confirmed', 'value' => 'Confirmed (Confirmation)', ],
            ['group' => 'order_confirmation', 'key' => 'not_confirmed', 'value' => 'Not Confirmed (Confirmation)', ],

            ['group' => 'packing', 'key' => 'pending', 'value' => 'Pending (Packing)', ],
            ['group' => 'packing', 'key' => 'processing', 'value' => 'Processing (Packing)', ],
            ['group' => 'packing', 'key' => 'processed', 'value' => 'Processed (Packing)', ],

            ['group' => 'shipping', 'key' => 'pending', 'value' => 'Pending (Shipping)', ],
            ['group' => 'shipping', 'key' => 'shipping', 'value' => 'Shipping (Shipping)', ],
            ['group' => 'shipping', 'key' => 'shipped', 'value' => 'Shipped (Shipping)', ],

            ['group' => 'delivery', 'key' => 'pending', 'value' => 'Pending (Delivery)', ],
            ['group' => 'delivery', 'key' => 'delivered', 'value' => 'Delivered (Delivery)', ],

            ['group' => 'delivery_document', 'key' => 'pending', 'value' => 'Pending (Delivery Document)', ],
            ['group' => 'delivery_document', 'key' => 'completed', 'value' => 'Completed (Delivery Document)', ],
            ['group' => 'delivery_document', 'key' => 'installment', 'value' => 'Installment (Delivery Document)', ],

            ['group' => 'payment_from_customer', 'key' => 'unpaid', 'value' => 'Unpaid (Payment from Customer)', ],
            ['group' => 'payment_from_customer', 'key' => 'paid', 'value' => 'Paid (Payment from Customer)', ],
            ['group' => 'payment_from_customer', 'key' => 'installment', 'value' => 'Installment (Payment from Customer)', ],

            ['group' => 'payment_to_store', 'key' => 'unpaid', 'value' => 'Unpaid (Payment to Store)', ],
            ['group' => 'payment_to_store', 'key' => 'paid', 'value' => 'Paid (Payment to Store)', ],
            ['group' => 'payment_to_store', 'key' => 'installment', 'value' => 'Installment (Payment to Store)', ],

            ['group' => 'changed_by_customer', 'key' => 'not_changed', 'value' => 'Not Changed', ],
            ['group' => 'changed_by_customer', 'key' => 'changed', 'value' => 'Changed', ],

            ['group' => 'canceled', 'key' => 'not_canceled', 'value' => 'Not Canceled', ],
            ['group' => 'canceled', 'key' => 'canceled_by_customer_submitted', 'value' => 'Canceled By customer Submitted', ],
            ['group' => 'canceled', 'key' => 'canceled_by_customer_approved', 'value' => 'Canceled By customer Approved', ],
            ['group' => 'canceled', 'key' => 'canceled_by_customer', 'value' => 'Canceled By customer', ],
            ['group' => 'canceled', 'key' => 'canceled_by_store_submitted', 'value' => 'Canceled By Store Submitted', ],
            ['group' => 'canceled', 'key' => 'canceled_by_store_approved', 'value' => 'Canceled By Store Approved', ],
            ['group' => 'canceled', 'key' => 'canceled_by_store', 'value' => 'Canceled By Store', ],

            ['group' => 'complain_source', 'key' => 'no_complain', 'value' => 'No Complain (Complain Source)', ],
            ['group' => 'complain_source', 'key' => 'complain_from_customer', 'value' => 'Complain From Customer', ],
            ['group' => 'complain_source', 'key' => 'complain_from_store', 'value' => 'Complain From Store', ],

            ['group' => 'complain_request', 'key' => 'no_complain', 'value' => 'No Complain (Complain Request)', ],
            ['group' => 'complain_request', 'key' => 'not_approved', 'value' => 'Not Approved (Complain Request)', ],
            ['group' => 'complain_request', 'key' => 'approved', 'value' => 'Approved (Complain Request)', ],

            ['group' => 'complain_status', 'key' => 'no_complain', 'value' => 'No Complain (Complain Status)', ],
            ['group' => 'complain_status', 'key' => 'unresolved', 'value' => 'Unresolved (Complain Status)', ],
            ['group' => 'complain_status', 'key' => 'resolved', 'value' => 'Resolved (Complain Status)', ],
            ['group' => 'complain_status', 'key' => 'escalation_solution', 'value' => 'Escalation Solution (Complain Status)', ],
        ];

        return $statuses;
    }
}
