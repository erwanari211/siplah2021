<?php

namespace database\seeds\siplah;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\User;
use DB;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Product;
use Illuminate\Support\Str;

class ProductsTableV1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $stores = Store::get();
        $categories = [
            [1, 22],
            [1, 23],

            [1, 22],
            [1, 23],

            [1, 22],
            [1, 23],

            [2, 20],
            [2, 21],
        ];
        $this->data['stores'] = $stores;
        $this->data['categories'] = $categories;

        foreach ($stores as $store) {
            for ($i=1; $i <= 3; $i++) {
                $category = $faker->randomElement($categories);
                $this->createProduct($store, $category);
            }
        }
    }

    public function createProduct($store, $category)
    {
        $name = strtolower(Str::random(8));
        $data = [
            'name' => 'product-' . $name,
            'slug' => 'product-' . $name,
            'sku' => 'p-' . $name,
            'description' => 'product-' . $name . ' desc',
            'price' => rand(1, 50) * 10000,
            'qty' => 100,
            'weight' => 100,
            'type' => 'product',
            'active' => true,
            'status' => 'active',
            'image' => 'images/no-image-v2.png',
            'is_approved' => true,
            'approved_status' => 'approved',
            'is_local_product' => true,
            'is_msme_product' => rand(0,1) == 1,
        ];

        $product = new Product;
        $product->name = $data['name'];
        $slug = str_slug($data['slug']);
        $product->slug = Product::getUniqueSlug($slug);
        $product->unique_code = Product::randomUniqueCode();
        $product->sku = $data['sku'];
        $product->description = $data['description'];
        $product->price = $data['price'];
        $product->qty = $data['qty'];
        $product->weight = $data['weight'];
        $product->type = $data['type'];
        $product->active = $data['active'];
        $product->status = $data['status'];
        $product->is_approved = $data['is_approved'];
        $product->approved_status = $data['approved_status'];
        $product->approved_at = date('Y-m-d H:i:s');
        $product->is_local_product = $data['is_local_product'];
        $product->is_msme_product = $data['is_msme_product'];
        $product->user_id = $store->user_id;
        $product->store_id = $store->id;
        $product->save();

        $product->image = $data['image'];;
        $product->save();

        $category = array_filter($category);
        if ($category) {
            $product->marketplaceCategories()->sync($category);
        }
    }

}
