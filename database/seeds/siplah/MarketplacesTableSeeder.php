<?php

namespace database\seeds\siplah;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceStaff;
use App\User;
use DB;
use App\Models\Modules\Marketplaces\MarketplaceSetting;

class MarketplacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        Marketplace::truncate();
        // DB::table('marketplace_promos')->truncate();
        // DB::table('marketplace_promo_details')->truncate();
        DB::table('marketplace_settings')->truncate();

        $this->createMarketplace();
        $this->addMarketplaceStaff();
    }

    public function createMarketplace()
    {
        $user = User::where('email', 'admin@app.com')->first();
        $imageDirectory = 'images';
        $pathDirectory = public_path().'/'.$imageDirectory;

        $marketplace = new Marketplace;
        $marketplace->user_id = $user->id;
        $marketplace->name = 'Marketplace';
        $marketplace->slug = str_slug($marketplace->name);
        $marketplace->image = $imageDirectory .'/no-image-v2.png';
        $marketplace->logo = $imageDirectory .'/no-image-v2.png';
        $marketplace->active = true;
        $marketplace->unique_code = Marketplace::randomUniqueCode();
        $marketplace->save();

        $settings = [
            'payment_bank_name' => 'Bank Indonesia',
            'payment_account_name' => 'Siplah Marketplace',
            'payment_account_number' => '123-456-789',
            'payment_bank_branch' => 'Indonesia',
            'skin' => 'blue',
            'store_auto_approve' => 0,
            'marketplace_description' => 'Marketplace Siplah',
        ];

        foreach ($settings as $key => $value) {
            MarketplaceSetting::updateOrCreate([
                'marketplace_id' => $marketplace->id,
                'group' => 'settings',
                'key' => $key,
            ], [
                'value' => $value
            ]);
        }

        $this->data['marketplace'] = $marketplace;
    }

    public function addMarketplaceStaff()
    {
        $user = User::where('email', 'admin@app.com')->first();
        $marketplace = $this->data['marketplace'];

        $staff = new MarketplaceStaff;
        $staff->marketplace_id = $marketplace->id;
        $staff->user_id = $user->id;
        $staff->role = 'admin';
        $staff->is_active = true;
        $staff->save();
    }
}
