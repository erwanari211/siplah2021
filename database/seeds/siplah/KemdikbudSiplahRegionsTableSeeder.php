<?php

namespace database\seeds\siplah;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceCategory;
use DB;

class KemdikbudSiplahRegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = $this->readCsv();
        $this->data['regions'] = $regions;
        $this->saveRegions();
    }

    public function readCsv()
    {
        $file = base_path().'/database/seeds/siplah/csv/siplah_region.csv';
        $header = [
            'region_id', 'indonesia_city_id',
            'kab_kota', 'province', 'province_map',
            'kab_lat', 'kab_long',
            'prov_lat', 'prov_long',
        ];

        $Csv = new CsvtoArray;
        $data = $Csv->csv_to_array($file, $header);

        $collection = collect($data);
        $collection->shift(); // remove header

        return $collection;
    }

    public function saveRegions()
    {
        $table = 'kemdikbud_siplah_regions';

        $collection = $this->data['regions'];
        foreach($collection->chunk(50) as $chunk) {
            $chunkArray = $chunk->toArray();
            foreach ($chunkArray as $item) {
                $item['kab_lat'] = $item['kab_lat'] == '' ? null : $item['kab_lat'];
                $item['kab_long'] = $item['kab_long'] == '' ? null : $item['kab_long'];
                $item['prov_lat'] = $item['prov_lat'] == '' ? null : $item['prov_lat'];
                $item['prov_long'] = $item['prov_long'] == '' ? null : $item['prov_long'];

                $region = [
                    'region_id' => $item['region_id'],
                    'indonesia_city_id' => $item['indonesia_city_id'],
                    'kab_kota' => $item['kab_kota'],
                    'province' => $item['province'],
                    'province_map' => $item['province_map'],
                    'kab_lat' => $item['kab_lat'] ?? null,
                    'kab_long' => $item['kab_long'] ?? null,
                    'prov_lat' => $item['prov_lat'] ?? null,
                    'prov_long' => $item['prov_long'] ?? null,
                ];

                DB::table($table)->insert($region);
            }
        }
    }
}
