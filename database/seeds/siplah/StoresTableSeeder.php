<?php

namespace database\seeds\siplah;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\User;
use DB;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreStaff;
use App\Models\Modules\Marketplaces\StoreAddress;
use Illuminate\Support\Str;
use App\Models\Modules\Marketplaces\StoreShippingMethod;
use App\Models\Modules\Marketplaces\StorePaymentMethod;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $stores = $this->readCsv();
        foreach ($stores as $store) {
            $this->createStoreAdmin($store);
            $this->createStore($store);
            $this->setStaff($store);
            $this->createStoreAddress($store);
        }
    }

    public function readCsv()
    {
        $file = base_path().'/database/seeds/siplah/csv/dummy_store.csv';
        $header = array('id', 'name', 'address', 'province_id', 'city_id', 'district_id', 'latitude', 'longitude');

        $Csv = new CsvtoArray;
        $data = $Csv->csv_to_array($file, $header);

        $collection = collect($data);
        $collection->shift(); // remove header

        return $collection;
    }

    public function createStoreAdmin($store)
    {
        $index = $store['id'];
        $index = $index ?? strtolower(Str::random(8));
        $user = new User;
        $user->name = 'store' . $index . '_admin';
        $user->username = 'store' . $index . '_admin';
        $user->email = 'store' . $index . '_admin@app.com';
        $user->password = bcrypt('password');
        $user->save();
        $user->setMeta('nik', 123);
        $user->setMeta('no_hp', 1234);

        $this->data['users'][$index] = $user;
    }

    public function createStore($store)
    {
        $storeData = $store;
        $index = $storeData['id'];
        $index = $index ?? strtolower(Str::random(8));
        $user = $this->data['users'][$index] ?? null;
        if (!$user) {
            return false;
        }

        $marketplace = get_default_marketplace();
        $store = new Store;
        $store->marketplace_id = $marketplace->id;
        $store->user_id = $user->id;
        $store->name = 'store' . $index . ' ' . $storeData['name'];
        $store->slug = $store->getUniqueSlug(Str::slug($store->name));
        $store->active = false;
        $store->status = 'inactive';

        $store->province_id = $storeData['province_id'];
        $store->city_id = $storeData['city_id'];
        $store->district_id = $storeData['district_id'];

        $store->latitude = $storeData['latitude'];
        $store->longitude = $storeData['longitude'];

        $store->unique_code = $store->randomUniqueCode();
        $store->save();

        $data = [
            'jenis_usaha' => 'Individu',
            'kelas_usaha' => 'Mikro',
            'status_usaha' => 'PKP',
            'npwp' => '234',
            'siup_nib' => '2345',
            'tdp' => '23456',

            'province_id' => '32',
            'city_id' => '3204',
            'district_id' => '3204190',

            'alamat' => 'bandung',
            'geolokasi' => '-',

            'kode_pos' => 40971,
            'email_toko' => 'store' . $index . '@app.com',

            'nama_lengkap_penanggungjawab' => 'store' . $index . '_admin',
            'email_penanggungjawab' => 'store' . $index . '_admin@app.com',
            'jabatan_penanggungjawab' => 'Dirut',
            'nik_penanggungjawab' => '123',

            'nama_bank' => 'BRI',
            'nama_pemilik_rekening' => 'store' . $index . '_admin',
            'no_rekening' => '345',
            'cabang_bank' => 'Soreang',
        ];

        $store->setMeta('no_tel_kantor', '123');
        $store->setMeta('jenis_usaha', $data['jenis_usaha']);
        $store->setMeta('kelas_usaha', $data['kelas_usaha']);
        $store->setMeta('status_usaha', $data['status_usaha']);
        $store->setMeta('npwp', $data['npwp']);
        $store->setMeta('siup_nib', $data['siup_nib']);
        $store->setMeta('tdp', $data['tdp']);

        $store->setMeta('province_id', $data['province_id']);
        $store->setMeta('city_id', $data['city_id']);
        $store->setMeta('district_id', $data['district_id']);

        $store->setMeta('alamat', $data['alamat']);
        $store->setMeta('geolokasi', $data['geolokasi']);

        // $store->setMeta('kab_kota', $data['kab_kota']);
        $store->setMeta('kode_pos', $data['kode_pos']);
        $store->setMeta('email_toko', $data['email_toko']);

        $store->setMeta('nama_lengkap_penanggungjawab', $data['nama_lengkap_penanggungjawab']);
        $store->setMeta('email_penanggungjawab', $data['email_penanggungjawab']);
        $store->setMeta('jabatan_penanggungjawab', $data['jabatan_penanggungjawab']);
        $store->setMeta('nik_penanggungjawab', $data['nik_penanggungjawab']);

        $store->setMeta('nama_bank', $data['nama_bank']);
        $store->setMeta('nama_pemilik_rekening', $data['nama_pemilik_rekening']);
        $store->setMeta('no_rekening', $data['no_rekening']);
        $store->setMeta('cabang_bank', $data['cabang_bank']);

        $files = ['scan_ktp_penanggung_jawab', 'scan_npwp', 'scan_siup', 'scan_tdp'];
        foreach ($files as $document) {
            $store->setMeta($document, 'images/no-image-v2.png');
        }

        $couriers = get_rajaongkir_couriers();
        $selectedCouriers = ['jne', 'pos', 'tiki'];
        foreach ($couriers as $courier => $courierName) {
            $method = new StoreShippingMethod;
            $method->group = 'rajaongkir';
            $method->label = $courier;
            $method->name = $courierName;
            $method->active = in_array($courier, $selectedCouriers);
            $method->store_id = $store->id;
            $method->save();
        }

        $customCouriers = get_siplah_custom_couriers();
        foreach ($customCouriers as $courier => $courierName) {
            $method = new StoreShippingMethod;
            $method->group = 'custom';
            $method->label = $courier;
            $method->name = $courierName;
            $method->active = true;
            $method->store_id = $store->id;
            $method->save();
        }

        $paymentMethod = new StorePaymentMethod;
        $paymentMethod->label = 'BRI';
        $paymentMethod->name = 'BRI';
        $paymentMethod->info = '
            Please Transfer to following bank account : <br>
            BRI <br>
            a/n Mr. John Smith <br>
            123-456-789 <br>
            Bandung <br>
        ';
        $store->paymentMethods()->save($paymentMethod);

        $this->data['stores'][$index] = $store;
    }

    public function setStaff($store)
    {
        $index = $store['id'];
        $index = $index ?? strtolower(Str::random(8));
        $user = $this->data['users'][$index] ?? null;
        $store = $this->data['stores'][$index] ?? null;

        if (!$user) {
            return false;
        }

        if (!$store) {
            return false;
        }

        $staff = new StoreStaff;
        $staff->store_id = $store->id;
        $staff->user_id = $user->id;
        $staff->role = 'admin';
        $staff->is_active = true;
        $staff->save();

        $data = [
            'jabatan' => 'Dirut',
        ];

        $staff->setMeta('jabatan', $data['jabatan']);
    }

    public function createStoreAddress($store)
    {
        $storeData = $store;
        $index = $storeData['id'];
        $store = $this->data['stores'][$index] ?? null;

        if (!$store) {
            return false;
        }



        $address = new StoreAddress;
        $address->label = 'Alamat Utama';
        $address->full_name = 'Alamat Utama';
        $address->company = $store->name;
        $address->address = $storeData['address'];
        $address->province_id = $storeData['province_id'];
        $address->city_id = $storeData['city_id'];
        $address->district_id = $storeData['district_id'];
        $address->phone = '-';
        $address->postcode = '-';

        $store->addresses()->save($address);

        $countStoreAddresses = StoreAddress::where('store_id', $store->id)->count();
        if ($countStoreAddresses == 1) {
            $address->default_address = true;
            $address->save();

            $store->saveDefaultAddress();
        }
    }
}
