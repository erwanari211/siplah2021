<?php

namespace database\seeds\siplah;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\User;
use DB;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Product;
use Illuminate\Support\Str;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $products = $this->readCsv();
        $groupedProducts = $products->groupBy('store_id');

        $stores = Store::get();
        $this->data['stores'] = $stores;

        foreach ($stores as $store) {
            $id = $store->id;
            $idx = $id % 10;
            if ($idx == 0) {
                $idx = 10;
            }

            $groupedProduct = $groupedProducts[$idx];
            foreach ($groupedProduct as $productData) {
                $this->createProduct($store, $productData);
            }
        }
    }

    public function readCsv()
    {
        $file = base_path().'/database/seeds/siplah/csv/dummy_product.csv';

        $header = array('store_id', 'product_name', 'price', 'categories');

        $Csv = new CsvtoArray;
        $data = $Csv->csv_to_array($file, $header);

        $collection = collect($data);
        $collection->shift(); // remove header

        return $collection;
    }

    public function createProduct($store, $productData)
    {
        $data = [
            'name' => $productData['product_name'],
            'slug' => Str::slug($productData['product_name']),
            'sku' => strtolower(Str::random(11)),
            'description' => 'product-' . $productData['product_name'] . ' desc',
            'price' => $productData['price'],
            'qty' => 100,
            'weight' => 100,
            'type' => 'product',
            'active' => true,
            'status' => 'active',
            'image' => 'images/no-image-v2.png',
            'is_approved' => true,
            'approved_status' => 'approved',
            'is_local_product' => true,
            'is_msme_product' => rand(0,1) == 1,
        ];

        $product = new Product;
        $product->name = $data['name'];
        $slug = str_slug($data['slug']);
        $product->slug = Product::getUniqueSlug($slug);
        $product->unique_code = Product::randomUniqueCode();
        $product->sku = $data['sku'];
        $product->description = $data['description'];
        $product->price = $data['price'];
        $product->qty = $data['qty'];
        $product->weight = $data['weight'];
        $product->type = $data['type'];
        $product->active = $data['active'];
        $product->status = $data['status'];
        $product->is_approved = $data['is_approved'];
        $product->approved_status = $data['approved_status'];
        $product->approved_at = date('Y-m-d H:i:s');
        $product->is_local_product = $data['is_local_product'];
        $product->is_msme_product = $data['is_msme_product'];
        $product->user_id = $store->user_id;
        $product->store_id = $store->id;
        $product->save();

        $product->image = $data['image'];;
        $product->save();

        $categories = json_decode($productData['categories']);
        $categories = array_filter($categories);
        if ($categories) {
            $product->marketplaceCategories()->sync($categories);
        }
    }

}
