<?php

use Illuminate\Database\Seeder;
use App\Models\Modules\Marketplaces\OrderStatus;

class OrderStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::truncate();

        $statuses = [
            ['group' => 'order', 'key' => 'pending', 'value' => 'Pending', ],
            ['group' => 'payment', 'key' => 'paid', 'value' => 'Paid', ],
            ['group' => 'shipping', 'key' => 'processing', 'value' => 'Processing', ],
            ['group' => 'shipping', 'key' => 'processed', 'value' => 'Processed', ],
            ['group' => 'shipping', 'key' => 'shipped', 'value' => 'Shipped', ],
            ['group' => 'order', 'key' => 'completed', 'value' => 'Completed', ],
            ['group' => 'order', 'key' => 'canceled', 'value' => 'Canceled', ],
            ['group' => 'order', 'key' => 'denied', 'value' => 'Denied', ],
            ['group' => 'shipping', 'key' => 'canceled reversal', 'value' => 'Canceled Reversal', ],
            ['group' => 'order', 'key' => 'failed', 'value' => 'Failed', ],
            ['group' => 'payment', 'key' => 'refunded', 'value' => 'Refunded', ],
            ['group' => 'shipping', 'key' => 'reversed', 'value' => 'Reversed', ],
            ['group' => 'payment', 'key' => 'charged back', 'value' => 'Charged Back', ],
            ['group' => 'order', 'key' => 'expired', 'value' => 'Expired', ],
            ['group' => 'order', 'key' => 'voided', 'value' => 'Voided', ],
            ['group' => 'payment', 'key' => 'installment', 'value' => 'Installment', ],
            ['group' => 'payment', 'key' => 'unpaid', 'value' => 'Unpaid', ],
            ['group' => 'payment', 'key' => 'payment confirmation', 'value' => 'Payment Confirmation', ],
        ];

        $index = 1;
        foreach ($statuses as $status) {
            $orderStatus = new OrderStatus;
            $orderStatus->group = $status['group'];
            $orderStatus->key = $status['key'];
            $orderStatus->value = $status['value'];
            $orderStatus->sort_order = $index;
            $orderStatus->save();

            $index++;
        }
    }
}
