<?php

use Illuminate\Database\Seeder;

class SiplahDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('database\seeds\siplah\LaratrustSeeder');

        $this->call('database\seeds\siplah\OrderStatusesTableSeeder');
        $this->call('database\seeds\siplah\MarketplacesTableSeeder');
        $this->call('database\seeds\siplah\MarketplaceCategoriesTableSeeder');
        $this->call('database\seeds\siplah\MarketplacesBannersTableSeeder');
        $this->call('database\seeds\siplah\StoresTableSeeder');
        $this->call('database\seeds\siplah\ProductsTableSeeder');

        $this->call('database\seeds\siplah\SupervisorGroupsTableSeeder');

        $this->call('database\seeds\siplah\KemdikbudSiplahRegionsTableSeeder');
    }
}
