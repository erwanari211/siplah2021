<?php

use Illuminate\Database\Seeder;

class LocationDatabaseSeeder extends Seeder
{
    public $useVillageSeeder = false;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('database\seeds\test\ZoneOngkirProvincesTableSeeder');
        $this->call('database\seeds\test\ZoneOngkirCitiesTableSeeder');

        $this->call('database\seeds\KemdikbudSeeder');
        $this->call('database\seeds\RajaongkirSeeder');

        // $this->call('Laravolt\Indonesia\Seeds\DatabaseSeeder');
        $this->call('Laravolt\Indonesia\Seeds\ProvincesSeeder');
        $this->call('Laravolt\Indonesia\Seeds\CitiesSeeder');
        $this->call('Laravolt\Indonesia\Seeds\DistrictsSeeder');
        if ($this->useVillageSeeder) {
            $this->call('Laravolt\Indonesia\Seeds\VillagesSeeder');
        }
    }
}
