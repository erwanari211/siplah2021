<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceSetting;
use DB;

class MarketplaceSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        MarketplaceSetting::truncate();

        $randomColors = ['blue', 'green', 'red', 'yellow'];
        $marketplaces = Marketplace::get();

        foreach ($marketplaces as $marketplace) {
            // set marketplace skin
            $randomColor = $faker->randomElement($randomColors);
            $setting = new MarketplaceSetting;
            $setting->marketplace_id = $marketplace->id;
            $setting->group = 'settings';
            $setting->key = 'skin';
            $setting->value = $randomColor;
            $setting->save();
        }
    }
}
