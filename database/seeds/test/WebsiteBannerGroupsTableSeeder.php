<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Website\WebsiteBannerGroup;
use DB;

class WebsiteBannerGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $imageDirectory = 'images';
        $pathDirectory = public_path().'/'.$imageDirectory;
        $totalItem = 2;
        for ($i=1; $i <= $totalItem; $i++) {
            $bannerGroup = new WebsiteBannerGroup;
            $bannerGroup->name = 'Banner group '.$i;
            $bannerGroup->active = true;
            $bannerGroup->type = $i == 1 ? 'slider' : 'normal';
            $bannerGroup->sort_order = $i;
            $bannerGroup->save();
        }
    }
}
