<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreBannerGroup;
use DB;

class StoreBannerGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        StoreBannerGroup::truncate();
        $stores = Store::get();

        foreach ($stores as $store) {
            $imageDirectory = 'images';
            $pathDirectory = public_path().'/'.$imageDirectory;
            $totalItem = 4;
            for ($i=1; $i <= $totalItem; $i++) {
                $bannerGroup = new StoreBannerGroup;
                $bannerGroup->store_id = $store->id;
                $bannerGroup->name = 'Banner group '.$i;
                $bannerGroup->active = true;
                $bannerGroup->type = $i == 1 ? 'slider' : 'normal';
                $bannerGroup->sort_order = $i;
                $bannerGroup->save();
            }
        }
    }
}
