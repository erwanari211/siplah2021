<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreShippingMethod;
use DB;

class StoreShippingMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        StoreShippingMethod::truncate();
        $stores = Store::get();
        $couriers = get_rajaongkir_couriers();

        foreach ($stores as $store) {
            $index = 1;
            foreach ($couriers as $courier => $courierName) {
                $method = new StoreShippingMethod;
                $method->group = 'rajaongkir';
                $method->label = $courier;
                $method->name = $courierName;
                $method->active = ($index <= 3) ? true : false;
                $method->store_id = $store->id;
                $method->save();

                $index++;
            }
        }
    }
}
