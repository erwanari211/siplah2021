<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StorePaymentMethod;
use DB;

class StorePaymentMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        StorePaymentMethod::truncate();
        $stores = Store::get();
        $methods = ['BRI', 'BNI', 'Mandiri'];

        foreach ($stores as $store) {

            $number1 = rand(100,999);
            $number2 = rand(100,999);
            $number3 = rand(100,999);

            foreach ($methods as $method) {
                $paymentMethod = new StorePaymentMethod;
                $paymentMethod->label = $method;
                $paymentMethod->name = $method;
                $paymentMethod->info = "Please Transfer to following bank account : <br> {$method} <br> a/n {$store->name} <br> {$number1}-{$number2}-{$number3} <br>";
                $paymentMethod->store_id = $store->id;
                $paymentMethod->save();
            }

        }
    }
}
