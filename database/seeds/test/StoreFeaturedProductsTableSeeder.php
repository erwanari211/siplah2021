<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreFeaturedProduct;
use DB;

class StoreFeaturedProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        StoreFeaturedProduct::truncate();
        $stores = Store::get();

        foreach ($stores as $store) {
            $imageDirectory = 'images';
            $pathDirectory = public_path().'/'.$imageDirectory;

            $featured = new StoreFeaturedProduct;
            $featured->store_id = $store->id;
            $featured->name = 'Featured product';
            $featured->slug = str_slug($featured->name);
            $featured->description = $faker->paragraph(4, true);
            $featured->image = $imageDirectory .'/no-image-v2.png';
            $featured->active = true;
            $featured->save();
        }
    }
}
