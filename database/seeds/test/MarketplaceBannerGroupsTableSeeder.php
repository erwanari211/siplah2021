<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceBannerGroup;
use DB;

class MarketplaceBannerGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        MarketplaceBannerGroup::truncate();
        $marketplaces = Marketplace::get();

        foreach ($marketplaces as $marketplace) {
            $imageDirectory = 'images';
            $pathDirectory = public_path().'/'.$imageDirectory;
            $totalItem = 4;
            for ($i=1; $i <= $totalItem; $i++) {
                $bannerGroup = new MarketplaceBannerGroup;
                $bannerGroup->marketplace_id = $marketplace->id;
                $bannerGroup->name = 'Banner group '.$i;
                $bannerGroup->active = true;
                $bannerGroup->type = $i == 1 ? 'slider' : 'normal';
                $bannerGroup->sort_order = $i;
                $bannerGroup->save();
            }
        }
    }
}
