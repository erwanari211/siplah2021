<?php

use Illuminate\Database\Seeder;

class TestDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('database\seeds\test\LaratrustSeeder');

        // $this->call('database\seeds\test\ZoneOngkirProvincesTableSeeder');
        // $this->call('database\seeds\test\ZoneOngkirCitiesTableSeeder');

        // $this->call('database\seeds\KemdikbudSeeder');
        // $this->call('database\seeds\RajaongkirSeeder');
        // $this->call('Laravolt\Indonesia\Seeds\DatabaseSeeder');

        $this->call('database\seeds\test\OrderStatusesTableSeeder');

        $this->call('database\seeds\test\UsersTableSeeder');

        $this->call('database\seeds\test\MarketplacesTableSeeder');
        $this->call('database\seeds\test\MarketplaceCategoriesTableSeeder');
        $this->call('database\seeds\test\MarketplaceBannersTableSeeder');
        $this->call('database\seeds\test\MarketplaceBannerGroupsTableSeeder');
        $this->call('database\seeds\test\MarketplaceBannerGroupDetailsTableSeeder');
        $this->call('database\seeds\test\MarketplaceSettingsTableSeeder');

        $this->call('database\seeds\test\StoresTableSeeder');
        $this->call('database\seeds\test\StoreAddressesTableSeeder');
        $this->call('database\seeds\test\StorePaymentMethodsTableSeeder');
        $this->call('database\seeds\test\StoreShippingMethodsTableSeeder');
        $this->call('database\seeds\test\StoreCategoriesTableSeeder');
        $this->call('database\seeds\test\StoreBannersTableSeeder');
        $this->call('database\seeds\test\StoreBannerGroupsTableSeeder');
        $this->call('database\seeds\test\StoreBannerGroupDetailsTableSeeder');
        $this->call('database\seeds\test\StoreSettingsTableSeeder');

        $this->call('database\seeds\test\ProductsTableSeeder');
        $this->call('database\seeds\test\ProductAttributesTableSeeder');

        $this->call('database\seeds\test\StoreCategoryProductTableSeeder');
        $this->call('database\seeds\test\StoreFeaturedProductsTableSeeder');
        $this->call('database\seeds\test\StoreFeaturedProductDetailsTableSeeder');

        $this->call('database\seeds\test\WebsiteBannersTableSeeder');
        $this->call('database\seeds\test\WebsiteBannerGroupsTableSeeder');
        $this->call('database\seeds\test\WebsiteBannerGroupDetailsTableSeeder');
        $this->call('database\seeds\test\WebsiteMarketplaceCollectionsTableSeeder');
        $this->call('database\seeds\test\WebsiteProductCollectionsTableSeeder');
    }
}
