<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceBanner;
use DB;

class MarketplaceBannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        MarketplaceBanner::truncate();
        $marketplaces = Marketplace::get();

        foreach ($marketplaces as $marketplace) {
            $imageDirectory = 'images';
            $pathDirectory = public_path().'/'.$imageDirectory;
            $totalItem = 10;
            for ($i=1; $i <= $totalItem; $i++) {
                $banner = new MarketplaceBanner;
                $banner->marketplace_id = $marketplace->id;
                $banner->image = $imageDirectory .'/random-banner.jpg';
                $banner->link = null;
                $banner->active = true;
                $banner->expired_at = date('Y-m-t', strtotime('+3 month'));
                $banner->note = 'banner '.$i;
                $banner->sort_order = $i;
                $banner->save();
            }
        }
    }
}
