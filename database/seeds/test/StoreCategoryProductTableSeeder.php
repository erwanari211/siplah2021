<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreCategory;
use App\Models\Modules\Marketplaces\StoreCategoryProduct;
use DB;

class StoreCategoryProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        DB::table('store_category_product')->truncate();
        $stores = Store::get();

        foreach ($stores as $store) {
            $storeCategories = $store->categories;
            $storeProducts = $store->products;
            $i = 0;
            foreach ($storeProducts as $product) {
                $randomCategory = $faker->randomElement($storeCategories);
                $randomCategory->products()->syncWithoutDetaching($product->id);
            }
        }
    }
}
