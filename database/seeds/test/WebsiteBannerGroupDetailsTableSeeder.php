<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Website\WebsiteBanner;
use App\Models\Modules\Website\WebsiteBannerGroup;
use App\Models\Modules\Website\WebsiteBannerGroupDetail;
use DB;

class WebsiteBannerGroupDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        WebsiteBannerGroupDetail::truncate();

        $imageDirectory = 'images';
        $pathDirectory = public_path().'/'.$imageDirectory;
        $totalItem = 4;

        $banners = WebsiteBanner::get();
        $bannerGroups = WebsiteBannerGroup::get();
        foreach ($bannerGroups as $bannerGroup) {
            $randomBanners = $faker->randomElements($banners, $totalItem);
            $index = 1;
            foreach ($randomBanners as $banner) {
                $detail = new WebsiteBannerGroupDetail;
                $detail->website_banner_group_id = $bannerGroup->id;
                $detail->website_banner_id = $banner->id;
                $detail->sort_order = $index;
                $detail->save();
                $index++;
            }
        }
    }
}
