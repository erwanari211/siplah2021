<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreBannerGroupDetail;
use DB;

class StoreBannerGroupDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        StoreBannerGroupDetail::truncate();
        $stores = Store::get();

        foreach ($stores as $store) {
            $imageDirectory = 'images';
            $pathDirectory = public_path().'/'.$imageDirectory;
            $totalItem = 4;
            $banners = $store->banners;
            $bannerGroups = $store->bannerGroups;
            foreach ($bannerGroups as $bannerGroup) {
                $randomBanners = $faker->randomElements($banners, $totalItem);
                $index = 1;
                foreach ($randomBanners as $banner) {
                    $detail = new StoreBannerGroupDetail;
                    $detail->store_banner_group_id = $bannerGroup->id;
                    $detail->store_banner_id = $banner->id;
                    $detail->sort_order = $index;
                    $detail->save();
                    $index++;
                }
            }
        }
    }
}
