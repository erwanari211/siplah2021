<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceCategory;
use DB;

class MarketplaceCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $totalItem = 3;
        MarketplaceCategory::truncate();

        $randomCategories = [
            [
                ['Clothing', 'Shoes', 'Accessories'],
                ['Make-up', 'Skin care', 'Hair Care', 'Personal Care'],
            ],
            [
                ['Phone', 'Camera', 'Video Game'],
                ['PC', 'PS4', 'PS3', 'Xbox'],
                ['Computer Component', 'Laptop', 'Data Storage'],
            ],
            [
                ['Children Book', 'Literature & Fiction', 'Scifi & fantasy', 'Educational Book'],
            ],
        ];

        $marketplaces = Marketplace::get();
        $index = 0;
        foreach ($marketplaces as $marketplace) {
            $randomCategoryIndex = $index % 3;
            $categories = $randomCategories[$randomCategoryIndex];
            $imageDirectory = 'images';
            $pathDirectory = public_path().'/'.$imageDirectory;

            $categorySortOrder = 1;
            foreach ($categories as $subcategories) {
                foreach ($subcategories as $categoryName) {
                    $category = new MarketplaceCategory;
                    $category->marketplace_id = $marketplace->id;
                    $category->parent_id = 0;
                    $category->type = 'product category';
                    $category->name = $categoryName;
                    $category->slug = str_slug($categoryName);
                    $category->description = $faker->paragraph(4, true);
                    $category->image = $imageDirectory .'/no-image-v2.png';
                    $category->active = true;
                    $category->sort_order = $categorySortOrder;
                    $category->save();

                    $categorySortOrder++;
                }
            }

            $index++;
        }
    }
}
