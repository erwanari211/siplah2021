<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreCategory;
use DB;

class StoreCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        StoreCategory::truncate();
        $stores = Store::get();

        foreach ($stores as $store) {
            $imageDirectory = 'images';
            $pathDirectory = public_path().'/'.$imageDirectory;
            $totalItem = $faker->numberBetween(1, 5);
            for ($i=1; $i <= $totalItem; $i++) {
                $category = new StoreCategory;
                $category->store_id = $store->id;
                $category->parent_id = 0;
                $category->type = 'product collection';
                $category->name = 'Collection '.$i;
                $category->slug = str_slug($category->name);
                $category->description = $faker->paragraph(4, true);
                $category->image = $imageDirectory .'/no-image-v2.png';
                $category->active = true;
                $category->sort_order = $i;
                $category->save();
            }
        }
    }
}
