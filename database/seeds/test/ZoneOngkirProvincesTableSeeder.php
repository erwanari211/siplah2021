<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use RajaOngkir;
use DB;

class ZoneOngkirProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = RajaOngkir::Provinsi()->all();
        DB::table('zone_ongkir_provinces')->truncate();
        DB::table('zone_ongkir_provinces')->insert($data);
    }
}
