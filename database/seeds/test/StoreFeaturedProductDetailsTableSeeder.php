<?php

namespace database\seeds\test;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreFeaturedProduct;
use App\Models\Modules\Marketplaces\StoreFeaturedProductDetail;
use DB;

class StoreFeaturedProductDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        StoreFeaturedProductDetail::truncate();
        $stores = Store::get();

        foreach ($stores as $store) {
            $featuredProducts = $store->storeFeaturedProducts;
            $products = $store->products;
            $totalRandomProduct = count($products) < 8 ? count($products) : 8;
            $randomProducts = $faker->randomElements($products, $totalRandomProduct);
            foreach ($featuredProducts as $featuredProduct) {
                foreach ($randomProducts as $product) {
                    $detail = new StoreFeaturedProductDetail;
                    $detail->store_featured_product_id = $featuredProduct->id;
                    $detail->product_id = $product->id;
                    $detail->save();
                }
            }
        }
    }
}
