<?php

namespace database\seeds\kemdikbud;

use Illuminate\Database\Seeder;
use DB;

class KemdikbudDistrictsSeeder extends Seeder
{
    public function run()
    {
        $table = 'kemdikbud_districts';
        $file = base_path().'/database/seeds/kemdikbud/csv/kemdikbud_districts.csv';
        $header = array('id', 'city_id', 'name', 'zone');

        $Csv = new CsvtoArray;
        $data = $Csv->csv_to_array($file, $header);

        $collection = collect($data);
        $collection->shift(); // remove header

        foreach($collection->chunk(50) as $chunk) {
            DB::table($table)->insert($chunk->toArray());
        }
    }
}
