<?php

namespace database\seeds\kemdikbud;

use Illuminate\Database\Seeder;
use DB;

class KemdikbudIndonesiaCitiesSeeder extends Seeder
{
    public function run()
    {
        $table = 'indonesia_city_kemdikbud';
        $file = base_path().'/database/seeds/kemdikbud/csv/kemdikbud_indonesia_cities.csv';
        $header = array('kemdikbud_city_id', 'indonesia_city_id');

        $Csv = new CsvtoArray;
        $data = $Csv->csv_to_array($file, $header);

        $collection = collect($data);
        $collection->shift(); // remove header

        foreach($collection->chunk(50) as $chunk) {
            DB::table($table)->insert($chunk->toArray());
        }
    }
}
