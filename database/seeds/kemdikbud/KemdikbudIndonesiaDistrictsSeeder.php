<?php

namespace database\seeds\kemdikbud;

use Illuminate\Database\Seeder;
use DB;

class KemdikbudIndonesiaDistrictsSeeder extends Seeder
{
    public function run()
    {
        $table = 'indonesia_district_kemdikbud';
        $file = base_path().'/database/seeds/kemdikbud/csv/kemdikbud_indonesia_districts.csv';
        $header = array('kemdikbud_district_id', 'indonesia_district_id');

        $Csv = new CsvtoArray;
        $data = $Csv->csv_to_array($file, $header);

        $collection = collect($data);
        $collection->shift(); // remove header

        foreach($collection->chunk(50) as $chunk) {
            DB::table($table)->insert($chunk->toArray());
        }
    }
}
