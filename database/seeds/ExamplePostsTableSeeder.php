<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Examples\ExamplePost;
use Carbon\Carbon;

class ExamplePostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $totalItem = 50;
        $date = Carbon::now();
        $date->hour = 20;
        $date->minute = 0;
        $date->second = 0;
        $date->modify('-1 year');

        ExamplePost::truncate();
        for ($i=0; $i < $totalItem; $i++) {
            $date->addDays(rand(5,10));

            $post = new ExamplePost;
            $post->title = $faker->sentence(4, true);
            $post->slug = $faker->slug();
            $post->content = $faker->sentence(250, true);
            $post->excerpt = $faker->sentence(30, true);
            $post->published_at = $date;
            $post->views = rand(10,30) * 5;
            $post->created_at = $date;
            $post->updated_at = $date;
            $post->save();
        }
    }
}
