<?php

namespace database\seeds\rajaongkir;

use Illuminate\Database\Seeder;
use DB;

class RajaongkirProvincesSeeder extends Seeder
{
    public function run()
    {
        $table = 'rajaongkir_provinces';
        $file = base_path().'/database/seeds/rajaongkir/csv/rajaongkir_provinces.csv';
        $header = array('province_id', 'province');

        $Csv = new CsvtoArray;
        $data = $Csv->csv_to_array($file, $header);

        $collection = collect($data);
        $collection->shift(); // remove header

        foreach($collection->chunk(50) as $chunk) {
            DB::table($table)->insert($chunk->toArray());
        }
    }
}
