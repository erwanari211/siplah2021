<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndonesiaProvinceRajaongkirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indonesia_province_rajaongkir', function (Blueprint $table) {
            $table->char('indonesia_province_id', 15);
            $table->unsignedBigInteger('rajaongkir_province_id');

            $table->index('indonesia_province_id');
            $table->index('rajaongkir_province_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indonesia_province_rajaongkir');
    }
}
