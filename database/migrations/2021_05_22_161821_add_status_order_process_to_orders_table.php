<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusOrderProcessToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('status_order_process')->default('pending')->nullable();
            $table->string('status_order_confirmation')->default('pending')->nullable();
            $table->string('status_packing')->default('pending')->nullable();
            $table->string('status_shipping')->default('pending')->nullable();
            $table->string('status_delivery')->default('pending')->nullable();
            $table->string('status_delivery_document')->default('pending')->nullable();
            $table->string('status_payment_from_customer')->default('unpaid')->nullable();
            $table->string('status_payment_to_store')->default('unpaid')->nullable();
            $table->string('status_changed_by_customer')->default('not_changed')->nullable();
            $table->string('status_canceled')->default('not_canceled')->nullable();
            $table->string('status_complain_source')->default('no_complain')->nullable();
            $table->string('status_complain_request')->default('no_complain')->nullable();
            $table->string('status_complain_status')->default('no_complain')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('status_order_process');
            $table->dropColumn('status_order_confirmation');
            $table->dropColumn('status_packing');
            $table->dropColumn('status_shipping');
            $table->dropColumn('status_delivery');
            $table->dropColumn('status_delivery_document');
            $table->dropColumn('status_payment_from_customer');
            $table->dropColumn('status_payment_to_store');
            $table->dropColumn('status_changed_by_customer');
            $table->dropColumn('status_canceled');
            $table->dropColumn('status_complain_source');
            $table->dropColumn('status_complain_request');
            $table->dropColumn('status_complain_status');
        });
    }
}
