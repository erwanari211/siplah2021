<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreMediaLibrariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_media_libraries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('store_id');
            $table->string('unique_code')->nullable();
            $table->string('group')->default('gallery');
            $table->string('mimetype')->nullable();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('url');
            $table->boolean('active')->default(1);
            $table->string('tags')->nullable();
            $table->boolean('is_public')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_media_libraries');
    }
}
