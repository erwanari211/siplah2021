<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistrictIdToStoreAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_addresses', function (Blueprint $table) {
            $table->char('province_id', 2)->default(0)->change();
            $table->char('city_id', 4)->default(0)->change();
            $table->char('district_id', 7)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_addresses', function (Blueprint $table) {
            $table->integer('province_id')->unsigned()->default(0)->change();
            $table->integer('city_id')->unsigned()->default(0)->change();
            $table->dropColumn('district_id');
        });
    }
}
