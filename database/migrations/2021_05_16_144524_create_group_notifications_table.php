<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('groupable_type');
            $table->unsignedInteger('groupable_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->text('data')->nullable();
            $table->datetime('read_at')->nullable();
            $table->unsignedInteger('read_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_notifications');
    }
}
