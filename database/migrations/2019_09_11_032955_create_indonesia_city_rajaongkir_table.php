<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndonesiaCityRajaongkirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indonesia_city_rajaongkir', function (Blueprint $table) {
            $table->char('indonesia_city_id', 15);
            $table->unsignedBigInteger('rajaongkir_city_id');
            $table->boolean('is_exact')->default(true);

            $table->index('indonesia_city_id');
            $table->index('rajaongkir_city_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indonesia_city_rajaongkir');
    }
}
