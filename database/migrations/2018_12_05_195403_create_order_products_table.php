<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->default(0)->unsigned();
            $table->integer('product_id')->default(0)->unsigned();
            $table->string('product_name');
            $table->integer('qty')->default(1)->unsigned();
            $table->decimal('price')->default(0)->unsigned();
            $table->decimal('total')->default(0)->unsigned();
            $table->text('note')->nullable();
            $table->integer('sort_order')->default(1)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
