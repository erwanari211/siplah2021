<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKemdikbudSiplahRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kemdikbud_siplah_regions', function (Blueprint $table) {
            $table->char('region_id', 7);
            $table->char('indonesia_city_id', 4);
            $table->string('kab_kota');
            $table->string('province');
            $table->string('province_map');
            $table->decimal('kab_lat', 10, 8)->nullable();
            $table->decimal('kab_long', 11, 8)->nullable();
            $table->decimal('prov_lat', 10, 8)->nullable();
            $table->decimal('prov_long', 11, 8)->nullable();
            $table->primary('region_id');
            $table->index('indonesia_city_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kemdikbud_siplah_regions');
    }
}
