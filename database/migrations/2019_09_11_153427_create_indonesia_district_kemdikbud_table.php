<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndonesiaDistrictKemdikbudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indonesia_district_kemdikbud', function (Blueprint $table) {
            $table->char('indonesia_district_id', 15);
            $table->char('kemdikbud_district_id', 15);

            $table->index('indonesia_district_id');
            $table->index('kemdikbud_district_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indonesia_district_kemdikbud');
    }
}
