<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKemdikbudDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kemdikbud_districts', function (Blueprint $table) {
            $table->char('id', 15);
            $table->char('city_id', 15);
            $table->string('name');
            $table->unsignedInteger('zone');

            $table->primary('id');
            $table->index('city_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kemdikbud_districts');
    }
}
