<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketplaceMediaLibrariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplace_media_libraries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('marketplace_id');
            $table->string('unique_code')->nullable();
            $table->string('group')->default('gallery');
            $table->string('mimetype')->nullable();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('url');
            $table->boolean('active')->default(1);
            $table->string('tags')->nullable();
            $table->boolean('is_public')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketplace_media_libraries');
    }
}
