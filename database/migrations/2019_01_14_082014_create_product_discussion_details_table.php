<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDiscussionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_discussion_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_discussion_id');
            $table->unsignedInteger('user_id');
            $table->boolean('is_store_owner')->default(false);
            $table->text('content');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_discussion_details');
    }
}
