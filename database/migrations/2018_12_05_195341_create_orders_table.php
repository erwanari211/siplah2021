<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('store_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('store_name');
            $table->string('user_name');

            $table->decimal('total')->unsigned()->default(0);
            $table->string('currency')->default('idr');
            $table->integer('order_status_id')->unsigned()->default(0);
            $table->string('order_status')->default('pending');

            $table->string('invoice_prefix')->nullable();
            $table->string('invoice_no')->nullable();

            $table->string('ip')->nullable();
            $table->string('user_agent')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
