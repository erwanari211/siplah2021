<?php

namespace Tests\Unit\Http;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HelpersTest extends TestCase
{
    use RefreshDatabase;

    /** @test **/
    public function get_default_marketplace()
    {
        $hasKey = cache()->has('defaultMarketplace');
        $this->assertFalse($hasKey);

        $this->createMarketplace();
        $marketplaceId = config('marketplace_app.default_marketplace_id');
        $defaultMarketplace = get_default_marketplace();
        $this->assertEquals($marketplaceId, $defaultMarketplace->id);

        $hasKey = cache()->has('defaultMarketplace');
        $this->assertTrue($hasKey);
    }

    public function createMarketplace()
    {
        $imageDirectory = 'images';
        $marketplace = new \App\Models\Modules\Marketplaces\Marketplace;
        $marketplace->user_id = 0;
        $marketplace->name = 'Marketplace';
        $marketplace->slug = str_slug($marketplace->name);
        $marketplace->image = $imageDirectory .'/no-image-v2.png';
        $marketplace->logo = $imageDirectory .'/no-image-v2.png';
        $marketplace->active = true;
        $marketplace->unique_code = $marketplace->randomUniqueCode();
        $marketplace->save();
    }
}
