<?php

namespace Tests\Feature\Http;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\UserLog;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\School;

class UserLogTest extends TestCase
{
    use RefreshDatabase;

    /** @test **/
    public function log_marketplace_admin()
    {
        $this->createUser();
        $this->createMarketplace();

        $user = $this->data['user'];
        $marketplace = $this->data['marketplace'];

        $activityname = 'login';
        $logAdditionalData = [
            'old' => [
                'foo' => 'bar'
            ],
            'new' => [
                'foo' => 'baz'
            ],
        ];
        $log = $user->addLog($activityname, [
            'team' => $marketplace,
            'data' => $logAdditionalData,
        ]);

        $this->assertEquals($log->user_id, $user->id);
        $this->assertEquals($log->team_type, 'marketplace');
        $this->assertEquals($log->teamable_id, $marketplace->id);
        $this->assertEquals($log->name, $activityname);
        $this->assertEquals($log->data, json_encode($logAdditionalData));
    }

    /** @test **/
    public function log_store_admin()
    {
        $this->createUser();
        $this->createStore();

        $user = $this->data['user'];
        $store = $this->data['store'];

        $activityname = 'login';
        $logAdditionalData = [
            'old' => [
                'foo' => 'bar'
            ],
            'new' => [
                'foo' => 'baz'
            ],
        ];
        $log = $user->addLog($activityname, [
            'team' => $store,
            'data' => $logAdditionalData,
        ]);

        $this->assertEquals($log->user_id, $user->id);
        $this->assertEquals($log->team_type, 'store');
        $this->assertEquals($log->teamable_id, $store->id);
        $this->assertEquals($log->name, $activityname);
        $this->assertEquals($log->data, json_encode($logAdditionalData));
    }

    /** @test **/
    public function log_school_admin()
    {
        $this->createUser();
        $this->createSchool();

        $user = $this->data['user'];
        $school = $this->data['school'];

        $activityname = 'login';
        $logAdditionalData = [
            'old' => [
                'foo' => 'bar'
            ],
            'new' => [
                'foo' => 'baz'
            ],
        ];
        $log = $user->addLog($activityname, [
            'team' => $school,
            'data' => $logAdditionalData,
        ]);

        $this->assertEquals($log->user_id, $user->id);
        $this->assertEquals($log->team_type, 'school');
        $this->assertEquals($log->teamable_id, $school->id);
        $this->assertEquals($log->name, $activityname);
        $this->assertEquals($log->data, json_encode($logAdditionalData));
    }

    /** @test **/
    public function filter_log()
    {
        $this->createUser();
        $this->createMarketplace();
        $this->createStore();
        $this->createSchool();

        $this->createSampleLogs();

        $user = $this->data['user'];
        $marketplace = $this->data['marketplace'];
        $store = $this->data['store'];
        $school = $this->data['school'];

        $sampleLogData = $this->data['sampleLogData'];

        $marketplaceCount = UserLog::filterByTeam($marketplace)->count();
        $this->assertEquals($marketplaceCount, $sampleLogData['marketplace']['count']);

        $storeCount = UserLog::filterByTeam($store)->count();
        $this->assertEquals($storeCount, $sampleLogData['store']['count']);

        $schoolCount = UserLog::filterByTeam($school)->count();
        $this->assertEquals($schoolCount, $sampleLogData['school']['count']);
    }

    public function createUser()
    {
        $user = new User;
        $user->name = 'admin';
        $user->username = 'admin';
        $user->email = 'admin@app.com';
        $user->password = bcrypt(12345678);
        $user->save();
        $this->data['user'] = $user;
    }

    public function createMarketplace()
    {
        $user = $this->data['user'];

        $marketplace = new Marketplace;
        $marketplace->unique_code = str_random(5);
        $marketplace->user_id = $user->id;
        $marketplace->name = 'Marketplace 1';
        $marketplace->slug = 'marketplace-1';
        $marketplace->save();
        $this->data['marketplace'] = $marketplace;
    }

    public function createStore()
    {
        $user = $this->data['user'];

        $store = new Store;
        $store->marketplace_id = 1;
        $store->unique_code = str_random(5);
        $store->user_id = $user->id;
        $store->name = 'store 1';
        $store->slug = 'store-1';
        $store->save();
        $this->data['store'] = $store;
    }

    public function createSchool()
    {
        $user = $this->data['user'];

        $school = new School;
        $school->unique_code = str_random(5);
        $school->user_id = $user->id;
        $school->name = 'school 1';
        $school->slug = 'school-1';
        $school->save();
        $this->data['school'] = $school;
    }

    public function createSampleLogs()
    {
        $user = $this->data['user'];
        $marketplace = $this->data['marketplace'];
        $store = $this->data['store'];
        $school = $this->data['school'];

        $sampleLogData = [
            'marketplace' => [
                'team' => $marketplace,
                'count' => 2,
            ],
            'store' => [
                'team' => $store,
                'count' => 5,
            ],
            'school' => [
                'team' => $school,
                'count' => 10,
            ],
        ];
        $this->data['sampleLogData'] = $sampleLogData;

        $activityname = 'login';
        $logAdditionalData = [
            'old' => [
                'foo' => 'bar'
            ],
            'new' => [
                'foo' => 'baz'
            ],
        ];

        foreach ($sampleLogData as $key => $data) {
            $count = $data['count'];
            $team = $data['team'];
            for ($i=0; $i < $count; $i++) {
                $user->addLog($activityname, [
                    'team' => $team,
                    'data' => $logAdditionalData,
                ]);
            }
        }
    }
}
