<?php

namespace Tests\Feature\Http;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\School;
use App\User;
use App\Models\Modules\Marketplaces\StoreMessage;
use App\Models\Modules\Marketplaces\StoreMessageDetail;

class StoreMessageTest extends TestCase
{
    use RefreshDatabase;

    /** @test **/
    public function basic()
    {
        $this->createStore();
        $this->createSchool();
        $this->createStoreMessage();
        $this->createStoreMessageDetail();

        $message = $this->data['message'];
        $this->assertEquals($message->store_id, $this->data['store']->id);
        $this->assertEquals($message->senderable_type, get_class($this->data['school']));
        $this->assertEquals($message->senderable_id, $this->data['school']->id);
        $this->assertEquals($message->sender_id, $this->data['school_admin']->id);
        $this->assertEquals($message->senderable->id, $this->data['school']->id);
        $this->assertEquals($message->user->id, $this->data['school_admin']->id);
        $this->assertEquals($message->sender->id, $this->data['school_admin']->id);

        $detail = $message->details()->first();
        $this->assertEquals($detail->store_message_id, $message->id);
        $detailFrom = $detail->from;
        if ($detailFrom == 'sender') {
            $this->assertEquals($detail->from_name, $this->data['school']->name);
        }
        if ($detailFrom == 'receiver') {
            $this->assertEquals($detail->from_name, $this->data['store']->name);
        }
    }

    public function createStore()
    {
        $user = new User;
        $user->name = 'admin toko';
        $user->username = 'admin_toko';
        $user->email = 'admin_toko@app.com';
        $user->password = bcrypt(12345678);
        $user->save();
        $this->data['store_admin'] = $user;

        $store = new Store;
        $store->marketplace_id = 1;
        $store->user_id = $user->id;
        $store->name = 'Toko 1';
        $store->slug = 'toko-1';
        $store->save();
        $this->data['store'] = $store;
    }

    public function createSchool()
    {
        $user = new User;
        $user->name = 'admin sekolah';
        $user->username = 'admin_sekolah';
        $user->email = 'admin_sekolah@app.com';
        $user->password = bcrypt(12345678);
        $user->save();
        $this->data['school_admin'] = $user;

        $school = new School;
        $school->unique_code = str_random(5);
        $school->user_id = $user->id;
        $school->name = 'Sekolah 1';
        $school->slug = 'sekolah-1';
        $school->save();
        $this->data['school'] = $school;
    }

    public function createStoreMessage()
    {
        $message = new StoreMessage;
        $message->store_id = $this->data['store']->id;
        $message->senderable_type = get_class($this->data['school']);
        $message->senderable_id = $this->data['school']->id;
        $message->sender_id = $this->data['school_admin']->id;
        $message->type = 'complain';
        $message->status = 'open';
        $message->save();

        $this->data['message'] = $message;
    }

    public function createStoreMessageDetail()
    {
        $message = $this->data['message'];
        $school_admin = $this->data['school_admin'];
        $store_admin = $this->data['store_admin'];

        for ($i=0; $i < 5; $i++) {
            $detail = new StoreMessageDetail;
            $detail->store_message_id = $message->id;

            $random = rand(1, 9);
            if ($random > 4) {
                // school
                $detail->user_id = $school_admin->id;
                $detail->from = 'sender';
            }

            if ($random <= 4) {
                // store
                $detail->user_id = $store_admin->id;
                $detail->from = 'receiver';
            }

            $detail->message = str_random(8);
            $detail->save();

        }
    }
}
