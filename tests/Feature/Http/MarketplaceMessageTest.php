<?php

namespace Tests\Feature\Http;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\School;
use App\Models\Modules\Marketplaces\Marketplace;
use App\User;
use App\Models\Modules\Marketplaces\MarketplaceMessage;
use App\Models\Modules\Marketplaces\MarketplaceMessageDetail;

class MarketplaceMessageTest extends TestCase
{
    use RefreshDatabase;

    /** @test **/
    public function basic()
    {
        $this->createStore();
        $this->createMarketplace();
        $this->createMarketplaceMessage();
        $this->createMarketplaceMessageDetail();

        $message = $this->data['message'];
        $this->assertEquals($message->marketplace_id, $this->data['store']->id);
        $this->assertEquals($message->senderable_type, get_class($this->data['store']));
        $this->assertEquals($message->senderable_id, $this->data['store']->id);
        $this->assertEquals($message->sender_id, $this->data['marketplace_admin']->id);
        $this->assertEquals($message->senderable->id, $this->data['store']->id);
        $this->assertEquals($message->user->id, $this->data['marketplace_admin']->id);
        $this->assertEquals($message->sender->id, $this->data['marketplace_admin']->id);

        $detail = $message->details()->first();
        $this->assertEquals($detail->marketplace_message_id, $message->id);
        $detailFrom = $detail->from;

        if ($detailFrom == 'sender') {
            $this->assertEquals($detail->from_name, $this->data['store']->name);
        }

        if ($detailFrom == 'receiver') {
            $this->assertEquals($detail->from_name, $this->data['marketplace']->name);
        }
    }

    public function createStore()
    {
        $user = new User;
        $user->name = 'admin toko';
        $user->username = 'admin_toko';
        $user->email = 'admin_toko@app.com';
        $user->password = bcrypt(12345678);
        $user->save();
        $this->data['store_admin'] = $user;

        $store = new Store;
        $store->marketplace_id = 1;
        $store->user_id = $user->id;
        $store->name = 'Toko 1';
        $store->slug = 'toko-1';
        $store->save();
        $this->data['store'] = $store;
    }

    public function createMarketplace()
    {
        $user = new User;
        $user->name = 'admin marketplace';
        $user->username = 'admin_marketplace';
        $user->email = 'admin_marketplace@app.com';
        $user->password = bcrypt(12345678);
        $user->save();
        $this->data['marketplace_admin'] = $user;

        $marketplace = new Marketplace;
        $marketplace->unique_code = str_random(5);
        $marketplace->user_id = $user->id;
        $marketplace->name = 'Marketplace 1';
        $marketplace->slug = 'marketplace-1';
        $marketplace->save();
        $this->data['marketplace'] = $marketplace;
    }

    public function createMarketplaceMessage()
    {
        $message = new MarketplaceMessage;
        $message->marketplace_id = $this->data['marketplace']->id;
        $message->senderable_type = get_class($this->data['store']);
        $message->senderable_id = $this->data['store']->id;
        $message->sender_id = $this->data['marketplace_admin']->id;
        $message->type = 'complain';
        $message->status = 'open';
        $message->save();

        $this->data['message'] = $message;
    }

    public function createMarketplaceMessageDetail()
    {
        $message = $this->data['message'];
        $marketplace_admin = $this->data['marketplace_admin'];
        $store_admin = $this->data['store_admin'];

        for ($i=0; $i < 5; $i++) {
            $detail = new MarketplaceMessageDetail;
            $detail->marketplace_message_id = $message->id;

            $random = rand(1, 9);
            if ($random > 4) {
                // store
                $detail->user_id = $store_admin->id;
                $detail->from = 'sender';
            }

            if ($random <= 4) {
                // marketplace
                $detail->user_id = $marketplace_admin->id;
                $detail->from = 'receiver';
            }

            $detail->message = str_random(8);
            $detail->save();

        }
    }
}
