<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Modules\Website\Frontend\HomepageController@displayHomepage');

Auth::routes();

Route::get('/register-store', 'Modules\Marketplaces\Frontend\RegisterStoreController@create')->name('marketplaces.stores.register.create');
Route::post('/register-store', 'Modules\Marketplaces\Frontend\RegisterStoreController@store')->name('marketplaces.stores.register.store');

Route::get('/login-store', 'Modules\Marketplaces\Frontend\LoginStoreController@create')->name('marketplaces.stores.login.create');
Route::post('/login-store', 'Modules\Marketplaces\Frontend\LoginStoreController@store')->name('marketplaces.stores.login.store');

Route::get('/login-marketplace', 'Modules\Marketplaces\Frontend\LoginMarketplaceController@create')->name('marketplaces.login.create');
Route::post('/login-marketplace', 'Modules\Marketplaces\Frontend\LoginMarketplaceController@store')->name('marketplaces.login.store');

Route::get('/login-supervisor-group', 'Modules\Marketplaces\Frontend\LoginSupervisorGroupController@create')->name('marketplaces.supervisor-groups.login.create');
Route::post('/login-supervisor-group', 'Modules\Marketplaces\Frontend\LoginSupervisorGroupController@store')->name('marketplaces.supervisor-groups.login.store');

Route::get('/login-school', 'Modules\Marketplaces\Frontend\LoginSchoolController@create')->name('marketplaces.schools.login.create');
Route::get('/login-school/callback', 'Modules\Marketplaces\Frontend\LoginSchoolController@store')->name('marketplaces.schools.login.store');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'example'], function() {
    Route::group(['prefix' => 'form'], function() {
        Route::get('/create', 'Examples\FormController@create');
        Route::post('/', 'Examples\FormController@store');
        Route::get('/{id}/edit', 'Examples\FormController@edit');
        Route::put('/{id}', 'Examples\FormController@update');
    });

    Route::group(['prefix' => 'flash'], function() {
        Route::get('/', 'Examples\FlashController@index');
    });

    Route::group(['prefix' => 'layout'], function() {
        Route::view('/', 'examples.blade.index');
    });

    Route::group(['prefix' => 'composer-views'], function() {
        Route::get('/', 'Examples\ComposerViewsController@index');
        Route::get('/{id}', 'Examples\ComposerViewsController@show');
        Route::get('/category/{category}', 'Examples\ComposerViewsController@filterByCategory');
    });

    Route::group(['prefix' => 'file-upload'], function() {
        Route::get('/basic', 'Examples\FileUploadController@create');
        Route::post('/basic', 'Examples\FileUploadController@store');
        Route::get('/multiple', 'Examples\MultipleFileUploadController@create');
        Route::post('/multiple', 'Examples\MultipleFileUploadController@store');
        Route::get('/image-resize', 'Examples\ImageResizeController@create');
        Route::post('/image-resize', 'Examples\ImageResizeController@store');
    });

    Route::resource('todos', 'Examples\ExampleTodosController', ['as'=>'examples']);
    Route::resource('send-email', 'Examples\SendEmailController', ['as'=>'examples']);
    Route::group(['prefix' => 'adminlte'], function() {
        Route::get('/', 'Examples\AdminlteController@index');
    });
    Route::resource('posts', 'Examples\ExamplePostsController', ['as'=>'examples']);
    Route::resource('trashed-posts', 'Examples\ExampleTrashedPostsController', ['as'=>'examples']);
    Route::resource('ajax-posts', 'Examples\ExampleAjaxPostsController', ['as'=>'examples']);

    Route::group(['prefix' => 'wilayah'], function() {
        Route::get('wilayah', 'Modules\Wilayah\Examples\WilayahIndonesiaController@index');
        Route::get('wilayah2', 'Modules\Wilayah\Examples\WilayahIndonesiaController@index2');
        Route::get('wilayah-k', 'Modules\Wilayah\Examples\WilayahIndonesiaController@indexKemdikbud');
        Route::get('wilayah-ro', 'Modules\Wilayah\Examples\WilayahIndonesiaController@indexRajaongkir');

        Route::get('kemdikbud', 'Modules\Wilayah\Examples\KemdikbudWilayahController@index');
        Route::get('kemdikbud2', 'Modules\Wilayah\Examples\KemdikbudWilayahController@index2');

        Route::get('rajaongkir', 'Modules\Wilayah\Examples\RajaongkirWilayahController@index');
        Route::get('rajaongkir2', 'Modules\Wilayah\Examples\RajaongkirWilayahController@index2');
    });

});

/**
 * /m                  : marketplaces.index
 * /m/{id}             : marketplaces.show
 * /m/{id}/c/{id}      : marketplaces.categories.show
 * /m/{id}/p           : marketplaces.products
 * /m/{id}/s           : marketplaces.stores.index
 *
 * /m/{id}/s/create        : marketplaces.stores.create
 * /m/{id}/s/{id}          : marketplaces.stores.show
 * /m/{id}/s/{id}/c/{id}   : marketplaces.stores.categories.show
 * /m/{id}/s/{id}/n        : marketplaces.stores.notes.index
 * /m/{id}/s/{id}/n/{id}   : marketplaces.stores.notes.show
 * /m/{id}/s/{id}/p-dis    : marketplaces.stores.product-discussions.index
 * /m/{id}/s/{id}/p-rev    : marketplaces.stores.product-reviews.index
 *
 * /m/{id}/s/{id}/p                : marketplaces.stores.products.index
 * /m/{id}/s/{id}/p/{id}           : marketplaces.stores.products.show
 * /m/{id}/s/{id}/p/{id}/dis       : marketplaces.stores.products.discussions.index
 * /m/{id}/s/{id}/p/{id}/dis/{id}  : marketplaces.stores.products.discussions.show
 * /m/{id}/s/{id}/p/{id}/rev       : marketplaces.stores.products.reviews.show
 *
 * cart                             : cart
 * checkout/{id}                    : checkout
 * checkout/{id}/billing-address    : checkout.payment-address
 * checkout/{id}/shipping-address   : checkout.shipping-address
 * checkout/{id}/shipping-method    : checkout.shipping-method
 * checkout/{id}/payment-method     : checkout.payment-method
 * checkout/{id}/confirm            : checkout.confirm
 *
 * su/p--{id} : shorturl.products.show
 * w/page/{id} : website.pages.show
 *
 * users/account/profile
 * users/account/password
 * users/account/addresses
 * users/account/orders
 *
 * dashboard/stores/{id}
 * dashboard/stores/{id}/profile
 * dashboard/stores/{id}/orders
 * dashboard/stores/{id}/collections
 * dashboard/stores/{id}/products
 * dashboard/stores/{id}/products/{id}/attributes
 * dashboard/stores/{id}/products/{id}/galleries
 * dashboard/stores/{id}/products/{id}/grouped-product-details
 * dashboard/stores/{id}/products/{id}/import-grouped-product-details
 * dashboard/stores/{id}/import-products
 * dashboard/stores/{id}/banners
 * dashboard/stores/{id}/banner-groups
 * dashboard/stores/{id}/product-collections
 * dashboard/stores/{id}/menus
 * dashboard/stores/{id}/notes
 * dashboard/stores/{id}/media-libraries
 * dashboard/stores/{id}/settings/addresses
 * dashboard/stores/{id}/settings/payment-methods
 * dashboard/stores/{id}/settings/shipping-methods
 * dashboard/stores/{id}/settings/store-settings
 * dashboard/stores/{id}/settings/addresses
 *
 * dashboard/marketplaces/{id}/profile
 * dashboard/marketplaces/{id}/categories
 * dashboard/marketplaces/{id}/stores
 * dashboard/marketplaces/{id}/banners
 * dashboard/marketplaces/{id}/banner-groups
 * dashboard/marketplaces/{id}/popular-searches
 * dashboard/marketplaces/{id}/store-collections
 * dashboard/marketplaces/{id}/products-collections
 * dashboard/marketplaces/{id}/media-libraries
 * dashboard/marketplaces/{id}/settings
 *
 * administrator
 * administrator/marketplaces
 * administrator/marketplace-collections
 * administrator/product-collections
 * administrator/banners
 * administrator/banner-groups
 * administrator/cms/layout-elements
 * administrator/cms/pages
 * administrator/cms/media-libraries
 * administrator/settings
 *
 */

/**
 * Marketplaces
 */
Route::group(['prefix' => 'm'], function() {
    Route::get('/', 'Modules\Marketplaces\Frontend\MarketplacesController@index')
        ->name('marketplaces.index');
    Route::get('create-marketplace', 'Modules\Marketplaces\Frontend\MarketplacesController@create')
        ->name('marketplaces.create')->middleware(['auth']);
    Route::post('create-marketplace', 'Modules\Marketplaces\Frontend\MarketplacesController@store')
        ->name('marketplaces.store')->middleware(['auth']);

    Route::get('delivery-status', 'Modules\Marketplaces\Frontend\DeliveryStatusController@index');
    Route::post('delivery-status-ajax', 'Modules\Marketplaces\Frontend\DeliveryStatusController@indexAjax');

    Route::get('all-stores', 'Modules\Marketplaces\Frontend\StoresController@displayAllStore');

    Route::get('{marketplace}', 'Modules\Marketplaces\Frontend\MarketplacesController@show')
        ->name('marketplaces.show');

    /**
     * Marketplace Store
     */
    Route::get('{marketplace}/s', 'Modules\Marketplaces\Frontend\StoresController@index')
        ->name('stores.index');
    Route::get('{marketplace}/create-store', 'Modules\Marketplaces\Frontend\StoresController@create')
        ->name('stores.create')->middleware(['auth']);
    Route::post('{marketplace}/create-store', 'Modules\Marketplaces\Frontend\StoresController@store')
        ->name('stores.store')->middleware(['auth']);

    /**
     * Marketplace Categories
     */
    Route::group(['prefix' => '{marketplace}/c'], function() {
        Route::get('{category}', 'Modules\Marketplaces\Frontend\MarketplaceCategoriesController@show')
            ->name('marketplaces.categories.show');
    });

    /**
     * Marketplace Products
     */
    Route::get('{marketplace}/p', 'Modules\Marketplaces\Frontend\MarketplaceProductsController@index')
        ->name('marketplaces.products.index');
});


/**
 * Stores
 */
Route::group(['prefix' => 'm/{marketplace}/s'], function() {
    Route::get('{store}', 'Modules\Marketplaces\Frontend\StoresController@show')
        ->name('stores.show');
    Route::get('{store}/c/{categories}', 'Modules\Marketplaces\Frontend\StoreCategoriesController@show')
        ->name('stores.categories.show');
    Route::get('{store}/ajax/store-info', 'Modules\Marketplaces\Frontend\AjaxStoresController@getStoreInfo')
        ->name('stores.ajax.store-info');

    Route::get('{store}/n', 'Modules\Marketplaces\Frontend\StoreNotesController@index')
        ->name('stores.notes.index');
    Route::get('{store}/n/{note}', 'Modules\Marketplaces\Frontend\StoreNotesController@show')
        ->name('stores.notes.show');

    Route::get('{store}/p-discussions', 'Modules\Marketplaces\Frontend\StoreProductDiscussionsController@index')
        ->name('stores.product-discussions.index');
    Route::get('{store}/p-reviews', 'Modules\Marketplaces\Frontend\StoreProductReviewsController@index')
        ->name('stores.product-reviews.index');
});

/**
 * Products
 */
Route::group(['prefix' => 'm/{marketplace}/s/{store}/p'], function() {
    Route::get('/', 'Modules\Marketplaces\Frontend\StoreProductsController@index')
        ->name('products.index');
    Route::get('{product}', 'Modules\Marketplaces\Frontend\StoreProductsController@show')
        ->name('products.show');
});

/**
 * Product Discussions
 */
Route::group(['prefix' => 'm/{marketplace}/s/{store}/p/{product}/discussions'], function() {
    Route::get('/', 'Modules\Marketplaces\Frontend\ProductDiscussionsController@index')
        ->name('products.discussions.index');
    Route::post('/', 'Modules\Marketplaces\Frontend\ProductDiscussionsController@store')
        ->name('products.discussions.store')->middleware('auth');
    Route::get('{discussion}', 'Modules\Marketplaces\Frontend\ProductDiscussionsController@show')
        ->name('products.discussions.show');
    Route::post('/{discussion}/details', 'Modules\Marketplaces\Frontend\ProductDiscussionDetailsController@store')
        ->name('products.discussions.details.store')->middleware('auth');
    Route::put('/{discussion}/trash/{reply}', 'Modules\Marketplaces\Frontend\ProductDiscussionDetailsController@trash')
        ->name('products.discussions.details.trash')->middleware('auth');
    Route::put('/{discussion}/restore/{reply}', 'Modules\Marketplaces\Frontend\ProductDiscussionDetailsController@restore')
        ->name('products.discussions.details.restore')->middleware('auth');
});

/**
 * Product Reviews
 */
Route::group(['prefix' => 'm/{marketplace}/s/{store}/p/{product}/reviews'], function() {
    Route::get('/', 'Modules\Marketplaces\Frontend\ProductReviewsController@index')
        ->name('products.reviews.index');
    Route::post('/', 'Modules\Marketplaces\Frontend\ProductReviewsController@store')
        ->name('products.reviews.store')->middleware('auth');
});

/**
 * Cart
 */
Route::get('cart', 'Modules\Marketplaces\Frontend\CartController@index')
    ->name('cart.index');
Route::post('cart', 'Modules\Marketplaces\Frontend\CartController@store')
    ->name('cart.store');
Route::put('cart/{store}', 'Modules\Marketplaces\Frontend\CartController@update')
    ->name('cart.update');
Route::delete('cart/{store}', 'Modules\Marketplaces\Frontend\CartController@destroy')
    ->name('cart.destroy');

/**
 * Checkout
 */
Route::middleware(['auth'])->group(function () {
    Route::group(['prefix' => 'checkout/{store}'], function() {
        Route::get('/', 'Modules\Marketplaces\Frontend\CheckoutController@edit')
            ->name('checkout.edit');
        Route::group(['prefix' => 'billing-address'], function() {
            Route::get('/', 'Modules\Marketplaces\Frontend\CheckoutPaymentAddressController@edit')
                ->name('checkout.steps.billing-address.edit');
            Route::put('/', 'Modules\Marketplaces\Frontend\CheckoutPaymentAddressController@update')
                ->name('checkout.steps.billing-address.update');
        });
        Route::group(['prefix' => 'shipping-address'], function() {
            Route::get('/', 'Modules\Marketplaces\Frontend\CheckoutShippingAddressController@edit')
                ->name('checkout.steps.shipping-address.edit');
            Route::put('/', 'Modules\Marketplaces\Frontend\CheckoutShippingAddressController@update')
                ->name('checkout.steps.shipping-address.update');
        });
        Route::group(['prefix' => 'shipping-method'], function() {
            Route::get('/', 'Modules\Marketplaces\Frontend\CheckoutShippingMethodController@edit')
                ->name('checkout.steps.shipping-method.edit');
            Route::put('/', 'Modules\Marketplaces\Frontend\CheckoutShippingMethodController@update')
                ->name('checkout.steps.shipping-method.update');
        });
        Route::group(['prefix' => 'payment-method'], function() {
            Route::get('/', 'Modules\Marketplaces\Frontend\CheckoutPaymentMethodController@edit')
                ->name('checkout.steps.payment-method.edit');
            Route::put('/', 'Modules\Marketplaces\Frontend\CheckoutPaymentMethodController@update')
                ->name('checkout.steps.payment-method.update');
        });
        Route::get('confirm', 'Modules\Marketplaces\Frontend\CheckoutController@create')
            ->name('checkout.steps.confirm.show');
        Route::post('confirm', 'Modules\Marketplaces\Frontend\CheckoutController@store')
            ->name('checkout.steps.confirm.store');
    });
});

/**
 * Links
 */
Route::group(['prefix' => 'su'], function() {
    Route::get('/p--{code}', 'Modules\Links\UrlShortenersController@products')
        ->name('shorturl.products.show');
});

/**
 * Website Pages
 */
Route::group(['prefix' => 'w/pages'], function() {
    Route::get('/{slug}', 'Modules\Website\Frontend\PagesController@show')
        ->name('website.pages.show');
});

Route::get('/w/product-collections/{id}.json', 'Modules\Marketplaces\Backend\Administrators\WebsiteProductCollectionsController@showAjax');

/**
 * Blog
 */
Route::group(['prefix' => 'w/blog'], function() {
    Route::get('/', 'Modules\Website\Frontend\BlogPostsController@index')
        ->name('website.blog.posts.index');
    Route::get('/{slug}', 'Modules\Website\Frontend\BlogPostsController@show')
        ->name('website.blog.posts.show');

    Route::get('/category/{slug}', 'Modules\Website\Frontend\BlogCategoriesController@show')
        ->name('website.blog.categories.show');
});

/**
 * Accounts
 */
Route::middleware(['auth'])->group(function () {
    Route::group(['prefix' => 'users'], function() {
        Route::group(['prefix' => 'account'], function() {
            Route::get('profile', 'Modules\Users\AccountProfileController@index')
                ->name('users.accounts.profile.index');
            Route::put('profile', 'Modules\Users\AccountProfileController@update')
                ->name('users.accounts.profile.update');
            Route::get('password', 'Modules\Users\AccountPasswordController@edit')
                ->name('users.accounts.password.edit');
            Route::put('password', 'Modules\Users\AccountPasswordController@update')
                ->name('users.accounts.password.update');
            Route::resource('addresses', 'Modules\Users\AccountAddressesController', ['as'=>'users.accounts']);
        });

        Route::get('orders/{order}/download-pdf', 'Modules\Marketplaces\Backend\Users\OrdersController@downloadPdf')
            ->name('users.orders.show.download-pdf');

        Route::get('orders/{order}/document-surat-pesanan', 'Modules\Marketplaces\Backend\Users\OrdersExportController@exportSuratPesanan')
            ->name('users.orders.show.documents.surat-pesanan');
        Route::get('orders/{order}/document-survey-barang', 'Modules\Marketplaces\Backend\Users\OrdersExportController@exportSurveyBarang')
            ->name('users.orders.show.documents.survey-barang');
        Route::get('orders/{order}/document-survey-berita-acara', 'Modules\Marketplaces\Backend\Users\OrdersExportController@exportSurveyBeritaAcara')
            ->name('users.orders.show.documents.survey-berita-acara');
        Route::get('orders/{order}/document-survey-berita-acara-lampiran', 'Modules\Marketplaces\Backend\Users\OrdersExportController@exportSurveyBeritaAcaraLampiran')
            ->name('users.orders.show.documents.survey-berita-acara-lampiran');
        Route::get('orders/{order}/document-hps', 'Modules\Marketplaces\Backend\Users\OrdersExportController@exportHps')
            ->name('users.orders.show.documents.hps');
        Route::get('orders/{order}/document-spk', 'Modules\Marketplaces\Backend\Users\OrdersExportController@exportSpk')
            ->name('users.orders.show.documents.spk');
        Route::get('orders/{order}/document-spk-lampiran', 'Modules\Marketplaces\Backend\Users\OrdersExportController@exportSpkLampiran')
            ->name('users.orders.show.documents.spk-lampiran');
        Route::get('orders/{order}/document-bappbj', 'Modules\Marketplaces\Backend\Users\OrdersExportController@exportBappbj')
            ->name('users.orders.show.documents.bappbj');
        Route::get('orders/{order}/document-bappbj-lampiran', 'Modules\Marketplaces\Backend\Users\OrdersExportController@exportBappbjLampiran')
            ->name('users.orders.show.documents.bappbj-lampiran');
        Route::get('orders/{order}/document-berita-acara-pembayaran', 'Modules\Marketplaces\Backend\Users\OrdersExportController@exportBeritaAcaraPembayaran')
            ->name('users.orders.show.documents.berita-acara-pembayaran');

        Route::resource('orders', 'Modules\Marketplaces\Backend\Users\OrdersController', ['as'=>'users']);

        Route::post('order-statuses/{order}', 'Modules\Marketplaces\Backend\Users\OrderStatusController@store')
            ->name('users.order.statuses.store');
        Route::post('order-statuses-delivery-document/{order}', 'Modules\Marketplaces\Backend\Users\OrderStatusController@storeDeliveryDocument')
            ->name('users.order.statuses.delivery-document.store');
        Route::post('order-statuses-payment/{order}', 'Modules\Marketplaces\Backend\Users\OrderStatusController@storePayment')
            ->name('users.order.statuses.payment-from-customer.store');

        Route::post('order-activities/negotiation-comments/{order}',
            'Modules\Marketplaces\Backend\Users\OrderActivitiesController@negotiationCommentsStore')
            ->name('users.order-activities.negotiation-comments.store');
        Route::post('order-activities/complain-comments/{order}',
            'Modules\Marketplaces\Backend\Users\OrderActivitiesController@complainCommentsStore')
            ->name('users.order-activities.complain-comments.store');
        Route::post('order-activities/documents/{order}',
            'Modules\Marketplaces\Backend\Users\OrderActivitiesController@documentsStore')
            ->name('users.order-activities.documents.store');
        Route::post('order-activities/payments/{order}',
            'Modules\Marketplaces\Backend\Users\OrderActivitiesController@paymentsStore')
            ->name('users.order-activities.payments.store');
        Route::group(['prefix' => 'payment-confirmation'], function() {
            Route::get('{order}', 'Modules\Marketplaces\Backend\Users\PaymentConfirmationsController@create')
                ->name('users.orders.payment-confirmations.create');
            Route::post('{order}', 'Modules\Marketplaces\Backend\Users\PaymentConfirmationsController@store')
                ->name('users.orders.payment-confirmations.store');
        });
    });
});

/**
 * Store Admin
 */
Route::middleware(['auth'])->group(function () {
    Route::group(['prefix' => 'dashboard/stores/{store}'], function() {
        Route::get('/', 'Modules\Marketplaces\Backend\Stores\StoresController@index')
            ->name('users.stores.index');

        Route::get('/profile', 'Modules\Marketplaces\Backend\Stores\StoresController@show')
            ->name('users.stores.show');
        Route::get('/profile/edit', 'Modules\Marketplaces\Backend\Stores\StoresController@edit')
            ->name('users.stores.edit');
        Route::put('/profile/edit', 'Modules\Marketplaces\Backend\Stores\StoresController@update')
            ->name('users.stores.update');

        Route::get('/profile/edit-geolocation', 'Modules\Marketplaces\Backend\Stores\StoresController@editGeolocation')
            ->name('users.stores.geolocation.edit');
        Route::put('/profile/edit-geolocation', 'Modules\Marketplaces\Backend\Stores\StoresController@updateGeolocation')
            ->name('users.stores.geolocation.update');

        Route::get('orders/{order}/download-pdf', 'Modules\Marketplaces\Backend\Stores\OrdersController@downloadPdf')
            ->name('users.stores.orders.show.download-pdf');

        Route::get('orders/{order}/document-bast', 'Modules\Marketplaces\Backend\Stores\OrdersExportController@exportBast')
            ->name('users.stores.orders.show.documents.bast');
        Route::get('orders/{order}/document-surat-jalan', 'Modules\Marketplaces\Backend\Stores\OrdersExportController@exportSuratJalan')
            ->name('users.stores.orders.show.documents.surat-jalan');
        Route::get('orders/{order}/document-faktur', 'Modules\Marketplaces\Backend\Stores\OrdersExportController@exportFaktur')
            ->name('users.stores.orders.show.documents.faktur');

        Route::resource('orders', 'Modules\Marketplaces\Backend\Stores\OrdersController', ['as'=>'users.stores']);

        Route::post('order-statuses/{order}', 'Modules\Marketplaces\Backend\Stores\OrderStatusController@store')
            ->name('users.stores.order.statuses.store');

        Route::post('order-histories/{order}', 'Modules\Marketplaces\Backend\Stores\OrderHistoriesController@store')
            ->name('users.stores.order-histories.store');
        Route::post('order-activities/negotiation-comments/{order}',
            'Modules\Marketplaces\Backend\Stores\OrderActivitiesController@negotiationCommentsStore')
            ->name('users.stores.order-activities.negotiation-comments.store');
        Route::post('order-activities/negotiation-prices/{order}',
            'Modules\Marketplaces\Backend\Stores\OrderActivitiesController@negotiationPricesStore')
            ->name('users.stores.order-activities.negotiation-prices.store');
        Route::post('order-activities/complain-comments/{order}',
            'Modules\Marketplaces\Backend\Stores\OrderActivitiesController@complainCommentsStore')
            ->name('users.stores.order-activities.complain-comments.store');
        Route::post('order-activities/documents/{order}',
            'Modules\Marketplaces\Backend\Stores\OrderActivitiesController@documentsStore')
            ->name('users.stores.order-activities.documents.store');

        Route::resource('collections', 'Modules\Marketplaces\Backend\Stores\StoreCategoriesController', ['as'=>'users.stores']);
        Route::post('collection-products/{collection}', 'Modules\Marketplaces\Backend\Stores\StoreCategoryProductController@store')
            ->name('users.stores.collections.products.store');
        Route::delete('collection-products/{collection}/{product}', 'Modules\Marketplaces\Backend\Stores\StoreCategoryProductController@destroy')
            ->name('users.stores.collections.products.destroy');

        Route::resource('products', 'Modules\Marketplaces\Backend\Stores\ProductsController', ['as'=>'users.stores']);
        Route::put('products/{product}/restore', 'Modules\Marketplaces\Backend\Stores\ProductsController@restore')
            ->name('users.stores.products.restore');
        Route::group(['prefix' => 'products/{product}'], function() {
            Route::resource('attributes', 'Modules\Marketplaces\Backend\Stores\ProductAttributesController', [
                'as'=>'users.stores.products'
            ]);
            Route::resource('galleries', 'Modules\Marketplaces\Backend\Stores\ProductGalleriesController', [
                'as'=>'users.stores.products'
            ]);
            Route::resource('grouped-products', 'Modules\Marketplaces\Backend\Stores\GroupedProductDetailsController', [
                'as'=>'users.stores.products'
            ]);

            Route::resource('zone-prices', 'Modules\Marketplaces\Backend\Stores\ProductKemdikbudZonePricesController', [
                'as'=>'users.stores.products'
            ]);

            Route::get('/import-zone-prices', 'Modules\Marketplaces\Backend\Stores\ProductKemdikbudZonePricesControllerImportController@create')
                ->name('users.stores.products.zone-prices.import.create');
            Route::post('/import-zone-prices', 'Modules\Marketplaces\Backend\Stores\ProductKemdikbudZonePricesControllerImportController@update')
                ->name('users.stores.products.zone-prices.import.update');
            Route::post('/import-zone-prices-template', 'Modules\Marketplaces\Backend\Stores\ProductKemdikbudZonePricesControllerImportController@downloadTemplate')
                ->name('users.stores.products.zone-prices.import.template');

            Route::get('/import-grouped-products', 'Modules\Marketplaces\Backend\Stores\GroupedProductDetailsImportController@create')
                ->name('users.stores.products.grouped-products.import.create');
            Route::post('/import-grouped-products', 'Modules\Marketplaces\Backend\Stores\GroupedProductDetailsImportController@update')
                ->name('users.stores.products.grouped-products.import.update');
            Route::get('/import-grouped-products-template', 'Modules\Marketplaces\Backend\Stores\GroupedProductDetailsImportController@downloadTemplate')
                ->name('users.stores.products.grouped-products.import.template');
        });

        Route::get('/import-products', 'Modules\Marketplaces\Backend\Stores\ProductsImportController@create')
            ->name('users.stores.import-products.create');
        Route::post('/import-products', 'Modules\Marketplaces\Backend\Stores\ProductsImportController@store')
            ->name('users.stores.import-products.store');
        Route::put('/import-products', 'Modules\Marketplaces\Backend\Stores\ProductsImportController@update')
            ->name('users.stores.import-products.update');

        Route::post('/export-products', 'Modules\Marketplaces\Backend\Stores\ProductsExportController@download')
            ->name('users.stores.export-products.download');

        Route::resource('banners', 'Modules\Marketplaces\Backend\Stores\StoreBannersController', ['as'=>'users.stores']);

        Route::resource('banner-groups', 'Modules\Marketplaces\Backend\Stores\StoreBannerGroupsController', ['as'=>'users.stores']);
        Route::post('banner-groups-details',
            'Modules\Marketplaces\Backend\Stores\StoreBannerGroupDetailsController@store')
            ->name('users.stores.banner-group-details.store');
        Route::delete('banner-groups-details/{bannerGroup}/{banner}',
            'Modules\Marketplaces\Backend\Stores\StoreBannerGroupDetailsController@destroy')
            ->name('users.stores.banner-group-details.destroy');

        Route::resource('featured-products', 'Modules\Marketplaces\Backend\Stores\StoreFeaturedProductsController', ['as'=>'users.stores']);
        Route::post('featured-product-details/{featuredProduct}',
            'Modules\Marketplaces\Backend\Stores\StoreFeaturedProductDetailsController@store')
            ->name('users.stores.featured-product-details.store');
        Route::delete('featured-product-details/{featuredProduct}/{product}',
            'Modules\Marketplaces\Backend\Stores\StoreFeaturedProductDetailsController@destroy')
            ->name('users.stores.featured-product-details.destroy');

        Route::resource('menus', 'Modules\Marketplaces\Backend\Stores\StoreMenusController', ['as'=>'users.stores']);
        Route::resource('notes', 'Modules\Marketplaces\Backend\Stores\StoreNotesController', ['as'=>'users.stores']);
        Route::resource('media-libraries', 'Modules\Marketplaces\Backend\Stores\StoreMediaLibrariesController', ['as'=>'users.stores']);

        Route::resource('store-sellers', 'Modules\Marketplaces\Backend\Stores\StoreSellersController', ['as'=>'users.stores']);
        Route::resource('shipping-methods', 'Modules\Marketplaces\Backend\Stores\StoreShippingMethodsController', ['as'=>'users.stores']);

        Route::resource('staffs', 'Modules\Marketplaces\Backend\Stores\StoreStaffController', ['as'=>'users.stores']);

        Route::resource('activities', 'Modules\Marketplaces\Backend\Stores\LogActivityController', ['as'=>'users.stores']);
        Route::resource('notifications', 'Modules\Marketplaces\Backend\Stores\NotificationController', ['as'=>'users.stores']);

        Route::prefix('/ajax')->group(function () {
            Route::get('/categories', 'Modules\Marketplaces\Backend\Stores\MarketplaceCategoryController@index')
                ->name('users.stores.ajax.categories.index');
        });

        Route::group(['prefix' => 'settings'], function() {
            Route::resource('addresses', 'Modules\Marketplaces\Backend\Stores\StoreAddressesController', ['as'=>'users.stores.settings']);
            Route::put('/addresses/set-default/{address}', 'Modules\Marketplaces\Backend\Stores\StoreDefaultAddressesController@update')
                ->name('users.stores.settings.addresses.default-addresses.update');

            Route::resource('payment-methods', 'Modules\Marketplaces\Backend\Stores\StorePaymentMethodsController',
                ['as'=>'users.stores.settings']);
            Route::put('/payment-methods/set-default/{paymentMethod}',
                'Modules\Marketplaces\Backend\Stores\StoreDefaultPaymentMethodsController@update')
                ->name('users.stores.settings.payment-methods.default-addresses.update');

            Route::get('/shipping-methods', 'Modules\Marketplaces\Backend\Stores\StoreShippingMethodsController@rajaongkirEdit')
                ->name('users.stores.settings.shipping-methods.rajaongkir-edit');
            Route::put('/shipping-methods', 'Modules\Marketplaces\Backend\Stores\StoreShippingMethodsController@rajaongkirUpdate')
                ->name('users.stores.settings.shipping-methods.rajaongkir-update');

            Route::get('/store-settings', 'Modules\Marketplaces\Backend\Stores\StoreSettingsController@edit')
                ->name('users.stores.settings.store-settings.edit');
            Route::put('/store-settings', 'Modules\Marketplaces\Backend\Stores\StoreSettingsController@update')
                ->name('users.stores.settings.store-settings.update');

            Route::get('/store-registration', 'Modules\Marketplaces\Backend\Stores\StoreRegistrationController@edit')
                ->name('users.stores.settings.store-registration.edit');
            Route::put('/store-registration', 'Modules\Marketplaces\Backend\Stores\StoreRegistrationController@update')
                ->name('users.stores.settings.store-registration.update');
        });
    });
});

/**
 * Marketplace Admin
 */
Route::middleware(['auth'])->group(function () {
    Route::group(['prefix' => 'dashboard/marketplaces/{marketplace}'], function() {
        Route::get('/', 'Modules\Marketplaces\Backend\Marketplaces\MarketplacesController@index')->name('users.marketplaces.index');

        Route::get('/profile', 'Modules\Marketplaces\Backend\Marketplaces\MarketplacesController@edit')
            ->name('users.marketplaces.edit');
        Route::put('/profile', 'Modules\Marketplaces\Backend\Marketplaces\MarketplacesController@update')
            ->name('users.marketplaces.update');

        Route::resource('categories', 'Modules\Marketplaces\Backend\Marketplaces\MarketplaceCategoriesController',
            ['as'=>'users.marketplaces']);
        Route::resource('stores', 'Modules\Marketplaces\Backend\Marketplaces\StoresController', ['as'=>'users.marketplaces']);
        Route::post('stores/{store}/active', 'Modules\Marketplaces\Backend\Marketplaces\StoreActivesController@store')
            ->name('users.marketplaces.stores.active.store');
        Route::delete('stores/{store}/active', 'Modules\Marketplaces\Backend\Marketplaces\StoreActivesController@destroy')
            ->name('users.marketplaces.stores.active.destroy');

        Route::resource('orders', 'Modules\Marketplaces\Backend\Marketplaces\OrderController', ['as'=>'users.marketplaces']);

        Route::resource('products', 'Modules\Marketplaces\Backend\Marketplaces\ProductController', ['as'=>'users.marketplaces']);

        Route::post('order-statuses-payment/{order}', 'Modules\Marketplaces\Backend\Marketplaces\OrderStatusController@storePaymentToStore')
            ->name('users.marketplaces.order.statuses.payment-to-store.store');

        Route::resource('banners', 'Modules\Marketplaces\Backend\Marketplaces\MarketplaceBannersController',
            ['as'=>'users.marketplaces']);
        Route::resource('banner-groups', 'Modules\Marketplaces\Backend\Marketplaces\MarketplaceBannerGroupsController',
            ['as'=>'users.marketplaces']);

        Route::post('banner-groups-details',
            'Modules\Marketplaces\Backend\Marketplaces\MarketplaceBannerGroupDetailsController@store')
            ->name('users.marketplaces.banner-group-details.store');
        Route::delete('banner-groups-details/{bannerGroup}/{banner}',
            'Modules\Marketplaces\Backend\Marketplaces\MarketplaceBannerGroupDetailsController@destroy')
            ->name('users.marketplaces.banner-group-details.destroy');

        Route::resource('popular-searches', 'Modules\Marketplaces\Backend\Marketplaces\MarketplacePromoPopularSearchesController',
            ['as'=>'users.marketplaces']);
        Route::get('popular-searches-imports/from-popular-searches',
            'Modules\Marketplaces\Backend\Marketplaces\MarketplacePromoPopularSearchesController@imports')
            ->name('users.marketplaces.popular-searches.imports');
        Route::get('popular-searches-imports/from-string',
            'Modules\Marketplaces\Backend\Marketplaces\MarketplacePromoPopularSearchesController@import')
            ->name('users.marketplaces.popular-searches.import');

        Route::resource('/store-collections',
            'Modules\Marketplaces\Backend\Marketplaces\MarketplaceStoreCollectionsController',
            ['as'=>'users.marketplaces']);
        Route::post('/store-collections-details',
            'Modules\Marketplaces\Backend\Marketplaces\MarketplaceStoreCollectionDetailsController@store')
            ->name('users.marketplaces.store-collection-details.store');
        Route::delete('/store-collections-details/{storeCollection}/{store}',
            'Modules\Marketplaces\Backend\Marketplaces\MarketplaceStoreCollectionDetailsController@destroy')
            ->name('users.marketplaces.store-collection-details.destroy');

        Route::resource('/product-collections',
            'Modules\Marketplaces\Backend\Marketplaces\MarketplaceProductCollectionsController',
            ['as'=>'users.marketplaces']);

        Route::get('/product-collection-products',
            'Modules\Marketplaces\Backend\Marketplaces\MarketplaceProductCollectionProductsController@index')
            ->name('users.marketplaces.product-collections.products.index');

        Route::post('/product-collection-details',
            'Modules\Marketplaces\Backend\Marketplaces\MarketplaceProductCollectionDetailsController@store')
            ->name('users.marketplaces.product-collection-details.store');
        Route::delete('/product-collection-details/{productCollection}/{product}',
            'Modules\Marketplaces\Backend\Marketplaces\MarketplaceProductCollectionDetailsController@destroy')
            ->name('users.marketplaces.product-collection-details.destroy');

        Route::resource('media-libraries', 'Modules\Marketplaces\Backend\Marketplaces\MarketplaceMediaLibrariesController', ['as'=>'users.marketplaces']);

        Route::resource('staffs', 'Modules\Marketplaces\Backend\Marketplaces\MarketplaceStaffController', ['as'=>'users.marketplaces']);

        Route::resource('supervisor-groups', 'Modules\Marketplaces\Backend\Marketplaces\SupervisorGroupController', ['as'=>'users.marketplaces']);
        Route::resource('supervisor-staffs', 'Modules\Marketplaces\Backend\Marketplaces\SupervisorStaffController', ['as'=>'users.marketplaces']);

        Route::resource('activities', 'Modules\Marketplaces\Backend\Marketplaces\LogActivityController', ['as'=>'users.marketplaces']);
        Route::get('user-activities', 'Modules\Marketplaces\Backend\Marketplaces\LogActivityController@userActivitiesIndex')->name('users.marketplaces.user-activities.index');

        Route::resource('notifications', 'Modules\Marketplaces\Backend\Marketplaces\NotificationController', ['as'=>'users.marketplaces']);

        Route::resource('users', 'Modules\Marketplaces\Backend\Marketplaces\UserController', ['as'=>'users.marketplaces']);

        Route::group(['prefix' => 'settings'], function() {
            Route::get('/marketplace-settings',
                'Modules\Marketplaces\Backend\Marketplaces\MarketplaceSettingsController@edit')
                ->name('users.marketplaces.settings.marketplace-settings.edit');
            Route::put('/marketplace-settings',
                'Modules\Marketplaces\Backend\Marketplaces\MarketplaceSettingsController@update')
                ->name('users.marketplaces.settings.marketplace-settings.update');
        });
    });
});

/**
 * Administrator
 */
Route::middleware(['auth'])->group(function () {
    Route::group(['prefix' => 'administrator'], function() {
        Route::get('/', 'Modules\Administrators\AdministratorsController@index')
            ->name('administrator.index');

        /**
         * Marketplaces
         */
        Route::get('/marketplaces', 'Modules\Marketplaces\Backend\Administrators\MarketplacesController@index')
            ->name('administrator.marketplaces.index');

        Route::post('marketplaces/{marketplace}/active',
            'Modules\Marketplaces\Backend\Administrators\MarketplaceActivesController@store')
            ->name('administrator.marketplaces.active.store');
        Route::delete('marketplaces/{marketplace}/active',
            'Modules\Marketplaces\Backend\Administrators\MarketplaceActivesController@destroy')
            ->name('administrator.marketplaces.active.destroy');

        /**
         * Marketplace Collection
         */
        Route::resource('/marketplace-collections',
            'Modules\Marketplaces\Backend\Administrators\WebsiteMarketplaceCollectionsController',
            ['as'=>'administrator']);

        Route::post('/marketplace-collections-details',
            'Modules\Marketplaces\Backend\Administrators\WebsiteMarketplaceCollectionDetailsController@store')
            ->name('administrator.marketplace-collection-details.store');
        Route::delete('/marketplace-collections-details/{marketplaceCollection}/{marketplace}',
            'Modules\Marketplaces\Backend\Administrators\WebsiteMarketplaceCollectionDetailsController@destroy')
            ->name('administrator.marketplace-collection-details.destroy');

        /**
         * Product Collections
         */
        Route::resource('/product-collections',
            'Modules\Marketplaces\Backend\Administrators\WebsiteProductCollectionsController',
            ['as'=>'administrator']);

        Route::get('/product-collection-products',
            'Modules\Marketplaces\Backend\Administrators\WebsiteProductCollectionProductsController@index')
            ->name('administrator.product-collections.products.index');

        Route::post('/product-collection-details',
            'Modules\Marketplaces\Backend\Administrators\WebsiteProductCollectionDetailsController@store')
            ->name('administrator.product-collection-details.store');
        Route::delete('/product-collection-details/{productCollection}/{product}',
            'Modules\Marketplaces\Backend\Administrators\WebsiteProductCollectionDetailsController@destroy')
            ->name('administrator.product-collection-details.destroy');

        /**
         * Banners
         */
        Route::resource('/website/banners', 'Modules\Website\Backend\Administrators\WebsiteBannersController',
            ['as'=>'administrator.website']);
        Route::resource('/website/banner-groups', 'Modules\Website\Backend\Administrators\WebsiteBannerGroupsController',
            ['as'=>'administrator.website']);

        Route::post('/website/banner-groups-details',
            'Modules\Website\Backend\Administrators\WebsiteBannerGroupDetailsController@store')
            ->name('administrator.website.banner-group-details.store');
        Route::delete('/website/banner-groups-details/{bannerGroup}/{banner}',
            'Modules\Website\Backend\Administrators\WebsiteBannerGroupDetailsController@destroy')
            ->name('administrator.website.banner-group-details.destroy');

        /**
         * CMS
         */

        Route::resource('/cms/layout-elements', 'Modules\Website\Backend\Administrators\WebsiteLayoutElementsController',
            ['as'=>'administrator.website.cms']);
        Route::resource('/cms/pages', 'Modules\Website\Backend\Administrators\WebsitePagesController',
            ['as'=>'administrator.website.cms']);
        Route::resource('/cms/media-libraries', 'Modules\Website\Backend\Administrators\WebsiteMediaLibrariesController',
            ['as'=>'administrator.website.cms']);
        Route::delete('/cms/forget-caches',
            'Modules\Website\Backend\Administrators\WebsiteForgetCachesController@forgetCaches')
            ->name('administrator.website.cms.forget-caches');

        Route::resource('/cms/blog-posts', 'Modules\Website\Backend\Administrators\WebsiteBlogPostsController',
            ['as'=>'administrator.website.cms']);
        Route::resource('/cms/blog-categories', 'Modules\Website\Backend\Administrators\WebsiteBlogCategoriesController',
            ['as'=>'administrator.website.cms']);
        Route::resource('/cms/blog-banners', 'Modules\Website\Backend\Administrators\WebsiteBlogBannersController',
            ['as'=>'administrator.website.cms']);

        Route::resource('/cms/blog-post-products', 'Modules\Website\Backend\Administrators\WebsiteBlogPostProductsController',
            ['as'=>'administrator.website.cms']);

        /**
         * Settings
         */
        Route::get('/settings',
            'Modules\Website\Backend\Administrators\WebsiteSettingsController@edit')
            ->name('administrator.website.settings.edit');
        Route::put('/settings',
            'Modules\Website\Backend\Administrators\WebsiteSettingsController@update')
            ->name('administrator.website.settings.update');

    });
});

/**
 * Supervisor Group Admin
 */
Route::middleware(['auth'])->group(function () {
    Route::group(['prefix' => 'dashboard/supervisor-groups/{supervisor_group}'], function() {
        Route::get('/', 'Modules\Marketplaces\Backend\SupervisorGroups\HomeController@index')->name('users.supervisor-groups.home');

        Route::resource('orders', 'Modules\Marketplaces\Backend\SupervisorGroups\OrderController', ['as'=>'users.supervisor-groups']);
        Route::resource('stores', 'Modules\Marketplaces\Backend\SupervisorGroups\StoreController', ['as'=>'users.supervisor-groups']);
        Route::resource('categories', 'Modules\Marketplaces\Backend\SupervisorGroups\CategoryController', ['as'=>'users.supervisor-groups']);
        Route::resource('staffs', 'Modules\Marketplaces\Backend\SupervisorGroups\StaffController', ['as'=>'users.supervisor-groups']);
        Route::resource('activities', 'Modules\Marketplaces\Backend\SupervisorGroups\LogActivityController', ['as'=>'users.supervisor-groups']);
        Route::get('user-activities', 'Modules\Marketplaces\Backend\SupervisorGroups\LogActivityController@userActivitiesIndex')->name('users.supervisor-groups.user-activities.index');
    });
});

/**
 * School Admin
 */
Route::middleware(['auth'])->group(function () {
    Route::group(['prefix' => 'dashboard/schools/{school}'], function() {
        Route::get('/', 'Modules\Marketplaces\Backend\Schools\HomeController@index')->name('users.schools.home');

        Route::resource('addresses', 'Modules\Marketplaces\Backend\Schools\SchoolAddressController', ['as'=>'users.schools']);
        Route::put('/addresses/set-default/{address}', 'Modules\Marketplaces\Backend\Schools\SchoolAddressController@setDefaultAddress')
                ->name('users.schools.addresses.default-addresses.update');

        Route::resource('staffs', 'Modules\Marketplaces\Backend\Schools\SchoolStaffController', ['as'=>'users.schools']);
        Route::resource('activities', 'Modules\Marketplaces\Backend\Schools\LogActivityController', ['as'=>'users.schools']);

        Route::get('orders/{order}/download-pdf', 'Modules\Marketplaces\Backend\Schools\OrderController@downloadPdf')
            ->name('users.schools.orders.show.download-pdf');

        Route::post('orders/{order}/add-product-rating', 'Modules\Marketplaces\Backend\Schools\OrderController@productRatingStore')
            ->name('users.schools.orders.product-reviews.store');

        Route::resource('orders', 'Modules\Marketplaces\Backend\Schools\OrderController', ['as'=>'users.schools']);

        Route::post('order-statuses/{order}', 'Modules\Marketplaces\Backend\Schools\OrderStatusController@store')
            ->name('users.schools.order.statuses.store');
        Route::post('order-statuses-delivery-document/{order}', 'Modules\Marketplaces\Backend\Schools\OrderStatusController@storeDeliveryDocument')
            ->name('users.schools.order.statuses.delivery-document.store');
        Route::post('order-statuses-payment/{order}', 'Modules\Marketplaces\Backend\Schools\OrderStatusController@storePayment')
            ->name('users.schools.order.statuses.payment-from-customer.store');

        Route::resource('notifications', 'Modules\Marketplaces\Backend\Schools\NotificationController', ['as'=>'users.schools']);
    });
});

Route::group(['prefix' => 'ajax'], function() {
    Route::get('/', 'Modules\Wilayah\Indonesia\CityController@index')->name('ajax.regions.cities.index');
});

// URL::forceScheme('https');
