<?php

namespace App\Services\Siplah;

use GuzzleHttp\Client;
use App\User;
use App\Models\Modules\Siplah\SiplahUser;
use App\Models\Modules\Marketplaces\School;
use App\Models\Modules\Marketplaces\SchoolStaff;
use Illuminate\Support\Str;
use App\Models\Modules\Marketplaces\SchoolAddress;
use App\Models\Modules\Wilayah\Kemdikbud\Siplah\KemdikbudSiplahRegion;

class SiplahService {

    public $baseUrl;
    public $code;
    public $token;

    public function __construct() {
        $this->baseUrl = config('siplah.siplah.base_url');
        $this->app_id = config('siplah.siplah.app_id');
        $this->client_secret = config('siplah.siplah.client_secret');
        $this->code = null;
        $this->token = null;
    }

    public function getToken($code)
    {
        $url = $this->baseUrl . "/token";
        $postData = [
            'code' => $code,
            'app_id' => $this->app_id,
            'client_secret' => $this->client_secret,
        ];

        $response = $this->postData($url, $postData);
        $token = $response->getBody()->getContents();

        $msg = "ERROR : Authorization code expired";
        if ($token == $msg) {
            abort(403, $msg);
        }

        $this->code = $code;
        $this->token = $token;

        return $token;
    }

    public function getProfile($token = null)
    {
        $token = $token ?? $this->token;
        $url = $this->baseUrl . '/profile';
        $postData = [ 'token' => $token ];

        $response = $this->postData($url, $postData);
        $data = $response->getBody()->getContents();
        return $data;
    }

    public function getInfoSp($token = null)
    {
        $token = $token ?? $this->token;
        $url = $this->baseUrl . '/infosp';
        $postData = [ 'token' => $token ];

        $response = $this->postData($url, $postData);
        $data = $response->getBody()->getContents();
        return $data;
    }

    public function getUser($profile)
    {
        $user = null;

        if ($profile->pengguna_id ?? null) {
            $userUuid = $profile->pengguna_id;
            $siplahUserExists = SiplahUser::where('siplah_user_id', $userUuid)->exists();
            if ($siplahUserExists) {
                $siplahUser = SiplahUser::where('siplah_user_id', $userUuid)->first();
            }

            if (!$siplahUserExists) {
                $siplahUser = SiplahUser::createNewUser($profile);
            }

            return  $siplahUser->user;
        }

        return $user;
    }

    public function getSchool($schoolInfo)
    {
        $school = null;

        if ($schoolInfo->sekolah_id ?? null) {
            $schoolUuid = $schoolInfo->sekolah_id;
            $schoolExists = School::where('unique_code', $schoolUuid)->exists();
            if ($schoolExists) {
                $school = School::where('unique_code', $schoolUuid)->first();
                return $school;
            }

            if (!$schoolExists) {
                $school = $this->createSchool($schoolInfo);
                $this->createSchoolAddress($school, $schoolInfo);
                $school->saveDefaultAddress();
                return $school;
            }
        }
        return $school;
        // return $schoolInfo;
    }

    public function createSchool($schoolInfo)
    {
        $school = new School;
        $school->unique_code = $schoolInfo->sekolah_id;
        $school->user_id = 0;
        $school->name = $schoolInfo->nama_sekolah;
        $school->slug = Str::slug($schoolInfo->nama_sekolah);
        $school->is_active = true;
        $school->status = 'active';
        $school->latitude = $schoolInfo->lintang;
        $school->longitude = $schoolInfo->bujur;
        $school->kemendikbud_zone = $schoolInfo->zona;
        $school->save();

        $school->setMeta('school_info', $schoolInfo);
        return $school;
    }

    public function setSchoolStaff($schoolId, $userId, $role = null)
    {
        $schoolStaff = null;
        $staffExists = SchoolStaff::where([
            'school_id' => $schoolId,
            'user_id' => $userId,
        ])->exists();

        if ($staffExists) {
            $schoolStaff = SchoolStaff::where([
                'school_id' => $schoolId,
                'user_id' => $userId,
            ])->first();

            return $schoolStaff;
        }

        if (!$staffExists) {
            $schoolStaff = SchoolStaff::create([
                'school_id' => $schoolId,
                'user_id' => $userId,
            ]);

            if ($role) {
                $schoolStaff->role = $role;
                $schoolStaff->save();
            }
        }

        return $schoolStaff;
    }

    public function createSchoolAddress($school, $schoolInfo)
    {
        $city_id = trim($schoolInfo->kd_kab);
        $siplahCity = KemdikbudSiplahRegion::find($city_id);
        $city = $siplahCity->indonesiaCity;

        $districtName = $schoolInfo->kec;
        $districtName = str_replace('Kec. ', '', $districtName);
        $district = null;
        if ($city) {
            $district = $city->districts()->where('name', $districtName)->first();
        }

        $address = new SchoolAddress;
        $address->school_id = $school->id;
        $address->label = 'Alamat Sekolah';
        $address->full_name = $school->name;
        $address->address = $schoolInfo->alamat;
        $address->province_id = $city->province_id;
        $address->city_id = $city->id;
        $address->district_id = $district->id ?? 0;
        $address->phone = $schoolInfo->nomor_telepon;
        $address->postcode = $schoolInfo->kode_pos;
        $address->default_address = true;
        $address->save();

    }

    public function postData($url, $postData)
    {
        $client = new Client;

        try {
            $response = $client->request('POST', $url, [
                'form_params' => $postData,
            ]);
            return $response;
        } catch(\GuzzleHttp\Exception\GuzzleException $e) {
            // Handle excpetion
            $message = $e->getMessage();

            // cURL error 7: Failed to connect to www.example.com port 80: Timed out (see http://curl.haxx.se/libcurl/c/libcurl-errors.html)
            if (Str::contains($message, 'cURL error 7: Failed to connect')) {
                abort(503, 'Failed to connect. Please try again');
            }

            dd($e->getMessage());
        }
    }
}
