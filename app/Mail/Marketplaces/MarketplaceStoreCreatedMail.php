<?php

namespace App\Mail\Marketplaces;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MarketplaceStoreCreatedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mailSubject = 'Store created | '.config('app.name');

        return $this
            ->subject($mailSubject)
            ->markdown('mails.marketplaces.marketplace-store-created');
    }
}
