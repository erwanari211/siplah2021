<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class LastOnlineAt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->guest()) {
            return $next($request);
        }

        /** @var \App\User|null $user */
        $user = auth()->user();
        $lastOnline = $user->last_online_at;
        if (is_null($lastOnline)) {
            $user->updateLastOnline();
        }

        if (!is_null($lastOnline)) {
            $updateLastOnlineInterval = 3; // 3 minutes
            if ($lastOnline->diffInMinutes(now()) >= $updateLastOnlineInterval) {
                $user->updateLastOnline();
            }
        }

        return $next($request);
    }
}
