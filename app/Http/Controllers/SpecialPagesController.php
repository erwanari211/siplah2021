<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Marketplace;

class SpecialPagesController extends Controller
{
    public function displayMarketplayList()
    {
        $marketplaces = Marketplace::where('active', true)->get();
        return view('modules.marketplaces.frontend.special-pages.index', compact('marketplaces'));
    }
}
