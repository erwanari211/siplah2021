<?php

namespace App\Http\Controllers\Examples;

use App\Models\Examples\ExamplePost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Examples\PostRequest;

class ExampleTrashedPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = ExamplePost::onlyTrashed()->latest()->paginate(10);
        return view('examples.trashed-posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Examples\ExamplePost  $trashedPost
     * @return \Illuminate\Http\Response
     */
    public function show(ExamplePost $trashedPost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Examples\ExamplePost  $trashedPost
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamplePost $trashedPost)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Examples\ExamplePost  $trashedPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = ExamplePost::withTrashed()->find($id);
        if (!$post->trashed()) {
            $post->delete();
        }

        flash('Data moved to trash');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Examples\ExamplePost  $trashedPost
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = ExamplePost::withTrashed()->find($id);
        if ($post->trashed()) {
            $post->restore();
        }

        flash('Data restored');
        return redirect()->back();
    }
}
