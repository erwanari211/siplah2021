<?php

namespace App\Http\Controllers\Examples;

use App\Models\Examples\ExampleTodo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Examples\TodoRequest;

class ExampleTodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        app()->setLocale('id');
        $status = request('status');

        $todos = new ExampleTodo;
        if ($status == 'completed') {
            $todos = $todos->completed();
        }
        if ($status == 'incomplete') {
            $todos = $todos->incomplete();
        }

        $limit = config('example_todo.item_per_page');
        $limit = $limit ? $limit : 3;
        $todos = $todos->latest()->paginate($limit);

        return view('examples.todos.index', compact('todos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        app()->setLocale('id');
        return view('examples.todos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TodoRequest $request)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $todo = new ExampleTodo;
        $todo->name = request('name');
        $todo->description = request('description');
        $todo->save();

        flash('Data saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Example\ExampleTodo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(ExampleTodo $todo)
    {
        app()->setLocale('id');
        return view('examples.todos.show', compact('todo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Example\ExampleTodo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(ExampleTodo $todo)
    {
        app()->setLocale('id');
        return view('examples.todos.edit', compact('todo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Example\ExampleTodo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(TodoRequest $request, ExampleTodo $todo)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $todo->name = request('name');
        $todo->description = request('description');
        $todo->completed = request('completed');
        $todo->save();

        flash('Data updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Example\ExampleTodo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExampleTodo $todo)
    {
        $todo->delete();

        flash('Data deleted');
        return redirect()->back();
    }
}
