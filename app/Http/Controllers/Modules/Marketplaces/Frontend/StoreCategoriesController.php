<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreCategory;
use App\Models\Modules\Marketplaces\MarketplaceActivitySearch;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;

class StoreCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($marketplace, $store, $category)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $store->addView();
        $storeSettings = $store->mappedSettings();
        $categories = $store->categories()->where('parent_id', 0)->orderBy('sort_order')->get();
        $collection = $store->categories()
            ->where('slug', $category)
            ->with(['childs' => function($q){
                $q->orderBy('sort_order');
            }])
            ->with('parent')
            ->firstOrFail();

        $url = url()->current();
        $queryString = http_build_query(array_except(request()->query(), ['page', 'sort']));
        $sortBy = request('sort') ? request('sort') : 'newest';

        $products = $collection->products()->active()->status('active');
        $products = $products->with('reviews');
        $products = $products->with('kemdikbudZonePrices');

        $search = request('search');
        if ($search) {
            $products = $products->search($search);

            MarketplaceActivitySearch::newSearch($search, [
                'marketplace_id' => $marketplace->id,
                'store_id' => $store->id,
            ]);
        }

        if ($sortBy) {
            if ($sortBy == 'newest') {
                $products = $products->latest();
            }
            if ($sortBy == 'price_asc') {
                $products = $products->orderBy('price', 'asc');
            }
            if ($sortBy == 'price_desc') {
                $products = $products->orderBy('price', 'desc');
            }
        }

        $display = request('display');
        if ($display == 'grid') {
            $products = $products->get();
        } else {
            $products = $products->paginate(20);
        }

        $dropdown['sortBy'] = [
            'newest' => 'Newest',
            'price_asc' => 'Price: Low to High',
            'price_desc' => 'Price: High to Low',
        ];

        $totalProducts = $store->products()->active()->status('active')->count();
        $totalCategories = $store->categories()->count();

        $hasDefaultStoreAddress = $store->hasDefaultStoreAddress();
        $defaultStoreAddress = $store->getDefaultStoreAddress();

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.stores.categories.show', compact(
            'marketplace', 'marketplaceSettings',
            'store', 'categories', 'collection', 'storeSettings',
            'products',
            'url', 'queryString', 'sortBy', 'dropdown',
            'totalProducts', 'totalCategories',
            'hasDefaultStoreAddress', 'defaultStoreAddress',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
