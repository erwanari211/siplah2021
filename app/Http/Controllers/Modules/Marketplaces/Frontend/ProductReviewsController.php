<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\ProductReview;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;

class ProductReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($marketplace, $store, $product)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $store->addView();
        $storeSettings = $store->mappedSettings();
        $categories = $store->categories()->where('parent_id', 0)->orderBy('sort_order')->get();
        $product = $store->products()->where('slug', $product)->firstOrFail();
        $productCategories = $product->marketplaceCategories;
        $attributes = $product->attributes()->orderBy('sort_order', 'asc')->get();
        $groupedAttributes = $attributes->groupBy('group');

        $productDiscussions = $product->discussions()->with('user')->withCount('details')->orderBy('updated_at', 'desc');
        $countProductDiscussions = $productDiscussions->count();

        $productReviews = $product->reviews()->with('user')->orderBy('created_at', 'desc');
        $countProductReviews = $productReviews->count();

        $productRating = $product->reviews()->average('rating');
        $product->rating = intval($productRating) ? intval($productRating) : 0;

        $reviewFilter = 'all';
        $productReviews = $product->reviews()->with('user')->orderBy('updated_at', 'desc');
        if (request('rating')) {
            $rating = request('rating');
            $productReviews = $productReviews->where('rating', $rating);
            $reviewFilter = 'rating_'.$rating;
        }
        $productReviews = $productReviews->paginate(10);

        $mappedKemdikbudZonePrices = [];
        $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
        if ($hasKemdikbudZonePrice) {
            $mappedKemdikbudZonePrices = $product->getMappedKemdikbudZonePrices();
        }

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.stores.products.reviews.index', compact(
            'marketplace', 'marketplaceSettings',
            'categories', 'store',
            'product', 'productCategories', 'groupedAttributes',
            'productReviews', 'reviewFilter',
            'countProductReviews', 'countProductDiscussions',
            'storeSettings',
            'hasKemdikbudZonePrice', 'mappedKemdikbudZonePrices',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $marketplace, $store, $product)
    {
        request()->validate([
            'content' => 'required',
            'rating' => 'required',
        ]);

        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $product = $store->products()->where('slug', $product)->firstOrFail();

        // $review = new ProductReview;
        // $review->product_id = $product->id;
        // $review->user_id = auth()->user()->id;
        // $review->rating = request('rating');
        // $review->content = request('content');
        // $review->save();
        ProductReview::updateOrCreate([
            'product_id' => $product->id,
            'user_id' => auth()->user()->id,
            'order_id' => request('order')
        ], [
            'rating' => request('rating'),
            'content' => request('content'),
        ]);

        $rating = $product->reviews->average('rating');
        $product->rating = $rating;
        $product->save();

        flash('Review submitted');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
