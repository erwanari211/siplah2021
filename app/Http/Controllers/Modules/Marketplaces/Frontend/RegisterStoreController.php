<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteSettings;
use App\User;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreStaff;
use App\Models\Modules\Marketplaces\StorePaymentMethod;
use App\Models\Modules\Marketplaces\StoreShippingMethod;
use Illuminate\Support\Str;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict;
use App\Models\Modules\Marketplaces\StoreAddress;
use App\Models\Modules\Marketplaces\GroupNotification;

class RegisterStoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();
        $dropdown['jenisUsaha'] = [
            'Individu' => 'Individu',
            'PT' => 'PT',
            'CV' => 'CV',
            'Koperasi' => 'Koperasi',
            'Lainnya' => 'Lainnya',
        ];
        $dropdown['kelasUsaha'] = [
            'Mikro' => 'Mikro',
            'Kecil' => 'Kecil',
            'Menengah' => 'Menengah',
            'Koperasi' => 'Koperasi',
            'Non UMKM' => 'Non UMKM',
        ];
        $dropdown['statusUsaha'] = [
            'PKP' => 'PKP',
            'NON-PKP' => 'NON-PKP',
        ];

        $provincesWithDetails = IndonesiaProvince::getProvincesWithDetails();

        $dropdown['provinces'] = IndonesiaProvince::getDropdown();

        $provinceId = old('province_id');
        $dropdown['cities'] = IndonesiaCity::getDropdown($provinceId);

        $cityId = old('city_id');
        $dropdown['districts'] = IndonesiaDistrict::getDropdown($cityId);

        return view('modules.marketplaces.frontend.stores.register', compact('websiteSettings', 'dropdown', 'provincesWithDetails'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return request()->all();

        $data = request()->validate([
            'nama_lengkap' => 'required',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|unique:users,username',
            'password' => 'required|min:8|confirmed',
            'jabatan' => 'required',
            'nik' => 'required',
            'no_hp' => 'required',
            'no_tel_kantor' => 'required',

            'nama_toko' => 'required',
            'jenis_usaha' => 'required',
            'kelas_usaha' => 'required',
            'status' => 'required',
            'npwp' => 'required',
            'siup_nib' => 'nullable',
            'tdp' => 'nullable',

            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',

            'alamat' => 'required',
            // 'geolokasi' => 'nullable',
            // 'kab_kota' => 'required',
            'latitude' => 'required|numeric|min:-90|max:90',
            'longitude' => 'required|numeric|min:-180|max:180',

            'kode_pos' => 'required',
            'email_toko' => 'required|email',

            'nama_lengkap_penanggungjawab' => 'required',
            'email_penanggungjawab' => 'required|email',
            'jabatan_penanggungjawab' => 'required',
            'nik_penanggungjawab' => 'required',

            'nama_bank' => 'required',
            'nama_pemilik_rekening' => 'required',
            'no_rekening' => 'required',
            'cabang_bank' => 'required',

            'setuju_ketentuan_penjual' => 'required|accepted',

            'scan_ktp_penanggung_jawab' => 'required|file|image|max:1024',
            'scan_npwp' => 'required|file|image|max:1024',
            'scan_siup' => 'required|file|image|max:1024',
            'scan_tdp' => 'required|file|image|max:1024',
        ]);

        // check user exists
        // create user
        $user = new User;
        $user->name = $data['nama_lengkap'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->save();
        $user->setMeta('nik', $data['nik']);
        $user->setMeta('no_hp', $data['no_hp']);
        // return $user;

        // create store
        $marketplace = get_default_marketplace();
        $store = new Store;
        $store->marketplace_id = $marketplace->id;
        $store->user_id = $user->id;
        $store->name = $data['nama_toko'];
        $store->slug = $store->getUniqueSlug(Str::slug($data['nama_toko']));
        $store->active = false;
        $store->status = 'inactive';
        $store->unique_code = $store->randomUniqueCode();
        $store->save();
        // return $store;

        $store->latitude = $data['latitude'];
        $store->longitude = $data['longitude'];
        $store->save();

        // add meta
        // upload file
        $store->setMeta('no_tel_kantor', $data['no_tel_kantor']);
        $store->setMeta('jenis_usaha', $data['jenis_usaha']);
        $store->setMeta('kelas_usaha', $data['kelas_usaha']);
        $store->setMeta('status_usaha', $data['status']);
        $store->setMeta('npwp', $data['npwp']);
        $store->setMeta('siup_nib', $data['siup_nib']);
        $store->setMeta('tdp', $data['tdp']);

        $store->setMeta('province_id', $data['province_id']);
        $store->setMeta('city_id', $data['city_id']);
        $store->setMeta('district_id', $data['district_id']);

        $store->setMeta('alamat', $data['alamat']);
        $store->setMeta('latitude', $data['latitude']);
        $store->setMeta('longitude', $data['longitude']);

        // $store->setMeta('kab_kota', $data['kab_kota']);
        $store->setMeta('kode_pos', $data['kode_pos']);
        $store->setMeta('email_toko', $data['email_toko']);

        $store->setMeta('nama_lengkap_penanggungjawab', $data['nama_lengkap_penanggungjawab']);
        $store->setMeta('email_penanggungjawab', $data['email_penanggungjawab']);
        $store->setMeta('jabatan_penanggungjawab', $data['jabatan_penanggungjawab']);
        $store->setMeta('nik_penanggungjawab', $data['nik_penanggungjawab']);

        $store->setMeta('nama_bank', $data['nama_bank']);
        $store->setMeta('nama_pemilik_rekening', $data['nama_pemilik_rekening']);
        $store->setMeta('no_rekening', $data['no_rekening']);
        $store->setMeta('cabang_bank', $data['cabang_bank']);

        $files = ['scan_ktp_penanggung_jawab', 'scan_npwp', 'scan_siup', 'scan_tdp'];
        foreach ($files as $document) {
            $image = $data[$document];
            if ($image) {
                $filename = 'store-documents';
                $filepath = upload_file($image, 'uploads', $filename);
                $store->setMeta($document, $filepath);
            }
        }
        // return $store->getAllMeta();

        // add staff
        $staff = new StoreStaff;
        $staff->store_id = $store->id;
        $staff->user_id = $user->id;
        $staff->role = 'admin';
        $staff->is_active = true;
        $staff->save();

        $staff->setMeta('jabatan', $data['jabatan']);
        // return $staff;

        // set address
        $address = new StoreAddress;
        $address->label = 'Alamat Utama';
        $address->full_name = 'Alamat Utama';
        $address->company = $store->name;
        $address->address = request('alamat');
        $address->province_id = request('province_id');
        $address->city_id = request('city_id');
        $address->district_id = request('district_id');
        $address->phone = '-';
        $address->postcode = '-';

        $store->addresses()->save($address);

        $countStoreAddresses = StoreAddress::where('store_id', $store->id)->count();
        if ($countStoreAddresses == 1) {
            $address->default_address = true;
            $address->save();

            $store->saveDefaultAddress();
        }

        $namaBank = request('nama_bank');
        $namaPemilikRekening = request('nama_pemilik_rekening');
        $noRekening = request('no_rekening');
        $cabangBank = request('cabang_bank');

        $paymentMethod = new StorePaymentMethod;
        $paymentMethod->store_id = $store->id;
        $paymentMethod->label = request('nama_bank');
        $paymentMethod->name = request('nama_bank');
        $paymentMethod->info = "
            Please Transfer to following bank account : <br>
            {$namaBank} <br>
            {$noRekening} <br>
            a/n {$namaPemilikRekening} <br>
            {$cabangBank} <br>
        ";
        $paymentMethod->default_payment_method = true;
        $paymentMethod->save();

        $customCouriers = get_siplah_custom_couriers();
        foreach ($customCouriers as $courier => $courierName) {
            $method = new StoreShippingMethod;
            $method->group = 'custom';
            $method->label = $courier;
            $method->name = $courierName;
            $method->active = true;
            $method->store_id = $store->id;
            $method->save();
        }

        $marketplace = $store->marketplace;
        $notificationData = [];
        $notificationData['data']['store'] = $store->getOriginal();
        $notificationData['description'] = 'Toko telah melakukan pendaftaran';
        GroupNotification::addNotification($marketplace, 'store_registration', $notificationData);

        \Auth::loginUsingId($user->id, true);
        $store->staffLoggedIn();

        flash('Pendaftaran sukses. Menunggu persetujuan dari admin');
        return redirect()->route('users.stores.index', $store->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
