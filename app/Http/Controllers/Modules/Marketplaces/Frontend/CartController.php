<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\ShoppingCart;
use App\Models\Modules\Marketplaces\Cart as CartModel;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Marketplaces\School;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ShoppingCart::getCarts();
        // return $data;
        $carts = $data['carts'];
        $stores = $data['stores'];
        $count = $data['count'];
        $subtotal = $data['subtotal'];

        $useHomepageFooter = true;

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.cart.index', compact(
            'carts', 'subtotal', 'count', 'stores',
            'useHomepageFooter',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        if (session('login_as') != 'school_staff') {
            flash('You dont have permission to add product to cart')->error();
            return redirect()->back();
        }

        $schoolId = session('school_id') ;
        $school = School::find($schoolId);

        $products = request('products');
        $isBatchInsert = $products ? true : false;
        if ($isBatchInsert) {
            $addedToCartTotal = 0;

            foreach ($products as $productInput) {
                $product_id = $productInput['product_id'];
                $product = Product::with('store')->findOrFail($product_id);
                $store = $product->store;
                $qty = $productInput['qty'];

                $meta = [];
                $meta['store'] = $store;
                $meta['product'] = $product;

                $price = $product->price;

                // KemdikbudZonePrice
                $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
                if ($hasKemdikbudZonePrice) {
                    $mappedKemdikbudZonePrices = $product->getMappedKemdikbudZonePrices();
                    $kemdikbud_zone = session('kemdikbud_zone');
                    if ($kemdikbud_zone) {
                        foreach ($mappedKemdikbudZonePrices as $zone => $zonePrice) {
                            if ($kemdikbud_zone == $zone) {
                                $price = $zonePrice;
                            }
                        }
                    }
                }

                if ($qty > 0) {
                    if ($product->qty > $qty) {
                        $item = Cart::instance($store->slug)->add($product->id, $product->name, $qty, $price);
                        $item->associate('App\Models\Modules\Marketplaces\Product');
                        if ($product->qty < $item->qty) {
                            $qty = $product->qty;
                            Cart::update($item->rowId, $qty);
                        }
                        $addedToCartTotal++;
                        $data[] = $item;
                    } else {
                        if ($product->qty > 0) {
                            $qty = $product->qty;
                            $item = Cart::instance($store->slug)->add($product->id, $product->name, $qty, $price);
                            $item->associate('App\Models\Modules\Marketplaces\Product');
                            $addedToCartTotal++;
                            $data[] = $item;
                        }
                    }
                }
            }

            flash($addedToCartTotal.' products added to cart');
        } else {
            $product_id = request('product_id');
            $product = Product::with('store')->findOrFail($product_id);
            $store = $product->store;
            $qty = request('qty');

            $meta = [];
            $meta['store'] = $store;
            $meta['product'] = $product;
            unset($meta['product']['views']);
            unset($meta['product']['created_at']);
            unset($meta['product']['updated_at']);

            $price = $product->price;

            // KemdikbudZonePrice
            $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
            if ($hasKemdikbudZonePrice) {
                $mappedKemdikbudZonePrices = $product->getMappedKemdikbudZonePrices();
                $kemdikbud_zone = session('kemdikbud_zone');
                if ($kemdikbud_zone) {
                    foreach ($mappedKemdikbudZonePrices as $zone => $zonePrice) {
                        if ($kemdikbud_zone == $zone) {
                            $price = $zonePrice;
                        }
                    }
                }
            }

            if ($product->qty > $qty) {
                $item = Cart::instance($store->slug)->add($product->id, $product->name, $qty, $price);
                // $item = Cart::instance($store->slug)->add($product->id, $product->name, $qty, $product->price, ['meta' => $meta]);
                $item->associate('App\Models\Modules\Marketplaces\Product');
                if ($product->qty < $item->qty) {
                    $qty = $product->qty;
                    Cart::update($item->rowId, $qty);
                    flash($product->name.' from '.$store->name.' available stock only '.$qty)->warning();
                }

                if ($school) {
                    CartModel::store($school, $store->slug);
                }

                flash('Product added to cart');
            } else {
                if ($product->qty == 0) {
                    flash('Product out of stock')->error();
                }

                if ($product->qty > 0) {
                    $qty = $product->qty;
                    $item = Cart::instance($store->slug)->add($product->id, $product->name, $qty, $price);
                    // $item = Cart::instance($store->slug)->add($product->id, $product->name, $qty, $product->price, ['meta' => $meta]);
                    $item->associate('App\Models\Modules\Marketplaces\Product');
                    if ($product->qty < $item->qty) {
                        $qty = $product->qty;
                        Cart::update($item->rowId, $qty);
                        flash($product->name.' from '.$store->name.' available stock only '.$qty)->warning();
                    }

                    if ($school) {
                        CartModel::store($school, $store->slug);
                    }

                    flash('Product added to cart');
                }
            }
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $store)
    {
        $user = auth()->user();
        $storeSlug = $store;

        if (session('login_as') != 'school_staff') {
            flash('You dont have permission to update cart')->error();
            return redirect()->back();
        }

        $schoolId = session('school_id') ;
        $school = School::find($schoolId);

        Cart::instance($storeSlug);
        $cartContent = Cart::content();
        $data = request('data');
        if ($data) {
            foreach ($data as $input) {
                $rowId = $input['rowId'];
                $product = $cartContent[$rowId]->model;
                // $store = $cartContent[$rowId]->options->meta['store'];
                $store = $product->store;

                if ($product->qty >= $input['qty']) {
                    $qty = $input['qty'];
                } else {
                    $qty = $product->qty;
                    flash($product->name.' from '.$store->name.' available stock only '.$qty)->warning();
                }
                Cart::update($input['rowId'], $qty);
            }
        }

        if (Cart::instance($storeSlug)->count() == 0) {
            Cart::instance($storeSlug)->destroy();
        }

        if ($school) {
            CartModel::store($school, $storeSlug);
        }

        flash('Cart updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($store)
    {
        $user = auth()->user();
        $storeSlug = $store;

        if (session('login_as') != 'school_staff') {
            flash('You dont have permission to update cart')->error();
            return redirect()->back();
        }

        $schoolId = session('school_id') ;
        $school = School::find($schoolId);

        Cart::instance($storeSlug);
        Cart::destroy();
        ShoppingCart::destroyCartMeta($storeSlug);

        if ($school) {
            CartModel::remove($school, $storeSlug);
        }

        flash('Cart deleted');
        return redirect()->back();
    }
}
