<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteSettings;
use App\Services\Siplah\SiplahService;
use Auth;

class LoginSchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.frontend.schools.login', compact('websiteSettings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = request('code');

        $siplah = new SiplahService;

        $token = $siplah->getToken($code);

        $profile = $siplah->getProfile();
        $profile = json_decode($profile);
        if ($profile->jabatan) {
            $allowedRoles = config('siplah.siplah.satdik_jabatan_yang_diizinkan');
            if (!in_array($profile->jabatan, $allowedRoles)) {
                abort(403);
            }
        }
        $user = $siplah->getUser($profile);

        $schoolInfo = $siplah->getInfoSp();
        $schoolInfo = json_decode($schoolInfo);
        $school = $siplah->getSchool($schoolInfo);

        $staffRole = $profile->jabatan ?? null;
        $schoolStaff = $siplah->setSchoolStaff($school->id, $user->id, $staffRole);

        Auth::login($user);
        $school->staffLoggedIn();

        return redirect()->route('users.schools.home', $school->id);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
