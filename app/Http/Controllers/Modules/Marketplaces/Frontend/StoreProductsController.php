<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\ProductActivityView;
use DB;
use App\Models\Modules\Marketplaces\MarketplaceActivitySearch;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity;

class StoreProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($marketplace, $store)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $store->addView();
        $storeSettings = $store->mappedSettings();
        $categories = $store->categories()->where('parent_id', 0)->orderBy('sort_order')->get();

        $url = url()->current();
        $queryString = http_build_query(array_except(request()->query(), ['page', 'sort']));
        $sortBy = request('sort') ? request('sort') : 'newest';

        $products = $store->products()->active()->status('active');
        $products = $products->withCount('reviews');
        $products = $products->with('kemdikbudZonePrices');

        $search = request('search');
        if ($search) {
            $products = $products->search($search);

            MarketplaceActivitySearch::newSearch($search, [
                'marketplace_id' => $marketplace->id,
                'store_id' => $store->id,
            ]);
        }

        if ($sortBy) {
            if ($sortBy == 'newest') {
                $products = $products->latest();
            }
            if ($sortBy == 'price_asc') {
                $products = $products->orderBy('price', 'asc');
            }
            if ($sortBy == 'price_desc') {
                $products = $products->orderBy('price', 'desc');
            }
        }

        $display = request('display');
        if ($display == 'grid') {
            $products = $products->get();
        } else {
            $products = $products->paginate(20);
        }

        $dropdown['sortBy'] = [
            'newest' => 'Newest',
            'price_asc' => 'Price: Low to High',
            'price_desc' => 'Price: High to Low',
        ];

        $totalProducts = $store->products()->active()->status('active')->count();
        $totalCategories = $store->categories()->count();

        $hasDefaultStoreAddress = $store->hasDefaultStoreAddress();
        $defaultStoreAddress = $store->getDefaultStoreAddress();

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.stores.products.index', compact(
            'marketplace', 'marketplaceSettings',
            'categories', 'store', 'storeSettings',
            'products',
            'url', 'queryString', 'sortBy', 'dropdown',
            'totalProducts', 'totalCategories',
            'hasDefaultStoreAddress', 'defaultStoreAddress',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($marketplace, $store, $product)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $store->addView();
        $categories = $store->categories()->where('parent_id', 0)->orderBy('sort_order')->get();
        $product = $store->products()->where('slug', $product)->firstOrFail();
        $productCategories = $product->marketplaceCategories;
        $attributes = $product->attributes()->orderBy('sort_order', 'asc')->get();
        $groupedAttributes = $attributes->groupBy('group');

        $productGalleries = $product->galleries()
            ->orderBy('sort_order')
            ->where(['group'=>'gallery', 'active'=> 1])
            ->get();

        $productDiscussions = $product->discussions()->with('user')->withCount('details')->orderBy('updated_at', 'desc');
        $countProductDiscussions = $productDiscussions->count();
        $productDiscussions = $productDiscussions->take(3)->get();
        $discussionFilter = 'all';
        $reviewFilter = 'all';

        $productReviews = $product->reviews()->with('user')->orderBy('created_at', 'desc');
        $countProductReviews = $productReviews->count();
        $productReviews = $productReviews->take(3)->get();

        // $productRating = $product->reviews()->average('rating');
        // $product->rating = intval($productRating) ? intval($productRating) : 0;

        $orderProducts = $product->orderProducts()->with(['order'=>function($q){
            $q->where('order_status', 'completed');
        }])->sum('qty');
        $product->sent = $orderProducts;

        $storeSettings = $store->settings;
        $storeSettings = $storeSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });

        $product->viewedByUser($product, $marketplace, $store);

        $mappedKemdikbudZonePrices = [];
        $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
        if ($hasKemdikbudZonePrice) {
            $mappedKemdikbudZonePrices = $product->getMappedKemdikbudZonePrices();
        }

        $totalProducts = $store->products()->active()->status('active')->count();
        $totalCategories = $store->categories()->count();

        $hasDefaultStoreAddress = $store->hasDefaultStoreAddress();
        $defaultStoreAddress = $store->getDefaultStoreAddress();

        $cities = KemdikbudCity::with('province')->get();
        $citiesGroupedByZone = [];
        foreach ($cities as $city) {
            $cityZone = $city['zone'];
            $cityName = $city['name'];
            $cityProvince = $city['province']['name'];
            $citiesGroupedByZone[$cityZone][$cityProvince][] = $cityName;
        }
        $citiesWithZone = [];
        foreach ($cities as $city) {
            $cityId = $city['id'];
            $cityZone = $city['zone'];
            $cityName = $city['name'];
            $citiesWithZone[$cityId] = compact('cityName', 'cityZone');
        }

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.stores.products.show', compact(
            'marketplace', 'marketplaceSettings',
            'categories', 'store', 'storeSettings',
            'product', 'productCategories', 'groupedAttributes', 'productGalleries',
            'productDiscussions', 'countProductDiscussions', 'discussionFilter',
            'productReviews', 'countProductReviews', 'reviewFilter',
            'totalProducts', 'totalCategories',
            'hasDefaultStoreAddress', 'defaultStoreAddress',
            'hasKemdikbudZonePrice', 'mappedKemdikbudZonePrices',
            'citiesGroupedByZone', 'citiesWithZone',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
