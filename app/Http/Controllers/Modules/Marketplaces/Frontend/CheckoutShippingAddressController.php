<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\ShoppingCart;
use Cart;
use App\Models\UserAddress;
use App\Models\ZoneOngkirProvince;
use App\Models\ZoneOngkirCity;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict;
use App\Models\Modules\Marketplaces\School;
use App\Models\Modules\Marketplaces\SchoolAddress;

class CheckoutShippingAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($storeSlug)
    {
        if (!ShoppingCart::checkCartStore($storeSlug)) {
            flash('Selected cart is empty')->error();
            return redirect()->route('cart.index');
        }

        if (!ShoppingCart::checkCheckoutSteps($storeSlug, [1, 2])) {
            flash('Please complete previous step')->error();
            return redirect()->route('checkout.steps.billing-address.edit', [$storeSlug]);
        }

        $staffRole = session('school_staff.role');
        $allowedRoles = config('siplah.siplah.satdik_jabatan_yang_bisa_checkout');
        $allowedToCheckout = in_array($staffRole, $allowedRoles);
        if (!$allowedToCheckout) {
            flash('You are not allowed to checkout')->error();
            return redirect()->route('cart.index');
        }
        $schoolId = session('school_id');
        $school = School::find($schoolId);

        $data = ShoppingCart::getCarts();
        $store = $data['stores'][$storeSlug];
        $marketplace = $store->marketplace;
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $categories = $marketplace->categories()->where('parent_id', 0)->get();
        Cart::instance($storeSlug);

        $schoolAddresses = SchoolAddress::getSchoolAddressesDropdown($school);
        $dropdown['schoolAddresses'] = $schoolAddresses;
        $schoolHasAddress = count($schoolAddresses) ? true : false;
        $selectedAddress = ShoppingCart::getShippingAddress($storeSlug);
        $selectedAddressId = ($selectedAddress && isset($selectedAddress->id)) ? $selectedAddress->id : null;

        /*
        $provinces = ZoneOngkirProvince::getProvinceWithCities();
        $provinceId = old('province_id');
        $dropdown['provinces'] = ZoneOngkirProvince::pluck('province', 'province_id');
        $dropdown['cities'] =  ZoneOngkirCity::getDropdown($provinceId);
        */

        // New
        $provinces = [];
        $provincesWithDetails = IndonesiaProvince::getProvincesWithDetails();

        $dropdown['provinces'] = IndonesiaProvince::getDropdown();

        $provinceId = old('province_id');
        $dropdown['cities'] = IndonesiaCity::getDropdown($provinceId);

        $cityId = old('city_id');
        $dropdown['districts'] = IndonesiaDistrict::getDropdown($cityId);

        $useHomepageFooter = true;

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.checkout.shipping-address', compact(
            'store', 'marketplace', 'marketplaceSettings', 'categories',
            'school', 'schoolHasAddress', 'selectedAddressId',
            'dropdown', 'provinces', 'provincesWithDetails',
            'useHomepageFooter',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $storeSlug)
    {
        request()->validate([
            'shipping_address' => 'required',
            'address_id' => 'required_if:shipping_address,existing',

            'label' => 'required_if:shipping_address,new',
            'full_name' => 'required_if:shipping_address,new',
            'address' => 'required_if:shipping_address,new',
            'phone' => 'required_if:shipping_address,new',
            'postcode' => 'required_if:shipping_address,new',
            'province_id' => 'required_if:shipping_address,new',
            'city_id' => 'required_if:shipping_address,new',
            'district_id' => 'required_if:payment_address,new',
        ]);

        if (!ShoppingCart::checkCartStore($storeSlug)) {
            flash('Selected cart is empty')->error();
            return redirect()->route('cart.index');
        }

        $staffRole = session('school_staff.role');
        $allowedRoles = config('siplah.siplah.satdik_jabatan_yang_bisa_checkout');
        $allowedToCheckout = in_array($staffRole, $allowedRoles);
        if (!$allowedToCheckout) {
            flash('You are not allowed to checkout')->error();
            return redirect()->route('cart.index');
        }
        $schoolId = session('school_id');
        $school = School::find($schoolId);

        $paymentAddress = request('shipping_address');
        if ($paymentAddress == 'existing') {
            $addressId = request('address_id');
            $address = $school->addresses()->where('id', $addressId)->first();
            if (!$address) {
                flash('Please select user address')->error();
                return redirect()->back();
            }
        }

        if ($paymentAddress == 'new') {
            $address = new SchoolAddress;
            $address->label = request('label');
            $address->full_name = request('full_name');
            $address->company = request('company');
            $address->address = request('address');
            $address->province_id = request('province_id');
            $address->city_id = request('city_id');
            $address->district_id = request('district_id');
            $address->phone = request('phone');
            $address->postcode = request('postcode');
            $address->default_address = false;

            $school->addresses()->save($address);
        }

        ShoppingCart::setshippingAddress($storeSlug, $address);
        $step = [
            'step_3' => ['name' => 'shipping_address', 'completed' => true, ],
        ];
        ShoppingCart::setCheckoutSteps($storeSlug, $step);

        return redirect()->route('checkout.steps.shipping-method.edit', [$storeSlug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
