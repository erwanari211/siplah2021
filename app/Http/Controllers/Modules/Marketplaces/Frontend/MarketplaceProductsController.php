<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceCategory;
use App\Models\Modules\Marketplaces\MarketplaceActivitySearch;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity;

class MarketplaceProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($marketplace)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });

        $searchType = request('search_type');
        $search = request('search');
        if ($searchType == 'store') {
            return redirect()->route('stores.index', [$marketplace->slug, 'search' => $search]);
        }

        $popularSearches = $marketplace->promoPopularSearches()->orderBy('sort_order')->take(5)->get();
        $categories = $marketplace->categories()->where('parent_id', null)->orderBy('sort_order')->get();

        $url = url()->current();
        $queryString = http_build_query(array_except(request()->query(), ['page', 'sort']));
        $sortBy = request('sort') ? request('sort') : 'newest';

        $products = $marketplace->products()
            ->with('store')
            ->with('store.addresses')
            ->with('store.addresses', 'store.addresses.province', 'store.addresses.city')
            ->where(['products.active'=>1, 'products.status'=>'active'])
            ->approved();
        $products = $products->withCount('reviews');
        $products = $products->with('kemdikbudZonePrices');

        $search = request('search');
        if ($search) {
            $products = $products->where([
                ['products.name', 'like', "%$search%"]
            ]);

            MarketplaceActivitySearch::newSearch($search, [
                'marketplace_id' => $marketplace->id,
                'store_id' => null,
            ]);
        }
        if ($sortBy) {
            if ($sortBy == 'newest') {
                $products = $products->latest();
            }
            if ($sortBy == 'price_asc') {
                $products = $products->orderBy('price', 'asc');
            }
            if ($sortBy == 'price_desc') {
                $products = $products->orderBy('price', 'desc');
            }

            if ($sortBy == 'nearest') {
                $userLat = null;
                $userLong = null;

                if (session('login_as') == 'school_staff') {
                    $userLat = session('school_coordinates.lat');
                    $userLong = session('school_coordinates.long');
                }

                if (is_numeric($userLat) && is_numeric($userLong)) {
                    $products = $products->sortByNearest($userLat, $userLong);
                } else {
                    $products = $products->latest();
                }
            }
        }

        if (request('min_price')) {
            $products = $products->where('price', '>=', request('min_price'));
        }
        if (request('max_price')) {
            $products = $products->where('price', '<=', request('max_price'));
        }

        if (request('rating')) {
            $products = $products->where('rating', '>=', request('rating'));
        }

        if ($isLocalProduct = request('is_local_product')) {
            if ($isLocalProduct == 'yes') {
                $products = $products->where('is_local_product', 1);
            }
            if ($isLocalProduct == 'no') {
                $products = $products->where('is_local_product', 0);
            }
        }
        if ($isMsmeProduct = request('is_msme_product')) {
            if ($isMsmeProduct == 'yes') {
                $products = $products->where('is_msme_product', 1);
            }
            if ($isMsmeProduct == 'no') {
                $products = $products->where('is_msme_product', 0);
            }
        }

        if ($isMsme = request('is_msme')) {
            if ($isMsme == 'yes') {
                $products = $products->whereHas('store', function($q){
                    $umkm = ['Mikro', 'Kecil', 'Menengah', 'Koperasi'];
                    return $q->whereMetaIn('kelas_usaha', $umkm);
                });
            }
            if ($isMsme == 'no') {
                $products = $products->whereHas('store', function($q){
                    $nonUmkm = ['Non UMKM'];
                    return $q->whereMetaIn('kelas_usaha', $nonUmkm);
                });
            }
        }

        if ($storeHasRating = request('store_has_rating')) {
            if ($storeHasRating == 'yes') {
                $products = $products->whereHas('store', function($q){
                    return $q->whereHas('productReviews');
                });
            }
            if ($storeHasRating == 'no') {
                $products = $products->whereHas('store', function($q){
                    return $q->whereDoesntHave('productReviews');
                });
            }
        }

        if ($distance = request('distance')) {
            $userLat = null;
            $userLong = null;

            $schoolCoordinates = session('school_coordinates');
            if ($schoolCoordinates) {
                $userLat = $schoolCoordinates['lat'];
                $userLong = $schoolCoordinates['long'];
            }

            if (is_numeric($userLat) && is_numeric($userLong) && is_numeric($distance)) {
                $distanceInMeter = $distance * 1000;
                $products = $products->filterByDistance($userLat, $userLong, $distanceInMeter);
            }
        }

        $selectedCity = null;
        if ($cityId = request('city_id')) {
            $selectedCity = IndonesiaCity::find($cityId);
            $products = $products->whereHas('store', function($q) use ($cityId) {
                return $q->where('city_id', $cityId);
            });
        }

        $products = $products->paginate(20);

        $dropdown['sortBy'] = [
            'newest' => 'Newest',
            'price_asc' => 'Price: Low to High',
            'price_desc' => 'Price: High to Low',
        ];

        if (session('login_as') == 'school_staff') {
            $dropdown['sortBy']['nearest'] = 'Nearest Location';
        }

        $dropdown['yes_no'] = [
            '' => 'Please Select',
            'yes' => 'Yes',
            'no' => 'No',
        ];

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.marketplaces.products.index', compact(
            'marketplace', 'marketplaceSettings',
            'popularSearches',
            'categories', 'category', 'products', 'subcategories',
            'url', 'queryString', 'sortBy', 'dropdown',
            'selectedCity',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
