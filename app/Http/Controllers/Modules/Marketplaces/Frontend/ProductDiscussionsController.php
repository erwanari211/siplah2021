<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\ProductDiscussion;
use App\Models\Modules\Marketplaces\ProductDiscussionDetail;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;

class ProductDiscussionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($marketplace, $store, $product)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $store->addView();
        $storeSettings = $store->mappedSettings();
        $categories = $store->categories()->where('parent_id', 0)->orderBy('sort_order')->get();
        $product = $store->products()->where('slug', $product)->firstOrFail();
        $productCategories = $product->marketplaceCategories;
        $attributes = $product->attributes()->orderBy('sort_order', 'asc')->get();
        $groupedAttributes = $attributes->groupBy('group');

        $productDiscussions = $product->discussions()->with('user')->withCount('details')->orderBy('updated_at', 'desc');
        $countProductDiscussions = $productDiscussions->count();

        $productReviews = $product->reviews()->with('user')->orderBy('created_at', 'desc');
        $countProductReviews = $productReviews->count();

        $productRating = $product->reviews()->average('rating');
        $product->rating = intval($productRating) ? intval($productRating) : 0;

        $discussionFilter = 'all';
        $productDiscussions = $product->discussions()->with('user')->withCount('details')->orderBy('updated_at', 'desc');
        if (auth()->check() && request('author') == 'me') {
            $productDiscussions = $productDiscussions->where('user_id', auth()->user()->id);
            $discussionFilter = 'me';
        }
        $productDiscussions = $productDiscussions->paginate(10);

        $mappedKemdikbudZonePrices = [];
        $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
        if ($hasKemdikbudZonePrice) {
            $mappedKemdikbudZonePrices = $product->getMappedKemdikbudZonePrices();
        }

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.stores.products.discussions.index', compact(
            'marketplace', 'marketplaceSettings',
            'categories', 'store',
            'product', 'productCategories', 'groupedAttributes',
            'productDiscussions', 'discussionFilter',
            'countProductDiscussions', 'countProductReviews',
            'storeSettings',
            'hasKemdikbudZonePrice', 'mappedKemdikbudZonePrices',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $marketplace, $store, $product)
    {
        request()->validate([
            'content' => 'required',
        ]);

        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $product = $store->products()->where('slug', $product)->firstOrFail();

        $discussion = new ProductDiscussion;
        $discussion->product_id = $product->id;
        $discussion->user_id = auth()->user()->id;
        $discussion->content = request('content');
        $discussion->save();

        flash('Discussion submitted');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($marketplace, $store, $product, $discussionId)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $store->addView();
        $storeSettings = $store->mappedSettings();
        $categories = $store->categories()->where('parent_id', 0)->orderBy('sort_order')->get();
        $product = $store->products()->where('slug', $product)->firstOrFail();
        $productCategories = $product->marketplaceCategories;
        $attributes = $product->attributes()->orderBy('sort_order', 'asc')->get();
        $groupedAttributes = $attributes->groupBy('group');

        $productDiscussions = $product->discussions()->with('user')->withCount('details')->orderBy('updated_at', 'desc');
        $countProductDiscussions = $productDiscussions->count();

        $productReviews = $product->reviews()->with('user')->orderBy('created_at', 'desc');
        $countProductReviews = $productReviews->count();

        $productRating = $product->reviews()->average('rating');
        $product->rating = intval($productRating) ? intval($productRating) : 0;

        $productDiscussion = $product->discussions()->with('user', 'details')->withCount('details')->findOrFail($discussionId);
        $productDiscussionDetails = $productDiscussion->details()->with('user')->get();

        $mappedKemdikbudZonePrices = [];
        $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
        if ($hasKemdikbudZonePrice) {
            $mappedKemdikbudZonePrices = $product->getMappedKemdikbudZonePrices();
        }

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.stores.products.discussions.show', compact(
            'marketplace', 'marketplaceSettings',
            'categories', 'store',
            'product', 'productCategories', 'groupedAttributes',
            'productDiscussion', 'productDiscussionDetails',
            'countProductDiscussions', 'countProductReviews',
            'storeSettings',
            'hasKemdikbudZonePrice', 'mappedKemdikbudZonePrices',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
