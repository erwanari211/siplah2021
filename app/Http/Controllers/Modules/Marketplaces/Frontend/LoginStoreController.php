<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteSettings;
use App\User;
use App\Models\Modules\Marketplaces\StoreStaff;

class LoginStoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.frontend.stores.login', compact('websiteSettings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the info, create rules for the inputs
        $rules = [
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        ];

        // run the validation rules on the inputs from the form
        $validator = \Validator::make(request()->all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Redirect::back()
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(request()->except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {
            $errorMessage = 'These credentials do not match our records';

            // check user email exists
            $email = request('email');
            $user = User::where('email', $email)->first();
            if (!$user) {
                return \Redirect::back()
                    ->withErrors([ 'email' => $errorMessage, ])
                    ->withInput(request()->except('password'));
            }

            // check user is store staff
            if ($user) {
                $isStoreStaff = StoreStaff::where('user_id', $user->id)->exists();
            }
            if (!$isStoreStaff) {
                return \Redirect::back()
                    ->withErrors([ 'email' => $errorMessage, ])
                    ->withInput(request()->except('password'));
            }

            // create our user data for the authentication
            $userdata = [
                'email'     => request('email'),
                'password'  => request('password')
            ];

            // attempt to do the login
            if (! \Auth::attempt($userdata)) {
                // validation not successful, send back to form
                return \Redirect::back()
                    ->withErrors([ 'email' => $errorMessage, ])
                    ->withInput(request()->except('password'));
            } else {
                // validation successful!
                // redirect them
                $staff = StoreStaff::with('store')->where('user_id', $user->id)->first();
                $store = $staff->store;
                $store->staffLoggedIn();
                return redirect()->route('users.stores.index', $staff->store_id);
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
