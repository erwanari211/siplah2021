<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\Store;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;

class StoreProductDiscussionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($marketplace, $store)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $store->addView();
        $storeSettings = $store->mappedSettings();

        $categories = $store->categories()->where('parent_id', 0)->orderBy('sort_order')->get();

        $storeMenus = $store->menus()
            ->where('group', 'menu header')
            ->where('parent_id', 0)
            ->with(['children' => function($q){
                $q->orderBy('sort_order');
            }])
            ->orderBy('sort_order')
            ->get();

        $storeDiscussions = $store->productDiscussions()
            ->withCount('details')
            ->with('product', 'product', 'user')
            ->latest()->paginate(10);

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.stores.discussions.index', compact(
            'marketplace', 'marketplaceSettings',
            'store', 'categories', 'collection', 'storeSettings',
            'storeMenus',
            'storeDiscussions',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
