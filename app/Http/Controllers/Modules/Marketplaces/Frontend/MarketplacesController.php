<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection;
use App\Models\Modules\Marketplaces\MarketplaceProductCollection;
use Mail;
use App\Mail\Marketplaces\MarketplaceCreatedMail;
use App\Models\Modules\Marketplaces\MarketplaceSetting;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;

class MarketplacesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marketplaces = Marketplace::active();
        $search = request('search');
        if ($search) {
            $marketplaces = $marketplaces->where('name', 'like', '%'.$search.'%');
        }
        $marketplaces = $marketplaces->latest()->paginate(24);

        $collectionId = request('collection');
        $collection = WebsiteMarketplaceCollection::find($collectionId);
        if ($collection) {
            $marketplaces = $collection->details()
                ->whereHas('marketplace', function($q){
                    $search = request('search');
                    if ($search) {
                        $q = $q->where('name', 'like', '%'.$search.'%');
                    }
                    $q->active();
                })
                ->with('marketplace')
                ->paginate(24);
        }

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.marketplaces.index', compact('marketplaces', 'collection', 'websiteSettings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.marketplaces.create', compact('websiteSettings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'image' => 'required|image|max:4000',
        ]);

        $user = auth()->user();

        $marketplace = new Marketplace;
        $marketplace->user_id = $user->id;
        $marketplace->name = request('name');
        $slug = str_slug(request('name'));
        $marketplace->slug = Marketplace::getUniqueSlug($slug);
        $marketplace->active = false;
        $marketplace->save();

        $image = request('image');
        if ($image) {
            $filename = 'marketplaces';
            $filepath = upload_file($image, 'uploads', $filename);
            $marketplace->image = $filepath;
            $marketplace->save();
        }

        $emailData = compact('user', 'marketplace');
        Mail::to($user->email)
            ->send(new MarketplaceCreatedMail($emailData));

        $defaultSettings = [
            'skin' => 'blue',
            'marketplace_description' => null,
            'store_auto_approve' => false
        ];
        foreach ($defaultSettings as $key => $value) {
            MarketplaceSetting::updateOrCreate([
                'marketplace_id' => $marketplace->id,
                'group' => 'settings',
                'key' => $key,
            ], [
                'value' => $value
            ]);
        }

        flash('Marketplace created. Waiting Approval');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Marketplace  $marketplace
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $marketplace = Marketplace::where('slug', $slug)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $stores = $marketplace->stores()->active()->has('addresses')->get();
        $categories = $marketplace->categories()->where('parent_id', null)->orderBy('sort_order')->get();

        $popularSearches = $marketplace->promoPopularSearches()->orderBy('sort_order')->take(5)->get();

        $bannerGroups = $marketplace->bannerGroups()
            ->sort()
            ->active()
            ->has('details')
            ->with(['details' => function($q){
                $q->sort()
                    ->with('marketplaceBanner')
                    ->whereHas('marketplaceBanner', function($sq1){
                        $sq1->where('active', 1);
                    });
            }])
            ->get();

        $storeCollections = $marketplace->storeCollections()
            ->sort()
            ->has('details')
            ->with(['details' => function($q) {
                $q->sort()
                    ->with('store')
                    ->whereHas('store', function($sq1){
                        $sq1->where('active', 1);
                    });
            }])
            ->get();
            // return $storeCollections;

        $productCollections = $marketplace->productCollections()->active()
            ->sort()
            ->has('details')
            ->with(['details' => function($q){
                 $q->sort()
                    ->with('product', 'product.store', 'product.store.marketplace', 'product.reviews')
                    ->whereHas('product', function($sq1){
                        $sq1->where('active', 1);
                    });
            }])
            ->get();

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.marketplaces.show', compact(
            'marketplace', 'marketplaceSettings',
            'bannerGroups', 'categories',
            'popularSearches',
            'stores',
            'storeCollections', 'productCollections',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Marketplace  $marketplace
     * @return \Illuminate\Http\Response
     */
    public function edit(Marketplace $marketplace)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Marketplace  $marketplace
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Marketplace  $marketplace
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace)
    {
        //
    }
}
