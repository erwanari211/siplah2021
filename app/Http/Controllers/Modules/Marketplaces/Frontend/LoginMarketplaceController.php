<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteSettings;
use App\User;
use App\Models\Modules\Marketplaces\MarketplaceStaff;

class LoginMarketplaceController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.frontend.marketplaces.login', compact('websiteSettings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'email'    => 'required|email',
            'password' => 'required|alphaNum|min:3'
        ];

        $validator = \Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            return \Redirect::back()
                ->withErrors($validator)
                ->withInput(request()->except('password'));
        } else {
            $errorMessage = 'These credentials do not match our records';

            $email = request('email');
            $user = User::where('email', $email)->first();
            if (!$user) {
                return \Redirect::back()
                    ->withErrors([ 'email' => $errorMessage, ])
                    ->withInput(request()->except('password'));
            }

            if ($user) {
                $isMarketplaceStaff = MarketplaceStaff::where('user_id', $user->id)->exists();
            }

            if (!$isMarketplaceStaff) {
                return \Redirect::back()
                    ->withErrors([ 'email' => $errorMessage, ])
                    ->withInput(request()->except('password'));
            }

            $userdata = [
                'email'     => request('email'),
                'password'  => request('password')
            ];

            if (! \Auth::attempt($userdata)) {
                return \Redirect::back()
                    ->withErrors([ 'email' => $errorMessage, ])
                    ->withInput(request()->except('password'));
            } else {
                $staff = MarketplaceStaff::where('user_id', $user->id)->first();
                $marketplace = $staff->marketplace;
                $marketplace->staffLoggedIn();
                return redirect()->route('users.marketplaces.index', $staff->marketplace_id);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
