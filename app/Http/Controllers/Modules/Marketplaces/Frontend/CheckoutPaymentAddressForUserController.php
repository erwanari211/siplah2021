<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\ShoppingCart;
use Cart;
use App\Models\UserAddress;
use App\Models\ZoneOngkirProvince;
use App\Models\ZoneOngkirCity;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict;

class CheckoutPaymentAddressForUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($storeSlug)
    {
        if (!ShoppingCart::checkCartStore($storeSlug)) {
            flash('Selected cart is empty')->error();
            return redirect()->route('cart.index');
        }

        $step = [
            'step_1' => ['name' => 'cart', 'completed' => true, ],
        ];
        ShoppingCart::setCheckoutSteps($storeSlug, $step);

        if (!ShoppingCart::checkCheckoutSteps($storeSlug, [1])) {
            flash('Please complete previous step')->error();
            return redirect()->route('checkout.show', [$storeSlug]);
        }

        $data = ShoppingCart::getCarts();
        $store = $data['stores'][$storeSlug];
        $marketplace = $store->marketplace;
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $categories = $marketplace->categories()->where('parent_id', 0)->get();
        Cart::instance($storeSlug);

        $user = auth()->user();
        $userAddresses = UserAddress::getUserAddressesDropdown($user);
        $dropdown['userAddresses'] = $userAddresses;
        $userHasAddress = count($userAddresses) ? true : false;
        $selectedAddress = ShoppingCart::getPaymentAddress($storeSlug);
        $selectedAddressId = ($selectedAddress && isset($selectedAddress->id)) ? $selectedAddress->id : null;

        /*
        $provinces = ZoneOngkirProvince::getProvinceWithCities();
        $provinceId = old('province_id');
        $dropdown['provinces'] = ZoneOngkirProvince::pluck('province', 'province_id');
        $dropdown['cities'] =  ZoneOngkirCity::getDropdown($provinceId);
        */

        // New
        $provinces = [];
        $provincesWithDetails = IndonesiaProvince::getProvincesWithDetails();

        $dropdown['provinces'] = IndonesiaProvince::getDropdown();

        $provinceId = old('province_id');
        $dropdown['cities'] = IndonesiaCity::getDropdown($provinceId);

        $cityId = old('city_id');
        $dropdown['districts'] = IndonesiaDistrict::getDropdown($cityId);

        $useHomepageFooter = true;

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.checkout.payment-address', compact(
            'store', 'marketplace', 'marketplaceSettings', 'categories',
            'user', 'userHasAddress', 'selectedAddressId',
            'dropdown', 'provinces', 'provincesWithDetails',
            'useHomepageFooter',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $storeSlug)
    {
        request()->validate([
            'payment_address' => 'required',
            'address_id' => 'required_if:payment_address,existing',

            'label' => 'required_if:payment_address,new',
            'full_name' => 'required_if:payment_address,new',
            'address' => 'required_if:payment_address,new',
            'phone' => 'required_if:payment_address,new',
            'postcode' => 'required_if:payment_address,new',
            'province_id' => 'required_if:payment_address,new',
            'city_id' => 'required_if:payment_address,new',
            'district_id' => 'required_if:payment_address,new',
        ]);

        if (!ShoppingCart::checkCartStore($storeSlug)) {
            flash('Selected cart is empty')->error();
            return redirect()->route('cart.index');
        }

        /** @var \App\User|null $user */
        $user = auth()->user();
        $paymentAddress = request('payment_address');

        if ($paymentAddress == 'existing') {
            $addressId = request('address_id');
            $address = $user->addresses()->where('id', $addressId)->first();
            if (!$address) {
                flash('Please select user address')->error();
                return redirect()->back();
            }
        }

        if ($paymentAddress == 'new') {
            $address = new UserAddress;
            $address->label = request('label');
            $address->full_name = request('full_name');
            $address->company = request('company');
            $address->address = request('address');
            $address->province_id = request('province_id');
            $address->city_id = request('city_id');
            $address->district_id = request('district_id');
            $address->phone = request('phone');
            $address->postcode = request('postcode');
            $address->default_address = false;

            $user->addresses()->save($address);
        }

        ShoppingCart::setPaymentAddress($storeSlug, $address);
        $step = [
            'step_2' => ['name' => 'payment_address', 'completed' => true, ],
        ];
        ShoppingCart::setCheckoutSteps($storeSlug, $step);

        return redirect()->route('checkout.steps.shipping-address.edit', [$storeSlug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
