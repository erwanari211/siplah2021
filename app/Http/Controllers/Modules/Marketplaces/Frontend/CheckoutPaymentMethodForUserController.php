<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\ShoppingCart;
use Cart;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Marketplaces\StoreSeller;

class CheckoutPaymentMethodForUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($storeSlug)
    {
        if (!ShoppingCart::checkCartStore($storeSlug)) {
            flash('Selected cart is empty')->error();
            return redirect()->route('cart.index');
        }

        if (!ShoppingCart::checkCheckoutSteps($storeSlug, [1, 2, 3, 4])) {
            flash('Please complete previous step')->error();
            return redirect()->route('checkout.steps.shipping-method.edit', [$storeSlug]);
        }

        $data = ShoppingCart::getCarts();
        $store = $data['stores'][$storeSlug];
        $marketplace = $store->marketplace;
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $categories = $marketplace->categories()->where('parent_id', 0)->get();
        Cart::instance($storeSlug);

        $paymentMethod = ShoppingCart::getPaymentMethod($storeSlug);
        $selectedPaymentMethod = null;
        $note = null;
        if ($paymentMethod) {
            $selectedPaymentMethod = isset($paymentMethod['label']) ? $paymentMethod['label'] : null;
            $note = isset($paymentMethod['note']) ? $paymentMethod['note'] : null;
        }

        $dropdown['paymentMethods'] = $store->paymentMethods()->pluck('name', 'label');

        $orderMeta = ShoppingCart::getOrder($storeSlug);
        $selectedStoreSeller = null;
        if ($orderMeta) {
            $selectedStoreSeller = isset($orderMeta['store_seller']) ? $orderMeta['store_seller'] : null;
        }

        $storeSellers = $store->storeSellers()->where('active', true)->orderBy('seller_name')->get();
        $dropdown['storeSellers'] = $storeSellers->mapWithKeys(function($item){
            $sellerName = $item['seller_name'];
            $sellerNote = $item['note'];
            $sellerName .= $sellerNote ? ' ('.$sellerNote.')' : '';
            return [$sellerName => $sellerName];
        });

        $useHomepageFooter = true;

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.checkout.payment-method', compact(
            'store', 'marketplace', 'marketplaceSettings', 'categories',
            'dropdown', 'selectedPaymentMethod', 'note',
            'storeSellers', 'selectedStoreSeller',
            'useHomepageFooter',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $storeSlug)
    {
        request()->validate([
            'payment_method' => 'required',
        ]);

        if (!ShoppingCart::checkCartStore($storeSlug)) {
            flash('Selected cart is empty')->error();
            return redirect()->route('cart.index');
        }

        $data = ShoppingCart::getCarts();
        $store = $data['stores'][$storeSlug];
        Cart::instance($storeSlug);
        $cartMeta = ShoppingCart::getCartMeta($storeSlug);

        $paymentMethod = request('payment_method');
        $selectedPaymentMethod = $store->paymentMethods()->where('label', $paymentMethod)->first();
        if (!$selectedPaymentMethod) {
            flash('Please select payment method')->error();
            return redirect()->back();
        }

        $uniqueNumber = isset($cartMeta['payment_method']['unique_number']) ? $cartMeta['payment_method']['unique_number'] : rand(101, 999);
        $paymentMethod = [
            'name' => $selectedPaymentMethod->name,
            'label' => $selectedPaymentMethod->label,
            'info' => $selectedPaymentMethod->info,
            'note' => request('note'),
            'unique_number' => $uniqueNumber,
        ];

        ShoppingCart::setPaymentMethod($storeSlug, $paymentMethod);
        $step = [
            'step_5' => ['name' => 'payment_method', 'completed' => true, ],
        ];
        ShoppingCart::setCheckoutSteps($storeSlug, $step);

        $cartMeta = ShoppingCart::getCartMeta($storeSlug);
        $subtotal = Cart::instance($store->slug)->subtotal();
        $order['store'] = $store;
        $order['subtotal'] = floatvalue($subtotal);
        $order['shipping'] = $cartMeta['shipping_method']['value'];
        $order['unique_number'] = $cartMeta['payment_method']['unique_number'];
        $order['total'] = $order['subtotal'] + $order['shipping'] + $order['unique_number'];

        $storeSeller = request('store_seller');
        $order['store_seller'] = $storeSeller;

        ShoppingCart::setOrder($storeSlug, $order);

        return redirect()->route('checkout.steps.confirm.show', [$storeSlug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
