<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreSetting;
use App\Models\Modules\Marketplaces\StoreShippingMethod;
use App\Models\Modules\Marketplaces\Marketplace;
use Mail;
use App\Mail\Marketplaces\MarketplaceStoreCreatedMail;
use App\Mail\Marketplaces\MarketplaceStoreApprovedMail;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity;

class StoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($marketplace)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $categories = $marketplace->categories()->where('parent_id', 0)->get();

        $stores = $marketplace->stores();
        $search = request('search');
        if ($search) {
            $stores = $stores->where('name', 'like', '%'.$search.'%');
        }

        $selectedCity = null;
        if ($cityId = request('city_id')) {
            $selectedCity = IndonesiaCity::find($cityId);
            $stores = $stores->where('city_id', $cityId);
        }

        $stores = $stores->latest()->paginate(24);

        $collectionId = request('collection');
        $collection = $marketplace->storeCollections()->find($collectionId);
        if ($collection) {
            $stores =  $collection->details()
                ->sort()
                ->with('store')
                ->whereHas('store', function($q){
                    $search = request('search');
                    if ($search) {
                        $q = $q->where('name', 'like', '%'.$search.'%');
                    }
                    $q->where('active', 1);
                })->paginate(24);
        }

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.stores.index', compact(
            'marketplace', 'marketplaceSettings',
            'categories', 'stores', 'collection',
            'selectedCity',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($marketplace)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $categories = $marketplace->categories()->where('parent_id', 0)->get();

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.stores.create', compact(
            'marketplace', 'marketplaceSettings', 'categories',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $marketplace)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();

        request()->validate([
            'name' => 'required',
            'image' => 'required|image|max:4000',
        ]);

        $user = auth()->user();

        $store = new Store;
        $store->marketplace_id = $marketplace->id;
        $store->user_id = $user->id;
        $store->name = request('name');
        $slug = str_slug(request('name'));
        $store->slug = Store::getUniqueSlug($slug);
        $store->active = false;
        $store->save();

        // send created mail
        $emailData = compact('user', 'marketplace', 'store');
        Mail::to($user->email)
            ->send(new MarketplaceStoreCreatedMail($emailData));

        $autoApprove = $marketplace->settings()->where('key', 'store_auto_approve')->first();
        if ($autoApprove) {
            if ($autoApprove->value == 1) {
                $store->active = true;
                $store->save();

                // send approved mail
                $owner = $user;
                $emailData = compact('owner', 'marketplace', 'store');
                Mail::to($user->email)
                    ->send(new MarketplaceStoreApprovedMail($emailData));
            }
        }
        // return $store;

        $image = request('image');
        if ($image) {
            $filename = 'stores';
            $filepath = upload_file($image, 'uploads', $filename);
            $store->image = $filepath;
            $store->save();
        }

        $keys = ['store_tagline', 'store_description', 'location', 'order_prefix'];
        foreach ($keys as $key) {
            $setting = new StoreSetting;
            $setting->store_id = $store->id;
            $setting->group = 'settings';
            $setting->key = $key;
            $setting->value = null;
            $setting->save();
        }

        $couriers = get_rajaongkir_couriers();
        foreach ($couriers as $courier => $courierName) {
            $method = new StoreShippingMethod;
            $method->group = 'rajaongkir';
            $method->label = $courier;
            $method->name = $courierName;
            $method->active = false;
            $method->store_id = $store->id;
            $method->save();
        }

        flash('Store created. Waiting Approval');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show($marketplace, $store)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();

        $store->addView();
        $storeSettings = $store->mappedSettings();
        $categories = $store->categories()->where('parent_id', 0)->orderBy('sort_order')->get();
        $totalCategories = $store->categories()->count();
        $products = $store->products()->active()->status('active');
        $totalProducts = $products->count();
        $products = $products->latest();
        $products = $products->with('kemdikbudZonePrices');
        $products = $products->take(12)->get();

        $bannerGroups = $store->bannerGroups()
            ->sort()
            ->active()
            ->has('details')
            ->with(['details' => function($q){
                $q->sort()
                    ->with('storeBanner')
                    ->whereHas('storeBanner', function($sq1){
                        $sq1->where('active', 1);
                    });
            }])
            ->get();

        $storeFeaturedProducts = $store->storeFeaturedProducts()->active()->with('details', 'details.product')->get();
        $hasDefaultStoreAddress = $store->hasDefaultStoreAddress();
        $defaultStoreAddress = $store->getDefaultStoreAddress();

        $storeMenus = $store->menus()
            ->where('group', 'menu header')
            ->where('parent_id', 0)
            ->with(['children' => function($q){
                $q->orderBy('sort_order');
            }])
            ->orderBy('sort_order')
            ->get();

        $storeNotes = $store->notes()
            ->where('active', 1)
            ->paginate(10);

        $storeDiscussions = $store->productDiscussions()
            ->withCount('details')
            ->with('product', 'user')
            ->latest()->paginate(5);

        $storeReviews = $store->productReviews()
            ->with('product', 'user')
            ->latest()->paginate(5);

        $shippingMethods = $store->shippingMethods;
        $storeShippingMethods = [];
        foreach ($shippingMethods as $shippingMethod) {
            $shippingMethodGroup = $shippingMethod['group'];
            $shippingMethodLabel = $shippingMethod['label'];
            $shippingMethodName = $shippingMethod['name'];
            if ($shippingMethod['active']) {
                $storeShippingMethods[$shippingMethodGroup][$shippingMethodLabel] = $shippingMethodName;
            }
        }

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.stores.show', compact(
            'marketplace', 'marketplaceSettings',
            'store', 'categories', 'totalCategories',
            'storeSettings', 'storeMenus', 'storeNotes',
            'storeBanners', 'storeFeaturedProducts', 'bannerGroups', 'hasDefaultStoreAddress',
            'defaultStoreAddress',
            'products', 'totalProducts',
            'storeDiscussions', 'storeReviews',
            'storeShippingMethods',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        //
    }

    public function displayAllStore()
    {
        $stores = Store::where('active', 1)
            ->with('marketplace')
            ->whereHas('marketplace', function($q){
                $q->where('active', 1);
            });
        $search = request('search');
        if ($search) {
            $stores = $stores->where('name', 'like', '%'.$search.'%');
        }
        $stores = $stores->latest()->paginate(24);

        // return $stores;

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.stores.all-stores', compact(
            'marketplace', 'marketplaceSettings',
            'categories', 'stores', 'collection',
            'websiteSettings'
        ));
    }
}
