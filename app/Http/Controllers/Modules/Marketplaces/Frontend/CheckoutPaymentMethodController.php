<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\ShoppingCart;
use Cart;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Marketplaces\StoreSeller;
use App\Models\Modules\Marketplaces\School;

class CheckoutPaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($storeSlug)
    {
        if (!ShoppingCart::checkCartStore($storeSlug)) {
            flash('Selected cart is empty')->error();
            return redirect()->route('cart.index');
        }

        if (!ShoppingCart::checkCheckoutSteps($storeSlug, [1, 2, 3, 4])) {
            flash('Please complete previous step')->error();
            return redirect()->route('checkout.steps.shipping-method.edit', [$storeSlug]);
        }

        $staffRole = session('school_staff.role');
        $allowedRoles = config('siplah.siplah.satdik_jabatan_yang_bisa_checkout');
        $allowedToCheckout = in_array($staffRole, $allowedRoles);
        if (!$allowedToCheckout) {
            flash('You are not allowed to checkout')->error();
            return redirect()->route('cart.index');
        }
        $schoolId = session('school_id');
        $school = School::find($schoolId);

        $data = ShoppingCart::getCarts();
        $store = $data['stores'][$storeSlug];
        $marketplace = $store->marketplace;
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $categories = $marketplace->categories()->where('parent_id', 0)->get();
        Cart::instance($storeSlug);

        $paymentMethod = ShoppingCart::getPaymentMethod($storeSlug);
        $selectedPaymentMethod = null;
        $note = null;
        if ($paymentMethod) {
            $selectedPaymentMethod = isset($paymentMethod['label']) ? $paymentMethod['label'] : null;
            $note = isset($paymentMethod['note']) ? $paymentMethod['note'] : null;
        }

        $orderMeta = ShoppingCart::getOrder($storeSlug);

        $useHomepageFooter = true;

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.checkout.payment-method', compact(
            'store', 'marketplace', 'marketplaceSettings', 'categories',
            'dropdown', 'selectedPaymentMethod', 'note',
            'useHomepageFooter',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $storeSlug)
    {
        request()->validate([
            'payment_method' => 'required',
            'note' => 'nullable',
        ]);

        if (!ShoppingCart::checkCartStore($storeSlug)) {
            flash('Selected cart is empty')->error();
            return redirect()->route('cart.index');
        }

        $staffRole = session('school_staff.role');
        $allowedRoles = config('siplah.siplah.satdik_jabatan_yang_bisa_checkout');
        $allowedToCheckout = in_array($staffRole, $allowedRoles);
        if (!$allowedToCheckout) {
            flash('You are not allowed to checkout')->error();
            return redirect()->route('cart.index');
        }
        $schoolId = session('school_id');
        $school = School::find($schoolId);

        $data = ShoppingCart::getCarts();
        $store = $data['stores'][$storeSlug];
        Cart::instance($storeSlug);
        $cartMeta = ShoppingCart::getCartMeta($storeSlug);

        $paymentMethod = request('payment_method');

        $marketplace = $store->marketplace;
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });

        $uniqueNumber = isset($cartMeta['payment_method']['unique_number']) ? $cartMeta['payment_method']['unique_number'] : rand(101, 999);
        $paymentMethod = [
            'name' => $marketplaceSettings['payment_bank_name'],
            'label' => $marketplaceSettings['payment_bank_name'],
            'info' => "Please transfer to following bank account : <br>
                {$marketplaceSettings['payment_bank_name']} <br>
                a/n {$marketplaceSettings['payment_account_name']} <br>
                {$marketplaceSettings['payment_account_number']} <br>
                {$marketplaceSettings['payment_bank_branch']} <br>
            ",
            'note' => request('note'),
            'unique_number' => $uniqueNumber,
        ];

        ShoppingCart::setPaymentMethod($storeSlug, $paymentMethod);
        $step = [
            'step_5' => ['name' => 'payment_method', 'completed' => true, ],
        ];
        ShoppingCart::setCheckoutSteps($storeSlug, $step);

        $cartMeta = ShoppingCart::getCartMeta($storeSlug);
        $subtotal = Cart::instance($store->slug)->subtotal();
        $order['store'] = $store;
        $order['subtotal'] = floatvalue($subtotal);
        $order['shipping'] = $cartMeta['shipping_method']['value'];
        $order['unique_number'] = $cartMeta['payment_method']['unique_number'];
        $order['total'] = $order['subtotal'] + $order['shipping'] + $order['unique_number'];

        $storeSeller = request('store_seller');
        $order['store_seller'] = $storeSeller;

        ShoppingCart::setOrder($storeSlug, $order);

        return redirect()->route('checkout.steps.confirm.show', [$storeSlug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
