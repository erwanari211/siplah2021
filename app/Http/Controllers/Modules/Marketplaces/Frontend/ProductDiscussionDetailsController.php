<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\ProductDiscussion;
use App\Models\Modules\Marketplaces\ProductDiscussionDetail;

class ProductDiscussionDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $marketplace, $store, $product, $discussionId)
    {
        request()->validate([
            'content' => 'required',
        ]);

        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $product = $store->products()->where('slug', $product)->firstOrFail();
        $discussion = $product->discussions()->findOrFail($discussionId);

        $reply = new ProductDiscussionDetail;
        $reply->product_discussion_id = $discussion->id;
        $reply->user_id = auth()->user()->id;
        $reply->is_store_owner = auth()->user()->id == $store->user_id;
        $reply->content = request('content');
        $reply->save();

        $discussion->touch();

        flash('Reply submitted');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Custom
     */
    public function trash(Request $request, $marketplace, $store, $product, $discussionId, $replyId)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $product = $store->products()->where('slug', $product)->firstOrFail();
        $discussion = $product->discussions()->findOrFail($discussionId);
        $reply = $discussion->details()->findOrFail($replyId);

        $reply->active = false;
        $reply->save();

        flash('Reply deleted');
        return redirect()->back();
    }

    public function restore(Request $request, $marketplace, $store, $product, $discussionId, $replyId)
    {
        $marketplace = Marketplace::where('slug', $marketplace)->firstOrFail();
        $store = $marketplace->stores()->where('slug', $store)->firstOrFail();
        $product = $store->products()->where('slug', $product)->firstOrFail();
        $discussion = $product->discussions()->findOrFail($discussionId);
        $reply = $discussion->details()->findOrFail($replyId);

        $reply->active = true;
        $reply->save();

        flash('Reply restored');
        return redirect()->back();
    }
}
