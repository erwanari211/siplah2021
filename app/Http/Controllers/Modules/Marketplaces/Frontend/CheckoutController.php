<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\ShoppingCart;
use Cart;
use App\Models\Modules\Marketplaces\Order;
use App\Models\Modules\Marketplaces\OrderProduct;
use App\Models\Modules\Marketplaces\OrderHistory;
use App\Models\Modules\Marketplaces\OrderMeta;
use App\Models\Modules\Marketplaces\OrderStatus;
use App\Models\ZoneOngkirProvince;
use App\Models\ZoneOngkirCity;
use Mail;
use App\Mail\Marketplaces\UserOrderCreatedMail;
use App\Mail\Marketplaces\StoreOrderCreatedMail;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Marketplaces\GroupNotification;
use App\Models\Modules\Marketplaces\School;
use App\Models\Modules\Marketplaces\Cart as CartModel;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($storeSlug)
    {
        if (!ShoppingCart::checkCartStore($storeSlug)) {
            flash('Selected cart is empty')->error();
            return redirect()->route('cart.index');
        }

        if (!ShoppingCart::checkCheckoutSteps($storeSlug, [1, 2, 3, 4, 5])) {
            flash('Please complete previous step')->error();
            return redirect()->route('checkout.steps.payment-method.edit', [$storeSlug]);
        }

        $data = ShoppingCart::getCarts();
        $store = $data['stores'][$storeSlug];
        $marketplace = $store->marketplace;
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $categories = $marketplace->categories()->where('parent_id', 0)->get();
        Cart::instance($storeSlug);
        $cartMeta = ShoppingCart::getCartMeta($storeSlug);

        $useHomepageFooter = true;

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.checkout.confirm', compact(
            'store', 'marketplace', 'marketplaceSettings', 'categories',
            'cartMeta',
            'useHomepageFooter',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $storeSlug)
    {
        $staffRole = session('school_staff.role');
        $allowedRoles = config('siplah.siplah.satdik_jabatan_yang_bisa_checkout');
        $allowedToCheckout = in_array($staffRole, $allowedRoles);
        if (!$allowedToCheckout) {
            flash('You are not allowed to checkout')->error();
            return redirect()->route('cart.index');
        }
        $schoolId = session('school_id');
        $school = School::find($schoolId);

        $data = ShoppingCart::getCarts();
        $store = $data['stores'][$storeSlug];
        Cart::instance($storeSlug);
        $cartMeta = ShoppingCart::getCartMeta($storeSlug);

        $orderStatus = OrderStatus::where('group', 'order_process')->where('key', 'pending')->first();
        $invocePrefix = $store->settings()->where('key', 'order_prefix')->first();
        $invocePrefix = isset($invocePrefix->value) ? $invocePrefix->value : '';

        $order = new Order;
        $order->store_id = $store->id;
        $order->school_id = $school->id;
        $order->user_id = auth()->user()->id;
        $order->store_name = $store->name;
        $order->school_name = $school->name;
        $order->user_name = auth()->user()->name;
        $order->total = $cartMeta['order']['total'];
        $order->currency = 'idr';
        $order->order_status_id = $orderStatus->id;
        $order->order_status = $orderStatus->value;
        $order->invoice_prefix = $invocePrefix;
        $order->invoice_no = date('ymd').'-'.str_random(6);
        $order->ip = request()->ip();
        $order->user_agent = request()->userAgent();
        $order->save();

        $notificationData['data']['new'] = $order->getOriginal();
        $notificationData['description'] = 'Order telah dibuat';
        GroupNotification::addNotification($store, 'order_created', $notificationData);

        $cartContent = Cart::instance($storeSlug)->content();
        OrderProduct::saveProducts($order, $cartContent);

        $history = new OrderHistory;
        $history->order_status_id = $orderStatus->id;
        $history->order_status = $orderStatus->key;
        $history->notify = 1;
        $order->histories()->save($history);

        $cartMeta = Shoppingcart::getCartMeta($storeSlug);
        $paymentAddress = $cartMeta['payment_address'];
        $shippingAddress = $cartMeta['shipping_address'];
        $shippingMethod = $cartMeta['shipping_method'];
        $paymentMethod = $cartMeta['payment_method'];
        $orderMeta = $cartMeta['order'];

        OrderMeta::setOrderPaymentAddress($order, $paymentAddress);
        OrderMeta::setOrderShippingAddress($order, $shippingAddress);
        OrderMeta::setOrderShippingMethod($order, $shippingMethod);
        OrderMeta::setOrderPaymentMethod($order, $paymentMethod);
        OrderMeta::setOrderDetail($order, $orderMeta);

        // dump('Store');
        // dump($store);
        // dump('order');
        // dump($order);
        // dump('cart content');
        // dump($cartContent);
        // dump('payment address');
        // dump($paymentAddress);
        // dump('shipping address');
        // dump($shippingAddress);
        // dump('shipping method');
        // dump($shippingMethod);
        // dump('payment method');
        // dump($paymentMethod);
        // dump('order meta');
        // dump($orderMeta);

        $provincesData = ZoneOngkirProvince::get();
        $citiesData = ZoneOngkirCity::get();

        $provinces = $provincesData->mapWithKeys(function ($item) {
            return [$item['province_id'] => $item];
        });
        $cities = $citiesData->mapWithKeys(function ($item) {
            return [$item['city_id'] => $item];
        });

        $user = auth()->user();
        $storeOwner = $store->user;
        $emailData = compact(
            'user', 'store', 'order',
            'cartContent',
            'paymentAddress', 'shippingAddress', 'paymentMethod',
            'orderMeta',
            'provinces', 'cities'
        );

        Mail::to($user->email)
            ->send(new UserOrderCreatedMail($emailData));
        Mail::to($storeOwner->email)
            ->send(new StoreOrderCreatedMail($emailData));

        Shoppingcart::destroyCartMeta($storeSlug);
        Cart::instance($storeSlug)->destroy();

        if ($school) {
            CartModel::remove($school, $storeSlug);
            $school->staffShoppingCartCheckout();
        }

        flash('Order saved');
        return redirect()->route('cart.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($storeSlug)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($storeSlug)
    {
        if (!ShoppingCart::checkCartStore($storeSlug)) {
            flash('Selected cart is empty')->error();
            return redirect()->route('cart.index');
        }

        $staffRole = session('school_staff.role');
        $allowedRoles = config('siplah.siplah.satdik_jabatan_yang_bisa_checkout');
        $allowedToCheckout = in_array($staffRole, $allowedRoles);
        if (!$allowedToCheckout) {
            flash('You are not allowed to checkout')->error();
            return redirect()->route('cart.index');
        }

        $data = ShoppingCart::getCarts();
        $store = $data['stores'][$storeSlug];

        Cart::instance($storeSlug);
        $cartContent = Cart::content();
        $data = $cartContent;
        if ($data) {
            foreach ($data as $input) {
                $rowId = $input->rowId;
                $product = $cartContent[$rowId]->model;

                if ($product->qty >= $input->qty) {
                    $qty = $input->qty;
                } else {
                    $qty = $product->qty;
                    flash($product->name.' from '.$store->name.' available stock only '.$qty)->warning();
                }
                Cart::update($input->rowId, $qty);
            }
        }

        if (Cart::instance($store)->count() == 0) {
            Cart::instance($store)->destroy();
        }

        $marketplace = $store->marketplace;
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $categories = $marketplace->categories()->where('parent_id', 0)->get();
        Cart::instance($storeSlug);

        $useHomepageFooter = true;

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.checkout.show', compact(
            'marketplace', 'marketplaceSettings', 'store', 'categories',
            'useHomepageFooter',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
