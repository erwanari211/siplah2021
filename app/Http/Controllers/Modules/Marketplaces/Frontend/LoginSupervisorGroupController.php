<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteSettings;
use App\User;
use App\Models\Modules\Marketplaces\SupervisorStaff;

class LoginSupervisorGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.frontend.supervisor-groups.login', compact('websiteSettings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'email'    => 'required|email',
            'password' => 'required|alphaNum|min:3'
        ];

        $validator = \Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            return \Redirect::back()
                ->withErrors($validator)
                ->withInput(request()->except('password'));
        }

        $errorMessage = 'These credentials do not match our records';

        $email = request('email');
        $user = User::where('email', $email)->first();
        if (!$user) {
            return \Redirect::back()
                ->withErrors([ 'email' => $errorMessage, ])
                ->withInput(request()->except('password'));
        }

        if ($user) {
            $isStoreStaff = SupervisorStaff::where('user_id', $user->id)->exists();
        }
        if (!$isStoreStaff) {
            return \Redirect::back()
                ->withErrors([ 'email' => $errorMessage, ])
                ->withInput(request()->except('password'));
        }

        $userdata = [
            'email'     => request('email'),
            'password'  => request('password')
        ];

        if (! \Auth::attempt($userdata)) {
            return \Redirect::back()
                ->withErrors([ 'email' => $errorMessage, ])
                ->withInput(request()->except('password'));
        }

        $staff = SupervisorStaff::where('user_id', $user->id)->first();
        $supervisorGroup = $staff->supervisorGroup;
        $supervisorGroup->staffLoggedIn();

        return redirect()->route('users.supervisor-groups.home', $staff->supervisor_group_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
