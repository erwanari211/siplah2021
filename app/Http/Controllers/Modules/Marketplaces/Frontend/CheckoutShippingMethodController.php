<?php

namespace App\Http\Controllers\Modules\Marketplaces\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\ShoppingCart;
use Cart;
use RajaOngkir;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;
use Exception;
use App\Models\Modules\Marketplaces\School;

class CheckoutShippingMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($storeSlug)
    {
        if (!ShoppingCart::checkCartStore($storeSlug)) {
            flash('Selected cart is empty')->error();
            return redirect()->route('cart.index');
        }

        if (!ShoppingCart::checkCheckoutSteps($storeSlug, [1, 2, 3])) {
            flash('Please complete previous step')->error();
            return redirect()->route('checkout.steps.shipping-address.edit', [$storeSlug]);
        }

        $data = ShoppingCart::getCarts();
        $store = $data['stores'][$storeSlug];
        $marketplace = $store->marketplace;
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $categories = $marketplace->categories()->where('parent_id', 0)->get();
        Cart::instance($storeSlug);

        $shippingMethod = ShoppingCart::getshippingMethod($storeSlug);
        $selectedGroup = null;
        $selectedCourier = null;
        $selectedService = null;
        if ($shippingMethod) {
            $selectedGroup = isset($shippingMethod['group']) ? $shippingMethod['group'] : 'rajaongkir';
            $selectedCourier = isset($shippingMethod['code']) ? $shippingMethod['code'] : null;
            $selectedService = isset($shippingMethod['service']) ? $shippingMethod['service'] : null;
        }

        if (!$store->hasDefaultStoreAddress()) {
            flash($store->name.' currently has no address. So we cant calculate shipping cost.')->error();
            return redirect()->route('cart.index');
        }

        $staffRole = session('school_staff.role');
        $allowedRoles = config('siplah.siplah.satdik_jabatan_yang_bisa_checkout');
        $allowedToCheckout = in_array($staffRole, $allowedRoles);
        if (!$allowedToCheckout) {
            flash('You are not allowed to checkout')->error();
            return redirect()->route('cart.index');
        }
        $schoolId = session('school_id');
        $school = School::find($schoolId);

        $storeAddress = $store->addresses()
            ->with('province', 'city', 'district', 'district.rajaongkirDistrict')
            ->defaultAddress()->first();
        $storeRajaongkirDistrict = $storeAddress->district->rajaongkirDistrict->first();
        $origin = $storeRajaongkirDistrict->district_id;
        // return $storeAddress;

        $shippingAddress = ShoppingCart::getShippingAddress($storeSlug);
        $shippingAddress->load('province', 'city', 'district', 'district.rajaongkirDistrict');
        $customerRajaongkirDistrict = $shippingAddress->district->rajaongkirDistrict->first();
        $destination = $customerRajaongkirDistrict->district_id;
        // return $shippingAddress;

        // return compact('storeAddress', 'shippingAddress');
        $weight = ShoppingCart::getCartWeight($storeSlug);

        $storeShippingMethods = $store->shippingMethods()->where([
            'group' => 'rajaongkir',
            'active' => true,
        ])->pluck('label');
        $couriers = $storeShippingMethods ? $storeShippingMethods : ['jne', 'tiki', 'pos'];

        $costs = [];
        $dropdown['couriers'] = [];
        try {
            foreach ($couriers as $courier) {
                if ($courier == 'pos') {
                    if ($weight > 50000) {
                        continue;
                    }
                }
                $cost = RajaOngkir::Cost([
                    'origin'          => $origin,
                    'originType'      => 'subdistrict',
                    'destination'     => $destination,
                    'destinationType' => 'subdistrict',
                    'weight'          => $weight,
                    'courier'         => $courier,
                ])->get();
                $costs[$courier] = $cost;
                $dropdown['couriers'][$courier] = $cost[0]['name'];
            }
        } catch (Exception $e) {

        }

        $dropdown['groups'] = [
            'rajaongkir' => 'Normal',
            'custom' => 'Custom',
        ];

        $customStoreShippingMethods = $store->shippingMethods()->where([
            'group' => 'custom',
            'active' => true,
        ])->get();
        $dropdown['customCouriers'] = [];
        foreach ($customStoreShippingMethods as $customStoreShippingMethod) {
            $itemLabel = $customStoreShippingMethod['label'];
            $itemName = $customStoreShippingMethod['name'];
            $dropdown['customCouriers'][$itemLabel] = $itemName;
        }

        $useHomepageFooter = true;

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        $view = 'modules.marketplaces.frontend.checkout.shipping-method';
        if ($customStoreShippingMethods) {
            $view = 'modules.marketplaces.frontend.checkout.shipping-method-custom';
        }

        return view($view, compact(
            'store', 'marketplace', 'marketplaceSettings', 'categories', 'costs', 'dropdown',
            'selectedCourier', 'selectedService', 'selectedGroup',
            'customStoreShippingMethods',
            'useHomepageFooter',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $storeSlug)
    {
        request()->validate([
            'group' => 'required',
            'courier' => 'required',
            'service' => 'required',
        ]);

        if (!ShoppingCart::checkCartStore($storeSlug)) {
            flash('Selected cart is empty')->error();
            return redirect()->route('cart.index');
        }

        $staffRole = session('school_staff.role');
        $allowedRoles = config('siplah.siplah.satdik_jabatan_yang_bisa_checkout');
        $allowedToCheckout = in_array($staffRole, $allowedRoles);
        if (!$allowedToCheckout) {
            flash('You are not allowed to checkout')->error();
            return redirect()->route('cart.index');
        }
        $schoolId = session('school_id');
        $school = School::find($schoolId);

        $data = ShoppingCart::getCarts();
        $store = $data['stores'][$storeSlug];
        Cart::instance($storeSlug);

        $courier = request('courier');
        $service = request('service');
        $group = request('group');

        if ($group == 'rajaongkir') {
            $storeAddress = $store->addresses()
                ->with('province', 'city', 'district', 'district.rajaongkirDistrict')
                ->defaultAddress()->first();
            $storeRajaongkirDistrict = $storeAddress->district->rajaongkirDistrict->first();
            $origin = $storeRajaongkirDistrict->district_id;
            // return $storeAddress;

            $shippingAddress = ShoppingCart::getShippingAddress($storeSlug);
            $shippingAddress->load('province', 'city', 'district', 'district.rajaongkirDistrict');
            $customerRajaongkirDistrict = $shippingAddress->district->rajaongkirDistrict->first();
            $destination = $customerRajaongkirDistrict->district_id;
            // return $shippingAddress;

            // return compact('storeAddress', 'shippingAddress');
            $weight = ShoppingCart::getCartWeight($storeSlug);

            $courierCost = RajaOngkir::Cost([
                'origin'        => $origin,
                'originType'      => 'subdistrict',
                'destination'   => $destination,
                'destinationType' => 'subdistrict',
                'weight'        => $weight,
                'courier'       => $courier,
            ])->get();

            $selectedService = [];
            $courierCost = collect($courierCost)->first();
            $courierServices = $courierCost['costs'];

            foreach ($courierServices as $courierService) {
                if ($courierService['service'] == $service) {
                    $selectedService['group'] = $group;
                    $selectedService['code'] = $courierCost['code'];
                    $selectedService['name'] = $courierCost['name'];

                    $courierServiceCost = collect($courierService['cost'])->first();
                    $selectedService = array_merge($selectedService, $courierService) ;
                    $selectedService['value'] = $courierServiceCost['value'];
                    break;
                }
            }
        } else {
            $courier = request('courier');
            $courierService = $store->shippingMethods()->where([
                'group' => 'custom',
                'label' => $courier,
            ])->first();

            if (!$courierService) {
                flash('Please select shipping method')->error();
                return redirect()->back();
            }

            $selectedService['group'] = $group;
            $selectedService['code'] = $courierService['label'];
            $selectedService['name'] = $courierService['name'];
            $selectedService['service'] = null;
            $selectedService['description'] = null;
            $selectedService['value'] = 0;
        }

        ShoppingCart::setshippingMethod($storeSlug, $selectedService);
        $step = [
            'step_4' => ['name' => 'shipping_method', 'completed' => true, ],
        ];
        ShoppingCart::setCheckoutSteps($storeSlug, $step);

        return redirect()->route('checkout.steps.payment-method.edit', [$storeSlug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
