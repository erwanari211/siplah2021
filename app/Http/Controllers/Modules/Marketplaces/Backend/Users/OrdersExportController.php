<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\FromCollectionWithViewExport;
use App\Exports\FromCollectionWithViewMultipleSheetsExport;
use App\Models\Modules\Marketplaces\Order;
use PDF;

class OrdersExportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function exportSuratPesanan(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $storeAddress = $store->getDefaultStoreAddress();
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();

        $view = 'modules.marketplaces.backend.users.orders.exports.surat-pesanan';
        $pdfData = compact(
            'user',
            'store', 'storeAddress',
            'order', 'products', 'orderMetas'
        );

        // return view($view, $pdfData);

        $pdf = PDF::loadView($view, $pdfData);

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        $filename = 'surat-pesanan-'.$filename;
        return $pdf->download($filename);
    }

    public function exportSurveyBarang(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $storeAddress = $store->getDefaultStoreAddress();
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();

        $view = 'modules.marketplaces.backend.users.orders.exports.survey-barang';
        $pdfData = compact(
            'user',
            'store', 'storeAddress',
            'order', 'products', 'orderMetas'
        );

        // return view($view, $pdfData);

        $pdf = PDF::loadView($view, $pdfData);

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        $filename = 'survey-barang-'.$filename;
        return $pdf->download($filename);
    }

    public function exportSurveyBeritaAcara(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $storeAddress = $store->getDefaultStoreAddress();
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();

        $view = 'modules.marketplaces.backend.users.orders.exports.survey-berita-acara';
        $pdfData = compact(
            'user',
            'store', 'storeAddress',
            'order', 'products', 'orderMetas'
        );

        // return view($view, $pdfData);

        $pdf = PDF::loadView($view, $pdfData);

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        $filename = 'berita-acara-survey-'.$filename;
        return $pdf->download($filename);
    }

    public function exportSurveyBeritaAcaraLampiran(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $storeAddress = $store->getDefaultStoreAddress();
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();

        $view = 'modules.marketplaces.backend.users.orders.exports.survey-berita-acara-lampiran';
        $pdfData = compact(
            'user',
            'store', 'storeAddress',
            'order', 'products', 'orderMetas'
        );

        // return view($view, $pdfData);

        $pdf = PDF::loadView($view, $pdfData)->setPaper('a4', 'landscape');

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        $filename = 'lampiran-berita-acara-survey-'.$filename;
        return $pdf->download($filename);
    }

    public function exportHps(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $storeAddress = $store->getDefaultStoreAddress();
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();

        $view = 'modules.marketplaces.backend.users.orders.exports.hps';
        $pdfData = compact(
            'user',
            'store', 'storeAddress',
            'order', 'products', 'orderMetas'
        );

        // return view($view, $pdfData);

        $pdf = PDF::loadView($view, $pdfData);

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        $filename = 'hps-'.$filename;
        return $pdf->download($filename);
    }

    public function exportSpk(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $storeAddress = $store->getDefaultStoreAddress();
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();

        $view = 'modules.marketplaces.backend.users.orders.exports.spk';
        $pdfData = compact(
            'user',
            'store', 'storeAddress',
            'order', 'products', 'orderMetas'
        );

        // return view($view, $pdfData);

        $pdf = PDF::loadView($view, $pdfData);

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        $filename = 'spk-'.$filename;
        return $pdf->download($filename);
    }

    public function exportSpkLampiran(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $storeAddress = $store->getDefaultStoreAddress();
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();

        $view = 'modules.marketplaces.backend.users.orders.exports.spk-lampiran';
        $pdfData = compact(
            'user',
            'store', 'storeAddress',
            'order', 'products', 'orderMetas'
        );

        // return view($view, $pdfData);

        $pdf = PDF::loadView($view, $pdfData);

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        $filename = 'spk-lampiran-'.$filename;
        return $pdf->download($filename);
    }

    public function exportBappbj(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $storeAddress = $store->getDefaultStoreAddress();
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();

        $view = 'modules.marketplaces.backend.users.orders.exports.bappbj';
        $pdfData = compact(
            'user',
            'store', 'storeAddress',
            'order', 'products', 'orderMetas'
        );

        // return view($view, $pdfData);

        $pdf = PDF::loadView($view, $pdfData);

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        $filename = 'bappbj-'.$filename;
        return $pdf->download($filename);
    }

    public function exportBappbjLampiran(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $storeAddress = $store->getDefaultStoreAddress();
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();

        $view = 'modules.marketplaces.backend.users.orders.exports.bappbj-lampiran';
        $pdfData = compact(
            'user',
            'store', 'storeAddress',
            'order', 'products', 'orderMetas'
        );

        // return view($view, $pdfData);

        $pdf = PDF::loadView($view, $pdfData);

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        $filename = 'lampiran-bappbj-'.$filename;
        return $pdf->download($filename);
    }

    public function exportBeritaAcaraPembayaran(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $storeAddress = $store->getDefaultStoreAddress();
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();

        $view = 'modules.marketplaces.backend.users.orders.exports.berita-acara-pembayaran';
        $pdfData = compact(
            'user',
            'store', 'storeAddress',
            'order', 'products', 'orderMetas'
        );

        // return view($view, $pdfData);

        $pdf = PDF::loadView($view, $pdfData);

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        $filename = 'berita-acara-pembayaran-'.$filename;
        return $pdf->download($filename);
    }
}
