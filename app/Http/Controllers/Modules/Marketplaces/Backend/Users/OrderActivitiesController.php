<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Users;

use App\Models\Modules\Marketplaces\OrderActivity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Order;
use App\Models\Modules\Marketplaces\OrderMeta;

class OrderActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\OrderActivity  $orderActivity
     * @return \Illuminate\Http\Response
     */
    public function show(OrderActivity $orderActivity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\OrderActivity  $orderActivity
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderActivity $orderActivity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\OrderActivity  $orderActivity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderActivity $orderActivity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\OrderActivity  $orderActivity
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderActivity $orderActivity)
    {
        //
    }

    public function negotiationCommentsStore(Request $request, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'activity' => 'required',
            'note' => 'required',
        ]);

        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.orders.index');
        }

        $activity = new OrderActivity;
        $activity->order_id = $order->id;
        $activity->user_id = $user->id;
        $activity->group = request('group');
        $activity->activity = request('activity');
        $activity->note = request('note');
        $activity->save();

        flash('Order activity saved');
        return redirect()->back();
    }

    public function complainCommentsStore(Request $request, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'activity' => 'required',
            'note' => 'required',
        ]);

        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.orders.index');
        }

        $activity = new OrderActivity;
        $activity->order_id = $order->id;
        $activity->user_id = $user->id;
        $activity->group = request('group');
        $activity->activity = request('activity');
        $activity->note = request('note');
        $activity->save();

        $file = request('file');
        if ($file) {
            $filename = 'order-complain-attachment';
            $filepath = upload_file($file, 'uploads', $filename);
            $activity->has_attachment = true;
            $activity->save();
            $activity->setMeta('attachments', $filepath);
        }

        flash('Order activity saved');
        return redirect()->back();
    }

    public function documentsStore(Request $request, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'activity' => 'required',
            'file' => 'required|file|mimes:jpeg,jpg,png,pdf,xls,xlsx,doc,docx|max:15000',
        ]);

        // return request()->all();

        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.orders.index');
        }

        $activity = new OrderActivity;
        $activity->order_id = $order->id;
        $activity->user_id = $user->id;
        $activity->group = request('group');
        $activity->activity = request('activity');
        $activity->note = request('note');
        $activity->save();

        $file = request('file');
        if ($file) {
            $filename = 'order-document-attachment';
            $filepath = upload_file($file, 'uploads', $filename);
            $activity->has_attachment = true;
            $activity->save();
            $activity->setMeta('attachments', $filepath);
        }

        flash('Order activity saved');
        return redirect()->back();
    }

    public function paymentsStore(Request $request, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'activity' => 'required',
            'total_payment' => 'required|integer|min:0',
            'file' => 'required|file|mimes:jpeg,jpg,png,pdf,xls,xlsx,doc,docx|max:15000',
        ]);

        // return request()->all();

        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.orders.index');
        }

        $activity = new OrderActivity;
        $activity->order_id = $order->id;
        $activity->user_id = $user->id;
        $activity->group = request('group');
        $activity->activity = request('activity');
        $activity->note = request('note');
        $activity->save();

        $file = request('file');
        if ($file) {
            $filename = 'order-document-attachment';
            $filepath = upload_file($file, 'uploads', $filename);
            $activity->has_attachment = true;
            $activity->save();
            $activity->setMeta('attachments', $filepath);
        }

        $total_payment = request('total_payment');
        $total_payment = $total_payment + 0;
        $activity->setMeta('total_payment', $total_payment);

        flash('Order activity saved');
        return redirect()->back();
    }
}
