<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Order;
use App\Models\Modules\Marketplaces\OrderStatus;
use App\Models\Modules\Website\WebsiteSettings;
use PDF;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /** @var \App\User|null $user */
        $user = auth()->user();

        $groupedOrderStatuses = OrderStatus::getGroupedStatus();
        $groupedOrderStatuses = Order::addOrderCountToUserOrderStatuses($user->id, $groupedOrderStatuses);
        $orderStatusId = OrderStatus::where('group', 'order_process')->where('key', 'pending')->first()->id;
        if (request('order_status_id')) {
            $orderStatusId = request('order_status_id');
        }

        $selectedOrderStatus = OrderStatus::find($orderStatusId);

        $status = 'pending';
        $orderStatus = request('status') ? request('status') : $status;
        $statuses = Order::getUserOrderStatuses($user->id);;

        // $orders = $user->orders()->filter($orderStatus)->with('store')->orderBy('updated_at', 'desc')->paginate(20);
        $orders = $user->orders()
            ->where('order_status_id', $orderStatusId)
            ->where('school_id', null)
            ->orderBy('updated_at', 'desc')->paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.users.orders.index', compact(
            'user', 'orders', 'orderStatus', 'statuses',
            'groupedOrderStatuses', 'orderStatusId', 'selectedOrderStatus',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.orders.index');
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();
        $histories = $order->histories()->with('meta')->latest()->get();

        $groupedOrderStatuses = OrderStatus::get()->groupBy('group');

        $activities['negotiation_comments'] = $order->activities()
            ->where('group', 'negotiation')
            // ->where('activity', 'negotiation comment')
            ->with('meta', 'user')
            ->get();
        $activities['complain_comments'] = $order->activities()
            ->where('group', 'complain')
            ->where('activity', 'complain comment')
            ->with('meta', 'user')
            ->get();
        $activities['documents'] = $order->activities()
            ->where('group', 'document')
            ->with('meta', 'user', 'order')
            ->get();
        $activities['payments'] = $order->activities()
            ->where('group', 'payment')
            ->with('meta', 'user')
            ->get();
        // return $activities['payments'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();
        $dropdown['yes_no'] = [0=>'No', 1=>'Yes'];

        return view('modules.marketplaces.backend.users.orders.show', compact(
            'user', 'store', 'marketplace', 'order', 'products', 'orderMetas', 'histories',
            'activities',
            'groupedOrderStatuses',
            'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function downloadPdfv1(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.orders.index');
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();
        // $histories = $order->histories()->with('meta')->latest()->get();

        // $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        // return view('modules.marketplaces.backend.users.orders.download-pdf', compact(
        //     'user', 'store', 'order', 'products', 'orderMetas', 'histories',
        //     'websiteSettings'
        // ));

        $pdf = PDF::loadView('modules.marketplaces.backend.users.orders.download-pdf', compact(
            'user', 'store', 'marketplace', 'order', 'products', 'orderMetas', 'histories',
            'websiteSettings'
        ));

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        return $pdf->download($filename);
    }

    public function downloadPdf(Order $order)
    {
        $user = auth()->user();
        if (!$order->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.orders.index');
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();
        // $histories = $order->histories()->with('meta')->latest()->get();

        // $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        $html = view('modules.marketplaces.backend.users.orders.download-pdf', compact(
            'user', 'store', 'order', 'products', 'orderMetas', 'histories',
            'websiteSettings'
        ))->render();
        // return $html;

        // $pdf = \App::make('dompdf.wrapper');
        // $pdf->loadHTML($html);
        // return $pdf->stream();

        $pdf = PDF::loadView('modules.marketplaces.backend.users.orders.download-pdf', compact(
            'user', 'store', 'marketplace', 'order', 'products', 'orderMetas', 'histories',
            'websiteSettings'
        ));

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        return $pdf->download($filename);
    }
}
