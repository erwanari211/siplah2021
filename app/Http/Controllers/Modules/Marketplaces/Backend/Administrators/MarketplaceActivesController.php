<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Administrators;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use Mail;
use App\Mail\Marketplaces\MarketplaceApprovedMail;
use App\Mail\Marketplaces\MarketplaceDisapprovedMail;

class MarketplaceActivesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplace->active = true;
        $marketplace->save();

        $marketplaceOwner = $marketplace->user;

        $emailData = compact('marketplaceOwner', 'marketplace');
        Mail::to($marketplaceOwner->email)
            ->send(new MarketplaceApprovedMail($emailData));

        flash('Marketplace approved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplace->active = false;
        $marketplace->save();

        $marketplaceOwner = $marketplace->user;

        $emailData = compact('marketplaceOwner', 'marketplace');
        Mail::to($marketplaceOwner->email)
            ->send(new MarketplaceDisapprovedMail($emailData));

        flash('Marketplace disapproved');
        return redirect()->back();
    }
}
