<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection;
use Illuminate\Http\Request;
use App\Models\Modules\Website\WebsiteSettings;

class WebsiteMarketplaceCollectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplaceCollections = WebsiteMarketplaceCollection::paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.administrators.marketplace-collections.index', compact(
            'marketplaceCollections',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = ['normal'=>'Normal'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.administrators.marketplace-collections.create', compact(
            'user', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'active' => 'required',
            'type' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $collection = new WebsiteMarketplaceCollection;
        $collection->name = request('name');
        $collection->display_name = request('display_name');
        $collection->active = request('active');
        $collection->type = request('type');
        $collection->sort_order = request('sort_order');
        $collection->save();

        flash('Collection saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteMarketplaceCollection  $websiteMarketplaceCollection
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteMarketplaceCollection $marketplaceCollection)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = ['normal'=>'Normal'];
        $details = $marketplaceCollection->details()->sort()->with('marketplace')->get();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.administrators.marketplace-collections.show', compact(
            'user', 'marketplaceCollection', 'dropdown', 'details',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteMarketplaceCollection  $websiteMarketplaceCollection
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteMarketplaceCollection $marketplaceCollection)
    {
        // return $marketplaceCollection;
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = ['normal'=>'Normal'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.administrators.marketplace-collections.edit', compact(
            'user', 'marketplaceCollection', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Website\WebsiteMarketplaceCollection  $websiteMarketplaceCollection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteMarketplaceCollection $marketplaceCollection)
    {
        request()->validate([
            'name' => 'required',
            'active' => 'required',
            'type' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $collection = $marketplaceCollection;
        $collection->name = request('name');
        $collection->display_name = request('display_name');
        $collection->active = request('active');
        $collection->type = request('type');
        $collection->sort_order = request('sort_order');
        $collection->save();

        flash('Collection updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Website\WebsiteMarketplaceCollection  $websiteMarketplaceCollection
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteMarketplaceCollection $marketplaceCollection)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplaceCollection->details()->delete();
        $marketplaceCollection->delete();

        flash('Collection deleted');
        return redirect()->back();
    }
}
