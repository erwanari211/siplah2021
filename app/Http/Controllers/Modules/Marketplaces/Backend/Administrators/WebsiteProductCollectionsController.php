<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\WebsiteProductCollection;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Website\WebsiteSettings;

class WebsiteProductCollectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $productWithoutUniqueCode = Product::where(function($query){
            $query->where('unique_code', null);
            $query->orWhere('unique_code', '');
        })->get();

        if (count($productWithoutUniqueCode)) {
            Product::fillUniqueCode();
        }

        $productCollections = WebsiteProductCollection::paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.administrators.product-collections.index', compact(
            'productCollections',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = ['normal'=>'Normal'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.administrators.product-collections.create', compact(
            'user', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'active' => 'required',
            'link' => 'nullable|url',
            'type' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $collection = new WebsiteProductCollection;
        $collection->name = request('name');
        $collection->display_name = request('display_name');
        $collection->link = request('link');
        $collection->active = request('active');
        $collection->type = request('type');
        $collection->sort_order = request('sort_order');
        $collection->save();

        flash('Collection saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\WebsiteProductCollection  $websiteProductCollection
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteProductCollection $productCollection)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = ['normal'=>'Normal'];
        $details = $productCollection->details()->sort()->with('product', 'product.store', 'product.store.marketplace')->get();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.administrators.product-collections.show', compact(
            'user', 'productCollection', 'dropdown', 'details',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\WebsiteProductCollection  $websiteProductCollection
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteProductCollection $productCollection)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = ['normal'=>'Normal'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.administrators.product-collections.edit', compact(
            'user', 'productCollection', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\WebsiteProductCollection  $websiteProductCollection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteProductCollection $productCollection)
    {
        request()->validate([
            'name' => 'required',
            'active' => 'required',
            'link' => 'nullable|url',
            'type' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $collection = $productCollection;
        $collection->name = request('name');
        $collection->display_name = request('display_name');
        $collection->link = request('link');
        $collection->active = request('active');
        $collection->type = request('type');
        $collection->sort_order = request('sort_order');
        $collection->save();

        flash('Collection updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\WebsiteProductCollection  $websiteProductCollection
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteProductCollection $productCollection)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $productCollection->details()->delete();
        $productCollection->delete();

        flash('Collection deleted');
        return redirect()->back();
    }

    public function showAjax($id)
    {
        $result['success'] = false;
        $result['data'] = null;
        $result['message'] = null;

        $productCollection = WebsiteProductCollection::with('details')->find($id);
        if (!$productCollection) {
            $result['message'] = 'Product Collection not found';
            return $result;
        }

        $limit = request('limit');

        $details = $productCollection->details()
            ->sort()
            ->with('product', 'product.store', 'product.store.marketplace')
            ->get();

        $products = $details->pluck('product');
        $products = collect($products)->shuffle();
        if ($limit) {
            $products = $products->take($limit);
        }

        foreach ($products as $product) {
            $product['productUrl'] = $product->productUrl;
            $product['formattedPrice'] = "Rp.".formatNumber($product->price);
            $product['imageUrl'] = $product->imageUrl;
        }

        $result['success'] = true;
        $result['data'] = compact('products');

        return $result;
    }
}
