<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\WebsiteProductCollection;

class WebsiteProductCollectionDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'collection_id' => 'required',
            'product_id' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $productId = request('product_id');
        $product = Product::findOrFail($productId);

        $collectionId = request('collection_id');
        $collection = WebsiteProductCollection::findOrFail($collectionId);

        WebsiteProductCollectionDetail::updateOrCreate([
            'website_product_collection_id' => $collection->id,
            'product_id' => $product->id,
        ], [
            'sort_order' => request('sort_order')
        ]);

        flash('Product added to collection');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail  $websiteProductCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteProductCollectionDetail $websiteProductCollectionDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail  $websiteProductCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteProductCollectionDetail $websiteProductCollectionDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail  $websiteProductCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteProductCollectionDetail $websiteProductCollectionDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail  $websiteProductCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteProductCollection $productCollection, Product $product)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        WebsiteProductCollectionDetail::where([
            'website_product_collection_id' => $productCollection->id,
            'product_id' => $product->id,
        ])->delete();

        flash('Product removed from collection');
        return redirect()->back();
    }
}
