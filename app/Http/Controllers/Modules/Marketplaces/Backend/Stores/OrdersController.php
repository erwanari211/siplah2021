<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Order;
use App\Models\Modules\Marketplaces\OrderStatus;
use App\Models\Modules\Website\WebsiteSettings;
use PDF;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $store->updateLastOnline();

        $groupedOrderStatuses = OrderStatus::getGroupedStatus();
        $groupedOrderStatuses = Order::addOrderCountToStoreOrderStatuses($store->id, $groupedOrderStatuses);
        $orderStatusId = OrderStatus::where('group', 'order_process')->where('key', 'pending')->first()->id;
        if (request('order_status_id')) {
            $orderStatusId = request('order_status_id');
        }
        $selectedOrderStatus = OrderStatus::find($orderStatusId);

        $status = 'pending';
        $orderStatus = request('status') ? request('status') : $status;
        $statuses = Order::getStoreOrderStatuses($store->id);
        // $orders = $store->orders()->filter($orderStatus)->orderBy('updated_at', 'desc')->paginate(10);
        $orders = $store->orders()->where('order_status_id', $orderStatusId)->orderBy('updated_at', 'desc')->paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.orders.index', compact(
            'user', 'store', 'orders', 'orderStatus', 'statuses',
            'groupedOrderStatuses', 'orderStatusId', 'selectedOrderStatus',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store, Order $order)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        $store->updateLastOnline();

        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();
        $histories = $order->histories()->with('meta')->latest()->get();

        $dropdown['order_statuses'] = OrderStatus::orderBy('value')->find([1,2,3,4,5,6,7])->pluck('key', 'id');
        $dropdown['yes_no'] = [0=>'No', 1=>'Yes'];

        $groupedOrderStatuses = OrderStatus::get()->groupBy('group');

        $activities['negotiation_comments'] = $order->activities()
            ->where('group', 'negotiation')
            // ->where('activity', 'negotiation comment')
            ->with('meta', 'user')
            ->get();
        // return $activities['negotiation_comments'];
        $dropdown['negotiation_price_types'] = [
            'change shipping cost' => 'Change shipping cost',
            'adjustment price' => 'Adjustment price',
            'change product price' => 'Change product price',
        ];
        $dropdownProducts = $products->pluck('product_name', 'id');
        $dropdown['order_products'] = $dropdownProducts;

        $activities['complain_comments'] = $order->activities()
            ->where('group', 'complain')
            ->where('activity', 'complain comment')
            ->with('meta', 'user')
            ->get();
        $activities['documents'] = $order->activities()
            ->where('group', 'document')
            ->with('meta', 'user', 'order')
            ->get();
        $activities['payments'] = $order->activities()
            ->where('group', 'payment')
            ->with('meta', 'user')
            ->get();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.orders.show', compact(
            'user', 'store', 'order', 'products', 'orderMetas', 'histories', 'dropdown',
            'activities',
            'groupedOrderStatuses',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function downloadPdf(Store $store, Order $order)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();
        // $histories = $order->histories()->with('meta')->latest()->get();

        // $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        // return view('modules.marketplaces.backend.users.orders.download-pdf', compact(
        //     'user', 'store', 'order', 'products', 'orderMetas', 'histories',
        //     'websiteSettings'
        // ));

        $pdf = PDF::loadView('modules.marketplaces.backend.stores.orders.download-pdf', compact(
            'user', 'store', 'marketplace', 'order', 'products', 'orderMetas', 'histories',
            'websiteSettings'
        ));

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        return $pdf->download($filename);
    }
}
