<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Order;
use App\Models\Modules\Marketplaces\OrderStatus;
use App\Models\Modules\Marketplaces\GroupNotification;

class OrderStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'status' => 'required',

            'notify' => 'required|boolean',
            'note' => 'nullable',
            'file' => 'nullable|image|max:4000',
        ]);

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        // return request()->all();

        $group = request('group');
        $status = request('status');
        $historyData = request()->only(['notify', 'note', 'file']);
        $order->updateGroupedOrderStatus($group, $status, $historyData);

        if ($group == 'order_confirmation') {
            if ($status == 'not_confirmed') {
                $school = $order->school;
                if($school){
                    $notificationData['data']['new'] = $order->getOriginal();
                    $notificationData['description'] = 'Pesanan telah ditolak penyedia';
                    GroupNotification::addNotification($school, 'order_rejected', $notificationData);
                }
            }

            if ($status == 'confirmed') {
                $school = $order->school;
                if ($school) {
                    $notificationData['data']['new'] = $order->getOriginal();
                    $notificationData['description'] = 'Pesanan telah diterima penyedia';
                    GroupNotification::addNotification($school, 'order_confirmed', $notificationData);
                }
            }
        }

        if ($group == 'packing') {
            if ($status == 'processing') {
                $school = $order->school;
                if ($school) {
                    $notificationData['data']['new'] = $order->getOriginal();
                    $notificationData['description'] = 'Pesanan sedang diproses penyedia';
                    GroupNotification::addNotification($school, 'order_packing_processing', $notificationData);
                }
            }
        }

        if ($group == 'shipping') {
            if ($status == 'shipping') {
                $school = $order->school;
                if ($school) {
                    $notificationData['data']['new'] = $order->getOriginal();
                    $notificationData['description'] = 'Pesanan sedang dalam proses pengiriman';
                    GroupNotification::addNotification($school, 'order_shipping_shipping', $notificationData);
                }
            }

            if ($status == 'shipped') {
                $school = $order->school;
                if ($school) {
                    $notificationData['data']['new'] = $order->getOriginal();
                    $notificationData['description'] = 'Pesanan sudah terkirim';
                    GroupNotification::addNotification($school, 'order_shipping_shipped', $notificationData);
                }
            }
        }

        $store->staffUpdatedOrder($order);

        flash('Order updated');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
