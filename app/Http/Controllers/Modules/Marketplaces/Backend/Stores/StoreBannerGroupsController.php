<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreBannerGroup;
use App\Models\Modules\Website\WebsiteSettings;

class StoreBannerGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $bannerGroups = $store->bannerGroups()->latest()->paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.banner-groups.index', compact(
            'store', 'bannerGroups',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = ['normal'=>'Normal', 'slider'=>'Slider'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.banner-groups.create', compact(
            'user', 'store', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'name' => 'required',
            'active' => 'required',
            'type' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $group = new StoreBannerGroup;
        $group->name = request('name');
        $group->display_name = request('display_name');
        $group->active = request('active');
        $group->type = request('type');
        $group->sort_order = request('sort_order');
        $group->store_id = $store->id;
        $group->save();

        flash('Banner group saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store, StoreBannerGroup $bannerGroup)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$bannerGroup->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.banner-groups.index', $store->id);
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = ['normal'=>'Normal', 'slider'=>'Slider'];

        $bannerGroup->details()->with('storeBanner')->doesntHave('storeBanner')->delete();
        $details = $bannerGroup->details()->with('storeBanner')->has('storeBanner')->get();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.banner-groups.show', compact(
            'user', 'store', 'bannerGroup', 'dropdown', 'details',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, StoreBannerGroup $bannerGroup)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$bannerGroup->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.banner-groups.index', $store->id);
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = ['normal'=>'Normal', 'slider'=>'Slider'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.banner-groups.edit', compact(
            'user', 'store', 'bannerGroup', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, StoreBannerGroup $bannerGroup)
    {
        request()->validate([
            'name' => 'required',
            'active' => 'required',
            'type' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$bannerGroup->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.banner-groups.index', $store->id);
        }

        $group = $bannerGroup;
        $group->name = request('name');
        $group->display_name = request('display_name');
        $group->active = request('active');
        $group->type = request('type');
        $group->sort_order = request('sort_order');
        $group->save();

        flash('Banner updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, StoreBannerGroup $bannerGroup)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$bannerGroup->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.banner-groups.index', $store->id);
        }

        $bannerGroup->details()->delete();
        $bannerGroup->delete();

        flash('Banner group deleted');
        return redirect()->back();
    }
}
