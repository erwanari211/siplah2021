<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\FromCollectionWithViewExport;
use App\Exports\FromCollectionWithViewMultipleSheetsExport;

class ProductsExportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download(Store $store)
    {
        $start = request('start');
        $end = request('end');
        $skip = ($start <= 0) ? 0 : $start - 1;
        if ($skip > $end) {
            $skip = $end;
        }
        $limit = $end - $start + 1;
        // return compact('start', 'end', 'skip', 'limit');

        $products = $store->products()->with('attributes', 'marketplaceCategories', 'storeCategories')
            // ->latest()
            ->skip($skip)
            ->take($limit)
            ->get();
        // return $products->count();
        // return ($products);

        $sheetData['products'] = [];
        $sheetData['product_attributes'] = [];

        $no = $skip + 1;
        foreach ($products as $item) {
            $product['index'] = $no;
            $product['id'] = $item['id'];
            $product['unique_code'] = $item['unique_code'];
            $product['sku'] = $item['sku'];
            $product['name'] = $item['name'];
            $product['description'] = $item['description'];
            $product['price'] = $item['price'];
            $product['qty'] = $item['qty'];
            $product['image'] = null;
            $product['weight'] = $item['weight'];
            $product['active'] = $item['active'];
            $product['categories'] = $item['marketplaceCategories'];
            $product['collections'] = $item['storeCategories'];
            $sheetData['products'][] = $product;

            if (isset($item['attributes'])) {
                foreach ($item['attributes'] as $itemAttribute) {
                    if ($item['unique_code']) {
                        $attribute['product_id'] = $item['id'];
                        $attribute['unique_code'] = $item['unique_code'];
                        $attribute['sku'] = $item['sku'];
                        $attribute['group'] = $itemAttribute['group'];
                        $attribute['name'] = $itemAttribute['key'];
                        $attribute['value'] = $itemAttribute['value'];
                        $attribute['sort_order'] = $itemAttribute['sort_order'];
                        $sheetData['product_attributes'][] = $attribute;
                    }
                }
            }

            $no++;
        }
        // return $sheetData['products'];

        $products = $sheetData['products'];
        $attributes = $sheetData['product_attributes'];

        $categories = $store->marketplace->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['childs' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $sheetData['categories'] = $categories;

        $collections = $store->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['childs' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $sheetData['collections'] = $collections;

        // return view('modules.marketplaces.backend.stores.products.export.products', compact('products'));
        // return view('modules.marketplaces.backend.stores.products.export.product_attributes', compact('attributes'));
        // return view('modules.marketplaces.backend.stores.products.export.marketplace-categories', compact('categories'));
        // return view('modules.marketplaces.backend.stores.products.export.store-collections', compact('collections'));

        $excelData = [];
        $excelData['products'] = [
            'sheetname' => 'products',
            'view' => 'modules.marketplaces.backend.stores.products.export.products',
            'data' => compact('products'),
        ];
        $excelData['product_attributes'] = [
            'sheetname' => 'product_attributes',
            'view' => 'modules.marketplaces.backend.stores.products.export.product_attributes',
            'data' => compact('attributes'),
        ];
        $excelData['marketplace_categories'] = [
            'sheetname' => 'categories',
            'view' => 'modules.marketplaces.backend.stores.products.export.marketplace-categories',
            'data' => compact('categories'),
        ];
        $excelData['store_collections'] = [
            'sheetname' => 'collections',
            'view' => 'modules.marketplaces.backend.stores.products.export.store-collections',
            'data' => compact('collections'),
        ];

        $date = date('Ymd');
        $randomString = str_random(8).'-'.time();
        $outputName = $date.'-export-product-data-'.$randomString.'.xlsx';
        return Excel::download(new FromCollectionWithViewMultipleSheetsExport($excelData), $outputName);
    }
}
