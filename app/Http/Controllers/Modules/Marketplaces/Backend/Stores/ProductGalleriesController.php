<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\ProductGallery;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Website\WebsiteSettings;

class ProductGalleriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['groups'] = [
            'gallery'=>'Gallery',
            'other'=>'Other',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.galleries.create', compact(
            'store', 'product', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store, Product $product)
    {
        request()->validate([
            'group' => 'required',
            // 'name' => 'required',
            'sort_order' => 'required|integer|min:1',
            'file' => 'required',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $file = request('file');
        if ($file) {
            $filename = 'product-gallery';
            $filepath = upload_file($file, 'uploads', $filename);

            $gallery = new ProductGallery;
            $gallery->product_id = $product->id;
            $gallery->group = request('group');
            $gallery->name = request('name');
            $gallery->description = request('description');
            $gallery->sort_order = request('sort_order');
            $gallery->active = request('active');

            $gallery->type = $file->getClientMimeType();
            $gallery->url = $filepath;
            $gallery->save();

            flash('Product gallery saved');
        }


        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\ProductGallery  $productGallery
     * @return \Illuminate\Http\Response
     */
    public function show(ProductGallery $productGallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\ProductGallery  $productGallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, Product $product, ProductGallery $gallery)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        if (!$gallery->isOwnedByProduct($product->id)) {
            flash('Product Attribute not found')->error();
            return redirect()->route('users.stores.products.show', [$store->id, $product->id]);
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['groups'] = [
            'gallery'=>'Gallery',
            'other'=>'Other',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.galleries.edit', compact(
            'store', 'product', 'gallery', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\ProductGallery  $productGallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, Product $product, ProductGallery $gallery)
    {
        request()->validate([
            'group' => 'required',
            // 'name' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        if (!$gallery->isOwnedByProduct($product->id)) {
            flash('Product Attribute not found')->error();
            return redirect()->route('users.stores.products.show', [$store->id, $product->id]);
        }

        $gallery->group = request('group');
        $gallery->name = request('name');
        $gallery->description = request('description');
        $gallery->sort_order = request('sort_order');
        $gallery->active = request('active');
        $gallery->save();

        flash('Product gallery updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\ProductGallery  $productGallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, Product $product, ProductGallery $gallery)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        if (!$gallery->isOwnedByProduct($product->id)) {
            flash('Product Attribute not found')->error();
            return redirect()->route('users.stores.products.show', [$store->id, $product->id]);
        }

        $gallery->delete();

        flash('Product gallery deleted');
        return redirect()->back();
    }
}
