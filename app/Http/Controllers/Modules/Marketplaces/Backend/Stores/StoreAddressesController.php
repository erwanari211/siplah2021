<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreAddress;
use App\Models\ZoneOngkirProvince;
use App\Models\ZoneOngkirCity;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict;

class StoreAddressesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }
        $addresses = $store->addresses()->with('province', 'city', 'district')->paginate(10);
        $store->has_addresses = $store->addresses()->count() ? true : false;
        $store->has_default_addresses = $store->addresses()->defaultAddress()->count() ? true : false;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.addresses.index', compact(
            'user', 'store', 'addresses',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        /*
        $provinces = ZoneOngkirProvince::getProvinceWithCities();
        $provinceId = old('province_id');
        $dropdown['provinces'] = ZoneOngkirProvince::pluck('province', 'province_id');
        $dropdown['cities'] =  ZoneOngkirCity::getDropdown($provinceId);
        */

        // \Cache::forget('provincesWithDetails');
        $provincesWithDetails = IndonesiaProvince::getProvincesWithDetails();

        $dropdown['provinces'] = IndonesiaProvince::getDropdown();

        $provinceId = old('province_id');
        $dropdown['cities'] = IndonesiaCity::getDropdown($provinceId);

        $cityId = old('city_id');
        $dropdown['districts'] = IndonesiaDistrict::getDropdown($cityId);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.addresses.create', compact(
            'user', 'store', 'provinces', 'dropdown',
            'provincesWithDetails',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'label' => 'required',
            'full_name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'postcode' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $address = new StoreAddress;
        $address->label = request('label');
        $address->full_name = request('full_name');
        $address->company = request('company');
        $address->address = request('address');
        $address->province_id = request('province_id');
        $address->city_id = request('city_id');
        $address->district_id = request('district_id');
        $address->phone = request('phone');
        $address->postcode = request('postcode');

        $store->addresses()->save($address);

        $countStoreAddresses = StoreAddress::where('store_id', $store->id)->count();
        if ($countStoreAddresses == 1) {
            $address->default_address = true;
            $address->save();

            $store->saveDefaultAddress();
        }

        flash('Address saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, StoreAddress $address)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$address->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.settings.addresses.index', $store->id);
        }

        /*
        $provinces = ZoneOngkirProvince::getProvinceWithCities();
        $provinceId = old('province_id');
        $dropdown['provinces'] = ZoneOngkirProvince::pluck('province', 'province_id');
        $dropdown['cities'] =  ZoneOngkirCity::getDropdown($provinceId);
        */

        $provincesWithDetails = IndonesiaProvince::getProvincesWithDetails();

        $dropdown['provinces'] = IndonesiaProvince::getDropdown();

        $provinceId = old('province_id', $address->province_id);
        $dropdown['cities'] = IndonesiaCity::getDropdown($provinceId);

        $cityId = old('city_id', $address->city_id);
        $dropdown['districts'] = IndonesiaDistrict::getDropdown($cityId);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.addresses.edit', compact(
            'user', 'store', 'provinces', 'dropdown', 'address',
            'provincesWithDetails',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, StoreAddress $address)
    {
        request()->validate([
            'label' => 'required',
            'full_name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'postcode' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$address->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.settings.addresses.index', $store->id);
        }

        $address->label = request('label');
        $address->full_name = request('full_name');
        $address->company = request('company');
        $address->address = request('address');
        $address->province_id = request('province_id');
        $address->city_id = request('city_id');
        $address->district_id = request('district_id');
        $address->phone = request('phone');
        $address->postcode = request('postcode');
        $address->save();

        if ($address->default_address) {
            $store->saveDefaultAddress();
        }

        flash('Address saved');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, StoreAddress $address)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$address->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.settings.addresses.index', $store->id);
        }

        if ($address->default_address) {
            flash('Cannot delete default address')->error();
            return redirect()->back();
        }

        $address->delete();

        flash('Address deleted');
        return redirect()->back();
    }
}
