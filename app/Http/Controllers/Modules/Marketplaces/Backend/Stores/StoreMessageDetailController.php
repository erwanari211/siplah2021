<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\StoreMessageDetail;
use Illuminate\Http\Request;

class StoreMessageDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\StoreMessageDetail  $storeMessageDetail
     * @return \Illuminate\Http\Response
     */
    public function show(StoreMessageDetail $storeMessageDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\StoreMessageDetail  $storeMessageDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(StoreMessageDetail $storeMessageDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\StoreMessageDetail  $storeMessageDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StoreMessageDetail $storeMessageDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\StoreMessageDetail  $storeMessageDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(StoreMessageDetail $storeMessageDetail)
    {
        //
    }
}
