<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Marketplaces\GroupNotification;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        Product::fillUniqueCode();

        $store->updateLastOnline();

        $approvedStatuses = [
            'pending' => 'Pending',
            'approved' => 'Approved',
            'rejected' => 'Rejected',
        ];

        $filters['approved_status'] = request('approved_status') ? request('approved_status') : null;
        $filters['active'] = request('active') ? request('active') : 'true';
        $filters['status'] = request('status') ? request('status') : null;
        $collectionId = request('collection_id');
        $categoryId = request('category_id');
        $products = $store->products()->with('marketplaceCategories', 'storeCategories', 'meta');

        $name = request('name');
        $sku = request('sku');
        if ($name) {
            $products = $products->where('name', 'LIKE', "%{$name}%");
            $filters['name'] = request('name') ? request('name') : null;
        }
        if ($sku) {
            $products = $products->where('sku', 'LIKE', "%{$sku}%");
            $filters['sku'] = request('sku') ? request('sku') : null;
        }
        if ($categoryId) {
            $products->whereHas('marketplaceCategories', function($q) use ($categoryId){
                $q->where('marketplace_categories.id', $categoryId);
            });
        }
        if ($collectionId) {
            $products->whereHas('storeCategories', function($q) use ($collectionId){
                $q->where('store_categories.id', $collectionId);
            });
        }

        $products = $products
            ->filter($filters)
            ->latest()
            ->paginate(20);

        $collections =  $store->categories()->orderBy('name')->pluck('name', 'id');
        $dropdown['collections'] = $collections;

        $categories =  $store->marketplace->categories()->orderBy('name')->pluck('name', 'id');
        $dropdown['categories'] = $categories;

        $marketplace = $store->marketplace;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.index', compact(
            'user', 'store', 'marketplace', 'products', 'filters', 'dropdown',
            'approvedStatuses',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $store->updateLastOnline();

        $marketplace = $store->marketplace;
        // $dropdown['categories'] = $marketplace->categories()->pluck('name', 'id');
        $marketplaceCategories = $marketplace->categories()
            ->where('parent_id', null)
            ->orderBy('name')
            ->get();

        $categories = [];
        foreach ($marketplaceCategories as $key => $marketplaceCategory) {
            $marketplaceId = $marketplaceCategory['id'];
            $marketplaceName = $marketplaceCategory['name'];
            $categories[$marketplaceId] = $marketplaceName;
        }
        $dropdown['marketplaceCategories'] = $categories;
        // return $dropdown['marketplaceCategories'];

        $storeCategories = $store->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['children' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $collections = [];

        foreach ($storeCategories as $key => $storeCategory) {
            $marketplaceId = $storeCategory['id'];
            $marketplaceName = $storeCategory['name'];
            $collections[$marketplaceId] = $marketplaceName;
            if (count($storeCategory->children)) {
                foreach ($storeCategory->children as $marketplaceSubCategory) {
                    $marketplaceId = $marketplaceSubCategory['id'];
                    $marketplaceName = '- - '.$marketplaceSubCategory['name'];
                    $collections[$marketplaceId] = $marketplaceName;
                }
            }
        }
        $dropdown['storeCategories'] = $collections;

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['statuses'] = ['active'=>'Active', 'inactive'=>'Inactive', 'archived'=>'Archived'];
        $dropdown['types'] = [
            'product'=>'Product',
            'grouped product'=>'Grouped Product',
            'digital product' => 'Digital Product',
            'service' => 'Service',
        ];

        $dropdown['product_status_ketersediaan'] = [
            'ready stock' => 'Ready Stock',
            'preorder' => 'Preorder',
        ];

        $dropdown['kelasUsaha'] = [
            'Mikro' => 'Mikro',
            'Kecil' => 'Kecil',
            'Menengah' => 'Menengah',
            'Koperasi' => 'Koperasi',
            'Non UMKM' => 'Non UMKM',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.create', compact(
            'user', 'store', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'price' => 'required|integer|min:0',
            'category' => 'required',
            'qty' => 'required|integer|min:0',
            'weight' => 'required|integer|min:0',
            'active' => 'required|integer',
            'image' => 'image|max:4000',
            'type' => 'required',
            'is_taxed' => 'required|boolean',
        ]);


        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $product = new Product;
        $product->name = request('name');
        $slug = request('slug') ? str_slug(request('slug')) : str_slug(request('name'));
        $product->slug = Product::getUniqueSlug($slug);
        $product->unique_code = Product::randomUniqueCode();
        $product->sku = request('sku');
        $product->description = request('description');
        $product->price = request('price');
        $product->qty = request('qty');
        $product->weight = request('weight');
        $product->type = request('type');
        $product->active = request('active');
        $product->status = request('status');
        $product->is_taxed = request('is_taxed');
        $product->user_id = $user->id;
        $product->store_id = $store->id;
        $product->save();

        $image = request('image');
        if ($image) {
            $filename = 'products';
            $filepath = upload_file($image, 'uploads', $filename);
            $product->image = $filepath;
            $product->save();
        }

        $category = request('category');
        $category = array_filter($category);
        if ($category) {
            $product->marketplaceCategories()->sync($category);
        }

        $collection = request('collection');
        if ($collection) {
            $collection = array_filter($collection);
            $product->storeCategories()->sync($collection);
        }

        $metaData = request('meta_data');
        foreach ($metaData as $key => $value) {
            $product->setMeta($key, $value);
        }

        $type = request('type');
        $productTypes = ['product', 'grouped product', 'digital product'];
        $serviceTypes = ['service'];
        if (in_array($type, $productTypes)) {
            $is_msme_product = request('meta_data.product_is_msme_product');
            $product->is_msme_product = in_array($is_msme_product, ['Mikro', 'Kecil', 'Menengah']);
            $product->save();
        }
        if (in_array($type, $serviceTypes)) {
            $is_msme_product = request('meta_data.service_is_msme_product');
            $product->is_msme_product = in_array($is_msme_product, ['Mikro', 'Kecil', 'Menengah']);
            $product->save();
        }

        $notificationData['data']['new']['product'] = $product->getOriginal();
        $notificationData['data']['new']['store'] = $store->getOriginal();
        $notificationData['description'] = 'Produk baru telah dibuat';
        $marketplace = $store->marketplace;
        GroupNotification::addNotification($marketplace, 'product_created', $notificationData);

        $store->staffCreatedNewProduct();

        flash('Product saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $store->updateLastOnline();

        $marketplace = $store->marketplace;
        // $dropdown['categories'] = $marketplace->categories()->pluck('name', 'id');
        /*
        $marketplaceCategories = $marketplace->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['children' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $categories = [];
        foreach ($marketplaceCategories as $key => $marketplaceCategory) {
            $marketplaceId = $marketplaceCategory['id'];
            $marketplaceName = $marketplaceCategory['name'];
            $categories[$marketplaceId] = $marketplaceName;
            if (count($marketplaceCategory->children)) {
                foreach ($marketplaceCategory->children as $marketplaceSubCategory) {
                    $marketplaceId = $marketplaceSubCategory['id'];
                    $marketplaceName = '- - '.$marketplaceSubCategory['name'];
                    $categories[$marketplaceId] = $marketplaceName;
                }
            }
        }
        $dropdown['marketplaceCategories'] = $categories;
        */

        $productMarketplaceCategories = $product->marketplaceCategories()->pluck('marketplace_category_id');
        $marketplaceCategories = $marketplace->categories()
            ->whereIn('parent_id', $productMarketplaceCategories)
            ->orderBy('name')
            ->get();

        $oldDropdowns = [];
        foreach ($productMarketplaceCategories as $productMarketplaceCategory) {
            $id = $productMarketplaceCategory;
            $oldDropdowns['data'][$id]['id'] = $id;
            $oldDropdowns['data'][$id]['data'] = [];
        }
        foreach ($marketplaceCategories as $marketplaceCategory) {
            $id = $marketplaceCategory->parent_id;
            $oldDropdowns['data'][$id]['data'][] = $marketplaceCategory;
        }
        // return $oldDropdowns;

        $marketplaceCategories = $marketplace->categories()
            ->where('parent_id', null)
            ->orderBy('name')
            ->get();

        $categories = [];
        foreach ($marketplaceCategories as $key => $marketplaceCategory) {
            $marketplaceId = $marketplaceCategory['id'];
            $marketplaceName = $marketplaceCategory['name'];
            $categories[$marketplaceId] = $marketplaceName;
        }
        $dropdown['marketplaceCategories'] = $categories;
        // return $dropdown['marketplaceCategories'];

        $storeCategories = $store->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['children' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $collections = [];
        foreach ($storeCategories as $key => $storeCategory) {
            $marketplaceId = $storeCategory['id'];
            $marketplaceName = $storeCategory['name'];
            $collections[$marketplaceId] = $marketplaceName;
            if (count($storeCategory->children)) {
                foreach ($storeCategory->children as $marketplaceSubCategory) {
                    $marketplaceId = $marketplaceSubCategory['id'];
                    $marketplaceName = '- - '.$marketplaceSubCategory['name'];
                    $collections[$marketplaceId] = $marketplaceName;
                }
            }
        }
        $dropdown['storeCategories'] = $collections;

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['statuses'] = ['active'=>'Active', 'inactive'=>'Inactive', 'archived'=>'Archived'];
        $dropdown['types'] = [
            'product'=>'Product',
            'grouped product'=>'Grouped Product',
            'digital product' => 'Digital Product',
            'service' => 'Service',
        ];

        $dropdown['product_status_ketersediaan'] = [
            'ready stock' => 'Ready Stock',
            'preorder' => 'Preorder',
        ];

        $dropdown['kelasUsaha'] = [
            'Mikro' => 'Mikro',
            'Kecil' => 'Kecil',
            'Menengah' => 'Menengah',
            'Koperasi' => 'Koperasi',
            'Non UMKM' => 'Non UMKM',
        ];

        $meta_data = $product->getAllMeta();
        $product->meta_data = $meta_data;

        // $productCategories = $product->marketplaceCategories()->pluck('marketplace_category_id');
        $groupedProductDetails = $product->groupedProductDetails()->with('parent', 'product')->get();
        $productMarketplaceCategories = $product->marketplaceCategories()->pluck('marketplace_category_id');
        $productStoreCategories = $product->storeCategories()->pluck('store_category_id');


        $attributes = $product->attributes()->orderBy('sort_order', 'asc')->get();
        $groupedAttributes = $attributes->groupBy('group');

        $galleries = $product->galleries()->orderBy('sort_order', 'asc')->get();

        $mappedKemdikbudZonePrices = [];
        $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
        if ($hasKemdikbudZonePrice) {
            $mappedKemdikbudZonePrices =  $product->getMappedKemdikbudZonePrices();
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.show', compact(
            'user', 'store', 'dropdown', 'product',
            'groupedAttributes', 'groupedProductDetails', 'galleries',
            'hasKemdikbudZonePrice', 'mappedKemdikbudZonePrices',
            'productMarketplaceCategories', 'productStoreCategories',
            'oldDropdowns',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $store->updateLastOnline();

        $marketplace = $store->marketplace;
        // $dropdown['categories'] = $marketplace->categories()->pluck('name', 'id');
        /*
        $marketplaceCategories = $marketplace->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['children' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $categories = [];
        foreach ($marketplaceCategories as $key => $marketplaceCategory) {
            $marketplaceId = $marketplaceCategory['id'];
            $marketplaceName = $marketplaceCategory['name'];
            $categories[$marketplaceId] = $marketplaceName;
            if (count($marketplaceCategory->children)) {
                foreach ($marketplaceCategory->children as $marketplaceSubCategory) {
                    $marketplaceId = $marketplaceSubCategory['id'];
                    $marketplaceName = '- - '.$marketplaceSubCategory['name'];
                    $categories[$marketplaceId] = $marketplaceName;
                }
            }
        }
        $dropdown['marketplaceCategories'] = $categories;
        */

        $productMarketplaceCategories = $product->marketplaceCategories()->pluck('marketplace_category_id');
        $marketplaceCategories = $marketplace->categories()
            ->whereIn('parent_id', $productMarketplaceCategories)
            ->orderBy('name')
            ->get();

        $oldDropdowns = [];
        foreach ($productMarketplaceCategories as $productMarketplaceCategory) {
            $id = $productMarketplaceCategory;
            $oldDropdowns['data'][$id]['id'] = $id;
            $oldDropdowns['data'][$id]['data'] = [];
        }
        foreach ($marketplaceCategories as $marketplaceCategory) {
            $id = $marketplaceCategory->parent_id;
            $oldDropdowns['data'][$id]['data'][] = $marketplaceCategory;
        }
        // return $oldDropdowns;

        $marketplaceCategories = $marketplace->categories()
            ->where('parent_id', null)
            ->orderBy('name')
            ->get();

        $categories = [];
        foreach ($marketplaceCategories as $key => $marketplaceCategory) {
            $marketplaceId = $marketplaceCategory['id'];
            $marketplaceName = $marketplaceCategory['name'];
            $categories[$marketplaceId] = $marketplaceName;
        }
        $dropdown['marketplaceCategories'] = $categories;
        // return $dropdown['marketplaceCategories'];

        $storeCategories = $store->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['children' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $collections = [];
        foreach ($storeCategories as $key => $storeCategory) {
            $marketplaceId = $storeCategory['id'];
            $marketplaceName = $storeCategory['name'];
            $collections[$marketplaceId] = $marketplaceName;
            if (count($storeCategory->children)) {
                foreach ($storeCategory->children as $marketplaceSubCategory) {
                    $marketplaceId = $marketplaceSubCategory['id'];
                    $marketplaceName = '- - '.$marketplaceSubCategory['name'];
                    $collections[$marketplaceId] = $marketplaceName;
                }
            }
        }
        $dropdown['storeCategories'] = $collections;

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['statuses'] = ['active'=>'Active', 'inactive'=>'Inactive', 'archived'=>'Archived'];
        $dropdown['types'] = [
            'product'=>'Product',
            'grouped product'=>'Grouped Product',
            'digital product' => 'Digital Product',
            'service' => 'Service',
        ];

        $dropdown['product_status_ketersediaan'] = [
            'ready stock' => 'Ready Stock',
            'preorder' => 'Preorder',
        ];

        $dropdown['kelasUsaha'] = [
            'Mikro' => 'Mikro',
            'Kecil' => 'Kecil',
            'Menengah' => 'Menengah',
            'Koperasi' => 'Koperasi',
            'Non UMKM' => 'Non UMKM',
        ];

        $meta_data = $product->getAllMeta();
        $product->meta_data = $meta_data;

        $productStoreCategories = $product->storeCategories()->pluck('store_category_id');

        $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.edit', compact(
            'user', 'store', 'dropdown', 'product',
            'hasKemdikbudZonePrice',
            'productMarketplaceCategories', 'productStoreCategories',
            'oldDropdowns',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, Product $product)
    {
        request()->validate([
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'price' => 'required|integer|min:0',
            'original_price' => 'nullable|integer|min:0',
            // 'category' => 'required',
            'qty' => 'required|integer|min:0',
            'weight' => 'required|numeric|min:0',
            'active' => 'required|integer',
            'image' => 'image|max:4000',
            'type' => 'required',
        ]);


        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $product->name = request('name');
        $slug = request('slug') ? str_slug(request('slug')) : str_slug(request('name'));
        $product->slug = Product::getUniqueSlug($slug, $product->id);
        $product->sku = request('sku');
        $product->description = request('description');
        $product->price = request('price');
        $product->qty = request('qty');
        $product->weight = request('weight');
        $product->type = request('type');
        $product->active = request('active');
        $product->status = request('status');
        $product->is_local_product = request('is_local_product');
        $product->is_taxed = request('is_taxed');
        $product->user_id = $user->id;
        $product->store_id = $store->id;
        $product->save();

        $image = request('image');
        if ($image) {
            $filename = 'products';
            $filepath = upload_file($image, 'uploads', $filename);
            $product->image = $filepath;
            $product->save();
        }

        $category = request('category');
        $category = is_array($category) ? array_filter($category) : null;
        if ($category) {
            $product->marketplaceCategories()->sync($category);
        } else {
            $product->marketplaceCategories()->sync([]);
        }

        $collection = request('collection');
        $collection = is_array($collection) ? array_filter($collection) : null;
        if ($collection) {
            $product->storeCategories()->sync($collection);
        } else {
            $product->storeCategories()->sync([]);
        }

        $isUniqueSku = Product::checkIsUniqueSku($product->id);
        if (!$isUniqueSku) {
            flash('SKU is not unique. Please change SKU')->warning();
        }

        // meta
        $originalPrice = request('original_price');
        $product->setMeta('original_price', $originalPrice);

        $hasZonePrice = request('has_kemdikbud_zone_price');
        $product->setMeta('has_kemdikbud_zone_price', $hasZonePrice ? true : false);

        $metaData = request('meta_data');
        foreach ($metaData as $key => $value) {
            $product->setMeta($key, $value);
        }

        $type = request('type');
        $productTypes = ['product', 'grouped product', 'digital product'];
        $serviceTypes = ['service'];
        if (in_array($type, $productTypes)) {
            $is_msme_product = request('meta_data.product_is_msme_product');
            $product->is_msme_product = in_array($is_msme_product, ['Mikro', 'Kecil', 'Menengah']);
            $product->save();
        }
        if (in_array($type, $serviceTypes)) {
            $is_msme_product = request('meta_data.service_is_msme_product');
            $product->is_msme_product = in_array($is_msme_product, ['Mikro', 'Kecil', 'Menengah']);
            $product->save();
        }

        $store->staffUpdatedProduct();

        flash('Product updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $product->active = false;
        $product->save();

        flash('Product deleted');
        return redirect()->back();
    }

    public function restore(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $product->active = true;
        $product->save();

        flash('Product restored');
        return redirect()->back();
    }
}
