<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity;

class ProductKemdikbudZonePricesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $product->load('kemdikbudZonePrices');
        $mappedKemdikbudZonePrices =  $product->getMappedKemdikbudZonePrices();
        $zones = KemdikbudCity::getZones();

        $groupedProductDetails = $product->groupedProductDetails->pluck('id');
        $storeProducts = $store->products()
            ->orderBy('name')
            ->whereNotIn('id', [$product->id])
            ->whereNotIn('id', $groupedProductDetails)
            ->pluck('name', 'id');
        $dropdown['products'] = $storeProducts;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.zone-prices.create', compact(
            'store', 'product', 'dropdown',
            'zones',
            'mappedKemdikbudZonePrices',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store, Product $product)
    {
        request()->validate([
            'prices' => 'required|array',
            'prices.*.zone' => 'required',
            'prices.*.price' => 'required|integer|min:0',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        // return request()->all();

        // validation price req array
        // proce.*.price  numric min0
        // return request('prices');
        $prices = request('prices');
        foreach ($prices as $price) {
            ProductKemdikbudZonePrice::updateOrCreate([
                'product_id' => $product->id,
                'zone' => $price['zone'],
            ], [
                'price' => $price['price'] ? $price['price'] : 0,
            ]);
        }

        flash('Zone Price updated');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice  $productKemdikbudZonePrice
     * @return \Illuminate\Http\Response
     */
    public function show(ProductKemdikbudZonePrice $productKemdikbudZonePrice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice  $productKemdikbudZonePrice
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductKemdikbudZonePrice $productKemdikbudZonePrice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice  $productKemdikbudZonePrice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductKemdikbudZonePrice $productKemdikbudZonePrice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice  $productKemdikbudZonePrice
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductKemdikbudZonePrice $productKemdikbudZonePrice)
    {
        //
    }
}
