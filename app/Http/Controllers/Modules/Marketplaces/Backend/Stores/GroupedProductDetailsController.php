<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\GroupedProductDetail;
use App\Models\Modules\Website\WebsiteSettings;

class GroupedProductDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store, Product $product)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $groupedProductDetails = $product->groupedProductDetails->pluck('id');
        $storeProducts = $store->products()
            ->orderBy('name')
            ->whereNotIn('id', [$product->id])
            ->whereNotIn('id', $groupedProductDetails)
            ->pluck('name', 'id');
        $dropdown['products'] = $storeProducts;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.products.grouped-products.create', compact(
            'store', 'product', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store, Product $product)
    {
        request()->validate([
            'product_id' => 'required',
            'qty' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        $detailProductId = request('product_id');
        $detailProduct = Product::find($detailProductId);
        if (!$detailProduct->isOwnedByStore($store->id)) {
            flash('Product not found')->error();
            return redirect()->route('users.stores.products.show', [$store->id, $product->id]);
        }

        GroupedProductDetail::updateOrCreate([
            'parent_id' => $product->id,
            'product_id' => request('product_id'),
        ], [
            'qty' => request('qty'),
            'note' => request('note'),
        ]);

        flash('Product added to grouped product');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, Product $product, GroupedProductDetail $groupedProduct)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        if (!$groupedProduct->isOwnedByProduct($product->id)) {
            flash('Product not found')->error();
            return redirect()->route('users.stores.products.show', [$store->id, $product->id]);
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        $groupedProduct->load('product');
        return view('modules.marketplaces.backend.stores.products.grouped-products.edit', compact(
            'store', 'product', 'groupedProduct',
            'websiteSettings'
        ));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, Product $product, GroupedProductDetail $groupedProduct)
    {
        request()->validate([
            'qty' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        if (!$groupedProduct->isOwnedByProduct($product->id)) {
            flash('Product not found')->error();
            return redirect()->route('users.stores.products.show', [$store->id, $product->id]);
        }

        $detail = $groupedProduct;
        $detail->qty = request('qty');
        $detail->note = request('note');
        $detail->save();

        flash('Product updated to grouped product');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, Product $product, GroupedProductDetail $groupedProduct)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$product->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.products.index', $store->id);
        }

        if (!$groupedProduct->isOwnedByProduct($product->id)) {
            flash('Product not found')->error();
            return redirect()->route('users.stores.products.show', [$store->id, $product->id]);
        }

        $groupedProduct->delete();

        flash('Product deleted from grouped product');
        return redirect()->back();
    }
}
