<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreSetting;
use App\Models\Modules\Marketplaces\StoreShippingMethod;
use App\Models\Modules\Website\WebsiteSettings;

class StoreShippingMethodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplace = $store->marketplace;
        $shippingMethods = $store->shippingMethods()->where('group', 'custom')->latest()->paginate(20);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.shipping-methods.index', compact(
            'user', 'store', 'marketplace', 'shippingMethods',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [
            1 => 'Yes',
            0 => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.shipping-methods.create', compact(
            'user', 'store', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'name' => 'required',
            'label' => 'required|alpha',
            'active' => 'required|integer',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $label = request('label');
        $isValid = StoreShippingMethod::checkLabelIsValid($label, $store->id);
        if (!$isValid) {
            return redirect()->back()
                ->withInput()
                ->withErrors(['label' => ['Code is already taken']]);
        }

        $shippingMethod = new StoreShippingMethod;
        $shippingMethod->group = 'custom';
        $shippingMethod->label = strtolower(request('label'));
        $shippingMethod->name = request('name');
        $shippingMethod->active = request('active');
        $store->menus()->save($shippingMethod);

        flash('Shipping Method saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, StoreShippingMethod $shippingMethod)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$shippingMethod->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.store-sellers.index', $store->id);
        }

        $dropdown['yes_no'] = [
            1 => 'Yes',
            0 => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.shipping-methods.edit', compact(
            'user', 'store', 'shippingMethod', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, StoreShippingMethod $shippingMethod)
    {
        request()->validate([
            'name' => 'required',
            'label' => 'required|alpha',
            'active' => 'required|integer',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$shippingMethod->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.shipping-methods.index', $store->id);
        }

        if ($shippingMethod->group == 'rajaongkir') {
            flash('Shipping Method cannot be updated')->error();
            return redirect()->route('users.stores.shipping-methods.index', $store->id);
        }

        $label = request('label');
        $isValid = StoreShippingMethod::checkLabelIsValid($label, $store->id, $shippingMethod->id);
        if (!$isValid) {
            return redirect()->back()
                ->withInput()
                ->withErrors(['label' => ['Code is already taken']]);
        }

        $shippingMethod->label = strtolower(request('label'));
        $shippingMethod->name = request('name');
        $shippingMethod->active = request('active');
        $shippingMethod->save();

        flash('Shipping Method updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, StoreShippingMethod $shippingMethod)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$shippingMethod->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.shipping-methods.index', $store->id);
        }

        if ($shippingMethod->group == 'rajaongkir') {
            flash('Shipping Method cannot be deleted')->error();
            return redirect()->route('users.stores.shipping-methods.index', $store->id);
        }

        $shippingMethod->delete();

        flash('Shipping Method deleted');
        return redirect()->back();
    }

    public function rajaongkirEdit(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        // $shippingMethods = $store->settings()->shoppingMethods()->first();
        // $storeShippingMethods = $shippingMethods ? explode(',', $shippingMethods->value) : [];
        $storeShippingMethods = $store->shippingMethods()->where('group', 'rajaongkir')->get();
        if (count($storeShippingMethods) == 0) {
            $couriers = get_rajaongkir_couriers();

            foreach ($couriers as $courier => $courierName) {
                $method = new StoreShippingMethod;
                $method->group = 'rajaongkir';
                $method->label = $courier;
                $method->name = $courierName;
                $method->active = false;
                $method->store_id = $store->id;
                $method->save();
            }
            $storeShippingMethods = $store->shippingMethods;
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.shipping-methods.rajaongkir-edit', compact(
            'user', 'store', 'storeShippingMethods',
            'websiteSettings'
        ));
    }

    public function rajaongkirUpdate(Request $request, Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        request()->validate([
            'shipping_methods' => 'required',
        ]);
        $selectedShippingMethods = request('shipping_methods');
        $store->shippingMethods()->update(['active'=>false]);
        $storeShippingMethods = $store->shippingMethods;
        foreach ($storeShippingMethods as $method) {
            if (in_array($method->label, $selectedShippingMethods)) {
                $method->active = true;
                $method->save();
            }
        }

        flash('Payment Method Saved');
        return redirect()->back();
    }
}
