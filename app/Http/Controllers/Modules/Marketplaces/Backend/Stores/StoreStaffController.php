<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\StoreStaff;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Website\WebsiteSettings;
use App\User;
use Illuminate\Support\Str;

class StoreStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $staffs = $store->staffs()->with('user', 'meta')->latest()->paginate(20);
        // return $staffs;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.staffs.index', compact(
            'user', 'store', 'staffs',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['roles'] = [
            'admin' => 'Admin',
            'staff' => 'Staff',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.staffs.create', compact(
            'user', 'store', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
            'role' => 'required|in:staff,admin',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        // create user
        $email = request('email');
        $password = request('password');
        $userExists = User::where('email', $email)->exists();
        if (!$userExists) {
            flash('User created');
            $user = User::create([
                'name' => $email,
                'username' => Str::random(11),
                'email' => $email,
                'password' => bcrypt($password),
            ]);
        } else {
            flash('User already exists')->warning();
            $user = User::where('email', $email)->first();
        }

        $staffExists = StoreStaff::where([
            'user_id' => $user->id,
            'store_id' => $store->id,
        ])->exists();

        if ($staffExists) {
            flash('Staff already exists')->warning();
            return redirect()->back();
        }

        $staff = new StoreStaff;
        $staff->user_id = $user->id;
        $staff->store_id = $store->id;
        $staff->role = request('role');
        $staff->save();

        flash('Staff saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\StoreStaff  $storeStaff
     * @return \Illuminate\Http\Response
     */
    public function show(StoreStaff $storeStaff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\StoreStaff  $storeStaff
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, StoreStaff $staff)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$staff->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.staffs.index', $store->id);
        }

        $dropdown['roles'] = [
            'admin' => 'Admin',
            'staff' => 'Staff',
        ];

        $dropdown['yes_no'] = [
            '1' => 'Yes',
            '0' => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.staffs.edit', compact(
            'user', 'store', 'staff', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\StoreStaff  $storeStaff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, StoreStaff $staff)
    {
        request()->validate([
            'role' => 'required|in:staff,admin',
            'is_active' => 'required|boolean',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$staff->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.staffs.index', $store->id);
        }

        $isOwner = $store->checkIsOwner($staff->user_id);
        if ($isOwner) {
            flash('Staff cannot be updated')->error();
            return redirect()->back();
        }

        $staff->role = request('role');
        $staff->is_active = request('is_active');
        $staff->save();

        flash('Staff updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\StoreStaff  $storeStaff
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, StoreStaff $staff)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$staff->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.staffs.index', $store->id);
        }

        $isOwner = $store->checkIsOwner($staff->user_id);
        if ($isOwner) {
            flash('Staff cannot be deleted')->error();
            return redirect()->back();
        }



        $staff->is_active = false;
        $staff->save();

        flash('Staff deleted');
        return redirect()->back();
    }
}
