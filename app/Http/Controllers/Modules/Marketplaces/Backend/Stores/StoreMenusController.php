<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\StoreMenu;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Website\WebsiteSettings;

class StoreMenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $menus = $store->menus()->with('children', 'parent')->latest()->paginate(20);
        // return $menus;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.menus.index', compact(
            'user', 'store', 'menus',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $parentMenus = $store->menus()
            ->where('parent_id', 0)
            ->orderBy('label')
            ->pluck('label', 'id')
            ->toArray();
        $dropdown['parent_menus'] = $parentMenus;
        $dropdown['parent_menus'] = array_prepend($dropdown['parent_menus'], 'Is Parent', 0);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.menus.create', compact(
            'user', 'store', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        request()->validate([
            'label' => 'required',
            'link' => 'url|nullable',
            'parent' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $menu = new StoreMenu;
        $menu->group = 'menu header';
        $menu->label = request('label');
        $menu->link = request('link');
        $menu->sort_order = request('sort_order');
        $store->menus()->save($menu);

        $parentId = request('parent');
        if ($parentId == 0) {
            $menu->parent_id = 0;
            $menu->save();
        } else {
            $parentCategory = $store->menus()->find($parentId);
            if ($parentCategory && $parentCategory->isOwnedByStore($store->id)) {
                $menu->parent_id = $parentCategory->id;
                $menu->save();
            }
        }

        flash('Menu saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplace\StoreMenu  $storeMenu
     * @return \Illuminate\Http\Response
     */
    public function show(StoreMenu $storeMenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplace\StoreMenu  $storeMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, StoreMenu $menu)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$menu->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.menus.index', $store->id);
        }

        $parentMenus = $store->menus()
            ->where('parent_id', 0)
            ->where('id', '<>', $menu->id)
            ->orderBy('label')
            ->pluck('label', 'id')
            ->toArray();
        $dropdown['parent_menus'] = $parentMenus;
        $dropdown['parent_menus'] = array_prepend($dropdown['parent_menus'], 'Is Parent', 0);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.stores.menus.edit', compact(
            'user', 'store', 'menu', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplace\StoreMenu  $storeMenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, StoreMenu $menu)
    {
        request()->validate([
            'label' => 'required',
            'link' => 'url|nullable',
            // 'parent' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$menu->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.menus.index', $store->id);
        }

        $menu->label = request('label');
        $menu->link = request('link');
        $menu->sort_order = request('sort_order');
        $menu->save();

        $parentId = request('parent');
        if ($parentId == 0) {
            $menu->parent_id = 0;
            $menu->save();
        } else {
            $parentCategory = $store->menus()->find($parentId);
            if ($parentCategory && $parentCategory->isOwnedByStore($store->id)) {
                $menu->parent_id = $parentCategory->id;
                $menu->save();
            }
        }

        flash('Menu updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplace\StoreMenu  $storeMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, StoreMenu $menu)
    {
        $user = auth()->user();
        if (!$store->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$menu->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.menus.index', $store->id);
        }

        $store->menus()->where('parent_id', $menu->id)->update(['parent_id'=> 0]);
        $menu->delete();

        flash('Menu deleted');
        return redirect()->back();
    }
}
