<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteSettings;
use App\User;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreStaff;
use Illuminate\Support\Str;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict;
use App\Models\Modules\Marketplaces\StoreAddress;
use App\Models\Modules\Marketplaces\GroupNotification;

class StoreRegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();
        $dropdown['jenisUsaha'] = [
            'Individu' => 'Individu',
            'PT' => 'PT',
            'CV' => 'CV',
            'Koperasi' => 'Koperasi',
            'Lainnya' => 'Lainnya',
        ];
        $dropdown['kelasUsaha'] = [
            'Mikro' => 'Mikro',
            'Kecil' => 'Kecil',
            'Menengah' => 'Menengah',
            'Koperasi' => 'Koperasi',
            'Non UMKM' => 'Non UMKM',
        ];
        $dropdown['statusUsaha'] = [
            'PKP' => 'PKP',
            'NON-PKP' => 'NON-PKP',
        ];

        $provincesWithDetails = IndonesiaProvince::getProvincesWithDetails();

        $dropdown['provinces'] = IndonesiaProvince::getDropdown();

        $provinceId = old('province_id');
        $dropdown['cities'] = IndonesiaCity::getDropdown($provinceId);

        $cityId = old('city_id');
        $dropdown['districts'] = IndonesiaDistrict::getDropdown($cityId);

        $storeMeta = $store->getAllMeta();
        // return $storeMeta;

        return view('modules.marketplaces.backend.stores.registration.edit', compact(
            'store', 'storeMeta',
            'websiteSettings',
            'dropdown',
            'provincesWithDetails'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        $data = request()->validate([

            'nama_toko' => 'required',
            'jenis_usaha' => 'required',
            'kelas_usaha' => 'required',
            'status' => 'required',
            'npwp' => 'required',
            'siup_nib' => 'required',
            'tdp' => 'required',
            'no_tel_kantor' => 'required',

            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',

            'alamat' => 'required',
            'geolokasi' => 'nullable',
            // 'kab_kota' => 'required',

            'kode_pos' => 'required',
            'email_toko' => 'required|email',

            'nama_lengkap_penanggungjawab' => 'required',
            'email_penanggungjawab' => 'required|email',
            'jabatan_penanggungjawab' => 'required',
            'nik_penanggungjawab' => 'required',

            'nama_bank' => 'required',
            'nama_pemilik_rekening' => 'required',
            'no_rekening' => 'required',
            'cabang_bank' => 'required',

            'setuju_ketentuan_penjual' => 'required|accepted',

            'scan_ktp_penanggung_jawab' => 'nullable|file|image|max:1024',
            'scan_npwp' => 'nullable|file|image|max:1024',
            'scan_siup' => 'nullable|file|image|max:1024',
            'scan_tdp' => 'nullable|file|image|max:1024',
        ]);

        $store->name = $data['nama_toko'];
        $store->slug = $store->getUniqueSlug(Str::slug($data['nama_toko']));
        $store->active = false;
        $store->status = 'inactive';
        $store->save();

        // add meta
        // upload file
        $store->setMeta('no_tel_kantor', $data['no_tel_kantor']);
        $store->setMeta('jenis_usaha', $data['jenis_usaha']);
        $store->setMeta('kelas_usaha', $data['kelas_usaha']);
        $store->setMeta('status_usaha', $data['status']);
        $store->setMeta('npwp', $data['npwp']);
        $store->setMeta('siup_nib', $data['siup_nib']);
        $store->setMeta('tdp', $data['tdp']);

        $store->setMeta('province_id', $data['province_id']);
        $store->setMeta('city_id', $data['city_id']);
        $store->setMeta('district_id', $data['district_id']);

        $store->setMeta('alamat', $data['alamat']);
        // $store->setMeta('geolokasi', $data['geolokasi']);

        // $store->setMeta('kab_kota', $data['kab_kota']);
        $store->setMeta('kode_pos', $data['kode_pos']);
        $store->setMeta('email_toko', $data['email_toko']);

        $store->setMeta('nama_lengkap_penanggungjawab', $data['nama_lengkap_penanggungjawab']);
        $store->setMeta('email_penanggungjawab', $data['email_penanggungjawab']);
        $store->setMeta('jabatan_penanggungjawab', $data['jabatan_penanggungjawab']);
        $store->setMeta('nik_penanggungjawab', $data['nik_penanggungjawab']);

        $store->setMeta('nama_bank', $data['nama_bank']);
        $store->setMeta('nama_pemilik_rekening', $data['nama_pemilik_rekening']);
        $store->setMeta('no_rekening', $data['no_rekening']);
        $store->setMeta('cabang_bank', $data['cabang_bank']);

        $files = ['scan_ktp_penanggung_jawab', 'scan_npwp', 'scan_siup', 'scan_tdp'];
        foreach ($files as $document) {
            $image = $data[$document] ?? null;
            if ($image) {
                $filename = 'store-documents';
                $filepath = upload_file($image, 'uploads', $filename);
                $store->setMeta($document, $filepath);
            }
        }
        // return $store->getAllMeta();

        // return $store;
        // return request()->all();

        $hasDefaultStoreAddress = $store->hasDefaultStoreAddress();
        if ($hasDefaultStoreAddress) {
            $address = $store->getDefaultStoreAddress();

            $address->label = 'Alamat Utama';
            $address->full_name = 'Alamat Utama';
            $address->company = $store->name;
            $address->address = request('alamat');
            $address->province_id = request('province_id');
            $address->city_id = request('city_id');
            $address->district_id = request('district_id');
            $address->phone = '-';
            $address->postcode = '-';

            $address->save();
        }

        $marketplace = $store->marketplace;
        $notificationData = [];
        $notificationData['data']['store'] = $store->getOriginal();
        $notificationData['description'] = 'Toko telah mengirim ulang data pendaftaran';
        GroupNotification::addNotification($marketplace, 'store_resend_registration', $notificationData);

        flash('Pendaftaran sukses. Menunggu persetujuan dari admin');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
