<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Schools;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\SchoolStaff;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\School;
use App\Models\Modules\Website\WebsiteSettings;

class SchoolStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(School $school)
    {
        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $staffs = $school->staffs()->with('user')->paginate(10);

        return view('modules.marketplaces.backend.schools.staffs.index', compact(
            'user', 'school',
            'staffs',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\SchoolStaff  $schoolStaff
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolStaff $schoolStaff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\SchoolStaff  $schoolStaff
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolStaff $schoolStaff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\SchoolStaff  $schoolStaff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolStaff $schoolStaff)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\SchoolStaff  $schoolStaff
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolStaff $schoolStaff)
    {
        //
    }
}
