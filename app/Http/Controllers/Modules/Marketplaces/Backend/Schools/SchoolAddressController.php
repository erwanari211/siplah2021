<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Schools;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\School;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Marketplaces\SchoolAddress;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict;

class SchoolAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(School $school)
    {
        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $addresses = $school->addresses()->with('province', 'city', 'district')->paginate(10);
        $school->has_addresses = $school->addresses()->count() ? true : false;
        $school->has_default_addresses = $school->addresses()->defaultAddress()->count() ? true : false;

        return view('modules.marketplaces.backend.schools.addresses.index', compact(
            'user', 'school',
            'addresses',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(School $school)
    {
        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $provincesWithDetails = IndonesiaProvince::getProvincesWithDetails();

        $dropdown['provinces'] = IndonesiaProvince::getDropdown();

        $provinceId = old('province_id');
        $dropdown['cities'] = IndonesiaCity::getDropdown($provinceId);

        $cityId = old('city_id');
        $dropdown['districts'] = IndonesiaDistrict::getDropdown($cityId);

        return view('modules.marketplaces.backend.schools.addresses.create', compact(
            'user', 'school',
            'provinces', 'dropdown',
            'provincesWithDetails',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, School $school)
    {
        request()->validate([
            'label' => 'required',
            'full_name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'postcode' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
        ]);

        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $address = new SchoolAddress;
        $address->label = request('label');
        $address->full_name = request('full_name');
        $address->company = request('company');
        $address->address = request('address');
        $address->province_id = request('province_id');
        $address->city_id = request('city_id');
        $address->district_id = request('district_id');
        $address->phone = request('phone');
        $address->postcode = request('postcode');
        $address->default_address = false;

        $school->addresses()->save($address);

        $countAddresses = SchoolAddress::where('school_id', $school->id)->count();
        if ($countAddresses == 1) {
            $address->default_address = true;
            $address->save();

            $school->saveDefaultAddress();
        }

        flash('Address saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school, SchoolAddress $address)
    {
        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$address->isOwnedBySchool($school->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.schools.addresses.index', $school->id);
        }

        $provincesWithDetails = IndonesiaProvince::getProvincesWithDetails();

        $dropdown['provinces'] = IndonesiaProvince::getDropdown();

        $provinceId = old('province_id', $address->province_id);
        $dropdown['cities'] = IndonesiaCity::getDropdown($provinceId);

        $cityId = old('city_id', $address->city_id);
        $dropdown['districts'] = IndonesiaDistrict::getDropdown($cityId);

        return view('modules.marketplaces.backend.schools.addresses.edit', compact(
            'user', 'school',
            'address',
            'provinces', 'dropdown',
            'provincesWithDetails',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, School $school, SchoolAddress $address)
    {
        request()->validate([
            'label' => 'required',
            'full_name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'postcode' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
        ]);

        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$address->isOwnedBySchool($school->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.schools.addresses.index', $school->id);
        }

        $address->label = request('label');
        $address->full_name = request('full_name');
        $address->company = request('company');
        $address->address = request('address');
        $address->province_id = request('province_id');
        $address->city_id = request('city_id');
        $address->district_id = request('district_id');
        $address->phone = request('phone');
        $address->postcode = request('postcode');
        $address->save();

        if ($address->default_address) {
            $school->saveDefaultAddress();
        }

        flash('Address saved');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $school, SchoolAddress $address)
    {
        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$address->isOwnedBySchool($school->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.schools.addresses.index', $school->id);
        }

        $address->delete();

        flash('Address deleted');
        return redirect()->back();
    }

    public function setDefaultAddress(Request $request, School $school, SchoolAddress $address)
    {
        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$address->isOwnedBySchool($school->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.schools.addresses.index', $school->id);
        }

        if ($address) {
            $school->addresses()->update(['default_address'=>false]);
            $address->default_address = true;
            $address->save();

            $school->saveDefaultAddress();
        }

        flash('Address saved');
        return redirect()->back();
    }
}
