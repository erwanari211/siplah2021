<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Schools;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\SchoolMessage;
use Illuminate\Http\Request;

class SchoolMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\SchoolMessage  $schoolMessage
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolMessage $schoolMessage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\SchoolMessage  $schoolMessage
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolMessage $schoolMessage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\SchoolMessage  $schoolMessage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolMessage $schoolMessage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\SchoolMessage  $schoolMessage
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolMessage $schoolMessage)
    {
        //
    }
}
