<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Schools;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\School;
use App\Models\Modules\Marketplaces\Order;
use App\Models\Modules\Marketplaces\GroupNotification;

class OrderStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, School $school, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'status' => 'required',

            'notify' => 'required|boolean',
            'note' => 'nullable',
            'file' => 'nullable|image|max:4000',
        ]);

        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedBySchool($school->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.schools.orders.index', $school->id);
        }

        $group = request('group');
        $status = request('status');
        $historyData = request()->only(['notify', 'note', 'file']);
        $order->updateGroupedOrderStatus($group, $status, $historyData);

        if ($group == 'delivery') {
            if ($status == 'delivered') {
                $store = $order->store;
                $notificationData['data']['new'] = $order->getOriginal();
                $notificationData['description'] = 'Pesanan telah diterima pembeli';
                GroupNotification::addNotification($store, 'order_delivered', $notificationData);
            }
        }

        flash('Order updated');
        return redirect()->back();
    }

    public function storeDeliveryDocument(Request $request, School $school, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'status' => 'required',

            'notify' => 'required|boolean',
            'note' => 'nullable',
            'file' => 'required|image|max:4000',
        ]);

        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedBySchool($school->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.schools.orders.index', $school->id);
        }

        $group = request('group');
        $status = request('status');
        $historyData = request()->only(['notify', 'note', 'file']);
        $order->updateGroupedOrderStatus($group, $status, $historyData);

        flash('Order updated');
        return redirect()->back();
    }

    public function storePayment(Request $request, School $school, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'status' => 'required',

            'notify' => 'required|boolean',
            'note' => 'nullable',
            'file' => 'required|image|max:4000',
        ]);

        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedBySchool($school->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.schools.orders.index', $school->id);
        }

        $notificationData['data']['old'] = $order->getOriginal();

        $group = request('group');
        $status = request('status');
        $historyData = request()->only(['notify', 'note', 'file']);
        $order->updateGroupedOrderStatus($group, $status, $historyData);

        $store = $order->store;
        $marketplace = $store->marketplace;
        $notificationData['data']['new'] = $order->getOriginal();
        $notificationData['description'] = 'Pembeli telah membayar pesanan';
        GroupNotification::addNotification($marketplace, 'order_payment_from_customer', $notificationData);
        GroupNotification::addNotification($store, 'order_payment_from_customer', $notificationData);

        flash('Order updated');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
