<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Schools;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\School;
use App\Models\Modules\Marketplaces\Order;
use App\Models\Modules\Marketplaces\OrderStatus;
use App\Models\Modules\Marketplaces\ProductReview;
use App\Models\Modules\Website\WebsiteSettings;
use PDF;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(School $school)
    {
        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $groupedOrderStatuses = OrderStatus::getGroupedStatus();
        $groupedOrderStatuses = Order::addOrderCountToSchoolOrderStatuses($school->id, $groupedOrderStatuses);
        $orderStatusId = OrderStatus::where('group', 'order_process')->where('key', 'pending')->first()->id;
        if (request('order_status_id')) {
            $orderStatusId = request('order_status_id');
        }
        $selectedOrderStatus = OrderStatus::find($orderStatusId);

        $status = 'pending';
        $orderStatus = request('status') ? request('status') : $status;
        $orders = $school->orders()->where('order_status_id', $orderStatusId)->orderBy('updated_at', 'desc')->paginate(10);

        return view('modules.marketplaces.backend.schools.orders.index', compact(
            'user', 'school',
            'orders', 'orderStatus',
            'groupedOrderStatuses', 'orderStatusId', 'selectedOrderStatus'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(School $school, Order $order)
    {
        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedBySchool($school->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.schools.orders.index', $school->id);
        }

        $order->load('store', 'store.marketplace', 'productReviews');
        $store = $order->store;
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();
        $groupedProductReviews = $order->productReviews->pluck('rating', 'product_id');
        $histories = $order->histories()->with('meta')->latest()->get();

        $groupedOrderStatuses = OrderStatus::get()->groupBy('group');

        $activities['negotiation_comments'] = $order->activities()
            ->where('group', 'negotiation')
            // ->where('activity', 'negotiation comment')
            ->with('meta', 'user')
            ->get();
        $activities['complain_comments'] = $order->activities()
            ->where('group', 'complain')
            ->where('activity', 'complain comment')
            ->with('meta', 'user')
            ->get();
        $activities['documents'] = $order->activities()
            ->where('group', 'document')
            ->with('meta', 'user', 'order')
            ->get();
        $activities['payments'] = $order->activities()
            ->where('group', 'payment')
            ->with('meta', 'user')
            ->get();
        // return $activities['payments'];

        $dropdown['yes_no'] = [0=>'No', 1=>'Yes'];

        return view('modules.marketplaces.backend.schools.orders.show', compact(
            'user', 'school',
            'store', 'marketplace', 'order', 'products', 'orderMetas', 'histories',
            'activities',
            'groupedOrderStatuses',
            'groupedProductReviews',
            'dropdown'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function downloadPdf(School $school, Order $order)
    {
        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedBySchool($school->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.schools.orders.index', $school->id);
        }

        $order->load('store', 'store.marketplace');
        $store = $order->store;
        $marketplace = $store->marketplace;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();
        // $histories = $order->histories()->with('meta')->latest()->get();

        // $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        $view = 'modules.marketplaces.backend.schools.orders.download-pdf';
        $viewData = compact(
            'user', 'store', 'marketplace', 'order', 'products', 'orderMetas', 'histories',
            'websiteSettings'
        );
        // return view($view, $viewData);

        $html = view($view, $viewData)->render();
        // return $html;

        // $pdf = \App::make('dompdf.wrapper');
        // $pdf->loadHTML($html);
        // return $pdf->stream();

        $pdf = PDF::loadView($view, $viewData);

        $invoice_number = $order->invoice_prefix.$order->invoice_no;
        $random = time().'-'.str_random(8);
        $filename = $invoice_number.'-'.$random.'.pdf';
        return $pdf->download($filename);
    }

    public function productRatingStore(Request $request, School $school, Order $order)
    {
        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedBySchool($school->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.schools.orders.index', $school->id);
        }

        request()->validate([
            'product-rating-product-id' => 'required',
            'product-rating-rating' => 'required|integer|min:0|max:5',
            'product-rating-content' => 'required',
        ]);

        $productId = request('product-rating-product-id');
        $rating = request('product-rating-rating');
        $content = request('product-rating-content');

        $isExists = $order->products()->where('product_id', $productId)->exists();
        if (!$isExists) {
            flash('Product not found')->error();
            return redirect()->back();
        }

        $review = ProductReview::updateOrCreate([
            'product_id' => $productId,
            'order_id' => $order->id,
        ], [
            'user_id' => $user->id,
            'rating' => $rating,
            'content' => $content,
        ]);

        $product = $review->product;
        $rating = $product->reviews->average('rating');
        $product->rating = $rating;
        $product->save();

        flash('Product rating saved');
        return redirect()->back();
    }
}
