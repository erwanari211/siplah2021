<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Schools;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\School;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Marketplaces\SchoolAddress;
use App\Models\Modules\Wilayah\Kemdikbud\Siplah\KemdikbudSiplahRegion;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(School $school)
    {
        $user = auth()->user();
        $isStaff = $school->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $school->has_addresses = $school->addresses()->count() ? true : false;
        $school->has_default_addresses = $school->addresses()->defaultAddress()->count() ? true : false;
        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        $orderData = $school->fetchSchoolOrderDataForDashboard();

        return view('modules.marketplaces.backend.schools.index', compact(
            'user', 'school',
            'orderData',
            'websiteSettings'
        ));

        /*
        $schoolName = $school->name;
        return $schoolName . ' Homepage';
        */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
