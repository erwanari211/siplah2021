<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\SupervisorGroups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\SupervisorGroup;
use App\Models\Modules\Marketplaces\SupervisorStaff;
use App\User;
use Illuminate\Support\Str;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SupervisorGroup $supervisorGroup)
    {
        $user = auth()->user();
        $isStaff = $supervisorGroup->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $staffs = $supervisorGroup->staffs()->with('user')->paginate(10);

        return view('modules.marketplaces.backend.supervisor-groups.staffs.index', compact(
            'user', 'supervisorGroup',
            'staffs'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(SupervisorGroup $supervisorGroup)
    {
        $user = auth()->user();
        $isStaff = $supervisorGroup->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $userStaff = $supervisorGroup->staffs()->where('user_id', $user->id)->first();
        if (!in_array($userStaff->role, ['admin'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.supervisor-groups.staffs.index', $supervisorGroup->id);
        }

        $dropdown['roles'] = [
            'admin' => 'Admin',
            'staff' => 'Staff',
        ];

        return view('modules.marketplaces.backend.supervisor-groups.staffs.create', compact(
            'user', 'supervisorGroup',
            'dropdown'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SupervisorGroup $supervisorGroup)
    {
        request()->validate([
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
            'role' => 'required|in:staff,admin',
        ]);

        $user = auth()->user();
        $isStaff = $supervisorGroup->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $userStaff = $supervisorGroup->staffs()->where('user_id', $user->id)->first();
        if (!in_array($userStaff->role, ['admin'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.supervisor-groups.staffs.index', $supervisorGroup->id);
        }

        // create user
        $groupId = $supervisorGroup->id;
        $email = request('email');
        $password = request('password');
        $userExists = User::where('email', $email)->exists();
        if (!$userExists) {
            flash('User created');
            $user = User::create([
                'name' => $email,
                'username' => Str::random(11),
                'email' => $email,
                'password' => bcrypt($password),
            ]);
        } else {
            flash('User already exists')->warning();
            $user = User::where('email', $email)->first();
        }

        $staffExists = SupervisorStaff::where([
            'user_id' => $user->id,
            'supervisor_group_id' => $groupId,
        ])->exists();

        if ($staffExists) {
            flash('Staff already exists')->warning();
            return redirect()->back();
        }

        $staff = new SupervisorStaff;
        $staff->user_id = $user->id;
        $staff->supervisor_group_id = $groupId;
        $staff->role = request('role');
        $staff->save();

        flash('Supervisor Staff saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SupervisorGroup $supervisorGroup, SupervisorStaff $staff)
    {
        $user = auth()->user();
        $isStaff = $supervisorGroup->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $userStaff = $supervisorGroup->staffs()->where('user_id', $user->id)->first();
        if (!in_array($userStaff->role, ['admin'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.supervisor-groups.staffs.index', $supervisorGroup->id);
        }


        if ($staff->supervisor_group_id != $supervisorGroup->id) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.supervisor-groups.staffs.index', $supervisorGroup->id);
        }

        $dropdown['roles'] = [
            'admin' => 'Admin',
            'staff' => 'Staff',
        ];
        $dropdown['yes_no'] = [
            '1' => 'Yes',
            '0' => 'No',
        ];

        return view('modules.marketplaces.backend.supervisor-groups.staffs.edit', compact(
            'user', 'supervisorGroup',
            'staff',
            'dropdown'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SupervisorGroup $supervisorGroup, SupervisorStaff $staff)
    {
        request()->validate([
            'role' => 'required|in:staff,admin',
            'is_active' => 'required|boolean',
        ]);

        $user = auth()->user();
        $isStaff = $supervisorGroup->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $userStaff = $supervisorGroup->staffs()->where('user_id', $user->id)->first();
        if (!in_array($userStaff->role, ['admin'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.supervisor-groups.staffs.index', $supervisorGroup->id);
        }

        if ($staff->supervisor_group_id != $supervisorGroup->id) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.supervisor-groups.staffs.index', $supervisorGroup->id);
        }

        if ($staff->user_id == $user->id) {
            flash('Staff cannot be edited')->error();
            return redirect()->route('users.supervisor-groups.staffs.index', $supervisorGroup->id);
        }

        $staff->role = request('role');
        $staff->is_active = request('is_active');
        $staff->save();

        flash('Supervisor Staff updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupervisorGroup $supervisorGroup, SupervisorStaff $staff)
    {
        $user = auth()->user();
        $isStaff = $supervisorGroup->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $userStaff = $supervisorGroup->staffs()->where('user_id', $user->id)->first();
        if (!in_array($userStaff->role, ['admin'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.supervisor-groups.staffs.index', $supervisorGroup->id);
        }

        if ($staff->supervisor_group_id != $supervisorGroup->id) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.supervisor-groups.staffs.index', $supervisorGroup->id);
        }

        if ($staff->user_id == $user->id) {
            flash('Staff cannot be deleted')->error();
            return redirect()->route('users.supervisor-groups.staffs.index', $supervisorGroup->id);
        }

        $staff->is_active = false;
        $staff->save();

        flash('Supervisor Staff deleted');
        return redirect()->back();
    }
}
