<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\SupervisorGroups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\SupervisorGroup;
use App\Models\Modules\Website\WebsiteSettings;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SupervisorGroup $supervisorGroup)
    {
        $user = auth()->user();
        $isStaff = $supervisorGroup->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $defaultMarketplace = get_default_marketplace();
        $orderData = $defaultMarketplace->fetchOrderDataForDashboard();

        return view('modules.marketplaces.backend.supervisor-groups.index', compact(
            'user', 'supervisorGroup',
            'orderData'
        ));

        $supervisorGroupName = $supervisorGroup->name;
        return $supervisorGroupName . ' Homepage';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
