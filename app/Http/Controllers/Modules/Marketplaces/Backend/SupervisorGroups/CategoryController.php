<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\SupervisorGroups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\SupervisorGroup;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SupervisorGroup $supervisorGroup)
    {
        $user = auth()->user();
        $isStaff = $supervisorGroup->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplace = get_default_marketplace();

        $category = null;
        $parentCategory = null;
        $rootCategories = null;
        $products = null;

        if (is_null($category)) {
            $rootCategories = $marketplace->categories()
                ->withCount('children', 'products')
                ->where('parent_id', null)
                ->orderBy('name')
                ->get();
        }

        $categoryId = request('category_id') ?? null;
        if (!is_null($categoryId)) {
            $rootCategories = null;
            $category = $marketplace->categories()
                ->with(['children' => function($q){
                    $q->withCount('products');
                } ])
                ->withCount('children', 'products')
                ->find($categoryId);
            $products = $category->products()->paginate(10);
        }

        return view('modules.marketplaces.backend.supervisor-groups.categories.index', compact(
            'user', 'supervisorGroup',
            'marketplace',
            'rootCategories',
            'category',
            'products'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
