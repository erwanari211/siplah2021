<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\SupervisorGroups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\SupervisorGroup;
use App\Models\Modules\Marketplaces\SupervisorStaff;
use App\User;
use Illuminate\Support\Str;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SupervisorGroup $supervisorGroup)
    {
        $user = auth()->user();
        $isStaff = $supervisorGroup->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplace = get_default_marketplace();

        $defaultFilter = 'inactive';
        $storeFilter = request('filter') ? request('filter') : 'all';
        $statuses = [
            'all' => 'All',
            'inactive' => 'Inactive',
            'active' => 'Active',
            'rejected' => 'Rejected',
            'suspended' => 'Suspended',
        ];
        $filter = $storeFilter == 'all' ? [1, 0] : [0];

        $stores = $marketplace->stores()->filterStatus($storeFilter)->latest()->with('user', 'marketplace')->paginate(10);

        return view('modules.marketplaces.backend.supervisor-groups.stores.index', compact(
            'user', 'supervisorGroup',
            'marketplace',
            'stores',
            'statuses', 'storeFilter'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SupervisorGroup $supervisorGroup, Store $store)
    {
        $user = auth()->user();
        $isStaff = $supervisorGroup->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplace = get_default_marketplace();
        if ($store->marketplace_id != $marketplace->id) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.supervisor-groups.stores.index', $supervisorGroup->id);
        }

        $provincesWithDetails = IndonesiaProvince::getProvincesWithDetails();
        $dropdown['provinces'] = IndonesiaProvince::getDropdown();
        $provinceId = old('province_id');
        $dropdown['cities'] = IndonesiaCity::getDropdown($provinceId);
        $cityId = old('city_id');
        $dropdown['districts'] = IndonesiaDistrict::getDropdown($cityId);

        $storeMeta = $store->getAllMeta();

        return view('modules.marketplaces.backend.supervisor-groups.stores.show', compact(
            'user', 'supervisorGroup',
            'marketplace',
            'store',
            'storeMeta',
            'provincesWithDetails',
            'dropdown'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
