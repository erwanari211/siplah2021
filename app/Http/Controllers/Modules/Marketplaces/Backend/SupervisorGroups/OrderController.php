<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\SupervisorGroups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\SupervisorGroup;
use App\Models\Modules\Marketplaces\Order;
use App\Models\Modules\Marketplaces\OrderStatus;
use App\Models\Modules\Marketplaces\Store;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SupervisorGroup $supervisorGroup)
    {
        $user = auth()->user();
        $isStaff = $supervisorGroup->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplace = get_default_marketplace();
        $selectedStore = null;

        $groupedOrderStatuses = OrderStatus::getGroupedStatus();
        $groupedOrderStatuses = Order::addOrderCountToMarketplaceOrderStatusesWithRrquestData($marketplace->id, $groupedOrderStatuses, request()->only(['store_id']));
        $orderStatusId = OrderStatus::where('group', 'order_process')->where('key', 'pending')->first()->id;
        if (request('order_status_id')) {
            $orderStatusId = request('order_status_id');
        }
        $selectedOrderStatus = OrderStatus::find($orderStatusId);

        $orders = Order::with('store', 'store.marketplace')->whereHas('store', function($q) use ($marketplace) {
            $q->whereHas('marketplace', function($q2) use ($marketplace) {
                $q2->where('marketplace_id', $marketplace->id);
            });
        });
        $orders = $orders->where('order_status_id', $orderStatusId);
        if (request('store_id')) {
            $orders = $orders->where('store_id', request('store_id'));
            $selectedStore = Store::find(request('store_id'));
        }
        $orders = $orders->orderBy('updated_at', 'desc')->paginate(10);

        return view('modules.marketplaces.backend.supervisor-groups.orders.index', compact(
            'user', 'supervisorGroup',
            'marketplace',
            'selectedStore',
            'orders',
            'groupedOrderStatuses', 'orderStatusId', 'selectedOrderStatus'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SupervisorGroup $supervisorGroup, Order $order)
    {
        $user = auth()->user();
        $isStaff = $supervisorGroup->checkIsStaff($user->id);
        if (!$isStaff) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplace = get_default_marketplace();
        $store = $order->store;
        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();
        $histories = $order->histories()->with('meta')->latest()->get();

        $dropdown['order_statuses'] = OrderStatus::orderBy('value')->find([1,2,3,4,5,6,7])->pluck('key', 'id');
        $dropdown['yes_no'] = [0=>'No', 1=>'Yes'];

        $groupedOrderStatuses = OrderStatus::get()->groupBy('group');

        $activities['negotiation_comments'] = $order->activities()
            ->where('group', 'negotiation')
            // ->where('activity', 'negotiation comment')
            ->with('meta', 'user')
            ->get();
        // return $activities['negotiation_comments'];
        $dropdown['negotiation_price_types'] = [
            'change shipping cost' => 'Change shipping cost',
            'adjustment price' => 'Adjustment price',
            'change product price' => 'Change product price',
        ];
        $dropdownProducts = $products->pluck('product_name', 'id');
        $dropdown['order_products'] = $dropdownProducts;

        $activities['complain_comments'] = $order->activities()
            ->where('group', 'complain')
            ->where('activity', 'complain comment')
            ->with('meta', 'user')
            ->get();
        $activities['documents'] = $order->activities()
            ->where('group', 'document')
            ->with('meta', 'user', 'order')
            ->get();
        $activities['payments'] = $order->activities()
            ->where('group', 'payment')
            ->with('meta', 'user')
            ->get();


        return view('modules.marketplaces.backend.supervisor-groups.orders.show', compact(
            'user', 'supervisorGroup',
            'marketplace', 'store',
            'order', 'products', 'orderMetas', 'histories', 'dropdown',
            'activities',
            'groupedOrderStatuses'
        ));

        /////

        $groupedOrderStatuses = OrderStatus::getGroupedStatus();
        $groupedOrderStatuses = Order::addOrderCountToMarketplaceOrderStatusesWithRrquestData($marketplace->id, $groupedOrderStatuses, request()->only(['store_id']));
        $orderStatusId = OrderStatus::where('group', 'order_process')->where('key', 'pending')->first()->id;
        if (request('order_status_id')) {
            $orderStatusId = request('order_status_id');
        }
        $selectedOrderStatus = OrderStatus::find($orderStatusId);

        $orders = Order::with('store', 'store.marketplace')->whereHas('store', function($q) use ($marketplace) {
            $q->whereHas('marketplace', function($q2) use ($marketplace) {
                $q2->where('marketplace_id', $marketplace->id);
            });
        });
        $orders = $orders->where('order_status_id', $orderStatusId);
        if (request('store_id')) {
            $orders = $orders->where('store_id', request('store_id'));
        }
        $orders = $orders->orderBy('updated_at', 'desc')->paginate(10);

        return view('modules.marketplaces.backend.supervisor-groups.orders.show', compact(
            'user', 'supervisorGroup',
            'marketplace',
            'orders',
            'groupedOrderStatuses', 'orderStatusId', 'selectedOrderStatus'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
