<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity;
use App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict;
use App\Models\Modules\Marketplaces\GroupNotification;

class StoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $defaultFilter = 'inactive';
        $storeFilter = request('filter') ? request('filter') : $defaultFilter;
        $statuses = [
            'all' => 'All',
            'inactive' => 'Inactive',
            'active' => 'Active',
            'rejected' => 'Rejected',
            'suspended' => 'Suspended',
        ];
        $filter = $storeFilter == 'all' ? [1, 0] : [0];

        $stores = $marketplace->stores()->filterStatus($storeFilter)->latest()->with('user', 'marketplace')->paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.stores.index', compact(
            'marketplace', 'stores', 'statuses', 'storeFilter',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Marketplace $marketplace, Store $store)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        $dropdown['jenisUsaha'] = [
            'Individu' => 'Individu',
            'PT' => 'PT',
            'CV' => 'CV',
            'Koperasi' => 'Koperasi',
            'Lainnya' => 'Lainnya',
        ];
        $dropdown['kelasUsaha'] = [
            'Mikro' => 'Mikro',
            'Kecil' => 'Kecil',
            'Sedang' => 'Sedang',
            'Besar' => 'Besar',
        ];
        $dropdown['statusUsaha'] = [
            'PKP' => 'PKP',
            'NON-PKP' => 'NON-PKP',
        ];

        $dropdown['storeStatuses'] = [
            'inactive' => 'Belum Aktif',
            'active' => 'Diterima',
            'rejected' => 'Ditolak',
            'suspended' => 'Dibekukan',
        ];

        $storeMeta = $store->getAllMeta();

        $provincesWithDetails = IndonesiaProvince::getProvincesWithDetails();
        $dropdown['provinces'] = IndonesiaProvince::getDropdown();
        $provinceId = old('province_id');
        $dropdown['cities'] = IndonesiaCity::getDropdown($provinceId);
        $cityId = old('city_id');
        $dropdown['districts'] = IndonesiaDistrict::getDropdown($cityId);

        return view('modules.marketplaces.backend.marketplaces.stores.show', compact(
            'marketplace',
            'store', 'storeMeta',
            'dropdown',
            'provincesWithDetails',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace, Store $store)
    {
        request()->validate([
            'status' => 'required|in:inactive,active,rejected,suspended',
            'note' => 'nullable'
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $status = request('status');
        $note = request('note');

        $notificationData = [];
        $notificationData['data']['old'] = $store->getOriginal();

        if ($status == 'suspended') {
            $store->active = false;
            $store->status = $status;
            $store->save();

            $notificationData['data']['new'] = $store->getOriginal();
            $notificationData['description'] = 'Toko telah dibekukan';
            GroupNotification::addNotification($store, 'store_status', $notificationData);

            $marketplace->staffChangedStoreStatus($store, 'suspended');
        }

        if ($status == 'rejected') {
            $store->active = false;
            $store->status = $status;
            $store->save();

            $notificationData['data']['new'] = $store->getOriginal();
            $notificationData['description'] = 'Toko telah ditolak';
            GroupNotification::addNotification($store, 'store_status', $notificationData);

            $marketplace->staffChangedStoreStatus($store, 'rejected');
        }

        if ($status == 'inactive') {
            $store->active = false;
            $store->status = $status;
            $store->save();

            $marketplace->staffChangedStoreStatus($store, 'inactive');
        }

        if ($status == 'active') {
            $store->active = true;
            $store->status = $status;
            $store->save();

            $notificationData['data']['new'] = $store->getOriginal();
            $notificationData['description'] = 'Toko telah diterima';
            GroupNotification::addNotification($store, 'store_status', $notificationData);

            $marketplace->staffChangedStoreStatus($store, 'active');
        }

        $store->setMeta('store_status_note', $note);

        flash('Data updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
