<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Marketplaces\GroupNotification;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        Product::fillUniqueCode();

        $approvedStatuses = [
            'pending' => 'Pending',
            'approved' => 'Approved',
            'rejected' => 'Rejected',
        ];

        $filters['approved_status'] = request('approved_status') ? request('approved_status') : 'pending';
        $collectionId = request('collection_id');
        $categoryId = request('category_id');
        $products = Product::whereHas('store', function($q) use ($marketplace) {
            $q->where('marketplace_id', $marketplace->id);
        })->with('marketplaceCategories', 'storeCategories', 'meta', 'store');

        $name = request('name');
        $sku = request('sku');
        if ($name) {
            $products = $products->where('name', 'LIKE', "%{$name}%");
            $filters['name'] = request('name') ? request('name') : null;
        }
        if ($sku) {
            $products = $products->where('sku', 'LIKE', "%{$sku}%");
            $filters['sku'] = request('sku') ? request('sku') : null;
        }
        if ($categoryId) {
            $products->whereHas('marketplaceCategories', function($q) use ($categoryId){
                $q->where('marketplace_categories.id', $categoryId);
            });
        }
        if ($collectionId) {
            $products->whereHas('storeCategories', function($q) use ($collectionId){
                $q->where('store_categories.id', $collectionId);
            });
        }

        $storeName = request('store_name');
        if ($storeName) {
            $products->whereHas('store', function($q) use ($storeName){
                $q->where('stores.name', 'like', '%' .  $storeName . '%');
            });
        }

        $storeId = request('store_id');
        if ($storeId) {
            $filters['store_id'] = $storeId;
            $products->whereHas('store', function($q) use ($storeId){
                $q->where('stores.id', $storeId);
            });
        }

        $products = $products
            ->filter($filters)
            ->latest()
            ->paginate(20);

        $categories =  $marketplace->categories()->orderBy('name')->pluck('name', 'id');
        $dropdown['categories'] = $categories;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.products.index', compact(
            'user', 'marketplace',
            'products', 'filters', 'dropdown',
            'approvedStatuses',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Marketplace $marketplace, Product $product)
    {
        $user = auth()->user();
        $store = $product->store;
        $storeMarketplace = $store->marketplace;
        if (!$storeMarketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if ($storeMarketplace->id != $marketplace->id) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        // $dropdown['categories'] = $marketplace->categories()->pluck('name', 'id');
        /*
        $marketplaceCategories = $marketplace->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['children' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $categories = [];
        foreach ($marketplaceCategories as $key => $marketplaceCategory) {
            $marketplaceId = $marketplaceCategory['id'];
            $marketplaceName = $marketplaceCategory['name'];
            $categories[$marketplaceId] = $marketplaceName;
            if (count($marketplaceCategory->children)) {
                foreach ($marketplaceCategory->children as $marketplaceSubCategory) {
                    $marketplaceId = $marketplaceSubCategory['id'];
                    $marketplaceName = '- - '.$marketplaceSubCategory['name'];
                    $categories[$marketplaceId] = $marketplaceName;
                }
            }
        }
        $dropdown['marketplaceCategories'] = $categories;
        */

        $productMarketplaceCategories = $product->marketplaceCategories()->pluck('marketplace_category_id');
        $marketplaceCategories = $marketplace->categories()
            ->whereIn('parent_id', $productMarketplaceCategories)
            ->orderBy('name')
            ->get();

        $oldDropdowns = [];
        foreach ($productMarketplaceCategories as $productMarketplaceCategory) {
            $id = $productMarketplaceCategory;
            $oldDropdowns['data'][$id]['id'] = $id;
            $oldDropdowns['data'][$id]['data'] = [];
        }
        foreach ($marketplaceCategories as $marketplaceCategory) {
            $id = $marketplaceCategory->parent_id;
            $oldDropdowns['data'][$id]['data'][] = $marketplaceCategory;
        }
        // return $oldDropdowns;

        $marketplaceCategories = $marketplace->categories()
            ->where('parent_id', null)
            ->orderBy('name')
            ->get();

        $categories = [];
        foreach ($marketplaceCategories as $key => $marketplaceCategory) {
            $marketplaceId = $marketplaceCategory['id'];
            $marketplaceName = $marketplaceCategory['name'];
            $categories[$marketplaceId] = $marketplaceName;
        }
        $dropdown['marketplaceCategories'] = $categories;
        // return $dropdown['marketplaceCategories'];

        $storeCategories = $store->categories()
            ->where('parent_id', 0)
            ->orderBy('name')
            ->with(['children' => function($q){
                $q->orderBy('name');
            }])
            ->get();
        $collections = [];
        foreach ($storeCategories as $key => $storeCategory) {
            $marketplaceId = $storeCategory['id'];
            $marketplaceName = $storeCategory['name'];
            $collections[$marketplaceId] = $marketplaceName;
            if (count($storeCategory->children)) {
                foreach ($storeCategory->children as $marketplaceSubCategory) {
                    $marketplaceId = $marketplaceSubCategory['id'];
                    $marketplaceName = '- - '.$marketplaceSubCategory['name'];
                    $collections[$marketplaceId] = $marketplaceName;
                }
            }
        }
        $dropdown['storeCategories'] = $collections;

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['statuses'] = ['active'=>'Active', 'inactive'=>'Inactive', 'archived'=>'Archived'];
        $dropdown['types'] = [
            'product'=>'Product',
            'grouped product'=>'Grouped Product',
            'digital product' => 'Digital Product',
            'service' => 'Service',
        ];

        $dropdown['product_status_ketersediaan'] = [
            'ready stock' => 'Ready Stock',
            'preorder' => 'Preorder',
        ];
        $meta_data = $product->getAllMeta();
        $product->meta_data = $meta_data;

        // $productCategories = $product->marketplaceCategories()->pluck('marketplace_category_id');
        $groupedProductDetails = $product->groupedProductDetails()->with('parent', 'product')->get();
        $productMarketplaceCategories = $product->marketplaceCategories()->pluck('marketplace_category_id');
        $productStoreCategories = $product->storeCategories()->pluck('store_category_id');


        $attributes = $product->attributes()->orderBy('sort_order', 'asc')->get();
        $groupedAttributes = $attributes->groupBy('group');

        $galleries = $product->galleries()->orderBy('sort_order', 'asc')->get();

        $mappedKemdikbudZonePrices = [];
        $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
        if ($hasKemdikbudZonePrice) {
            $mappedKemdikbudZonePrices =  $product->getMappedKemdikbudZonePrices();
        }

        $dropdown['approvedStatuses'] = [
            'pending' => 'Pending',
            'approved' => 'Approved',
            'rejected' => 'Rejected',
        ];

        $reason = $product->getMeta('approved_status_reason');
        $product->reason =$reason;
        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.products.show', compact(
            'user', 'store', 'marketplace',
            'product', 'filters', 'dropdown',
            'approvedStatuses',
            'groupedAttributes', 'groupedProductDetails', 'galleries',
            'hasKemdikbudZonePrice', 'mappedKemdikbudZonePrices',
            'productMarketplaceCategories', 'productStoreCategories',
            'oldDropdowns',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace, Product $product)
    {
        $user = auth()->user();
        $store = $product->store;
        $storeMarketplace = $store->marketplace;
        if (!$storeMarketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if ($storeMarketplace->id != $marketplace->id) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        request()->validate([
            'status' => 'required|in:pending,approved,rejected',
            'reason' => 'nullable',
        ]);

        $status = request('status');
        $reason = request('reason');

        if ($status == 'pending') {
            $notificationData['data']['old']['product'] = $product->getOriginal();
            $notificationData['data']['old']['store'] = $store->getOriginal();

            $product->is_approved = 0;
            $product->approved_status = $status;
            $product->approved_at = null;
            $product->save();
            $product->setMeta('approved_status_reason', $reason);

            $notificationData['data']['new']['product'] = $product->getOriginal();
            $notificationData['data']['new']['store'] = $store->getOriginal();
            $notificationData['data']['new']['reason'] = $reason;
            $notificationData['description'] = 'Persetujuan produk dipending';
            GroupNotification::addNotification($store, 'product_approval_pending', $notificationData);

            $marketplace->staffChangedProductStatus($product, 'pending');
        }

        if ($status == 'approved') {
            $notificationData['data']['old']['product'] = $product->getOriginal();
            $notificationData['data']['old']['store'] = $store->getOriginal();

            $product->is_approved = 1;
            $product->approved_status = $status;
            $product->approved_at = date('Y-m-d H:i:s');
            $product->save();
            $product->setMeta('approved_status_reason', $reason);

            $notificationData['data']['new']['product'] = $product->getOriginal();
            $notificationData['data']['new']['store'] = $store->getOriginal();
            $notificationData['data']['new']['reason'] = $reason;
            $notificationData['description'] = 'Produk telah disetujui';
            GroupNotification::addNotification($store, 'product_approval_approved', $notificationData);

            $marketplace->staffChangedProductStatus($product, 'approved');
        }

        if ($status == 'rejected') {
            $notificationData['data']['old']['product'] = $product->getOriginal();
            $notificationData['data']['old']['store'] = $store->getOriginal();

            $product->is_approved = 0;
            $product->approved_status = $status;
            $product->approved_at = null;
            $product->save();
            $product->setMeta('approved_status_reason', $reason);

            $notificationData['data']['new']['product'] = $product->getOriginal();
            $notificationData['data']['new']['store'] = $store->getOriginal();
            $notificationData['data']['new']['reason'] = $reason;
            $notificationData['description'] = 'Produk telah ditolak';
            GroupNotification::addNotification($store, 'product_approval_rejected', $notificationData);

            $marketplace->staffChangedProductStatus($product, 'rejected');
        }

        flash('Product updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
