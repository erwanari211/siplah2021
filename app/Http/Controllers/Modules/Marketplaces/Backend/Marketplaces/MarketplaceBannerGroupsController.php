<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceBannerGroup;
use App\Models\Modules\Website\WebsiteSettings;

class MarketplaceBannerGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $bannerGroups = $marketplace->bannerGroups()->paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.banner-groups.index', compact(
            'marketplace', 'bannerGroups',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = ['normal'=>'Normal', 'slider'=>'Slider'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.banner-groups.create', compact(
            'user', 'marketplace', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace)
    {
        request()->validate([
            'name' => 'required',
            'active' => 'required',
            'type' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $group = new MarketplaceBannerGroup;
        $group->name = request('name');
        $group->display_name = request('display_name');
        $group->active = request('active');
        $group->type = request('type');
        $group->sort_order = request('sort_order');
        $group->marketplace_id = $marketplace->id;
        $group->save();

        flash('Banner group saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Marketplace $marketplace, MarketplaceBannerGroup $bannerGroup)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$bannerGroup->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.banner-groups.index', $marketplace->id);
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = ['normal'=>'Normal', 'slider'=>'Slider'];
        $details = $bannerGroup->details()->with('marketplaceBanner')->has('marketplaceBanner')->get();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.banner-groups.show', compact(
            'user', 'marketplace', 'bannerGroup', 'dropdown', 'details',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Marketplace $marketplace, MarketplaceBannerGroup $bannerGroup)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$bannerGroup->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.banner-groups.index', $marketplace->id);
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['types'] = ['normal'=>'Normal', 'slider'=>'Slider'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.banner-groups.edit', compact(
            'user', 'marketplace', 'bannerGroup', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace, MarketplaceBannerGroup $bannerGroup)
    {
        request()->validate([
            'name' => 'required',
            'active' => 'required',
            'type' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$bannerGroup->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.banner-groups.index', $marketplace->id);
        }

        $group = $bannerGroup;
        $group->name = request('name');
        $group->display_name = request('display_name');
        $group->active = request('active');
        $group->type = request('type');
        $group->sort_order = request('sort_order');
        $group->save();

        flash('Banner updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace, MarketplaceBannerGroup $bannerGroup)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$bannerGroup->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.banner-groups.index', $marketplace->id);
        }

        $bannerGroup->details()->delete();
        $bannerGroup->delete();

        flash('Banner group deleted');
        return redirect()->back();
    }
}
