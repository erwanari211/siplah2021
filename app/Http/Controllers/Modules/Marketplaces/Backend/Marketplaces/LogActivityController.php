<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Marketplaces\GroupActivity;

class LogActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $activities = $marketplace->groupActivities()->with('user', 'groupable')->latest()->paginate(20);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.activities.index', compact(
            'user', 'store', 'marketplace', 'activities',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function userActivitiesIndex(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $groups = [
            'App\Models\Modules\Marketplaces\Store',
            'App\Models\Modules\Marketplaces\School',
            'App\Models\Modules\Marketplaces\SupervisorGroup',
        ];
        $activities = GroupActivity::whereIn('groupable_type', $groups)->with('user', 'groupable')->latest()->paginate(10);

        return view('modules.marketplaces.backend.marketplaces.user-activities.index', compact(
            'user', 'marketplace',
            'activities'
        ));
    }
}
