<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\MarketplaceMessageDetail;
use Illuminate\Http\Request;

class MarketplaceMessageDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceMessageDetail  $marketplaceMessageDetail
     * @return \Illuminate\Http\Response
     */
    public function show(MarketplaceMessageDetail $marketplaceMessageDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceMessageDetail  $marketplaceMessageDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(MarketplaceMessageDetail $marketplaceMessageDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\MarketplaceMessageDetail  $marketplaceMessageDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MarketplaceMessageDetail $marketplaceMessageDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceMessageDetail  $marketplaceMessageDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(MarketplaceMessageDetail $marketplaceMessageDetail)
    {
        //
    }
}
