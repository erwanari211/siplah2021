<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\User;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $users = User::latest();
        if (request('name')) {
            $users = $users->where('name', 'like', '%' . request('name') . '%');
        }
        if (request('email')) {
            $users = $users->where('email', 'like', '%' . request('email') . '%');
        }

        if ($onlineStatus = request('online_status')) {
            if ($onlineStatus == 'online') {
                $users = $users->where('last_online_at', '>=', Carbon::now()->subMinutes(5)->toDateTimeString());
            }

            if ($onlineStatus == 'offline') {
                $users = $users->where('last_online_at', '<', Carbon::now()->subMinutes(5)->toDateTimeString())
                    ->orWhere('last_online_at', null);
            }
        }

        if ($status = request('status')) {
            $users = $users->where('status', $status);
        }

        $users = $users->paginate(10);

        $dropdown['onlineStatuses'] = [
            'online' => 'Online',
            'offline' => 'Offline',
        ];

        $dropdown['statuses'] = [
            'active' => 'active',
            'suspended' => 'suspended',
        ];

        return view('modules.marketplaces.backend.marketplaces.users.index', compact(
            'marketplace',
            'users',
            'dropdown'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Marketplace $marketplace, User $user)
    {
        $currentUser = auth()->user();
        if (!$marketplace->isOwnedBy($currentUser->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['userStatuses'] = [
            'active' => 'Active',
            'suspended' => 'Suspended',
        ];

        return view('modules.marketplaces.backend.marketplaces.users.show', compact(
            'marketplace',
            'user',
            'dropdown'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace, User $user)
    {
        request()->validate([
            'status' => 'required',
            'reason' => 'nullable|required_if:status,suspended',
        ]);

        $currentUser = auth()->user();
        if (!$marketplace->isOwnedBy($currentUser->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        // canty update if curent user
        if ($currentUser->id == $user->id) {
            flash('User cannot be updated')->error();
            return redirect()->back();
        }

        if ($user->hasRole('administrator')) {
            flash('User cannot be updated')->error();
            return redirect()->back();
        }

        $user->status = request('status');
        $user->save();

        $user->setMeta('user_status_reason', request('reason'));

        flash('User updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
