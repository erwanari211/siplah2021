<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\MarketplaceMessage;
use Illuminate\Http\Request;

class MarketplaceMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Markeplaces\MarketplaceMessage  $marketplaceMessage
     * @return \Illuminate\Http\Response
     */
    public function show(MarketplaceMessage $marketplaceMessage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Markeplaces\MarketplaceMessage  $marketplaceMessage
     * @return \Illuminate\Http\Response
     */
    public function edit(MarketplaceMessage $marketplaceMessage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Markeplaces\MarketplaceMessage  $marketplaceMessage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MarketplaceMessage $marketplaceMessage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Markeplaces\MarketplaceMessage  $marketplaceMessage
     * @return \Illuminate\Http\Response
     */
    public function destroy(MarketplaceMessage $marketplaceMessage)
    {
        //
    }
}
