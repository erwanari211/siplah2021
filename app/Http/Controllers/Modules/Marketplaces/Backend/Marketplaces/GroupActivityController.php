<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\GroupActivity;
use Illuminate\Http\Request;

class GroupActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\GroupActivity  $groupActivity
     * @return \Illuminate\Http\Response
     */
    public function show(GroupActivity $groupActivity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\GroupActivity  $groupActivity
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupActivity $groupActivity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\GroupActivity  $groupActivity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupActivity $groupActivity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\GroupActivity  $groupActivity
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupActivity $groupActivity)
    {
        //
    }
}
