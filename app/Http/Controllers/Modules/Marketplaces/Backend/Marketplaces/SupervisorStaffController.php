<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\SupervisorStaff;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Marketplaces\SupervisorGroup;
use App\User;
use Illuminate\Support\Str;

class SupervisorStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $staffs = SupervisorStaff::with('group')->latest()->paginate(20);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.supervisor-staffs.index', compact(
            'user', 'store', 'marketplace', 'staffs',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['roles'] = [
            'admin' => 'Admin',
            'staff' => 'Staff',
        ];

        $dropdown['groups'] = SupervisorGroup::orderBy('name')->pluck('name', 'id');

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.supervisor-staffs.create', compact(
            'user', 'marketplace', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace)
    {
        request()->validate([
            'group' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
            'role' => 'required|in:staff,admin',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        // create user
        $groupId = request('group');
        $email = request('email');
        $password = request('password');
        $userExists = User::where('email', $email)->exists();
        if (!$userExists) {
            flash('User created');
            $user = User::create([
                'name' => $email,
                'username' => Str::random(11),
                'email' => $email,
                'password' => bcrypt($password),
            ]);
        } else {
            flash('User already exists')->warning();
            $user = User::where('email', $email)->first();
        }

        $staffExists = SupervisorStaff::where([
            'user_id' => $user->id,
            'supervisor_group_id' => $groupId,
        ])->exists();

        if ($staffExists) {
            flash('Staff already exists')->warning();
            return redirect()->back();
        }

        $staff = new SupervisorStaff;
        $staff->user_id = $user->id;
        $staff->supervisor_group_id = $groupId;
        $staff->role = request('role');
        $staff->save();

        flash('Supervisor Staff saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\SupervisorStaff  $supervisorStaff
     * @return \Illuminate\Http\Response
     */
    public function show(SupervisorStaff $supervisorStaff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\SupervisorStaff  $supervisorStaff
     * @return \Illuminate\Http\Response
     */
    public function edit(Marketplace $marketplace, SupervisorStaff $supervisorStaff)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['roles'] = [
            'admin' => 'Admin',
            'staff' => 'Staff',
        ];

        $dropdown['yes_no'] = [
            '1' => 'Yes',
            '0' => 'No',
        ];

        $dropdown['groups'] = SupervisorGroup::orderBy('name')->pluck('name', 'id');

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();
        $staff = $supervisorStaff;

        return view('modules.marketplaces.backend.marketplaces.supervisor-staffs.edit', compact(
            'user', 'marketplace', 'dropdown',
            'staff',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\SupervisorStaff  $supervisorStaff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace, SupervisorStaff $supervisorStaff)
    {
        request()->validate([
            'group' => 'required',
            'role' => 'required|in:staff,admin',
            'is_active' => 'required|boolean',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $staff = $supervisorStaff;

        $staff->supervisor_group_id = request('group');
        $staff->role = request('role');
        $staff->is_active = request('is_active');
        $staff->save();

        flash('Supervisor Staff updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\SupervisorStaff  $supervisorStaff
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace, SupervisorStaff $supervisorStaff)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $staff = $supervisorStaff;
        $staff->is_active = false;
        $staff->save();

        flash('Supervisor Staff deleted');
        return redirect()->back();
    }
}
