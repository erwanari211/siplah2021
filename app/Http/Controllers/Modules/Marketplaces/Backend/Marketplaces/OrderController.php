<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\Order;
use App\Models\Modules\Marketplaces\OrderStatus;
use App\Models\Modules\Website\WebsiteSettings;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $groupedOrderStatuses = OrderStatus::getGroupedStatus();
        $groupedOrderStatuses = Order::addOrderCountToMarketplaceOrderStatuses($marketplace->id, $groupedOrderStatuses);
        $orderStatusId = OrderStatus::where('group', 'payment_from_customer')->where('key', 'paid')->first()->id;
        if (request('order_status_id')) {
            $orderStatusId = request('order_status_id');
        }
        $selectedOrderStatus = OrderStatus::find($orderStatusId);

        $orders = Order::with('store', 'store.marketplace')->whereHas('store', function($q) use ($marketplace) {
            $q->whereHas('marketplace', function($q2) use ($marketplace) {
                $q2->where('marketplace_id', $marketplace->id);
            });
        });
        $orders = $orders->where('order_status_id', $orderStatusId);
        $orders = $orders->orderBy('updated_at', 'desc')->paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.orders.index', compact(
            'marketplace',
            'orders',
            'groupedOrderStatuses', 'orderStatusId', 'selectedOrderStatus',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Marketplace $marketplace, Order $order)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $store = $order->store;
        if (!$store->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        $store->updateLastOnline();

        $orderMetas = $order->getOrderMetas();
        $products = $order->products()->with('product')->get();
        $histories = $order->histories()->with('meta')->latest()->get();

        $dropdown['order_statuses'] = OrderStatus::orderBy('value')->find([1,2,3,4,5,6,7])->pluck('key', 'id');
        $dropdown['yes_no'] = [0=>'No', 1=>'Yes'];

        $groupedOrderStatuses = OrderStatus::get()->groupBy('group');

        $activities['negotiation_comments'] = $order->activities()
            ->where('group', 'negotiation')
            // ->where('activity', 'negotiation comment')
            ->with('meta', 'user')
            ->get();
        // return $activities['negotiation_comments'];
        $dropdown['negotiation_price_types'] = [
            'change shipping cost' => 'Change shipping cost',
            'adjustment price' => 'Adjustment price',
            'change product price' => 'Change product price',
        ];
        $dropdownProducts = $products->pluck('product_name', 'id');
        $dropdown['order_products'] = $dropdownProducts;

        $activities['complain_comments'] = $order->activities()
            ->where('group', 'complain')
            ->where('activity', 'complain comment')
            ->with('meta', 'user')
            ->get();
        $activities['documents'] = $order->activities()
            ->where('group', 'document')
            ->with('meta', 'user', 'order')
            ->get();
        $activities['payments'] = $order->activities()
            ->where('group', 'payment')
            ->with('meta', 'user')
            ->get();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.orders.show', compact(
            'user', 'marketplace', 'store',
            'order', 'products', 'orderMetas', 'histories', 'dropdown',
            'activities',
            'groupedOrderStatuses',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
