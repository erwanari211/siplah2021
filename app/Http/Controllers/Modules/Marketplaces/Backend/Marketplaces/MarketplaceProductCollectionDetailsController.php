<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceProductCollection;
use App\Models\Modules\Marketplaces\Product;

class MarketplaceProductCollectionDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace)
    {
        request()->validate([
            'collection_id' => 'required',
            'product_id' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $productId = request('product_id');
        $product = $marketplace->products()->findOrFail($productId);

        $collectionId = request('collection_id');
        $collection = $marketplace->productCollections()->findOrFail($collectionId);

        MarketplaceProductCollectionDetail::updateOrCreate([
            'marketplace_product_collection_id' => $collection->id,
            'product_id' => $product->id,
        ], [
            'sort_order' => request('sort_order')
        ]);

        flash('Product added to collection');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail  $marketplaceProductCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function show(MarketplaceProductCollectionDetail $marketplaceProductCollectionDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail  $marketplaceProductCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(MarketplaceProductCollectionDetail $marketplaceProductCollectionDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail  $marketplaceProductCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MarketplaceProductCollectionDetail $marketplaceProductCollectionDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail  $marketplaceProductCollectionDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace, MarketplaceProductCollection $productCollection, Product $product)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$productCollection->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.product-collections.index', $marketplace->id);
        }

        MarketplaceProductCollectionDetail::where([
            'marketplace_product_collection_id' => $productCollection->id,
            'product_id' => $product->id,
        ])->delete();

        flash('Product removed from collection');
        return redirect()->back();
    }
}
