<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceActivitySearch;
use DB;
use App\Models\Modules\Website\WebsiteSettings;

class MarketplacesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $oneWeekAgo = date('Y-m-d', strtotime('-1 week'));
        $popularSearchThisWeek = MarketplaceActivitySearch::groupBy('search')
            ->select('search', DB::raw('count(*) as total_search'))
            ->where('marketplace_id', $marketplace->id)
            ->whereDate('viewed_at', '>=', $oneWeekAgo)
            ->orderBy('total_search', 'desc')
            ->take(10)
            ->get();

        $oneMonthAgo = date('Y-m-d', strtotime('-1 month'));
        $popularSearchThisMonth = MarketplaceActivitySearch::groupBy('search')
            ->select('search', DB::raw('count(*) as total_search'))
            ->where('marketplace_id', $marketplace->id)
            ->whereDate('viewed_at', '>=', $oneMonthAgo)
            ->orderBy('total_search', 'desc')
            ->take(10)
            ->get();

        $hasCategory = $marketplace->categories()->where('active', 1)->count() ? true : false;

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        $orderData = $marketplace->fetchOrderDataForDashboard();
        $userData = $marketplace->fetchUserDataForDashboard();
        $storeData = $marketplace->fetchStoreDataForDashboard();
        $schoolData = $marketplace->fetchSchoolDataForDashboard();

        return view('modules.marketplaces.backend.marketplaces.index', compact(
            'user', 'marketplace',
            'popularSearchThisWeek', 'popularSearchThisMonth',
            'hasCategory',
            'orderData', 'userData', 'storeData', 'schoolData',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.profile.edit', compact(
            'marketplace',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace)
    {
        request()->validate([
            'name' => 'required',
            'slug' => 'required',
            'image' => 'image|max:4000',
            'logo' => 'image|max:4000',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $marketplace->name = request('name');
        $slug = request('slug') ? str_slug(request('slug')) : str_slug(request('name'));
        $marketplace->slug = Marketplace::getUniqueSlug($slug, $marketplace->id);
        $marketplace->save();

        $image = request('image');
        if ($image) {
            $filename = 'marketplace-profile-pictures';
            $filepath = upload_file($image, 'uploads', $filename);
            $marketplace->image = $filepath;
            $marketplace->save();
        }

        $remove_logo = request('remove_logo');
        if ($remove_logo) {
            $marketplace->logo = null;
            $marketplace->save();
        }

        $logo = request('logo');
        if ($logo) {
            $filename = 'marketplace-logos';
            $filepath = upload_file($logo, 'uploads', $filename);
            $marketplace->logo = $filepath;
            $marketplace->save();
        }

        flash('Marketplace updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
