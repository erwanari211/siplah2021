<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceActivitySearch;
use App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch;
use DB;
use App\Models\Modules\Website\WebsiteSettings;

class MarketplacePromoPopularSearchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $oneMonthAgo = date('Y-m-d', strtotime('-1 month'));
        $popularSearchThisMonth = MarketplaceActivitySearch::groupBy('search')
            ->select('search', DB::raw('count(*) as total_search'))
            ->where('marketplace_id', $marketplace->id)
            ->whereDate('viewed_at', '>=', $oneMonthAgo)
            ->orderBy('total_search', 'desc')
            ->take(25)
            ->get();

        $popularSearches = $marketplace->promoPopularSearches()->orderBy('sort_order')->paginate(20);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.popular-searches.index', compact(
            'marketplace',
            'popularSearches', 'popularSearchThisMonth',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.popular-searches.create', compact(
            'user', 'marketplace', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace)
    {
        request()->validate([
            'search' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $popularSearch = new MarketplacePromoPopularSearch;
        $popularSearch->marketplace_id = $marketplace->id;
        $popularSearch->search = request('search');
        $popularSearch->sort_order = request('sort_order');
        $popularSearch->save();

        flash('Popular search added');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Marketplace $marketplace, MarketplacePromoPopularSearch $popularSearch)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$popularSearch->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.index', $marketplace->id);
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.popular-searches.edit', compact(
            'user', 'marketplace', 'popularSearch',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace, MarketplacePromoPopularSearch $popularSearch)
    {
        request()->validate([
            'search' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$popularSearch->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.index', $marketplace->id);
        }

        $popularSearch->search = request('search');
        $popularSearch->sort_order = request('sort_order');
        $popularSearch->save();

        flash('Popular search updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace, MarketplacePromoPopularSearch $popularSearch)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$popularSearch->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplaces.index', $marketplace->id);
        }

        $popularSearch->delete();

        flash('Popular search deleted');
        return redirect()->back();
    }

    public function imports(Marketplace $marketplace)
    {
        $oneMonthAgo = date('Y-m-d', strtotime('-1 month'));
        $popularSearchThisMonth = MarketplaceActivitySearch::groupBy('search')
            ->select('search', DB::raw('count(*) as total_search'))
            ->where('marketplace_id', $marketplace->id)
            ->whereDate('viewed_at', '>=', $oneMonthAgo)
            ->orderBy('total_search', 'desc')
            ->take(10)
            ->get();

        $marketplace->promoPopularSearches()->delete();

        $i = 1;
        if (count($popularSearchThisMonth)) {
            foreach ($popularSearchThisMonth as $item) {
                $popularSearch = new MarketplacePromoPopularSearch;
                $popularSearch->marketplace_id = $marketplace->id;
                $popularSearch->search = $item->search;
                $popularSearch->sort_order = $i;
                $popularSearch->save();
                $i++;
            }
            flash('Popular searches imported');
        } else {
            flash('Popular searches not imported')->warning();
        }

        return redirect()->back();
    }

    public function import(Marketplace $marketplace)
    {
        $search = request('search');
        if ($search) {
            echo "ada";
            $popularSearch = new MarketplacePromoPopularSearch;
            $popularSearch->marketplace_id = $marketplace->id;
            $popularSearch->search = $search;
            $popularSearch->sort_order = 5;
            $popularSearch->save();

            flash('Popular searches imported');
        } else {
            flash('Popular searches not imported')->warning();
        }

        return redirect()->back();
    }
}
