<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\SupervisorGroup;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Website\WebsiteSettings;

class SupervisorGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        /** @var \App\User|null $user */
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$user->hasRole(['administrator'])) {
            flash('You dont have permission to access previous page')->error();
            if (\URL::current() != \URL::previous()) {
                return redirect()->back();
            } else {
                return redirect()->route('users.marketplaces.index', $marketplace->id);
            }
        }

        $groups = SupervisorGroup::latest()->paginate(20);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.supervisor-groups.index', compact(
            'user', 'store', 'marketplace', 'groups',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Marketplace $marketplace)
    {
        /** @var \App\User|null $user */
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$user->hasRole(['administrator'])) {
            flash('You dont have permission to access previous page')->error();
            if (\URL::current() != \URL::previous()) {
                return redirect()->back();
            } else {
                return redirect()->route('users.marketplaces.index', $marketplace->id);
            }
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.supervisor-groups.create', compact(
            'user', 'marketplace', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace)
    {
        request()->validate([
            'name' => 'required',
            'image' => 'nullable|file|image|max:5000',
        ]);

        /** @var \App\User|null $user */
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$user->hasRole(['administrator'])) {
            flash('You dont have permission to access previous page')->error();
            if (\URL::current() != \URL::previous()) {
                return redirect()->back();
            } else {
                return redirect()->route('users.marketplaces.index', $marketplace->id);
            }
        }

        $group = new SupervisorGroup;
        $group->unique_code = $group->randomUniqueCode();
        $group->name = request('name');
        $group->slug = str_slug(request('name'));

        $image = request('image');
        if ($image) {
            $filename = 'supervisor-groups';
            $filepath = upload_file($image, 'uploads', $filename);
            $group->image = $filepath;
        }
        $group->save();

        flash('Supervisor Group saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\SupervisorGroup  $supervisorGroup
     * @return \Illuminate\Http\Response
     */
    public function show(SupervisorGroup $supervisorGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\SupervisorGroup  $supervisorGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(Marketplace $marketplace, SupervisorGroup $supervisorGroup)
    {
        /** @var \App\User|null $user */
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$user->hasRole(['administrator'])) {
            flash('You dont have permission to access previous page')->error();
            if (\URL::current() != \URL::previous()) {
                return redirect()->back();
            } else {
                return redirect()->route('users.marketplaces.index', $marketplace->id);
            }
        }

        $dropdown['yes_no'] = [
            '1' => 'Yes',
            '0' => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.supervisor-groups.edit', compact(
            'user', 'marketplace', 'dropdown',
            'supervisorGroup',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\SupervisorGroup  $supervisorGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace, SupervisorGroup $supervisorGroup)
    {
        request()->validate([
            'name' => 'required',
            'slug' => 'required|unique:supervisor_groups,slug,' . $supervisorGroup->id,
            'image' => 'nullable|file|image|max:5000',
        ]);

        /** @var \App\User|null $user */
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$user->hasRole(['administrator'])) {
            flash('You dont have permission to access previous page')->error();
            if (\URL::current() != \URL::previous()) {
                return redirect()->back();
            } else {
                return redirect()->route('users.marketplaces.index', $marketplace->id);
            }
        }


        $group = $supervisorGroup;
        $group->name = request('name');
        $group->slug = str_slug(request('slug'));
        $group->is_active = request('is_active');
        $group->status = request('is_active') ? 'active' : 'inactive';
        $image = request('image');
        if ($image) {
            $filename = 'supervisor-groups';
            $filepath = upload_file($image, 'uploads', $filename);
            $group->image = $filepath;
        }
        $group->save();

        flash('Supervisor Group updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\SupervisorGroup  $supervisorGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace, SupervisorGroup $supervisorGroup)
    {
        /** @var \App\User|null $user */
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$user->hasRole(['administrator'])) {
            flash('You dont have permission to access previous page')->error();
            if (\URL::current() != \URL::previous()) {
                return redirect()->back();
            } else {
                return redirect()->route('users.marketplaces.index', $marketplace->id);
            }
        }

        $group = $supervisorGroup;
        $group->is_active = false;
        $group->status = 'inactive';
        $group->save();

        flash('Supervisor Group deleted');
        return redirect()->back();
    }
}
