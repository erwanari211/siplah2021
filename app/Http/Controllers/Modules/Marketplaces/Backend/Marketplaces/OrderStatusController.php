<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\Order;
use App\Models\Modules\Marketplaces\OrderStatus;
use App\Models\Modules\Marketplaces\GroupNotification;

class OrderStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,  Marketplace $marketplace, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'status' => 'required',

            'notify' => 'required|boolean',
            'note' => 'nullable',
            'file' => 'nullable|image|max:4000',
        ]);

        $store = $order->store;
        if (!$store->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        $group = request('group');
        $status = request('status');
        $historyData = request()->only(['notify', 'note', 'file']);
        $order->updateGroupedOrderStatus($group, $status, $historyData);

        flash('Order updated');
        return redirect()->back();
    }

    public function storePaymentToStore(Request $request, Marketplace $marketplace, Order $order)
    {
        request()->validate([
            'group' => 'required',
            'status' => 'required',

            'notify' => 'required|boolean',
            'note' => 'nullable',
            'file' => 'required|image|max:4000',
        ]);

        $store = $order->store;
        if (!$store->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$order->isOwnedByStore($store->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.stores.orders.index', [$store->id]);
        }

        $notificationData['data']['old'] = $order->getOriginal();

        $group = request('group');
        $status = request('status');
        $historyData = request()->only(['notify', 'note', 'file']);
        $order->updateGroupedOrderStatus($group, $status, $historyData);

        $school = $order->school;
        $notificationData['data']['new'] = $order->getOriginal();
        $notificationData['description'] = 'Pembayaran telah diterima oleh Mitra';
        GroupNotification::addNotification($school, 'order_payment_from_customer_received', $notificationData);

        $store = $order->store;
        $notificationData['data']['new'] = $order->getOriginal();
        $notificationData['description'] = 'Mitra telah mentransfer pembayaran';
        GroupNotification::addNotification($store, 'order_payment_to_store', $notificationData);

        $school = $order->school;
        $notificationData['data']['new'] = $order->getOriginal();
        $notificationData['description'] = 'Mitra telah mentransfer pembayaran ke Penyedia';
        GroupNotification::addNotification($school, 'order_payment_to_store_sent', $notificationData);

        flash('Order updated');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
