<?php

namespace App\Http\Controllers\Modules\Marketplaces\Backend\Marketplaces;

use App\Http\Controllers\Controller;
use App\Models\Modules\Marketplaces\MarketplaceStaff;
use Illuminate\Http\Request;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Website\WebsiteSettings;
use App\User;
use Illuminate\Support\Str;

class MarketplaceStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $staffs = $marketplace->staffs()->with('user', 'meta')->latest()->paginate(20);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.staffs.index', compact(
            'user', 'store', 'marketplace', 'staffs',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Marketplace $marketplace)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['roles'] = [
            'admin' => 'Admin',
            'staff' => 'Staff',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.staffs.create', compact(
            'user', 'marketplace', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Marketplace $marketplace)
    {
        request()->validate([
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
            'role' => 'required|in:staff,admin',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        // create user
        $email = request('email');
        $password = request('password');
        $userExists = User::where('email', $email)->exists();
        if (!$userExists) {
            flash('User created');
            $user = User::create([
                'name' => $email,
                'username' => Str::random(11),
                'email' => $email,
                'password' => bcrypt($password),
            ]);
        } else {
            flash('User already exists')->warning();
            $user = User::where('email', $email)->first();
        }

        $staffExists = MarketplaceStaff::where([
            'user_id' => $user->id,
            'marketplace_id' => $marketplace->id,
        ])->exists();

        if ($staffExists) {
            flash('Staff already exists')->warning();
            return redirect()->back();
        }

        $staff = new MarketplaceStaff;
        $staff->user_id = $user->id;
        $staff->marketplace_id = $marketplace->id;
        $staff->role = request('role');
        $staff->save();

        $user->detachRole('administrator');
        if (request('role') == 'administrator' || request('role') == 'admin') {
            $user->attachRole('administrator');
        }

        flash('Staff saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceStaff  $marketplaceStaff
     * @return \Illuminate\Http\Response
     */
    public function show(MarketplaceStaff $marketplaceStaff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceStaff  $marketplaceStaff
     * @return \Illuminate\Http\Response
     */
    public function edit(Marketplace $marketplace, MarketplaceStaff $staff)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$staff->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplace.staffs.index', $marketplace->id);
        }

        $dropdown['roles'] = [
            'admin' => 'Admin',
            'staff' => 'Staff',
        ];

        $dropdown['yes_no'] = [
            '1' => 'Yes',
            '0' => 'No',
        ];

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.marketplaces.backend.marketplaces.staffs.edit', compact(
            'user', 'marketplace', 'dropdown',
            'staff',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Marketplaces\MarketplaceStaff  $marketplaceStaff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marketplace $marketplace, MarketplaceStaff $staff)
    {
        request()->validate([
            'role' => 'required|in:staff,admin',
        ]);

        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$staff->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplace.staffs.index', $marketplace->id);
        }

        $isOwner = $marketplace->checkIsOwner($staff->user_id);
        if ($isOwner) {
            flash('Staff cannot be updated')->error();
            return redirect()->back();
        }

        $staff->role = request('role');
        $staff->save();

        $user = $staff->user;
        $user->detachRole('administrator');
        if (request('role') == 'administrator' || request('role') == 'admin') {
            $user->attachRole('administrator');
        }

        flash('Staff updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Marketplaces\MarketplaceStaff  $marketplaceStaff
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketplace $marketplace, MarketplaceStaff $staff)
    {
        $user = auth()->user();
        if (!$marketplace->isOwnedBy($user->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        if (!$staff->isOwnedByMarketplace($marketplace->id)) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.marketplace.staffs.index', $marketplace->id);
        }

        $isOwner = $marketplace->checkIsOwner($staff->user_id);
        if ($isOwner) {
            flash('Staff cannot be deleted')->error();
            return redirect()->back();
        }

        $staff->is_active = false;
        $staff->save();

        flash('Staff deleted');
        return redirect()->back();
    }
}
