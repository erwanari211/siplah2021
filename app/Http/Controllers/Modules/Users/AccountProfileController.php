<?php

namespace App\Http\Controllers\Modules\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Marketplaces\StoreStaff;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\MarketplaceStaff;
use App\Models\Modules\Marketplaces\SupervisorGroup;
use App\Models\Modules\Marketplaces\SupervisorStaff;
use App\Models\Modules\Marketplaces\School;
use App\Models\Modules\Marketplaces\SchoolStaff;

class AccountProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /** @var \App\User | null $user */
        $user = auth()->user();

        // $userStores = $user->stores;
        $storeIds = StoreStaff::where('user_id', $user->id)->pluck('store_id');
        $userStores = Store::find($storeIds);

        $marketplaceIds = MarketplaceStaff::where('user_id', $user->id)->pluck('marketplace_id');
        $userMarketplaces = Marketplace::find($marketplaceIds);

        $supervisorGroupIds = SupervisorStaff::where('user_id', $user->id)->pluck('supervisor_group_id');
        $userSupervisorGroups = SupervisorGroup::find($supervisorGroupIds);

        $schoolIds = SchoolStaff::where('user_id', $user->id)->pluck('school_id');
        $userSchools = School::find($schoolIds);

        $hasDefaultStoreAddress = $user->hasDefaultStoreAddress();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.user-accounts.backend.profiles.index', compact(
            'user',
            'userStores',
            'userMarketplaces',
            'userSupervisorGroups',
            'userSchools',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        /** @var \App\User | null $user */
        $user = auth()->user();
        request()->validate([
            'name' => 'required',
            'username' => 'required|unique:users,username,'.$user->id,
            'email' => 'required|email|unique:users,email,'.$user->id,
        ]);


        $isSiplahUser = $user->hasMeta('siplah_profile');
        if ($isSiplahUser) {
            $oldEmail = $user->email;
            $newEmail = request('email');
            if ($oldEmail != $newEmail) {
                flash('Email cannot be changed')->warning();
                return redirect()->back()->withInput(request()->all());
            }
        }

        $user->name = request('name');
        $user->username = strtolower(request('username'));
        $user->email = request('email');
        $user->save();

        $image = request('image');
        if ($image) {
            $filename = 'user-photo';
            $filepath = upload_file($image, 'uploads', $filename);
            $user->photo = $filepath;
            $user->save();
        }

        flash('Account updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
