<?php

namespace App\Http\Controllers\Modules\Wilayah\Examples;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Wilayah\Rajaongkir\RajaongkirProvince;
use App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity;
use App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict;

class RajaongkirWilayahController extends Controller
{
    public function index()
    {
        // $provinces = RajaongkirProvince::get();
        // return $provinces;

        // $provincesCount = RajaongkirProvince::count();
        // return $provincesCount;

        // $provinceId = 9; // Jawa Barat
        // $province = RajaongkirProvince::find($provinceId);
        // return $province;

        // $provinces = RajaongkirProvince::search('ja')->get();
        // return $provinces;

        // $cities = RajaongkirCity::with('province')->get();
        // return $cities;

        // $citiesCount = RajaongkirCity::with('province')->count();
        // return $citiesCount;

        // $cityId = 22; // Kab. Bandung
        // $city = RajaongkirCity::with('province')->find($cityId);
        // return $city;

        // $cities = RajaongkirCity::with('province')->search('banyu')->get();
        // return $cities;

        // $provinceId = 9; // Jawa Barat
        // $cities = RajaongkirCity::with('province')->byProvince($provinceId)->get();
        // return $cities;

        // $provinceId = 9; // Jawa Barat
        // $citiesCount = RajaongkirCity::with('province')->byProvince($provinceId)->count();
        // return $citiesCount;

        // $provinceId = 9; // Jawa Barat
        // $cities = RajaongkirCity::with('province')
        //     ->byProvince($provinceId)
        //     ->search('ban')
        //     ->get();
        // return $cities;

        // $cityId = 22; // Kab. Bandung
        // $districts = RajaongkirDistrict::byCity($cityId)->get();
        // return $districts;

        // $cityId = 22; // Kab. Bandung
        // $districtsCount = RajaongkirDistrict::byCity($cityId)->count();
        // return $districtsCount;

        // $districtId = 337; // Soreang
        // $district = RajaongkirDistrict::with('city', 'city.province')->find($districtId);
        // return $district;

        // $districtId = 337; // Soreang
        // $cityId = 22; // Kab. Bandung
        // $districts = RajaongkirDistrict::byCity($cityId)->search('so')->get();
        // return $districts;

        // $provinceId = 9; // Jawa Barat
        // return RajaongkirProvince::with('cities')->find($provinceId);

        // $cityId = 22; // Kab. Bandung
        // return RajaongkirCity::with('districts')->find($cityId);
    }

    public function index2()
    {
        // $provinceId = 9; // Jawa Barat
        // return RajaongkirProvince::with('indonesiaProvince')->find($provinceId);

        // $provinceId = 9; // Jawa Barat
        // $province = RajaongkirProvince::find($provinceId);
        // $indonesiaProvince = $province->indonesiaProvince ? $province->indonesiaProvince->first() : null;
        // return $indonesiaProvince;

        // return RajaongkirProvince::with('indonesiaProvince')
        //     ->doesnthave('indonesiaProvince')
        //     ->get();

        // return RajaongkirProvince::with('indonesiaProvince')
        //     ->has('indonesiaProvince')
        //     ->count();

        // return RajaongkirCity::with('indonesiaCity')
        //     ->has('indonesiaCity')
        //     ->paginate(10);

        // return RajaongkirCity::with('indonesiaCity')
        //     ->has('indonesiaCity')
        //     ->count();

        // return RajaongkirCity::with('indonesiaCity')
        //     ->doesnthave('indonesiaCity')
        //     ->count();

        // $cities = RajaongkirCity::withCount('indonesiaCity')->get();
        // $keyed = $cities->mapWithKeys(function ($item) {
        //     $indonesia_city_count = $item['indonesia_city_count'];
        //     $type = $item['type'];
        //     $city_name = $item['city_name'];
        //     $name = $type.' '.$city_name;
        //     return [$name => [
        //         'id' => $item['city_id'],
        //         'count' => $indonesia_city_count,
        //     ]];
        // });
        // $citiesWithRelations = [];
        // $cities = $keyed->all();
        // foreach ($cities as $city => $item) {
        //     if ($item['count'] != 1) {
        //         $citiesWithRelations[] = RajaongkirCity::with('indonesiaCity')->find($item['id']);
        //     }
        // }
        // return $citiesWithRelations;

        // return RajaongkirDistrict::paginate(10);

        // return RajaongkirDistrict::with('indonesiaDistrict')
        //     ->has('indonesiaDistrict')
        //     ->count();

        // return RajaongkirDistrict::with('indonesiaDistrict')
        //     ->has('indonesiaDistrict')
        //     ->paginate(10);

        // return RajaongkirDistrict::with('indonesiaDistrict')
        //     ->doesnthave('indonesiaDistrict')
        //     ->count();

        // $districts = RajaongkirDistrict::withCount('indonesiaDistrict')->get();
        // $keyed = $districts->mapWithKeys(function ($item) {
        //     $indonesia_district_count = $item['indonesia_district_count'];
        //     $type = $item['type'];
        //     $district_name = $item['district_name'];
        //     $name = $type.' '.$district_name;
        //     return [$name => [
        //         'id' => $item['district_id'],
        //         'count' => $indonesia_district_count,
        //     ]];
        // });
        // $districtsWithRelations = [];
        // $districts = $keyed->all();
        // foreach ($districts as $district => $item) {
        //     if ($item['count'] != 1) {
        //         $districtsWithRelations[] = RajaongkirDistrict::with('indonesiaDistrict')->find($item['id']);
        //     }
        // }
        // return $districtsWithRelations;
    }
}
