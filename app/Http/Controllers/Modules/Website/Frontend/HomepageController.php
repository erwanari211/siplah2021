<?php

namespace App\Http\Controllers\Modules\Website\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Website\WebsiteBannerGroup;
use App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection;
use App\Models\Modules\Marketplaces\WebsiteProductCollection;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Marketplaces\Product;
use DB;

class HomepageController extends Controller
{
    public function displayHomepage()
    {
        return $this->v02();
    }

    public function v01()
    {
        Product::fillUniqueCode();

        $cacheDurationSecond = 600;
        $bannerGroups = Cache::remember('bannerGroups', $cacheDurationSecond, function () {
            return WebsiteBannerGroup::sort()
                ->active()
                ->has('details')
                ->with(['details' => function($q){
                    $q->sort()
                        ->with('websiteBanner')
                        ->whereHas('websiteBanner', function($sq1){
                            $sq1->where('active', 1);
                        });
                }])
                ->get();
            return DB::table('users')->get();
        });

        $marketplaceCollections = Cache::remember('marketplaceCollections', $cacheDurationSecond, function () {
            return WebsiteMarketplaceCollection::active()
                ->sort()
                ->has('details')
                ->with(['details' => function($q){
                    $q->sort()
                        ->with('marketplace')
                        ->whereHas('marketplace', function($sq1){
                            $sq1->where('active', 1);
                        });
                }])
                ->get();
        });

        // Cache::forget('productCollections');
        $productCollections = Cache::remember('productCollections', $cacheDurationSecond, function () {
            return WebsiteProductCollection::active()
                ->sort()
                ->has('details')
                ->with(['details' => function($q){
                     $q->sort()
                        ->with('product', 'product.store', 'product.store.marketplace', 'product.reviews')
                        ->whereHas('product', function($sq1){
                            $sq1->where('active', 1);
                        });
                }])
                ->get();
        });

        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        $marketplaces = Marketplace::where('active', true)->get();
        $isHomepage = true;
        return view('modules.website.frontend.homepage.v01.index', compact(
            'marketplaces',
            'bannerGroups', 'marketplaceCollections', 'productCollections',
            'websiteSettings',
            'isHomepage'
        ));
    }

    public function v02()
    {
        $marketplace = get_default_marketplace();

        $marketplace->addView();
        $marketplaceSettings = $marketplace->settings;
        $marketplaceSettings = $marketplaceSettings->mapWithKeys(function($item){
            return [$item['key'] => $item['value']];
        });
        $stores = $marketplace->stores()->active()->has('addresses')->get();
        $categories = $marketplace->categories()->where('parent_id', null)->orderBy('sort_order')->get();

        $popularSearches = $marketplace->promoPopularSearches()->orderBy('sort_order')->take(5)->get();

        $bannerGroups = $marketplace->bannerGroups()
            ->sort()
            ->active()
            ->has('details')
            ->with(['details' => function($q){
                $q->sort()
                    ->with('marketplaceBanner')
                    ->whereHas('marketplaceBanner', function($sq1){
                        $sq1->where('active', 1);
                    });
            }])
            ->get();

        $storeCollections = $marketplace->storeCollections()
            ->sort()
            ->has('details')
            ->with(['details' => function($q) {
                $q->sort()
                    ->with('store')
                    ->whereHas('store', function($sq1){
                        $sq1->where('active', 1);
                    });
            }])
            ->get();
            // return $storeCollections;

        $productCollections = $marketplace->productCollections()->active()
            ->sort()
            ->has('details')
            ->with(['details' => function($q){
                $q->sort()
                    ->with('product', 'product.store', 'product.store.marketplace', 'product.reviews')
                    ->whereHas('product', function($sq1){
                        $sq1->where('active', 1);
                    });
            }])
            ->get();

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.marketplaces.frontend.marketplaces.show', compact(
            'marketplace', 'marketplaceSettings',
            'bannerGroups', 'categories',
            'popularSearches',
            'stores',
            'storeCollections', 'productCollections',
            'websiteSettings'
        ));
    }
}
