<?php

namespace App\Http\Controllers\Modules\Website\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteBlogPost;
use App\Models\Modules\Website\WebsiteBlogCategory;
use Illuminate\Support\Facades\Cache;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Website\WebsiteBlogBanner;

class BlogPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = WebsiteBlogPost::where([
            ['published_at','<>',null]
        ])
        ->with('user', 'categories')
        ->latest('published_at')
        ->paginate(10);

        $categories = WebsiteBlogCategory::orderBy('sort_order')
            ->withCount('posts')
            ->get();

        $blogBanners = WebsiteBlogBanner::getBlogBanners();

        $isHomepage = true;

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.website.frontend.blog.posts.index', compact(
            'posts', 'categories',
            'blogBanners',
            'isHomepage',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = WebsiteBlogPost::where([
            'slug'=>$slug,
            ['published_at','<>',null]
        ])->with('user', 'categories')->firstOrFail();

        $categories = WebsiteBlogCategory::orderBy('sort_order')
            ->withCount('posts')
            ->get();

        $postMeta = $post->getAllMeta();
        $isHomepage = true;

        $blogBanners = WebsiteBlogBanner::getBlogBanners();
        $products = $post->getRelatedProducts();

        $cacheDurationSecond = 600;
        $websiteSettings = Cache::remember('websiteSettings', $cacheDurationSecond, function () {
            $settings = WebsiteSettings::group('settings')->get();
            $settings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });

            return $settings;
        });

        return view('modules.website.frontend.blog.posts.show', compact(
            'post', 'postMeta', 'categories',
            'blogBanners',
            'products',
            'isHomepage',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
