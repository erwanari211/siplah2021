<?php

namespace App\Http\Controllers\Modules\Website\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteBlogPostProduct;
use Illuminate\Http\Request;
use App\Models\Modules\Website\WebsiteBlogPost;
use App\Models\Modules\Marketplaces\Product;
use App\Models\Modules\Marketplaces\Marketplace;
use App\Models\Modules\Marketplaces\Store;
use App\Models\Modules\Website\WebsiteSettings;

class WebsiteBlogPostProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $postId = request('post_id');
        $post = WebsiteBlogPost::findOrFail($postId);

        $marketplaceId = request('marketplace');
        $storeId = request('store');
        $name = request('name');
        $sku = request('sku');

        $products = Product::has('store')
            ->with('store', 'store.marketplace');
        if ($marketplaceId) {
            $products = $products->whereHas('store.marketplace', function($q) use ($marketplaceId) {
                $q->where('id', $marketplaceId);
            });
        }
        if ($storeId) {
            $products = $products->whereHas('store', function($q) use ($storeId) {
                $q->where('id', $storeId);
            });
        }
        if ($name) {
            $products = $products->where('name', 'LIKE', "%{$name}%");
        }
        if ($sku) {
            $products = $products->where('sku', 'LIKE', "%{$sku}%");
        }
        $products = $products->paginate(20);

        $dropdown['marketplaces'] = [];
        $dropdown['stores'] = [];

        $marketplaces = Marketplace::pluck('name', 'id');
        $dropdown['marketplaces'] = $marketplaces;
        if ($marketplaceId) {
            $stores = Store::where('marketplace_id', $marketplaceId)->pluck('name', 'id');
            $dropdown['stores'] = $stores;
        }

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.blog-posts.products.index', compact(
            'post',
            'products', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'post_id' => 'required',
            'product_id' => 'required',
            'sort_order' => 'required|integer|min:1',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $productId = request('product_id');
        $product = Product::findOrFail($productId);

        $postId = request('post_id');
        $post = WebsiteBlogPost::findOrFail($postId);

        WebsiteBlogPostProduct::updateOrCreate([
            'website_blog_post_id' => $post->id,
            'product_id' => $product->id,
        ], [
            'sort_order' => request('sort_order')
        ]);

        flash('Post related products updated');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteBlogPostProduct  $websiteBlogPostProduct
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteBlogPostProduct $websiteBlogPostProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteBlogPostProduct  $websiteBlogPostProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteBlogPostProduct $websiteBlogPostProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Website\WebsiteBlogPostProduct  $websiteBlogPostProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteBlogPostProduct $websiteBlogPostProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Website\WebsiteBlogPostProduct  $websiteBlogPostProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteBlogPostProduct $blogPostProduct)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $blogPostProduct->delete();

        flash('Product removed');
        return redirect()->back();
    }
}
