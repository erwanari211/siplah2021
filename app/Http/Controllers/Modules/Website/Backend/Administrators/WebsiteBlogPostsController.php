<?php

namespace App\Http\Controllers\Modules\Website\Backend\Administrators;

use App\Http\Controllers\Controller;
use App\Models\Modules\Website\WebsiteBlogPost;
use Illuminate\Http\Request;
use App\Models\Modules\Website\WebsiteSettings;
use App\Models\Modules\Website\WebsiteBlogCategory;

class WebsiteBlogPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $posts = WebsiteBlogPost::with('user', 'categories')->latest()->paginate(10);

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.blog-posts.index', compact(
            'posts',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $dropdown['yes_no'] = [1=>'Yes', 0=>'No'];
        $dropdown['categories'] = WebsiteBlogCategory::pluck('name', 'id');

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.blog-posts.create', compact(
            'user', 'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
            'slug' => 'required|unique:website_blog_posts',
            'content' => 'required',
            'active' => 'required|integer',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $slugCount = WebsiteBlogPost::where('slug', str_slug(request('slug')))->count();
        if ($slugCount > 1) {
            flash('Slug is not unique')->warning();
            return redirect()->back();
        }

        $post = new WebsiteBlogPost;
        $post->unique_code = $post->randomUniqueCode();
        $post->user_id = auth()->user()->id;
        $post->title = request('title');
        $post->slug = str_slug(request('slug'));
        $post->content = request('content');
        $post->excerpt = request('excerpt');
        $post->save();

        if (request('active')) {
            $post->status = 'published';
            $post->published_at = date('Y-m-d H:i:s');
            $post->save();
        }

        $post->setMeta('seo_title', request('seo_title'));
        $post->setMeta('seo_description', request('seo_description'));
        $post->setMeta('seo_keywords', request('seo_keywords'));

        $image = request('featured_image');
        if ($image) {
            $filename = 'blog-post';
            $filepath = upload_file($image, 'uploads', $filename);
            $post->setMeta('featured_image', $filepath);
        }

        $post->categories()->sync(request('category'));

        flash('Post saved');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteBlogPost $blog_post)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $post = $blog_post;
        $dropdown['statuses'] = [
            'draft'=>'Draft',
            'published'=>'Published'
        ];
        $dropdown['categories'] = WebsiteBlogCategory::pluck('name', 'id');

        $postMeta = $post->getAllMeta();
        $postCategories = $post->categories()->pluck('website_blog_categories.id');

        $products = $post->getRelatedProducts();

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.blog-posts.show', compact(
            'user', 'post',
            'postMeta',  'postCategories',
            'dropdown',
            'products',
            'websiteSettings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteBlogPost $blog_post)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $post = $blog_post;
        $dropdown['statuses'] = [
            'draft'=>'Draft',
            'published'=>'Published'
        ];
        $dropdown['categories'] = WebsiteBlogCategory::pluck('name', 'id');

        $postMeta = $post->getAllMeta();
        $postCategories = $post->categories()->pluck('website_blog_categories.id');

        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('modules.website.backend.administrators.cms.blog-posts.edit', compact(
            'user', 'post',
            'postMeta',  'postCategories',
            'dropdown',
            'websiteSettings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteBlogPost $blog_post)
    {
        // return request('category');
        // return request()->all();

        request()->validate([
            'title' => 'required',
            'slug' => 'required|unique:website_blog_posts,slug,'.$blog_post->id,
            'content' => 'required',
            'status' => 'required|in:published,draft',
            'published_at' => 'nullable|required_if:status,published|date_format:Y-m-d H:i',
        ]);

        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $slugCount = WebsiteBlogPost::where([
            'slug' => str_slug(request('slug')),
            ['id', '<>', $blog_post->id]
        ])->count();

        if ($slugCount >= 1) {
            flash('Slug is not unique')->warning();
            return redirect()->back()->withInput(request()->all());
        }

        $post = $blog_post;
        $post->title = request('title');
        $post->slug = str_slug(request('slug'));
        $post->content = request('content');
        $post->excerpt = request('excerpt');
        $post->status = request('status');
        $post->published_at = request('published_at');
        $post->save();

        $post->setMeta('seo_title', request('seo_title'));
        $post->setMeta('seo_description', request('seo_description'));
        $post->setMeta('seo_keywords', request('seo_keywords'));

        $image = request('featured_image');
        if ($image) {
            $filename = 'blog-post';
            $filepath = upload_file($image, 'uploads', $filename);
            $post->setMeta('featured_image', $filepath);
        }

        $post->categories()->sync(request('category'));

        flash('Post updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modules\Website\WebsiteLayoutElement  $websiteLayoutElement
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteBlogPost $blog_post)
    {
        $user = auth()->user();
        if (!$user->hasRole(['superadministrator', 'administrator'])) {
            flash('You dont have permission to access previous page')->error();
            return redirect()->route('users.accounts.profile.index');
        }

        $post = $blog_post;
        $post->delete();

        flash('Post deleted');
        return redirect()->back();
    }
}
