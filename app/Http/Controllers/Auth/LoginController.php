<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\Modules\Website\WebsiteSettings;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/users/account/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        $websiteSettings = WebsiteSettings::getCachedWebsiteSettings();

        return view('auth.login', compact('websiteSettings'));
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'username';

        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
        ];
    }

    public function logout(Request $request){
        $this->beforeLogout();

        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return $this->loggedOut($request) ?: redirect('/login');
    }

    protected function authenticated(Request $request, $user)
    {
        $hasDefaultStoreAddress = $user->hasDefaultStoreAddress();
        if ($hasDefaultStoreAddress) {
            $defaultAddress = $user->getDefaultStoreAddress();
            $city = $defaultAddress->city;
            $kemdikbudCity = $city->kemdikbudCity->first();
            $zone = $kemdikbudCity->zone;
            session(['kemdikbud_zone' => $zone]);
        }
    }

    public function beforeLogout()
    {
        staff_logged_out();
    }

}
