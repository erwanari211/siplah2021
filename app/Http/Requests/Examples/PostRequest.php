<?php

namespace App\Http\Requests\Examples;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'slug' => 'required|unique:example_posts',
            'content' => 'required',
            'excerpt' => 'required',
        ];

        switch ($this->method()) {
            case 'PUT':
            case 'PATCH':
                $post = $this->route('post');
                $rules['slug'] = 'required|unique:example_posts,slug,'.$post->id;
                $rules['published_at'] = 'required|date_format:Y-m-d H:i';
                break;
            default:

                break;
        }

        return $rules;
    }
}
