<?php
namespace App\Http\ViewComposers\Examples;

use Illuminate\View\View;

class SidebarComposer
{
    public function compose(View $view)
    {
        $this->composeCategories($view);
        $this->composeTags($view);
    }

    public function composeCategories(View $view)
    {
            $categories = ['PHP', 'Laravel', 'Javascript'];

            return $view->with('categories', $categories);
    }

    public function composeTags(View $view)
    {
            $tags = ['PHP', 'Laravel', 'Javascript', 'React', 'Vuejs'];

            return $view->with('tags', $tags);
    }
}
