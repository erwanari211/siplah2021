<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Modules\Website\WebsiteLayoutElement;
use Illuminate\Support\Facades\Cache;

class WebsiteFooterLayoutElementsComposer
{
    public function compose(View $view)
    {
        $this->composeFooterLayoutElements($view);
    }

    public function composeFooterLayoutElements(View $view)
    {
        // Cache::forget('websiteFooterLayoutElements');
        $websiteFooterLayoutElements = Cache::remember('websiteFooterLayoutElements', 3600, function () {
            return WebsiteLayoutElement::orderBy('sort_order')
                ->where([
                    'group' => 'footer',
                    'active' => 1
                ])->get();
        });
        $groupedLayoutElements['footer'] = collect($websiteFooterLayoutElements)->groupBy('position');

        return $view->with('groupedLayoutElements', $groupedLayoutElements);
    }
}
