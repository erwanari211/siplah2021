<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Modules\Marketplaces\ShoppingCart;

class NavbarCartComposer
{
    public function compose(View $view)
    {
        $this->composeCart($view);
    }

    public function composeCart(View $view)
    {
        $_navbarCart = ShoppingCart::getCarts();

        return $view->with('_navbarCart', $_navbarCart);
    }
}
