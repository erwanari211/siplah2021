<?php

namespace App\Libraries\Rajaongkir;

use Exception;

class Waybill {
    protected $method = "waybill";
    protected $parameters;
    protected $overWriteOptions = [];

    public function __construct(){
        $this->endPointAPI = config('rajaongkir.end_point_api', 'http://rajaongkir.com/api/starter');
        $this->apiKey = config('rajaongkir.api_key');
    }

    public function check($attributes = [])
    {
        $curl = curl_init();

        $options = [
            CURLOPT_URL => $this->endPointAPI."/".$this->method.$this->parameters,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => [
                "key: ".$this->apiKey
            ],
        ];

        $this->overWriteOptions = [
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query($attributes),
            CURLOPT_HTTPHEADER => [
                "content-type: application/x-www-form-urlencoded",
                "key: ".$this->apiKey
            ]
        ];

        foreach( $this->overWriteOptions as $key => $val){
            $options[$key] = $val;
        }

        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new Exception($err, 1);
        } else {
            $data = json_decode($response, true);
            $code = $data['rajaongkir']['status']['code'];
            if($code == 400){
                throw new Exception($data['rajaongkir']['status']['description'], 1);
            }else{
                return $data['rajaongkir']['result'];
            }
        }
    }
}
