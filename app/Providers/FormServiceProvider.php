<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Form::component('bs3Text', 'components.forms.bs3.text', ['name', 'value' => null, 'attributes' => []]);
        Form::component('bs3Textarea', 'components.forms.bs3.textarea', ['name', 'value' => null, 'attributes' => []]);
        Form::component('bs3Password', 'components.forms.bs3.password', ['name', 'attributes' => []]);
        Form::component('bs3Email', 'components.forms.bs3.email', ['name', 'value' => null, 'attributes' => []]);
        Form::component('bs3File', 'components.forms.bs3.file', ['name', 'attributes' => []]);
        Form::component('bs3Number', 'components.forms.bs3.number', ['name', 'value' => null, 'attributes' => []]);
        Form::component('bs3Date', 'components.forms.bs3.date', ['name', 'value' => null, 'attributes' => []]);
        Form::component('bs3Select', 'components.forms.bs3.select', ['name', 'options'=>[], 'value' => null, 'attributes' => []]);
        Form::component('bs3Submit', 'components.forms.bs3.submit', ['name', 'attributes' => []]);
        Form::component('bs3LinkToRoute', 'components.forms.bs3.link-to-route', ['routeName', 'title'=>null, 'parameters'=>[], 'attributes' => []]);
        Form::component('bs3Datetimepicker', 'components.forms.bs3.datetimepicker', ['name', 'value' => null, 'attributes' => []]);
        Form::component('bs3Datepicker', 'components.forms.bs3.datepicker', ['name', 'value' => null, 'attributes' => []]);
        Form::component('bs3Timepicker', 'components.forms.bs3.timepicker', ['name', 'value' => null, 'attributes' => []]);
        Form::component('bs3LinkToRouteHtml', 'components.forms.bs3.link-to-route-html', ['routeName', 'title'=>null, 'parameters'=>[], 'attributes' => []]);
        Form::component('bs3SubmitHtml', 'components.forms.bs3.submit-html', ['name', 'attributes' => []]);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
