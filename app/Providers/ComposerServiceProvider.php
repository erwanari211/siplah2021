<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('examples.composer-views.sidebar', 'App\Http\ViewComposers\Examples\SidebarComposer');
        view()->composer('themes.bs3.custom.navbar', 'App\Http\ViewComposers\NavbarCartComposer');
        view()->composer('themes.marika-natsuki.custom.navbar-top-btn-cart', 'App\Http\ViewComposers\NavbarCartComposer');
        view()->composer('themes.marika-natsuki.custom.sidebar-cart', 'App\Http\ViewComposers\NavbarCartComposer');
        view()->composer('modules.marketplaces.frontend.inc.navbar-top-btn-cart', 'App\Http\ViewComposers\NavbarCartComposer');
        view()->composer('modules.marketplaces.frontend.inc.sidebar-cart', 'App\Http\ViewComposers\NavbarCartComposer');
        view()->composer('modules.website.frontend.homepage.v01.inc.footer', 'App\Http\ViewComposers\WebsiteFooterLayoutElementsComposer');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
