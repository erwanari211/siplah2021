<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Plank\Metable\Metable;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;
    use Metable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        "last_online_at" => "datetime"
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function hasDefaultStoreAddress()
    {
        return $this->addresses()->where('default_address', 1)->count() > 0;
    }

    public function getDefaultStoreAddress()
    {
        return $this->addresses()->where('default_address', 1)
            ->with('province', 'city', 'district')
            ->first();
    }

    public function addLog($name, $data = [])
    {
        $logData = ['name' => $name];

        if ($data['team'] ?? false) {
            $className = get_class($data['team']);
            if ($className == 'App\Models\Modules\Marketplaces\Marketplace') {
                $logData['team_type'] = 'marketplace';
                $logData['teamable_type'] = $className;
                $logData['teamable_id'] = $data['team']->id;
            }

            if ($className == 'App\Models\Modules\Marketplaces\Store') {
                $logData['team_type'] = 'store';
                $logData['teamable_type'] = $className;
                $logData['teamable_id'] = $data['team']->id;
            }

            if ($className == 'App\Models\Modules\Marketplaces\School') {
                $logData['team_type'] = 'school';
                $logData['teamable_type'] = $className;
                $logData['teamable_id'] = $data['team']->id;
            }

        }

        if ($data['data'] ?? false) {
            $logData['data'] = json_encode($data['data']);
        }

        return $this->userLogs()->create($logData);
    }

    public function updateLastOnline()
    {
        $this->last_online_at = now();
        $this->save();
    }

    public function checkIsOnline()
    {
        $isOnline = false;

        if (is_null($this->last_online_at)) {
            return $isOnline;
        }

        $lastOnlineInterval = 5;
        $isOnline = $this->last_online_at->diffInMinutes(now()) < $lastOnlineInterval;
        return $isOnline;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function addresses()
    {
        // return $this->morphMany('App\Models\Address', 'addressable');
        return $this->hasMany('App\Models\UserAddress');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\Order');
    }

    public function stores()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\Store');
    }

    public function marketplaces()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\Marketplace');
    }

    public function userLogs()
    {
        return $this->hasMany('App\UserLog');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
