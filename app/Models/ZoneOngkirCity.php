<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ZoneOngkirCity extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    protected $primaryKey = 'city_id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function province()
    {
        return $this->belongsTo('App\Models\ZoneOngkirProvince', 'province_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeSelectColumnForDropdown($query)
    {
        return $query->select('city_id', 'type', 'city_name', DB::raw("CONCAT(type, ' ', city_name) AS display_name"));
    }

    public function scopeGetDropdown($query, $provinceId = null)
    {
        $query->selectColumnForDropdown();
        if ($provinceId) {
            $query->where('province_id', $provinceId);
        }
        return $query->pluck('display_name', 'city_id');
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
