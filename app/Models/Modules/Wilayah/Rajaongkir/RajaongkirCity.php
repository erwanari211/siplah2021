<?php

namespace App\Models\Modules\Wilayah\Rajaongkir;

use Illuminate\Database\Eloquent\Model;

class RajaongkirCity extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    protected $primaryKey = 'city_id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function province()
    {
        return $this->belongsTo('App\Models\Modules\Wilayah\Rajaongkir\RajaongkirProvince', 'province_id');
    }

    public function districts()
    {
        return $this->hasMany('App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict', 'city_id');
    }

    public function indonesiaCity()
    {
        return $this->belongsToMany(
            'App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity',
            'indonesia_city_rajaongkir',
            'rajaongkir_city_id',
            'indonesia_city_id'
        )->withPivot('is_exact');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeSearch($query, $name = null)
    {
        if ($name) {
            $query->where('city_name', 'like', "%$name%");
        }

        return $query;
    }

    public function scopeByProvince($query, $provinceId = null)
    {
        if ($provinceId) {
            $query->where('province_id', $provinceId);
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
