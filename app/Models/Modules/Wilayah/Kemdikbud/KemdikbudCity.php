<?php

namespace App\Models\Modules\Wilayah\Kemdikbud;

use Illuminate\Database\Eloquent\Model;

class KemdikbudCity extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    public $incrementing = false;
    protected $keyType = 'string';
    // protected $casts = ['id' => 'string'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getZones()
    {
        return static::select('zone')->groupBy('zone')->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function districts()
    {
        return $this->hasMany('App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict', 'city_id');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Modules\Wilayah\Kemdikbud\KemdikbudProvince', 'province_id');
    }

    public function indonesiaCity()
    {
        return $this->belongsToMany(
            'App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity',
            'indonesia_city_kemdikbud',
            'kemdikbud_city_id',
            'indonesia_city_id'
        );
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeSearch($query, $name = null)
    {
        if ($name) {
            $query->where('name', 'like', "%$name%");
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
