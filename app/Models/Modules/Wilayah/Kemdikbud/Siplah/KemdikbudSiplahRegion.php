<?php

namespace App\Models\Modules\Wilayah\Kemdikbud\Siplah;

use Illuminate\Database\Eloquent\Model;

class KemdikbudSiplahRegion extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    protected $primaryKey = 'region_id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    public $incrementing = false;
    protected $keyType = 'string';
    // protected $casts = ['id' => 'string'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function indonesiaCity()
    {
        return $this->belongsTo(
            'App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity',
            'indonesia_city_id'
        );
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
