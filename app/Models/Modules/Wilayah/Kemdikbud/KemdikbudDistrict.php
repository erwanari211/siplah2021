<?php

namespace App\Models\Modules\Wilayah\Kemdikbud;

use Illuminate\Database\Eloquent\Model;

class KemdikbudDistrict extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    public $incrementing = false;
    protected $keyType = 'string';
    // protected $casts = ['id' => 'string'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function city()
    {
        return $this->belongsTo('App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity', 'city_id');
    }

    public function indonesiaDistrict()
    {
        return $this->belongsToMany(
            'App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict',
            'indonesia_district_kemdikbud',
            'kemdikbud_district_id',
            'indonesia_district_id'
        );
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeSearch($query, $name = null)
    {
        if ($name) {
            $query->where('name', 'like', "%$name%");
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
