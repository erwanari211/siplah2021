<?php

namespace App\Models\Modules\Wilayah\WilayahAdministratif;

use Illuminate\Database\Eloquent\Model;

class IndonesiaCity extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    public $incrementing = false;
    protected $keyType = 'string';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getDropdown($provinceId = null)
    {
        $cities = static::orderBy('name');
        if ($provinceId) {
            $cities = $cities->where('province_id', $provinceId);
        }
        $cities = $cities->pluck('name', 'id');
        return $cities;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function districts()
    {
        return $this->hasMany('App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict', 'city_id');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince', 'province_id');
    }

    public function kemdikbudCity()
    {
        return $this->belongsToMany(
            'App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity',
            'indonesia_city_kemdikbud',
            'indonesia_city_id',
            'kemdikbud_city_id'
        );
    }

    public function rajaongkirCity()
    {
        return $this->belongsToMany(
            'App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity',
            'indonesia_city_rajaongkir',
            'indonesia_city_id',
            'rajaongkir_city_id'
        )->withPivot('is_exact');
    }

    public function kemdikbudSiplahCity()
    {
        return $this->hasOne( 'App\Models\Modules\Wilayah\Kemdikbud\Siplah\KemdikbudSiplahRegion' );
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeSearch($query, $name = null)
    {
        if ($name) {
            $query->where('name', 'like', "%$name%");
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
