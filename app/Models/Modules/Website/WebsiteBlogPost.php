<?php

namespace App\Models\Modules\Website;

use Illuminate\Database\Eloquent\Model;
use Plank\Metable\Metable;

class WebsiteBlogPost extends Model
{
    use Metable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];
    protected $with = ['meta'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function randomUniqueCode()
    {
        $table = 'website_blog_posts';
        $column = 'unique_code';

        $id = strtolower(str_random(8));
        $validator = \Validator::make(['id'=>$id],['id'=>'unique:'.$table.','.$column]);
        if($validator->fails()){
            $this->randomUniqueCode();
        }

        return $id;
    }

    public function getRelatedProducts()
    {
        return $this->products()->has('product')
            ->with('product.store', 'product.store.marketplace')
            ->orderBy('sort_order')
            ->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Modules\Website\WebsiteBlogCategory',
            'website_blog_category_post', 'post_id', 'category_id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Modules\Website\WebsiteBlogPostProduct')->with('product');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
