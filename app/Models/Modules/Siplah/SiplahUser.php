<?php

namespace App\Models\Modules\Siplah;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Str;

class SiplahUser extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'siplah_user_id', 'user_id',
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function createNewUser($profile)
    {
        $emailAlreadyExists = false;
        $isValidEmail = false;
        $email = strtolower(Str::random(16)) . '@randomemail.com';
        $username = $profile->username;
        $userUuid = $profile->pengguna_id;

        if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $isValidEmail = true;
            $email = $username;
        }

        if ($isValidEmail) {
            $emailAlreadyExists = User::where('email', $username)->exists();
        }

        if (!$emailAlreadyExists) {
            $user = static::createUserFromSiplahProfile($email, $profile);
        }

        if ($emailAlreadyExists) {
            $user = User::where('email', $profile->username)->first();
        }

        $siplahUser = static::firstOrCreate([
            'siplah_user_id' => $userUuid,
        ], [
            'user_id' => $user->id,
        ]);

        return $siplahUser;
    }

    public static function createUserFromSiplahProfile($email, $profile)
    {
        $user = new User;
        $user->name = $profile->nama;
        $user->username = $profile->username;
        $user->email = $email;
        $user->password = bcrypt(Str::random(8));
        $user->save();

        $siplahProfile = clone $profile;
        unset($siplahProfile->sekolah_id);
        unset($siplahProfile->jenis);
        unset($siplahProfile->jabatan);
        $user->setMeta('siplah_profile', $siplahProfile);

        return $user;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
