<?php

namespace App\Models\Modules\Marketplaces;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Plank\Metable\Metable;

class Product extends Model
{
    use Metable;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];
    protected $with = ['meta'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedByStore($storeId)
    {
        return $storeId == $this->store_id;
    }

    public function isOwnedByMarketplace($marketplaceId)
    {
        $marketplace = $this->store->marketplace;
        return $marketplaceId == $marketplace->id;
    }

    public static function getUniqueSlug($slug, $id = null, $index = 0)
    {
        $newSlug = ($index == 0) ? $slug : $slug.'--'.$index;
        $rules = ['slug'=>'unique:products,slug'];
        if ($id) {
            $rules = ['slug'=>'unique:products,slug,'.$id];
        }

        $validator = \Validator::make(['slug'=>$newSlug],$rules);
        if($validator->fails()){
            $index++;
            return static::getUniqueSlug($slug, $id, $index);
        }

        return $newSlug;
    }

    public static function checkIsUniqueSku($id)
    {
        $isUnique = true;
        $product = Product::find($id);
        $count = Product::where([
            'sku' => $product->sku,
            'store_id' => $product->store_id,
        ])->whereNotNull('sku')->count();

        if ($count > 1) {
            $isUnique = false;
        }

        return $isUnique;
    }

    public static function randomUniqueCode()
    {
        $table = 'products';
        $column = 'unique_code';

        $id = strtolower(str_random(8));
        $validator = \Validator::make(['id'=>$id],['id'=>'unique:'.$table.','.$column]);
        if($validator->fails()){
            static::randomUniqueCode();
        }

        return $id;
    }

    public static function fillUniqueCode()
    {
        $total = 0;
        $products = static::where(function($query) {
            $query->where('unique_code', null);
            $query->orWhere('unique_code', '');
        })->get();

        foreach ($products as $product) {
            if (!$product->unique_code) {
                $uniqueCode = $product->randomUniqueCode();
                $product->unique_code = $uniqueCode;
                $product->save();
                $total++;
            }
        }

        if ($total) {
            flash($total.' row affected (fill product blank Unique Code)');
        }
    }

    public function viewedByUser($product, $marketplace, $store)
    {
        $product->activityViews()->updateOrCreate([
            'user_id' => auth()->check() ? auth()->user()->id : null,
            'marketplace_id' => $marketplace->id,
            'store_id' => $store->id,
            'product_id' => $product->id,
            'ip_address' => request()->ip(),
            [\DB::raw("DATE(viewed_at)"), date('Y-m-d') ]
        ], [
            'viewed_at' => date('Y-m-d H:i:s'),
        ]);

        $product->increment('views');
    }

    public function getMappedKemdikbudZonePrices()
    {
        $prices = $this->kemdikbudZonePrices;
        $keyed = collect($prices)->mapWithKeys(function ($item) {
            return [$item['zone'] => $item['price']];
        });

        $kemdikbudPrices = $keyed->all();
        return $kemdikbudPrices;
    }

    public function getKemdikbudMinMaxZonePrices($data = null)
    {
        $prices = $data;
        if (!$data) {
            $prices = $this->getMappedKemdikbudZonePrices();
        }

        $price = 0;
        $minPrice = $this->getKemdikbudMinZonePrices($price);
        $maxPrice = $this->getKemdikbudMaxZonePrices($price);

        $result = compact('minPrice', 'maxPrice', 'prices');
        return $result;
    }

    public function getKemdikbudMinZonePrices($data = null)
    {
        $minPrice = 0;
        $prices = $data;
        if (!$data) {
            $prices = $this->getMappedKemdikbudZonePrices();
        }

        $index = 0;
        $temp = 0;

        foreach ($prices as $zone => $zonePrice) {
            if ($index == 0) {
                $minPrice = $temp = $zonePrice;
            }

            if ($index > 0) {
                if ($zonePrice < $temp) {
                    $minPrice = $zonePrice;
                }
            }
            $index++;
        }

        return $minPrice;
    }

    public function getKemdikbudMaxZonePrices($data = null)
    {
        $maxPrice = 0;
        $prices = $data;
        if (!$data) {
            $prices = $this->getMappedKemdikbudZonePrices();
        }

        $index = 0;
        $temp = 0;

        foreach ($prices as $zone => $zonePrice) {
            if ($index == 0) {
                $maxPrice = $temp = $zonePrice;
            }

            if ($index > 0) {
                if ($zonePrice > $temp) {
                    $maxPrice = $zonePrice;
                }
            }
            $index++;
        }

        return $maxPrice;
    }

    public function getKemdikbudFormattedZonePrices($minPrice = 0, $maxPrice = 0)
    {
        $formattedZonePrice = 0;
        if ($minPrice == $maxPrice) {
            $formattedZonePrice = "Rp.".formatNumber($maxPrice);
        }
        if ($minPrice != $maxPrice) {
            $formattedZonePrice = "Rp.".formatNumber($minPrice).' - '."Rp.".formatNumber($maxPrice);
        }

        return $formattedZonePrice;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\Store');
    }

    public function storeCategories()
    {
        return $this->belongsToMany('App\Models\Modules\Marketplaces\StoreCategory', 'store_category_product');
    }

    public function marketplaceCategories()
    {
        return $this->belongsToMany('App\Models\Modules\Marketplaces\MarketplaceCategory', 'marketplace_category_product');
    }

    public function attributes()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\ProductAttribute');
    }

    public function groupedProductDetails()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\GroupedProductDetail', 'parent_id');
    }

    public function discussions()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\ProductDiscussion');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\ProductReview');
    }

    public function activityViews()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\ProductActivityView');
    }

    public function orderProducts()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\OrderProduct');
    }

    public function galleries()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\ProductGallery');
    }

    public function kemdikbudZonePrices()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeActive($query)
    {
        return $query->where('products.active', 1);
    }

    public function scopeApproved($query)
    {
        return $query->where('is_approved', 1);
    }

    public function scopeStatus($query, $value = ['active'])
    {
        if (is_array($value)) {
            return $query->whereIn('products.status', $value);
        }

        return $query->where('products.status', $value);
    }

    public function scopeFilter($query, $filters = [])
    {
        if (is_array($filters)) {
            if ($filters['active'] ?? false) {
                $active = $filters['active'] == 'true' ? true : false;
                $query->where('active', $active);
            }
            if ($filters['status'] ?? false) {
                $query->where('status', $filters['status']);
            }

            if ($filters['approved_status'] ?? false) {
                $query->where('approved_status', $filters['approved_status']);
            }
        }

        return $query;
    }

    public function scopeSearch($query, $search = null)
    {
        if ($search) {
            return $query->where('products.name', 'like', "%$search%");
        }

        return $query;
    }

    public function scopeSortByNearest($query, $lat = null, $long = null)
    {
        if (is_numeric($lat) && is_numeric($long)) {
            $sf = 3.14159 / 180; // scaling factor
            $r_earth =  6371;

            $latColumn = "`stores`.`latitude`";
            $longColumn = "`stores`.`longitude`";

            $query->orderByRaw("(
                {$r_earth} *
                acos (
                    cos({$lat}*{$sf}) * cos({$latColumn}*{$sf}) * cos(({$longColumn}*{$sf}) - ({$long}*{$sf})) +
                    sin({$lat}*{$sf}) * sin({$latColumn}*{$sf})
                ) *
                1000
            )");
        }

        return $query;
    }

    public function scopeFilterByDistance($query, $lat = null, $long = null, $distance = 0)
    {
        if (is_numeric($lat) && is_numeric($long) && is_numeric($distance)) {
            $sf = 3.14159 / 180; // scaling factor
            $r_earth =  6371;

            $latColumn = "`stores`.`latitude`";
            $longColumn = "`stores`.`longitude`";

            $query->whereRaw("
                (
                    {$r_earth} *
                    acos (
                        cos({$lat}*{$sf}) * cos({$latColumn}*{$sf}) * cos(({$longColumn}*{$sf}) - ({$long}*{$sf})) +
                        sin({$lat}*{$sf}) * sin({$latColumn}*{$sf})
                    ) *
                    1000
                ) <= {$distance}
            ");
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getActiveButtonAttribute($value)
    {
        if ($this->active) {
            $label = 'Active';
            $class = 'btn btn-success btn-xs';
        } else {
            $label = 'Inactive';
            $class = 'btn btn-danger btn-xs';
        }

        return '<span class="'.$class.'">'.$label.'</span>';
    }

    public function getStatusButtonAttribute($value)
    {
        $label = 'Inactive';
        $class = 'btn btn-danger btn-xs';

        if ($this->status == 'active') {
            $label = ucwords($this->status);
            $class = 'btn btn-success btn-xs';
        }

        if ($this->status == 'inactive') {
            $label = ucwords($this->status);
            $class = 'btn btn-danger btn-xs';
        }

        if ($this->status == 'archived') {
            $label = ucwords($this->status);
            $class = 'btn btn-warning btn-xs';
        }

        return '<span class="'.$class.'">'.$label.'</span>';
    }

    public function getApprovedButtonAttribute($value)
    {
        if ($this->is_approved) {
            $label = 'Approved';
            $class = 'btn btn-success btn-xs';
        } else {
            $label = 'Unapproved';
            $class = 'btn btn-danger btn-xs';
        }

        return '<span class="'.$class.'">'.$label.'</span>';
    }

    public function getApprovedStatusButtonAttribute($value)
    {
        $label = 'Pending';
        $class = 'btn btn-warning btn-xs';

        if ($this->approved_status == 'pending') {
            $label = ucwords($this->approved_status);
            $class = 'btn btn-warning btn-xs';
        }

        if ($this->approved_status == 'approved') {
            $label = ucwords($this->approved_status);
            $class = 'btn btn-success btn-xs';
        }

        if ($this->approved_status == 'rejected') {
            $label = ucwords($this->approved_status);
            $class = 'btn btn-danger btn-xs';
        }

        return '<span class="'.$class.'">'.$label.'</span>';
    }

    public function getImageUrlAttribute($value)
    {
        $noImageUrl = 'images/no-image-v2.png';
        $imageUrl = '';
        if ($this->image) {
            $imageUrl = $this->image;
        } else {
            $imageUrl = $noImageUrl;
        }

        return asset($imageUrl);
    }

    public function getProductUrlAttribute($value)
    {
        $productSlug = $this->slug;
        $storeSlug = $this->store->slug;
        $marketplaceSlug = $this->store->marketplace->slug;
        return route('products.show', [$marketplaceSlug, $storeSlug, $productSlug]);
    }

    public function getDiscountAmountPercentAttribute($value='')
    {
        $discount = false;
        $originalPrice = $this->getmeta('original_price');
        $price = $this->price;
        if ($originalPrice) {
            if ($originalPrice > $price) {
                $diff = $originalPrice - $price;
                $discount = round($diff / $originalPrice * 100);
            }
        }

        return $discount;
    }

    public function getIsNewProductAttribute()
    {
        $createdAt = $this->created_at;
        $now = Carbon::now();
        $diff = $createdAt->diffInDays($now);
        return $diff <= 7;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
