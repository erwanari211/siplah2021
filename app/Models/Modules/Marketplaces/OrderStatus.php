<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getGroupedStatus()
    {
        $data = [];
        $groupedStatus = static::get()->groupBy('group');

        $pending = $groupedStatus['order_process']->where('key', 'pending')->first();
        $confirmed = $groupedStatus['order_confirmation']->where('key', 'confirmed')->first();
        $data['confirmation']['pending']['info'] = $pending;
        $data['confirmation']['confirmed']['info'] = $confirmed;

        $processing = $groupedStatus['packing']->where('key', 'processing')->first();
        $processed = $groupedStatus['packing']->where('key', 'processed')->first();
        $data['packing']['processing']['info'] = $processing;
        $data['packing']['processed']['info'] = $processed;

        $shipping = $groupedStatus['shipping']->where('key', 'shipping')->first();
        $shipped = $groupedStatus['shipping']->where('key', 'shipped')->first();
        $data['shipping']['shipping']['info'] = $shipping;
        $data['shipping']['shipped']['info'] = $shipped;

        $delivered = $groupedStatus['delivery']->where('key', 'delivered')->first();
        $data['delivery']['delivery']['info'] = $delivered;

        $completed = $groupedStatus['delivery_document']->where('key', 'completed')->first();
        $data['delivery_document']['completed']['info'] = $completed;

        $unpaid = $groupedStatus['payment_from_customer']->where('key', 'unpaid')->first();
        $paid = $groupedStatus['payment_from_customer']->where('key', 'paid')->first();
        $data['payment_from_customer']['unpaid']['info'] = $unpaid;
        $data['payment_from_customer']['paid']['info'] = $paid;

        $unpaid = $groupedStatus['payment_to_store']->where('key', 'unpaid')->first();
        $paid = $groupedStatus['payment_to_store']->where('key', 'paid')->first();
        $data['payment_to_store']['unpaid']['info'] = $unpaid;
        $data['payment_to_store']['paid']['info'] = $paid;

        $completed = $groupedStatus['order_process']->where('key', 'completed')->first();
        $data['completed']['completed']['info'] = $completed;

        $unresolved = $groupedStatus['complain_status']->where('key', 'unresolved')->first();
        $resolved = $groupedStatus['complain_status']->where('key', 'resolved')->first();
        $escalationSolution = $groupedStatus['complain_status']->where('key', 'escalation_solution')->first();
        $data['complain']['unresolved']['info'] = $unresolved;
        $data['complain']['resolved']['info'] = $resolved;
        $data['complain']['escalation_solution']['info'] = $escalationSolution;

        $canceledByCustomer = $groupedStatus['canceled']->where('key', 'canceled_by_customer')->first();
        $canceledByStore = $groupedStatus['canceled']->where('key', 'canceled_by_store')->first();
        $data['canceled']['canceled_by_customer']['info'] = $canceledByCustomer;
        $data['canceled']['canceled_by_store']['info'] = $canceledByStore;

        $rejected = $groupedStatus['order_process']->where('key', 'rejected')->first();
        $data['rejected']['rejected']['info'] = $rejected;


        return $data;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
