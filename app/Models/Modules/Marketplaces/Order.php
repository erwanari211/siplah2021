<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;
use App\Models\Modules\Marketplaces\OrderStatus;
use App\Models\Modules\Marketplaces\OrderHistory;

class Order extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'store_id', 'user_id', 'store_name', 'user_name', 'total', 'currency',
        'order_status_id', 'order_status',
        'invoice_prefix', 'invoice_no',
        'ip', 'user_agent',
        'payment_status', 'shipping_status',
        'status_order_process',
        'status_order_confirmation', 'status_packing', 'status_shipping',
        'status_delivery', 'status_delivery_document',
        'status_payment_from_customer', 'status_payment_to_store',
        'status_changed_by_customer',
        'status_canceled',
        'status_complain_source', 'status_complain_request', 'status_complain_status',
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedBy($userId)
    {
        return $userId == $this->user_id;
    }

    public function isOwnedByStore($storeId)
    {
        return $storeId == $this->store_id;
    }

    public function isOwnedBySchool($schoolId)
    {
        return $schoolId == $this->school_id;
    }

    public function getOrderMetas()
    {
        $metas = $this->orderMetas()
            ->orderBy('group')
            ->orderBy('sort_order')
            ->get();

        $groupedMetas = [];
        foreach ($metas as $meta) {
            $group = $meta['group'];
            $key = $meta['key'];
            $value = $meta['value'];
            $groupedMetas[$group][$key] = $value;
        }
        return $groupedMetas;
    }

    public static function getStoreOrderStatuses($storeId)
    {
        $statuses = ['pending', 'paid', 'shipping', 'completed', 'canceled'];
        $orderStatuses = [];
        foreach ($statuses as $status) {
            $count = static::where('store_id', $storeId)->filter($status)->count();
            $orderStatuses[$status] = [
                'status' => $status,
                'total' => $count,
            ];
        }
        return $orderStatuses;
    }

    public static function getUserOrderStatuses($userId)
    {
        $statuses = ['pending', 'paid', 'shipping', 'completed', 'canceled'];
        $orderStatuses = [];
        foreach ($statuses as $status) {
            $count = static::where('user_id', $userId)->filter($status)->count();
            $orderStatuses[$status] = [
                'status' => $status,
                'total' => $count,
            ];
        }
        return $orderStatuses;
    }

    public function updateGroupedOrderStatus($group, $status, $historyData = [])
    {
        if ($group == 'order_confirmation') {
            if ($status == 'confirmed') {
                $this->update([
                    'status_order_process' => 'processing',
                    'status_order_confirmation' => 'confirmed',
                ]);

                $this->updateOrderStatus($group, $status);
                $this->addeOrderHistory($group, $status, $historyData);
            }

            if ($status == 'not_confirmed') {
                $this->update([
                    'status_order_process' => 'rejected',
                    'status_order_confirmation' => 'not_confirmed',
                ]);

                $this->updateOrderStatus('order_process', 'rejected');
                $this->addeOrderHistory('order_process', 'rejected', $historyData);
            }
        }

        if ($group == 'packing') {
            $this->update([
                'status_order_process' => 'processing',
                'status_packing' => $status,
            ]);

            $this->updateOrderStatus($group, $status);
            $this->addeOrderHistory($group, $status, $historyData);
        }

        if ($group == 'shipping') {
            $this->update([
                'status_order_process' => 'processing',
                'status_shipping' => $status,
            ]);

            $this->updateOrderStatus($group, $status);
            $this->addeOrderHistory($group, $status, $historyData);
        }

        if ($group == 'delivery') {
            $this->update([
                'status_order_process' => 'processing',
                'status_delivery' => $status,
            ]);

            $this->updateOrderStatus($group, $status);
            $this->addeOrderHistory($group, $status, $historyData);
        }

        if ($group == 'delivery_document') {
            $this->update([
                'status_order_process' => 'processing',
                'status_delivery_document' => $status,
            ]);

            $this->updateOrderStatus($group, $status);
            $this->addeOrderHistory($group, $status, $historyData);
        }

        if ($group == 'payment_from_customer') {
            $this->update([
                'status_order_process' => 'processing',
                'status_payment_from_customer' => $status,
            ]);

            $this->updateOrderStatus($group, $status);
            $this->addeOrderHistory($group, $status, $historyData);
        }

        if ($group == 'payment_to_store') {
            $this->update([
                'status_order_process' => 'processing',
                'status_payment_to_store' => $status,
            ]);

            $this->updateOrderStatus($group, $status);
            $this->addeOrderHistory($group, $status, $historyData);
        }

        if ($group == 'order_process') {
            if ($status == 'completed') {
                $this->update([
                    'status_order_process' => $status,
                ]);

                $this->updateOrderStatus($group, $status);
                $this->addeOrderHistory($group, $status, $historyData);
            }
        }

    }

    public function updateOrderStatus($group, $status)
    {
        $orderStatus = OrderStatus::where([
            'group' => $group,
            'key' => $status,
        ])->first();

        $this->order_status_id = $orderStatus->id;
        $this->order_status = $orderStatus->value;
        $this->save();
    }

    public function addeOrderHistory($group, $status, $historyData = [])
    {
        $orderStatus = OrderStatus::where([
            'group' => $group,
            'key' => $status,
        ])->first();

        $history = new OrderHistory;
        $history->order_id = $this->id;
        $history->order_status_id = $orderStatus->id;
        $history->order_status = $orderStatus->value;
        $history->notify = $historyData['notify'] ?? false;
        $history->note = $historyData['note'] ?? null;
        $history->has_attachment = false;
        $history->save();

        $file = $historyData['file'] ?? false;
        if ($file) {
            $filename = 'order-history-attachment';
            $filepath = upload_file($file, 'uploads', $filename);
            $history->has_attachment = true;
            $history->save();
            $history->setMeta('attachments', $filepath);
        }
    }

    public static function addOrderCountToMarketplaceOrderStatuses($marketplaceId, $data)
    {
        foreach ($data as $group => $groupData) {
            foreach ($groupData as $status => $statusData) {
                $info = $statusData['info'];
                $count = static::with('store', 'store.marketplace')->whereHas('store', function($q) use ($marketplaceId) {
                    $q->whereHas('marketplace', function($q2) use ($marketplaceId) {
                        $q2->where('marketplace_id', $marketplaceId);
                    });
                });
                $count = $count->where('order_status_id', $info->id)->count();
                $data[$group][$status]['count'] = $count;
            }
        }
        return $data;
    }

    public static function addOrderCountToMarketplaceOrderStatusesWithRrquestData($marketplaceId, $data, $requestData = [])
    {
        foreach ($data as $group => $groupData) {
            foreach ($groupData as $status => $statusData) {
                $info = $statusData['info'];
                $count = static::with('store', 'store.marketplace')->whereHas('store', function($q) use ($marketplaceId, $requestData) {
                    $q->whereHas('marketplace', function($q2) use ($marketplaceId) {
                        $q2->where('marketplace_id', $marketplaceId);
                    });

                    if ($requestData['store_id'] ?? false) {
                        $q->where('store_id', $requestData['store_id']);
                    }
                });
                $count = $count->where('order_status_id', $info->id)->count();
                $data[$group][$status]['count'] = $count;
            }
        }
        return $data;
    }

    public static function addOrderCountToStoreOrderStatuses($storeId, $data)
    {
        foreach ($data as $group => $groupData) {
            foreach ($groupData as $status => $statusData) {
                $info = $statusData['info'];
                $count = static::where('store_id', $storeId);
                $count = $count->where('order_status_id', $info->id)->count();
                $data[$group][$status]['count'] = $count;
            }
        }
        return $data;
    }

    public static function addOrderCountToUserOrderStatuses($userId, $data)
    {
        foreach ($data as $group => $groupData) {
            foreach ($groupData as $status => $statusData) {
                $info = $statusData['info'];
                $count = static::where('user_id', $userId);
                $count = $count->where('school_id', null);
                $count = $count->where('order_status_id', $info->id)->count();
                $data[$group][$status]['count'] = $count;
            }
        }
        return $data;
    }

    public static function addOrderCountToSchoolOrderStatuses($schoolId, $data)
    {
        foreach ($data as $group => $groupData) {
            foreach ($groupData as $status => $statusData) {
                $info = $statusData['info'];
                $count = static::where('school_id', $schoolId);
                $count = $count->where('order_status_id', $info->id)->count();
                $data[$group][$status]['count'] = $count;
            }
        }
        return $data;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function products()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\OrderProduct');
    }

    public function histories()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\OrderHistory');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\Store');
    }

    public function school()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\School');
    }

    public function orderMetas()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\OrderMeta');
    }

    public function activities()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\OrderActivity');
    }

    public function orderStatus()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\OrderStatus');
    }

    public function productReviews()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\ProductReview');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeFilter($query, $status = null)
    {
        if ($status) {
            if ($status == 'pending') {
                $query->whereIn('order_status', ['pending']);
            }
            if ($status == 'paid') {
                $query->whereIn('order_status', ['payment confirmation', 'paid']);
            }
            if ($status == 'shipping') {
                $query->whereIn('order_status', ['processing', 'processed', 'shipped']);
            }
            if ($status == 'completed') {
                $query->whereIn('order_status', ['completed']);
            }
            if ($status == 'canceled') {
                $query->whereIn('order_status', ['canceled']);
            }
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
