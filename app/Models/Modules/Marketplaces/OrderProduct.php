<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;
use App\Models\Modules\Marketplaces\Product;

class OrderProduct extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function saveProducts($order, $products)
    {
        $index = 1;
        foreach ($products as $item) {
            $orderProduct = new OrderProduct;
            $orderProduct->order_id = $order->id;
            $orderProduct->product_id = $item->id;
            $orderProduct->product_name = $item->name;
            $orderProduct->qty = $item->qty;
            $orderProduct->price = $item->price;
            $orderProduct->total = $item->subtotal;
            $orderProduct->sort_order = $index;
            $orderProduct->save();

            $product = Product::find($orderProduct->product_id);
            if ($product) {
                $product->qty = $product->qty - $orderProduct->qty;
                $product->save();
            }

            $index++;
        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function product()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\Product');
    }

    public function order($value='')
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\Order');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
