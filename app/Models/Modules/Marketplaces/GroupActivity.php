<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;
use Plank\Metable\Metable;

class GroupActivity extends Model
{
    use Metable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'groupable_type', 'groupable_id', 'user_id',
        'name', 'data',
        'ip', 'user_agent',
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function groupable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getGroupTypeAttribute()
    {
        $groupType = null;
        $groupableType = $this->groupable_type;

        if ($groupableType == 'App\Models\Modules\Marketplaces\Marketplace') {
            $groupType = 'Marketplace';
        }

        if ($groupableType == 'App\Models\Modules\Marketplaces\Store') {
            $groupType = 'Store';
        }

        if ($groupableType == 'App\Models\Modules\Marketplaces\SupervisorGroup') {
            $groupType = 'SupervisorGroup';
        }

        if ($groupableType == 'App\Models\Modules\Marketplaces\School') {
            $groupType = 'School';
        }

        return $groupType;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
