<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;
use Cart;

class ShoppingCart extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getCarts()
    {
        $stores = [];
        $carts = session('cart');
        $count = 0;
        $subtotal = 0;

        if (is_array($carts)) {
            foreach ($carts as $store => $cartContent) {
                if (Cart::instance($store)->count() == 0) {
                    Cart::instance($store)->destroy();
                    unset($carts[$store]);
                } else {
                    $subtotal += floatvalue(Cart::instance($store)->subtotal());
                    $count += Cart::instance($store)->content()->count();
                    $stores[$store] = Store::with('marketplace')->where('slug', $store)->first();
                }
            }
        }

        $data = compact('carts', 'stores', 'count', 'subtotal');
        return $data;
    }

    public static function getCartWeight($storeSlug)
    {
        $weightTotal = 0;
        $content = Cart::instance($storeSlug)->content();
        foreach ($content as $rowId => $item) {
            $qty = $item->qty;
            $itemWeight = $item->model->weight;
            $weightSubtotal = $itemWeight * $item->qty;
            $weightTotal += $weightSubtotal;
        }
        return $weightTotal;
    }

    public static function getCartMeta($storeSlug)
    {
        return session('cart_meta.'.$storeSlug);
    }

    public static function destroyCartMeta($storeSlug)
    {
        return session()->forget('cart_meta.'.$storeSlug);
    }

    public static function checkCartStore($storeSlug)
    {
        return Cart::instance($storeSlug)->count() == 0 ? false : true;
    }

    public static function getPaymentAddress($storeSlug)
    {
        return session('cart_meta.'.$storeSlug.'.payment_address');
    }

    public static function setPaymentAddress($storeSlug, $address)
    {
        session(['cart_meta.'.$storeSlug.'.payment_address'=>$address]);
        return session('cart_meta');
    }

    public static function getShippingAddress($storeSlug)
    {
        return session('cart_meta.'.$storeSlug.'.shipping_address');
    }

    public static function setShippingAddress($storeSlug, $address)
    {
        session(['cart_meta.'.$storeSlug.'.shipping_address'=>$address]);
        return session('cart_meta');
    }

    public static function getShippingMethod($storeSlug)
    {
        return session('cart_meta.'.$storeSlug.'.shipping_method');
    }

    public static function setShippingMethod($storeSlug, $method)
    {
        session(['cart_meta.'.$storeSlug.'.shipping_method'=>$method]);
        return session('cart_meta');
    }

    public static function getPaymentMethod($storeSlug)
    {
        return session('cart_meta.'.$storeSlug.'.payment_method');
    }

    public static function setPaymentMethod($storeSlug, $method)
    {
        session(['cart_meta.'.$storeSlug.'.payment_method'=>$method]);
        return session('cart_meta');
    }

    public static function getOrder($storeSlug)
    {
        return session('cart_meta.'.$storeSlug.'.order');
    }

    public static function setOrder($storeSlug, $data)
    {
        session(['cart_meta.'.$storeSlug.'.order'=>$data]);
        return session('cart_meta');
    }

    public static function getCheckoutSteps($storeSlug)
    {
        return session('cart_meta.'.$storeSlug.'.steps');
    }

    public static function setCheckoutSteps($storeSlug, $steps)
    {
        $currenctSteps = static::getCheckoutSteps($storeSlug);
        $currenctSteps = is_array($currenctSteps) ? $currenctSteps : [];
        $steps = array_merge($currenctSteps, $steps);
        session(['cart_meta.'.$storeSlug.'.steps'=>$steps]);
        return session('cart_meta');
    }

    public static function checkCheckoutStep($storeSlug, $step = 0)
    {
        $steps = session('cart_meta.'.$storeSlug.'.steps');
        $step_num = 'step_'.$step;
        if (isset($steps) && isset($steps[$step_num])) {
            if (isset($steps[$step_num]['completed']) && $steps[$step_num]['completed']) {
                return true;
            }
        }

        return false;
    }

    public static function checkCheckoutSteps($storeSlug, $steps = [])
    {
        $conditions = [];
        foreach ($steps as $step) {
            $conditions[] = static::checkCheckoutStep($storeSlug, $step);
        }

        if (in_array(false, $conditions)) {
            return false;
        }

        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
