<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;
use Plank\Metable\Metable;
use Carbon\Carbon;
use DateTime;

class StoreStaff extends Model
{
    use Metable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedByStore($storeId)
    {
        return $storeId == $this->store_id;
    }

    public function checkIsOnline()
    {
        $isOnline = false;

        $lastOnline = $this->getMeta('last_online');
        $isValidDate = DateTime::createFromFormat('Y-m-d H:i:s', $lastOnline) !== false;
        if ($isValidDate) {
            $now = Carbon::now();
            $parsedLastOnline = Carbon::parse($lastOnline);

            $diffInMinutes = $now->diffInMinutes($parsedLastOnline);
            $minutes = 5;
            if ($diffInMinutes <= $minutes) {
                $isOnline = true;
            }
        }

        return $isOnline;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function store()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\Store');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
