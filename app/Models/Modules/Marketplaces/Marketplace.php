<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Carbon\Carbon;
use App\Models\Modules\Marketplaces\School;

class Marketplace extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedBy($userId)
    {
        // return $userId == $this->user_id;
        $isOwnedByUser = false;
        $isOwner = $userId == $this->user_id;
        if ($isOwner) {
            $isOwnedByUser = true;
        } else {
            $isStaff = false;
            $isStaff = $this->staffs()->where('user_id', $userId)->where('is_active', true)->exists();
            if ($isStaff) {
                $isOwnedByUser = true;
            }
        }

        return $isOwnedByUser;
    }

    public static function getUniqueSlug($slug, $id = null, $index = 0)
    {
        $newSlug = ($index == 0) ? $slug : $slug.'--'.$index;
        $rules = ['slug'=>'unique:marketplaces,slug'];
        if ($id) {
            $rules = ['slug'=>'unique:marketplaces,slug,'.$id];
        }

        $validator = \Validator::make(['slug'=>$newSlug],$rules);
        if($validator->fails()){
            $index++;
            return static::getUniqueSlug($slug, $id, $index);
        }

        return $newSlug;
    }

    public static function randomUniqueCode()
    {
        $table = 'marketplaces';
        $column = 'unique_code';

        $id = strtolower(str_random(8));
        $validator = \Validator::make(['id'=>$id],['id'=>'unique:'.$table.','.$column]);
        if($validator->fails()){
            static::randomUniqueCode();
        }

        return $id;
    }

    public static function fillUniqueCode()
    {
        $total = 0;
        $marketplaces = static::get();
        foreach ($marketplaces as $marketplace) {
            if (!$marketplace->unique_code) {
                $uniqueCode = $marketplace->randomUniqueCode();
                $marketplace->unique_code = $uniqueCode;
                $marketplace->save();
                $total++;
            }
        }

        dump('completed');
        if ($total) {
            dump($total.' row affected');
        }
    }

    public function addView($view = 1)
    {
        $this->increment('views', $view);
        return $this->views;
    }

    public function checkIsOwner($userId)
    {
        return $userId == $this->user_id;
    }

    public function addGroupActivity($name, $data = null, $userId = null)
    {
        $userId = $userId ?? auth()->id();
        return $this->groupActivities()->create([
            'name' => $name,
            'user_id' => $userId ?? 0,
            'data' => $data ? json_encode($data) : null,
            'ip' => request()->ip(),
            'user_agent' => request()->server('HTTP_USER_AGENT'),
        ]);
    }

    public function staffLoggedIn()
    {
        $this->addGroupActivity('login');
        session([
            'login_as' => 'marketplace_staff',
            'marketplace_id' => $this->id,
        ]);
    }

    public function staffLoggedOut()
    {
        $loginAs = session('login_as');
        if ($loginAs == 'marketplace_staff') {
            $this->addGroupActivity('logout');
        }
    }

    public function staffChangedStoreStatus($store, $status = null)
    {
        $loginAs = session('login_as');
        if ($loginAs == 'marketplace_staff') {
            if (is_null($status)) {
                $activity = $this->addGroupActivity('changed store status');
            }

            $availableStatuses = ['active', 'inactive', 'rejected', 'suspended'];
            if (!in_array($status, $availableStatuses)) {
                $activity = $this->addGroupActivity('changed store status');
            }

            if (in_array($status, $availableStatuses)) {
                if ($status == 'active') {
                    $activity = $this->addGroupActivity('approved store');
                }

                if ($status == 'inactive') {
                    $activity = $this->addGroupActivity('changed store status to inactive');
                }

                if ($status == 'rejected') {
                    $activity = $this->addGroupActivity('changed store status to rejected');
                }

                if ($status == 'suspended') {
                    $activity = $this->addGroupActivity('changed store status to suspended');
                }
            }

            $activity->setMeta('store', $store->getOriginal());
        }
    }

    public function staffChangedProductStatus($product, $status = null)
    {
        $loginAs = session('login_as');
        if ($loginAs == 'marketplace_staff') {
            if (is_null($status)) {
                $activity = $this->addGroupActivity('changed product status');
            }

            $availableStatuses = ['pending', 'approved', 'rejected'];

            if (!in_array($status, $availableStatuses)) {
                $activity = $this->addGroupActivity('changed product status');
            }

            if (in_array($status, $availableStatuses)) {
                if ($status == 'approved') {
                    $activity = $this->addGroupActivity('approved product');
                }

                if ($status == 'pending') {
                    $activity = $this->addGroupActivity('changed product status to pending');
                }

                if ($status == 'rejected') {
                    $activity = $this->addGroupActivity('rejected product');
                }
            }

            $activity->setMeta('product', $product->getOriginal());
        }
    }

    public function updateMarketplaceStaffLastOnline()
    {
        if (auth()->check()) {
            $userId = auth()->id();
            $staff = $this->staffs()->where('user_id', $userId)->first();
            if ($staff) {
                $staff->setMeta('last_online', date('Y-m-d H:i:s'));
            }
        }
    }

    public function fetchOrderDataForDashboard()
    {
        $data = \DB::table('orders')
            ->selectRaw("IFNULL(SUM(case when status_order_confirmation = 'pending' then total end), 0 ) as confirmation_pending_total")
            ->selectRaw("COUNT(case when status_order_confirmation = 'pending' then 1 end) as confirmation_pending_count")
            ->selectRaw("IFNULL(SUM(case when status_order_confirmation = 'confirmed' then total end), 0 ) as confirmation_confirmed_total")
            ->selectRaw("COUNT(case when status_order_confirmation = 'confirmed' then 1 end) as confirmation_confirmed_count")
            ->selectRaw("IFNULL(SUM(case when status_order_confirmation = 'not_confirmed' then total end), 0 ) as confirmation_not_confirmed_total")
            ->selectRaw("COUNT(case when status_order_confirmation = 'not_confirmed' then 1 end) as confirmation_not_confirmed_count")

            ->selectRaw("IFNULL(SUM(case when status_packing = 'pending' then total end), 0 ) as packing_pending_total")
            ->selectRaw("COUNT(case when status_packing = 'pending' then 1 end) as packing_pending_count")
            ->selectRaw("IFNULL(SUM(case when status_packing = 'processing' then total end), 0 ) as packing_processing_total")
            ->selectRaw("COUNT(case when status_packing = 'processing' then 1 end) as packing_processing_count")
            ->selectRaw("IFNULL(SUM(case when status_packing = 'processed' then total end), 0 ) as packing_processed_total")
            ->selectRaw("COUNT(case when status_packing = 'processed' then 1 end) as packing_processed_count")

            ->selectRaw("IFNULL(SUM(case when status_shipping = 'pending' then total end), 0 ) as shipping_pending_total")
            ->selectRaw("COUNT(case when status_shipping = 'pending' then 1 end) as shipping_pending_count")
            ->selectRaw("IFNULL(SUM(case when status_shipping = 'shipping' then total end), 0 ) as shipping_shipping_total")
            ->selectRaw("COUNT(case when status_shipping = 'shipping' then 1 end) as shipping_shipping_count")
            ->selectRaw("IFNULL(SUM(case when status_shipping = 'shipped' then total end), 0 ) as shipping_shipped_total")
            ->selectRaw("COUNT(case when status_shipping = 'shipped' then 1 end) as shipping_shipped_count")

            ->selectRaw("IFNULL(SUM(case when status_delivery = 'undelivered' then total end), 0 ) as delivery_undelivered_total")
            ->selectRaw("COUNT(case when status_delivery = 'undelivered' then 1 end) as delivery_undelivered_count")
            ->selectRaw("IFNULL(SUM(case when status_delivery = 'delivered' then total end), 0 ) as delivery_delivered_total")
            ->selectRaw("COUNT(case when status_delivery = 'delivered' then 1 end) as delivery_delivered_count")

            ->selectRaw("IFNULL(SUM(case when status_delivery_document = 'pending' then total end), 0 ) as delivery_document_pending_total")
            ->selectRaw("COUNT(case when status_delivery_document = 'pending' then 1 end) as delivery_document_pending_count")
            ->selectRaw("IFNULL(SUM(case when status_delivery_document = 'completed' then total end), 0 ) as delivery_document_completed_total")
            ->selectRaw("COUNT(case when status_delivery_document = 'completed' then 1 end) as delivery_document_completed_count")

            ->selectRaw("IFNULL(SUM(case when status_payment_from_customer = 'unpaid' then total end), 0 ) as payment_from_customer_unpaid_total")
            ->selectRaw("COUNT(case when status_payment_from_customer = 'unpaid' then 1 end) as payment_from_customer_unpaid_count")
            ->selectRaw("IFNULL(SUM(case when status_payment_from_customer = 'paid' then total end), 0 ) as payment_from_customer_paid_total")
            ->selectRaw("COUNT(case when status_payment_from_customer = 'paid' then 1 end) as payment_from_customer_paid_count")

            ->selectRaw("IFNULL(SUM(case when status_payment_to_store = 'unpaid' then total end), 0 ) as payment_to_store_unpaid_total")
            ->selectRaw("COUNT(case when status_payment_to_store = 'unpaid' then 1 end) as payment_to_store_unpaid_count")
            ->selectRaw("IFNULL(SUM(case when status_payment_to_store = 'paid' then total end), 0 ) as payment_to_store_paid_total")
            ->selectRaw("COUNT(case when status_payment_to_store = 'paid' then 1 end) as payment_to_store_paid_count")

            ->selectRaw("IFNULL(SUM(case when status_order_process = 'pending' then total end), 0 ) as order_process_pending_total")
            ->selectRaw("COUNT(case when status_order_process = 'pending' then 1 end) as order_process_pending_count")
            ->selectRaw("IFNULL(SUM(case when status_order_process = 'processing' then total end), 0 ) as order_process_processing_total")
            ->selectRaw("COUNT(case when status_order_process = 'processing' then 1 end) as order_process_processing_count")
            ->selectRaw("IFNULL(SUM(case when status_order_process = 'completed' then total end), 0 ) as order_process_completed_total")
            ->selectRaw("COUNT(case when status_order_process = 'completed' then 1 end) as order_process_completed_count")
            ->selectRaw("IFNULL(SUM(case when status_order_process = 'complain' then total end), 0 ) as order_process_complain_total")
            ->selectRaw("COUNT(case when status_order_process = 'complain' then 1 end) as order_process_complain_count")
            ->selectRaw("IFNULL(SUM(case when status_order_process = 'canceled' then total end), 0 ) as order_process_canceled_total")
            ->selectRaw("COUNT(case when status_order_process = 'canceled' then 1 end) as order_process_canceled_count")

            ->selectRaw("IFNULL(SUM(case when status_complain_source = 'complain_from_store' then total end), 0) as complain_source_complain_from_store_total")
            ->selectRaw("COUNT(case when status_complain_source = 'complain_from_store' then 1 end) as complain_source_complain_from_store_count")
            ->selectRaw("IFNULL(SUM(case when status_complain_source = 'complain_from_customer' then total end), 0) as complain_source_complain_from_customer_total")
            ->selectRaw("COUNT(case when status_complain_source = 'complain_from_customer' then 1 end) as complain_source_complain_from_customer_count")

            ->selectRaw("IFNULL(SUM(case when status_complain_request = 'not_approved' then total end), 0) as complain_request_not_approved_total")
            ->selectRaw("COUNT(case when status_complain_request = 'not_approved' then 1 end) as complain_request_not_approved_count")
            ->selectRaw("IFNULL(SUM(case when status_complain_request = 'approved' then total end), 0) as complain_request_approved_total")
            ->selectRaw("COUNT(case when status_complain_request = 'approved' then 1 end) as complain_request_approved_count")

            ->selectRaw("IFNULL(SUM(case when status_complain_status = 'complain_unresolved' then total end), 0) as complain_status_complain_unresolved_total")
            ->selectRaw("COUNT(case when status_complain_status = 'complain_unresolved' then 1 end) as complain_status_complain_unresolved_count")
            ->selectRaw("IFNULL(SUM(case when status_complain_status = 'complain_resolved' then total end), 0) as complain_status_complain_resolved_total")
            ->selectRaw("COUNT(case when status_complain_status = 'complain_resolved' then 1 end) as complain_status_complain_resolved_count")
            ->selectRaw("IFNULL(SUM(case when status_complain_status = 'escalation_solution' then total end), 0) as complain_status_escalation_solution_total")
            ->selectRaw("COUNT(case when status_complain_status = 'escalation_solution' then 1 end) as complain_status_escalation_solution_count")

            ->leftJoin('stores', 'orders.store_id', '=', 'stores.id')
            ->where('stores.marketplace_id', $this->id)

            ->first();

        $orderData = [];

        $orderData['confirmation']['pending']['total'] = floatval($data->confirmation_pending_total);
        $orderData['confirmation']['pending']['count'] = floatval($data->confirmation_pending_count);
        $orderData['confirmation']['confirmed']['total'] = floatval($data->confirmation_confirmed_total);
        $orderData['confirmation']['confirmed']['count'] = floatval($data->confirmation_confirmed_count);
        $orderData['confirmation']['not_confirmed']['total'] = floatval($data->confirmation_not_confirmed_total);
        $orderData['confirmation']['not_confirmed']['count'] = floatval($data->confirmation_not_confirmed_count);

        $orderData['packing']['pending']['total'] = floatval($data->packing_pending_total);
        $orderData['packing']['pending']['count'] = floatval($data->packing_pending_count);
        $orderData['packing']['processing']['total'] = floatval($data->packing_processing_total);
        $orderData['packing']['processing']['count'] = floatval($data->packing_processing_count);
        $orderData['packing']['processed']['total'] = floatval($data->packing_processed_total);
        $orderData['packing']['processed']['count'] = floatval($data->packing_processed_count);

        $orderData['shipping']['pending']['total'] = floatval($data->shipping_pending_total);
        $orderData['shipping']['pending']['count'] = floatval($data->shipping_pending_count);
        $orderData['shipping']['shipping']['total'] = floatval($data->shipping_shipping_total);
        $orderData['shipping']['shipping']['count'] = floatval($data->shipping_shipping_count);
        $orderData['shipping']['shipped']['total'] = floatval($data->shipping_shipped_total);
        $orderData['shipping']['shipped']['count'] = floatval($data->shipping_shipped_count);

        $orderData['delivery']['undelivered']['total'] = floatval($data->delivery_undelivered_total);
        $orderData['delivery']['undelivered']['count'] = floatval($data->delivery_undelivered_count);
        $orderData['delivery']['delivered']['total'] = floatval($data->delivery_delivered_total);
        $orderData['delivery']['delivered']['count'] = floatval($data->delivery_delivered_count);

        $orderData['delivery_document']['pending']['total'] = floatval($data->delivery_document_pending_total);
        $orderData['delivery_document']['pending']['count'] = floatval($data->delivery_document_pending_count);
        $orderData['delivery_document']['completed']['total'] = floatval($data->delivery_document_completed_total);
        $orderData['delivery_document']['completed']['count'] = floatval($data->delivery_document_completed_count);

        $orderData['payment_from_customer']['unpaid']['total'] = floatval($data->payment_from_customer_unpaid_total);
        $orderData['payment_from_customer']['unpaid']['count'] = floatval($data->payment_from_customer_unpaid_count);
        $orderData['payment_from_customer']['paid']['total'] = floatval($data->payment_from_customer_paid_total);
        $orderData['payment_from_customer']['paid']['count'] = floatval($data->payment_from_customer_paid_count);

        $orderData['payment_to_store']['unpaid']['total'] = floatval($data->payment_to_store_unpaid_total);
        $orderData['payment_to_store']['unpaid']['count'] = floatval($data->payment_to_store_unpaid_count);
        $orderData['payment_to_store']['paid']['total'] = floatval($data->payment_to_store_paid_total);
        $orderData['payment_to_store']['paid']['count'] = floatval($data->payment_to_store_paid_count);

        $orderData['order_process']['pending']['total'] = floatval($data->order_process_pending_total);
        $orderData['order_process']['pending']['count'] = floatval($data->order_process_pending_count);
        $orderData['order_process']['processing']['total'] = floatval($data->order_process_processing_total);
        $orderData['order_process']['processing']['count'] = floatval($data->order_process_processing_count);
        $orderData['order_process']['completed']['total'] = floatval($data->order_process_completed_total);
        $orderData['order_process']['completed']['count'] = floatval($data->order_process_completed_count);
        $orderData['order_process']['complain']['total'] = floatval($data->order_process_complain_total);
        $orderData['order_process']['complain']['count'] = floatval($data->order_process_complain_count);
        $orderData['order_process']['canceled']['total'] = floatval($data->order_process_canceled_total);
        $orderData['order_process']['canceled']['count'] = floatval($data->order_process_canceled_count);

        $orderData['order']['all']['total'] = $orderData['order_process']['pending']['total'] + $orderData['order_process']['processing']['total'] + $orderData['order_process']['completed']['total'];
        $orderData['order']['all']['count'] = $orderData['order_process']['pending']['count'] + $orderData['order_process']['processing']['count'] + $orderData['order_process']['completed']['count'];

        $orderData['complain_source']['complain_from_store']['total'] = floatval($data->complain_source_complain_from_store_total);
        $orderData['complain_source']['complain_from_store']['count'] = floatval($data->complain_source_complain_from_store_count);
        $orderData['complain_source']['complain_from_customer']['total'] = floatval($data->complain_source_complain_from_customer_total);
        $orderData['complain_source']['complain_from_customer']['count'] = floatval($data->complain_source_complain_from_customer_count);

        $orderData['complain_request']['not_approved']['total'] = floatval($data->complain_request_not_approved_total);
        $orderData['complain_request']['not_approved']['count'] = floatval($data->complain_request_not_approved_count);
        $orderData['complain_request']['approved']['total'] = floatval($data->complain_request_approved_total);
        $orderData['complain_request']['approved']['count'] = floatval($data->complain_request_approved_count);

        $orderData['complain_status']['complain_unresolved']['total'] = floatval($data->complain_status_complain_unresolved_total);
        $orderData['complain_status']['complain_unresolved']['count'] = floatval($data->complain_status_complain_unresolved_count);
        $orderData['complain_status']['complain_resolved']['total'] = floatval($data->complain_status_complain_resolved_total);
        $orderData['complain_status']['complain_resolved']['count'] = floatval($data->complain_status_complain_resolved_count);
        $orderData['complain_status']['escalation_solution']['total'] = floatval($data->complain_status_escalation_solution_total);
        $orderData['complain_status']['escalation_solution']['count'] = floatval($data->complain_status_escalation_solution_count);

        return $orderData;
    }

    public function fetchUserDataForDashboard()
    {
        $registeredUsers = User::count();
        $onlineUsers = User::where('last_online_at', '>=', Carbon::now()->subMinutes(5)->toDateTimeString())->count();
        $suspendedUsers = User::where('status', 'suspended')->count();

        return [
            'registeredUsers' => $registeredUsers,
            'onlineUsers' => $onlineUsers,
            'suspendedUsers' => $suspendedUsers,
        ];
    }

    public function fetchStoreDataForDashboard()
    {
        $registeredStores = $this->stores()->count();
        $activeStores = $this->stores()->where('status', 'active')->count();
        $suspendedStores = $this->stores()->where('status', 'suspended')->count();
        $storeWithOrders = $this->stores()->whereHas('orders')->count();

        return [
            'registeredStores' => $registeredStores,
            'activeStores' => $activeStores,
            'suspendedStores' => $suspendedStores,
            'storeWithOrders' => $storeWithOrders,
        ];
    }

    public function fetchSchoolDataForDashboard()
    {
        $schoolCount = School::count();
        $schoolWithOrderCount = School::whereHas('orders')->count();

        return [
            'schoolCount' => $schoolCount,
            'schoolWithOrderCount' => $schoolWithOrderCount,
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function stores()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\Store');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->hasManyThrough('App\Models\Modules\Marketplaces\Product', 'App\Models\Modules\Marketplaces\Store');
    }

    public function categories()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceCategory');
    }

    public function promos()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch');
    }

    public function settings()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceSetting');
    }

    public function banners()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceBanner');
    }

    public function bannerGroups()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceBannerGroup');
    }

    public function activitySearches()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceActivitySearch');
    }

    public function promoPopularSearches()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch');
    }

    public function storeCollections()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceStoreCollection');
    }

    public function productCollections()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceProductCollection');
    }

    public function mediaLibraries()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceMediaLibrary');
    }

    public function staffs()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\MarketplaceStaff');
    }

    public function groupActivities()
    {
        return $this->morphMany('App\Models\Modules\Marketplaces\GroupActivity', 'groupable');
    }

    public function groupNotifications()
    {
        return $this->morphMany('App\Models\Modules\Marketplaces\GroupNotification', 'groupable');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getMarketplaceUrlAttribute($value)
    {
        $marketplace = $this->slug;
        return route('marketplaces.show', [$marketplace]);
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
