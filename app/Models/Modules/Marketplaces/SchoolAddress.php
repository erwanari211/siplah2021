<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;

class SchoolAddress extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedBySchool($schoolId)
    {
        return $schoolId == $this->school_id;
    }

    public static function getSchoolAddressesDropdown($school)
    {
        $addresses = $school->addresses()->with('province', 'city', 'district', 'district.rajaongkirDistrict')->get();
        return $addresses->mapWithKeys(function($item) {
            $display_name = '';
            $label = $item['label'];
            $district = $item['district']['name'];
            $city = $item['city']['name'];
            $province = $item['province']['name'];
            $isDefault = $item['default_address'];
            if ($isDefault) {
                $display_name .= "* ";
            }
            $display_name .= "{$label} ({$district}, {$city}, {$province})";
            return [$item['id'] => $display_name];
        });
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function school()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\School');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince', 'province_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity', 'city_id');
    }

    public function district()
    {
        return $this->belongsTo('App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict', 'district_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeDefaultAddress($query)
    {
        return $query->where('default_address', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
