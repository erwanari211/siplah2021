<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;
use Plank\Metable\Metable;

use function GuzzleHttp\json_decode;

class Store extends Model
{
    use Metable;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];
    protected $with = ['meta'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedBy($userId)
    {
        $isOwnedByUser = false;
        $isOwner = $userId == $this->user_id;
        if ($isOwner) {
            $isOwnedByUser = true;
        } else {
            $isStaff = false;
            $isStaff = $this->staffs()->where('user_id', $userId)->where('is_active', true)->exists();
            if ($isStaff) {
                $isOwnedByUser = true;
            }
        }

        return $isOwnedByUser;
    }

    public function checkIsOwner($userId)
    {
        return $userId == $this->user_id;
    }

    public function isOwnedByMarketplace($marketplaceId)
    {
        return $marketplaceId == $this->marketplace_id;
    }

    public static function getUniqueSlug($slug, $id = null, $index = 0)
    {
        $newSlug = ($index == 0) ? $slug : $slug.'--'.$index;
        $rules = ['slug'=>'unique:stores,slug'];
        if ($id) {
            $rules = ['slug'=>'unique:stores,slug,'.$id];
        }

        $validator = \Validator::make(['slug'=>$newSlug],$rules);
        if($validator->fails()){
            $index++;
            return static::getUniqueSlug($slug, $id, $index);
        }

        return $newSlug;
    }

    public static function randomUniqueCode()
    {
        $table = 'stores';
        $column = 'unique_code';

        $id = strtolower(str_random(8));
        $validator = \Validator::make(['id'=>$id],['id'=>'unique:'.$table.','.$column]);
        if($validator->fails()){
            static::randomUniqueCode();
        }

        return $id;
    }

    public static function fillUniqueCode()
    {
        $total = 0;
        $stores = static::get();
        foreach ($stores as $store) {
            if (!$store->unique_code) {
                $uniqueCode = $store->randomUniqueCode();
                $store->unique_code = $uniqueCode;
                $store->save();
                $total++;
            }
        }

        dump('completed');
        if ($total) {
            dump($total.' row affected');
        }
    }

    public function hasDefaultStoreAddress()
    {
        return $this->addresses()->where('default_address', 1)->count() > 0;
    }

    public function getDefaultStoreAddress()
    {
        return $this->addresses()->where('default_address', 1)
            ->with('province', 'city', 'district')
            ->first();
    }

    public function mappedSettings()
    {
        $mappedSettings = [];
        $settings = $this->settings;
        if ($settings) {
            $mappedSettings = $settings->mapWithKeys(function($item){
                return [$item['key'] => $item['value']];
            });
        }

        return $mappedSettings;
    }

    public function updateLastOnline()
    {
        $now = date('Y-m-d H:i:s');
        $this->setMeta('last_online', $now);
        return $now;
    }

    public function addView($view = 1)
    {
        $this->increment('views', $view);
        return $this->views;
    }

    public function addGroupActivity($name, $data = null, $userId = null)
    {
        $userId = $userId ?? auth()->id();
        return $this->groupActivities()->create([
            'name' => $name,
            'user_id' => $userId ?? 0,
            'data' => $data ? json_encode($data) : null,
            'ip' => request()->ip(),
            'user_agent' => request()->server('HTTP_USER_AGENT'),
        ]);
    }

    public function staffLoggedIn()
    {
        $this->addGroupActivity('login');
        session([
            'login_as' => 'store_staff',
            'store_id' => $this->id,
        ]);
    }

    public function staffLoggedOut()
    {
        $loginAs = session('login_as');
        if ($loginAs == 'store_staff') {
            $this->addGroupActivity('logout');
        }
    }

    public function staffCreatedNewProduct()
    {
        $loginAs = session('login_as');
        if ($loginAs == 'store_staff') {
            $this->addGroupActivity('created new product');
        }
    }

    public function staffUpdatedProduct()
    {
        $loginAs = session('login_as');
        if ($loginAs == 'store_staff') {
            $this->addGroupActivity('updated product');
        }
    }

    public function staffUpdatedOrder($order)
    {
        $loginAs = session('login_as');
        if ($loginAs == 'store_staff') {
            $activity = $this->addGroupActivity('updated order');
            $activity->setMeta('order', $order->getOriginal());
        }
    }

    public function fetchStoreOrderDataForDashboardV01()
    {
        $orderData = [];

        // unconfirmed
        $unconfirmed = $this->orders()->where([
            'status_order_confirmation' => 'pending',
        ]);
        $orderData['unconfirmed']['total'] = $unconfirmed->sum('total');
        $orderData['unconfirmed']['count'] = $unconfirmed->count();

        // confirmed
        $confirmed = $this->orders()->where([
            'status_order_confirmation' => 'confirmed',
        ]);
        $orderData['confirmed']['total'] = $confirmed->sum('total');
        $orderData['confirmed']['count'] = $confirmed->count();

        // processing
        $processing = $this->orders()->where([
            'status_packing' => 'processing',
        ]);
        $orderData['processing']['total'] = $processing->sum('total');
        $orderData['processing']['count'] = $processing->count();

        // shipping
        $shipping = $this->orders()->where([
            'status_shipping' => 'shipping',
        ]);
        $orderData['shipping']['total'] = $shipping->sum('total');
        $orderData['shipping']['count'] = $shipping->count();

        // order document completed
        $orderDocumentCompleted = $this->orders()->where([
            'status_delivery_document' => 'completed',
        ]);
        $orderData['delivery_document']['total'] = $orderDocumentCompleted->sum('total');
        $orderData['delivery_document']['count'] = $orderDocumentCompleted->count();

        // paid (from customer)
        $paidFromCustomer = $this->orders()->where([
            'status_payment_from_customer' => 'paid',
        ]);
        $orderData['payment_from_customer']['total'] = $paidFromCustomer->sum('total');
        $orderData['payment_from_customer']['count'] = $paidFromCustomer->count();

        // completed
        $completed = $this->orders()->where([
            'status_order_process' => 'completed',
        ]);
        $orderData['completed']['total'] = $completed->sum('total');
        $orderData['completed']['count'] = $completed->count();

        // complain
        $complain = $this->orders()->where([
            'status_order_process' => 'complain',
        ]);
        $orderData['complain']['total'] = $complain->sum('total');
        $orderData['complain']['count'] = $complain->count();

        return $orderData;
    }

    public function fetchStoreOrderDataForDashboard()
    {
        $data = \DB::table('orders')
            ->selectRaw("IFNULL(SUM(case when status_order_confirmation = 'pending' then total end), 0 ) as unconfirmed_total")
            ->selectRaw("COUNT(case when status_order_confirmation = 'pending' then 1 end) as unconfirmed_count")

            ->selectRaw("IFNULL(SUM(case when status_order_confirmation = 'confirmed' then total end), 0 ) as confirmed_total")
            ->selectRaw("COUNT(case when status_order_confirmation = 'confirmed' then 1 end) as confirmed_count")

            ->selectRaw("IFNULL(SUM(case when status_packing = 'processing' then total end), 0 ) as processing_total")
            ->selectRaw("COUNT(case when status_packing = 'processing' then 1 end) as processing_count")

            ->selectRaw("IFNULL(SUM(case when status_shipping = 'shipping' then total end), 0 ) as shipping_total")
            ->selectRaw("COUNT(case when status_shipping = 'shipping' then 1 end) as shipping_count")

            ->selectRaw("IFNULL(SUM(case when status_delivery_document = 'completed' then total end), 0 ) as delivery_document_total")
            ->selectRaw("COUNT(case when status_delivery_document = 'completed' then 1 end) as delivery_document_count")

            ->selectRaw("IFNULL(SUM(case when status_payment_from_customer = 'paid' then total end), 0 ) as paid_total")
            ->selectRaw("COUNT(case when status_payment_from_customer = 'paid' then 1 end) as paid_count")

            ->selectRaw("IFNULL(SUM(case when status_order_process = 'completed' then total end), 0 ) as completed_total")
            ->selectRaw("COUNT(case when status_order_process = 'completed' then 1 end) as completed_count")

            ->selectRaw("IFNULL(SUM(case when status_order_process = 'complain' then total end), 0) as complain_total")
            ->selectRaw("COUNT(case when status_order_process = 'complain' then 1 end) as complain_count")

            ->where("store_id", $this->id)
            ->first();

        $orderData = [];

        // unconfirmed
        $orderData['unconfirmed']['total'] = floatval($data->unconfirmed_total);
        $orderData['unconfirmed']['count'] = floatval($data->unconfirmed_count);

        // confirmed
        $orderData['confirmed']['total'] = floatval($data->confirmed_total);
        $orderData['confirmed']['count'] = floatval($data->confirmed_count);

        // processing
        $orderData['processing']['total'] = floatval($data->processing_total);
        $orderData['processing']['count'] = floatval($data->processing_count);

        // shipping
        $orderData['shipping']['total'] = floatval($data->shipping_total);
        $orderData['shipping']['count'] = floatval($data->shipping_count);

        // order document completed
        $orderData['delivery_document']['total'] = floatval($data->delivery_document_total);
        $orderData['delivery_document']['count'] = floatval($data->delivery_document_count);

        // paid (from customer)
        $orderData['payment_from_customer']['total'] = floatval($data->paid_total);
        $orderData['payment_from_customer']['count'] = floatval($data->paid_count);

        // completed
        $orderData['completed']['total'] = floatval($data->completed_total);
        $orderData['completed']['count'] = floatval($data->completed_count);

        // complain
        $orderData['complain']['total'] = floatval($data->complain_total);
        $orderData['complain']['count'] = floatval($data->complain_count);

        return $orderData;
    }

    public function fetchStoreProductDataForDashboard()
    {
        $data = \DB::table('products')
            ->selectRaw("COUNT(case when approved_status = 'pending' then 1 end) as pending")
            ->selectRaw("COUNT(case when approved_status = 'approved' then 1 end) as approved")
            ->selectRaw("COUNT(case when approved_status = 'rejected' then 1 end) as rejected")

            ->where("store_id", $this->id)
            ->first();

        return $data;
    }

    public function updateStoreStaffLastOnline()
    {
        if (auth()->check()) {
            $userId = auth()->id();
            $staff = $this->staffs()->where('user_id', $userId)->first();
            if ($staff) {
                $staff->setMeta('last_online', date('Y-m-d H:i:s'));
            }
        }
    }

    public function saveDefaultAddress()
    {
        $address = $this->getDefaultStoreAddress();
        $this->province_id = $address->province_id;
        $this->city_id = $address->city_id;
        $this->district_id = $address->district_id;
        $this->save();

        return $this;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function marketplace()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\Marketplace');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\Product');
    }

    public function addresses()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreAddress');
    }

    public function paymentMethods()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StorePaymentMethod');
    }

    public function shippingMethods()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreShippingMethod');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\Order');
    }

    public function categories()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreCategory');
    }

    public function settings()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreSetting');
    }

    public function banners()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreBanner');
    }

    public function bannerGroups()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreBannerGroup');
    }

    public function storeFeaturedProducts()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreFeaturedProduct');
    }

    public function menus()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreMenu');
    }

    public function notes()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreNote');
    }

    public function productDiscussions()
    {
        return $this->hasManyThrough('App\Models\Modules\Marketplaces\ProductDiscussion', 'App\Models\Modules\Marketplaces\Product');
    }

    public function productReviews()
    {
        return $this->hasManyThrough('App\Models\Modules\Marketplaces\ProductReview', 'App\Models\Modules\Marketplaces\Product');
    }

    public function mediaLibraries()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreMediaLibrary');
    }

    public function storeSellers()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreSeller');
    }

    public function staffs()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\StoreStaff');
    }

    public function groupActivities()
    {
        return $this->morphMany('App\Models\Modules\Marketplaces\GroupActivity', 'groupable');
    }

    public function groupNotifications()
    {
        return $this->morphMany('App\Models\Modules\Marketplaces\GroupNotification', 'groupable');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeFilterActive($query, $status = null)
    {
        if ($status) {
            if (is_array($status)) {
                $query->whereIn('active', $status);
            } else {
                $query->where('active', $status);
            }
        }

        return $query;
    }

    public function scopeFilterStatus($query, $status = null)
    {
        if ($status) {
            if ($status == 'active') {
                $query->where('status', $status);
            }

            if ($status == 'inactive') {
                $query->where('status', $status);
            }

            if ($status == 'rejected') {
                $query->where('status', $status);
            }

            if ($status == 'suspended') {
                $query->where('status', $status);
            }

            if ($status == 'all') {
                $query;
            }
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getStoreUrlAttribute($value)
    {
        $store = $this->slug;
        $marketplace = $this->marketplace->slug;
        return route('stores.show', [$marketplace, $store]);
    }

    public function getStatusButtonAttribute()
    {
        $label = '';
        $class = '';
        $status = $this->status;

        if ($status == 'inactive') {
            $label = ucwords($status);
            $class = 'btn btn-warning btn-xs';
        }

        if ($status == 'active') {
            $label = ucwords($status);
            $class = 'btn btn-success btn-xs';
        }

        if ($status == 'rejected') {
            $label = ucwords($status);
            $class = 'btn btn-danger btn-xs';
        }

        if ($status == 'suspended') {
            $label = ucwords($status);
            $class = 'btn btn-danger btn-xs';
        }

        return '<span class="'.$class.'">'.$label.'</span>';
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
