<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;

class StoreShippingMethod extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedByStore($storeId)
    {
        return $storeId == $this->store_id;
    }

    public static function checkLabelIsValid($label = null, $storeId = null, $id = null)
    {
        $isValid = false;
        $where = [
            'store_id' => $storeId,
            'label' => $label,
        ];

        $dataCount = static::where($where);
        if ($id) {
            $dataCount = $dataCount->where('id', '<>', $id);
        }
        $dataCount = $dataCount->count();

        if ($dataCount == 0) {
            $isValid = true;
        }

        return $isValid;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function store()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\Store');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
