<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;
use Plank\Metable\Metable;
use App\Models\Modules\Marketplaces\Cart as CartModel;

class School extends Model
{
    use Metable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function checkIsStaff($userId = null)
    {
        if (is_null($userId)) {
            if (auth()->check()) {
                $userId = auth()->user()->id;
            }
        }

        return $this->staffs()->where([
            'user_id' => $userId,
            'is_active' => true,
        ])->exists();
    }

    public function addGroupActivity($name, $data = null, $userId = null)
    {
        $userId = $userId ?? auth()->id();
        return $this->groupActivities()->create([
            'name' => $name,
            'user_id' => $userId ?? 0,
            'data' => $data ? json_encode($data) : null,
            'ip' => request()->ip(),
            'user_agent' => request()->server('HTTP_USER_AGENT'),
        ]);
    }

    public function staffLoggedIn()
    {
        $this->addGroupActivity('login');
        session([
            'login_as' => 'school_staff',
            'school_id' => $this->id,
            'zone' => $this->kemendikbud_zone,
            'school_coordinates' => [
                'lat' => floatval($this->latitude),
                'long' => floatval($this->longitude),
            ],
        ]);

        $school = $this;
        $user = auth()->user();
        $staff = $school->staffs()->where('user_id', $user->id)->first();
        session([
            'school_staff' => [
                'user_id' => $user->id,
                'role' => $staff->role ?? null,
            ],
            'school_serialized' => serialize($school),
        ]);

        CartModel::restore($school);
    }

    public function staffLoggedOut()
    {
        $loginAs = session('login_as');
        if ($loginAs == 'school_staff') {
            $this->addGroupActivity('logout');
        }
    }

    public function staffSearchingProduct($query = null)
    {
        $loginAs = session('login_as');
        if ($loginAs == 'school_staff') {
            if ($query) {
                $activity = $this->addGroupActivity('searching');
                $activity->setMeta('search_query', $query);
            }
        }
    }

    public function staffShoppingCartCheckout()
    {
        $loginAs = session('login_as');
        if ($loginAs == 'school_staff') {
            $this->addGroupActivity('checkout');
        }
    }

    public function updateSchoolStaffLastOnline()
    {
        if (auth()->check()) {
            $userId = auth()->id();
            $staff = $this->staffs()->where('user_id', $userId)->first();
            if ($staff) {
                $staff->setMeta('last_online', date('Y-m-d H:i:s'));
            }
        }
    }

    public function getDefaultStoreAddress()
    {
        return $this->addresses()->where('default_address', 1)
            ->with('province', 'city', 'district')
            ->first();
    }

    public function saveDefaultAddress()
    {
        $address = $this->getDefaultStoreAddress();
        if ($address) {
            $this->province_id = $address->province_id;
            $this->city_id = $address->city_id;
            $this->district_id = $address->district_id;
            $this->save();
        }

        return $this;
    }

    public function fetchSchoolOrderDataForDashboard()
    {
        $data = \DB::table('orders')
            ->selectRaw("IFNULL(SUM(case when status_order_confirmation = 'pending' then total end), 0 ) as unconfirmed_total")
            ->selectRaw("COUNT(case when status_order_confirmation = 'pending' then 1 end) as unconfirmed_count")

            ->selectRaw("IFNULL(SUM(case when status_order_confirmation = 'confirmed' then total end), 0 ) as confirmed_total")
            ->selectRaw("COUNT(case when status_order_confirmation = 'confirmed' then 1 end) as confirmed_count")

            ->selectRaw("IFNULL(SUM(case when status_packing = 'processing' then total end), 0 ) as processing_total")
            ->selectRaw("COUNT(case when status_packing = 'processing' then 1 end) as processing_count")

            ->selectRaw("IFNULL(SUM(case when status_shipping = 'shipping' then total end), 0 ) as shipping_total")
            ->selectRaw("COUNT(case when status_shipping = 'shipping' then 1 end) as shipping_count")

            ->selectRaw("IFNULL(SUM(case when status_delivery_document = 'completed' then total end), 0 ) as delivery_document_total")
            ->selectRaw("COUNT(case when status_delivery_document = 'completed' then 1 end) as delivery_document_count")

            ->selectRaw("IFNULL(SUM(case when status_payment_from_customer = 'paid' then total end), 0 ) as paid_total")
            ->selectRaw("COUNT(case when status_payment_from_customer = 'paid' then 1 end) as paid_count")

            ->selectRaw("IFNULL(SUM(case when status_order_process = 'completed' then total end), 0 ) as completed_total")
            ->selectRaw("COUNT(case when status_order_process = 'completed' then 1 end) as completed_count")

            ->selectRaw("IFNULL(SUM(case when status_order_process = 'complain' then total end), 0) as complain_total")
            ->selectRaw("COUNT(case when status_order_process = 'complain' then 1 end) as complain_count")

            ->where("school_id", $this->id)
            ->first();

        $orderData = [];

        // unconfirmed
        $orderData['unconfirmed']['total'] = floatval($data->unconfirmed_total);
        $orderData['unconfirmed']['count'] = floatval($data->unconfirmed_count);

        // confirmed
        $orderData['confirmed']['total'] = floatval($data->confirmed_total);
        $orderData['confirmed']['count'] = floatval($data->confirmed_count);

        // processing
        $orderData['processing']['total'] = floatval($data->processing_total);
        $orderData['processing']['count'] = floatval($data->processing_count);

        // shipping
        $orderData['shipping']['total'] = floatval($data->shipping_total);
        $orderData['shipping']['count'] = floatval($data->shipping_count);

        // order document completed
        $orderData['delivery_document']['total'] = floatval($data->delivery_document_total);
        $orderData['delivery_document']['count'] = floatval($data->delivery_document_count);

        // paid (from customer)
        $orderData['payment_from_customer']['total'] = floatval($data->paid_total);
        $orderData['payment_from_customer']['count'] = floatval($data->paid_count);

        // completed
        $orderData['completed']['total'] = floatval($data->completed_total);
        $orderData['completed']['count'] = floatval($data->completed_count);

        // complain
        $orderData['complain']['total'] = floatval($data->complain_total);
        $orderData['complain']['count'] = floatval($data->complain_count);

        return $orderData;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function staffs()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\SchoolStaff');
    }

    public function groupActivities()
    {
        return $this->morphMany('App\Models\Modules\Marketplaces\GroupActivity', 'groupable');
    }

    public function addresses()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\SchoolAddress');
    }

    public function groupNotifications()
    {
        return $this->morphMany('App\Models\Modules\Marketplaces\GroupNotification', 'groupable');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\Order');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
