<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;

class MarketplaceMediaLibrary extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedByMarketplace($marketplaceId)
    {
        return $marketplaceId == $this->marketplace_id;
    }

    public static function randomUniqueCode()
    {
        $table = 'marketplace_media_libraries';
        $column = 'unique_code';

        $id = strtolower(str_random(8));
        $validator = \Validator::make(['id'=>$id],['id'=>'unique:'.$table.','.$column]);
        if($validator->fails()){
            $this->randomUniqueCode();
        }

        return $id;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function marketplace()
    {
        return $this->belongsTo('App\Models\Modules\Marketplaces\Marketplace');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getFileTypeAttribute($value)
    {
        $fileType = 'file';
        $type = $this->mimetype;

        if (strpos($type, 'image/') !== false) {
            $fileType = 'image';
        }

        if (strpos($type, 'video/') !== false) {
            $fileType = 'video';
        }

        if (strpos($type, 'pdf') !== false) {
            $fileType = 'pdf';
        }

        if (strpos($type, 'spreadsheet') !== false) {
            $fileType = 'excel';
        }

        if (strpos($type, 'excel') !== false) {
            $fileType = 'excel';
        }

        if (strpos($type, 'xls') !== false) {
            $fileType = 'excel';
        }

        return $fileType;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
