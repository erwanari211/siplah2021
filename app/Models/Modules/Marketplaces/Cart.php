<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;
use Cart as ShoppingCart;
use App\Models\Modules\Marketplaces\Store;

class Cart extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'identifierable_type', 'identifierable_id',
        'instance', 'content',
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function store($identifier, $instance)
    {
        $id = $identifier->id;
        $type = get_class($identifier);
        $content = ShoppingCart::instance($instance)->content();;

        static::updateOrCreate([
            'identifierable_type' => $type,
            'identifierable_id' => $id,
            'instance' => $instance,
        ], [
            'content' => serialize($content),
        ]);
    }

    public static function restore($identifier, $instance = null)
    {
        $id = $identifier->id;
        $type = get_class($identifier);

        $where = [
            'identifierable_type' => $type,
            'identifierable_id' => $id,
        ];

        if ($instance) {
            $where['instance'] = $instance;
        }

        $carts = static::where($where)->get();
        if (count($carts)) {
            foreach ($carts as $cart) {
                $instance = $cart->instance;

                $storeExists = Store::where('slug', $instance)->exists();
                if (!$storeExists) {
                    $cart->delete();
                    continue;
                }

                $contents = unserialize($cart->content);
                $isEmpty = ShoppingCart::instance($instance)->count() == 0;
                if ($isEmpty) {
                    foreach ($contents as $content) {
                        $item = ShoppingCart::instance($instance)->add($content->id, $content->name, $content->qty, $content->price);
                        $item->associate('App\Models\Modules\Marketplaces\Product');
                    }
                }
            }
        }
    }

    public static function remove($identifier, $instance = null)
    {
        $id = $identifier->id;
        $type = get_class($identifier);

        $where = [
            'identifierable_type' => $type,
            'identifierable_id' => $id,
        ];

        if ($instance) {
            $where['instance'] = $instance;
            static::where($where)->delete();
        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function identifierable()
    {
        return $this->morphTo();
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
