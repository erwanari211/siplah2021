<?php

namespace App\Models\Modules\Marketplaces\Presenters;

trait GroupNotificationSchoolTrait {

    public $availableSchoolMethods = [
        'order_rejected',
        'order_confirmed',
        'order_packing_processing',
        'order_shipping_shipping',
        'order_shipping_shipped',
        'order_payment_from_customer_received',
        'order_payment_to_store_sent',
    ];

    public function fetchNotificationSchoolLabel()
    {
        if (in_array($this->name, $this->availableSchoolMethods)) {
            $method = $this->fetchMethodForLabel('school');
            return $this->callNotificationMethod($method);
        }

        return null;
    }

    public function fetchNotificationSchoolLink()
    {
        if (in_array($this->name, $this->availableSchoolMethods)) {
            $method = $this->fetchMethodForLink('school');
            return $this->callNotificationMethod($method);
        }

        return null;
    }

    // order_rejected : label
    public function handleOrderRejectedLabelForSchool()
    {
        return $this->description;
    }

    // order_rejected : link
    public function handleOrderRejectedLinkForSchool()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $order = $data['new'];
            $output = route('users.schools.orders.show', [$order->school_id, $order->id]);
        }

        return $output;
    }

    // order_confirmed : label
    public function handleOrderConfirmedLabelForSchool()
    {
        return $this->description;
    }

    // order_confirmed : link
    public function handleOrderConfirmedLinkForSchool()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $order = $data['new'];
            $output = route('users.schools.orders.show', [$order->school_id, $order->id]);
        }

        return $output;
    }

    // order_packing_processing : label
    public function handleOrderPackingProcessingLabelForSchool()
    {
        return $this->description;
    }

    // order_packing_processing : link
    public function handleOrderPackingProcessingLinkForSchool()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $order = $data['new'];
            $output = route('users.schools.orders.show', [$order->school_id, $order->id]);
        }

        return $output;
    }

    // order_shipping_shipping : label
    public function handleOrderShippingShippingLabelForSchool()
    {
        return $this->description;
    }

    // order_shipping_shipping : link
    public function handleOrderShippingShippingLinkForSchool()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $order = $data['new'];
            $output = route('users.schools.orders.show', [$order->school_id, $order->id]);
        }

        return $output;
    }

    // order_shipping_shipped : label
    public function handleOrderShippingShippedLabelForSchool()
    {
        return $this->description;
    }

    // order_shipping_shipped : link
    public function handleOrderShippingShippedLinkForSchool()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $order = $data['new'];
            $output = route('users.schools.orders.show', [$order->school_id, $order->id]);
        }

        return $output;
    }

    // order_payment_from_customer_received : label
    public function handleOrderPaymentFromCustomerReceivedLabelForSchool()
    {
        return $this->description;
    }

    // order_payment_from_customer_received : link
    public function handleOrderPaymentFromCustomerReceivedLinkForSchool()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $order = $data['new'];
            $output = route('users.schools.orders.show', [$order->school_id, $order->id]);
        }

        return $output;
    }


    // order_payment_to_store_sent : label
    public function handleOrderPaymentToStoreSentLabelForSchool()
    {
        return $this->description;
    }

    // order_payment_to_store_sent : link
    public function handleOrderPaymentToStoreSentLinkForSchool()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $order = $data['new'];
            $output = route('users.schools.orders.show', [$order->school_id, $order->id]);
        }

        return $output;
    }
}
