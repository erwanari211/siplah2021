<?php

namespace App\Models\Modules\Marketplaces\Presenters;

trait GroupNotificationStoreTrait {

    public $availableStoreMethods = [
        'store_status', 'order_created',
        'order_delivered',
        'order_payment_from_customer',
        'order_payment_to_store',
        'product_approval_pending',
        'product_approval_approved',
        'product_approval_rejected',
    ];

    public function fetchNotificationStoreLabel()
    {
        if (in_array($this->name, $this->availableStoreMethods)) {
            $method = $this->fetchMethodForLabel('store');
            return $this->callNotificationMethod($method);
        }

        return null;
    }

    public function fetchNotificationStoreLink()
    {
        if (in_array($this->name, $this->availableStoreMethods)) {
            $method = $this->fetchMethodForLink('store');
            return $this->callNotificationMethod($method);
        }

        return null;
    }

    // store_status : label
    public function handleStoreStatusLabelForStore()
    {
        return $this->description;
    }

    // store_status : link
    public function handleStoreStatusLinkForStore()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $store = $data['new'];
            if (!$store->active) {
                if (in_array($store->status, ['rejected'])) {
                    $output = route('users.stores.settings.store-registration.edit', $store->id);
                }
            }
        }

        return $output;
    }

    // order_created : label
    public function handleOrderCreatedLabelForStore()
    {
        return $this->description;
    }

    // order_created : link
    public function handleOrderCreatedLinkForStore()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $order = $data['new'];
            $output = route('users.stores.orders.show', [$order->store_id, $order->id]);
        }

        return $output;
    }

    // order_payment_from_customer : label
    public function handleOrderPaymentFromCustomerLabelForStore()
    {
        return $this->description;
    }

    // order_payment_from_customer : link
    public function handleOrderPaymentFromCustomerLinkForStore()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $order = $data['new'];
            $output = route('users.stores.orders.show', [$order->store_id, $order->id]);
        }

        return $output;
    }

    // order_payment_to_store : label
    public function handleOrderPaymentToStoreLabelForStore()
    {
        return $this->description;
    }

    // order_payment_to_store : link
    public function handleOrderPaymentToStoreLinkForStore()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $order = $data['new'];
            $output = route('users.stores.orders.show', [$order->store_id, $order->id]);
        }

        return $output;
    }

    // product_approval_approved : label
    public function handleProductApprovalApprovedLabelForStore()
    {
        $output = $this->description;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $item = $data['new'];
            $product = $item->product;
            $output = 'Produk "' . $product->name . '" telah disetujui';
        }
        return $output;
    }

    // product_approval_approved : link
    public function handleProductApprovalApprovedLinkForStore()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $item = $data['new'];
            $product = $item->product;
            $output = route('users.stores.products.show', [$product->store_id, $product->id]);
        }

        return $output;
    }

    // product_approval_rejected : label
    public function handleProductApprovalRejectedLabelForStore()
    {
        $output = $this->description;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $item = $data['new'];
            $product = $item->product;
            $reason = $item->reason;
            $output = 'Produk "' . $product->name . '" telah ditolak';
            $output .= ' karena "' . $reason . '"';
        }
        return $output;
    }

    // product_approval_rejected : link
    public function handleProductApprovalRejectedLinkForStore()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $item = $data['new'];
            $product = $item->product;
            $output = route('users.stores.products.show', [$product->store_id, $product->id]);
        }

        return $output;
    }

    // product_approval_pending : label
    public function handleProductApprovalPendingLabelForStore()
    {
        $output = $this->description;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $item = $data['new'];
            $product = $item->product;
            $output = 'Produk "' . $product->name . '" telah dipending';
        }
        return $output;
    }

    // product_approval_pending : link
    public function handleProductApprovalPendingLinkForStore()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $item = $data['new'];
            $product = $item->product;
            $output = route('users.stores.products.show', [$product->store_id, $product->id]);
        }

        return $output;
    }


    // order_delivered : label
    public function handleOrderDeliveredLabelForStore()
    {
        return $this->description;
    }

    // order_delivered : link
    public function handleOrderDeliveredLinkForStore()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data ?? null);
        if($data['new'] ?? null) {
            $order = $data['new'];
            $output = route('users.stores.orders.show', [$order->store_id, $order->id]);
        }

        return $output;
    }
}
