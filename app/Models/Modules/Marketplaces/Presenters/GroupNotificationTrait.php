<?php

namespace App\Models\Modules\Marketplaces\Presenters;

use App\Models\Modules\Marketplaces\Presenters\GroupNotificationMarketplaceTrait;
use App\Models\Modules\Marketplaces\Presenters\GroupNotificationStoreTrait;
use App\Models\Modules\Marketplaces\Presenters\GroupNotificationSchoolTrait;
use Illuminate\Support\Str;

trait GroupNotificationTrait {

    use GroupNotificationMarketplaceTrait;
    use GroupNotificationStoreTrait;
    use GroupNotificationSchoolTrait;

    public function fetchMethodForLink($type = null)
    {
        $name = $this->name;
        $method = 'handle'. Str::studly($name) . 'Link';
        if ($type) {
            $method .= 'For' . Str::studly($type);
        }
        return $method;
    }

    public function fetchMethodForLabel($type = null)
    {
        $name = $this->name;
        $method = 'handle'. Str::studly($name) . 'Label';
        if ($type) {
            $method .= 'For' . Str::studly($type);
        }
        return $method;
    }

    public function callNotificationMethod($method, $params = [])
    {
        $isMethodExists = method_exists($this, $method);
        if($isMethodExists) {
            if(is_callable([$this, $method])){
                return call_user_func_array([$this, $method], $params);
            }
        }
    }

    public function fetchNotificationLabel()
    {
        if ($this->groupable_type == 'App\Models\Modules\Marketplaces\Marketplace') {
            return $this->fetchNotificationMarketplaceLabel();
        }

        if ($this->groupable_type == 'App\Models\Modules\Marketplaces\Store') {
            return $this->fetchNotificationStoreLabel();
        }

        if ($this->groupable_type == 'App\Models\Modules\Marketplaces\School') {
            return $this->fetchNotificationSchoolLabel();
        }

        return null;
    }

    public function fetchNotificationLink()
    {
        if ($this->groupable_type == 'App\Models\Modules\Marketplaces\Marketplace') {
            return $this->fetchNotificationMarketplaceLink();
        }

        if ($this->groupable_type == 'App\Models\Modules\Marketplaces\Store') {
            return $this->fetchNotificationStoreLink();
        }

        if ($this->groupable_type == 'App\Models\Modules\Marketplaces\School') {
            return $this->fetchNotificationSchoolLink();
        }

        return null;
    }
}
