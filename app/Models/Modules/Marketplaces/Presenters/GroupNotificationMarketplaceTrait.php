<?php

namespace App\Models\Modules\Marketplaces\Presenters;

use Illuminate\Support\Str;

trait GroupNotificationMarketplaceTrait {

    public $availableMarketplaceMethods = [
        'store_registration',
        'store_resend_registration',
        'order_payment_from_customer',
        'product_created',
    ];

    public function fetchNotificationMarketplaceLabel()
    {
        if (in_array($this->name, $this->availableMarketplaceMethods)) {
            $method = $this->fetchMethodForLabel('marketplace');
            return $this->callNotificationMethod($method);
        }

        return null;
    }

    public function fetchNotificationMarketplaceLink()
    {
        if (in_array($this->name, $this->availableMarketplaceMethods)) {
            $method = $this->fetchMethodForLink('marketplace');
            return $this->callNotificationMethod($method);
        }

        return null;
    }

    // store_registration : label
    public function handleStoreRegistrationLabelForMarketplace()
    {
        $data = json_decode($this->data);
        $store = collect($data->store ?? null);
        $output = 'Toko telah melakukan pendaftaran';
        if($store) {
            $storeName = $store['name'];
            $output = $storeName . ' telah melakukan pendaftaran';
        }

        return $output;
    }

    // store_registration : link
    public function handleStoreRegistrationLinkForMarketplace()
    {
        $output = null;
        $data = json_decode($this->data);
        $store = collect($data->store ?? null);
        if($store) {
            $marketplace = $this->groupable;
            $output = route('users.marketplaces.stores.show', [$marketplace->id, $store['id']]);
        }

        return $output;
    }

    // store_resend_registration : label
    public function handleStoreResendRegistrationLabelForMarketplace()
    {
        $data = json_decode($this->data);
        $store = collect($data->store ?? null);
        $output = 'Toko telah mengirim ulang data pendaftaran';
        if($store) {
            $storeName = $store['name'];
            $output = $storeName . ' telah mengirim ulang data pendaftaran';
        }

        return $output;
    }

    // store_resend_registration : link
    public function handleStoreResendRegistrationLinkForMarketplace()
    {
        $output = null;
        $data = json_decode($this->data);
        $store = collect($data->store ?? null);
        if($store) {
            $marketplace = $this->groupable;
            $output = route('users.marketplaces.stores.show', [$marketplace->id, $store['id']]);
        }

        return $output;
    }

    // order_payment_from_customer : label
    public function handleOrderPaymentFromCustomerLabelForMarketplace()
    {
        $data = json_decode($this->data);
        $order = collect($data->new ?? null);
        $output = 'Pembeli telah membayar pesanan';
        if($order) {
        }

        return $output;
    }

    // order_payment_from_customer : link
    public function handleOrderPaymentFromCustomerLinkForMarketplace()
    {
        $output = null;
        $data = json_decode($this->data);
        $order = collect($data->new ?? null);
        if($order) {
            $marketplace = $this->groupable;
            $output = route('users.marketplaces.orders.show', [$marketplace->id, $order['id']]);
        }

        return $output;
    }

    // product_created : label
    public function handleProductCreatedLabelForMarketplace()
    {
        $output = $this->description;
        $data = json_decode($this->data);
        $data = collect($data->new ?? null);

        if($data) {
            $store = $data['store'];
            $output = $store->name . ' telah membuat produk baru';
        }
        return $output;
    }

    // product_created : link
    public function handleProductCreatedLinkForMarketplace()
    {
        $output = null;
        $data = json_decode($this->data);
        $data = collect($data->new ?? null);
        if($data) {
            $marketplace = $this->groupable;
            $product = $data['product'];
            $output = route('users.marketplaces.products.show', [$marketplace->id, $product->id]);
        }

        return $output;
    }
}
