<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;

class SupervisorGroup extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getUniqueSlug($slug, $id = null, $index = 0)
    {
        $newSlug = ($index == 0) ? $slug : $slug.'--'.$index;
        $rules = ['slug'=>'unique:stores,slug'];
        if ($id) {
            $rules = ['slug'=>'unique:stores,slug,'.$id];
        }

        $validator = \Validator::make(['slug'=>$newSlug],$rules);
        if($validator->fails()){
            $index++;
            return static::getUniqueSlug($slug, $id, $index);
        }

        return $newSlug;
    }

    public static function randomUniqueCode()
    {
        $table = 'supervisor_groups';
        $column = 'unique_code';

        $id = strtolower(str_random(8));
        $validator = \Validator::make(['id'=>$id],['id'=>'unique:'.$table.','.$column]);
        if($validator->fails()){
            static::randomUniqueCode();
        }

        return $id;
    }

    public function checkIsStaff($userId = null)
    {
        if (is_null($userId)) {
            if (auth()->check()) {
                $userId = auth()->user()->id;
            }
        }

        return $this->staffs()->where([
            'user_id' => $userId,
            'is_active' => true,
        ])->exists();
    }

    public function addGroupActivity($name, $data = null, $userId = null)
    {
        $userId = $userId ?? auth()->id();
        return $this->groupActivities()->create([
            'name' => $name,
            'user_id' => $userId ?? 0,
            'data' => $data ? json_encode($data) : null,
            'ip' => request()->ip(),
            'user_agent' => request()->server('HTTP_USER_AGENT'),
        ]);
    }

    public function staffLoggedIn()
    {
        $this->addGroupActivity('login');
        session([
            'login_as' => 'supervisor_staff',
            'supervisor_group_id' => $this->id,
        ]);
    }

    public function staffLoggedOut()
    {
        $loginAs = session('login_as');
        if ($loginAs == 'supervisor_staff') {
            $this->addGroupActivity('logout');
        }
    }

    public function updateSupervisorStaffLastOnline()
    {
        if (auth()->check()) {
            $userId = auth()->id();
            $staff = $this->staffs()->where('user_id', $userId)->first();
            if ($staff) {
                $staff->setMeta('last_online', date('Y-m-d H:i:s'));
            }
        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function supervisorStaffs()
    {
        return $this->hasMany('App\Models\Modules\Marketplaces\SupervisorStaff', 'supervisor_group_id');
    }

    public function staffs()
    {
        return $this->supervisorStaffs();
    }

    public function groupActivities()
    {
        return $this->morphMany('App\Models\Modules\Marketplaces\GroupActivity', 'groupable');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
