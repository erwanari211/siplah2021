<?php

namespace App\Models\Modules\Marketplaces;

use Illuminate\Database\Eloquent\Model;
use App\Models\Modules\Marketplaces\Presenters\GroupNotificationTrait;

class GroupNotification extends Model
{
    use GroupNotificationTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'groupable_type', 'groupable_id',
        'name', 'description', 'data',
        'read_at', 'read_by',
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function addNotification($group, $name, $additionalData = [])
    {
        $groupableId = $group->id;
        $groupableType = get_class($group);

        static::create([
            'groupable_type' => $groupableType,
            'groupable_id' => $groupableId,
            'name' => $name,
            'description' => $additionalData['description'] ?? null,
            'data' => isset($additionalData['data']) ? json_encode($additionalData['data']) : null,
        ]);
    }

    public function setAsRead()
    {
        if (auth()->check()) {
            if (is_null($this->read_at)) {
                $userId = auth()->id();
                $this->read_at = date('Y-m-d H:i:s');
                $this->read_by = $userId;
                $this->save();

                return true;
            }
        }

        return false;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function groupable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'read_by');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeUnread($query)
    {
        return $query->where('read_at', null);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getNotificationLabelAttribute()
    {
        return $this->fetchNotificationLabel();
    }

    public function getNotificationLinkAttribute()
    {
        return $this->fetchNotificationLink();
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
