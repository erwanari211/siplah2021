<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedBy($userId)
    {
        return $userId == $this->user_id;
    }

    public static function getUserAddressesDropdown($user)
    {
        $addresses = $user->addresses()->with('province', 'city', 'district', 'district.rajaongkirDistrict')->get();
        return $addresses->mapWithKeys(function($item) {
            $display_name = '';
            $label = $item['label'];
            $district = $item['district']['name'];
            $city = $item['city']['name'];
            $province = $item['province']['name'];
            $isDefault = $item['default_address'];
            if ($isDefault) {
                $display_name .= "* ";
            }
            $display_name .= "{$label} ({$district}, {$city}, {$province})";
            return [$item['id'] => $display_name];
        });
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince', 'province_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity', 'city_id');
    }

    public function district()
    {
        return $this->belongsTo('App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict', 'district_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
