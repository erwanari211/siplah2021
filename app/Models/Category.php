<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedByStore($storeId)
    {
        if ($this->categoriable_type == 'App\Models\Store') {
            $store = $this->categoriable;
            return $storeId == $store->id;
        }

        return false;
    }

    public function subcategories()
    {
        $parent_id = $this->id;
        return Category::where('parent_id', $parent_id)->get();
    }

    public function isOwnedByMarketplace($marketplaceId)
    {
        if ($this->categoriable_type == 'App\Models\Marketplace') {
            $marketplace = $this->categoriable;
            return $marketplaceId == $marketplace->id;
        }

        return false;
    }

    public function checkIsUnique($slug = null)
    {
        $categoriable_type = $this->categoriable_type;
        $categoriable_id = $this->categoriable_id;
        $type = $this->type;
        $slug = $slug ? $slug : $this->slug;

        $result = Category::where([
            'categoriable_type' => $categoriable_type,
            'categoriable_id' => $categoriable_id,
            'slug' => $slug,
        ])->get();

        if ($result->count() > 1) {
            return false;
        }

        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function categoriable()
    {
        return $this->morphTo();
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Modules\Marketplaces\Product');
    }

    public function childs()
    {
        return $this->hasMany('App\Models\Category', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
