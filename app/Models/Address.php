<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function isOwnedBy($userId)
    {
        if ($this->addressable_type == 'App\User') {
            $user = $this->addressable;
            return $userId == $user->id;
        }

        return false;
    }

    public function isOwnedByStore($storeId)
    {
        if ($this->addressable_type == 'App\Models\Store') {
            $store = $this->addressable;
            return $storeId == $store->id;
        }

        return false;
    }

    public static function getUserAddressesDropdown($user)
    {
        $addresses = $user->addresses()->with('province', 'city')->get();
        return $addresses->mapWithKeys(function($item) {
            $display_name = '';
            $label = $item['label'];
            $city = $item['city']['type'].' '.$item['city']['city_name'];
            $province = $item['city']['province'];
            $display_name .= "{$label} ({$city}, {$province})";
            return [$item['id'] => $display_name];
        });
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function addressable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\ZoneOngkirProvince', 'province_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\ZoneOngkirCity', 'city_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
