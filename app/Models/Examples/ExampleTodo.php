<?php

namespace App\Models\Examples;

use Illuminate\Database\Eloquent\Model;

class ExampleTodo extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    // protected $table = 'table';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = ['name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeCompleted($query)
    {
        return $query->where('completed', 1);
    }

    public function scopeIncomplete($query)
    {
        return $query->where('completed', 0);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getCompletedLabelAttribute($value)
    {
        $label = '';
        if ($this->completed) {
            $label = '<span class="btn btn-xs btn-success">Complete</span>';
        } else {
            $label = '<span class="btn btn-xs btn-warning">Incomplete</span>';
        }

        return $label;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
