<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Address
 *
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $addressable
 * @property-read \App\Models\ZoneOngkirCity $city
 * @property-read \App\Models\ZoneOngkirProvince $province
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address query()
 */
	class Address extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Category
 *
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $categoriable
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $childs
 * @property-read int|null $childs_count
 * @property-read \App\Models\Category $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category query()
 */
	class Category extends \Eloquent {}
}

namespace App\Models\Examples{
/**
 * App\Models\Examples\ExamplePost
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property string $excerpt
 * @property string|null $published_at
 * @property string $post_status
 * @property string $comment_status
 * @property int $views
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Examples\ExamplePost onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost whereCommentStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost wherePostStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExamplePost whereViews($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Examples\ExamplePost withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Examples\ExamplePost withoutTrashed()
 */
	class ExamplePost extends \Eloquent {}
}

namespace App\Models\Examples{
/**
 * App\Models\Examples\ExampleTodo
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int $completed
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $completed_label
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExampleTodo completed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExampleTodo incomplete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExampleTodo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExampleTodo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExampleTodo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExampleTodo whereCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExampleTodo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExampleTodo whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExampleTodo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExampleTodo whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examples\ExampleTodo whereUpdatedAt($value)
 */
	class ExampleTodo extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SampleModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SampleModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SampleModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SampleModel query()
 */
	class SampleModel extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\GroupedProductDetail
 *
 * @property int $id
 * @property int $parent_id
 * @property int $product_id
 * @property int $qty
 * @property string|null $note
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Product $parent
 * @property-read \App\Models\Modules\Marketplaces\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\GroupedProductDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\GroupedProductDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\GroupedProductDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\GroupedProductDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\GroupedProductDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\GroupedProductDetail whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\GroupedProductDetail whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\GroupedProductDetail whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\GroupedProductDetail whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\GroupedProductDetail whereUpdatedAt($value)
 */
	class GroupedProductDetail extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces\Kemdikbud{
/**
 * App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice
 *
 * @property int $id
 * @property int $product_id
 * @property int $zone
 * @property int $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice whereZone($value)
 */
	class ProductKemdikbudZonePrice extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\Marketplace
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $slug
 * @property string|null $image
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $logo
 * @property string|null $unique_code
 * @property int $views
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceActivitySearch[] $activitySearches
 * @property-read int|null $activity_searches_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup[] $bannerGroups
 * @property-read int|null $banner_groups_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceBanner[] $banners
 * @property-read int|null $banners_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceCategory[] $categories
 * @property-read int|null $categories_count
 * @property-read mixed $marketplace_url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary[] $mediaLibraries
 * @property-read int|null $media_libraries_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceProductCollection[] $productCollections
 * @property-read int|null $product_collections_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\Product[] $products
 * @property-read int|null $products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch[] $promoPopularSearches
 * @property-read int|null $promo_popular_searches_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch[] $promos
 * @property-read int|null $promos_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceSetting[] $settings
 * @property-read int|null $settings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection[] $storeCollections
 * @property-read int|null $store_collections_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\Store[] $stores
 * @property-read int|null $stores_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace whereUniqueCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Marketplace whereViews($value)
 */
	class Marketplace extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\MarketplaceActivitySearch
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $marketplace_id
 * @property int|null $store_id
 * @property string $search
 * @property string $viewed_at
 * @property string|null $ip_address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceActivitySearch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceActivitySearch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceActivitySearch query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceActivitySearch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceActivitySearch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceActivitySearch whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceActivitySearch whereMarketplaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceActivitySearch whereSearch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceActivitySearch whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceActivitySearch whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceActivitySearch whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceActivitySearch whereViewedAt($value)
 */
	class MarketplaceActivitySearch extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\MarketplaceBanner
 *
 * @property int $id
 * @property int $marketplace_id
 * @property string $image
 * @property string|null $link
 * @property int $active
 * @property string|null $expired_at
 * @property string|null $note
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $type
 * @property-read \App\Models\Modules\Marketplaces\Marketplace $marketplace
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail[] $marketplaceBannerGroupDetails
 * @property-read int|null $marketplace_banner_group_details_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner notExpired()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner whereMarketplaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBanner whereUpdatedAt($value)
 */
	class MarketplaceBanner extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\MarketplaceBannerGroup
 *
 * @property int $id
 * @property int $marketplace_id
 * @property string $name
 * @property int $active
 * @property string $type
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $display_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail[] $details
 * @property-read int|null $details_count
 * @property-read \App\Models\Modules\Marketplaces\Marketplace $marketplace
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup whereMarketplaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroup whereUpdatedAt($value)
 */
	class MarketplaceBannerGroup extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail
 *
 * @property int $id
 * @property int $marketplace_banner_group_id
 * @property int $marketplace_banner_id
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\MarketplaceBannerGroup $group
 * @property-read \App\Models\Modules\Marketplaces\MarketplaceBanner $marketplaceBanner
 * @property-read \App\Models\Modules\Marketplaces\MarketplaceBannerGroup $marketplaceBannerGroup
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail whereMarketplaceBannerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail whereMarketplaceBannerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceBannerGroupDetail whereUpdatedAt($value)
 */
	class MarketplaceBannerGroupDetail extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\MarketplaceCategory
 *
 * @property int $id
 * @property int $marketplace_id
 * @property int $parent_id
 * @property string $type
 * @property string $name
 * @property string $slug
 * @property string|null $description
 * @property string|null $image
 * @property int $active
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceCategory[] $children
 * @property-read int|null $children_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceCategory[] $childs
 * @property-read int|null $childs_count
 * @property-read \App\Models\Modules\Marketplaces\Marketplace $marketplace
 * @property-read \App\Models\Modules\Marketplaces\MarketplaceCategory $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory whereMarketplaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategory whereUpdatedAt($value)
 */
	class MarketplaceCategory extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\MarketplaceCategoryProduct
 *
 * @property-read \App\Models\Modules\Marketplaces\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategoryProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategoryProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceCategoryProduct query()
 */
	class MarketplaceCategoryProduct extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\MarketplaceMediaLibrary
 *
 * @property int $id
 * @property int $marketplace_id
 * @property string|null $unique_code
 * @property string $group
 * @property string|null $mimetype
 * @property string|null $name
 * @property string|null $description
 * @property string $url
 * @property int $active
 * @property string|null $tags
 * @property int $is_public
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $file_type
 * @property-read \App\Models\Modules\Marketplaces\Marketplace $marketplace
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary whereIsPublic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary whereMarketplaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary whereMimetype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary whereUniqueCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceMediaLibrary whereUrl($value)
 */
	class MarketplaceMediaLibrary extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\MarketplaceProductCollection
 *
 * @property int $id
 * @property int $marketplace_id
 * @property string $name
 * @property string|null $display_name
 * @property int $active
 * @property string $type
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail[] $details
 * @property-read int|null $details_count
 * @property-read \App\Models\Modules\Marketplaces\Marketplace $marketplace
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection whereMarketplaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollection whereUpdatedAt($value)
 */
	class MarketplaceProductCollection extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail
 *
 * @property int $id
 * @property int $marketplace_product_collection_id
 * @property int $product_id
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Product $product
 * @property-read \App\Models\Modules\Marketplaces\MarketplaceProductCollection $productCollection
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail whereMarketplaceProductCollectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceProductCollectionDetail whereUpdatedAt($value)
 */
	class MarketplaceProductCollectionDetail extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch
 *
 * @property int $id
 * @property int $marketplace_id
 * @property string $search
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Marketplace $marketplace
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch whereMarketplaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch whereSearch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplacePromoPopularSearch whereUpdatedAt($value)
 */
	class MarketplacePromoPopularSearch extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\MarketplaceSetting
 *
 * @property int $id
 * @property int $marketplace_id
 * @property string|null $group
 * @property string $key
 * @property string|null $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Marketplace $marketplace
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceSetting group($value = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceSetting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceSetting whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceSetting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceSetting whereMarketplaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceSetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceSetting whereValue($value)
 */
	class MarketplaceSetting extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\MarketplaceStoreCollection
 *
 * @property int $id
 * @property int $marketplace_id
 * @property string $name
 * @property string|null $display_name
 * @property int $active
 * @property string $type
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail[] $details
 * @property-read int|null $details_count
 * @property-read \App\Models\Modules\Marketplaces\Marketplace $marketplace
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection whereMarketplaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollection whereUpdatedAt($value)
 */
	class MarketplaceStoreCollection extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail
 *
 * @property int $id
 * @property int $marketplace_store_collection_id
 * @property int $store_id
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @property-read \App\Models\Modules\Marketplaces\MarketplaceStoreCollection $storeCollection
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail whereMarketplaceStoreCollectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\MarketplaceStoreCollectionDetail whereUpdatedAt($value)
 */
	class MarketplaceStoreCollectionDetail extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\Order
 *
 * @property int $id
 * @property int $store_id
 * @property int $user_id
 * @property string $store_name
 * @property string $user_name
 * @property float $total
 * @property string $currency
 * @property int $order_status_id
 * @property string $order_status
 * @property string|null $invoice_prefix
 * @property string|null $invoice_no
 * @property string|null $ip
 * @property string|null $user_agent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $payment_status
 * @property string $shipping_status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\OrderActivity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\OrderHistory[] $histories
 * @property-read int|null $histories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\OrderMeta[] $orderMetas
 * @property-read int|null $order_metas_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\OrderProduct[] $products
 * @property-read int|null $products_count
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order filter($status = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereInvoiceNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereInvoicePrefix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereOrderStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereOrderStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order wherePaymentStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereShippingStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereUserAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Order whereUserName($value)
 */
	class Order extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\OrderActivity
 *
 * @property int $id
 * @property int $order_id
 * @property int $user_id
 * @property string|null $group
 * @property string|null $activity
 * @property string|null $note
 * @property int $has_attachment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $attachments
 * @property-read \Illuminate\Database\Eloquent\Collection|\Plank\Metable\Meta[] $meta
 * @property-read int|null $meta_count
 * @property-read \App\Models\Modules\Marketplaces\Order $order
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity orderByMeta($key, $direction = 'asc', $strict = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity orderByMetaNumeric($key, $direction = 'asc', $strict = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereActivity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereDoesntHaveMeta($key)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereHasAttachment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereHasMeta($key)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereHasMetaKeys($keys)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereMeta($key, $operator, $value = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereMetaIn($key, $values)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereMetaNumeric($key, $operator, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderActivity whereUserId($value)
 */
	class OrderActivity extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\OrderHistory
 *
 * @property int $id
 * @property int $order_id
 * @property int $order_status_id
 * @property string|null $order_status
 * @property int $notify
 * @property string|null $note
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $has_attachment
 * @property-read mixed $attachments
 * @property-read \Illuminate\Database\Eloquent\Collection|\Plank\Metable\Meta[] $meta
 * @property-read int|null $meta_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory orderByMeta($key, $direction = 'asc', $strict = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory orderByMetaNumeric($key, $direction = 'asc', $strict = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereDoesntHaveMeta($key)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereHasAttachment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereHasMeta($key)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereHasMetaKeys($keys)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereMeta($key, $operator, $value = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereMetaIn($key, $values)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereMetaNumeric($key, $operator, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereNotify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereOrderStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereOrderStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderHistory whereUpdatedAt($value)
 */
	class OrderHistory extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\OrderMeta
 *
 * @property int $id
 * @property int $order_id
 * @property string|null $group
 * @property string $key
 * @property string|null $value
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderMeta newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderMeta newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderMeta query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderMeta whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderMeta whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderMeta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderMeta whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderMeta whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderMeta whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderMeta whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderMeta whereValue($value)
 */
	class OrderMeta extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\OrderProduct
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property string $product_name
 * @property int $qty
 * @property float $price
 * @property float $total
 * @property string|null $note
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct whereProductName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderProduct whereUpdatedAt($value)
 */
	class OrderProduct extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\OrderStatus
 *
 * @property int $id
 * @property string|null $group
 * @property string $key
 * @property string|null $value
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderStatus whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderStatus whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderStatus whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderStatus whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\OrderStatus whereValue($value)
 */
	class OrderStatus extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\Product
 *
 * @property int $id
 * @property int $store_id
 * @property int $user_id
 * @property string $name
 * @property string $slug
 * @property string|null $description
 * @property int $price
 * @property int $qty
 * @property string|null $image
 * @property int $active
 * @property int $views
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property float $weight
 * @property string $type
 * @property string $status
 * @property string|null $sku
 * @property string|null $unique_code
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\ProductActivityView[] $activityViews
 * @property-read int|null $activity_views_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\ProductAttribute[] $attributes
 * @property-read int|null $attributes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\ProductDiscussion[] $discussions
 * @property-read int|null $discussions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\ProductGallery[] $galleries
 * @property-read int|null $galleries_count
 * @property-read mixed $active_button
 * @property-read mixed $discount_amount_percent
 * @property-read mixed $image_url
 * @property-read mixed $product_url
 * @property-read mixed $status_button
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\GroupedProductDetail[] $groupedProductDetails
 * @property-read int|null $grouped_product_details_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\Kemdikbud\ProductKemdikbudZonePrice[] $kemdikbudZonePrices
 * @property-read int|null $kemdikbud_zone_prices_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\MarketplaceCategory[] $marketplaceCategories
 * @property-read int|null $marketplace_categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Plank\Metable\Meta[] $meta
 * @property-read int|null $meta_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\OrderProduct[] $orderProducts
 * @property-read int|null $order_products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\ProductReview[] $reviews
 * @property-read int|null $reviews_count
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreCategory[] $storeCategories
 * @property-read int|null $store_categories_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product filter($filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product orderByMeta($key, $direction = 'asc', $strict = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product orderByMetaNumeric($key, $direction = 'asc', $strict = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product search($search = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product status($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereDoesntHaveMeta($key)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereHasMeta($key)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereHasMetaKeys($keys)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereMeta($key, $operator, $value = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereMetaIn($key, $values)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereMetaNumeric($key, $operator, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereUniqueCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereViews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Product whereWeight($value)
 */
	class Product extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\ProductActivityView
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $marketplace_id
 * @property int|null $store_id
 * @property int|null $product_id
 * @property string $viewed_at
 * @property string|null $ip_address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Product|null $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductActivityView newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductActivityView newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductActivityView query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductActivityView whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductActivityView whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductActivityView whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductActivityView whereMarketplaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductActivityView whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductActivityView whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductActivityView whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductActivityView whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductActivityView whereViewedAt($value)
 */
	class ProductActivityView extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\ProductAttribute
 *
 * @property int $id
 * @property int $product_id
 * @property string|null $group
 * @property string $key
 * @property string|null $value
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductAttribute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductAttribute newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductAttribute query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductAttribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductAttribute whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductAttribute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductAttribute whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductAttribute whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductAttribute whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductAttribute whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductAttribute whereValue($value)
 */
	class ProductAttribute extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\ProductDiscussion
 *
 * @property int $id
 * @property int $product_id
 * @property int $user_id
 * @property string $content
 * @property int $active
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\ProductDiscussionDetail[] $details
 * @property-read int|null $details_count
 * @property-read \App\Models\Modules\Marketplaces\Product $product
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussion query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussion whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussion whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussion whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussion whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussion whereUserId($value)
 */
	class ProductDiscussion extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\ProductDiscussionDetail
 *
 * @property int $id
 * @property int $product_discussion_id
 * @property int $user_id
 * @property int $is_store_owner
 * @property string $content
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\ProductDiscussion $productDiscussion
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussionDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussionDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussionDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussionDetail whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussionDetail whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussionDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussionDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussionDetail whereIsStoreOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussionDetail whereProductDiscussionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussionDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductDiscussionDetail whereUserId($value)
 */
	class ProductDiscussionDetail extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\ProductGallery
 *
 * @property int $id
 * @property int $product_id
 * @property string $group
 * @property string|null $type
 * @property string|null $name
 * @property string $description
 * @property string $url
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $sort_order
 * @property-read mixed $file_type
 * @property-read \App\Models\Modules\Marketplaces\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductGallery whereUrl($value)
 */
	class ProductGallery extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\ProductReview
 *
 * @property int $id
 * @property int $product_id
 * @property int $user_id
 * @property int|null $order_id
 * @property int $rating
 * @property string|null $content
 * @property int $has_attachment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\ProductReviewAttachment[] $attachments
 * @property-read int|null $attachments_count
 * @property-read \App\Models\Modules\Marketplaces\Product $product
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReview newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReview newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReview query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReview whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReview whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReview whereHasAttachment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReview whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReview whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReview whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReview whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReview whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReview whereUserId($value)
 */
	class ProductReview extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\ProductReviewAttachment
 *
 * @property int $id
 * @property int $product_review_id
 * @property string $attachment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\ProductReview $productReview
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReviewAttachment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReviewAttachment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReviewAttachment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReviewAttachment whereAttachment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReviewAttachment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReviewAttachment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReviewAttachment whereProductReviewId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ProductReviewAttachment whereUpdatedAt($value)
 */
	class ProductReviewAttachment extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\ShoppingCart
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ShoppingCart newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ShoppingCart newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\ShoppingCart query()
 */
	class ShoppingCart extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\Store
 *
 * @property int $id
 * @property int $marketplace_id
 * @property int $user_id
 * @property string $name
 * @property string $slug
 * @property string|null $image
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $unique_code
 * @property int $views
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreAddress[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreBannerGroup[] $bannerGroups
 * @property-read int|null $banner_groups_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreBanner[] $banners
 * @property-read int|null $banners_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreCategory[] $categories
 * @property-read int|null $categories_count
 * @property-read mixed $store_url
 * @property-read \App\Models\Modules\Marketplaces\Marketplace $marketplace
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreMediaLibrary[] $mediaLibraries
 * @property-read int|null $media_libraries_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreMenu[] $menus
 * @property-read int|null $menus_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Plank\Metable\Meta[] $meta
 * @property-read int|null $meta_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreNote[] $notes
 * @property-read int|null $notes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StorePaymentMethod[] $paymentMethods
 * @property-read int|null $payment_methods_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\ProductDiscussion[] $productDiscussions
 * @property-read int|null $product_discussions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\ProductReview[] $productReviews
 * @property-read int|null $product_reviews_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\Product[] $products
 * @property-read int|null $products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreSetting[] $settings
 * @property-read int|null $settings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreShippingMethod[] $shippingMethods
 * @property-read int|null $shipping_methods_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreFeaturedProduct[] $storeFeaturedProducts
 * @property-read int|null $store_featured_products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreSeller[] $storeSellers
 * @property-read int|null $store_sellers_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store filterActive($status = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store orderByMeta($key, $direction = 'asc', $strict = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store orderByMetaNumeric($key, $direction = 'asc', $strict = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereDoesntHaveMeta($key)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereHasMeta($key)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereHasMetaKeys($keys)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereMarketplaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereMeta($key, $operator, $value = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereMetaIn($key, $values)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereMetaNumeric($key, $operator, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereUniqueCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\Store whereViews($value)
 */
	class Store extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreAddress
 *
 * @property int $id
 * @property int $store_id
 * @property string $label
 * @property string $full_name
 * @property string|null $company
 * @property string $address
 * @property string $province_id
 * @property string $city_id
 * @property string|null $phone
 * @property string|null $postcode
 * @property int $default_address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $district_id
 * @property-read \App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity $city
 * @property-read \App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict $district
 * @property-read \App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince $province
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress defaultAddress()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress whereDefaultAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress whereDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress wherePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreAddress whereUpdatedAt($value)
 */
	class StoreAddress extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreBanner
 *
 * @property int $id
 * @property int $store_id
 * @property string $image
 * @property string|null $link
 * @property int $active
 * @property string|null $expired_at
 * @property string|null $note
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreBannerGroupDetail[] $storeBannerGroupDetails
 * @property-read int|null $store_banner_group_details_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner notExpired()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBanner whereUpdatedAt($value)
 */
	class StoreBanner extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreBannerGroup
 *
 * @property int $id
 * @property int $store_id
 * @property string $name
 * @property int $active
 * @property string $type
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $display_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreBannerGroupDetail[] $details
 * @property-read int|null $details_count
 * @property-read \App\Models\Modules\Marketplaces\Marketplace $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroup whereUpdatedAt($value)
 */
	class StoreBannerGroup extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreBannerGroupDetail
 *
 * @property int $id
 * @property int $store_banner_group_id
 * @property int $store_banner_id
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\StoreBanner $storeBanner
 * @property-read \App\Models\Modules\Marketplaces\StoreBannerGroup $storeBannerGroup
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroupDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroupDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroupDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroupDetail sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroupDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroupDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroupDetail whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroupDetail whereStoreBannerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroupDetail whereStoreBannerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreBannerGroupDetail whereUpdatedAt($value)
 */
	class StoreBannerGroupDetail extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreCategory
 *
 * @property int $id
 * @property int $store_id
 * @property int $parent_id
 * @property string $type
 * @property string $name
 * @property string $slug
 * @property string|null $description
 * @property string|null $image
 * @property int $active
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreCategory[] $children
 * @property-read int|null $children_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreCategory[] $childs
 * @property-read int|null $childs_count
 * @property-read \App\Models\Modules\Marketplaces\StoreCategory $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\Product[] $products
 * @property-read int|null $products_count
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategory whereUpdatedAt($value)
 */
	class StoreCategory extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreCategoryProduct
 *
 * @property-read \App\Models\Modules\Marketplaces\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategoryProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategoryProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreCategoryProduct query()
 */
	class StoreCategoryProduct extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreFeaturedProduct
 *
 * @property int $id
 * @property int $store_id
 * @property string $name
 * @property string $slug
 * @property string|null $description
 * @property string|null $image
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreFeaturedProductDetail[] $details
 * @property-read int|null $details_count
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProduct active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProduct whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProduct whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProduct whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProduct whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProduct whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProduct whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProduct whereUpdatedAt($value)
 */
	class StoreFeaturedProduct extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreFeaturedProductDetail
 *
 * @property int $id
 * @property int $store_featured_product_id
 * @property int $product_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Product $product
 * @property-read \App\Models\Modules\Marketplaces\StoreFeaturedProduct $storeFeaturedProduct
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProductDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProductDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProductDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProductDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProductDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProductDetail whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProductDetail whereStoreFeaturedProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreFeaturedProductDetail whereUpdatedAt($value)
 */
	class StoreFeaturedProductDetail extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreMediaLibrary
 *
 * @property int $id
 * @property int $store_id
 * @property string|null $unique_code
 * @property string $group
 * @property string|null $mimetype
 * @property string|null $name
 * @property string|null $description
 * @property string $url
 * @property int $active
 * @property string|null $tags
 * @property int $is_public
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $file_type
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary whereIsPublic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary whereMimetype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary whereUniqueCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMediaLibrary whereUrl($value)
 */
	class StoreMediaLibrary extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreMenu
 *
 * @property int $id
 * @property int $store_id
 * @property string|null $group
 * @property string|null $label
 * @property string|null $link
 * @property int $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $sort_order
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\StoreMenu[] $children
 * @property-read int|null $children_count
 * @property-read \App\Models\Modules\Marketplaces\StoreMenu|null $parent
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMenu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMenu newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMenu query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMenu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMenu whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMenu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMenu whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMenu whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMenu whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMenu whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMenu whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreMenu whereUpdatedAt($value)
 */
	class StoreMenu extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreNote
 *
 * @property int $id
 * @property int $user_id
 * @property int $store_id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreNote newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreNote newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreNote query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreNote whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreNote whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreNote whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreNote whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreNote whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreNote whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreNote whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreNote whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreNote whereUserId($value)
 */
	class StoreNote extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StorePaymentMethod
 *
 * @property int $id
 * @property int $store_id
 * @property string $label
 * @property string $name
 * @property string|null $info
 * @property int $default_payment_method
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StorePaymentMethod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StorePaymentMethod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StorePaymentMethod query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StorePaymentMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StorePaymentMethod whereDefaultPaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StorePaymentMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StorePaymentMethod whereInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StorePaymentMethod whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StorePaymentMethod whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StorePaymentMethod whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StorePaymentMethod whereUpdatedAt($value)
 */
	class StorePaymentMethod extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreSeller
 *
 * @property int $id
 * @property int $store_id
 * @property string $seller_name
 * @property string|null $note
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSeller newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSeller newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSeller query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSeller whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSeller whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSeller whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSeller whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSeller whereSellerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSeller whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSeller whereUpdatedAt($value)
 */
	class StoreSeller extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreSetting
 *
 * @property int $id
 * @property int $store_id
 * @property string|null $group
 * @property string $key
 * @property string|null $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSetting group($value = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSetting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSetting shoppingMethods()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSetting whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSetting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSetting whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreSetting whereValue($value)
 */
	class StoreSetting extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\StoreShippingMethod
 *
 * @property int $id
 * @property int $store_id
 * @property string $group
 * @property string $label
 * @property string $name
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Store $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreShippingMethod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreShippingMethod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreShippingMethod query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreShippingMethod whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreShippingMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreShippingMethod whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreShippingMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreShippingMethod whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreShippingMethod whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreShippingMethod whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\StoreShippingMethod whereUpdatedAt($value)
 */
	class StoreShippingMethod extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property int $active
 * @property string $type
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail[] $details
 * @property-read int|null $details_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection whereUpdatedAt($value)
 */
	class WebsiteMarketplaceCollection extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail
 *
 * @property int $id
 * @property int $website_marketplace_collection_id
 * @property int $marketplace_id
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\WebsiteMarketplaceCollection $collection
 * @property-read \App\Models\Modules\Marketplaces\Marketplace $marketplace
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail whereMarketplaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteMarketplaceCollectionDetail whereWebsiteMarketplaceCollectionId($value)
 */
	class WebsiteMarketplaceCollectionDetail extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\WebsiteProductCollection
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property int $active
 * @property string $type
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $link
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail[] $details
 * @property-read int|null $details_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollection whereUpdatedAt($value)
 */
	class WebsiteProductCollection extends \Eloquent {}
}

namespace App\Models\Modules\Marketplaces{
/**
 * App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail
 *
 * @property int $id
 * @property int $website_product_collection_id
 * @property int $product_id
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\WebsiteProductCollection $collection
 * @property-read \App\Models\Modules\Marketplaces\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Marketplaces\WebsiteProductCollectionDetail whereWebsiteProductCollectionId($value)
 */
	class WebsiteProductCollectionDetail extends \Eloquent {}
}

namespace App\Models\Modules\Website{
/**
 * App\Models\Modules\Website\WebsiteBanner
 *
 * @property int $id
 * @property string $image
 * @property string|null $link
 * @property int $active
 * @property string|null $expired_at
 * @property string|null $note
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner notExpired()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBanner whereUpdatedAt($value)
 */
	class WebsiteBanner extends \Eloquent {}
}

namespace App\Models\Modules\Website{
/**
 * App\Models\Modules\Website\WebsiteBannerGroup
 *
 * @property int $id
 * @property string $name
 * @property int $active
 * @property string $type
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $display_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Website\WebsiteBannerGroupDetail[] $details
 * @property-read int|null $details_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroup active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroup sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroup whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroup whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroup whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroup whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroup whereUpdatedAt($value)
 */
	class WebsiteBannerGroup extends \Eloquent {}
}

namespace App\Models\Modules\Website{
/**
 * App\Models\Modules\Website\WebsiteBannerGroupDetail
 *
 * @property int $id
 * @property int $website_banner_group_id
 * @property int $website_banner_id
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Website\WebsiteBannerGroup $group
 * @property-read \App\Models\Modules\Website\WebsiteBanner $websiteBanner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroupDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroupDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroupDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroupDetail sort()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroupDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroupDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroupDetail whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroupDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroupDetail whereWebsiteBannerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBannerGroupDetail whereWebsiteBannerId($value)
 */
	class WebsiteBannerGroupDetail extends \Eloquent {}
}

namespace App\Models\Modules\Website{
/**
 * App\Models\Modules\Website\WebsiteBlogBanner
 *
 * @property int $id
 * @property string $image
 * @property string|null $link
 * @property int $active
 * @property string $type
 * @property string|null $note
 * @property int $sort_order
 * @property string|null $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogBanner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogBanner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogBanner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogBanner whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogBanner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogBanner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogBanner whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogBanner whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogBanner whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogBanner wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogBanner whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogBanner whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogBanner whereUpdatedAt($value)
 */
	class WebsiteBlogBanner extends \Eloquent {}
}

namespace App\Models\Modules\Website{
/**
 * App\Models\Modules\Website\WebsiteBlogCategory
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $description
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Website\WebsiteBlogPost[] $posts
 * @property-read int|null $posts_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogCategory whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogCategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogCategory whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogCategory whereUpdatedAt($value)
 */
	class WebsiteBlogCategory extends \Eloquent {}
}

namespace App\Models\Modules\Website{
/**
 * App\Models\Modules\Website\WebsiteBlogPost
 *
 * @property int $id
 * @property int $user_id
 * @property string $unique_code
 * @property string $title
 * @property string $content
 * @property string|null $excerpt
 * @property string $slug
 * @property string $type
 * @property string|null $mime_type
 * @property string $status
 * @property string|null $published_at
 * @property string $comment_status
 * @property string|null $password
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Website\WebsiteBlogCategory[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Plank\Metable\Meta[] $meta
 * @property-read int|null $meta_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Website\WebsiteBlogPostProduct[] $products
 * @property-read int|null $products_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost orderByMeta($key, $direction = 'asc', $strict = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost orderByMetaNumeric($key, $direction = 'asc', $strict = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereCommentStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereDoesntHaveMeta($key)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereHasMeta($key)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereHasMetaKeys($keys)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereMeta($key, $operator, $value = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereMetaIn($key, $values)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereMetaNumeric($key, $operator, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereMimeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereUniqueCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPost whereUserId($value)
 */
	class WebsiteBlogPost extends \Eloquent {}
}

namespace App\Models\Modules\Website{
/**
 * App\Models\Modules\Website\WebsiteBlogPostProduct
 *
 * @property int $id
 * @property int $website_blog_post_id
 * @property int $product_id
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Marketplaces\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPostProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPostProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPostProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPostProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPostProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPostProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPostProduct whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPostProduct whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteBlogPostProduct whereWebsiteBlogPostId($value)
 */
	class WebsiteBlogPostProduct extends \Eloquent {}
}

namespace App\Models\Modules\Website{
/**
 * App\Models\Modules\Website\WebsiteLayoutElement
 *
 * @property int $id
 * @property string|null $group
 * @property string|null $position
 * @property string $name
 * @property string $content
 * @property int $active
 * @property int $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteLayoutElement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteLayoutElement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteLayoutElement query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteLayoutElement whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteLayoutElement whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteLayoutElement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteLayoutElement whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteLayoutElement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteLayoutElement whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteLayoutElement wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteLayoutElement whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteLayoutElement whereUpdatedAt($value)
 */
	class WebsiteLayoutElement extends \Eloquent {}
}

namespace App\Models\Modules\Website{
/**
 * App\Models\Modules\Website\WebsiteMediaLibrary
 *
 * @property int $id
 * @property string|null $unique_code
 * @property string $group
 * @property string|null $mimetype
 * @property string|null $name
 * @property string|null $description
 * @property string $url
 * @property int $active
 * @property string|null $tags
 * @property int $is_public
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $file_type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary whereIsPublic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary whereMimetype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary whereUniqueCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteMediaLibrary whereUrl($value)
 */
	class WebsiteMediaLibrary extends \Eloquent {}
}

namespace App\Models\Modules\Website{
/**
 * App\Models\Modules\Website\WebsitePage
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsitePage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsitePage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsitePage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsitePage whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsitePage whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsitePage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsitePage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsitePage whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsitePage whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsitePage whereUpdatedAt($value)
 */
	class WebsitePage extends \Eloquent {}
}

namespace App\Models\Modules\Website{
/**
 * App\Models\Modules\Website\WebsiteSettings
 *
 * @property int $id
 * @property string|null $group
 * @property string $key
 * @property string|null $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteSettings group($value = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteSettings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteSettings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteSettings query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteSettings whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteSettings whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteSettings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteSettings whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteSettings whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Website\WebsiteSettings whereValue($value)
 */
	class WebsiteSettings extends \Eloquent {}
}

namespace App\Models\Modules\Wilayah\Kemdikbud{
/**
 * App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity
 *
 * @property string $id
 * @property string $province_id
 * @property string $name
 * @property int $zone
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict[] $districts
 * @property-read int|null $districts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity[] $indonesiaCity
 * @property-read int|null $indonesia_city_count
 * @property-read \App\Models\Modules\Wilayah\Kemdikbud\KemdikbudProvince $province
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity search($name = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity whereZone($value)
 */
	class KemdikbudCity extends \Eloquent {}
}

namespace App\Models\Modules\Wilayah\Kemdikbud{
/**
 * App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict
 *
 * @property string $id
 * @property string $city_id
 * @property string $name
 * @property int $zone
 * @property-read \App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity $city
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict[] $indonesiaDistrict
 * @property-read int|null $indonesia_district_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict search($name = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict whereZone($value)
 */
	class KemdikbudDistrict extends \Eloquent {}
}

namespace App\Models\Modules\Wilayah\Kemdikbud{
/**
 * App\Models\Modules\Wilayah\Kemdikbud\KemdikbudProvince
 *
 * @property string $id
 * @property string $name
 * @property int $zone
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity[] $cities
 * @property-read int|null $cities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince[] $indonesiaProvince
 * @property-read int|null $indonesia_province_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudProvince newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudProvince newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudProvince query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudProvince search($name = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudProvince whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudProvince whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudProvince whereZone($value)
 */
	class KemdikbudProvince extends \Eloquent {}
}

namespace App\Models\Modules\Wilayah\Rajaongkir{
/**
 * App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity
 *
 * @property int $city_id
 * @property int $province_id
 * @property string $type
 * @property string $city_name
 * @property int $postal_code
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict[] $districts
 * @property-read int|null $districts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity[] $indonesiaCity
 * @property-read int|null $indonesia_city_count
 * @property-read \App\Models\Modules\Wilayah\Rajaongkir\RajaongkirProvince $province
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity byProvince($provinceId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity search($name = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity whereCityName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity whereType($value)
 */
	class RajaongkirCity extends \Eloquent {}
}

namespace App\Models\Modules\Wilayah\Rajaongkir{
/**
 * App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict
 *
 * @property int $district_id
 * @property int $city_id
 * @property string $district_name
 * @property-read \App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity $city
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict[] $indonesiaDistrict
 * @property-read int|null $indonesia_district_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict byCity($cityId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict search($name = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict whereDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict whereDistrictName($value)
 */
	class RajaongkirDistrict extends \Eloquent {}
}

namespace App\Models\Modules\Wilayah\Rajaongkir{
/**
 * App\Models\Modules\Wilayah\Rajaongkir\RajaongkirProvince
 *
 * @property int $province_id
 * @property string $province
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity[] $cities
 * @property-read int|null $cities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince[] $indonesiaProvince
 * @property-read int|null $indonesia_province_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirProvince newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirProvince newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirProvince query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirProvince search($name = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirProvince whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirProvince whereProvinceId($value)
 */
	class RajaongkirProvince extends \Eloquent {}
}

namespace App\Models\Modules\Wilayah\WilayahAdministratif{
/**
 * App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity
 *
 * @property string $id
 * @property string $province_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict[] $districts
 * @property-read int|null $districts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudCity[] $kemdikbudCity
 * @property-read int|null $kemdikbud_city_count
 * @property-read \App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince $province
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirCity[] $rajaongkirCity
 * @property-read int|null $rajaongkir_city_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity search($name = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity whereUpdatedAt($value)
 */
	class IndonesiaCity extends \Eloquent {}
}

namespace App\Models\Modules\Wilayah\WilayahAdministratif{
/**
 * App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict
 *
 * @property string $id
 * @property string $city_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity $city
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudDistrict[] $kemdikbudDistrict
 * @property-read int|null $kemdikbud_district_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirDistrict[] $rajaongkirDistrict
 * @property-read int|null $rajaongkir_district_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaVillage[] $villages
 * @property-read int|null $villages_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict search($name = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict whereUpdatedAt($value)
 */
	class IndonesiaDistrict extends \Eloquent {}
}

namespace App\Models\Modules\Wilayah\WilayahAdministratif{
/**
 * App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince
 *
 * @property string $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity[] $cities
 * @property-read int|null $cities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\Kemdikbud\KemdikbudProvince[] $kemdikbudProvince
 * @property-read int|null $kemdikbud_province_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Wilayah\Rajaongkir\RajaongkirProvince[] $rajaongkirProvince
 * @property-read int|null $rajaongkir_province_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince search($name = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince whereUpdatedAt($value)
 */
	class IndonesiaProvince extends \Eloquent {}
}

namespace App\Models\Modules\Wilayah\WilayahAdministratif{
/**
 * App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaVillage
 *
 * @property string $id
 * @property string $district_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict $district
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaVillage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaVillage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaVillage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaVillage search($name = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaVillage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaVillage whereDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaVillage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaVillage whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaVillage whereUpdatedAt($value)
 */
	class IndonesiaVillage extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\UserAddress
 *
 * @property int $id
 * @property int $user_id
 * @property string $label
 * @property string $full_name
 * @property string|null $company
 * @property string $address
 * @property string $province_id
 * @property string $city_id
 * @property string|null $phone
 * @property string|null $postcode
 * @property int $default_address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $district_id
 * @property-read \App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaCity $city
 * @property-read \App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaDistrict $district
 * @property-read \App\Models\Modules\Wilayah\WilayahAdministratif\IndonesiaProvince $province
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereDefaultAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress wherePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAddress whereUserId($value)
 */
	class UserAddress extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ZoneOngkirCity
 *
 * @property int $city_id
 * @property int $province_id
 * @property \App\Models\ZoneOngkirProvince $province
 * @property string $type
 * @property string $city_name
 * @property int $postal_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirCity getDropdown($provinceId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirCity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirCity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirCity query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirCity selectColumnForDropdown()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirCity whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirCity whereCityName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirCity wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirCity whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirCity whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirCity whereType($value)
 */
	class ZoneOngkirCity extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ZoneOngkirProvince
 *
 * @property int $province_id
 * @property string $province
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ZoneOngkirCity[] $cities
 * @property-read int|null $cities_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirProvince newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirProvince newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirProvince query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirProvince whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ZoneOngkirProvince whereProvinceId($value)
 */
	class ZoneOngkirProvince extends \Eloquent {}
}

namespace App{
/**
 * App\Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereUpdatedAt($value)
 */
	class Permission extends \Eloquent {}
}

namespace App{
/**
 * App\Role
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $photo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserAddress[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\Marketplace[] $marketplaces
 * @property-read int|null $marketplaces_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Modules\Marketplaces\Store[] $stores
 * @property-read int|null $stores_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePermissionIs($permission = '')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRoleIs($role = '')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUsername($value)
 */
	class User extends \Eloquent {}
}

