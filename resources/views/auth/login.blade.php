@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Login')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <div class="box">
                        <div class="box-content">
                            <div class="auth-form">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <h3 class="auth-form-header">Login</h3>

                                    <div class="mb-3 text-center">
                                        <a class="btn btn-primary" href="{{ route('marketplaces.login.create') }}">
                                            Login Sebagai Admin
                                        </a>
                                    </div>

                                    <div class="mb-3 text-center">
                                        <a class="btn btn-primary" href="{{ route('marketplaces.stores.login.create') }}">
                                            Login Sebagai Penyedia
                                        </a>
                                    </div>

                                    <div class="mb-3 text-center">
                                        <a class="btn btn-primary" href="{{ route('marketplaces.supervisor-groups.login.create') }}">
                                            Login Sebagai Pengawas
                                        </a>
                                    </div>

                                    <div class="mb-3 text-center">
                                        <a class="btn btn-primary" href="{{ route('marketplaces.schools.login.create') }}">
                                            Login Sebagai Satdik
                                        </a>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
