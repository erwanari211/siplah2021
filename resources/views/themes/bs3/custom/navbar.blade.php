<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">PlanetBuku</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Link</a></li>
                <li><a href="#">Link</a></li>
            </ul>
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Link</a></li>

                @php
                    $cartExist = $_navbarCart && $_navbarCart['count'] ? true : false;
                    $cartItems = 0;
                    $cartSubtotal = 0;
                    if ($cartExist) {
                        $cartItems = $_navbarCart['count'];
                        $cartSubtotal = $_navbarCart['subtotal'];
                    }
                @endphp

                <li class="dropdown">
                    <a href="{{ route('cart.index') }}" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="label label-danger">{{ $cartItems }}</span>
                        <i class="fa fa-shopping-cart"></i> Cart
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        @if ($cartExist)
                            <li><a href="#">{{ $cartItems .' '. str_plural('item', $cartItems) }}</a></li>
                            <li><a href="#">{{ formatNumber($cartSubtotal) }}</a></li>
                            <li>
                                <a href="{{ route('cart.index') }}">
                                     <i class="fa fa-shopping-cart"></i> View cart
                                </a>
                            </li>
                        @else
                            <li><a href="#">Cart is empty</a></li>
                        @endif
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        @if (!auth()->check())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li>
                                <a href="{{ route('users.accounts.profile.index') }}">
                                    <i class="fa fa-user"></i> {{ auth()->user()->name }}
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out"></i> {{ __('Logout') }}
                                </a>
                            </li>


                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endif
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>
