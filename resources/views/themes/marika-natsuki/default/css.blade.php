<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('assets/marika-natsuki/plugins') }}/bootstrap-3.3.7/css/bootstrap.min.css" >

<!-- Plugins CSS -->
<link rel="stylesheet" href="{{ asset('assets/marika-natsuki/plugins') }}/fontawesome-free-5.6.1-web/css/all.min.css">
<link rel="stylesheet" href="{{ asset('assets/marika-natsuki/plugins') }}/toastr/build/toastr.min.css">
<link rel="stylesheet" href="{{ asset('assets/marika-natsuki/plugins') }}/metismenu/dist/metisMenu.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/owl-carousel2-2.3.4/dist/assets/owl.carousel.min.css">
<!-- Custom Plugins CSS -->
<link rel="stylesheet" href="{{ asset('assets/marika-natsuki/css') }}/plugins/fontawesome-5-stars.css">
<link rel="stylesheet" href="{{ asset('assets/marika-natsuki/css') }}/plugins/metismenu-vertical-light.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/css') }}/plugins/owl.theme.custom.css">

<!-- App CSS -->
<link rel="stylesheet" href="{{ asset('assets/marika-natsuki/css') }}/app.css">
<link rel="stylesheet" href="{{ asset('assets/marika-natsuki/css') }}/skins.css">
<link rel="stylesheet" href="{{ asset('css') }}/custom.css">

<!-- Page CSS -->
@stack('css')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
