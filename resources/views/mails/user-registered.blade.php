@component('mail::message')
# Welcome to {{ config('app.name') }}

Thank you for registering {{ config('app.name') }}.<br/>

**Registration Detail**<br/>
Username : **{{ $user->username }}**<br/>
Email : **{{ $user->email }}**<br/>

Please click the button below to access your account

@component('mail::button', ['url' => route('login')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
