@php
    $marketplace = $data['marketplace'];
    $store = $data['store'];
    $owner = $data['owner'];
@endphp

@component('mail::message')
# Store disapproved

Your store is disapproved and currently inactive.
We need to review your store before approved.

**Store Detail**<br/>
Name : **{{ $store->name }}**<br/>
Owner : **{{ $owner->name }}**<br/>
Status : Inactive<br/>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
