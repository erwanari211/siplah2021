@php
    $store = $data['store'];
    $order = $data['order'];
    $paymentAddress = $data['paymentAddress'];
    $shippingAddress = $data['shippingAddress'];
    $paymentMethod = $data['paymentMethod'];
    $orderMeta = $data['orderMeta'];
    $cartContent = $data['cartContent'];
    $provinces = $data['provinces'];
    $cities = $data['cities'];
@endphp

@component('mail::message')
# Order

Order Number : {{ $order->invoice_no }}

Thank you for ordering product from **{{ $store->name }}**.
Your order has been created and is being processed.
See below for details.

@component('mail::panel')
**Billing Information** <br/>
Name : {{ $paymentAddress->full_name }} <br/>
Phone : {{ $paymentAddress->phone }} <br/>
Address : {{ $paymentAddress->address }} <br/>
Post code : {{ $paymentAddress->postcode }} <br/>
District : {{ ucwords(strtolower($paymentAddress->district->name)) }} <br/>
City : {{ ucwords(strtolower($paymentAddress->city->name)) }} <br/>
Province : {{ ucwords(strtolower($paymentAddress->province->name)) }} <br/>
@endcomponent

@component('mail::panel')
**Shipping Information** <br/>
Name : {{ $shippingAddress->full_name }} <br/>
Phone : {{ $shippingAddress->phone }} <br/>
Address : {{ $shippingAddress->address }} <br/>
Post code : {{ $shippingAddress->postcode }} <br/>
District : {{ ucwords(strtolower($shippingAddress->district->name)) }} <br/>
City : {{ ucwords(strtolower($shippingAddress->city->name)) }} <br/>
Province : {{ ucwords(strtolower($shippingAddress->province->name)) }} <br/>
@endcomponent

@component('mail::table')
| Image        | Product      | Qty  | Price   | Subtotal |
| :----------: |:-----------: | :---:| ------: | -------: |
    @foreach ($cartContent as $item)
        @php
            $image = '<img src="'.asset($item->model->image).'" style="width: 50px; margin-bottom: 5px;">';
            $productName = '<span style="display: inline-block; padding-left:5px; width: 100px;">'.$item->name.'</span>';
            $itemQty = $item->qty;
            $itemPrice = formatNumber($item->price);
            $itemTotal = formatNumber($item->total);
        @endphp
    | {!! $image !!} | {!! $productName !!} | {{$itemQty}} | {{$itemPrice}} | {{$itemTotal}} |
    @endforeach
@endcomponent

@component('mail::table')
|               |                                                 |
| ------------  |-----------------------------------------------: |
| Products      | {{ formatNumber($orderMeta['subtotal']) }}      |
| Shipping cost | {{ formatNumber($orderMeta['shipping']) }}      |
| Unique number | {{ formatNumber($orderMeta['unique_number']) }} |
| **Total**     | **{{ formatNumber($orderMeta['total']) }}**     |
@endcomponent

@component('mail::panel')
**Payment Info** <br/>
{!! $paymentMethod['info'] !!}
@endcomponent

To view your order click button below
@component('mail::button', ['url' => route('users.orders.show', $order->id)])
View order detail
@endcomponent

Thanks again for shopping and we hope to see you again soon.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
