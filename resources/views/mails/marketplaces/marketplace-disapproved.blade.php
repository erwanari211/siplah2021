@php
    $marketplace = $data['marketplace'];
    $owner = $data['marketplaceOwner'];
@endphp

@component('mail::message')
# Marketplace disapproved

Your marketplace is disapproved and currently inactive.
We need to review your marketplace before approved.

**Marketplace Detail**<br/>
Name : **{{ $marketplace->name }}**<br/>
Owner : **{{ $owner->name }}**<br/>
Status : Inactive<br/>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
