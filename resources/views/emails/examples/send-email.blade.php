@component('mail::message')
# {{ $data['title'] }}

Hi, {{ $data['name'] }} ({{ $data['email'] }}) <br>
{{ $data['message'] }}


@component('mail::button', ['url' => ''])
Click here
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
