<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('assets/adminlte-2.4.5') }}/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets/adminlte-2.4.5') }}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

@yield('js')

<!-- Slimscroll -->
<script src="{{ asset('assets/adminlte-2.4.5') }}/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="{{ asset('assets/adminlte-2.4.5') }}/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/adminlte-2.4.5') }}/dist/js/adminlte.min.js"></script>
