@php
  /*
    # example :
    $menu = [
      'name'     => 'menu_name',
      'label'    => 'Menu',
      'icon'     => 'fa fa-bars',
      'url'      => '/home',
      'treeview' => true,
      'submenu'  => [
        [
          'name'  => 'submenu_name',
          'label' => 'Submenu',
          'icon'  => 'fa fa-bars',
          'url'   => '/submenu',
        ]
      ],
    ]
   */
  $activeMenu = ['menu_c', 'menu_c1'];
  $sidebarMenu = [
    [
      'name' => 'menu_a', 'label' => 'Menu A', 'icon' => 'fa fa-home', 'url' => '/menu-a', 'treeview' => false,
    ],
    [
      'name' => 'menu_b', 'label' => 'Menu B',
      'submenu' => [
        ['name' => 'menu_b1', 'label' => 'Menu B1', 'url' => '/menu-b1', ],
      ],
    ],
    [
      'name' => 'menu_c', 'label' => 'Menu C',
      'submenu' => [
        ['name' => 'menu_c1', 'label' => 'Menu C1', 'url' => '/menu-c1', ],
        ['name' => 'menu_c2', 'label' => 'Menu C2', 'url' => '/menu-c2', ],
      ]
    ],
  ];
@endphp

<aside class="main-sidebar">
  <section class="sidebar">
    {!! adminlte_get_sidebar_menu($sidebarMenu, $activeMenu, $title = 'MAIN NAVIGATION') !!}
  </section>
</aside>
