@extends('examples.themes.adminlte.main')

@section('title', 'Admin Dashboard')

@section('css')
@endsection

@section('js')
@endsection

@section('title', 'Dashboard')

@section('content-header')
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
@endsection

@section('content')
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-6">

     <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove">
              <i class="fa fa-times"></i>
            </button>
          </div>
        </div>

        <div class="box-body">

        </div>

        <div class="box-footer clearfix">
          <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Left Button</a>
          <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">Right Button</a>
        </div>
      </div>

      {!! adminlte_box_open('Title', 'primary') !!}

        <div class="box-body">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores in magnam voluptates accusantium aliquam assumenda repellendus id, repellat dignissimos quibusdam, corrupti aspernatur eius natus nostrum vel sint sunt quae? Harum quisquam eligendi nobis recusandae et modi, sunt reiciendis perspiciatis mollitia nam sint blanditiis omnis nulla, ratione libero cupiditate quas dolorem.
        </div>

        <div class="box-footer clearfix">
          <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Left Button</a>
          <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">Right Button</a>
        </div>
      {!! adminlte_box_close() !!}

    </section>
    <!-- /.Left col -->

  </div>
  <!-- /.row (main row) -->
@endsection
