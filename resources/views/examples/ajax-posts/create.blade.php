@extends('examples.themes.adminlte.main')

@section('title', 'Create Post')

@section('css')
@endsection

@section('js')
  <script>
    $(document).ready(function() {
      $('#main-form-messages')
        .hide()
        .on('click', function(event) {
          event.preventDefault();
          $(this).hide();
        });

      $('#main-form').on('submit', function(event) {
        event.preventDefault();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        console.log('submitted');
        var formData = {
            title: $('#title').val(),
            slug: $('#slug').val(),
            content: $('#content').val(),
            excerpt: $('#excerpt').val(),
        }
        $.ajax({
          url: '{{ route('examples.ajax-posts.store') }}',
          type: 'POST',
          dataType: 'json',
          data: formData,
        })
        .done(function(data) {
          console.log("success");
          console.log(data);

          var message = data.message;

          $('#main-form-messages')
            .addClass('alert-success')
            .removeClass('alert-danger')
            .hide()
            .html(message)
            .fadeIn();
        })
        .fail(function(data) {
          var res = data.responseJSON;
          var errors = res.errors;
          var output = '';

          for (var i in errors) {
            var error = errors[i][0];
            output += error + '<br>';
          }

          $('#main-form-messages')
            .addClass('alert-danger')
            .removeClass('alert-success')
            .hide()
            .html(output)
            .fadeIn();
        })
        .always(function() {
          console.log("complete");
        });
      });

    });
  </script>
@endsection

@section('content-header')
  <h1>
    Create Post (AJAX)
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Posts</a></li>
    <li class="active">Create</li>
  </ol>
@endsection

@section('content')
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    {!! Form::open(['route' => ['examples.posts.store'], 'method' => 'POST', 'files' => false, 'id'=>'main-form']) !!}
      <section class="col-lg-8">

        {!! adminlte_box_open('Create Post') !!}

          <div class="box-body">

            @include('flash::message')
            @include('examples.todos.messages')
            <div class="alert" id="main-form-messages">
            </div>

            {!! Form::bs3Text('title') !!}
            {!! Form::bs3Text('slug') !!}
            {!! Form::bs3Textarea('content', null, ['rows'=>10]) !!}
            {!! Form::bs3Textarea('excerpt') !!}


          </div>

          <div class="box-footer clearfix">
            {!! Form::bs3Submit('Save', ['class'=>'btn btn-primary btn-flat']); !!}
            {!! Form::bs3LinkToRoute('examples.posts.index', 'Back', [], ['class'=>'btn btn-default btn-flat']); !!}
          </div>

        {!! adminlte_box_close() !!}

      </section>
    {!! Form::close() !!}
    <!-- /.Left col -->

  </div>
  <!-- /.row (main row) -->
@endsection
