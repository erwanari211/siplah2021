@extends('examples.forms.layout')

@section('title', 'Create Form')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                @include('flash::message')

                {!! Form::open([
                    'action' => ['Examples\FileUploadController@store'],
                    'method' => 'POST',
                    'files' => true,
                    ]) !!}

                    {!! Form::bs3File('file') !!}

                    {!! Form::bs3Submit('Upload'); !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

