@php
    // themes : ['first-theme', 'second-theme', 'third-theme']
    $_currentTheme = request('theme') ? request('theme') : 'second-theme';
@endphp

@extends('examples.blade.themes.main')

@section('content')
    <h1 class="text-center">Blade Theme</h1>
    <h3 class="text-center">Now using {{ $_currentTheme }}</h3>
@endsection
