@extends('examples.todos.layout')

@section('title', 'Todos')

@section('content')
    <style>
        .mb-10 {
            margin-bottom: 10px;
            display: block;
        }
    </style>
    <h3>{{ __('example_todo.todos') }}</h3>

    @include('flash::message')

    @foreach ($todos as $todo)
        <div class="panel panel-default">
            <div class="panel-body">
                <strong class="mb-10">
                    <a href="{{ route('examples.todos.show', [$todo->id]) }}">
                            {{ $todo->name }}
                    </a>
                </strong>
                <div class="mb-10">
                    {!! Form::bs3LinkToRoute('examples.todos.edit', __('example_todo.menu.edit'), [$todo->id], ['class'=>'btn btn-xs btn-info']); !!}
                    {!! Form::open([
                        'route' => ['examples.todos.destroy', $todo->id],
                        'method' => 'DELETE',
                        'style' => 'display: inline-block;'
                        ]) !!}
                        {!! Form::bs3Submit( __('example_todo.menu.delete'), ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                    {!! Form::close() !!}
                    {!! $todo->completed_label !!}
                </div>
                <span class="mb-10">
                    {{ $todo->description }}
                </span>
            </div>
        </div>
    @endforeach

    {{ $todos->appends(request()->only(['status']))->links() }}

@endsection
