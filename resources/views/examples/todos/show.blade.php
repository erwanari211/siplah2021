@extends('examples.todos.layout')

@section('title', 'Todos')

@section('content')

    <div class="panel panel-default">
        <div class="panel-body">
            <p>
                <a href="{{ route('examples.todos.show', [$todo->id]) }}">
                    <strong>{{ $todo->name }}</strong>
                </a>
            </p>
            <p>{{ $todo->description }}</p>
        </div>
    </div>

@endsection
