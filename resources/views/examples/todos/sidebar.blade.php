<div class="list-group">
    <a href="{{ route('examples.todos.index') }}" class="list-group-item">
        {{ __('example_todo.menu.all') }}
    </a>
    <a href="{{ route('examples.todos.index', ['status'=>'completed']) }}" class="list-group-item">
        {{ __('example_todo.menu.completed') }}
    </a>
    <a href="{{ route('examples.todos.index', ['status'=>'incomplete']) }}" class="list-group-item">
        {{ __('example_todo.menu.incomplete') }}
    </a>
    <a href="{{ route('examples.todos.create') }}" class="list-group-item">
        {{ __('example_todo.menu.create') }}
    </a>
</div>
