@extends('examples.themes.adminlte.main')

@section('title', 'Posts')

@section('css')
@endsection

@section('js')
  <script>
    $(document).ready(function() {
      $('.pagination').addClass('pagination-sm no-margin pull-left');
    });
  </script>
@endsection

@section('content-header')
  <h1>
    Posts
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Posts</li>
  </ol>
@endsection

@section('content')
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12">

      @include('flash::message')
      @include('examples.todos.messages')

      {!! adminlte_box_open('Posts') !!}

        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Options</th>
                  <th>Title</th>
                  <th>Status</th>
                  <th>Date Published</th>
                </tr>
              </thead>
              <tbody>
                @if (count($posts))
                  @php
                    $no = $posts->firstItem();
                  @endphp
                  @foreach ($posts as $post)
                    <tr>
                      <td>{{ $no }}</td>
                      <td>
                        {!! Form::open([
                          'route' => ['examples.trashed-posts.destroy', $post->id],
                          'method' => 'DELETE',
                          'style' => 'display: inline-block;'
                          ]) !!}
                          {!! Form::bs3SubmitHtml('<i class="fa fa-refresh"></i>', ['class'=>'btn btn-xs btn-info btn-flat', 'onclick'=>'return confirm("Restore?")']); !!}
                        {!! Form::close() !!}

                        {!! Form::open([
                          'route' => ['examples.posts.destroy', $post->id],
                          'method' => 'DELETE',
                          'style' => 'display: inline-block;'
                          ]) !!}
                          {!! Form::bs3SubmitHtml('<i class="fa fa-remove"></i>', ['class'=>'btn btn-xs btn-danger btn-flat', 'onclick'=>'return confirm("Permanent Delete?")']); !!}
                        {!! Form::close() !!}
                      </td>
                      <td>{{ $post->title }}</td>
                      <td>{{ $post->post_status }}</td>
                      <td>{{ $post->published_at }}</td>
                    </tr>
                    @php
                      $no++;
                    @endphp
                  @endforeach
                @else
                  <tr>
                    <td>No data</td>
                  </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>

        <div class="box-footer clearfix">
          {{ $posts->links() }}
          <a href="{{ route('examples.posts.index') }}" class="btn btn-sm btn-default btn-flat pull-right">All Posts</a>
          <a href="{{ route('examples.posts.create') }}" class="btn btn-sm btn-primary btn-flat pull-right">Add new</a>
        </div>

      {!! adminlte_box_close() !!}

    </section>
    <!-- /.Left col -->

  </div>
  <!-- /.row (main row) -->
@endsection
