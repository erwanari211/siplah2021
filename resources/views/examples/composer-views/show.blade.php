@extends('examples.composer-views.layout')

@section('title', 'Show')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <h3>Content</h3>
                <p><strong>{{ $post }}</strong></p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam ipsam, cum sit adipisci qui, vel doloremque, obcaecati nulla, repellendus dignissimos debitis vero dolore omnis tenetur sapiente minus eos deserunt magni error saepe mollitia incidunt? Excepturi a quae expedita, fugit neque dignissimos modi quam aperiam, quas nulla debitis incidunt. Molestiae dicta porro unde, facere est, debitis, magnam illo mollitia illum saepe consectetur maxime obcaecati ducimus minima aperiam ipsam sunt quasi! Et, non vel molestias iusto atque ipsam fugit! Assumenda ipsum eligendi fugit, error rem ipsa temporibus et repellendus praesentium, voluptate ex necessitatibus, commodi eius. Dicta illo voluptates, ducimus vel placeat, quidem iure consequuntur atque sapiente officiis nobis ipsum minima delectus nostrum culpa doloremque non! Quos a ex sint aut inventore, repellendus eos modi dolor, labore maiores velit. Harum molestias itaque obcaecati similique, ea! Necessitatibus eligendi aliquid, placeat temporibus deleniti! Nesciunt assumenda sint necessitatibus molestias impedit vel totam pariatur harum nostrum, dolorum nobis, odit recusandae distinctio tenetur fugit dolorem. Rerum unde harum voluptatum voluptatem doloremque laudantium aperiam eligendi ratione voluptatibus fugit expedita nesciunt iure sequi, quis dolorum voluptates facere magni sapiente quas porro saepe iusto perspiciatis maxime vitae. Odio aspernatur, eligendi atque nostrum impedit nam ex sit porro architecto possimus repudiandae placeat.
                </p>
            </div>
            <div class="col-sm-4">
                @include('examples.composer-views.sidebar')
            </div>

        </div>
    </div>
@endsection
