@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'User Addresses')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.accounts.profile.index') }}">Home</a>
                        </li>
                        <li class="active">
                            <a href="{{ route('users.accounts.addresses.index') }}">Addresses</a>
                        </li>
                    </ol>

                    <h3>Addresses</h3>

                    <a class="btn btn-primary" href="{{ route('users.accounts.addresses.create') }}">Add address</a> <br><br>

                    @include('flash::message')

                    @if (!$hasDefaultStoreAddress)
                        <div class="alert alert-danger">
                            You dont have default address. Please create / select a default address.
                        </div>
                    @endif

                    @foreach ($addresses as $address)
                        <div class="box">
                            <div class="box-content">
                                <p><strong>{{ $address->label }}</strong></p>
                                <span>{{ $address->full_name }}</span> <br>
                                @if ($address->company)
                                    <span>{{ $address->company }}</span> <br>
                                @endif
                                <span>{{ $address->phone }}</span> <br>
                                <span>{{ $address->address }}</span> <br>
                                <span>{{ $address->postcode }}</span> <br>
                                {{--
                                <span>{{ $address->city->type.' '.$address->city->city_name }}</span> <br>
                                <span>{{ $address->province->province }}</span> <br>
                                --}}
                                @php
                                    $fullAddress = [];
                                    if ($address->district) {
                                        $fullAddress[] = $address->district->name;
                                    }
                                    if ($address->city) {
                                        $fullAddress[] = $address->city->name;
                                    }
                                    if ($address->province) {
                                        $fullAddress[] = $address->province->name;
                                    }
                                @endphp
                                @if ($fullAddress)
                                    <span>
                                        {{ implode(', ', $fullAddress) }}
                                    </span>
                                    <br>
                                @endif
                                <br>
                                <a class="btn btn-success btn-xs" href="{{ route('users.accounts.addresses.edit', $address->id) }}">Edit</a>
                                {!! Form::open([
                                        'route' => ['users.accounts.addresses.destroy', $address->id],
                                        'method' => 'DELETE',
                                        'style' => 'display: inline-block;'
                                    ]) !!}
                                    {!! Form::bs3Submit('Delete', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                {!! Form::close() !!}
                                @if ($address->default_address)
                                    <span class="btn btn-xs btn-success">
                                        <i class="fa fa-check"></i>
                                        Default Address
                                    </span>
                                @endif
                            </div>
                        </div>
                    @endforeach


                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-account')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
