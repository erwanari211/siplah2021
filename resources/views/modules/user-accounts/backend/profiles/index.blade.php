@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Profile')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.accounts.profile.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Profile</a>
                        </li>
                        <li class="active">Edit</li>
                    </ol>

                    <h3>User Account</h3>

                    @php
                        $isSiplahUser = $user->hasMeta('siplah_profile');
                    @endphp
                    <div class="box">
                        <div class="box-content">
                            @include('flash::message')

                            {!! Form::model($user, [
                                'route' => ['users.accounts.profile.update'],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('name') !!}
                                {!! Form::bs3Text('username') !!}
                                <div class="{{ $isSiplahUser ? 'hide' : '' }}">
                                    {!! Form::bs3Email('email') !!}
                                </div>
                                {!! Form::bs3File('image') !!}
                                <span class="help-block">Recomended size 300 x 300</span>
                                <div class="form-group ">
                                    <img class="img-thumbnail" src="{{ asset($user->photo) }}" width="100" height="100">
                                </div>

                                {!! Form::bs3Submit('Update'); !!}

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    @php
                        // $userStores = $user->stores;
                        $userHasStore = count($userStores) ? true : false;
                    @endphp
                    @if ($userHasStore)
                        <div id="user-stores-container">
                            <h3><i class="fa fa-fw fa-store"></i> My Stores</h3>
                            <div class="row">
                                @foreach ($userStores as $store)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="box">
                                            <div class="box-content">
                                                <div class="icon-box">
                                                    <a class="icon-box-link" href="{{ route('users.stores.index', $store->id) }}">
                                                        <div class="icon-box-icon-container">
                                                            <img class="icon-box-image lazy loaded" data-src="{{ asset($store->image) }}">
                                                        </div>
                                                        <div class="icon-box-content">
                                                            <span class="icon-box-title line-clamp-2">{{ $store->name }}</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @php
                        // $userMarketplace = $user->marketplaces;
                        $userHasMarketplace = count($userMarketplaces) ? true : false;
                    @endphp
                    @if ($userHasMarketplace)
                        <div id="user-marketplaces-container">
                            <h3><i class="fa fa-fw fa-store"></i> My Marketplace</h3>
                            <div class="row">
                                @foreach ($userMarketplaces as $marketplace)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="box">
                                            <div class="box-content">
                                                <div class="icon-box">
                                                    <a class="icon-box-link" href="{{ route('users.marketplaces.index', $marketplace->id) }}">
                                                        <div class="icon-box-icon-container">
                                                            <img class="icon-box-image lazy loaded" data-src="{{ asset($marketplace->image) }}">
                                                        </div>
                                                        <div class="icon-box-content">
                                                            <span class="icon-box-title line-clamp-2">{{ $marketplace->name }}</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @php
                        $userHasSupervisorGroups = count($userSupervisorGroups) ? true : false;
                    @endphp
                    @if ($userHasSupervisorGroups)
                        <div id="user-marketplaces-container">
                            <h3><i class="fa fa-fw fa-user-tie"></i> My Supervisor Group</h3>
                            <div class="row">
                                @foreach ($userSupervisorGroups as $supervisorGroup)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="box">
                                            <div class="box-content">
                                                <div class="icon-box">
                                                    <a class="icon-box-link" href="{{ route('users.supervisor-groups.home', $supervisorGroup->id) }}">
                                                        <div class="icon-box-icon-container">
                                                            <img class="icon-box-image lazy loaded" data-src="{{ asset($supervisorGroup->image) }}">
                                                        </div>
                                                        <div class="icon-box-content">
                                                            <span class="icon-box-title line-clamp-2">{{ $supervisorGroup->name }}</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @php
                        $userHasSchools = count($userSchools) ? true : false;
                    @endphp
                    @if ($userHasSchools)
                        @php
                            $schoolId = null;
                            $loginAs = session('login_as');
                            if ($loginAs == 'school_staff') {
                                $schoolId = session('school_id');
                            }
                        @endphp
                        @if ($schoolId)
                            <div id="user-marketplaces-container">
                                <h3><i class="fa fa-fw fa-school"></i> My School</h3>
                                <div class="row">
                                    @foreach ($userSchools as $school)
                                        @if ($school->id == $schoolId)
                                            <div class="col-md-4 col-sm-6">
                                                <div class="box">
                                                    <div class="box-content">
                                                        <div class="icon-box">
                                                            <a class="icon-box-link" href="{{ route('users.schools.home', $school->id) }}">
                                                                <div class="icon-box-icon-container">
                                                                    <img class="icon-box-image lazy loaded" data-src="{{ asset($school->image) }}">
                                                                </div>
                                                                <div class="icon-box-content">
                                                                    <span class="icon-box-title line-clamp-2">{{ $school->name }}</span>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    @endif

                    @php
                        $userIsAdministrator = $user->hasRole(['administrator', 'superadministrator']);
                    @endphp
                    @if ($userIsAdministrator)
                        <h3><i class="fa fa-fw fa-user"></i> Adminsitrator</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="box">
                                    <div class="box-content">
                                        <div class="icon-box">
                                            <a class="icon-box-link" href="{{ route('administrator.index') }}">
                                                <div class="icon-box-icon-container">
                                                    <img class="icon-box-image lazy loaded" data-src="{{ asset('images/administrator.png') }}">
                                                </div>
                                                <div class="icon-box-content">
                                                    <span class="icon-box-title line-clamp-2">Adminsitrator</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-account')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
