@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Password')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.accounts.profile.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Password</a>
                        </li>
                        <li class="active">Edit</li>
                    </ol>

                    <h3>Password</h3>

                    <div class="box">
                        <div class="box-content">
                            @include('flash::message')

                            {!! Form::model($user, [
                                'route' => ['users.accounts.password.update'],
                                'method' => 'PUT',
                                'files' => false,
                                ]) !!}

                                {!! Form::bs3Password('old_password') !!}
                                {!! Form::bs3Password('password') !!}
                                {!! Form::bs3Password('password_confirmation') !!}

                                {!! Form::bs3Submit('Update'); !!}

                            {!! Form::close() !!}
                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-account')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
