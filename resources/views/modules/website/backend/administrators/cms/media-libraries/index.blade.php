@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Media Libraries')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script type="text/javascript" src="{{ asset('assets/plugins') }}/clipboard.js/dist/clipboard.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.gallery-copy-url').tooltip({
                trigger: 'click',
                placement: 'bottom'
            });

            var clipboardGalleryUrl = new ClipboardJS('.gallery-copy-url');

            clipboardGalleryUrl.on('success', function(e) {
                var target = e.trigger;
                setTooltip(target, 'Copied!');
                hideTooltip(target);
            });

            function setTooltip(target, message) {
                $(target).tooltip('hide')
                    .attr('data-original-title', message)
                    .tooltip('show');
            }

            function hideTooltip(target) {
                setTimeout(function() {
                    $(target).tooltip('destroy');
                }, 1000);
            }
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.administrators.backend.inc.navbar-top-administrator')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('administrator.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Website</a>
                        </li>
                        <li>
                            <a href="#">CMS</a>
                        </li>
                        <li class="active">
                            <a href="{{ route('administrator.website.cms.media-libraries.index') }}">Media Libraries</a>
                        </li>
                    </ol>

                    <h3>Media Libraries</h3>

                    @include('flash::message')
                    @include('themes.marika-natsuki.messages')

                    <a class="btn btn-primary"
                        href="{{ route('administrator.website.cms.media-libraries.create') }}">
                        Add Media
                    </a>

                    <div class="box mt-3">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th></th>
                                            <th>Group</th>
                                            <th>Type</th>
                                            <th>Name</th>
                                            <th>Tags</th>
                                            <th>Active</th>
                                            <th>Is Public</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($medias))
                                            @php
                                                $no = $medias->firstItem();
                                            @endphp
                                            @foreach ($medias as $media)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-success"
                                                            href="{{ route('administrator.website.cms.media-libraries.edit', [$media->id]) }}" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['administrator.website.cms.media-libraries.destroy', $media->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                        <button class="btn btn-xs btn-default gallery-copy-url"
                                                            data-clipboard-text="{{ asset($media->url) }}">
                                                            <i class="fa fa-copy"></i>
                                                            Copy url
                                                        </button>
                                                    </td>
                                                    <td>
                                                        @if ($media->fileType == 'image')
                                                            <img src="{{ asset($media->url) }}" height="50">
                                                        @else
                                                            <a class="btn btn-xs btn-default" href="{{ asset($media->url) }}">Download</a>
                                                        @endif
                                                    </td>
                                                    <td>{{ $media->group }}</td>
                                                    <td>{{ $media->fileType }}</td>
                                                    <td>{{ $media->name }}</td>
                                                    <td>
                                                        @php
                                                            $tags_string = trim($media->tags);
                                                            $tags = array_filter(explode(',', $tags_string));
                                                        @endphp
                                                        @if (count($tags))
                                                            @foreach ($tags as $tag)
                                                                <span class="btn btn-xs btn-default">
                                                                    {{ trim($tag) }}
                                                                </span>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td>{{ $media->active ? 'Yes' : 'No' }}</td>
                                                    <td>{{ $media->is_public ? 'Yes' : 'No' }}</td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="9">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $medias->links() }}
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>

    @include('modules.administrators.backend.inc.sidebar-administrator')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
