@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Banners')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.administrators.backend.inc.navbar-top-administrator')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('administrator.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Website</a>
                        </li>
                        <li>
                            <a href="#">Blog</a>
                        </li>
                        <li class="active">
                            <a href="{{ route('administrator.website.cms.blog-banners.index') }}">Banners</a>
                        </li>
                    </ol>

                    <h3>Banners</h3>

                    @include('flash::message')
                    @include('themes.marika-natsuki.messages')

                    <a class="btn btn-primary"
                        href="{{ route('administrator.website.cms.blog-banners.create') }}">
                        Add Banner
                    </a>

                    <div class="box mt-3">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Image</th>
                                            <th>Link</th>
                                            <th>Active</th>
                                            <th>Position</th>
                                            <th>Note</th>
                                            <th>Sort Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($banners))
                                            @php
                                                $no = $banners->firstItem();
                                            @endphp
                                            @foreach ($banners as $banner)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-success"
                                                            href="{{ route('administrator.website.cms.blog-banners.edit', [$banner->id]) }}" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['administrator.website.cms.blog-banners.destroy', $banner->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td>
                                                        <img class="lazy" src="{{ asset($banner->image) }}" width="150" height="50">
                                                    </td>
                                                    <td>
                                                        @if ($banner->link)
                                                            <a href="{{ $banner->link }}">Yes</a>
                                                        @else
                                                            No
                                                        @endif
                                                    </td>
                                                    <td>{{ $banner->active ? 'Yes' : 'No' }}</td>
                                                    <td>{{ $banner->position }}</td>
                                                    <td>{!! $banner->note !!}</td>
                                                    <td>{{ $banner->sort_order }}</td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $banners->links() }}
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>

    @include('modules.administrators.backend.inc.sidebar-administrator')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
