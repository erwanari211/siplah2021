@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Detail Post')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@endpush

@push('js')
    <script src="{{ asset('assets/adminlte-2.4.5/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins') }}/tinymce_4.7.9/tinymce/js/tinymce/tinymce.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.datetimepicker').datetimepicker();

            /**
             * Tinymce settings
             */
            var plugins = 'preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern code help spellchecker';
            var toolbar1 = 'formatselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat';
            var toolbar2 = 'undo redo | link unlink image media styleselect | nanospell | fullscreen preview code ';

            tinymce.init({
                selector: '#content',
                height: 300,
                plugins: plugins,
                toolbar1: toolbar1,
                toolbar2: toolbar2,
                image_advtab: true,
                selection_toolbar: 'bold italic | quicklink h2 h3 blockquote',
                imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",
                content_css: [
                  '//www.tinymce.com/css/codepen.min.css',
                  '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'
                ],

                relative_urls : false,
                remove_script_host : false,
                convert_urls : true,
                image_class_list: [
                    {title: 'Lazyload', value: 'lazy'},
                    {title: 'None', value: ''},
                ],

                table_default_attributes: {
                    'class': 'table table-bordered'
                },
                table_default_styles: {},
                table_class_list: [
                    {title: 'None', value: ''},
                    {title: 'Table Bordered', value: 'table table-bordered'},
                ],

                menubar:true,
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-product-id]').on('click', function(event) {
                event.preventDefault();
                var productId = $(this).attr('data-product-id');
                var sortOrder = $(this).attr('data-sort-order');
                $('#product_id').val(productId);
                $('#modal-add-to-product-collection').find('#sort_order').val(sortOrder);
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('administrator.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Website</a>
                        </li>
                        <li>
                            <a href="#">Blog</a>
                        </li>
                        <li>
                            <a href="{{ route('administrator.website.cms.blog-posts.index') }}">Posts</a>
                        </li>
                        <li class="active">Detail</li>
                    </ol>

                    <h3>Detail Post</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($post, [
                                'route' => ['administrator.website.cms.blog-posts.update', $post->id],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('title') !!}
                                {!! Form::bs3Text('slug') !!}
                                {!! Form::bs3Textarea('content') !!}
                                {!! Form::bs3Textarea('excerpt') !!}

                                <hr>

                                {!! Form::bs3Select('category', $dropdown['categories'], $postCategories) !!}

                                {!! Form::bs3Select('status', $dropdown['statuses']) !!}
                                {!! Form::bs3Datetimepicker('published_at') !!}

                                <hr>

                                {!! Form::bs3Text('seo_title', $postMeta['seo_title']) !!}
                                {!! Form::bs3Textarea('seo_description', $postMeta['seo_description']) !!}
                                {!! Form::bs3Text('seo_keywords', $postMeta['seo_keywords']) !!}

                                <hr>

                                {!! Form::bs3File('featured_image') !!}
                                @if (isset($postMeta['featured_image']))
                                    <div class="form-group ">
                                        <img class="img-thumbnail" src="{{ asset($postMeta['featured_image']) }}"
                                            style="max-height: 150px; max-width: 150px;">
                                    </div>
                                @endif
                                <span class="help-block">Recomended size 1200 x 400</span>

                                <a class="btn btn-default" href="{{ route('administrator.website.cms.blog-posts.index') }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                    <h3>Related Products</h3>

                    <div class="mb-4">
                        <a class="btn btn-primary"
                            href="{{ route('administrator.website.cms.blog-post-products.index', ['post_id' =>$post->id]) }}">
                            Add Product
                        </a>
                    </div>

                    <div class="box">
                        <div class="box-content">
                            @if (count($products))
                                <table class="table table-hover table-bordered mb-0">
                                    <thead>
                                        <tr>
                                            <th>Options</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Store</th>
                                            <th>Marketplace</th>
                                            <th>Sort Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($products as $item)
                                            @php
                                                $product = $item->product;
                                            @endphp
                                            <tr>
                                                <td>
                                                    <a class="btn btn-xs btn-success"
                                                        data-toggle="modal"
                                                        data-product-id="{{ $product->id }}"
                                                        data-sort-order="{{ $item->sort_order }}"
                                                        href="#modal-add-to-product-collection" title="Add to collection">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    {!! Form::open([
                                                            'route' => ['administrator.website.cms.blog-post-products.destroy', $item->id],
                                                            'method' => 'DELETE',
                                                            'style' => 'display: inline-block;'
                                                        ]) !!}
                                                        {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                    {!! Form::close() !!}
                                                </td>
                                                <td>
                                                    <img class="lazy" src="{{ asset($product->image) }}" height="50">
                                                </td>
                                                <td>{{ $product->name }}</td>
                                                <td>{{ $product->store->name }}</td>
                                                <td>{{ $product->store->marketplace->name }}</td>
                                                <td>{{ $item->sort_order }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                No related product
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-add-to-product-collection">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Update to Related Products</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open([
                        'route' => ['administrator.website.cms.blog-post-products.store'],
                        'method' => 'POST',
                        'files' => true,
                        ]) !!}

                        <div class="hide">
                            {!! Form::bs3Text('product_id') !!}
                            {!! Form::bs3Text('post_id', $post->id) !!}
                        </div>
                        {!! Form::bs3Number('sort_order') !!}

                        {!! Form::bs3Submit('Save'); !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    @include('modules.administrators.backend.inc.sidebar-administrator')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
