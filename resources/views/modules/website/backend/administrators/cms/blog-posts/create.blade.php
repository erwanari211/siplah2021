@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Create Post')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@endpush

@push('js')
    <script src="{{ asset('assets/adminlte-2.4.5/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins') }}/tinymce_4.7.9/tinymce/js/tinymce/tinymce.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.datetimepicker').datetimepicker();

            /**
             * Tinymce settings
             */
            var plugins = 'preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern code help spellchecker';
            var toolbar1 = 'formatselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat';
            var toolbar2 = 'undo redo | link unlink image media styleselect | nanospell | fullscreen preview code ';

            tinymce.init({
                selector: '#content',
                height: 300,
                plugins: plugins,
                toolbar1: toolbar1,
                toolbar2: toolbar2,
                image_advtab: true,
                selection_toolbar: 'bold italic | quicklink h2 h3 blockquote',
                imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",
                content_css: [
                  '//www.tinymce.com/css/codepen.min.css',
                  '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'
                ],

                relative_urls : false,
                remove_script_host : false,
                convert_urls : true,
                image_class_list: [
                    {title: 'Lazyload', value: 'lazy'},
                    {title: 'None', value: ''},
                ],

                table_default_attributes: {
                    'class': 'table table-bordered'
                },
                table_default_styles: {},
                table_class_list: [
                    {title: 'None', value: ''},
                    {title: 'Table Bordered', value: 'table table-bordered'},
                ],

                menubar:true,
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('administrator.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Website</a>
                        </li>
                        <li>
                            <a href="#">Blog</a>
                        </li>
                        <li>
                            <a href="{{ route('administrator.website.cms.blog-posts.index') }}">Posts</a>
                        </li>
                        <li class="active">Create</li>
                    </ol>

                    <h3>Create Post</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['administrator.website.cms.blog-posts.store'],
                                'method' => 'POST',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('title') !!}
                                {!! Form::bs3Text('slug') !!}
                                {!! Form::bs3Textarea('content') !!}
                                {!! Form::bs3Textarea('excerpt') !!}

                                <hr>

                                {!! Form::bs3Select('category', $dropdown['categories']) !!}
                                {!! Form::bs3Select('active', $dropdown['yes_no'], 1) !!}

                                <hr>

                                {!! Form::bs3Text('seo_title') !!}
                                {!! Form::bs3Textarea('seo_description') !!}
                                {!! Form::bs3Text('seo_keywords') !!}

                                <hr>

                                {!! Form::bs3File('featured_image') !!}
                                <span class="help-block">Recomended size 1200 x 400</span>

                                {!! Form::bs3Submit('Save'); !!}
                                <a class="btn btn-default" href="{{ route('administrator.website.cms.blog-posts.index') }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    @include('modules.administrators.backend.inc.sidebar-administrator')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
