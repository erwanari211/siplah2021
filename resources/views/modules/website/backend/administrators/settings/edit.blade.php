@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Settings')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('administrator.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Website</a>
                        </li>
                        <li>
                            <a href="{{ route('administrator.website.settings.edit') }}">Settings</a>
                        </li>
                    </ol>

                    <h3>Settings</h3>

                    @include('flash::message')

                    <div class="mb-4">
                        {!! Form::open([
                                'route' => ['administrator.website.cms.forget-caches'],
                                'method' => 'DELETE',
                                'style' => 'display: inline-block;'
                            ]) !!}
                            {!! Form::bs3SubmitHtml('Clear Caches', ['class'=>'btn btn-danger', 'onclick'=>'return confirm("Clear?")']); !!}
                        {!! Form::close() !!}
                    </div>

                    <div class="box">
                        <div class="box-content">
                            @php
                                $favicon = isset($settings['favicon']) && $settings['favicon'] ? $settings['favicon'] : false;
                            @endphp
                            @php
                                $logo = isset($settings['logo']) && $settings['logo'] ? $settings['logo'] : false;
                            @endphp
                            {!! Form::model($settings, [
                                'route' => ['administrator.website.settings.update'],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3File('favicon') !!}
                                @if ($favicon)
                                    <div class="form-group ">
                                        <img class="img-thumbnail" src="{{ asset($favicon) }}" width="64" height="64">
                                    </div>
                                @endif
                                <span class="help-block">Recommended size 32 x 32. Format PNG</span>

                                {!! Form::bs3File('logo') !!}
                                @if ($logo)
                                    <div class="form-group ">
                                        <img class="img-thumbnail" src="{{ asset($logo) }}" width="100" height="100">
                                    </div>
                                @endif
                                <span class="help-block">Recommended size 600 x 150. Format PNG</span>

                                {!! Form::bs3Text('title') !!}
                                {!! Form::bs3Textarea('seo_description') !!}
                                {!! Form::bs3Text('seo_keywords') !!}

                                <hr>

                                {!! Form::bs3Textarea('google_analytics') !!}

                                <hr>

                                {!! Form::bs3Textarea('chatra_widget') !!}

                                <hr>
                                {!! Form::bs3Text('facebook_url') !!}
                                {!! Form::bs3Text('twitter_url') !!}
                                {!! Form::bs3Text('youtube_url') !!}
                                {!! Form::bs3Text('instagram_url') !!}

                                {!! Form::bs3Submit('Update'); !!}
                                <a class="btn btn-default" href="{{ route('administrator.index') }}">Back</a>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    @include('modules.administrators.backend.inc.sidebar-administrator')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
