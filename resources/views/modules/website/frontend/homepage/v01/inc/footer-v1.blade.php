<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-4">
                @if (isset($isHomepage) && $isHomepage)
                    @if (isset($groupedLayoutElements['footer']))
                        @if (isset($groupedLayoutElements['footer']['footer.col-1']) && count($groupedLayoutElements['footer']['footer.col-1']))
                            @foreach ($groupedLayoutElements['footer']['footer.col-1'] as $item)
                                <div class="mb-4" data-role="layout-element">
                                    {!! $item['content'] !!}
                                </div>
                            @endforeach
                        @endif
                    @endif
                @else
                    @php
                        $aboutUs = '';
                        if (isset($marketplace) && $marketplace) {
                            $defaultdescription = '';
                            $defaultdescription .= '<h3 class="mt-0">About Us</h3>';
                            $defaultdescription .= '<strong>'.$marketplace->name.'</strong> is one of marketplace at Zuala.id. ';
                            $defaultdescription .= 'In this site you can buy and sell various kinds of products. ';
                            $aboutUs .= $defaultdescription;
                        }

                        if (isset($marketplaceSettings)) {
                            if (isset($marketplaceSettings['marketplace_description']) && $marketplaceSettings['marketplace_description']) {
                                $aboutUs = '';
                                $aboutUs .= '<h3 class="mt-0">About Us</h3>';
                                $aboutUs .= $marketplaceSettings['marketplace_description'];
                            }
                        }
                    @endphp

                    @php
                        $marketplaceHasSocialMedia = false;
                        $marketplaceInfos['facebook'] = ['url'=>null, 'name'=>'facebook'];
                        $marketplaceInfos['twitter'] = ['url'=>null, 'name'=>'twitter'];
                        $marketplaceInfos['youtube'] = ['url'=>null, 'name'=>'youtube'];
                        $marketplaceInfos['instagram'] = ['url'=>null, 'name'=>'instagram'];

                        if (isset($marketplaceSettings['facebook_url']) && $marketplaceSettings['facebook_url']) {
                            $marketplaceInfos['facebook']['url'] = $marketplaceSettings['facebook_url'];
                            if (isset($marketplaceSettings['facebook_name']) && $marketplaceSettings['facebook_name']) {
                                $marketplaceInfos['facebook']['name'] = $marketplaceSettings['facebook_name'];
                            }
                            $marketplaceHasSocialMedia = true;
                        }
                        if (isset($marketplaceSettings['twitter_url']) && $marketplaceSettings['twitter_url']) {
                            $marketplaceInfos['twitter']['url'] = $marketplaceSettings['twitter_url'];
                            if (isset($marketplaceSettings['twitter_name']) && $marketplaceSettings['twitter_name']) {
                                $marketplaceInfos['twitter']['name'] = $marketplaceSettings['twitter_name'];
                            }
                            $marketplaceHasSocialMedia = true;
                        }
                        if (isset($marketplaceSettings['youtube_url']) && $marketplaceSettings['youtube_url']) {
                            $marketplaceInfos['youtube']['url'] = $marketplaceSettings['youtube_url'];
                            if (isset($marketplaceSettings['youtube_name']) && $marketplaceSettings['youtube_name']) {
                                $marketplaceInfos['youtube']['name'] = $marketplaceSettings['youtube_name'];
                            }
                            $marketplaceHasSocialMedia = true;
                        }
                        if (isset($marketplaceSettings['instagram_url']) && $marketplaceSettings['instagram_url']) {
                            $marketplaceInfos['instagram']['url'] = $marketplaceSettings['instagram_url'];
                            if (isset($marketplaceSettings['instagram_name']) && $marketplaceSettings['instagram_name']) {
                                $marketplaceInfos['instagram']['name'] = $marketplaceSettings['instagram_name'];
                            }
                            $marketplaceHasSocialMedia = true;
                        }
                    @endphp

                    {!! $aboutUs !!}

                    @if ($marketplaceHasSocialMedia)
                        <h3 class="mt-0">Follow Us</h3>
                    @endif
                    @if ($marketplaceInfos['facebook']['url'])
                        <i class="fab fa-fw fa-facebook"></i>
                        <a href="{{ $marketplaceInfos['facebook']['url'] }}">
                            {{ $marketplaceInfos['facebook']['name'] }}
                        </a>
                        <br>
                    @endif
                    @if ($marketplaceInfos['twitter']['url'])
                        <i class="fab fa-fw fa-twitter"></i>
                        <a href="{{ $marketplaceInfos['twitter']['url'] }}">
                            {{ $marketplaceInfos['twitter']['name'] }}
                        </a>
                        <br>
                    @endif
                    @if ($marketplaceInfos['youtube']['url'])
                        <i class="fab fa-fw fa-youtube"></i>
                        <a href="{{ $marketplaceInfos['youtube']['url'] }}">
                            {{ $marketplaceInfos['youtube']['name'] }}
                        </a>
                        <br>
                    @endif
                    @if ($marketplaceInfos['instagram']['url'])
                        <i class="fab fa-fw fa-instagram"></i>
                        <a href="{{ $marketplaceInfos['instagram']['url'] }}">
                            {{ $marketplaceInfos['instagram']['name'] }}
                        </a>
                        <br>
                    @endif
                    @if (isset($marketplaceSettings['phone']) && $marketplaceSettings['phone'])
                        <i class="fa fa-fw fa-phone"></i> {{ $marketplaceSettings['phone'] }}
                        <br>
                    @endif
                    @if (isset($marketplaceSettings['mobile_phone']) && $marketplaceSettings['mobile_phone'])
                        <i class="fa fa-fw fa-mobile"></i> {{ $marketplaceSettings['mobile_phone'] }}
                        <br>
                    @endif
                @endif
            </div>
            <div class="col-md-3 mb-4">
                <div class="row">
                    <div class="col-xs-6 col-md-12">
                        <h3 class="mt-0">How to Sell</h3>
                        <ul class="list-unstyled">
                            <li> <a href="#">How to Sell</a> </li>
                            <li> <a href="#">Withdrawal</a> </li>
                            <li> <a href="#">Register Official Store</a> </li>
                            <li> <a href="#">Advertise</a> </li>
                            <li> <a href="#">Seller Center</a> </li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-md-12">
                        <h3 class="mt-0">How to Buy</h3>
                        <ul class="list-unstyled">
                            <li> <a href="#">How to Buy</a> </li>
                            <li> <a href="#">Payment</a> </li>
                            <li> <a href="#">Refund</a> </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-3 mb-4">
                <div class="row">
                    <div class="col-xs-6 col-md-12">
                        <h3 class="mt-0">Help</h3>
                        <ul class="list-unstyled">
                            <li> <a href="#">Terms and Conditions</a> </li>
                            <li> <a href="#">Privacy Policies</a> </li>
                            <li> <a href="#">Order Complaint</a> </li>
                            <li> <a href="#">Contact Us</a> </li>
                            <li> <a href="#">Safety Guide</a> </li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-md-12">
                        <h3 class="mt-0">Follow Zuala.id</h3>
                        <div class="social-icon">
                            <a href="#">
                                <i class="fab fa-facebook"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12 text-center mt-4">
                &copy; Copyright {{ date('Y') }}
            </div>
        </div>
    </div>
</div>
