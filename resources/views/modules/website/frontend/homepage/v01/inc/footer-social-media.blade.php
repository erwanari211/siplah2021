@if (isset($websiteSettings))
    @php
        $footerIcon['active'] = false;
        $footerIcon['social']['facebook'] = false;
        $footerIcon['social']['twitter'] = false;
        $footerIcon['social']['youtube'] = false;
        $footerIcon['social']['instagram'] = false;

        if (isset($websiteSettings['facebook_url']) && $websiteSettings['facebook_url']) {
            $footerIcon['social']['facebook'] = $websiteSettings['facebook_url'];
            $footerIcon['active'] = true;
        }
        if (isset($websiteSettings['twitter_url']) && $websiteSettings['twitter_url']) {
            $footerIcon['social']['twitter'] = $websiteSettings['twitter_url'];
            $footerIcon['active'] = true;
        }
        if (isset($websiteSettings['youtube_url']) && $websiteSettings['youtube_url']) {
            $footerIcon['social']['youtube'] = $websiteSettings['youtube_url'];
            $footerIcon['active'] = true;
        }
        if (isset($websiteSettings['instagram_url']) && $websiteSettings['instagram_url']) {
            $footerIcon['social']['instagram'] = $websiteSettings['instagram_url'];
            $footerIcon['active'] = true;
        }

    @endphp

    <div class="website-social-media">
        @if ($footerIcon['active'])
            <h3>Follow Us</h3>

            @if ($footerIcon['social']['facebook'])
                <a class="icon-link" href="{{ $footerIcon['social']['facebook'] }}">
                    <i class="fab fa-facebook"></i>
                </a>
            @endif
            @if ($footerIcon['social']['twitter'])
                <a class="icon-link" href="{{ $footerIcon['social']['twitter'] }}">
                    <i class="fab fa-twitter"></i>
                </a>
            @endif
            @if ($footerIcon['social']['youtube'])
                <a class="icon-link" href="{{ $footerIcon['social']['youtube'] }}">
                    <i class="fab fa-youtube"></i>
                </a>
            @endif
            @if ($footerIcon['social']['instagram'])
                <a class="icon-link" href="{{ $footerIcon['social']['instagram'] }}">
                    <i class="fab fa-instagram"></i>
                </a>
            @endif
        @endif
    </div>
@endif
