@push('css')
    <style type="text/css">
        .footer h3 {
            margin-top: 0;
        }

        .website-social-media a.icon-link {
            text-decoration: none;
        }

        .fa-facebook, .fa-facebook-square {
            color: #3b5998
        }
        .fa-twitter, .fa-twitter-square {
            color: #00aced
        }
        .fa-google-plus, .fa-google-plus-square {
            color: #dd4b39
        }
        .fa-youtube, .fa-youtube-play, .fa-youtube-square {
            color: #bb0000
        }
        .fa-tumblr, .fa-tumblr-square {
            color: #32506d
        }
        .fa-vine {
            color: #00bf8f
        }
        .fa-flickr {
            color: #ff0084
        }
        .fa-vimeo-square {
            color: #aad450
        }
        .fa-pinterest, .fa-pinterest-square {
            color: #cb2027
        }
        .fa-linkedin, .fa-linkedin-square {
            color: #007bb6
        }
        .fa-instagram {
            color: #517fa4;
        }
        .fa-spotify {
            color: #1ED760;
        }
    </style>
@endpush
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-4">
                @if (isset($isHomepage) && $isHomepage)
                    @if (isset($groupedLayoutElements['footer']))
                        @if (isset($groupedLayoutElements['footer']['footer.col-1']) && count($groupedLayoutElements['footer']['footer.col-1']))
                            @foreach ($groupedLayoutElements['footer']['footer.col-1'] as $item)
                                <div class="mb-4" data-role="layout-element">
                                    {!! $item['content'] !!}
                                </div>
                            @endforeach
                        @endif
                    @endif
                @else
                    @php
                        $aboutUs = '';
                        if (isset($marketplace) && $marketplace) {
                            $defaultdescription = '';
                            $defaultdescription .= '<h3 class="mt-0">About Us</h3>';
                            $defaultdescription .= '<strong>'.$marketplace->name.'</strong> is one of marketplace at Zuala.id. ';
                            $defaultdescription .= 'In this site you can buy and sell various kinds of products. ';
                            $aboutUs .= $defaultdescription;
                        }

                        if (isset($marketplaceSettings)) {
                            if (isset($marketplaceSettings['marketplace_description']) && $marketplaceSettings['marketplace_description']) {
                                $aboutUs = '';
                                $aboutUs .= '<h3 class="mt-0">About Us</h3>';
                                $aboutUs .= $marketplaceSettings['marketplace_description'];
                            }
                        }
                    @endphp

                    @php
                        $marketplaceHasSocialMedia = false;
                        $marketplaceInfos['facebook'] = ['url'=>null, 'name'=>'facebook'];
                        $marketplaceInfos['twitter'] = ['url'=>null, 'name'=>'twitter'];
                        $marketplaceInfos['youtube'] = ['url'=>null, 'name'=>'youtube'];
                        $marketplaceInfos['instagram'] = ['url'=>null, 'name'=>'instagram'];

                        if (isset($marketplaceSettings['facebook_url']) && $marketplaceSettings['facebook_url']) {
                            $marketplaceInfos['facebook']['url'] = $marketplaceSettings['facebook_url'];
                            if (isset($marketplaceSettings['facebook_name']) && $marketplaceSettings['facebook_name']) {
                                $marketplaceInfos['facebook']['name'] = $marketplaceSettings['facebook_name'];
                            }
                            $marketplaceHasSocialMedia = true;
                        }
                        if (isset($marketplaceSettings['twitter_url']) && $marketplaceSettings['twitter_url']) {
                            $marketplaceInfos['twitter']['url'] = $marketplaceSettings['twitter_url'];
                            if (isset($marketplaceSettings['twitter_name']) && $marketplaceSettings['twitter_name']) {
                                $marketplaceInfos['twitter']['name'] = $marketplaceSettings['twitter_name'];
                            }
                            $marketplaceHasSocialMedia = true;
                        }
                        if (isset($marketplaceSettings['youtube_url']) && $marketplaceSettings['youtube_url']) {
                            $marketplaceInfos['youtube']['url'] = $marketplaceSettings['youtube_url'];
                            if (isset($marketplaceSettings['youtube_name']) && $marketplaceSettings['youtube_name']) {
                                $marketplaceInfos['youtube']['name'] = $marketplaceSettings['youtube_name'];
                            }
                            $marketplaceHasSocialMedia = true;
                        }
                        if (isset($marketplaceSettings['instagram_url']) && $marketplaceSettings['instagram_url']) {
                            $marketplaceInfos['instagram']['url'] = $marketplaceSettings['instagram_url'];
                            if (isset($marketplaceSettings['instagram_name']) && $marketplaceSettings['instagram_name']) {
                                $marketplaceInfos['instagram']['name'] = $marketplaceSettings['instagram_name'];
                            }
                            $marketplaceHasSocialMedia = true;
                        }
                    @endphp

                    {!! $aboutUs !!}

                    @if ($marketplaceHasSocialMedia)
                        <h3 class="mt-0">Follow Us</h3>
                    @endif
                    @if ($marketplaceInfos['facebook']['url'])
                        <i class="fab fa-fw fa-facebook"></i>
                        <a href="{{ $marketplaceInfos['facebook']['url'] }}">
                            {{ $marketplaceInfos['facebook']['name'] }}
                        </a>
                        <br>
                    @endif
                    @if ($marketplaceInfos['twitter']['url'])
                        <i class="fab fa-fw fa-twitter"></i>
                        <a href="{{ $marketplaceInfos['twitter']['url'] }}">
                            {{ $marketplaceInfos['twitter']['name'] }}
                        </a>
                        <br>
                    @endif
                    @if ($marketplaceInfos['youtube']['url'])
                        <i class="fab fa-fw fa-youtube"></i>
                        <a href="{{ $marketplaceInfos['youtube']['url'] }}">
                            {{ $marketplaceInfos['youtube']['name'] }}
                        </a>
                        <br>
                    @endif
                    @if ($marketplaceInfos['instagram']['url'])
                        <i class="fab fa-fw fa-instagram"></i>
                        <a href="{{ $marketplaceInfos['instagram']['url'] }}">
                            {{ $marketplaceInfos['instagram']['name'] }}
                        </a>
                        <br>
                    @endif
                    @if (isset($marketplaceSettings['phone']) && $marketplaceSettings['phone'])
                        <i class="fa fa-fw fa-phone"></i> {{ $marketplaceSettings['phone'] }}
                        <br>
                    @endif
                    @if (isset($marketplaceSettings['mobile_phone']) && $marketplaceSettings['mobile_phone'])
                        <i class="fa fa-fw fa-mobile"></i> {{ $marketplaceSettings['mobile_phone'] }}
                        <br>
                    @endif
                @endif
            </div>
            <div class="col-md-3 mb-4">
                <div class="row">
                    <div class="col-xs-6 col-md-12">
                        @if (isset($groupedLayoutElements['footer']))
                            @if (isset($groupedLayoutElements['footer']['footer.col-2.a']) && count($groupedLayoutElements['footer']['footer.col-2.a']))
                                @foreach ($groupedLayoutElements['footer']['footer.col-2.a'] as $item)
                                    <div class="mb-4" data-role="layout-element">
                                        {!! $item['content'] !!}
                                    </div>
                                @endforeach
                            @endif
                        @endif
                    </div>
                    <div class="col-xs-6 col-md-12">
                        @if (isset($groupedLayoutElements['footer']))
                            @if (isset($groupedLayoutElements['footer']['footer.col-2.b']) && count($groupedLayoutElements['footer']['footer.col-2.b']))
                                @foreach ($groupedLayoutElements['footer']['footer.col-2.b'] as $item)
                                    <div class="mb-4" data-role="layout-element">
                                        {!! $item['content'] !!}
                                    </div>
                                @endforeach
                            @endif
                        @endif
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-3 mb-4">
                <div class="row">
                    <div class="col-xs-6 col-md-12">
                        @if (isset($groupedLayoutElements['footer']))
                            @if (isset($groupedLayoutElements['footer']['footer.col-3.a']) && count($groupedLayoutElements['footer']['footer.col-3.a']))
                                @foreach ($groupedLayoutElements['footer']['footer.col-3.a'] as $item)
                                    <div class="mb-4" data-role="layout-element">
                                        {!! $item['content'] !!}
                                    </div>
                                @endforeach
                            @endif
                        @endif
                    </div>
                    <div class="col-xs-6 col-md-12">
                        @if (isset($groupedLayoutElements['footer']))
                            @if (isset($groupedLayoutElements['footer']['footer.col-3.b']) && count($groupedLayoutElements['footer']['footer.col-3.b']))
                                @foreach ($groupedLayoutElements['footer']['footer.col-3.b'] as $item)
                                    <div class="mb-4" data-role="layout-element">
                                        {!! $item['content'] !!}
                                    </div>
                                @endforeach
                            @endif
                        @endif

                        @includeIf('modules.website.frontend.homepage.v01.inc.footer-social-media')
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12 text-center mt-4">
                &copy; Copyright {{ date('Y') }}
            </div>
        </div>
    </div>
</div>
