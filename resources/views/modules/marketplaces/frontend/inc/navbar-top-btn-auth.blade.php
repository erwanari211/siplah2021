@if (!auth()->check())
    <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i>
        </a>
        <ul class="dropdown-menu">
            <li>
                <div class="p-4">
                    <a class="btn btn-default" href="{{ route('login') }}"><i class="fa fa-sign-in-alt"></i> Login</a>
                    {{-- <a class="btn btn-default pull-right" href="{{ route('register') }}"><i class="fa fa-user-plus"></i> Register</a> --}}
                    <a class="btn btn-default pull-right" href="{{ route('marketplaces.stores.register.create') }}"><i class="fa fa-user-plus"></i> Register</a>
                </div>
            </li>
        </ul>
    </li>
@else
    @php
        $userPhoto = auth()->user()->photo ? asset(auth()->user()->photo) : asset('images/avatar-v01.png');
    @endphp
    <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ $userPhoto }}" class="user-image" alt="User Image">
            <!-- <i class="fa fa-user"></i> -->
        </a>
        <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
                <img src="{{ $userPhoto }}" class="img-circle" alt="User Image">
                <p>
                    {{ auth()->user()->name }}
                    <small></small>
                </p>
            </li>
            <li class="user-footer">
                <div class="pull-left">
                    <a href="{{ route('users.accounts.profile.index') }}" class="btn btn-default">Profile</a>
                </div>
                <div class="pull-right">
                    <a class="btn btn-default" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> Sign out
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </li>
@endif
