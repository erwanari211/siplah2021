<div class="sidenav sidenav-overlay sidenav-overlay--left" id="marketplace-category-sidebar">
    <div class="sidenav-overlay-toolbar">
        <button class="btn btn-default btn-xs sidenav-overlay-close"
            data-action="close-sidenav"
            data-target="#marketplace-category-sidebar">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <div class="sidenav-overlay-container">
        <div class="sidenav-overlay-content slimscroll">
            <nav class="metismenu-vertical-container">
                <ul class="metismenu">
                    @if (count($categories))
                        <li>
                            <a href="{{ route('products.index', [$marketplace->slug, $store->slug]) }}">
                                All Categories
                            </a>
                        </li>
                        @foreach ($categories as $category)
                            <li>
                                <a href="{{ route('stores.categories.show', [$marketplace->slug, $store->slug, $category->slug]) }}">
                                    {{ $category->name }}
                                </a>
                            </li>
                        @endforeach
                    @else
                        <li>
                            <a href="{{ route('products.index', [$marketplace->slug, $store->slug]) }}">
                                All Products
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
</div>
