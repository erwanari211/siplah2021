@push('css')
    <style>
        .navbar-top-search-wrapper .form-control {
            width: 50%;
        }
    </style>
@endpush

<ul class="dropdown-menu">
    <li>
        <div class="p-3">
            <form action="{{ route('marketplaces.products.index', [$marketplace->slug]) }}" method="GET" role="form"
                id="navbar-search-form">
                <div class="input-group navbar-top-search-wrapper">
                    <input type="text" class="form-control" name="search" value="{{ request('search') }}" placeholder="Search product from marketplace">
                    <select class="form-control" name="search_type">
                        <option value="product">Product</option>
                        <option value="store">Store</option>
                    </select>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div><!-- /input-group -->
            </form>
            <div class="mt-3 popular-search">
                @if (isset($popularSearches) && count($popularSearches))
                    @foreach ($popularSearches as $item)
                        <span class="btn btn-xs btn-default mr-3" data-popular-search="{{ $item->search }}">
                            {{ $item->search }}
                        </span>
                    @endforeach
                @endif
            </div>
        </div>
    </li>
</ul>
