@php
    $cartExist = $_navbarCart && $_navbarCart['count'] ? true : false;
    $cartItems = 0;
    $cartSubtotal = 0;
    if ($cartExist) {
        $cartItems = $_navbarCart['count'];
        $cartSubtotal = $_navbarCart['subtotal'];
    }
@endphp
<div class="sidenav sidenav-overlay sidenav-overlay--right" id="cart-sidebar">
    <div class="sidenav-overlay-toolbar">
        <button class="btn btn-default btn-xs sidenav-overlay-close"
            data-action="close-sidenav"
            data-target="#cart-sidebar">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <div class="sidenav-overlay-container">
        <div class="sidenav-overlay-content slimscroll">
            <div class="p-4">
                @if ($cartExist)
                    <span class="btn btn-default btn-block">
                        {{ $cartItems .' '. str_plural('item', $cartItems) }} -
                        {{ "Rp.".formatNumber($cartSubtotal) }}
                    </span>
                    <a class="btn btn-skin btn-block" href="{{ route('cart.index') }}">
                        <i class="fa fa-shopping-cart"></i> View cart
                    </a>
                @else
                    Cart is empty
                @endif
            </div>
        </div>
    </div>
</div>
