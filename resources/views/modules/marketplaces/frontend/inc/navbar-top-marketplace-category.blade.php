@php
    $_themes['marketplace_logo_mini'] = substr($marketplace->name, 0, 2);
    $_themes['marketplace_logo_lg'] = '';
    if ($marketplace->logo) {
        $_themes['marketplace_logo_lg'] = '<img src="'.asset($marketplace->logo).'">';
    } else {
        $_themes['marketplace_logo_lg'] = $marketplace->name;
    }
@endphp
<header class="main-header fixed navbar-black-light">
    <!-- Logo -->
    <a href="{{ route('marketplaces.show', [$marketplace->slug]) }}" class="logo navbar-brand">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            {!! $_themes['marketplace_logo_mini'] !!}
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            {!! $_themes['marketplace_logo_lg'] !!}
        </span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->

        <div class="navbar-left">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/') }}" title="Home">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                {{--
                <li>
                    <a href="{{ route('stores.create', $marketplace->slug) }}"
                        title="Create Store">
                        <i class="fa fa-store"></i>
                        <span class="label bg-skin">+</span>
                    </a>
                </li>
                --}}
                <li>
                    <a href="#" data-action="open-sidenav"
                        data-target="#marketplace-category-sidebar"
                        title="Categories">
                        <i class="fa fa-bars"></i>
                        <span class="hidden-xs">
                            Categories
                        </span>
                    </a>
                </li>
            </ul>
        </div>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('m/delivery-status') }}"
                        title="Track Delivery">
                        <i class="fa fa-truck"></i>
                    </a>
                </li>
                <li class="dropdown menu-search">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-search"></i>
                    </a>
                    @include('modules.marketplaces.frontend.inc.navbar-search-marketplace-category')
                </li>
                @include('modules.marketplaces.frontend.inc.navbar-top-btn-cart')
                @include('modules.marketplaces.frontend.inc.navbar-top-btn-auth')
            </ul>
        </div>
    </nav>
</header>
