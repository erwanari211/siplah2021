<ul class="dropdown-menu">
    <li>
        <div class="p-3">
            <form action="{{ route('stores.categories.show', [$marketplace->slug, $store->slug, $collection->slug]) }}" method="GET" role="form">
                <div class="input-group">
                    <input type="text" class="form-control" name="search" value="{{ request('search') }}" placeholder="Search product from store collection">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div><!-- /input-group -->
            </form>
        </div>
    </li>
</ul>
