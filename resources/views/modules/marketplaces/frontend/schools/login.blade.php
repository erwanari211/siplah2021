@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Login School')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <div class="box">
                        <div class="box-content">
                            <div class="auth-form">
                                {!! Form::open([
                                    'url' => route('marketplaces.schools.login.store'),
                                    'method' => 'GET',
                                    'files' => true,
                                    ]) !!}

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <span>{{ $error }}</span>
                                                <br>
                                            @endforeach
                                        </div>
                                    @endif

                                    @include('flash::message')

                                    <h3 class="text-primary">
                                        Login
                                    </h3>

                                    {!! Form::bs3Text('code') !!}

                                    <button class="btn btn-primary" type="submit" id="submit-form-btn">
                                        Login
                                    </button>
                                    <a class="btn btn-default"  href="{{ route('login') }}">
                                        Back
                                    </a>

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
