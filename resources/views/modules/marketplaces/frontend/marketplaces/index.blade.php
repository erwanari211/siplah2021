@php
    // $_themeSetting['skin'] = 'skin-red';

    $templatePageTitle = 'Marketplaces at {APP} | {APP}';
    $templatePageDescription = 'Get products you need from {APP}. Fast Delivery and secure payment at {APP}.';
    $searchArray = ['{APP}'];
    $replaceArray = [env('APP_NAME')];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .pagination {
            margin-top: 0;
        }
    </style>
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-normal')

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('marketplaces.index') }}">Marketplaces</a>
                        </li>
                    </ol>

                    @if ($collection)
                        <h3>{{ $collection->name }}</h3>
                    @else
                        <h3>Marketplaces</h3>
                    @endif

                    @include('flash::message')

                    <div class="mb-4">
                        <a class="btn btn-default" href="{{ route('marketplaces.create') }}">
                            <i class="fa fa-plus"></i>
                            Create Marketplace
                        </a>
                        <a class="btn btn-default" data-toggle="collapse" href="#search-marketplace-container">
                            <i class="fa fa-search"></i>
                            Search Marketplace
                        </a>
                        @if ($collection)
                            <a class="btn btn-default" href="{{ route('marketplaces.index') }}">
                                View All Marketplaces
                            </a>
                        @endif
                    </div>

                    <div class="collapse" id="search-marketplace-container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="box">
                                    <div class="box-content">
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-md-9 mb-2">
                                                    <input
                                                        type="text"
                                                        class="form-control"
                                                        name="search"
                                                        value="{{ request('search') }}"
                                                        placeholder="Search Marketplace">
                                                    @if (request('collection'))
                                                        <input
                                                            type="hidden"
                                                            class="form-control"
                                                            name="collection"
                                                            value="{{ request('collection') }}">
                                                    @endif
                                                </div>
                                                <div class="col-md-3 mb-2">
                                                    <button type="submit" class="btn btn-block btn-default">
                                                        <i class="fa fa-search"></i>
                                                        Search
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if (count($marketplaces))
                        <div class="row">
                            @foreach ($marketplaces as $marketplace)
                                @if ($collection)
                                    @php
                                        $detail = $marketplace;
                                        $marketplace = $detail->marketplace;
                                    @endphp
                                @endif
                                <div class="col-xs-6 col-sm-3 col-md-2">
                                    <div class="box">
                                        <div class="box-content">
                                            <div class="icon-box">
                                                <a class="icon-box-link" href="{{ route('marketplaces.show', $marketplace->slug) }}">
                                                    <div class="icon-box-icon-container">
                                                        <img class="icon-box-image lazy " data-src="{{ asset($marketplace->image) }}">
                                                    </div>
                                                    <div class="icon-box-content">
                                                        <span class="icon-box-title line-clamp-2">{{ $marketplace->name }}</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        {{ $marketplaces->appends(request()->only(['collection', 'search']))->links() }}
                    @endif
                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
