@push('js')
    <script>
        $(document).ready(function () {
            $('.select-rating').barrating({
                theme: 'fontawesome-stars',
                showSelectedRating: true,
            });

            function updatePriceRange() {
                var priceRangeType = $('[name="price_range"]').val();
                var minPriceElm = $('[name="min_price"]');
                var maxPriceElm = $('[name="max_price"]');

                if (priceRangeType == 'custom') {

                }

                if (priceRangeType == '0_10') {
                    minPriceElm.val(0);
                    maxPriceElm.val(10000000);
                }

                if (priceRangeType == '10_50') {
                    minPriceElm.val(10000000);
                    maxPriceElm.val(50000000);
                }

                if (priceRangeType == '50_200') {
                    minPriceElm.val(50000000);
                    maxPriceElm.val(200000000);
                }

                if (priceRangeType == 'gte_200') {
                    minPriceElm.val(200000000);
                    maxPriceElm.val('');
                }
            }

            updatePriceRange();
            $('[name="price_range"]').on('change', function () {
                updatePriceRange();
            });
        });
    </script>
@endpush

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
@endpush

@push('js')
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            var url = "{{ route('ajax.regions.cities.index') }}";
            $('.js-data-example-ajax').select2({
                ajax: {
                    url: url,
                    delay: 250,
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                        }

                        return query;
                    },
                    processResults: function (data, params) {
                        return {
                            results: data,
                        };
                    },
                    cache: true,
                    minimumInputLength: 1,
                }
            });
        });
    </script>
@endpush

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-content">
                    {!! Form::model(request()->all(), [
                        'url' => url()->current(),
                        'method' => 'GET',
                        ]) !!}


                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group hidden">
                                    {!! Form::text('search', request('search'), ['class' => 'form-control']) !!}
                                </div>

                                <p>Harga</p>
                                @php
                                    $dropdown['price_range'] = [
                                        'custom' => 'Custom',
                                        '0_10' => 'Kurang dari 10 juta',
                                        '10_50' => '10 s/d 50 juta',
                                        '50_200' => '50 s/d 200 juta',
                                        'gte_200' => 'Lebih dari 200 juta',
                                    ];
                                @endphp
                                <div class="form-group">
                                    {!! Form::select('price_range', $dropdown['price_range'], request('price_range'), ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::number('min_price', null, ['placeholder' => 'Harga Min', 'class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::number('max_price', null, ['placeholder' => 'Harga Max', 'class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <p>Produk Dalam Negeri</p>
                                <div class="form-group">
                                    {!! Form::select('is_local_product', $dropdown['yes_no'], request('is_local_product'), ['class' => 'form-control']) !!}
                                </div>

                                <p>Produk UMKM</p>
                                <div class="form-group">
                                    {!! Form::select('is_msme_product', $dropdown['yes_no'], request('is_msme_product'), ['class' => 'form-control']) !!}
                                </div>

                                <p>Rating</p>
                                @php
                                    $dropdown['rating'] = [
                                        '' => 0,
                                        '1' => 1,
                                        '2' => 2,
                                        '3' => 3,
                                        '4' => 4,
                                        '5' => 5,
                                    ]
                                @endphp
                                <div class="form-group">
                                    {!! Form::select('rating', $dropdown['rating'], request('rating'), [
                                        'class' => 'select-rating',
                                        'data-initial-rating' => request('rating') ?? 0,
                                    ]) !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <p>Perusahaan UMKM</p>
                                <div class="form-group">
                                    {!! Form::select('is_msme', $dropdown['yes_no'], request('is_msme'), ['class' => 'form-control']) !!}
                                </div>

                                <p>Memiliki rating</p>
                                <div class="form-group">
                                    {!! Form::select('store_has_rating', $dropdown['yes_no'], request('store_has_rating'), ['class' => 'form-control']) !!}
                                </div>

                                <p>Lokasi</p>
                                <div class="form-group">
                                    <select name="city_id" class="js-data-example-ajax" style="width:100%">
                                        @if (request('city_id'))
                                            <option value="{{ $selectedCity->id }}" selected="selected">
                                                {{ $selectedCity->name }}
                                            </option>
                                        @endif
                                    </select>
                                </div>

                                @if (session('login_as') == 'school_staff')
                                    <p>Jarak Pencarian</p>
                                    <div class="form-group">
                                        {!! Form::number('distance', null, ['placeholder' => 'Jarak dalam KM', 'class' => 'form-control']) !!}
                                    </div>
                                @endif
                            </div>

                            <div class="col-sm-3">
                                <p>Sort Order</p>
                                <div class="form-group">
                                    {!! Form::select('sort', $dropdown['sortBy'], request('sort'), ['id'=>'products-sort-filter', 'class' => 'form-control']) !!}
                                </div>
                            </div>

                        </div>

                        {!! Form::bs3Submit('Filter'); !!}
                        <a class="btn btn-default" href="{{ url()->current() }}">
                            Reset Filter
                        </a>


                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
