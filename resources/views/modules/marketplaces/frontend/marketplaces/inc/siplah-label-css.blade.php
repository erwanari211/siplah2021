@push('css')
    <style>
        .products.products-grid .product .product__siplah-label {
            position: absolute;
            z-index: 3;
            bottom: 0px;
            background-color: transparent;
        }

        .products.products-grid .product .product__siplah-label.left {
            left: 0px;
            max-width: 95%;
        }

        .products.products-grid .product .product__siplah-label .siplah-label {
            font-size: 10px;
            display: inline-block;
            padding: 0 5px;
            border-radius: 4px;
        }

        .products.products-grid .product .product__siplah-label .siplah-label-produk-umkm {
            background-color: #1C3B6D;
            color: white;
        }

        .products.products-grid .product .product__siplah-label .siplah-label-produk-lokal {
            background-color: #F70000;
            color: white;
        }

        .products.products-grid .product .product__siplah-label .siplah-label-produk-baru {
            background-color: #5CB85C;
            color: white;
        }

    </style>
@endpush
