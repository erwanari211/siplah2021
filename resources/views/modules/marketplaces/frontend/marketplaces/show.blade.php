@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    $templatePageTitle = '{marketplace} | {APP}';
    $templatePageDescription = 'Get products you need from {marketplace}. Fast Delivery and secure payment at {APP}.';
    $searchArray = ['{marketplace}', '{APP}'];
    $replaceArray = [$marketplace->name, env('APP_NAME')];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .promo-title span {
            display: inline-block;
            border-bottom: 5px solid;
        }

        .hr-banner {
            margin: 10px 0;
            border-color: #ddd;
        }
    </style>
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#marketplace-category-sidebar').metisMenu({
                toggle: false,
            });

            $('.product-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin:15,
                stagePadding: 50,
                nav:true,
                navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                autoHeight:true,
                lazyLoad:true,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        stagePadding: 80,
                    },
                    480:{
                        items:2,
                        nav:false,
                    },
                    768:{
                        items:3
                    },
                    992:{
                        items:4
                    },
                    1200:{
                        items:4
                    }
                },
            });

            $('.banner-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin:15,
                stagePadding: 50,
                nav:true,
                navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                autoHeight:true,
                lazyLoad:true,
                // responsiveClass:true,
                // responsive:{
                //     0:{
                //         items:1,
                //         nav:false,
                //         stagePadding: 80,
                //     },
                //     480:{
                //         items:2,
                //         nav:false,
                //     },
                //     768:{
                //         items:3
                //     },
                //     992:{
                //         items:4
                //     },
                //     1200:{
                //         items:4
                //     }
                // },
            });

            $('[data-popular-search]').on('click', function(event) {
                event.preventDefault();
                var searchString = $(this).attr('data-popular-search');
                var marketplaceProductUrl = '{{ route('marketplaces.products.index', [$marketplace->slug]) }}';
                $('[name="search"]').val(searchString);
                $('#navbar-search-form').attr('action', marketplaceProductUrl);
                $('#navbar-search-form').submit();
            });

            $('[data-action="display-video"]').on('click', function(event) {
                event.preventDefault();

                var link = $(this).attr('href');

                var video = $("#banner-video")[0];
                var source = $(video).find("#banner-video-mp4");
                source.attr("src", link);

                video.load();
                video.pause();
                video.currentTime = 0;
                video.play();

                $('#modal-banner-video').modal('show');
            });

            $('#modal-banner-video').on('hidden.bs.modal', function() {
                var video = $('#banner-video')[0];
                video.pause();
                video.currentTime = 0;
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-marketplace')

    <div class="page-content">
        @include('modules.marketplaces.frontend.marketplaces.inc.marketplace-inactive-message')

        <div class="container">
            @if ($bannerGroups)
                @foreach ($bannerGroups as $group)
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>{{ $group->display_name }}</h3>
                        </div>
                    </div>
                    @if ($group->type == 'normal')
                        <div class="row">
                            @foreach ($group->details as $detail)
                                @php
                                    $banner = $detail->marketplaceBanner;
                                @endphp
                                <div class="col-sm-6">
                                    <div class="box">
                                        <div class="box-content no-padding">
                                            @if ($banner->link)
                                                @if ($banner->type == 'video')
                                                    <a href="{{ $banner->link }}"
                                                        data-action="display-video">
                                                        <img class="lazy" data-src="{{ asset($banner->image) }}" width="100%">
                                                    </a>
                                                @else
                                                    <a href="{{ $banner->link }}">
                                                        <img class="lazy" data-src="{{ asset($banner->image) }}" width="100%">
                                                    </a>
                                                @endif
                                            @else
                                                <img class="lazy" data-src="{{ asset($banner->image) }}" width="100%">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    @if ($group->type == 'slider')
                        <div class="banner-carousel owl-carousel owl-theme">
                            @foreach ($group->details as $detail)
                                @php
                                    $banner = $detail->marketplaceBanner;
                                @endphp
                                <div class="item">
                                    <div class="box">
                                        <div class="box-content no-padding">
                                            @if ($banner->link)
                                                @if ($banner->type == 'video')
                                                    <a href="{{ $banner->link }}"
                                                        data-action="display-video">
                                                        <img class="owl-lazy" data-src="{{ asset($banner->image) }}" width="100%">
                                                    </a>
                                                @else
                                                    <a href="{{ $banner->link }}">
                                                        <img class="owl-lazy" data-src="{{ asset($banner->image) }}" width="100%">
                                                    </a>
                                                @endif
                                            @else
                                                <img class="owl-lazy" data-src="{{ asset($banner->image) }}" width="100%">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    <hr class="hr-banner">
                @endforeach
            @endif
        </div>

        <div class="container">
            <div class="row store__collections store__collections--icon-box">
                @if (count($categories))
                    <div class="col-sm-12">
                        <h3>Categories</h3>
                    </div>
                    @foreach ($categories as $category)
                        <div class="col-xs-6 col-sm-3 col-md-2">
                            <div class="box">
                                <div class="box-content">
                                    <div class="icon-box">
                                        <a class="icon-box-link"
                                            href="{{ route('marketplaces.categories.show', [$marketplace->slug, $category->slug]) }}">
                                            <div class="icon-box-icon-container">
                                                <img class="icon-box-image lazy" data-src="{{ asset($category->image) }}">
                                            </div>
                                            <div class="icon-box-content">
                                                <span class="icon-box-title line-clamp-2">{{ $category->name }}</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-sm-12">
                        <span>This store have no collection</span>
                    </div>
                @endif
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="container">
            @if (count($storeCollections))
                @foreach ($storeCollections as $collection)
                    @if (count($collection->details))
                        {{-- <h3>{{ $collection->display_name }}</h3> --}}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="mt-4">
                                    <h3 class="mt-0 pull-left">
                                        {{ $collection->display_name ? $collection->display_name : 'Our Stores' }}
                                    </h3>
                                    <a class="btn btn-default btn-xs pull-right mb-4" href="{{ route('stores.index', [$marketplace->slug, 'collection' => $collection->id]) }}">See more</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @foreach ($collection->details as $detail)
                                @php
                                    $store = $detail->store;
                                @endphp
                                <div class="col-xs-6 col-sm-3 col-md-2">
                                    <div class="box">
                                        <div class="box-content">
                                            <div class="icon-box">
                                                <a class="icon-box-link" href="{{ route('stores.show', [$marketplace->slug, $store->slug]) }}">
                                                    <div class="icon-box-icon-container">
                                                        <img class="icon-box-image lazy" data-src="{{ asset($store->image) }}">
                                                    </div>
                                                    <div class="icon-box-content">
                                                        <span class="icon-box-title line-clamp-2">{{ $store->name }}</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                @endforeach
            @endif
        </div>

        <div class="container">
            @if ($productCollections)
                @foreach ($productCollections as $collection)
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="mt-4">
                                <h3 class="mt-0">
                                    {{ $collection->display_name }}
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="products products-grid row">
                                @if (count($collection->details))
                                    @foreach ($collection->details as $detail)
                                        @php
                                            $product = $detail->product;
                                            $store = $product->store;
                                            $marketplace = $store->marketplace;
                                            $productMetas = $product->getAllMeta();
                                            $originalPrice = isset($productMetas['original_price']) ? $productMetas['original_price'] : 0;
                                            $discountAmountPercent = $product->discountAmountPercent;
                                        @endphp
                                        <div class="col-md-3 col-sm-4 col-xs-6">
                                            <div class="product">
                                                @if ($discountAmountPercent)
                                                    <span class="product__label right">
                                                        <div class="text-center">
                                                        {{ $discountAmountPercent }}%<br>OFF
                                                        </div>
                                                    </span>
                                                @endif
                                                <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">
                                                    <div class="product__image-wrapper">
                                                        <img class="product__image lazy" data-src="{{ $product->image_url }}">
                                                    </div>
                                                </a>
                                                <strong class="product__name">
                                                    <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">
                                                        {{ $product->name }}
                                                    </a>
                                                </strong>
                                                <div class="product__price">
                                                    @if ($originalPrice)
                                                        <span class="product__original-price">{{ "Rp.".formatNumber($originalPrice) }}</span>
                                                    @endif
                                                    <span class="product__current-price">{{ "Rp.".formatNumber($product->price) }}</span>
                                                </div>
                                                <div class="product__rating">
                                                    <select class="rating" data-initial-rating="{{ $product->reviews->average('rating') }}">
                                                      <option value="">0</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                    </select>
                                                    @if ($product->reviews->count())
                                                        <span>({{ $product->reviews->count() }})</span>
                                                    @endif
                                                </div>
                                                <div class="product__store">
                                                    <div class="product__store__name">
                                                        <a class="text-muted"
                                                            href="{{ route('stores.show', [$marketplace->slug, $product->store->slug]) }}">
                                                            <span>{{ $product->store->name }}</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    No Product
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

        <div class="modal fade" id="modal-banner-video">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Video</h4>
                    </div>
                    <div class="modal-body">
                        <video controls="" id="banner-video" style="width: 100%; height: auto; margin:0 auto; frameborder:0;">
                            <source id="banner-video-mp4" src="" type="video/mp4">
                            Your browser doesn't support HTML5 video tag.
                        </video>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-marketplace-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
