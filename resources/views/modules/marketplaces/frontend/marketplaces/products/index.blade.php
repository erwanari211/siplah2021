@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    $templatePageTitle = 'Products from {marketplace} | {APP}';
    $templatePageDescription = 'Get products you need from {marketplace}. Fast Delivery and secure payment at {APP}.';
    $searchArray = ['{marketplace}', '{APP}'];
    $replaceArray = [$marketplace->name, env('APP_NAME')];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .promo-title span {
            display: inline-block;
            border-bottom: 5px solid;
        }

        .product__name a,
        .product__store__name a {
            text-decoration: none;
        }

        .d-inline-block {
            display: inline-block;
        }
    </style>
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#marketplace-category-sidebar').metisMenu({
                toggle: false,
            });

            $('[data-popular-search]').on('click', function(event) {
                event.preventDefault();
                var searchString = $(this).attr('data-popular-search');
                $('[name="search"]').val(searchString);
                $('#navbar-search-form').submit();
            });

            $('#products-sort').on('change', function(event) {
                event.preventDefault();
                var url = '{{ $url }}';
                var queryString = '{{ $queryString }}';
                var sortBy = $(this).val();
                var newUrl = '';
                newUrl += url + '?sort=' + sortBy;
                newUrl += (queryString) ? '&' + queryString : '';
                window.location.replace(newUrl);
            });
        });
    </script>
@endpush

@include('modules.marketplaces.frontend.marketplaces.inc.siplah-label-css')

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-marketplace')

    <div class="page-content">
        @include('modules.marketplaces.frontend.marketplaces.inc.marketplace-inactive-message')

        <div class="container">
            <div class="row">
                <div class="col-sm-12">


                    <ol class="breadcrumb mb-4">
                        <li>
                            <a href="{{ route('marketplaces.show', [$marketplace->slug]) }}">
                                {{ $marketplace->name }}
                            </a>
                        </li>
                        <li class="active">
                            <a href="{{ route('marketplaces.products.index', [$marketplace->slug]) }}">
                                Products
                            </a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>

        @include('modules.marketplaces.frontend.marketplaces.inc.product-filter-options')

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    @php
                        $productTotal = $products->total();
                        $resultLabel = str_plural('result', $productTotal);
                    @endphp

                    @if (!request('search'))
                        <h4>
                            Found {{ $productTotal }} {{ $resultLabel }}
                        </h4>
                    @endif

                    @if (request('search'))
                        <h4>
                            Search result for "{{ request('search') }}"
                            found {{ $productTotal }} {{ $resultLabel }}
                        </h4>
                    @endif

                    @if (count($products))
                        <div class="products products-grid row">
                            @foreach ($products as $product)
                                @php
                                    $productMetas = $product->getAllMeta();
                                    $originalPrice = isset($productMetas['original_price']) ? $productMetas['original_price'] : 0;
                                    $discountAmountPercent = $product->discountAmountPercent;
                                @endphp
                                <div class="col-md-3 col-sm-4 col-xs-6">
                                    <div class="product">
                                        @if ($discountAmountPercent)
                                            <span class="product__label right">
                                                <div class="text-center">
                                                {{ $discountAmountPercent }}%<br>OFF
                                                </div>
                                            </span>
                                        @endif
                                        <a href="{{ route('products.show', [$marketplace->slug, $product->store->slug, $product->slug]) }}">
                                            <div class="product__image-wrapper">

                                                <div class="product__siplah-label left">
                                                    @if ($product->is_new_product)
                                                        <span class="text-center siplah-label siplah-label-produk-baru">
                                                            Baru
                                                        </span>
                                                    @endif

                                                    @if ($product->is_msme_product)
                                                        <span class="text-center siplah-label siplah-label-produk-umkm">
                                                            Produk UMKM
                                                        </span>
                                                    @endif

                                                    @if ($product->is_local_product)
                                                        <span class="text-center siplah-label siplah-label-produk-lokal">
                                                            Produk Lokal
                                                        </span>
                                                    @endif
                                                </div>

                                                <img class="product__image lazy" data-src="{{ $product->image_url }}">
                                            </div>
                                        </a>
                                        <strong class="product__name">
                                            <a href="{{ route('products.show', [$marketplace->slug, $product->store->slug, $product->slug]) }}">
                                                {{ $product->name }}
                                            </a>
                                        </strong>

                                        @php
                                            $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
                                        @endphp

                                        @if (!$hasKemdikbudZonePrice)

                                        <div class="product__price">
                                            @if ($originalPrice)
                                                <span class="product__original-price">{{ "Rp.".formatNumber($originalPrice) }}</span>
                                            @endif
                                            <span class="product__current-price">{{ "Rp.".formatNumber($product->price) }}</span>
                                        </div>

                                        @endif

                                        @if ($hasKemdikbudZonePrice)
                                            @php
                                                $price = 0;
                                                $minPrice = 0;
                                                $maxPrice = 0;

                                                $mappedKemdikbudZonePrices = $product->getMappedKemdikbudZonePrices();
                                                $kemdikbudMinMaxZonePrice = $product->getKemdikbudMinMaxZonePrices($mappedKemdikbudZonePrices);
                                                $minPrice = $kemdikbudMinMaxZonePrice['minPrice'];
                                                $maxPrice = $kemdikbudMinMaxZonePrice['maxPrice'];
                                                $formattedZonePrice = $product->getKemdikbudFormattedZonePrices($minPrice, $maxPrice);
                                            @endphp
                                            <div class="product__price">
                                                <span class="product__current-price">{{ $formattedZonePrice }}</span>
                                            </div>
                                        @endif

                                        <div class="product__rating">
                                            <select class="rating" data-initial-rating="{{ $product->rating }}">
                                                <option value="">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                            @if ($product->reviews_count)
                                                <span>({{ $product->reviews_count }})</span>
                                            @endif
                                        </div>
                                        <div class="product__store">
                                            <div class="product__store__name">
                                                <a class="text-muted"
                                                    href="{{ route('stores.show', [$marketplace->slug, $product->store->slug]) }}">
                                                    <span>{{ $product->store->name }}</span>
                                                </a>
                                            </div>
                                            @php
                                                $store = $product->store;
                                                $addresses = $store->addresses;
                                                $storeAddress = null;
                                                foreach ($addresses as $address) {
                                                    if ($address->default_address) {
                                                        $storeAddress = $address;
                                                    }
                                                }

                                                $location = [];
                                                if ($storeAddress) {
                                                    if ($storeAddress->city) {
                                                        $cityName = $storeAddress->city->name;
                                                        $location[] = $cityName;
                                                    }
                                                    if ($storeAddress->province) {
                                                        $location[] = $storeAddress->province->name;
                                                    }
                                                }

                                                $storeAddress = '';
                                                $storeAddress = ucwords(strtolower(implode(', ', $location)))
                                            @endphp
                                            <div class="product__store__location">
                                                @if ($storeAddress)
                                                    <small title="{{ $storeAddress }}">
                                                        <i class="fa fa-map-marker-alt text-danger"></i>
                                                        <span class="text-muted">{{ $storeAddress }}</span>
                                                    </small>
                                                @else
                                                    <small> &nbsp; </small>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        No Product
                    @endif
                    {{ $products->appends(request()->only([
                        'search', 'sort',
                        'min_price', 'max_price',
                        'rating',
                        'is_local_product', 'is_msme_product',
                        'is_msme', 'store_has_rating', 'city_id',
                        'distance',
                    ]))->links() }}
                </div>
            </div>
        </div>
    </div>

    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-marketplace-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
