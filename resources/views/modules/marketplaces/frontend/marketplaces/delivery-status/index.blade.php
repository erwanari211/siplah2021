@php
    // $_themeSetting['skin'] = 'skin-red';

    $templatePageTitle = 'Marketplaces at {APP} | {APP}';
    $templatePageDescription = 'Get products you need from {APP}. Fast Delivery and secure payment at {APP}.';
    $searchArray = ['{APP}'];
    $replaceArray = [env('APP_NAME')];

    $_page['title'] = 'Cek Resi Online JNE, POS, TIKI, JNT, Wahana, Si Cepat - '.env('APP_NAME');
    $_page['meta_description'] = 'Cek nomor resi pengiriman paket barang online dari JNE Express, Pos Indonesia, TIKI, J&T Express, Wahana Logistik, Sicepat tanpa captcha. Lacak sekarang!';

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .pagination {
            margin-top: 0;
        }

        .borderless tr,
        .borderless td,
        .borderless th {
            border: none !important;
        }

        .table-condensed > tbody > tr > td {
            padding: 0;
        }

        .text-nowrap {
            white-space: nowrap;
        }
    </style>
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            var currentUrl = '{{ url()->current() }}';

            $('#delivery-status-wrapper').hide();
            $('#check_waybill').on('click', function(event) {
                event.preventDefault();

                $('#spinner-icon').removeClass('hide').show();
                hideData();

                let url = '{{ url('/m/delivery-status-ajax') }}';

                var _token = $("input[name='_token']").val();
                let courier = $('[name=courier]').val();
                let waybill = $('[name=waybill]').val();
                let postData = {
                    _token: _token,
                    courier: courier,
                    waybill: waybill,
                }

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: postData,
                })
                .done(function(result) {
                    if (result.success) {
                        let data = result.data;
                        processData(data);

                        let newUrl = '';
                        newUrl += currentUrl;
                        newUrl += '?waybill='+waybill+'&courier='+courier;
                        window.history.pushState(postData, '', newUrl);
                    } else {
                        $('#delivery-status-error-message').find('.alert-danger').html('');

                        let message = result.message;
                        $('#delivery-status-error-message').find('.alert-danger').html(message);
                        $('#delivery-status-error-message').removeClass('hide').fadeIn();
                    }

                    $('#spinner-icon').hide();
                });
            });

            $('#reset_waybill').on('click', function(event) {
                event.preventDefault();

                $('[name=courier]').val('');
                $('[name=waybill]').val('');

                hideData();
            });

            function hideData() {
                $('#delivery-status-wrapper').hide();
                $('#delivery-status-error-message').hide();
            }

            function processData(data) {
                let output = '';

                // INFO
                $('#delivery-status-info').html('');
                output = '';
                output += `
                    <table class="table table-condensed borderless no-padding mb-0">
                        <tbody>
                            <tr>
                                <td> <small class="text-muted">Code</small> </td>
                                <td> <small class="text-muted mr-2">:</small> </td>
                                <td> ${data.details.waybill_number} </td>
                            </tr>
                            <tr>
                                <td> <small class="text-muted">Courier</small> </td>
                                <td> <small class="text-muted mr-2">:</small> </td>
                                <td> ${data.summary.courier_name} </td>
                            </tr>
                            <tr>
                                <td> <small class="text-muted">Service</small> </td>
                                <td> <small class="text-muted mr-2">:</small> </td>
                                <td> ${data.summary.service_code} </td>
                            </tr>
                            <tr>
                                <td> <small class="text-muted">Shipped at</small> </td>
                                <td> <small class="text-muted mr-2">:</small> </td>
                                <td> ${data.details.waybill_date} ${data.details.waybill_time} </td>
                            </tr>
                            <tr>
                                <td> <small class="text-muted">Weight</small> </td>
                                <td> <small class="text-muted mr-2">:</small> </td>
                                <td> ${data.details.weight} Kg </td>
                            </tr>
                        </tbody>
                    </table>
                `;
                $('#delivery-status-info').html(output);

                // STATUS
                $('#delivery-status-status').html('');
                output = '';
                output += `
                    <table class="table table-condensed borderless no-padding mb-0">
                        <tbody>
                `;
                if (data.delivery_status.status != 'DELIVERED') {
                    output += `
                        <tr>
                            <td> <small class="text-muted">Status</small> </td>
                            <td> <small class="text-muted mr-2">:</small> </td>
                            <td> ${data.delivery_status.status} </td>
                        </tr>
                    `;
                }
                if (data.delivery_status.status == 'DELIVERED') {
                    output += `
                        <tr>
                            <td> <small class="text-muted">Status</small> </td>
                            <td> <small class="text-muted mr-2">:</small> </td>
                            <td>
                                ${data.delivery_status.status}
                                <i class="fa fa-check-circle text-success"></i>
                            </td>
                        </tr>
                        <tr>
                            <td> <small class="text-muted">Received by</small> </td>
                            <td> <small class="text-muted mr-2">:</small> </td>
                            <td> ${data.delivery_status.pod_receiver} </td>
                        </tr>
                        <tr>
                            <td> <small class="text-muted">Received at</small> </td>
                            <td> <small class="text-muted mr-2">:</small> </td>
                            <td> ${data.delivery_status.pod_date} ${data.delivery_status.pod_time} </td>
                        </tr>
                    `;
                }
                output += `
                        </tbody>
                    </table>
                `;
                $('#delivery-status-status').html(output);

                // SHIPPER
                $('#delivery-status-shipper').html('');
                output = '';
                output += `
                    <small class="text-muted">Shipper : </small>
                    <br>
                    ${data.details.shippper_name}
                    <br>
                    <small class="text-muted">
                        ${data.details.shipper_address1}
                        ${data.details.shipper_address2 ? ', '+data.details.shipper_address2 : ''}
                        ${data.details.shipper_address3 ? ', '+data.details.shipper_address3 : ''}
                    </small>
                    <br>
                    <small class="text-muted">
                        ${data.details.shipper_city}
                    </small>
                    <br>
                `;
                $('#delivery-status-shipper').html(output);

                // RECEIVER
                $('#delivery-status-receiver').html('');
                output = '';
                output += `
                    <small class="text-muted">Receiver :</small>
                    <br>
                    ${data.details.receiver_name}
                    <br>
                    <small class="text-muted">
                        ${data.details.receiver_address1}
                        ${data.details.receiver_address2 ? ', '+data.details.receiver_address2 : ''}
                        ${data.details.receiver_address3 ? ', '+data.details.receiver_address3 : ''}
                    </small>
                    <br>
                    <small class="text-muted">
                        ${data.details.receiver_city}
                    </small>
                    <br>
                `;
                $('#delivery-status-receiver').html(output);

                // DETAILS
                $('#delivery-status-details').find('tbody').html('');
                output = '';
                if (data.manifest && data.manifest.length) {
                    for (var i = 0; i < data.manifest.length; i++) {
                        let detail = data.manifest[i]
                        output += `
                            <tr>
                                <td>
                                    <small class="text-muted text-nowrap">
                                        ${detail.manifest_date}
                                    </small>
                                    <small class="text-muted text-nowrap">
                                        ${detail.manifest_time}
                                    </small>
                                </td>
                                <td>
                                    <span>${detail.manifest_description}</span>
                                </td>
                            </tr>
                        `;
                    }
                } else {
                    output += `
                        <tr>
                            <td colspan="2">
                                No data
                            </td>
                        </tr>
                    `;
                }
                $('#delivery-status-details').find('tbody').html(output);

                $('#delivery-status-wrapper').removeClass('hide').fadeIn();
            }
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-normal')

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">Home</a>
                        </li>
                        <li>
                            <span>
                                Check Delivery Status
                            </span>
                        </li>
                    </ol>


                    <div class="row">
                        <div class="col-md-3">
                            <div class="box">
                                <div class="box-content">
                                    <h3 class="mt-0">Cek Resi</h3>

                                    @php
                                        $courier = request('courier');
                                        $waybill = request('waybill');
                                    @endphp

                                    <div class="form-group hide">
                                        {!! Form::text('_token', csrf_token(), [
                                            'class'=>'form-control',
                                        ]) !!}
                                    </div>

                                    <div class="form-group ">
                                        {!! Form::select('courier', $dropdown['couriers'], $courier, [
                                            'class'=>'form-control',
                                            'placeholder' => 'Pilih salah satu',
                                        ]) !!}
                                    </div>

                                    <div class="form-group ">
                                        {!! Form::text('waybill', $waybill, [
                                            'class'=>'form-control',
                                            'placeholder' => 'Masukkan Nomor Resi',
                                        ]) !!}
                                    </div>

                                    <button id="check_waybill" class="btn btn-primary" type="button">
                                        <i id="spinner-icon" class="fas fa-spinner fa-pulse hide"></i>
                                        Check
                                    </button>
                                    <button id="reset_waybill" class="btn btn-default" type="button">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-9">
                            <div id="delivery-status-error-message" class="hide">
                                <div class="alert alert-danger">
                                    <span>Message</span>
                                </div>
                            </div>

                            <div id="delivery-status-wrapper" class="hide">
                                <div class="box">
                                    <div class="box-content">
                                        <h4 class="mt-0">INFO</h4>

                                        <div class="row mb-3">
                                            <div class="col-sm-6 mb-3" id="delivery-status-info"> </div>
                                            <div class="col-sm-6 mb-3" id="delivery-status-status"> </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 mb-3" id="delivery-status-shipper"> </div>
                                            <div class="col-sm-6 mb-3" id="delivery-status-receiver"> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box">
                                    <div class="box-content">
                                        <h4 class="mt-0">DETAILS</h4>

                                        <table class="table table-bordered table-hover" id="delivery-status-details">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Note</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
