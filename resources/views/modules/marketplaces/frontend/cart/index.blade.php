@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Cart')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .product-qty-container {
            width: 120px;
        }
    </style>
@endpush

@push('js')
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/bootstrap-number-input/bootstrap-number-input.js" ></script>
    <script>
        $(document).ready(function() {
            $('body').on('click', '[data-action="delete-cart"]', function(event) {
                event.preventDefault();
                var formId = $(this).attr('data-form-id');
                var form = $('#'+formId);
                if(confirm("Delete cart?")){
                    form.submit();
                }
            });

            $('body').on('click', '[data-action="delete-cart-item"]', function(event) {
                event.preventDefault();
                var row = $(this).attr('data-row');
                var form = $(this).parents('form');
                if(confirm("Delete item?")){
                    form.find('[name="data['+row+'][qty]"]').val(0);
                    form.submit();
                }
            });

            $('.product-qty').bootstrapNumber();
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-normal')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">
                                Home
                            </a>
                        </li>
                        <li class="active">
                            Cart
                        </li>
                    </ol>
                    <h3>Cart</h3>

                    @include('flash::message')

                    @if ($carts && count($carts))
                        @foreach ($carts as $storeSlug => $cart)
                            @php
                                $currentStore = $stores[$storeSlug];
                                $marketplace = $currentStore->marketplace;
                                $subtotal = Cart::instance($storeSlug)->subtotal(0, ',', '.');
                                $i = 1;
                            @endphp
                            <div class="box">
                                <div class="box-content">
                                    {!! Form::open([
                                        'route' => ['cart.update', $storeSlug],
                                        'method' => 'PUT',
                                        'files' => false,
                                        ]) !!}

                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered mb-0">
                                                <thead>
                                                    <tr class="bg-info">
                                                        <th colspan="7">
                                                            <a href="{{ route('stores.show', [$marketplace->slug, $currentStore->slug]) }}">
                                                                {{ $currentStore->name }}
                                                            </a>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <thead>
                                                    <tr>
                                                        <td colspan="7">
                                                            <a class="btn btn-primary" href="{{ route('checkout.edit', [$currentStore->slug]) }}">Checkout</a>
                                                            <button type="submit" class="btn btn-primary">Update</button>
                                                            {!! Form::bs3Submit('Delete', [
                                                                'class'=>'btn btn-danger',
                                                                'data-action'=>'delete-cart',
                                                                'data-form-id'=>"form-{$currentStore->slug}",
                                                                ]); !!}
                                                        </td>
                                                    </tr>
                                                </thead>
                                                <thead>
                                                    <tr>
                                                        <th>Image</th>
                                                        <th>Product</th>
                                                        <th>Qty</th>
                                                        <th>Price</th>
                                                        <th>Subtotal</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($cart as $row)
                                                        <tr>
                                                            <td>
                                                                <img src="{{ asset($row->model->image) }}" width="100" height="100">
                                                            </td>
                                                            <td>{{ $row->name }}</td>
                                                            <td>
                                                                {!! Form::hidden("data[$i][rowId]", $row->rowId) !!}
                                                                <div class="form-group product-qty-container">
                                                                    {!! Form::number("data[$i][qty]", $row->qty, ['min'=>0, 'class'=>'form-control product-qty']) !!}
                                                                </div>

                                                            </td>
                                                            <td>{{ formatNumber($row->price) }}</td>
                                                            <td>{{ formatNumber($row->total) }}</td>
                                                            <td>
                                                                {!! Form::button('<i class="fa fa-trash"></i>', [
                                                                    'class'=>'btn btn-xs btn-danger',
                                                                    'data-action'=>'delete-cart-item',
                                                                    'data-row'=>$i,
                                                                ]); !!}
                                                            </td>
                                                        </tr>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="4">Subtotal</td>
                                                        <td>{{ $subtotal }}</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7">
                                                            <a class="btn btn-primary" href="{{ route('checkout.edit', [$currentStore->slug]) }}">Checkout</a>
                                                            <button type="submit" class="btn btn-primary">Update</button>
                                                            {!! Form::bs3Submit('Delete', [
                                                                'class'=>'btn btn-danger',
                                                                'data-action'=>'delete-cart',
                                                                'data-form-id'=>"form-{$currentStore->slug}",
                                                                ]); !!}
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>

                            {!! Form::open([
                                    'route' => ['cart.destroy', $currentStore->slug],
                                    'method' => 'DELETE',
                                    'style' => 'display: inline-block;',
                                    'id' => 'form-'.$currentStore->slug,
                                    'class' => 'm-0'
                                ]) !!}
                            {!! Form::close() !!}
                        @endforeach
                    @else
                        <div class="alert alert-warning">
                            Cart is empty
                        </div>
                    @endif
                </div>
            </div>
        </div>


    </div>

    @php
        $isHomepage = $useHomepageFooter;
    @endphp
    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
