@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Shipping Address')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script>
        $(document).ready(function() {
            // var provinces = @json($provinces);

            $('#shipping-address-existing').hide();
            $('#shipping-address-new').hide();

            function displayPaymentAddress() {
                var paymentAddress = $('input[name=shipping_address]').filter(':checked').val();

                if (paymentAddress == 'existing') {
                    $('#shipping-address-new').hide();
                    $('#shipping-address-existing').show();
                } else {
                    $('#shipping-address-existing').hide();
                    $('#shipping-address-new').show();
                }
            }

            displayPaymentAddress();
            $('[name="shipping_address"]').click(function(event) {
                displayPaymentAddress();
            });

            /*
            $('#province_id').on('change', function(event) {
                event.preventDefault();
                $('#city_id').prop('disabled', 'disabled');
                var provinceId = $(this).val();
                var output = '';
                output += '<option selected="selected" value="">Please Select</option>';
                $('#city_id').html(output);

                if (provinceId) {
                    var province = provinces[provinceId];
                    var cities = province['cities'];
                    for (var i in cities) {
                        var city = cities[i];
                        output += `<option value="${city['city_id']}">${city['type']} ${city['city_name']}</option>`;
                    }
                    $('#city_id').html(output);
                    $('#city_id').prop('disabled', false);
                }
            });
            */

            var provincesWithDetails = @json($provincesWithDetails);
            console.log(provincesWithDetails);

            $('#province_id').on('change', function(event) {
                event.preventDefault();
                $('#city_id').prop('disabled', 'disabled');
                var provinceId = $(this).val();

                var output = '';
                output += '<option selected="selected" value="">Please Select</option>';
                $('#city_id').html(output);
                $('#district_id').html(output);

                if (provinceId) {
                    var province = provincesWithDetails[provinceId];
                    var cities = province['cities_data'];
                    for (var i in cities) {
                        var city = cities[i];
                        output += `<option value="${city['id']}">${city['name']}</option>`;
                    }
                    $('#city_id').html(output);
                    $('#city_id').prop('disabled', false);
                }
            });

            $('#city_id').on('change', function(event) {
                event.preventDefault();
                $('#district_id').prop('disabled', 'disabled');
                var provinceId = $('#province_id').val();
                var cityId = $(this).val();

                var output = '';
                output += '<option selected="selected" value="">Please Select</option>';
                $('#district_id').html(output);

                if (cityId) {
                    var city = provincesWithDetails[provinceId]['cities_data'][cityId];
                    console.log(city);
                    var districts = city['districts_data'];
                    console.log(districts);
                    for (var i in districts) {
                        var district = districts[i];
                        output += `<option value="${district['id']}">${district['name']}</option>`;
                    }
                    $('#district_id').html(output);
                    $('#district_id').prop('disabled', false);
                }
            });

        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-store')

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cart.index') }}">
                                Cart
                            </a>
                        </li>
                        <li class="active">
                            Shipping Address
                        </li>
                    </ol>

                    <h3>Shipping Address</h3>

                    <div class="box">
                        <div class="box-content">
                            <a class="btn btn-default" href="{{ route('checkout.edit', [$store->slug]) }}">Step 1</a>
                            <a class="btn btn-default" href="{{ route('checkout.steps.billing-address.edit', [$store->slug]) }}">Step 2</a>
                            <a class="btn btn-primary" href="{{ route('checkout.steps.shipping-address.edit', [$store->slug]) }}">Step 3</a>
                            <a class="btn btn-default disabled" href="{{ route('checkout.steps.shipping-method.edit', [$store->slug]) }}">Step 4</a>
                            <a class="btn btn-default disabled" href="{{ route('checkout.steps.payment-method.edit', [$store->slug]) }}">Step 5</a>
                            <a class="btn btn-default disabled" href="{{ route('checkout.steps.confirm.show', [$store->slug]) }}">Step 6</a>
                        </div>
                    </div>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['checkout.steps.shipping-address.update', $store->slug],
                                'method' => 'PUT',
                                'files' => false,
                                ]) !!}

                                @if ($schoolHasAddress)
                                    <div class="radio">
                                        <label>
                                            {!! Form::radio('shipping_address', 'existing', $schoolHasAddress) !!}
                                            I want to use an existing address
                                        </label>
                                    </div>
                                    <div id="shipping-address-existing">
                                        {!! Form::bs3Select('address_id', $dropdown['schoolAddresses'], $selectedAddressId, ['label'=>'Address']) !!}
                                    </div>
                                @endif
                                <div class="radio">
                                    <label>
                                        {!! Form::radio('shipping_address', 'new', !$schoolHasAddress) !!}
                                        I want to use a new address
                                    </label>
                                </div>
                                <div id="shipping-address-new">
                                    {!! Form::bs3Text('label', 'School address') !!}
                                    {!! Form::bs3Text('full_name', $school->name) !!}
                                    {!! Form::bs3Text('company') !!}
                                    {!! Form::bs3Textarea('address') !!}
                                    {!! Form::bs3Text('phone') !!}
                                    {!! Form::bs3Text('postcode') !!}
                                    {!! Form::bs3Select('province_id', $dropdown['provinces'], null, ['label'=>'Province']) !!}
                                    {!! Form::bs3Select('city_id', $dropdown['cities'], null, ['label'=>'City']) !!}
                                    {!! Form::bs3Select('district_id', $dropdown['districts'], null, ['label'=>'District']) !!}

                                </div>


                                {!! Form::bs3Submit('Next'); !!}

                            {!! Form::close() !!}
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>

    @php
        $isHomepage = $useHomepageFooter;
    @endphp
    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-store-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
