@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Payment Method')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-store')

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cart.index') }}">
                                Cart
                            </a>
                        </li>
                        <li class="active">
                            Payment Method
                        </li>
                    </ol>

                    <h3>Payment Method</h3>

                    <div class="box">
                        <div class="box-content">
                            <a class="btn btn-default" href="{{ route('checkout.edit', [$store->slug]) }}">Step 1</a>
                            <a class="btn btn-default" href="{{ route('checkout.steps.billing-address.edit', [$store->slug]) }}">Step 2</a>
                            <a class="btn btn-default" href="{{ route('checkout.steps.shipping-address.edit', [$store->slug]) }}">Step 3</a>
                            <a class="btn btn-default" href="{{ route('checkout.steps.shipping-method.edit', [$store->slug]) }}">Step 4</a>
                            <a class="btn btn-primary" href="{{ route('checkout.steps.payment-method.edit', [$store->slug]) }}">Step 5</a>
                            <a class="btn btn-default disabled" href="{{ route('checkout.steps.confirm.show', [$store->slug]) }}">Step 6</a>
                        </div>
                    </div>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['checkout.steps.payment-method.update', $store->slug],
                                'method' => 'PUT',
                                'files' => false,
                                ]) !!}

                                {!! Form::bs3Text('payment_method', $marketplaceSettings['payment_bank_name'], ['readonly']) !!}
                                {!! Form::bs3Textarea('note', $note) !!}




                                {!! Form::bs3Submit('Next'); !!}

                            {!! Form::close() !!}
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>

    @php
        $isHomepage = $useHomepageFooter;
    @endphp
    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-store-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
