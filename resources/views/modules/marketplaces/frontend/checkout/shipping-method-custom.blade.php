@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Shipping Method')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script>
        $(document).ready(function() {
            function number_format(number, decimals, dec_point, thousands_sep) {
                var n = !isFinite(+number) ? 0 : +number,
                    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                    toFixedFix = function (n, prec) {
                        var k = Math.pow(10, prec);
                        return Math.round(n * k) / k;
                    },
                    s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
                if (s[0].length > 3) {
                    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
                }
                if ((s[1] || '').length < prec) {
                    s[1] = s[1] || '';
                    s[1] += new Array(prec - s[1].length + 1).join('0');
                }
                return s.join(dec);
            }

            var costs = @json($costs);
            console.log(costs);

            // group
            function displayGroupCourier() {
                var group = $('#group').val();
                if (group == 'rajaongkir') {
                    $('#normal-group-wrapper').show();
                    $('#custom-group-wrapper').hide();
                } else {
                    $('#normal-group-wrapper').hide();
                    $('#custom-group-wrapper').show();
                }
            }
            displayGroupCourier();
            $('#group').on('change', function(event) {
                event.preventDefault();
                displayGroupCourier();
            });

            // normal group
            function displayNormalCouriersServices() {
                var courier = $('#normal_courier').val();
                output = `
                    <tr>
                        <td colspan="5">Select courier</td>
                    </tr>
                `;
                $('#normal-courier-services tbody').html(output);

                if (courier) {
                    var cost = costs[courier][0];

                    var output = '';
                    for(var i in cost['costs']){

                        var service = cost['costs'][i];
                        var serviceName = service['service'];
                        var description = service['description'];
                        var serviceCost = service['cost'][0]['value'];
                        var estimate = service['cost'][0]['etd'];

                        serviceCost = number_format(serviceCost, 0, ',', '.');
                        estimate = estimate ? estimate : '?';
                        estimate = estimate.replace(/hari/gi, '');

                        output += `
                            <tr>
                                <td><input name="normal_service" type="radio" value="${serviceName}"></td>
                                <td>${courier.toUpperCase()}</td>
                                <td>
                                    ${serviceName} <br>
                                    ${description}
                                </td>
                                <td>${serviceCost}</td>
                                <td>${estimate} hari</td>
                            </tr>
                        `;
                    }
                } else {
                    output = `
                        <tr>
                            <td colspan="5">Select courier</td>
                        </tr>
                    `;
                }
                $('#normal-courier-services tbody').html(output);
            }
            displayNormalCouriersServices();
            $('#normal_courier').on('change', function(event) {
                event.preventDefault();
                displayNormalCouriersServices();
            });

            var selectedService = '{{ $selectedService }}';
            if (selectedService) {
                $('input:radio[value="'+selectedService+'"]').prop('checked', true);
            }

            $('body').on('click', '#normal-courier-services tr', function(event) {
                event.preventDefault();
                var row = $(this);
                row.find('input:radio').prop('checked', true);
            });

            // form submit
            $('#submit-button').on('click', function(event) {
                event.preventDefault();
                console.log('submitted');
                let group = $('#group').val();
                if (group == 'rajaongkir') {
                    let courier = $('#normal_courier').val();
                    let service = $('input[name="normal_service"]:checked', '#shipping-method-form').val()
                    $('#courier').val(courier);
                    $('#service').val(service);
                } else {
                    let courier = $('#custom_courier').val();
                    $('#courier').val(courier);
                    $('#service').val('-');
                }

                let courier = $('#courier').val();
                let service = $('#service').val();

                $('#shipping-method-form').submit();
            });

        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-store')

    <div class="page-content">
        <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cart.index') }}">
                                Cart
                            </a>
                        </li>
                        <li class="active">
                            Shipping Method
                        </li>
                    </ol>

                        <h3>Shipping Method</h3>

                        <div class="box">
                            <div class="box-content">
                                <a class="btn btn-default" href="{{ route('checkout.edit', [$store->slug]) }}">Step 1</a>
                                <a class="btn btn-default" href="{{ route('checkout.steps.billing-address.edit', [$store->slug]) }}">Step 2</a>
                                <a class="btn btn-default" href="{{ route('checkout.steps.shipping-address.edit', [$store->slug]) }}">Step 3</a>
                                <a class="btn btn-primary" href="{{ route('checkout.steps.shipping-method.edit', [$store->slug]) }}">Step 4</a>
                                <a class="btn btn-default disabled" href="{{ route('checkout.steps.payment-method.edit', [$store->slug]) }}">Step 5</a>
                                <a class="btn btn-default disabled" href="{{ route('checkout.steps.confirm.show', [$store->slug]) }}">Step 6</a>
                            </div>
                        </div>

                        @include('flash::message')

                        <div class="box">
                            <div class="box-content">
                                {!! Form::open([
                                    'route' => ['checkout.steps.shipping-method.update', $store->slug],
                                    'method' => 'PUT',
                                    'files' => false,
                                    'id' => 'shipping-method-form'
                                    ]) !!}


                                    {!! Form::bs3Select('group', $dropdown['groups'], $selectedGroup) !!}

                                    <div class="hide">
                                        {!! Form::bs3Text('courier') !!}
                                        {!! Form::bs3Text('service') !!}
                                    </div>

                                    <div class="mb-3" id="normal-group-wrapper">
                                        {!! Form::bs3Select('normal_courier', $dropdown['couriers'], $selectedCourier) !!}
                                        <div id="normal-courier-services">
                                            <table class="table table-condensed table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Courier</th>
                                                        <th>Service</th>
                                                        <th>Price</th>
                                                        <th>Est</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="5">Select courier</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            @if ($errors->has('service'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('service') }}</strong>
                                                </span>
                                                <br><br>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="mb-3" id="custom-group-wrapper">
                                        <div class="alert alert-info">
                                            Biaya Pengiriman Kustom akan tertera Rp. 0.
                                            Dan akan berubah saat dikonfirmasi oleh Penyedia
                                        </div>

                                        {!! Form::bs3Select('custom_courier', $dropdown['customCouriers'], $selectedCourier) !!}
                                    </div>



                                    {!! Form::bs3Submit('Next', ['id'=>'submit-button']); !!}

                                {!! Form::close() !!}
                            </div>
                        </div>




                    </div>
                </div>
        </div>
    </div>

    @php
        $isHomepage = $useHomepageFooter;
    @endphp
    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-store-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
