@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Register')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style>
        [data-role="upload-image-wrapper"] [data-role="preview"] img {
            max-height: 150px;
            margin-bottom: 16px;
        }

        .border.border-danger {
            border: 1px solid red;
        }
    </style>
@endpush

@push('js')
    <script>
        $(document).ready(function () {
            $('[data-role="upload-image" ]').on('click', function (e) {
                e.preventDefault();
                var target = $(this).attr('data-target');
                $(target).click();
            });

            $('[data-role="upload-image-wrapper"]').on('change', '[type="file"]', function (e) {
                e.preventDefault();
                console.log('changed')

                var input = this;
                var parent = $(this).parents('[data-role="upload-image-wrapper"]');
                // data-role="preview"
                var uploadImgBtn = $(parent).find('[data-role="upload-image"]');
                uploadImgBtn.html('Change Image');

                var previewElm = $(parent).find('[data-role="preview"]');
                readURL(input, previewElm);
            });

            function readURL(input, previewElm) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        var img = e.target.result;

                        var isImage = checkImage(img);
                        if (!isImage) {
                            alert('please upload valid image');
                            return false;
                        }

                        var output = `
                            <img src="${img}" class="img-thumbnail">
                        `;

                        previewElm.html('');
                        previewElm.html(output);
                    }

                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                }
            }

            function checkImage(str) {
                var types = [
                    'data:image/png;base64',
                    'data:image/jpeg;base64',
                ]

                for (let i = 0; i < types.length; i++) {
                    const type = types[i];
                    var isImage = str.includes(type);
                    if (isImage) {
                        return true;
                    }
                }

                return false;
            }

            checkSubmitFormBtn();
            $('[name="setuju_ketentuan_penjual"]').on('change', function () {
                checkSubmitFormBtn();
            });

            function checkSubmitFormBtn() {
                var elm = $('[name="setuju_ketentuan_penjual"]');
                var submitBtn = $("#submit-form-btn");
                var isChecked = elm.is(":checked");

                if (isChecked) {
                    submitBtn.removeAttr('disabled');
                }
                if (!isChecked) {
                    submitBtn.attr('disabled', true);
                }
            }



        });
    </script>
    <script>
        $(document).ready(function () {
            $('#nama_lengkap').on('change', function () {
                var value = $(this).val();

                var namaLengkapPenanggungjawabElm = $('#nama_lengkap_penanggungjawab');
                var namaPemilikRekeningElm = $('#nama_pemilik_rekening');

                if (value) {
                    console.log('changed')
                    if (!namaLengkapPenanggungjawabElm.val()) {
                        namaLengkapPenanggungjawabElm.val(value);
                    }

                    if (!namaPemilikRekeningElm.val()) {
                        namaPemilikRekeningElm.val(value);
                    }
                }

                if (value == '') {
                    console.log('changed')
                    namaLengkapPenanggungjawabElm.val('');
                    namaPemilikRekeningElm.val('');
                }
            });

            $('#email').on('change', function () {
                var value = $(this).val();

                var emailPenanggungjawabElm = $('#email_penanggungjawab');

                if (value) {
                    console.log('changed')
                    if (!emailPenanggungjawabElm.val()) {
                        emailPenanggungjawabElm.val(value);
                    }
                }

                if (value == '') {
                    console.log('changed')
                    emailPenanggungjawabElm.val('');
                }
            });

            $('#jabatan').on('change', function () {
                var value = $(this).val();

                var jabatanPenanggungjawabElm = $('#jabatan_penanggungjawab');

                if (value) {
                    console.log('changed')
                    if (!jabatanPenanggungjawabElm.val()) {
                        jabatanPenanggungjawabElm.val(value);
                    }
                }

                if (value == '') {
                    console.log('changed')
                    jabatanPenanggungjawabElm.val('');
                }
            });

            $('#nik').on('change', function () {
                var value = $(this).val();

                var nikPenanggungjawabElm = $('#nik_penanggungjawab');

                if (value) {
                    console.log('changed')
                    if (!nikPenanggungjawabElm.val()) {
                        nikPenanggungjawabElm.val(value);
                    }
                }

                if (value == '') {
                    console.log('changed')
                    nikPenanggungjawabElm.val('');
                }
            });
        });
    </script>
@endpush

@push('js')
    <script>
        $(document).ready(function() {
            var provincesWithDetails = @json($provincesWithDetails);
            console.log(provincesWithDetails);

            $('#province_id').on('change', function(event) {
                event.preventDefault();
                $('#city_id').prop('disabled', 'disabled');
                var provinceId = $(this).val();

                var output = '';
                output += '<option selected="selected" value="">Please Select</option>';
                $('#city_id').html(output);
                $('#district_id').html(output);

                if (provinceId) {
                    var province = provincesWithDetails[provinceId];
                    var cities = province['cities_data'];
                    for (var i in cities) {
                        var city = cities[i];
                        output += `<option value="${city['id']}">${city['name']}</option>`;
                    }
                    $('#city_id').html(output);
                    $('#city_id').prop('disabled', false);
                }
            });

            $('#city_id').on('change', function(event) {
                event.preventDefault();
                $('#district_id').prop('disabled', 'disabled');
                var provinceId = $('#province_id').val();
                var cityId = $(this).val();

                var output = '';
                output += '<option selected="selected" value="">Please Select</option>';
                $('#district_id').html(output);

                if (cityId) {
                    var city = provincesWithDetails[provinceId]['cities_data'][cityId];
                    console.log(city);
                    var districts = city['districts_data'];
                    console.log(districts);
                    for (var i in districts) {
                        var district = districts[i];
                        output += `<option value="${district['id']}">${district['name']}</option>`;
                    }
                    $('#district_id').html(output);
                    $('#district_id').prop('disabled', false);
                }
            });

        });
    </script>
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/leaflet/leaflet.css') }}">
    <style>
        #main-map { height: 200px; margin-bottom: 20px; }
    </style>
@endpush

@push('js')
    <script src="{{ asset('assets/plugins/leaflet/leaflet.js') }}"></script>
    <script>
        $(document).ready(function () {
            var defaultLatLng = {
                lat: -6.2088,
                lng: 106.8456,
            }

            var oldLat = $('#latitude').val();
            var oldLng = $('#longitude').val();
            if (oldLat && oldLng) {
                defaultLatLng.lat = oldLat;
                defaultLatLng.lng = oldLng;
            }

            var mainMap = L.map('main-map').setView(defaultLatLng, 13);

            var openstreetmap = {}
            openstreetmap = {
                url : 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                options : {
                    maxZoom: 19,
                    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                }
            }

            L.tileLayer(openstreetmap.url, openstreetmap.options).addTo(mainMap);

            var marker = L.marker(defaultLatLng).addTo(mainMap);
            setMarkerPopup(marker)

            function setMapView(map, lat, lng, zoom = 13) {
                map.setView([lat, lng], zoom);
            }

            function setMarkerPopup(marker) {
                var latlng = marker.getLatLng();
                var lat = latlng.lat.toFixed(2)
                var lng = latlng.lng.toFixed(2)
                marker.bindPopup(`${lat}, ${lng}`);
            }

            function setMarkerPosition(marker, lat, lng) {
                marker.setLatLng([lat, lng]);
            }

            function setGeolocationInput(lat, lng) {
                var lat = lat.toFixed(8);
                var lng = lng.toFixed(8);
                $('#latitude').val(lat);
                $('#longitude').val(lng);
            }

            function onMapClick(e) {
                var latlng = e.latlng;
                setMarkerPosition(marker, latlng.lat, latlng.lng);
                setMarkerPopup(marker);
                setGeolocationInput(latlng.lat, latlng.lng);
            }

            mainMap.on('click', onMapClick);

            $('#get-location-btn').on('click', function () {
                getLocation()
            });

            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else {
                    alert("Geolocation is not supported by this browser.")
                }
            }

            function showPosition(position) {
                var latlng = []
                latlng.lat = position.coords.latitude;
                latlng.lng = position.coords.longitude;

                setMapView(mainMap, latlng.lat, latlng.lng);
                setMarkerPosition(marker, latlng.lat, latlng.lng);
                setMarkerPopup(marker);
                setGeolocationInput(latlng.lat, latlng.lng);
            }


        });
    </script>
@endpush


@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">

                    <div class="box">
                        <div class="box-content">
                            <div class="auth-form">
                                {!! Form::open([
                                    'url' => route('marketplaces.stores.register.store'),
                                    'method' => 'POST',
                                    'files' => true,
                                    ]) !!}

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <span>{{ $error }}</span>
                                                <br>
                                            @endforeach
                                        </div>
                                    @endif

                                    @include('flash::message')

                                    <h3 class="text-primary">
                                        Login Pengguna
                                    </h3>

                                    {!! Form::bs3Text('nama_lengkap', null, ['label' => 'Nama Lengkap *']) !!}
                                    {!! Form::bs3Text('email', null, ['label' => 'Email *']) !!}
                                    {!! Form::bs3Text('username', null, ['label' => 'Username *']) !!}
                                    {!! Form::bs3Password('password', ['label' => 'Password *']) !!}
                                    {!! Form::bs3Password('password_confirmation', ['label' => 'Ulangi Password *']) !!}
                                    {!! Form::bs3Text('jabatan', null, ['label' => 'Jabatan *']) !!}
                                    {!! Form::bs3Text('nik', null, ['label' => 'NIK *']) !!}
                                    {!! Form::bs3Text('no_hp', null, ['label' => 'No Handphone *']) !!}
                                    {!! Form::bs3Text('no_tel_kantor', null, ['label' => 'No Telepon Kantor *']) !!}

                                    <hr>

                                    <h3 class="text-primary">
                                        Toko
                                    </h3>

                                    <h5 class="text-primary">
                                        Profile
                                    </h5>

                                    {!! Form::bs3Text('nama_toko', null, ['label' => 'Nama Toko *']) !!}
                                    {!! Form::bs3Select('jenis_usaha', $dropdown['jenisUsaha'], null, ['label' => 'Jenis Usaha *']) !!}
                                    {!! Form::bs3Select('kelas_usaha', $dropdown['kelasUsaha'], null, ['label' => 'Kelas Usaha *']) !!}
                                    {!! Form::bs3Select('status', $dropdown['statusUsaha'], null, ['label' => 'Status *']) !!}

                                    {!! Form::bs3Text('npwp', null, ['label' => 'NPWP *']) !!}
                                    {!! Form::bs3Text('siup_nib', null, ['label' => 'SIUP/NIB']) !!}
                                    {!! Form::bs3Text('tdp', null, ['label' => 'TDP']) !!}

                                    <hr>

                                    {{-- {!! Form::bs3Text('kab_kota', null, ['label' => 'Kab/Kota *']) !!} --}}
                                    {!! Form::bs3Select('province_id', $dropdown['provinces'], null, ['label'=>'Provinsi *']) !!}
                                    {!! Form::bs3Select('city_id', $dropdown['cities'], null, ['label'=>'Kab / Kota *']) !!}
                                    {!! Form::bs3Select('district_id', $dropdown['districts'], null, ['label'=>'Kecamatan *']) !!}

                                    {!! Form::bs3Textarea('alamat', null, ['label' => 'Alamat *']) !!}

                                    <div class="map-wrapper">
                                        <div id="main-map"></div>
                                        <div class="mb-3">
                                            <span class="btn btn-sm btn-default" id="get-location-btn">Get your location</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            {!! Form::bs3Number('latitude', null, ['label' => 'Latitude *', 'step' => 'any']) !!}
                                        </div>
                                        <div class="col-sm-6">
                                            {!! Form::bs3Number('longitude', null, ['label' => 'Longitude *', 'step' => 'any']) !!}
                                        </div>
                                    </div>

                                    {!! Form::bs3Text('kode_pos', null, ['label' => 'Kode Pos *']) !!}
                                    {!! Form::bs3Text('email_toko', null, ['label' => 'Email Toko *']) !!}

                                    <hr>

                                    <h5 class="text-primary">
                                        Penanggungjawab / Penandatangan
                                    </h5>

                                    {!! Form::bs3Text('nama_lengkap_penanggungjawab', null, ['label' => 'Nama Lengkap *']) !!}
                                    {!! Form::bs3Text('email_penanggungjawab', null, ['label' => 'Email *']) !!}
                                    {!! Form::bs3Text('jabatan_penanggungjawab', null, ['label' => 'Jabatan *']) !!}
                                    {!! Form::bs3Text('nik_penanggungjawab', null, ['label' => 'NIK/KTP *']) !!}

                                    <br>

                                    <h3 class="text-primary">
                                        Bank
                                    </h3>

                                    {!! Form::bs3Text('nama_bank', null, ['label' => 'Nama Bank *']) !!}
                                    {!! Form::bs3Text('nama_pemilik_rekening', null, ['label' => 'Nama Pemilik Rekening *']) !!}
                                    {!! Form::bs3Text('no_rekening', null, ['label' => 'Nomor Rekening *']) !!}
                                    {!! Form::bs3Text('cabang_bank', null, ['label' => 'Cabang Bank *']) !!}

                                    <hr>

                                    <h3 class="text-primary">
                                        Scan Document
                                    </h3>

                                    <div class="mb-3">
                                        <span>
                                            Gunakan file gambar berekstensi <strong>.png</strong> atau <strong>.jpg</strong>, maksimum ukuran 1 MB
                                        </span>
                                    </div>

                                    <div class="text-center">
                                        <label for="scan_ktp_penanggung_jawab" class="control-label">
                                            KTP Penanggungjawab
                                        </label>
                                        <div class="well text-center {{ $errors->has('scan_ktp_penanggung_jawab') ? ' border border-danger' : '' }}"
                                            data-role="upload-image-wrapper">
                                            <div data-role="preview">
                                            </div>
                                            <button class="btn btn-secondary"
                                                data-role="upload-image"
                                                data-target="#scan_ktp_penanggung_jawab">
                                                Upload Image
                                            </button>
                                            <div class="hidden">
                                                {!! Form::file('scan_ktp_penanggung_jawab', [
                                                    'id' => 'scan_ktp_penanggung_jawab',
                                                    'accept' => "image/*"
                                                ]) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <label for="scan_npwp" class="control-label">
                                            NPWP
                                        </label>
                                        <div class="well text-center  {{ $errors->has('scan_npwp') ? ' border border-danger' : '' }}"
                                            data-role="upload-image-wrapper">
                                            <div data-role="preview">
                                            </div>
                                            <button class="btn btn-secondary"
                                                data-role="upload-image"
                                                data-target="#scan_npwp">
                                                Upload Image
                                            </button>
                                            <div class="hidden">
                                                {!! Form::file('scan_npwp', [
                                                    'id' => 'scan_npwp',
                                                    'accept' => "image/*"
                                                ]) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <label for="scan_siup" class="control-label">
                                            SIUP/NIB
                                        </label>
                                        <div class="well text-center  {{ $errors->has('scan_siup') ? ' border border-danger' : '' }}"
                                            data-role="upload-image-wrapper">
                                            <div data-role="preview">
                                            </div>
                                            <button class="btn btn-secondary"
                                                data-role="upload-image"
                                                data-target="#scan_siup">
                                                Upload Image
                                            </button>
                                            <div class="hidden">
                                                {!! Form::file('scan_siup', [
                                                    'id' => 'scan_siup',
                                                    'accept' => "image/*"
                                                ]) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <label for="scan_tdp" class="control-label">
                                            TDP
                                        </label>
                                        <div class="well text-center  {{ $errors->has('scan_tdp') ? ' border border-danger' : '' }}"
                                            data-role="upload-image-wrapper">
                                            <div data-role="preview">
                                            </div>
                                            <button class="btn btn-secondary"
                                                data-role="upload-image"
                                                data-target="#scan_tdp">
                                                Upload Image
                                            </button>
                                            <div class="hidden">
                                                {!! Form::file('scan_tdp', [
                                                    'id' => 'scan_tdp',
                                                    'accept' => "image/*"
                                                ]) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <label style="font-weight: normal">
                                            <input type="checkbox" name="setuju_ketentuan_penjual">
                                            Saya Setuju terhadap
                                            <a href="#" target="_blank">Ketentuan Penjual</a>
                                        </label>
                                    </div>

                                    <button class="btn btn-primary" type="submit" id="submit-form-btn">
                                        Daftar
                                    </button>

                                {!! Form::close() !!}

                                <hr>




                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
