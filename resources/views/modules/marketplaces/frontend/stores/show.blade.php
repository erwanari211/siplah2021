@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    $templatePageTitle = '{store} | {APP}';
    $templatePageDescription = 'Get products you need from {store}. Find smiliar product at {marketplace}. Fast Delivery and secure payment at {APP}.';
    $searchArray = ['{store}', '{marketplace}', '{APP}'];
    $replaceArray = [$store->name, $marketplace->name, env('APP_NAME')];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .hr-banner {
            margin: 10px 0;
            border-color: #ddd;
        }
    </style>
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#store-category-sidebar').metisMenu({
                toggle: false,
            });

            $('.product-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin:15,
                stagePadding: 50,
                nav:true,
                navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                autoHeight:true,
                lazyLoad:true,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        stagePadding: 80,
                    },
                    480:{
                        items:2,
                        nav:false,
                    },
                    768:{
                        items:3
                    },
                    992:{
                        items:4
                    },
                    1200:{
                        items:4
                    }
                },
            });

            $('.banner-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin:15,
                stagePadding: 50,
                nav:true,
                navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                autoHeight:true,
                lazyLoad:true,
                // responsiveClass:true,
                // responsive:{
                //     0:{
                //         items:1,
                //         nav:false,
                //         stagePadding: 80,
                //     },
                //     480:{
                //         items:2,
                //         nav:false,
                //     },
                //     768:{
                //         items:3
                //     },
                //     992:{
                //         items:4
                //     },
                //     1200:{
                //         items:4
                //     }
                // },
            });



        });
    </script>
@endpush

@include('modules.marketplaces.frontend.stores.inc.store-info-script')

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-store')

    <div class="page-content">
        @include('modules.marketplaces.frontend.marketplaces.inc.marketplace-inactive-message')
        @include('modules.marketplaces.frontend.stores.inc.store-inactive-message')

        <div class="mb-3">
            <div class="container">
                @include('modules.marketplaces.frontend.stores.inc.store-info')
                @include('modules.marketplaces.frontend.stores.inc.store-menu-navbar')
            </div>
        </div>

        @if (!$hasDefaultStoreAddress)
            <div class="container">
                <div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    This store currently has no address. So we cant calculate shipping cost.
                </div>
            </div>
        @endif

        <div class="container">
            @php
                $homeTabActive = false;
                if (count($bannerGroups) OR count($storeFeaturedProducts)) {
                    $homeTabActive = true;
                }
            @endphp
            <div role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs border-bottom-nav" role="tablist">
                    <li role="presentation" class="{{ $homeTabActive ? 'active' : '' }}">
                        <a href="#tab-home" aria-controls="tab-home" role="tab" data-toggle="tab">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab-collections" aria-controls="tab-collections" role="tab" data-toggle="tab">Collections</a>
                    </li>
                    <li role="presentation" class="{{ $homeTabActive ? '' : 'active' }}">
                        <a href="#tab-products" aria-controls="tab-products" role="tab" data-toggle="tab">Products</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab-notes" aria-controls="tab-notes" role="tab" data-toggle="tab">Notes</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab-discussions" aria-controls="tab-discussions" role="tab" data-toggle="tab">Discussions</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab-reviews" aria-controls="tab-reviews" role="tab" data-toggle="tab">Reviews</a>
                    </li>
                </ul>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane pt-4 {{ $homeTabActive ? 'active' : '' }}" id="tab-home">
                    @if (isset($storeSettings['display_collection_in_home_tab']) && $storeSettings['display_collection_in_home_tab'])
                        @if (count($categories))
                            <div class="row store__collections store__collections--icon-box">
                                @foreach ($categories as $category)
                                    <div class="col-xs-6 col-sm-3 col-md-2">
                                        <div class="box">
                                            <div class="box-content">
                                                <div class="icon-box">
                                                    <a class="icon-box-link"
                                                        href="{{ route('stores.categories.show', [$marketplace->slug, $store->slug, $category->slug]) }}">
                                                        <div class="icon-box-icon-container">
                                                            <img class="icon-box-image lazy" data-src="{{ asset($category->image) }}">
                                                        </div>
                                                        <div class="icon-box-content">
                                                            <span class="icon-box-title line-clamp-2">{{ $category->name }}</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    @endif

                    @if ($bannerGroups)
                        @foreach ($bannerGroups as $group)
                            <div class="row">
                                @if ($group->display_name)
                                    <div class="col-sm-12">
                                        <h3>{{ $group->display_name }}</h3>
                                    </div>
                                @endif
                            </div>
                            @if ($group->type == 'normal')
                                <div class="row">
                                    @foreach ($group->details as $detail)
                                        @php
                                            $banner = $detail->storeBanner;
                                        @endphp
                                        <div class="col-sm-6">
                                            <div class="box">
                                                <div class="box-content no-padding">
                                                    @if ($banner->link)
                                                        <a href="{{ $banner->link }}">
                                                            <img class="lazy" data-src="{{ asset($banner->image) }}" width="100%">
                                                        </a>
                                                    @else
                                                        <img class="lazy" data-src="{{ asset($banner->image) }}" width="100%">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                            @if ($group->type == 'slider')
                                <div class="banner-carousel owl-carousel owl-theme">
                                    @foreach ($group->details as $detail)
                                        @php
                                            $banner = $detail->storeBanner;
                                        @endphp
                                        <div class="item">
                                            <div class="box">
                                                <div class="box-content no-padding">
                                                    @if ($banner->link)
                                                        <a href="{{ $banner->link }}">
                                                            <img class="owl-lazy" data-src="{{ asset($banner->image) }}" width="100%">
                                                        </a>
                                                    @else
                                                        <img class="owl-lazy" data-src="{{ asset($banner->image) }}" width="100%">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                            <hr class="hr-banner">
                        @endforeach
                    @endif

                    @if (count($storeFeaturedProducts))
                        <div class="row">
                            @foreach ($storeFeaturedProducts as $featuredProduct)
                                @if (count($featuredProduct->details))
                                    <div class="col-sm-12">
                                        <h3>{{ $featuredProduct->name }}</h3>
                                        <div class="product-carousel owl-carousel owl-theme products products-grid">
                                            @foreach ($featuredProduct->details as $productDetail)
                                                @php
                                                    $product = $productDetail->product;
                                                @endphp
                                                @php
                                                    $productMetas = $product->getAllMeta();
                                                    $originalPrice = isset($productMetas['original_price']) ? $productMetas['original_price'] : 0;
                                                    $discountAmountPercent = $product->discountAmountPercent;
                                                @endphp
                                                <div class="item">
                                                    <div class="product">
                                                        @if ($discountAmountPercent)
                                                            <span class="product__label right">
                                                                <div class="text-center">
                                                                {{ $discountAmountPercent }}%<br>OFF
                                                                </div>
                                                            </span>
                                                        @endif
                                                        <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">
                                                            <div class="product__image-wrapper">
                                                                <img class="product__image owl-lazy" data-src="{{ asset($product->image) }}">
                                                            </div>
                                                        </a>
                                                        <span class="product__name">
                                                            <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">
                                                                {{ $product->name }}
                                                            </a>
                                                        </span>
                                                        <div class="product__price">
                                                            @if ($originalPrice)
                                                                <span class="product__original-price">{{ "Rp.".formatNumber($originalPrice) }}</span>
                                                            @endif
                                                            <span class="product__current-price">{{ "Rp.".formatNumber($product->price) }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @endif


                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane pt-4" id="tab-collections">
                    <div class="row store__collections store__collections--icon-box">
                        @if (count($categories))
                            @foreach ($categories as $category)
                                <div class="col-xs-6 col-sm-3 col-md-2">
                                    <div class="box">
                                        <div class="box-content">
                                            <div class="icon-box">
                                                <a class="icon-box-link"
                                                    href="{{ route('stores.categories.show', [$marketplace->slug, $store->slug, $category->slug]) }}">
                                                    <div class="icon-box-icon-container">
                                                        <img class="icon-box-image lazy" data-src="{{ asset($category->image) }}">
                                                    </div>
                                                    <div class="icon-box-content">
                                                        <span class="icon-box-title line-clamp-2">{{ $category->name }}</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-sm-12">
                                <span>This store have no collection</span>
                            </div>
                        @endif
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane pt-4  {{ $homeTabActive ? '' : 'active' }}" id="tab-products">
                    <div class="products products-grid row">
                        @if (count($products))
                            @foreach ($products as $product)
                                @php
                                    $productMetas = $product->getAllMeta();
                                    $originalPrice = isset($productMetas['original_price']) ? $productMetas['original_price'] : 0;
                                    $discountAmountPercent = $product->discountAmountPercent;
                                @endphp
                                <div class="col-md-3 col-sm-4 col-xs-6">
                                    <div class="product">
                                        @if ($discountAmountPercent)
                                            <span class="product__label right">
                                                <div class="text-center">
                                                {{ $discountAmountPercent }}%<br>OFF
                                                </div>
                                            </span>
                                        @endif
                                        <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">
                                            <div class="product__image-wrapper">
                                                <img class="product__image lazy" data-src="{{ $product->image_url }}">
                                            </div>
                                        </a>
                                        <strong class="product__name">
                                            <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">
                                                {{ $product->name }}
                                            </a>
                                        </strong>

                                        @php
                                            $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
                                        @endphp

                                        @if (!$hasKemdikbudZonePrice)

                                        <div class="product__price">
                                            @if ($originalPrice)
                                                <span class="product__original-price">{{ "Rp.".formatNumber($originalPrice) }}</span>
                                            @endif
                                            <span class="product__current-price">{{ "Rp.".formatNumber($product->price) }}</span>
                                        </div>

                                        @endif

                                        @if ($hasKemdikbudZonePrice)
                                            @php
                                                $price = 0;
                                                $minPrice = 0;
                                                $maxPrice = 0;

                                                $mappedKemdikbudZonePrices = $product->getMappedKemdikbudZonePrices();
                                                $kemdikbudMinMaxZonePrice = $product->getKemdikbudMinMaxZonePrices($mappedKemdikbudZonePrices);
                                                $minPrice = $kemdikbudMinMaxZonePrice['minPrice'];
                                                $maxPrice = $kemdikbudMinMaxZonePrice['maxPrice'];
                                                $formattedZonePrice = $product->getKemdikbudFormattedZonePrices($minPrice, $maxPrice);
                                            @endphp
                                            <div class="product__price">
                                                <span class="product__current-price">{{ $formattedZonePrice }}</span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-sm-12">
                                No Product
                            </div>
                        @endif
                    </div>

                    @if ($totalProducts > 12)
                        <a class="btn btn-default" href="{{ route('products.index', [$marketplace->slug, $store->slug]) }}">See more</a>
                    @endif

                </div>
                <div role="tabpanel" class="tab-pane pt-4" id="tab-notes">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box">
                                <div class="box-content">
                                    <h3 class="mt-0">Recent Notes</h3>

                                    @if (count($storeNotes))
                                        @foreach ($storeNotes as $note)
                                            <strong>
                                                <a href="{{ route('stores.notes.show', [$marketplace->slug, $store->slug, $note->id]) }}" style="margin-right: 20px;">
                                                    {{ $note->title }}
                                                </a>
                                            </strong>
                                            <br>
                                            <small>
                                                <i class="fa fa-clock"></i>
                                                {{ $note->created_at->format('Y-m-d') }}
                                            </small>
                                            <hr class="mt-4 mb-2">
                                        @endforeach
                                        @php
                                            $countNote = count($storeNotes);
                                            $totalNote = $storeNotes->total();
                                        @endphp
                                        @if ($totalNote > $countNote)
                                            <a class="btn btn-default btn-xs"
                                                href="{{ route('stores.notes.index', [$marketplace->slug, $store->slug]) }}">
                                                See More
                                            </a>
                                        @endif
                                    @else
                                        No Note
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="mb-4"></div>
                </div>
                <div role="tabpanel" class="tab-pane pt-4" id="tab-discussions">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box">
                                <div class="box-content">
                                    <h3 class="mt-0">Recent Discussions</h3>

                                    <div class="product__discussions">
                                        @if (count($storeDiscussions))
                                            @foreach ($storeDiscussions as $discussion)
                                                <div class="product__discussion">
                                                    <div class="media">
                                                        <a class="pull-left" href="{{ route('products.show', [$marketplace->slug, $store->slug, $discussion->product->slug]) }}">
                                                            <img class="media-object lazy "
                                                                data-src="{{ $discussion->product->image_url }}" alt="Image" height="50">
                                                        </a>
                                                        <div class="media-body">
                                                            <span class="media-heading">
                                                                {{ $discussion->user->name }}
                                                                on
                                                                <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $discussion->product->slug]) }}">
                                                                    {{ $discussion->product->name }}
                                                                </a>
                                                            </span>
                                                            <br>
                                                            <small class="text-muted">
                                                                <i class="fa fa-clock"></i>
                                                                <em>{{ $discussion->created_at }}</em>
                                                            </small>
                                                            <p>{{ $discussion->content }}</p>
                                                            <span class="btn btn-default btn-xs">
                                                                <small>
                                                                    <i class="fa fa-comments"></i>
                                                                    {{ $discussion->details_count }}
                                                                </small>
                                                            </span>
                                                            <a class="btn btn-default btn-xs" href="{{ route('products.discussions.show', [$marketplace->slug, $store->slug, $discussion->product->slug, $discussion->id]) }}">
                                                                View Discussion
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                            @endforeach
                                            @if ($storeDiscussions->total() > 5)
                                                <a class="btn btn-default btn-xs"
                                                    href="{{ route('stores.product-discussions.index', [$marketplace->slug, $store->slug]) }}">
                                                    See more discussions
                                                </a>
                                            @endif
                                        @else
                                            <p>No discussion</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="mb-4"></div>
                </div>
                <div role="tabpanel" class="tab-pane pt-4" id="tab-reviews">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box">
                                <div class="box-content">
                                    <h3 class="mt-0">Recent Reviews</h3>

                                    <div class="product__discussions">
                                        @if (count($storeReviews))
                                            @foreach ($storeReviews as $review)
                                                <div class="product__discussion">
                                                    <div class="media">
                                                        <a class="pull-left" href="{{ route('products.show', [$marketplace->slug, $store->slug, $review->product->slug]) }}">
                                                            <img class="media-object lazy"
                                                                data-src="{{ $review->product->image_url }}" alt="Image" height="50">
                                                        </a>
                                                        <div class="media-body">
                                                            <span class="media-heading">
                                                                {{ $review->user->name }}
                                                                on
                                                                <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $review->product->slug]) }}">
                                                                    {{ $review->product->name }}
                                                                </a>
                                                            </span>
                                                            <br>
                                                            <small class="text-muted">
                                                                <i class="fa fa-clock"></i>
                                                                <em>{{ $review->updated_at }}</em>
                                                            </small>
                                                            <div class="mt-3">
                                                                <select class="rating" data-initial-rating="{{ $review->rating }}">
                                                                  <option value="">0</option>
                                                                  <option value="1">1</option>
                                                                  <option value="2">2</option>
                                                                  <option value="3">3</option>
                                                                  <option value="4">4</option>
                                                                  <option value="5">5</option>
                                                                </select>
                                                            </div>
                                                            <p>{{ $review->content }}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                            @endforeach
                                            @if ($storeReviews->total() > 5)
                                                <a class="btn btn-default btn-xs"
                                                    href="{{ route('stores.product-reviews.index', [$marketplace->slug, $store->slug]) }}">
                                                    See more reviews
                                                </a>
                                            @endif
                                        @else
                                            <p>No review</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="mb-4"></div>
                </div>
            </div>
        </div>
    </div>

    @include('modules.website.frontend.homepage.v01.inc.footer')



    @include('modules.marketplaces.frontend.inc.sidebar-store-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
