@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    $templatePageTitle = 'Discussions {product} from {store} | {APP}';
    $templatePageDescription = 'Get {product} only {price} from {store}. Find smiliar product at {marketplace}. Fast Delivery and secure payment at {APP}.';
    $searchArray = ['{product}', '{price}', '{store}', '{marketplace}', '{APP}'];
    $replaceArray = [$product->name, 'Rp.'.formatNumber($product->price), $store->name, $marketplace->name, env('APP_NAME')];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        @include('modules.marketplaces.frontend.stores.inc.single-product-info-style')
    </style>
@endpush

@push('js')
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/bootstrap-number-input/bootstrap-number-input.js" ></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#store-category-sidebar').metisMenu({
                toggle: false,
            });

            $('.product-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin:15,
                stagePadding: 50,
                nav:true,
                navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                autoHeight:true,
                lazyLoad:true,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        stagePadding: 80,
                    },
                    480:{
                        items:2,
                        nav:false,
                    },
                    768:{
                        items:3
                    },
                    992:{
                        items:4
                    },
                    1200:{
                        items:4
                    }
                },
            });

            $('#product-qty').bootstrapNumber();

            @include('modules.marketplaces.frontend.stores.inc.single-product-info-script')

            @include('modules.marketplaces.frontend.stores.inc.store-info-script')

        });
    </script>

    <script type="text/javascript" src="{{ asset('/assets') }}/awesome-qr/dist/require.js"></script>
    <script type="text/javascript">
        var __awesome_qr_base_path = "../../../../../../assets/awesome-qr/dist";

        @include('modules.marketplaces.frontend.stores.inc.single-product-info-qr-script')
    </script>

@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-store')

    <div class="page-content">
        @include('modules.marketplaces.frontend.marketplaces.inc.marketplace-inactive-message')
        @include('modules.marketplaces.frontend.stores.inc.store-inactive-message')

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    @include('flash::message')

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('stores.show', [$marketplace->slug, $store->slug]) }}">
                                {{ $store->name }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('products.index', [$marketplace->slug, $store->slug]) }}">
                                Products
                            </a>
                        </li>
                         <li>
                            <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">
                                {{ $product->name }}
                            </a>
                        </li>
                        <li class="active">
                            Discussions
                        </li>
                    </ol>

                    <div class="product-single">
                        <div class="product">
                            @include('modules.marketplaces.frontend.stores.inc.single-product-info')

                            @include('modules.marketplaces.frontend.stores.inc.store-info')

                            <div class="box">
                                <div class="box-content">
                                    <div role="tabpanel">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs border-bottom-nav" role="tablist">
                                            <li role="presentation">
                                                <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">Back to Product</a>
                                            </li>
                                            <li role="presentation" class="active">
                                                <a href="#tab-product-discussions" aria-controls="tab-product-discussions" role="tab" data-toggle="tab">Discussions</a>
                                            </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane pt-4 active" id="tab-product-discussions">
                                                @include('modules.marketplaces.frontend.stores.products.discussions.inc.create_form')
                                                <hr>
                                                <h3>Discussions</h3>
                                                @include('modules.marketplaces.frontend.stores.products.discussions.inc.filter')
                                                <div class="product__discussions">
                                                    @if (count($productDiscussions))
                                                        @foreach ($productDiscussions as $discussion)
                                                            <div class="product__discussion">
                                                                <div class="media">
                                                                    <a class="pull-left" href="{{ route('products.discussions.show', [$marketplace->slug, $store->slug, $product->slug, $discussion->id]) }}">
                                                                        <img class="media-object lazy img-circle"
                                                                            data-src="{{ asset($discussion->user->photo) }}" alt="Image" height="50">
                                                                    </a>
                                                                    <div class="media-body">
                                                                        <h4 class="media-heading">{{ $discussion->user->name }}</h4>
                                                                        <small class="text-muted">
                                                                            <i class="fa fa-clock"></i>
                                                                            <em>{{ $discussion->created_at }}</em>
                                                                        </small>
                                                                        <p>{{ $discussion->content }}</p>
                                                                        <span class="btn btn-default btn-xs">
                                                                            <small>
                                                                                <i class="fa fa-comments"></i>
                                                                                {{ $discussion->details_count }}
                                                                            </small>
                                                                        </span>
                                                                        <a class="btn btn-default btn-xs" href="{{ route('products.discussions.show', [$marketplace->slug, $store->slug, $product->slug, $discussion->id]) }}">
                                                                            View Discussion
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            </div>
                                                        @endforeach
                                                        {{ $productDiscussions->appends(request()->only(['author']))->links() }}
                                                    @else
                                                        <p>No discussion</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        
    </div>



    @include('modules.marketplaces.frontend.inc.sidebar-store-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
