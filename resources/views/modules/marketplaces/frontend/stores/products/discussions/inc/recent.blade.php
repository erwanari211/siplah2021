@include('modules.marketplaces.frontend.stores.products.discussions.inc.create_form')
<hr>
<h3>Recent Discussions</h3>
@include('modules.marketplaces.frontend.stores.products.discussions.inc.filter')
<div class="product__discussions">
    @if (count($productDiscussions))
        @foreach ($productDiscussions as $discussion)
            <div class="product__discussion">
                <div class="media">
                    <a class="pull-left" href="{{ route('products.discussions.show', [$marketplace->slug, $store->slug, $product->slug, $discussion->id]) }}">
                        <img class="media-object lazy img-circle"
                            data-src="{{ asset($discussion->user->photo) }}" alt="Image" height="50">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{ $discussion->user->name }}</h4>
                        <small class="text-muted">
                            <i class="fa fa-clock"></i>
                            <em>{{ $discussion->created_at }}</em>
                        </small>
                        <p>{{ $discussion->content }}</p>
                        <span class="btn btn-default btn-xs">
                            <small>
                                <i class="fa fa-comments"></i>
                                {{ $discussion->details_count }}
                            </small>
                        </span>
                        <a class="btn btn-default btn-xs" href="{{ route('products.discussions.show', [$marketplace->slug, $store->slug, $product->slug, $discussion->id]) }}">
                            View Discussion
                        </a>
                    </div>
                </div>
                <hr>
            </div>
        @endforeach
        @if ($countProductDiscussions > 3)
            <a class="btn btn-default btn-xs"
                href="{{ route('products.discussions.index', [$marketplace->slug, $store->slug, $product->slug]) }}">
                See more discussions
            </a>
        @endif
    @else
        <p>No discussion</p>
    @endif
</div>
