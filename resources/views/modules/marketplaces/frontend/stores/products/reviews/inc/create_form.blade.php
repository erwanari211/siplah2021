@if (auth()->check())
    <h3 class="mt-0">Add Review</h3>
    {!! Form::open([
        'route' => [
            'products.reviews.store',
            $marketplace->slug,
            $store->slug,
            $product->slug
        ],
        'method' => 'POST',
        'files' => false,
        'class' => 'mb-4'
        ]) !!}

        <select class="select-rating" name="rating" data-initial-rating="4.6">
          <option value="">0</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
        <div class="form-group {{ $errors->has('rating') ?  'has-error' : '' }}">
            @if ($errors->has('rating'))
                <span class="help-block" role="alert">
                    <strong>{{ $errors->first('rating') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('content') ?  'has-error' : '' }}">
            <textarea name="content" class="form-control" rows="3"  placeholder="Review"></textarea>
            @if ($errors->has('content'))
                <span class="help-block" role="alert">
                    <strong>{{ $errors->first('content') }}</strong>
                </span>
            @endif
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>

    {!! Form::close() !!}
@else
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Please <a href="{{ route('login') }}">login</a> to add review.
    </div>
@endif
