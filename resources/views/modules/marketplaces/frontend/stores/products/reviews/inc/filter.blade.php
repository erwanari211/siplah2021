@php
    $filters = [
        'all' => [
            'url' => route('products.reviews.index', [$marketplace->slug, $store->slug, $product->slug]),
            'label' => 'All'
        ],
        'rating_1' => [
            'url' => route('products.reviews.index', [$marketplace->slug, $store->slug, $product->slug, 'rating' => '1']),
            'label' => '<i class="fa fa-star with-color"></i> 1'
        ],
        'rating_2' => [
            'url' => route('products.reviews.index', [$marketplace->slug, $store->slug, $product->slug, 'rating' => '2']),
            'label' => '<i class="fa fa-star with-color"></i> 2'
        ],
        'rating_3' => [
            'url' => route('products.reviews.index', [$marketplace->slug, $store->slug, $product->slug, 'rating' => '3']),
            'label' => '<i class="fa fa-star with-color"></i> 3'
        ],
        'rating_4' => [
            'url' => route('products.reviews.index', [$marketplace->slug, $store->slug, $product->slug, 'rating' => '4']),
            'label' => '<i class="fa fa-star with-color"></i> 4'
        ],
        'rating_5' => [
            'url' => route('products.reviews.index', [$marketplace->slug, $store->slug, $product->slug, 'rating' => '5']),
            'label' => '<i class="fa fa-star with-color"></i> 5'
        ],
    ]
@endphp
<div class="discussion-filters mb-4">
    @foreach ($filters as $filterName => $filter)
        <a class="btn {{ $filterName == $reviewFilter ? 'btn-primary' : 'btn-default' }}"
            href="{{ $filter['url'] }}">
            {!! $filter['label'] !!}
        </a>
    @endforeach
</div>
