@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    $templatePageTitle = 'Products from {store} | {APP}';
    $templatePageDescription = 'Get products you need from {store}. Find smiliar product at {marketplace}. Fast Delivery and secure payment at {APP}.';
    $searchArray = ['{store}', '{marketplace}', '{APP}'];
    $replaceArray = [$store->name, $marketplace->name, env('APP_NAME')];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .change-qty-container {
            width: 200px;
        }

        .qty-column {
            width: 150px;
        }

        .product-qty-container {
            width: 135px;
        }

        .d-inline-block {
            display: inline-block;
        }
    </style>
@endpush

@push('js')
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/bootstrap-number-input/bootstrap-number-input.js" ></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#store-category-sidebar').metisMenu({
                toggle: false,
            });

            $('.product-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin:15,
                stagePadding: 50,
                nav:true,
                navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                autoHeight:true,
                lazyLoad:true,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        stagePadding: 80,
                    },
                    480:{
                        items:2,
                        nav:false,
                    },
                    768:{
                        items:3
                    },
                    992:{
                        items:4
                    },
                    1200:{
                        items:4
                    }
                },
            });

            @include('modules.marketplaces.frontend.stores.inc.store-info-script')

            $('#products-sort').on('change', function(event) {
                event.preventDefault();
                var url = '{{ $url }}';
                var queryString = '{{ $queryString }}';
                var sortBy = $(this).val();
                var newUrl = '';
                newUrl += url + '?sort=' + sortBy;
                newUrl += (queryString) ? '&' + queryString : '';
                window.location.replace(newUrl);
            });

            $('.product-qty').bootstrapNumber();

            // change qty
            $('[data-action="change-qty"]').on('click', function(event) {
                event.preventDefault();
                var qty = $('[data-id="qty"]').val();
                var isNumber = (isNaN(qty) || !qty.length) ? false : true;
                if (isNumber) {
                    $('.product-qty').val(qty);
                }
            });

        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-store')

    <div class="page-content">
        @include('modules.marketplaces.frontend.marketplaces.inc.marketplace-inactive-message')
        @include('modules.marketplaces.frontend.stores.inc.store-inactive-message')

        <div class="mb-3">
            <div class="container">
                @include('flash::message')

                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('stores.show', [$marketplace->slug, $store->slug]) }}">
                            {{ $store->name }}
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{ route('products.index', [$marketplace->slug, $store->slug]) }}">
                            Products
                        </a>
                    </li>
                </ol>
                @include('modules.marketplaces.frontend.stores.inc.store-info')
            </div>
        </div>

        @include('modules.marketplaces.frontend.stores.inc.product-display-options')

        @if (request('display') == 'grid')
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box">
                            <div class="box-content">
                                @include('modules.marketplaces.frontend.stores.inc.product-grid')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="products products-grid row">
                            @if (count($products))
                                @foreach ($products as $product)
                                    @php
                                        $productMetas = $product->getAllMeta();
                                        $originalPrice = isset($productMetas['original_price']) ? $productMetas['original_price'] : 0;
                                        $discountAmountPercent = $product->discountAmountPercent;
                                    @endphp
                                    <div class="col-md-3 col-sm-4 col-xs-6">
                                        <div class="product">
                                            @if ($discountAmountPercent)
                                                <span class="product__label right">
                                                    <div class="text-center">
                                                    {{ $discountAmountPercent }}%<br>OFF
                                                    </div>
                                                </span>
                                            @endif
                                            <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">
                                                <div class="product__image-wrapper">
                                                    <img class="product__image lazy" data-src="{{ $product->image_url }}">
                                                </div>
                                            </a>
                                            <strong class="product__name">
                                                <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">
                                                    {{ $product->name }}
                                                </a>
                                            </strong>

                                            @php
                                                $hasKemdikbudZonePrice = $product->getMeta('has_kemdikbud_zone_price');
                                            @endphp

                                            @if (!$hasKemdikbudZonePrice)

                                            <div class="product__price">
                                                @if ($originalPrice)
                                                    <span class="product__original-price">{{ "Rp.".formatNumber($originalPrice) }}</span>
                                                @endif
                                                <span class="product__current-price">{{ "Rp.".formatNumber($product->price) }}</span>
                                            </div>

                                            @endif

                                            @if ($hasKemdikbudZonePrice)
                                                @php
                                                    $price = 0;
                                                    $minPrice = 0;
                                                    $maxPrice = 0;

                                                    $mappedKemdikbudZonePrices = $product->getMappedKemdikbudZonePrices();
                                                    $kemdikbudMinMaxZonePrice = $product->getKemdikbudMinMaxZonePrices($mappedKemdikbudZonePrices);
                                                    $minPrice = $kemdikbudMinMaxZonePrice['minPrice'];
                                                    $maxPrice = $kemdikbudMinMaxZonePrice['maxPrice'];
                                                    $formattedZonePrice = $product->getKemdikbudFormattedZonePrices($minPrice, $maxPrice);
                                                @endphp
                                                <div class="product__price">
                                                    <span class="product__current-price">{{ $formattedZonePrice }}</span>
                                                </div>
                                            @endif

                                            <div class="product__rating">
                                                <select class="rating" data-initial-rating="{{ $product->rating }}">
                                                  <option value="">0</option>
                                                  <option value="1">1</option>
                                                  <option value="2">2</option>
                                                  <option value="3">3</option>
                                                  <option value="4">4</option>
                                                  <option value="5">5</option>
                                                </select>
                                                @if ($product->reviews_count)
                                                    <span>({{ $product->reviews_count }})</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="col-sm-12">
                                    No Product
                                </div>
                            @endif
                        </div>
                        {{ $products->appends(request()->only(['search', 'sort']))->links() }}
                    </div>
                </div>

            </div>
        @endif

    </div>

    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-store-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
