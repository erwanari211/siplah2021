@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    $templatePageTitle = '{product} from {store} | {APP}';
    $templatePageDescription = 'Get {product} only {price} from {store}. Find smiliar product at {marketplace}. Fast Delivery and secure payment at {APP}.';
    $searchArray = ['{product}', '{price}', '{store}', '{marketplace}', '{APP}'];
    $replaceArray = [$product->name, 'Rp.'.formatNumber($product->price), $store->name, $marketplace->name, env('APP_NAME')];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins') }}/lightcase-2.5.0/src/css/lightcase.css">
    <style type="text/css">
        .fa.fa-star.with-color {
            color: #edb867;
        }

        .product__product-galleries {
            overflow-x: auto;
            white-space: nowrap;
            padding-bottom: 5px;
        }

        .product__product-galleries > a {
            text-decoration: none;
        }

        .product__product-galleries .product-gallery-item {
            height: 50px;
            display: inline-block;
            vertical-align: top;
        }

        /**
         * Lightbox custom
         */
        #lightcase-info {
            bottom: 0;
            background-color: rgba(0,0,0,0.5);
            z-index: 10;
            width: 100%;
            padding: 10px;
        }

        #lightcase-sequenceInfo { color: #fff!important; }
        #lightcase-title { color: #fff!important; }
        #lightcase-caption { color: #fff!important; }

        /**
         * WA Chat button
         */
        .wa-chat-btn {
            color: #fff;
            background-color: #12AF0A;
            border-color: #0c7a06;
        }



        .font-weight-bold {
            font-weight: 700!important;
        }

        .select2-container {
            width: 100% !important;
        }
    </style>
@endpush

@include('modules.marketplaces.frontend.stores.inc.single-product-info-style')

@push('js')
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/js/select2.min.js"></script>
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/bootstrap-number-input/bootstrap-number-input.js"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins') }}/lightcase-2.5.0/src/js/lightcase.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#store-category-sidebar').metisMenu({
                toggle: false,
            });

            $('.product-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin:15,
                stagePadding: 50,
                nav:true,
                navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                autoHeight:true,
                lazyLoad:true,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        stagePadding: 80,
                    },
                    480:{
                        items:2,
                        nav:false,
                    },
                    768:{
                        items:3
                    },
                    992:{
                        items:4
                    },
                    1200:{
                        items:4
                    }
                },
            });

            $('#product-qty').bootstrapNumber();

            $('.select-rating').barrating({
                theme: 'fontawesome-stars',
                showSelectedRating: true,
            });





            $('a[data-rel^=lightcase]').lightcase({
                showSequenceInfo: false,
            });

            var citiesWithZone = @json($citiesWithZone);

            $.fn.select2.defaults.set( "theme", "bootstrap" );

            $('.select2').select2({
                // placeholder: 'Please Select'
            });

            function setFindZoneDropdown() {
                $('#find_zone').html('');
                $('#find_zone_message').hide();

                let dropdown = '';
                dropdown += `
                    <option value="">Please select</option>
                `;
                for (let cityId in citiesWithZone) {
                    let city = citiesWithZone[cityId];
                    dropdown += `
                        <option value="${cityId}">${city.cityName}</option>
                    `;
                }
                $('#find_zone').html(dropdown);
                $('.select2').select2({
                    // placeholder: 'Please Select'
                });

                $('#find_zone').on('change', function(event) {
                    event.preventDefault();
                    let cityId = $(this).val();
                    $('#find_zone_message').hide();
                    $('#find_zone_message').html('');

                    if (cityId) {
                        let city = citiesWithZone[cityId];
                        let message = `${city.cityName} is located at <strong>Zone ${city.cityZone}<strong>`;
                        $('#find_zone_message').html(message);
                        $('#find_zone_message').show();
                    }
                });
            }
            setFindZoneDropdown();



        });
    </script>

    {{--
    <script type="text/javascript" src="{{ asset('/assets') }}/awesome-qr/dist/require.js"></script>
    <script type="text/javascript">
        var __awesome_qr_base_path = "../../../../../assets/awesome-qr/dist";

        @include('modules.marketplaces.frontend.stores.inc.single-product-info-qr-script')

        @php
            $productUrl = '';
            if ($product->unique_code) {
                $productUrl = route('shorturl.products.show', [$product->unique_code]);
            } else {
                $productUrl = route('products.show', [$marketplace->slug, $store->slug, $product->slug]);
            }
        @endphp
        var logo = new Image();
        logo.crossOrigin = "Anonymous";
        logo.onload = () => {
            require([__awesome_qr_base_path+'/awesome-qr'], function(AwesomeQR) {
                AwesomeQR.create({
                    text: '{{ $productUrl }}',
                    size: 800,
                    margin: 20,
                    logoImage: logo,
                    bindElement: 'qrcode-logo',

                    colorDark: '#0000ff',
                    dotScale: 0.7,
                    logoMargin: 8,
                });
            });
        };
        logo.src = "{{ asset('images') }}/zuala-logo-small-v0.2.png";
    </script>
    --}}
@endpush

@include('modules.marketplaces.frontend.stores.inc.single-product-info-script')
@include('modules.marketplaces.frontend.stores.inc.store-info-script')

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')
    @include('modules.marketplaces.frontend.inc.navbar-top-store')

    <div class="page-content">
        @include('modules.marketplaces.frontend.marketplaces.inc.marketplace-inactive-message')
        @include('modules.marketplaces.frontend.stores.inc.store-inactive-message')

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    @include('flash::message')

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('stores.show', [$marketplace->slug, $store->slug]) }}">
                                {{ $store->name }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('products.index', [$marketplace->slug, $store->slug]) }}">
                                Products
                            </a>
                        </li>
                        <li class="active">
                            {{ $product->name }}
                        </li>
                    </ol>

                    <div class="product-single">
                        <div class="product">
                            @include('modules.marketplaces.frontend.stores.inc.single-product-info')

                            @include('modules.marketplaces.frontend.stores.inc.store-info')

                            <div class="box">
                                <div class="box-content">
                                    <div>
                                        <!-- Nav tabs -->
                                        @php
                                            $activeTab = 'product-description';
                                            $isGroupedProduct = $product->type == 'grouped product';
                                            if ($hasKemdikbudZonePrice) {
                                                $activeTab = 'zone-prices';
                                            }
                                            if ($isGroupedProduct) {
                                                $activeTab = 'grouped-product-details';
                                            }
                                        @endphp
                                        <ul class="nav nav-tabs border-bottom-nav" role="tablist">
                                            @if ($isGroupedProduct)
                                                <li class="{{ $activeTab == 'grouped-product-details' ? 'active' : '' }}">
                                                    <a href="#tab-grouped-product-details" data-toggle="tab">
                                                        <span class="hidden-xs">Grouped Product</span>
                                                        <span class="visible-xs">Grouped..</span>
                                                    </a>
                                                </li>
                                            @endif
                                            <li class="{{ $activeTab == 'product-description' ? 'active' : '' }}">
                                                <a href="#tab-product-description" data-toggle="tab">
                                                    <span class="hidden-xs">Description</span>
                                                    <span class="visible-xs">Desc.</span>
                                                </a>
                                            </li>
                                            @if ($hasKemdikbudZonePrice)
                                                <li class="{{ $activeTab == 'zone-prices' ? 'active' : '' }}">
                                                    <a href="#tab-zone-prices" data-toggle="tab">
                                                        <span class="hidden-xs">Zone Prices</span>
                                                        <span class="visible-xs">Zone Prices</span>
                                                    </a>
                                                </li>
                                            @endif
                                            <li>
                                                <a href="#tab-product-attributes" data-toggle="tab">
                                                    <span class="hidden-xs">Specification</span>
                                                    <span class="visible-xs">Spec.</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab-qr-code" data-toggle="tab">
                                                    <span class="hidden-xs">QR Code</span>
                                                    <span class="visible-xs">QR</span>
                                                </a>
                                            </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div
                                                class="tab-pane pt-4
                                                {{ $activeTab == 'product-description' ? 'active' : '' }}"
                                                id="tab-product-description">
                                                {!! $product->description !!}
                                            </div>
                                            <div class="tab-pane pt-4" id="tab-product-attributes">
                                                <table class="table table-hover table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <th class="product-attribute-group" colspan="2">INFO</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="product-attribute-name">
                                                                <span>Categories</span>
                                                            </td>
                                                            <td class="product-attribute-value">
                                                                @foreach ($productCategories as $category)
                                                                    <a class="label label-primary" href="{{ route('marketplaces.categories.show', [$marketplace->slug, $category->slug]) }}">
                                                                        {{ $category->name }}
                                                                    </a>&nbsp;
                                                                @endforeach
                                                            </td>
                                                        </tr>

                                                        @php
                                                            $productMeta = $product->getAllMeta();
                                                        @endphp

                                                        @if (in_array($product->type, ['product', 'grouped product', 'digital product']))
                                                            <tr>
                                                                <td class="product-attribute-name">
                                                                    Jenis
                                                                </td>
                                                                <td class="product-attribute-value">
                                                                    Produk
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="product-attribute-name">
                                                                    PPN
                                                                </td>
                                                                <td class="product-attribute-value">
                                                                    {{ $product->is_taxed ? 'Ya' : 'Tidak' }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="product-attribute-name">
                                                                    Produk Dalam Negeri
                                                                </td>
                                                                <td class="product-attribute-value">
                                                                    {{ $product->is_local_product ? 'Ya' : 'Tidak' }}
                                                                </td>
                                                            </tr>
                                                            @if ($productMeta['product_is_msme_product'] ?? null)
                                                                <tr>
                                                                    <td class="product-attribute-name">
                                                                        Produk UMKM
                                                                    </td>
                                                                    <td class="product-attribute-value">
                                                                        {{ in_array($productMeta['product_is_msme_product'], ['Mikro', 'Kecil', 'Menengah']) ? 'UMKM' : $productMeta['product_is_msme_product'] }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if ($productMeta['product_jaminan_kebaruan'] ?? null)
                                                                <tr>
                                                                    <td class="product-attribute-name">
                                                                        Jaminan Kebaruan
                                                                    </td>
                                                                    <td class="product-attribute-value">
                                                                        {{ $productMeta['product_jaminan_kebaruan'] ? 'Ya' : 'Tidak' }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if ($productMeta['product_garansi'] ?? null)
                                                                <tr>
                                                                    <td class="product-attribute-name">
                                                                        Garansi
                                                                    </td>
                                                                    <td class="product-attribute-value">
                                                                        {{ $productMeta['product_garansi'] }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if ($productMeta['product_status_ketersediaan'] ?? null)
                                                                <tr>
                                                                    <td class="product-attribute-name">
                                                                        Status Ketersediaan
                                                                    </td>
                                                                    <td class="product-attribute-value">
                                                                        {{ $productMeta['product_status_ketersediaan'] }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if ($productMeta['product_minimal_order'] ?? null)
                                                                <tr>
                                                                    <td class="product-attribute-name">
                                                                        Minimal Order
                                                                    </td>
                                                                    <td class="product-attribute-value">
                                                                        {{ $productMeta['product_minimal_order'] }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if ($productMeta['product_cara_pengiriman'] ?? null)
                                                                <tr>
                                                                    <td class="product-attribute-name">
                                                                        Cara Pengiriman
                                                                    </td>
                                                                    <td class="product-attribute-value">
                                                                        {{ $productMeta['product_cara_pengiriman'] }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endif

                                                        @if (in_array($product->type, ['service']))
                                                            <tr>
                                                                <td class="product-attribute-name">
                                                                    Jenis
                                                                </td>
                                                                <td class="product-attribute-value">
                                                                    Jasa
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="product-attribute-name">
                                                                    PPN
                                                                </td>
                                                                <td class="product-attribute-value">
                                                                    {{ $product->is_taxed ? 'Ya' : 'Tidak' }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="product-attribute-name">
                                                                    Produk Dalam Negeri
                                                                </td>
                                                                <td class="product-attribute-value">
                                                                    {{ $product->is_local_product ? 'Ya' : 'Tidak' }}
                                                                </td>
                                                            </tr>

                                                            @if ($productMeta['service_is_msme_product'] ?? null)
                                                                <tr>
                                                                    <td class="product-attribute-name">
                                                                        Jasa UMKM
                                                                    </td>
                                                                    <td class="product-attribute-value">
                                                                        {{ in_array($productMeta['service_is_msme_product'], ['Mikro', 'Kecil', 'Menengah']) ? 'UMKM' : $productMeta['product_is_msme_product'] }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if ($productMeta['service_jasa_kemendikbud'] ?? null)
                                                                <tr>
                                                                    <td class="product-attribute-name">
                                                                        Jasa Kemendikbud
                                                                    </td>
                                                                    <td class="product-attribute-value">
                                                                        {{ $productMeta['service_jasa_kemendikbud'] ? 'Ya' : 'Tidak' }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if ($productMeta['service_garansi'] ?? null)
                                                                <tr>
                                                                    <td class="product-attribute-name">
                                                                        Garansi
                                                                    </td>
                                                                    <td class="product-attribute-value">
                                                                        {{ $productMeta['service_garansi'] }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if ($productMeta['service_waktu_cara_pelaksanaan'] ?? null)
                                                                <tr>
                                                                    <td class="product-attribute-name">
                                                                        Waktu dan Cara Pelaksanaan
                                                                    </td>
                                                                    <td class="product-attribute-value">
                                                                        {{ $productMeta['service_waktu_cara_pelaksanaan'] }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if ($productMeta['service_jaminan_pelaksanaan'] ?? null)
                                                                <tr>
                                                                    <td class="product-attribute-name">
                                                                        Jaminan Pelaksanaan
                                                                    </td>
                                                                    <td class="product-attribute-value">
                                                                        {{ $productMeta['service_jaminan_pelaksanaan'] }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if ($productMeta['service_status_verifikasi_jasa'] ?? null)
                                                                <tr>
                                                                    <td class="product-attribute-name">
                                                                        Status Verifikasi Jasa
                                                                    </td>
                                                                    <td class="product-attribute-value">
                                                                        {{ $productMeta['service_status_verifikasi_jasa'] }}
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endif

                                                        @if (count($groupedAttributes))
                                                            @foreach ($groupedAttributes as $group => $attributes)
                                                                <tr>
                                                                    <td class="product-attribute-group" colspan="2"><strong>{{ $group }}</strong></td>
                                                                </tr>
                                                                @foreach ($attributes as $attribute)
                                                                    <tr>
                                                                        <td class="product-attribute-name">{{ $attribute->key }}</td>
                                                                        <td class="product-attribute-value">{{ $attribute->value }}</td>
                                                                    </tr>
                                                                @endforeach
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                            @if ($isGroupedProduct)
                                                @php
                                                    $groupedProductDetails = $product->groupedProductDetails()->with('parent', 'product')->get();
                                                @endphp
                                                <div
                                                    class="tab-pane pt-4
                                                    {{ $activeTab == 'grouped-product-details' ? 'active' : '' }}"
                                                    id="tab-grouped-product-details">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <div class="table-responsive">
                                                                <table class="table table-bordered table-hover mb-0">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="100px">Image</th>
                                                                            <th>Product</th>
                                                                            <th width="100px">Qty</th>
                                                                            <th>Note</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @if (count($groupedProductDetails))
                                                                            @foreach ($groupedProductDetails as $detail)
                                                                                <tr>
                                                                                    <td>
                                                                                        <img src="{{ asset($detail->product->image) }}" width="50" height="50">
                                                                                    </td>
                                                                                    <td>
                                                                                        <a href="{{ route('products.show', [
                                                                                            $marketplace->slug,
                                                                                            $store->slug,
                                                                                            $detail->product->slug]) }}"
                                                                                            target="_blank">
                                                                                            {{ $detail->product->name }}
                                                                                        </a>
                                                                                    </td>
                                                                                    <td>{{ $detail->qty }}</td>
                                                                                    <td>{{ $detail->note }}</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @else
                                                                            <tr>
                                                                                <td colspan="4">No data</td>
                                                                            </tr>
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="tab-pane pt-4" id="tab-qr-code">
                                                <img class="img-thumbnail" id="qrcode-logo" width="200" height="200">
                                            </div>

                                            @if ($hasKemdikbudZonePrice)
                                                @php
                                                    $kemdikbud_zone = session('kemdikbud_zone');
                                                @endphp
                                                <div
                                                    class="tab-pane pt-4
                                                    {{ $activeTab == 'zone-prices' ? 'active' : '' }}"
                                                    id="tab-zone-prices">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <div class="table-responsive">
                                                                <table class="table table-bordered">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Zone</th>
                                                                            <th>Price</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @if (count($mappedKemdikbudZonePrices))
                                                                            @foreach ($mappedKemdikbudZonePrices as $zone => $zonePrice)
                                                                                @php
                                                                                    $selectedZonePrice = false;
                                                                                    if ($kemdikbud_zone == $zone) {
                                                                                        $selectedZonePrice = true;
                                                                                    }
                                                                                @endphp
                                                                                <tr class="{{ $selectedZonePrice ? 'bg-success' : '' }}">
                                                                                    <td>
                                                                                        <a
                                                                                            class="{{ $selectedZonePrice ? 'font-weight-bold' : '' }}"
                                                                                            data-toggle="modal" href='#modal-zone-{{ $zone }}'>
                                                                                            Zone
                                                                                            {{ $zone }}
                                                                                        </a>
                                                                                    </td>
                                                                                    <td>
                                                                                        <span
                                                                                            class="{{ $selectedZonePrice ? 'font-weight-bold' : '' }}">
                                                                                            {{ "Rp.".formatNumber($zonePrice) }}
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @else
                                                                            <tr>
                                                                                <td colspan="4">No data</td>
                                                                            </tr>
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label for="find_zone" class="control-label">Find Your Zone</label>
                                                                <select class="form-control select2"
                                                                    id="find_zone" name="find_zone">
                                                                    <option value="">Please Select</option>
                                                                </select>

                                                            </div>
                                                            <div class="well" id="find_zone_message">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box">
                                <div class="box-content">
                                    <div>
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs border-bottom-nav" role="tablist">
                                            <li class="active">
                                                <a href="#tab-product-reviews" data-toggle="tab">Review</a>
                                            </li>
                                            <li>
                                                <a href="#tab-product-discussions" data-toggle="tab">Discussions</a>
                                            </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane pt-4 active" id="tab-product-reviews">
                                                @include('modules.marketplaces.frontend.stores.products.reviews.inc.recent')
                                            </div>
                                            <div class="tab-pane pt-4" id="tab-product-discussions">
                                                @include('modules.marketplaces.frontend.stores.products.discussions.inc.recent')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

    @if ($hasKemdikbudZonePrice)
        @foreach ($citiesGroupedByZone as $zoneGroup => $provinceGroup)
            <div class="modal fade" id="modal-zone-{{ $zoneGroup }}">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Zone {{ $zoneGroup }}</h4>
                        </div>
                        <div class="modal-body">
                            <div style="max-height: 60vh; overflow: auto;">
                                @foreach ($provinceGroup as $provinceName => $citiesName)
                                    <strong>{{ $provinceName }}</strong> <br>
                                    @foreach ($citiesName as $cityName)
                                        <span>{{ $cityName }}</span> <br>
                                    @endforeach
                                    <hr>
                                @endforeach
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif


    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-store-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
