@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    $templatePageTitle = 'Stores at {marketplace} | {APP}';
    $templatePageDescription = 'Get products you need from {marketplace}. Fast Delivery and secure payment at {APP}.';
    $searchArray = ['{marketplace}', '{APP}'];
    $replaceArray = [$marketplace->name, env('APP_NAME')];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .promo-title span {
            display: inline-block;
            border-bottom: 5px solid;
        }
    </style>
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#marketplace-category-sidebar').metisMenu({
                toggle: false,
            });

            $('.product-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin:15,
                stagePadding: 50,
                nav:true,
                navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                autoHeight:true,
                lazyLoad:true,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        stagePadding: 80,
                    },
                    480:{
                        items:2,
                        nav:false,
                    },
                    768:{
                        items:3
                    },
                    992:{
                        items:4
                    },
                    1200:{
                        items:4
                    }
                },
            });
        });
    </script>
@endpush

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
@endpush

@push('js')
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            var url = "{{ route('ajax.regions.cities.index') }}";
            $('.js-data-example-ajax').select2({
                ajax: {
                    url: url,
                    delay: 250,
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                        }

                        return query;
                    },
                    processResults: function (data, params) {
                        return {
                            results: data,
                        };
                    },
                    cache: true,
                    minimumInputLength: 1,
                }
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-marketplace')

    <div class="page-content">
        @include('modules.marketplaces.frontend.marketplaces.inc.marketplace-inactive-message')

        <div class="container">

            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('marketplaces.show', [$marketplace->slug]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('stores.index', [$marketplace->slug]) }}">Stores</a>
                        </li>
                    </ol>

                    @if ($collection)
                        <div class="row">
                            <div class="col-sm-12">
                                <div>
                                    <h3 class="mt-0 pull-left">
                                        {{ $collection->display_name ? $collection->display_name : 'Our Stores' }}
                                    </h3>
                                    <a class="btn btn-default btn-xs pull-right mb-4" href="{{ route('stores.index', [$marketplace->slug]) }}">View all stores</a>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="mb-4">
                                    <a class="btn btn-default" data-toggle="collapse" href="#search-store-container">
                                        <i class="fa fa-search"></i>
                                        Search Store
                                    </a>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-sm-12">
                                <div>
                                    <h3 class="mt-0">Stores</h3>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="mb-4">
                                    <a class="btn btn-default" data-toggle="collapse" href="#search-store-container">
                                        <i class="fa fa-search"></i>
                                        Search Store
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endif

                    @include('flash::message')

                    <div class="collapse" id="search-store-container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="box">
                                    <div class="box-content">
                                        <form class="form">
                                            <div class="form-group">

                                                <input
                                                        type="text"
                                                        class="form-control"
                                                        name="search"
                                                        value="{{ request('search') }}"
                                                        placeholder="Search Store">

                                                    @if (request('collection'))
                                                        <input
                                                            type="hidden"
                                                            class="form-control"
                                                            name="collection"
                                                            value="{{ request('collection') }}">
                                                    @endif

                                            </div>

                                            <div class="form-group">
                                                <select name="city_id" class="js-data-example-ajax" style="width:100%">
                                                    @if (request('city_id'))
                                                        <option value="{{ $selectedCity->id }}" selected="selected">
                                                            {{ $selectedCity->name }}
                                                        </option>
                                                    @endif
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-default">
                                                    <i class="fa fa-search"></i>
                                                    Search
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if (count($stores))
                        <div class="row">
                                @foreach ($stores as $store)
                                    @if ($collection)
                                        @php
                                            $detail = $store;
                                            $store = $detail->store;
                                        @endphp
                                    @endif
                                    <div class="col-xs-6 col-sm-3 col-md-2">
                                        <div class="box">
                                            <div class="box-content">
                                                <div class="icon-box">
                                                    <a class="icon-box-link" href="{{ route('stores.show', [$marketplace->slug, $store->slug]) }}">
                                                        <div class="icon-box-icon-container">
                                                            <img class="icon-box-image lazy" data-src="{{ asset($store->image) }}">
                                                        </div>
                                                        <div class="icon-box-content">
                                                            <span class="icon-box-title line-clamp-2">{{ $store->name }}</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="clearfix"></div>
                                <div class="col-sm-12">
                                    {{ $stores->appends(request()->only(['collection', 'search', 'city_id']))->links() }}
                                </div>
                            </div>
                    @else
                        No store
                    @endif
                </div>
            </div>

        </div>

    </div>

    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-marketplace-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
