@push('css')
<style>

@media (min-width: 768px) {
    .product-single .product .product__add-to-cart-container {
        width: 100%;
    }
}

.product-single .product .product__store .product__store__img {
    width: 100px;
    height: 100px;
}

.qrcode-logo-alt-wrapper {
    position: relative;
}

#qrcode-logo-alt {
    bottom: 0;
    right: 20px;
    width: 100px;
    height: 100px;
    position: absolute;
}

</style>
@endpush
