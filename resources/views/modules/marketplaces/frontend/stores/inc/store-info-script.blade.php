@push('js')
    <script>
        $(document).ready(function () {
            function getStoreInfo(url) {
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'json',
                })
                .done(function(data) {
                    $('[data-store-info="views"]').html(data['storeViews']);
                    $('[data-store-info="last-online"]').html(data['lastLogin']);
                    $('[data-store-info="successful-transactions"]').html(data['successfulTransactions']);
                    $('[data-store-info="product-sold"]').html(data['productSold']);
                });
            }
            var ajaxStoreUrl = '{{ route('stores.ajax.store-info', [$marketplace->slug, $store->slug]) }}';
            getStoreInfo(ajaxStoreUrl);

            $('[data-toggle="popover"]').popover();
        });
    </script>
@endpush

@push('js')
    @if (isset($storeSettings['chatra_widget']) && $storeSettings['chatra_widget'])
        {!! $storeSettings['chatra_widget'] !!}
    @endif
@endpush
