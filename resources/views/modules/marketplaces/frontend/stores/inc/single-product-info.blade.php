@php
    $productMetas = $product->getAllMeta();
    $originalPrice = isset($productMetas['original_price']) ? $productMetas['original_price'] : 0;
    $discountAmountPercent = $product->discountAmountPercent;
@endphp

<div class="box">
    <div class="box-content">
        <div class="row">
            <div class="col-sm-4">
                <div class="product__image-wrapper">
                    <img class="product__image lazy" data-src="{{ $product->image_url }}">
                </div>

                @if (isset($productGalleries))
                    @if (count($productGalleries))
                        <div class="product__product-galleries">
                            @foreach ($productGalleries as $gallery)
                                @if ($gallery->fileType == 'image')
                                    <a
                                        href="{{ asset($gallery->url) }}"
                                        data-rel="lightcase:product_galleries"
                                        title="{{ $gallery->name }}"
                                        data-lc-caption="{{ $gallery->description }}"
                                        >
                                        <img class="lazy img-thumbnail product-gallery-item" src="{{ asset($gallery->url) }}">
                                    </a>
                                @endif
                                @if ($gallery->fileType == 'video')
                                    <a
                                        href="{{ asset($gallery->url) }}"
                                        data-rel="lightcase:product_galleries"
                                        title="{{ $gallery->name }}"
                                        data-lc-caption="{{ $gallery->description }}"
                                        >
                                        <img class="lazy img-thumbnail product-gallery-item" src="{{ asset('images/icon') }}/video.png">
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    @endif
                @endif
            </div>
            <div class="col-sm-8">
                <h3 class="product__name">
                    <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}">
                        {{ $product->name }}
                    </a>
                </h3>

                @if (!$hasKemdikbudZonePrice)

                <div class="product__price">
                    @if ($originalPrice)
                        <span class="product__original-price">{{ "Rp.".formatNumber($originalPrice) }}</span>
                    @endif
                    @php
                        $discount = '';
                        if ($discountAmountPercent) {
                            $discount .= '
                                <small class="btn btn-xs btn-skin">
                                    '.$discountAmountPercent.'% OFF
                                </small>';
                        }
                    @endphp
                    <span class="product__current-price">{{ "Rp.".formatNumber($product->price) }} {!! $discount !!}</span>

                </div>

                @endif

                @if ($hasKemdikbudZonePrice)
                    @php
                        $price = 0;
                        $minPrice = 0;
                        $maxPrice = 0;

                        $kemdikbudMinMaxZonePrice = $product->getKemdikbudMinMaxZonePrices($mappedKemdikbudZonePrices);
                        $minPrice = $kemdikbudMinMaxZonePrice['minPrice'];
                        $maxPrice = $kemdikbudMinMaxZonePrice['maxPrice'];
                        $formattedZonePrice = $product->getKemdikbudFormattedZonePrices($minPrice, $maxPrice);

                        $kemdikbud_zone = session('kemdikbud_zone');
                        if ($kemdikbud_zone) {
                            foreach ($mappedKemdikbudZonePrices as $zone => $zonePrice) {
                                if ($kemdikbud_zone == $zone) {
                                    $formattedZonePrice = "Rp.".formatNumber($zonePrice);
                                }
                            }
                        }

                        $zonePriceMessage = null;
                        if (isset($storeSettings['zone_price_message'])) {
                            $zonePriceMessage = $storeSettings['zone_price_message'];
                        }

                    @endphp

                    <div class="product__price">
                        <div class="normal-price-wrapper">
                            <span class="mr-3">Normal Price</span>
                            @if ($originalPrice)
                                <span class="product__original-price">
                                    {{ "Rp.".formatNumber($originalPrice) }}
                                </span>
                            @endif
                            @php
                                $discount = '';
                                if ($discountAmountPercent) {
                                    $discount .= '
                                        <small class="btn btn-xs btn-skin">
                                            '.$discountAmountPercent.'% OFF
                                        </small>';
                                }
                            @endphp
                            <span class="product__current-price">{{ "Rp.".formatNumber($product->price) }} {!! $discount !!}</span>
                        </div>
                    </div>
                    <div class="product__price">
                        <div>
                            <span class="mr-3">
                                Zone Price
                                @if ($zonePriceMessage)
                                    <i class="fa fa-info-circle"
                                        data-toggle="popover"
                                        data-content="{{ $zonePriceMessage }}"></i>
                                @endif
                            </span>
                            <span class="product__current-price">
                                {{ $formattedZonePrice }}
                                @if ($kemdikbud_zone)
                                    (zone {{ $kemdikbud_zone }})
                                @endif
                            </span>
                        </div>
                    </div>
                @endif


                <div class="product__rating">
                    <select class="rating"
                        data-initial-rating="{{ $product->rating }}">
                        <option value="">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    @php
                        $reviewsLabel = '';
                        $reviewsLabel .= $countProductReviews.' ';
                        $reviewsLabel .= str_plural('review', $countProductReviews);
                    @endphp
                    <span> ({{ $reviewsLabel }}) </span>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="product__add-to-cart-container">
                            @php
                                $isSchoolStaff = session('login_as') == 'school_staff';
                            @endphp
                            @php
                                $productAvailable = $product->qty > 0;
                                $availableClass = !$productAvailable ? 'disabled' : '';
                            @endphp

                            @if ($isSchoolStaff)

                            @if ($product->active && $product->status == 'active')
                                @if ($productAvailable)
                                    {!! Form::open([
                                            'route' => ['cart.store'],
                                            'method' => 'POST',
                                            'style' => 'display: inline-block;'
                                        ]) !!}
                                        {!! Form::hidden('product_id', $product->id) !!}
                                        <div class="form-group">
                                            <input id="product-qty" class="form-control product__add-to-cart-qty" name="qty" type="number" value="1" min="1" />
                                        </div>
                                        <button class="btn btn-block btn-skin {{ $availableClass }} product__add-to-cart-btn" type="submit">
                                            <i class="fa fa-shopping-cart"></i>
                                            Add to Cart
                                        </button>
                                    {!! Form::close() !!}
                                @else
                                    <button class="btn btn-block btn-skin {{ $availableClass }} product__add-to-cart-btn"
                                        type="button">
                                        Out of stock
                                    </button>
                                @endif
                            @endif

                            @endif

                            @if (isset($storeSettings['whatsapp_number']) && $storeSettings['whatsapp_number'])
                                <div class="mt-4">
                                    <a class="btn wa-chat-btn" href="https://wa.me/{{ $storeSettings['whatsapp_number'] }}" target="_blank">
                                        <i class="fab fa-whatsapp"></i>
                                        Chat with seller
                                    </a>
                                </div>
                            @endif



                            <div class="mt-4">
                                <span class="btn btn-default btn-xs">
                                    <i class="fa fa-boxes"></i>
                                    Stock available : {{ $product->qty }}
                                </span>
                                <br>
                                <span class="btn btn-default btn-xs">
                                    <i class="fa fa-eye"></i> {{ $product->views }} x
                                </span>
                                <span class="btn btn-default btn-xs">
                                    <i class="fa fa-truck"></i> {{ $product->sent }} x
                                </span>
                                @php
                                    $reviewsLabel = '';
                                    $reviewsLabel .= $countProductReviews.' ';
                                    $reviewsLabel .= str_plural('review', $countProductReviews);

                                    $discussionsLabel = '';
                                    $discussionsLabel .= $countProductDiscussions.' ';
                                    $discussionsLabel .= str_plural('discussion', $countProductDiscussions);
                                @endphp
                                <span class="btn btn-default btn-xs">
                                    <i class="fa fa-comment-dots"></i> {{ $reviewsLabel }}
                                </span>
                                <span class="btn btn-default btn-xs">
                                    <i class="fa fa-comments"></i> {{ $discussionsLabel }}
                                </span>
                            </div>

                            <div class="mt-4">
                                @include('modules.marketplaces.frontend.stores.inc.add-to-any-buttons')
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 visible-lg-block visible-md-block qrcode-logo-alt-wrapper">
                        <img class="img-thumbnail pull-right" id="qrcode-logo-alt">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
