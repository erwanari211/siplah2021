@if ($store->latitude && $store->longitude)
    <div id="main-map"
        data-store-name="{{ $store->name }}"
        data-lat="{{ $store->latitude }}" data-lng="{{ $store->longitude }}">
    </div>
@endif


@push('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/leaflet/leaflet.css') }}">
    <style>
        #main-map { height: 200px; margin-bottom: 20px; }
    </style>
@endpush

@push('js')
    <script src="{{ asset('assets/plugins/leaflet/leaflet.js') }}"></script>
    <script>
        $(document).ready(function () {

            var defaultLatLng = {
                lat: null,
                lng: null,
            }
            var displayMap = false;

            var oldLat = $('#main-map').attr('data-lat');
            var oldLng = $('#main-map').attr('data-lng');
            var storeName = $('#main-map').attr('data-store-name');

            if (oldLat && oldLng) {
                displayMap = true;
                defaultLatLng.lat = oldLat;
                defaultLatLng.lng = oldLng;
            }

            var openstreetmap = {}
            openstreetmap = {
                url : 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                options : {
                    maxZoom: 19,
                    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                }
            }

            if(displayMap) {
                var mainMap = L.map('main-map').setView(defaultLatLng, 13);
                L.tileLayer(openstreetmap.url, openstreetmap.options).addTo(mainMap);
                var marker = L.marker(defaultLatLng).addTo(mainMap);
                marker.bindPopup(`${storeName}`);
            }

        });
    </script>
@endpush
