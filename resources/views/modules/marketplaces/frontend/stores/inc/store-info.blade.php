@push('css')
    <style type="text/css">
        .courier-logo {
            width: 90px;
            height: 45px;
        }
    </style>
@endpush

<div class="store-info">
    <div class="box mt-4">
        <div class="box-content">
            <div class="row">
                <div class="col-sm-6">
                    <div class="media mb-4">
                        <a class="pull-left" href="{{ route('stores.show', [$marketplace->slug, $store->slug]) }}">
                            <img class="store__image media-object lazy img-thumbnail" data-src="{{ asset($store->image) }}" alt="Image">
                        </a>
                        <div class="media-body">
                            <h4 class="store__name media-heading">
                                <a href="{{ route('stores.show', [$marketplace->slug, $store->slug]) }}">
                                    {{ $store->name }}
                                </a>
                            </h4>
                            @if (isset($storeSettings['store_tagline']) && $storeSettings['store_tagline'])
                                <p><em>{{ $storeSettings['store_tagline'] }}</em></p>
                            @endif
                            @if (isset($storeSettings['store_description']) && $storeSettings['store_description'])
                                <p>{{ $storeSettings['store_description'] }}</p>
                            @endif
                            {{--
                            @if (isset($storeSettings['location']) && $storeSettings['location'])
                                <p class="store__location">
                                    <i class="fa fa-map-marker-alt"></i> {{ $storeSettings['location'] }}
                                </p>
                            @endif
                            --}}

                            @if (isset($hasDefaultStoreAddress) && $hasDefaultStoreAddress)
                                @if (isset($defaultStoreAddress) && $defaultStoreAddress)
                                    @php
                                        $location = [];

                                        if ($defaultStoreAddress) {
                                            // dump($defaultStoreAddress->toArray());
                                            if ($defaultStoreAddress->city) {
                                                $cityName = $defaultStoreAddress->city->name;
                                                $location[] = $cityName;
                                            }
                                            if ($defaultStoreAddress->province) {
                                                $location[] = $defaultStoreAddress->province->name;
                                            }
                                        }

                                        $storeLocation = '';
                                        $storeLocation = ucwords(strtolower(implode(', ', $location)));

                                        $locationFull = [];
                                        $storeLocationFull = '';
                                        if ($defaultStoreAddress) {
                                            if ($defaultStoreAddress->address) {
                                                $address = $defaultStoreAddress->address;
                                                $locationFull[] = $address;
                                            }
                                            if ($defaultStoreAddress->district) {
                                                $districtName = $defaultStoreAddress->district->name;
                                                $locationFull[] = $districtName;
                                            }
                                            if ($defaultStoreAddress->city) {
                                                $cityName = $defaultStoreAddress->city->name;
                                                $locationFull[] = $cityName;
                                            }
                                            if ($defaultStoreAddress->province) {
                                                $locationFull[] = $defaultStoreAddress->province->name;
                                            }
                                        }
                                        $storeLocationFull = ucwords(strtolower(implode(', ', $locationFull)));

                                    @endphp
                                    <p class="store__location">
                                        <span
                                            style="cursor: pointer;"
                                            data-toggle="popover"
                                            data-content="{{ $storeLocationFull }}">
                                            <i class="fa fa-map-marker-alt"></i>
                                            {{ $storeLocation }}
                                        </span>
                                    </p>
                                @endif
                            @endif

                            <div class="mt-4">
                                @include('modules.marketplaces.frontend.stores.inc.add-to-any-buttons')
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <h4 class="mt-0">Store Info</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-4">
                                <i class="fa fa-fw fa-clock"></i>
                                Joined : {{ $store->created_at->format('Y-m-d') }}
                                <br>
                                @if (isset($totalProducts))
                                    <i class="fa fa-fw fa-box"></i>
                                    Total Products :
                                    <span data-store-info="total-products">
                                            {{ $totalProducts }}
                                    </span>
                                    <br>
                                @endif
                                @if (isset($totalCategories))
                                    <i class="fa fa-fw fa-tags"></i>
                                    Total Categories :
                                    <span data-store-info="total-categories">
                                            {{ $totalCategories }}
                                    </span>
                                    <br>
                                @endif
                                <i class="fa fa-fw fa-eye"></i>
                                Views : <span data-store-info="views">loading...</span>
                                <br>
                                <i class="fa fa-fw fa-sign-in-alt"></i>
                                Last Online : <span data-store-info="last-online">loading...</span>
                                <br>
                                <i class="fa fa-fw fa-handshake"></i>
                                Successful Transactions : <span data-store-info="successful-transactions">loading...</span>
                                <br>
                                <i class="fa fa-fw fa-shopping-cart"></i>
                                Product Sold : <span data-store-info="product-sold">loading...</span>
                                <br>
                                @if (isset($storeShippingMethods))
                                    <i class="fa fa-fw fa-truck"></i>
                                    <span
                                        style="cursor:pointer;"
                                        data-toggle="modal" href='#modal-store-shipping-methods'>
                                        Shipping Methods
                                    </span>
                                    <br>
                                @endif

                                @php
                                    $storeMeta = $store->getAllMeta();
                                    $isUmkm = in_array($storeMeta['kelas_usaha'], ['Mikro', 'Kecil', 'Menengah']);
                                @endphp
                                <i class="fa fa-fw fa-store"></i>
                                Kategori Usaha : {{ $isUmkm ? 'UMKM' : $storeMeta['kelas_usaha'] }}
                                <br>
                                <i class="fa fa-fw fa-phone"></i>
                                No Telp : {{ $storeMeta['no_tel_kantor'] ?? '-' }}
                                <br>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="mb-4">
                                @php
                                    $storeInfos['facebook'] = ['url'=>null, 'name'=>'facebook'];
                                    $storeInfos['twitter'] = ['url'=>null, 'name'=>'twitter'];
                                    $storeInfos['youtube'] = ['url'=>null, 'name'=>'youtube'];
                                    $storeInfos['instagram'] = ['url'=>null, 'name'=>'instagram'];

                                    if (isset($storeSettings['facebook_url']) && $storeSettings['facebook_url']) {
                                        $storeInfos['facebook']['url'] = $storeSettings['facebook_url'];
                                        if (isset($storeSettings['facebook_name']) && $storeSettings['facebook_name']) {
                                            $storeInfos['facebook']['name'] = $storeSettings['facebook_name'];
                                        }
                                    }
                                    if (isset($storeSettings['twitter_url']) && $storeSettings['twitter_url']) {
                                        $storeInfos['twitter']['url'] = $storeSettings['twitter_url'];
                                        if (isset($storeSettings['twitter_name']) && $storeSettings['twitter_name']) {
                                            $storeInfos['twitter']['name'] = $storeSettings['twitter_name'];
                                        }
                                    }
                                    if (isset($storeSettings['youtube_url']) && $storeSettings['youtube_url']) {
                                        $storeInfos['youtube']['url'] = $storeSettings['youtube_url'];
                                        if (isset($storeSettings['youtube_name']) && $storeSettings['youtube_name']) {
                                            $storeInfos['youtube']['name'] = $storeSettings['youtube_name'];
                                        }
                                    }
                                    if (isset($storeSettings['instagram_url']) && $storeSettings['instagram_url']) {
                                        $storeInfos['instagram']['url'] = $storeSettings['instagram_url'];
                                        if (isset($storeSettings['instagram_name']) && $storeSettings['instagram_name']) {
                                            $storeInfos['instagram']['name'] = $storeSettings['instagram_name'];
                                        }
                                    }
                                @endphp
                                @if ($storeInfos['facebook']['url'])
                                    <i class="fab fa-fw fa-facebook"></i>
                                    <a href="{{ $storeInfos['facebook']['url'] }}">
                                        {{ $storeInfos['facebook']['name'] }}
                                    </a>
                                    <br>
                                @endif
                                @if ($storeInfos['twitter']['url'])
                                    <i class="fab fa-fw fa-twitter"></i>
                                    <a href="{{ $storeInfos['twitter']['url'] }}">
                                        {{ $storeInfos['twitter']['name'] }}
                                    </a>
                                    <br>
                                @endif
                                @if ($storeInfos['youtube']['url'])
                                    <i class="fab fa-fw fa-youtube"></i>
                                    <a href="{{ $storeInfos['youtube']['url'] }}">
                                        {{ $storeInfos['youtube']['name'] }}
                                    </a>
                                    <br>
                                @endif
                                @if ($storeInfos['instagram']['url'])
                                    <i class="fab fa-fw fa-instagram"></i>
                                    <a href="{{ $storeInfos['instagram']['url'] }}">
                                        {{ $storeInfos['instagram']['name'] }}
                                    </a>
                                    <br>
                                @endif
                                @if (isset($storeSettings['phone']) && $storeSettings['phone'])
                                    <i class="fa fa-fw fa-phone"></i> {{ $storeSettings['phone'] }}
                                    <br>
                                @endif
                                @if (isset($storeSettings['mobile_phone']) && $storeSettings['mobile_phone'])
                                    <i class="fa fa-fw fa-mobile"></i> {{ $storeSettings['mobile_phone'] }}
                                    <br>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            @include('modules.marketplaces.frontend.stores.inc.store-info-map')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if (isset($storeShippingMethods))
<div class="modal fade" id="modal-store-shipping-methods">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Shipping Methods</h4>
            </div>
            <div class="modal-body">
                @foreach ($storeShippingMethods as $shippingMethodGroup => $shippingMethods)
                    @foreach ($shippingMethods as $shippingMethodCode => $shippingMethodName)
                        @if ($shippingMethodGroup == 'rajaongkir')
                            @php
                                // $courierLogoUrl = 'https://via.placeholder.com/64x32/0000FF/FFFFFF?text='.strtoupper($shippingMethodCode);
                                $courierLogoUrl = asset('images/logo/external_couriers/'.strtolower($shippingMethodCode).'.png');
                            @endphp
                            <img
                                class="img-thumbnail mb-2 courier-logo"
                                title="{{ $shippingMethodName }}"
                                src="{{ $courierLogoUrl }}">
                        @endif
                    @endforeach
                    @if (!$loop->last)
                        <hr>
                    @endif
                @endforeach
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endif
