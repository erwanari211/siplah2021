<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-content">
                    <div class="d-inline-block mr-4">
                        Display
                        <a class="btn btn-xs btn-default" href="{{ $url }}">
                            <i class="fa fa-th"></i>
                        </a>
                        <a class="btn btn-xs btn-default" href="{{ $url }}?display=grid">
                            <i class="fa fa-list"></i>
                        </a>
                    </div>

                    <div class="d-inline-block mr-4">
                        Sort
                        {!! Form::select('products-sort', $dropdown['sortBy'], $sortBy, ['id'=>'products-sort']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
