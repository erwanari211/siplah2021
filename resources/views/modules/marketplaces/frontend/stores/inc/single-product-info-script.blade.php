@push('js')
    <script>
        $(document).ready(function () {
            function resizeLogoWrapper() {
                var cartContainerHeight = $('.product__add-to-cart-container').height();
                var qrLogoWrapper = $('.qrcode-logo-alt-wrapper');
                var qrLogo = $('#qrcode-logo-alt');
                qrLogoWrapper.height(cartContainerHeight);
                qrLogo.css({
                    height: cartContainerHeight / 2,
                    width: cartContainerHeight / 2
                });
            }

            resizeLogoWrapper();
            $(window).on('resize', function(event) {
                event.preventDefault();
                resizeLogoWrapper();
            });

            $('[data-toggle="popover"]').popover();

        });
    </script>
@endpush
