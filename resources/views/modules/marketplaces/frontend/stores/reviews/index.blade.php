@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    $templatePageTitle = 'Reviews at {store} | {APP}';
    $templatePageDescription = 'Get products you need from {store}. Find smiliar product at {marketplace}. Fast Delivery and secure payment at {APP}.';
    $searchArray = ['{store}', '{marketplace}', '{APP}'];
    $replaceArray = [$store->name, $marketplace->name, env('APP_NAME')];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .change-qty-container {
            width: 200px;
        }

        .qty-column {
            width: 150px;
        }

        .product-qty-container {
            width: 135px;
        }
    </style>
@endpush

@push('js')
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/bootstrap-number-input/bootstrap-number-input.js" ></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#store-category-sidebar').metisMenu({
                toggle: false,
            });

            $('.product-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin:15,
                stagePadding: 50,
                nav:true,
                navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                autoHeight:true,
                lazyLoad:true,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        stagePadding: 80,
                    },
                    480:{
                        items:2,
                        nav:false,
                    },
                    768:{
                        items:3
                    },
                    992:{
                        items:4
                    },
                    1200:{
                        items:4
                    }
                },
            });

            @include('modules.marketplaces.frontend.stores.inc.store-info-script')

            $('.product-qty').bootstrapNumber();

            $('[data-action="change-qty"]').on('click', function(event) {
                event.preventDefault();
                var qty = $('[data-id="qty"]').val();
                var isNumber = (isNaN(qty) || !qty.length) ? false : true;
                if (isNumber) {
                    $('.product-qty').val(qty);
                }
            });

        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-store')

    <div class="page-content">
        @include('modules.marketplaces.frontend.marketplaces.inc.marketplace-inactive-message')
        @include('modules.marketplaces.frontend.stores.inc.store-inactive-message')

        <div class="mb-3">
            <div class="container">
                @include('flash::message')

                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('stores.show', [$marketplace->slug, $store->slug]) }}">
                            {{ $store->name }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('stores.product-reviews.index', [$marketplace->slug, $store->slug]) }}">
                            Reviews
                        </a>
                    </li>
                </ol>
                @include('modules.marketplaces.frontend.stores.inc.store-info')
                @include('modules.marketplaces.frontend.stores.inc.store-menu-navbar')
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="box">
                        <div class="box-content">
                            <h3 class="mt-0">Reviews</h3>

                            <div class="product__discussions">
                                @if (count($storeReviews))
                                    @foreach ($storeReviews as $review)
                                        <div class="product__discussion">
                                            <div class="media">
                                                <a class="pull-left" href="{{ route('products.show', [$marketplace->slug, $store->slug, $review->product->slug]) }}">
                                                    <img class="media-object lazy"
                                                        data-src="{{ $review->product->image_url }}" alt="Image" height="50">
                                                </a>
                                                <div class="media-body">
                                                    <span class="media-heading">
                                                        {{ $review->user->name }}
                                                        on
                                                        <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $review->product->slug]) }}">
                                                            {{ $review->product->name }}
                                                        </a>
                                                    </span>
                                                    <br>
                                                    <small class="text-muted">
                                                        <i class="fa fa-clock"></i>
                                                        <em>{{ $review->updated_at }}</em>
                                                    </small>
                                                    <div class="mt-3">
                                                        <select class="rating" data-initial-rating="{{ $review->rating }}">
                                                          <option value="">0</option>
                                                          <option value="1">1</option>
                                                          <option value="2">2</option>
                                                          <option value="3">3</option>
                                                          <option value="4">4</option>
                                                          <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                    <p>{{ $review->content }}</p>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    @endforeach
                                    {{ $storeReviews->links() }}
                                @else
                                    <p>No review</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>





    </div>

    @include('modules.website.frontend.homepage.v01.inc.footer')

    @include('modules.marketplaces.frontend.inc.sidebar-store-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
