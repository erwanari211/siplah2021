@php
    $_themeSetting['skin'] = isset($marketplaceSettings['skin']) ? 'skin-'.$marketplaceSettings['skin'] : 'skin-blue';

    $templatePageTitle = 'Create store at {marketplace} | {APP}';
    $templatePageDescription = 'Get products you need from {marketplace}. Fast Delivery and secure payment at {APP}.';
    $searchArray = ['{marketplace}', '{APP}'];
    $replaceArray = [$marketplace->name, env('APP_NAME')];

    $_page['title'] = str_replace($searchArray, $replaceArray, $templatePageTitle);
    $_page['meta_description'] = str_replace($searchArray, $replaceArray, $templatePageDescription);

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;

@endphp

@extends('themes.marika-natsuki.main')

@section('title', $_page['title'])

@push('meta')
    <meta name="title" content="{{ $_page['title'] }}">
    <meta name="description" content="{{ $_page['meta_description'] }}">
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style type="text/css">
        .promo-title span {
            display: inline-block;
            border-bottom: 5px solid;
        }
    </style>
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#marketplace-category-sidebar').metisMenu({
                toggle: false,
            });

            $('.product-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin:15,
                stagePadding: 50,
                nav:true,
                navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                autoHeight:true,
                lazyLoad:true,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        stagePadding: 80,
                    },
                    480:{
                        items:2,
                        nav:false,
                    },
                    768:{
                        items:3
                    },
                    992:{
                        items:4
                    },
                    1200:{
                        items:4
                    }
                },
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-marketplace')

    <div class="page-content">

        <div class="container">

            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('marketplaces.show', [$marketplace->slug]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('stores.index', [$marketplace->slug]) }}">Stores</a>
                        </li>
                        <li class="active">Create Store</li>
                    </ol>
                    <h3>Create Store</h3>
                    @include('flash::message')
                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['stores.store', $marketplace->slug],
                                'method' => 'POST',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('name') !!}
                                {!! Form::bs3File('image') !!}
                                <span class="help-block">Recomended size 300 x 300</span>

                                {!! Form::bs3Submit('Save'); !!}

                                <a class="btn btn-default" href="{{ route('marketplaces.show', [$marketplace->slug]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    @include('modules.marketplaces.frontend.inc.sidebar-marketplace-category')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
