@php
    // $_themeSetting['skin'] = 'skin-red';
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Home')

@push('css')
    <style type="text/css">
        .pagination {
            margin-top: 0;
        }
    </style>
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-normal')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4>Marketplaces</h4>
                    @if (count($marketplaces))
                        <div class="row">
                            @foreach ($marketplaces as $marketplace)
                                <div class="col-xs-6 col-sm-3 col-md-2">
                                    <div class="box">
                                        <div class="box-content">
                                            <div class="icon-box">
                                                <a class="icon-box-link" href="{{ route('marketplaces.show', $marketplace->slug) }}">
                                                    <div class="icon-box-icon-container">
                                                        <img class="icon-box-image lazy " data-src="{{ asset($marketplace->image) }}">
                                                    </div>
                                                    <div class="icon-box-content">
                                                        <span class="icon-box-title line-clamp-2">{{ $marketplace->name }}</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    <a class="btn btn-default" href="{{ route('marketplaces.create') }}">
                        <i class="fa fa-plus"></i>
                        Create Marketplace
                    </a>
                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
