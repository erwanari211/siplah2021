@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Create Banner')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@endpush

@push('js')
    <script src="{{ asset('assets/adminlte-2.4.5/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script>
        $(document).ready(function() {
          $('.datetimepicker').datetimepicker();
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.marketplaces.banners.index', [$marketplace->id]) }}">Banners</a>
                        </li>
                        <li class="active">Create</li>
                    </ol>

                    <h3>Create Banner</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['users.marketplaces.banners.store', $marketplace->id],
                                'method' => 'POST',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3File('image') !!}
                                <span class="help-block">Recomended size 1200 x 400</span>
                                {!! Form::bs3Text('link') !!}
                                {!! Form::bs3Select('active', $dropdown['yes_no'], 1) !!}
                                {!! Form::bs3Textarea('note') !!}
                                {!! Form::bs3Datetimepicker('expired_at', date('Y-m-t 24:00')) !!}
                                {!! Form::bs3Number('sort_order', 1) !!}

                                {!! Form::bs3Submit('Save'); !!}
                                <a class="btn btn-default" href="{{ route('users.marketplaces.banners.index', [$marketplace->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
