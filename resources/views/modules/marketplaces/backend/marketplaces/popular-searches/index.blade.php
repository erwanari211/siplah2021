@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Popular Searches')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.marketplaces.popular-searches.index', [$marketplace->id]) }}">
                                Popular Searches
                            </a>
                        </li>
                    </ol>

                    <h3>Popular Searches</h3>

                    @include('flash::message')
                    @include('themes.marika-natsuki.messages')
                </div>
                <div class="col-md-6">
                    <h4>Marketplace Popular search</h4>
                    <a class="btn btn-primary" href="{{ route('users.marketplaces.popular-searches.create', [$marketplace->id]) }}">
                        Add
                    </a>
                    <a class="btn btn-default" href="{{ route('users.marketplaces.popular-searches.imports', [$marketplace->id]) }}"
                        onclick="return confirm('Import data will delete all current popular search. Continue?')">
                        Import
                    </a>
                    <div class="box mt-3">
                        <div class="box-content">
                            <p>Only 5 top popular search that will be displayed at frontend</p>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered mb-0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Option</th>
                                            <th>Search</th>
                                            <th>Sort Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($popularSearches))
                                            @php
                                                $i = $popularSearches->firstItem();
                                            @endphp
                                            @foreach ($popularSearches as $item)
                                                <tr>
                                                    <td>{{ $i }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-success"
                                                            href="{{ route('users.marketplaces.popular-searches.edit', [$marketplace->id, $item->id]) }}" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['users.marketplaces.popular-searches.destroy', $marketplace->id, $item->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td>{{ $item->search }}</td>
                                                    <td>{{ $item->sort_order }}</td>
                                                </tr>
                                                @php
                                                    $i++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="4">No Data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $popularSearches->links() }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4>Popular search this month</h4>
                    <div class="box mt-3">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered mb-0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Search</th>
                                            <th>Total Searches</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($popularSearchThisMonth))
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach ($popularSearchThisMonth as $item)
                                                <tr>
                                                    <td>{{ $i }}</td>
                                                    <td>
                                                        <a class="btn btn-default btn-xs"
                                                            href="{{ route('users.marketplaces.popular-searches.import', [
                                                                $marketplace->id,
                                                                'search' => $item->search,
                                                            ]) }}">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </td>
                                                    <td>{{ $item->search }}</td>
                                                    <td>{{ $item->total_search }}</td>
                                                </tr>
                                                @php
                                                    $i++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="3">No Data</td>
                                            </tr>
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
