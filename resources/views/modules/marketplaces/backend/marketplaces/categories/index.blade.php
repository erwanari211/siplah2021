@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Categories')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li class="active">
                            <a href="{{ route('users.marketplaces.categories.index', [$marketplace->id]) }}">Categories</a>
                        </li>
                    </ol>

                    <h3>Categories</h3>

                    @include('flash::message')

                    <a class="btn btn-primary" href="{{ route('users.marketplaces.categories.create', [$marketplace->id]) }}">
                        Add Category
                    </a>
                    <a class="btn btn-default" href="{{ route('users.marketplaces.categories.index', [
                        $marketplace->id, 'parent_id'=>0
                        ]) }}">
                        View Parent Only
                    </a>
                    <a class="btn btn-default" href="{{ route('users.marketplaces.categories.index', [
                        $marketplace->id
                        ]) }}">
                        View All
                    </a>

                    <div class="box mt-4">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Sort Order</th>
                                            <th>Active</th>
                                            <th>Is Parent</th>
                                            <th>Childs</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($categories))
                                            @php
                                                $no = $categories->firstItem();
                                            @endphp
                                            @foreach ($categories as $category)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-success"
                                                            href="{{ route('users.marketplaces.categories.edit', [$marketplace->id, $category->id]) }}" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['users.marketplaces.categories.destroy', $marketplace->id, $category->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                        @if ($category->parent_id == 0)
                                                            <a class="btn btn-default btn-xs"
                                                                title="View subcategories"
                                                                href="{{ route('users.marketplaces.categories.index', [
                                                                    $marketplace->id,
                                                                    'parent_id'=>$category->id
                                                                    ]) }}">
                                                                <i class="fa fa-sitemap"></i>
                                                            </a>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <img src="{{ asset($category->image) }}" width="50" height="50">
                                                    </td>
                                                    <td>
                                                        {{ $category->name }}
                                                        @if ($category->parent_id != 0)
                                                            <br>
                                                            <span class="small text-muted">
                                                                ({{ $category->parent->name }})
                                                            </span>
                                                        @endif
                                                    </td>
                                                    <td>{{ $category->sort_order }}</td>
                                                    <td>{{ $category->active ? 'Yes' : 'No' }}</td>
                                                    <td>{{ $category->parent_id == 0 ? 'Yes' : 'No' }} </td>
                                                    <td>{{ count($category->childs) }} </td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $categories->appends(request()->only(['parent_id']))->links() }}
                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
