@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Marketplace Dashboard')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style>
        .info-box-label {
            display: block;
        }
        .info-box-count {
            display: block;
            font-weight: bold;
            font-size: 32px;
        }
        .info-box-total {
            display: block;
            font-weight: bold;
            color: #777;
        }
    </style>
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li class="active">Dashboard</li>
                    </ol>

                    <h3>Marketplace Dashboard</h3>

                    @if (!$marketplace->active)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Marketplace is inactive. Waiting admin approval.
                        </div>
                    @endif

                    @if (!$hasCategory)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Marketplace dont have category. Please create category.
                        </div>
                    @endif


                </div>
            </div>

            @include('modules.marketplaces.backend.marketplaces.dashboard.inc.data')

            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <h4>Popular Search</h4>
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs border-bottom-nav">
                            <li class="active">
                                <a href="#tab-popular-search-weekly" data-toggle="tab">Weekly</a>
                            </li>
                            <li>
                                <a href="#tab-popular-search-monthly" data-toggle="tab">Monthly</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in pt-4 active" id="tab-popular-search-weekly">
                                <div class="box">
                                    <div class="box-content">
                                        <table class="table table-bordered table-hover mb-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Search</th>
                                                    <th>Views</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($popularSearchThisWeek))
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach ($popularSearchThisWeek as $item)
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $item->search }}</td>
                                                            <td>{{ $item->total_search }}</td>
                                                        </tr>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No Data</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade pt-4" id="tab-popular-search-monthly">
                                <div class="box">
                                    <div class="box-content">
                                        <table class="table table-bordered table-hover mb-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Search</th>
                                                    <th>Views</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($popularSearchThisMonth))
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach ($popularSearchThisMonth as $item)
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $item->search }}</td>
                                                            <td>{{ $item->total_search }}</td>
                                                        </tr>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No Data</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-4 col-sm-6"></div>
                <div class="col-md-4 col-sm-6"></div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
