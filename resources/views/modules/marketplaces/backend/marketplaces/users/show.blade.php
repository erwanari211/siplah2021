@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'User Detail')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.marketplaces.users.index', [$marketplace->id]) }}">Users</a>
                        </li>
                        <li class="active">Detail</li>
                    </ol>

                    <h3>User Detail</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($user, [
                                'route' => ['users.marketplaces.users.update', $marketplace->id, $user->id],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('name', null, ['disabled']) !!}
                                {!! Form::bs3Text('email', null, ['disabled']) !!}
                                {!! Form::bs3Select('status', $dropdown['userStatuses']) !!}
                                {!! Form::bs3Textarea('reason') !!}

                                {!! Form::bs3Submit('Save'); !!}
                                <a class="btn btn-default"
                                    href="{{ route('users.marketplaces.users.index', $marketplace->id) }}">
                                    Back
                                </a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
