@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Users')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li class="active">Users</li>
                    </ol>

                    <h3>Users</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['users.marketplaces.users.index', $marketplace->id],
                                'method' => 'GET',
                                'files' => true,
                                ]) !!}

                                <div class="row">
                                    <div class="col-sm-4">
                                        {!! Form::bs3Text('name', request('name')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! Form::bs3Text('email', request('email')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! Form::bs3Select('online_status', $dropdown['onlineStatuses'], request('online_status')) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! Form::bs3Select('status', $dropdown['statuses'], request('status')) !!}
                                    </div>
                                </div>

                                {!! Form::bs3Submit('Filter'); !!}
                                <a class="btn btn-default"
                                    href="{{ route('users.marketplaces.users.index', $marketplace->id) }}">
                                    Reset
                                </a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                    @php
                        $userTotal = $users->total();
                    @endphp
                    <h4>
                        Found {{ $userTotal }} {{ str_plural('result', $userTotal) }}
                    </h4>

                    <div class="box">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Is Online</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($users))
                                            @php
                                                $no = $users->firstItem();
                                            @endphp
                                            @foreach ($users as $user)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-sm btn-success" href="{{ route('users.marketplaces.users.edit', [$marketplace->id, $user->id]) }}">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        @if ($user->photo)
                                                            <img src="{{ asset($user->photo) }}" width="50" height="50">
                                                        @endif
                                                    </td>
                                                    <td> {{ $user->name }} </td>
                                                    <td> {{ $user->email }} </td>
                                                    <td>
                                                        @if ($user->checkIsOnline())
                                                            <i class="fa fa-fw fa-circle text-success"></i>
                                                            Online
                                                        @endif

                                                        @if (!$user->checkIsOnline())
                                                            <i class="fa fa-fw fa-circle"></i>
                                                            Offline
                                                        @endif

                                                    </td>
                                                    <td>{{ $user->status }}</td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $users->appends(request()->only(['filter', 'name', 'email', 'online_status', 'status']))->links() }}
                        </div>
                    </div>



                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
