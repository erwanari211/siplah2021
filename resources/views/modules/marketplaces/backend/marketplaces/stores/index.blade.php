@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Stores')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li class="active">Stores</li>
                    </ol>

                    <h3>Stores</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            @foreach ($statuses as $status => $label)
                                <a class="btn {{ $status == $storeFilter ? 'btn-primary' : 'btn-default'}}"
                                    href="{{ route('users.marketplaces.stores.index', [$marketplace->id, 'filter'=>$status]) }}">
                                    {{ ucwords($label) }}
                                </a>
                            @endforeach
                        </div>
                    </div>

                    <div class="box">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Owner</th>
                                            <th>Active</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($stores))
                                            @php
                                                $no = $stores->firstItem();
                                            @endphp
                                            @foreach ($stores as $store)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-default btn-xs" href="{{ route('users.marketplaces.stores.show', [$marketplace->id, $store->id]) }}">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                        @if (!$store->active)
                                                            {!! Form::open([
                                                                    'route' => ['users.marketplaces.stores.active.store', $marketplace->id, $store->id],
                                                                    'method' => 'POST',
                                                                    'style' => 'display: inline-block;'
                                                                ]) !!}
                                                                {!! Form::bs3SubmitHtml('<i class="fa fa-check"></i> Approve', ['class'=>'btn btn-xs btn-success', 'onclick'=>'return confirm("Approve?")']); !!}
                                                            {!! Form::close() !!}
                                                        @else
                                                            {!! Form::open([
                                                                    'route' => ['users.marketplaces.stores.active.destroy', $marketplace->id, $store->id],
                                                                    'method' => 'DELETE',
                                                                    'style' => 'display: inline-block;'
                                                                ]) !!}
                                                                {!! Form::bs3SubmitHtml('<i class="fa fa-times"></i> Disapprove', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Disapprove?")']); !!}
                                                            {!! Form::close() !!}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <img src="{{ asset($store->image) }}" width="50" height="50">
                                                    </td>
                                                    <td>
                                                        <a href="{{ $store->store_url }}">
                                                            {{ $store->name }}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        {{ $store->user->name }} <br>
                                                        {{ $store->user->email }}
                                                    </td>
                                                    <td>{{ $store->active ? 'Yes' : 'No' }}</td>
                                                    <td>{!! $store->status_button !!}</td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $stores->appends(request()->only(['filter']))->links() }}
                        </div>
                    </div>



                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
