@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Detail Store')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style>
        .w-150 { width: 150px !important; }
        .w-200 { width: 200px !important; }
    </style>
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.marketplaces.stores.index', [$marketplace->id]) }}">Stores</a>
                        </li>
                        <li class="active">Detail</li>
                    </ol>

                    <h3>Store Detail</h3>

                    @include('flash::message')


                    <div class="box">
                        <div class="box-content">

                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th colspan="2" class="text-center"> Profile </th>
                                    </tr>

                                    <tr>
                                        <td class="w-150">Nama Toko</td>
                                        <td>{{ $store->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Usaha</td>
                                        <td>{{ $dropdown['jenisUsaha'][$storeMeta['jenis_usaha'] ?? '-'] ?? '-' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kelas Usaha</td>
                                        <td>{{ $dropdown['kelasUsaha'][$storeMeta['kelas_usaha'] ?? '-'] ?? '-' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Status Usaha</td>
                                        <td>{{ $dropdown['statusUsaha'][$storeMeta['status_usaha'] ?? '-'] ?? '-' }}</td>
                                    </tr>
                                    <tr>
                                        <td>NPWP</td>
                                        <td>{{ $storeMeta['npwp'] ?? '-'  }}</td>
                                    </tr>
                                    <tr>
                                        <td>SIP/NIB</td>
                                        <td>{{ $storeMeta['siup_nib'] ?? '-' }}</td>
                                    </tr>
                                    <tr>
                                        <td>TDP</td>
                                        <td>{{ $storeMeta['tdp'] ?? '-' }}</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td>Provinsi</td>
                                        <td>{{ $dropdown['provinces'][$storeMeta['province_id'] ?? '-'] ?? '-' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kab / Kota</td>
                                        <td>{{ $dropdown['cities'][$storeMeta['city_id'] ?? '-'] ?? '-' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>{{ $dropdown['districts'][$storeMeta['district_id'] ?? '-'] ?? '-' }}</td>
                                    </tr>

                                    <tr>
                                        <td>Alamat</td>
                                        <td>{{ $storeMeta['alamat'] ?? '-' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Geolokasi</td>
                                        <td>{{ $storeMeta['geolokasi'] ?? '-' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kode pos</td>
                                        <td>{{ $storeMeta['kode_pos'] ?? '-' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email Toko</td>
                                        <td>{{ $storeMeta['email_toko'] ?? '-' }}</td>
                                    </tr>

                                </tbody>
                            </table>

                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th colspan="2" class="text-center"> Penanggung Jawab </th>
                                    </tr>

                                    <tr>
                                        <td class="w-150">Nama Lengkap</td>
                                        <td>{{ $storeMeta['nama_lengkap_penanggungjawab'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>{{ $storeMeta['email_penanggungjawab'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jabatan</td>
                                        <td>{{ $storeMeta['jabatan_penanggungjawab'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>NIK/KTP</td>
                                        <td>{{ $storeMeta['nik_penanggungjawab'] }}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th colspan="2" class="text-center"> Bank </th>
                                    </tr>

                                    <tr>
                                        <td class="w-200">Nama Bank</td>
                                        <td>{{ $storeMeta['nama_bank'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nama Pemilik Rekening</td>
                                        <td>{{ $storeMeta['nama_pemilik_rekening'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nomor Rekening</td>
                                        <td>{{ $storeMeta['no_rekening'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Cabang Bank</td>
                                        <td>{{ $storeMeta['cabang_bank'] }}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th colspan="2" class="text-center"> File Lampiran </th>
                                    </tr>

                                    <tr>
                                        <td class="w-200">KTP Penanggungjawab</td>
                                        <td>
                                            <a target="_blank" href="{{ asset($storeMeta['scan_ktp_penanggung_jawab'] ?? '#') }}">
                                                <i class="fa fa-download"></i>
                                                Download
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>NPWP</td>
                                        <td>
                                            <a target="_blank" href="{{ asset($storeMeta['scan_npwp'] ?? '#') }}">
                                                <i class="fa fa-download"></i>
                                                Download
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>SIUP/NIB</td>
                                        <td>
                                            <a target="_blank" href="{{ asset($storeMeta['scan_siup'] ?? '#') }}">
                                                <i class="fa fa-download"></i>
                                                Download
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>TDP</td>
                                        <td>
                                            <a target="_blank" href="{{ asset($storeMeta['scan_tdp'] ?? '#') }}">
                                                <i class="fa fa-download"></i>
                                                Download
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box-content">
                            <h5>Aktivasi Penyedia</h5>

                            {!! Form::open([
                                'route' => ['users.marketplaces.stores.update', $marketplace->id, $store->id],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Select('status', $dropdown['storeStatuses'], $store->status) !!}
                                {!! Form::bs3Textarea('note', $storeMeta['store_status_note'] ?? '', ['label' => 'Catatan']) !!}

                                {!! Form::bs3Submit('Save'); !!}
                                <a class="btn btn-default" href="{{ route('users.marketplaces.stores.index', [$marketplace->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>



                </div>
            </div>
        </div>




    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
