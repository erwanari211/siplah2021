@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Marketplace Settings')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script src="{{ asset('assets/plugins') }}/tinymce_4.7.9/tinymce/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            /**
             * Tinymce settings
             */
            var plugins = 'preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern code help spellchecker';
            var toolbar1 = 'formatselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat';
            var toolbar2 = 'undo redo | link unlink image media styleselect | nanospell | fullscreen preview code ';

            tinymce.init({
                selector: '#marketplace_description',
                height: 300,
                plugins: plugins,
                toolbar1: toolbar1,
                toolbar2: toolbar2,
                image_advtab: true,
                selection_toolbar: 'bold italic | quicklink h2 h3 blockquote',
                imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",
                content_css: [
                  '//www.tinymce.com/css/codepen.min.css',
                  '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'
                ],

                relative_urls : false,
                remove_script_host : false,
                convert_urls : true,
                image_class_list: [
                    {title: 'Lazyload', value: 'lazy'},
                    {title: 'None', value: ''},
                ],

                table_default_attributes: {
                    'class': 'table table-bordered'
                },
                table_default_styles: {},
                table_class_list: [
                    {title: 'None', value: ''},
                    {title: 'Table Bordered', value: 'table table-bordered'},
                ],

                menubar:false,
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="#"> Settings </a>
                        </li>
                        <li class="active">Marketplace Settings</li>
                    </ol>

                    <h3>Marketplace Settings</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            @php
                                $defaultSkin = 'blue';
                                if (isset($marketplaceSettings['skin']) && $marketplaceSettings['skin']) {
                                    $skin = $marketplaceSettings['skin'];
                                } else {
                                    $skin = $defaultSkin;
                                }
                            @endphp

                            {!! Form::model($marketplaceSettings, [
                                'route' => ['users.marketplaces.settings.marketplace-settings.update', $marketplace->id],
                                'method' => 'PUT',
                                'files' => false,
                                ]) !!}

                                {!! Form::bs3Select('skin', $dropdown['skins'], $skin) !!}
                                {!! Form::bs3Select('store_auto_approve', $dropdown['yes_no']) !!}
                                {!! Form::bs3Textarea('marketplace_description') !!}

                                <hr>

                                <h4>Payment</h4>
                                {!! Form::bs3Text('payment_bank_name', null, ['label' => 'Nama Bank']) !!}
                                {!! Form::bs3Text('payment_account_name', null, ['label' => 'Nama Pemilik Rekening']) !!}
                                {!! Form::bs3Text('payment_account_number', null, ['label' => 'Nomor Rekening']) !!}
                                {!! Form::bs3Text('payment_bank_branch', null, ['label' => 'Cabang Bank']) !!}

                                <hr>

                                <h4>Social Media</h4>
                                {!! Form::bs3Text('facebook_name') !!}
                                {!! Form::bs3Text('facebook_url') !!}
                                {!! Form::bs3Text('twitter_name') !!}
                                {!! Form::bs3Text('twitter_url') !!}
                                {!! Form::bs3Text('youtube_name') !!}
                                {!! Form::bs3Text('youtube_url') !!}
                                {!! Form::bs3Text('instagram_name') !!}
                                {!! Form::bs3Text('instagram_url') !!}

                                {!! Form::bs3Submit('Update'); !!}
                                <a class="btn btn-default" href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
