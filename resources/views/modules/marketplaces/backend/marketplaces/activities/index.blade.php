@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Activities')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style>
        .mw-250 {
            max-width: 250px;
            display: inline-block;
        }
    </style>
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li class="active">
                            Activities
                        </li>
                    </ol>

                    <h3>Activities</h3>

                    @include('flash::message')

                    <div class="box mt-3">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Name</th>
                                            <th>Aktivitas</th>
                                            <th>Keterangan</th>
                                            <th>Waktu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($activities))
                                            @php
                                                $no = $activities->firstItem();
                                            @endphp
                                            @foreach ($activities as $activity)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        {{ $activity->user->name }}
                                                        <br>
                                                        <small class="text-muted">({{ $activity->user->email }})</small>
                                                    </td>
                                                    <td>{{ $activity->name }}</td>
                                                    <td>
                                                        <strong>
                                                            {{ $activity->ip }}
                                                        </strong>
                                                        <br>
                                                        <small class="mw-250">
                                                            {{ $activity->user_agent }}
                                                        </small>
                                                    </td>
                                                    <td>{{ $activity->created_at->format('Y-m-d H:i:s') }}</td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="9">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $activities->links() }}
                        </div>
                    </div>



                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
