@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Products')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
@endpush

@push('js')
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.select2').select2({
                // placeholder: 'Please Select'
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li class="active">Products</li>
                    </ol>

                    <h3>Products</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['users.marketplaces.products.index', $marketplace->id],
                                'method' => 'GET',
                                'files' => true,
                                ]) !!}

                                <div class="row">
                                    <div class="col-sm-12 hide">
                                        @if (request('active'))
                                            {!! Form::bs3Text('active', request('active')) !!}
                                        @endif
                                        @if (request('status'))
                                            {!! Form::bs3Text('status', request('status')) !!}
                                        @endif

                                        {!! Form::bs3Text('approved_status', $filters['approved_status'] ?? request('approved_status')) !!}
                                        {!! Form::bs3Text('store_id', request('store_id')) !!}

                                    </div>
                                    <div class="col-sm-6">
                                        {!! Form::bs3Text('name', request('name')) !!}
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Form::bs3Text('sku', request('sku')) !!}
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Form::bs3Select('category_id', $dropdown['categories'], request('category_id'), [
                                            'class'=>'form-control select2', 'label'=>'Category'
                                        ]) !!}
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Form::bs3Text('store_name', request('store_name')) !!}
                                    </div>

                                </div>

                                {!! Form::bs3Submit('Filter'); !!}
                                <a class="btn btn-default"
                                    href="{{ route('users.marketplaces.products.index', $marketplace->id) }}">
                                    Reset
                                </a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="box">
                        <div class="box-content">
                            <div style="display: inline-block;">
                                <strong>Approved Status</strong> <br>
                                @foreach ($approvedStatuses as $status => $label)
                                    @php
                                        $routeParams = array_merge($filters, ['approved_status'=>$status]);
                                        $routeParams = [$marketplace->id] + $routeParams;
                                        $class = $filters['approved_status'] == $status ? 'btn btn-primary' : 'btn btn-default';
                                    @endphp
                                    <a class="{{ $class }}"
                                        href="{{ route('users.marketplaces.products.index', $routeParams) }}">
                                        {{ $label }}
                                    </a>
                                @endforeach
                                {!! nbsp(3, true) !!}
                            </div>

                            <div style="display: inline-block;">
                                <br>
                                <a class="btn btn-default"
                                    href="{{ route('users.marketplaces.products.index', $marketplace->id) }}">
                                    Reset
                                </a>
                            </div>
                        </div>
                    </div>

                    @php
                        $productTotal = $products->total();
                    @endphp
                    <h4>
                        Found {{ $productTotal }} {{ str_plural('result', $productTotal) }}
                    </h4>

                    <div class="box">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Image</th>
                                            <th>Store</th>
                                            <th>Name</th>
                                            <th>SKU</th>
                                            <th>Price</th>
                                            <th>Category</th>
                                            <th>Approved</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($products))
                                            @php
                                                $no = $products->firstItem();
                                            @endphp
                                            @foreach ($products as $product)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>

                                                        <a class="btn btn-xs btn-default"
                                                            href="{{ route('users.marketplaces.products.show', [$marketplace->id, $product->id]) }}" title="View">
                                                            <i class="fa fa-eye"></i>
                                                        </a>

                                                        <a class="btn btn-xs btn-default"
                                                            href="{{ route('products.show', [$marketplace->slug, $product->store_id, $product->slug]) }}"
                                                            target="_blank">
                                                            <i class="fa fa-external-link-alt"></i>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <img src="{{ $product->image_url }}" width="50" height="50">
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('users.marketplaces.products.index', [
                                                                $marketplace->id,
                                                                'store_id' => $product->store_id,
                                                                'approved_status' => $filters['approved_status'] ?? request('approved_status'),
                                                            ]) }}">
                                                            {{ $product->store->name }}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('users.marketplaces.products.show', [$marketplace->id, $product->id]) }}">
                                                            {{ $product->name }}
                                                        </a>
                                                    </td>
                                                    <td>{{ $product->sku }}</td>
                                                    <td>
                                                        {{ formatNumber($product->price) }}
                                                        @if ($product->getMeta('has_kemdikbud_zone_price'))
                                                            <br>
                                                            <i class="fa fa-globe"
                                                                data-toggle="tooltip"
                                                                title="Has zone prices"></i>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if (count($product->marketplaceCategories))
                                                            @foreach ($product->marketplaceCategories as $productCategory)
                                                                <span class="btn btn-primary btn-xs">{{ $productCategory->name }}</span>
                                                            @endforeach
                                                        @endif
                                                    </td>

                                                    <td>
                                                        {!! $product->approved_button !!}
                                                    </td>
                                                    <td>
                                                        {!! $product->approved_status_button !!}
                                                    </td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="11">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{
                                $products->appends(request()->only([
                                    'status', 'active',
                                    'name', 'sku',
                                    'collection_id', 'category_id', 'approved_status',
                                    'store_name', 'store_id',
                                ]))->links()
                            }}
                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
