@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Products')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script>
        $(document).ready(function () {
            function in_array(needle, haystack = []) {
                if (Array.isArray(haystack)) {
                    return haystack.indexOf(needle) != -1;
                }

                return false;
            }

            $('#type').on('change', function () {
                handleTypeOnChange();
            });

            handleTypeOnChange();

            function handleTypeOnChange() {
                var type = $('#type').val();

                $('[data-id="product-meta-wrapper"]').hide();
                $('[data-id="service-meta-wrapper"]').hide();

                var productTypes = ['product', 'grouped product', 'digital product'];
                var serviceTypes = ['service'];

                if (in_array(type, productTypes)) {
                    $('[data-id="product-meta-wrapper"]').show();
                }

                if (in_array(type, serviceTypes)) {
                    $('[data-id="service-meta-wrapper"]').show();
                }
            }
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.marketplaces.products.index', [$marketplace->id]) }}">Products</a>
                        </li>
                        <li class="active">Detail</li>
                    </ol>

                    <h3>Product</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($product, [
                                'url' => route('users.marketplaces.products.update', [$marketplace->id, $product->id]),
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Select('status', $dropdown['approvedStatuses'], $product->approved_status) !!}
                                {!! Form::bs3Textarea('reason', null) !!}

                                {!! Form::bs3Submit('Submit'); !!}
                                <a class="btn btn-default" href="{{ route('users.marketplaces.products.index', [$marketplace->id]) }}">Back</a>
                            {!! Form::close() !!}

                        </div>
                    </div>

                    @php
                        $productMetas = $product->getAllMeta();
                        $originalPrice = isset($productMetas['original_price']) ? $productMetas['original_price'] : 0;
                        $discountAmountPercent = $product->discountAmountPercent;
                    @endphp

                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs border-bottom-nav">
                            <li class="active">
                                <a href="#tab-detail" data-toggle="tab">Detail</a>
                            </li>
                            <li>
                                <a href="#tab-attributes" data-toggle="tab">Attributes</a>
                            </li>
                            <li>
                                <a href="#tab-galleries" data-toggle="tab">Galleries</a>
                            </li>
                            @if ($product->type == 'grouped product')
                                <li>
                                    <a href="#tab-grouped-product" data-toggle="tab">Group Product</a>
                                </li>
                            @endif
                            @if ($hasKemdikbudZonePrice)
                                <li>
                                    <a href="#tab-zone-price" data-toggle="tab">Zone Price</a>
                                </li>
                            @endif
                        </ul>



                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active pt-4" id="tab-detail">
                                <div class="box">
                                    <div class="box-content">
                                        {!! Form::model($product, [
                                            'url' => '#',
                                            'method' => 'POST',
                                            'files' => true,
                                            ]) !!}

                                            {!! Form::bs3Select('is_approved', $dropdown['yes_no'], null, ['disabled']) !!}

                                            {!! Form::bs3Text('name') !!}
                                            {!! Form::bs3Text('slug') !!}
                                            {!! Form::bs3Text('sku', null, ['label'=>'SKU']) !!}
                                            {!! Form::bs3Textarea('description') !!}
                                            {!! Form::bs3Number('price') !!}
                                            {!! Form::bs3Number('original_price', $originalPrice) !!}
                                            {!! Form::bs3Number('discount', $discountAmountPercent, ['readonly'=>'readonly', 'label'=>'Discount (%)']) !!}

                                            {{--
                                            {!! Form::bs3Select('category[]', $dropdown['marketplaceCategories'], $productMarketplaceCategories, [
                                                'multiple'=>'multiple', 'class'=>'form-control select2', 'label'=>'Category', 'readonly'=>'readonly'
                                            ]) !!}
                                            {!! Form::bs3Select('collection[]', $dropdown['storeCategories'], $productStoreCategories, [
                                                'multiple'=>'multiple', 'class'=>'form-control select2', 'label'=>'Collection'
                                            ]) !!}
                                            --}}

                                            <hr>

                                            <div data-role="category-wrapper">
                                                {!! Form::bs3Select('category[]', $dropdown['marketplaceCategories'], $productMarketplaceCategories, [
                                                    'class'=>'form-control select2', 'label'=>'Category',
                                                    'style' => 'width:100%',
                                                    'data-level' => 0,
                                                    'data-role' => 'dropdown-category',
                                                ]) !!}

                                                @php
                                                    $level = 1;
                                                @endphp
                                                @foreach ($oldDropdowns['data'] as $oldDropdown)
                                                    @php
                                                        $oldDropdown = collect($oldDropdown['data']);
                                                        $oldDropdownPlucked = $oldDropdown->pluck('name', 'id');
                                                    @endphp
                                                    @if (count($oldDropdownPlucked))
                                                        {!! Form::bs3Select('category[]', $oldDropdownPlucked, $productMarketplaceCategories, [
                                                            'class'=>'form-control select2',
                                                            'label'=>'Subcategory ' . $level,
                                                            'style' => 'width:100%',
                                                            'data-level' => $level,
                                                            'data-role' => 'dropdown-category',
                                                            'id' => 'category-level-'.$level,
                                                        ]) !!}
                                                    @endif
                                                    @php
                                                        $level++;
                                                    @endphp
                                                @endforeach
                                            </div>

                                            <hr>

                                            {!! Form::bs3Number('qty') !!}
                                            {!! Form::bs3Number('weight', null, ['label'=>'Weight (gr)', 'step'=>'0.01']) !!}
                                            {!! Form::bs3File('image') !!}
                                            <div class="form-group ">
                                                <img class="img-thumbnail" src="{{ asset($product->image) }}" width="100" height="100">
                                            </div>

                                            <hr>

                                            {!! Form::bs3Select('type', $dropdown['types']) !!}
                                            {!! Form::bs3Select('active', $dropdown['yes_no']) !!}
                                            {!! Form::bs3Select('status', $dropdown['statuses']) !!}

                                            <hr>

                                            {!! Form::bs3Select('is_local_product', $dropdown['yes_no'], null, ['label' => 'Produk Dalam Negeri']) !!}

                                            <div data-id="product-meta-wrapper">
                                                {!! Form::bs3Select('product_is_msme_product', $dropdown['yes_no'], $product->is_msme_product, ['label' => 'Barang UMKM']) !!}
                                                {!! Form::bs3Select('meta_data[product_jaminan_kebaruan]', $dropdown['yes_no'], null, ['label' => 'Jaminan Kebaruan']) !!}
                                                {!! Form::bs3Select('meta_data[product_garansi]', $dropdown['yes_no'], null, ['label' => 'Garansi']) !!}
                                                {!! Form::bs3Select('meta_data[product_status_ketersediaan]', $dropdown['product_status_ketersediaan'], null, ['label' => 'Status Ketersediaan']) !!}
                                                {!! Form::bs3Number('meta_data[product_minimal_order]', null, ['label' => 'Minimal Order']) !!}
                                            </div>

                                            <div data-id="service-meta-wrapper">
                                                {!! Form::bs3Select('service_is_msme_product', $dropdown['yes_no'], $product->is_msme_product, ['label' => 'Jasa UMKM']) !!}
                                                {!! Form::bs3Select('meta_data[service_jasa_kemendikbud]', $dropdown['yes_no'], null, ['label' => 'Jasa Kemendikbud']) !!}
                                                {!! Form::bs3Select('meta_data[service_garansi]', $dropdown['yes_no'], null, ['label' => 'Garansi']) !!}
                                                {!! Form::bs3Text('meta_data[service_waktu_cara_pelaksanaan]', null, ['label' => 'Waktu dan Cara Pelaksanaan']) !!}
                                                {!! Form::bs3Select('meta_data[service_jaminan_pelaksanaan]', $dropdown['yes_no'], null, ['label' => 'Jaminan Pelaksanaan']) !!}
                                                {!! Form::bs3Text('meta_data[service_status_verifikasi_jasa]', null, ['label' => 'Status Verifikasi Jasa']) !!}
                                            </div>

                                            <hr>

                                            {!! Form::bs3Select('has_kemdikbud_zone_price', $dropdown['yes_no'], $hasKemdikbudZonePrice, ['label'=>'Zone Price']) !!}

                                            <a class="btn btn-default" href="{{ route('users.marketplaces.products.index', [$store->id]) }}">Back</a>

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane pt-4" id="tab-attributes">
                                <div class="box">
                                    <div class="box-content">
                                        <table class="table table-bordered table-hover mt-4 mb-0">
                                            <tbody>
                                                @if (count($groupedAttributes))
                                                    <tr>
                                                        <th>Options</th>
                                                        <th>Key</th>
                                                        <th>Value</th>
                                                    </tr>
                                                    @foreach ($groupedAttributes as $group => $attributes)
                                                        <tr>
                                                            <td></td>
                                                            <td colspan="2"><strong>{{ $group }}</strong></td>
                                                        </tr>
                                                        @foreach ($attributes as $attribute)
                                                            <tr>
                                                                <td></td>
                                                                <td>{{ $attribute->key }}</td>
                                                                <td>{{ $attribute->value }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td>No Attributes</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane pt-4" id="tab-galleries">
                                <div class="box">
                                    <div class="box-content">

                                        <table class="table table-bordered table-hover mt-4 mb-0">
                                            <tbody>
                                                @if (count($galleries))
                                                    <tr>
                                                        <th>Options</th>
                                                        <th></th>
                                                        <th>Type</th>
                                                        <th>Group</th>
                                                        <th>Name</th>
                                                        <th>Active</th>
                                                        <th>Sort Order</th>
                                                    </tr>
                                                    @foreach ($galleries as $gallery)
                                                        <tr>
                                                            <td> </td>
                                                            <td>
                                                                @if ($gallery->fileType == 'image')
                                                                    <img src="{{ asset($gallery->url) }}" style="height: 50px;">
                                                                @else
                                                                    <a class="btn btn-default btn-xs"
                                                                        href="{{ asset($gallery->url) }}" target="_blank">Download</a>
                                                                @endif
                                                            </td>
                                                            <td>{{ $gallery->fileType }}</td>
                                                            <td>{{ $gallery->group }}</td>
                                                            <td>{{ $gallery->name }}</td>
                                                            <td>{{ $gallery->active ? 'Yes' : 'No' }}</td>
                                                            <td>{{ $gallery->sort_order }}</td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td>No Galleries</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            @if ($product->type == 'grouped product')
                                <div class="tab-pane pt-4" id="tab-grouped-product">
                                    <div class="box">
                                        <div class="box-content">
                                            <table class="table table-bordered table-hover mt-4 mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>Options</th>
                                                        <th>Image</th>
                                                        <th>Product</th>
                                                        <th>Qty</th>
                                                        <th>Note</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (count($groupedProductDetails))
                                                        @foreach ($groupedProductDetails as $detail)
                                                            <tr>
                                                                <td> </td>
                                                                <td>
                                                                    <img src="{{ asset($detail->product->image) }}" width="50" height="50">
                                                                </td>
                                                                <td>
                                                                    <a href="#">
                                                                        {{ $detail->product->name }}
                                                                    </a>
                                                                </td>
                                                                <td>{{ $detail->qty }}</td>
                                                                <td>{{ $detail->note }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="5">No data</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            @endif

                            @if ($hasKemdikbudZonePrice)
                                <div class="tab-pane pt-4" id="tab-zone-price">
                                    <div class="box">
                                        <div class="box-content">

                                            <table class="table table-bordered table-hover mt-4 mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>Zone</th>
                                                        <th>Price</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (count($mappedKemdikbudZonePrices))
                                                        @foreach ($mappedKemdikbudZonePrices as $zone => $zonePrice)
                                                            <tr>
                                                                <td>Zone {{ $zone }}</td>
                                                                <td>{{ $zonePrice }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="5">No data</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            @endif

                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
