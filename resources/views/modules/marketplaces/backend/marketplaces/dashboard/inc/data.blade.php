<div class="row">
    <div class="col-md-12">
        <h4>Transaksi Pesanan</h4>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Total Pesanan</span>
                <span class="info-box-count">{{ $orderData['order']['all']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['order']['all']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Belum Dikonfirmasi</span>
                <span class="info-box-count">{{ $orderData['confirmation']['pending']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['confirmation']['pending']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Sudah Dikonfirmasi</span>
                <span class="info-box-count">{{ $orderData['confirmation']['confirmed']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['confirmation']['confirmed']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Tidak Dikonfirmasi</span>
                <span class="info-box-count">{{ $orderData['confirmation']['not_confirmed']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['confirmation']['not_confirmed']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Belum Diproses</span>
                <span class="info-box-count">{{ $orderData['packing']['pending']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['packing']['pending']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Sudah Diproses</span>
                <span class="info-box-count">{{ $orderData['packing']['processed']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['packing']['processed']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content bg-info">
                <span class="info-box-label">Diubah Satdik</span>
                <span class="info-box-count">10</span>
                <span class="info-box-total">Rp 125.000.000</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content bg-info">
                <span class="info-box-label">Dibatalkan Satdik</span>
                <span class="info-box-count">10</span>
                <span class="info-box-total">Rp 125.000.000</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content bg-info">
                <span class="info-box-label">DIbatalkan Penyedia</span>
                <span class="info-box-count">10</span>
                <span class="info-box-total">Rp 125.000.000</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Belum Dikirim</span>
                <span class="info-box-count">{{ $orderData['shipping']['pending']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['shipping']['pending']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Sudah Dikirim</span>
                <span class="info-box-count">{{ $orderData['shipping']['shipped']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['shipping']['shipped']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Belum Diterima</span>
                <span class="info-box-count">{{ $orderData['delivery']['undelivered']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['delivery']['undelivered']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Sudah Diterima</span>
                <span class="info-box-count">{{ $orderData['delivery']['delivered']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['delivery']['delivered']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Belum BAST</span>
                <span class="info-box-count">{{ $orderData['delivery_document']['pending']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['delivery_document']['pending']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Sudah BAST</span>
                <span class="info-box-count">{{ $orderData['delivery_document']['completed']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['delivery_document']['completed']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">BAST Belum Dibayar Satdik</span>
                <span class="info-box-count">{{ $orderData['payment_from_customer']['unpaid']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['payment_from_customer']['unpaid']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">BAST Sudah Dibayar Satdik</span>
                <span class="info-box-count">{{ $orderData['payment_from_customer']['paid']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['payment_from_customer']['paid']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Belum dibayarkan ke Penyedia</span>
                <span class="info-box-count">{{ $orderData['payment_to_store']['unpaid']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['payment_to_store']['unpaid']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Sudah Dibayarkan ke Penyedia</span>
                <span class="info-box-count">{{ $orderData['payment_to_store']['paid']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['payment_to_store']['paid']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Dikomplain Satdik</span>
                <span class="info-box-count">{{ $orderData['complain_source']['complain_from_customer']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['complain_source']['complain_from_customer']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Dikomplain Penyedia</span>
                <span class="info-box-count">{{ $orderData['complain_source']['complain_from_store']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['complain_source']['complain_from_store']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Solusi Komplain Belum Disepakati</span>
                <span class="info-box-count">{{ $orderData['complain_request']['not_approved']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['complain_request']['not_approved']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Solusi Komplain Sudah Disepakati</span>
                <span class="info-box-count">{{ $orderData['complain_request']['approved']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['complain_request']['approved']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Solusi Komplain Sudah Selesai</span>
                <span class="info-box-count">{{ $orderData['complain_status']['complain_resolved']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['complain_status']['complain_resolved']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Komplain di ekskalasi</span>
                <span class="info-box-count">{{ $orderData['complain_status']['escalation_solution']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['complain_status']['escalation_solution']['total'], '2', ',', '.') }}</span>
            </div>
        </div>
    </div>

    <hr>

    <div class="col-md-12">
        <h4>Pengguna</h4>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Pengguna Terdaftar</span>
                <span class="info-box-count">{{ $userData['registeredUsers'] }}</span>
                <span class="info-box-total">&nbsp;</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Pengguna yang sedang Aktif</span>
                <span class="info-box-count">{{ $userData['onlineUsers'] }}</span>
                <span class="info-box-total">&nbsp;</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Pengguna dibekukan</span>
                <span class="info-box-count">{{ $userData['suspendedUsers'] }}</span>
                <span class="info-box-total">&nbsp;</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Satdik yang pernah Login</span>
                <span class="info-box-count">{{ $schoolData['schoolCount'] }}</span>
                <span class="info-box-total">&nbsp;</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Satdiik yang pernah transaksi</span>
                <span class="info-box-count">{{ $schoolData['schoolWithOrderCount'] }}</span>
                <span class="info-box-total">&nbsp;</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Penyedia yang Registerasi</span>
                <span class="info-box-count">{{ $storeData['registeredStores'] }}</span>
                <span class="info-box-total">&nbsp;</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Penyedia yang sudah Aktivasi</span>
                <span class="info-box-count">{{ $storeData['activeStores'] }}</span>
                <span class="info-box-total">&nbsp;</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Penyedai yang sudah Transaksi</span>
                <span class="info-box-count">{{ $storeData['storeWithOrders'] }}</span>
                <span class="info-box-total">&nbsp;</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Penyedia yang dibekukan</span>
                <span class="info-box-count">{{ $storeData['suspendedStores'] }}</span>
                <span class="info-box-total">&nbsp;</span>
            </div>
        </div>
    </div>
</div>
