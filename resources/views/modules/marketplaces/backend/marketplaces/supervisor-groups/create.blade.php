@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Add Supervisor Group')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.marketplaces.supervisor-groups.index', [$marketplace->id]) }}">
                                Supervisor Groups
                            </a>
                        </li>
                        <li class="active">
                            Add
                        </li>
                    </ol>

                    <h3>Add Supervisor Group</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['users.marketplaces.supervisor-groups.store', $marketplace->id],
                                'method' => 'POST',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('name') !!}
                                {!! Form::bs3File('image') !!}

                                {!! Form::bs3Submit('Save'); !!}
                                <a class="btn btn-default" href="{{ route('users.marketplaces.supervisor-groups.index', [$marketplace->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
