@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Supervisor Groups')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style>
        .group-logo {
            max-width: 50px;
            max-height: 50px;
        }
    </style>
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Home</a>
                        </li>
                        <li class="active">
                            Supervisor Groups
                        </li>
                    </ol>

                    <h3>Supervisor Groups</h3>

                    @include('flash::message')

                    <a class="btn btn-primary" href="{{ route('users.marketplaces.supervisor-groups.create', [$marketplace->id]) }}">Add Group</a>

                    <div class="box mt-3">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th></th>
                                            <th>Name</th>
                                            <th>Is Active</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($groups))
                                            @php
                                                $no = $groups->firstItem();
                                            @endphp
                                            @foreach ($groups as $group)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-success"
                                                            href="{{ route('users.marketplaces.supervisor-groups.edit', [$marketplace->id, $group->id]) }}" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['users.marketplaces.supervisor-groups.destroy', $marketplace->id, $group->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td style="width: 75px">
                                                        @if ($group->image)
                                                            <div>
                                                                <img class="group-logo" src="{{ asset($group->image) }}" alt="logo">
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td>{{ $group->name }}</td>
                                                    <td>{{ $group->is_active ? 'Yes' : 'No' }}</td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="9">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{-- {{ $groups->links() }} --}}
                        </div>
                    </div>



                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-marketplace')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
