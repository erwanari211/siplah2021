<div class="row">
    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Belum dikonfirmasi</span>
                <span class="info-box-count">{{ $orderData['unconfirmed']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['unconfirmed']['total'], 2, ',', '.') }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Telah dikonfirmasi</span>
                <span class="info-box-count">{{ $orderData['confirmed']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['confirmed']['total'], 2, ',', '.') }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Sedang Diproses</span>
                <span class="info-box-count">{{ $orderData['processing']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['processing']['total'], 2, ',', '.') }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Sedang Dikirim</span>
                <span class="info-box-count">{{ $orderData['shipping']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['shipping']['total'], 2, ',', '.') }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">BAST</span>
                <span class="info-box-count">{{ $orderData['delivery_document']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['delivery_document']['total'], 2, ',', '.') }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Sudah Dibayar</span>
                <span class="info-box-count">{{ $orderData['payment_from_customer']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['payment_from_customer']['total'], 2, ',', '.') }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Selesai</span>
                <span class="info-box-count">{{ $orderData['completed']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['completed']['total'], 2, ',', '.') }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="box dashboard-info-box">
            <div class="box-content">
                <span class="info-box-label">Komplain</span>
                <span class="info-box-count">{{ $orderData['complain']['count'] }}</span>
                <span class="info-box-total">{{ number_format($orderData['complain']['total'], 2, ',', '.') }}</span>
            </div>
        </div>
    </div>
</div>
