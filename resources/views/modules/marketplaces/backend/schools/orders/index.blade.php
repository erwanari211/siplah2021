@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Orders')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.schools.home', [$school->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.schools.orders.index', [$school->id]) }}">Orders</a>
                        </li>
                        <li class="active">
                            List
                        </li>
                    </ol>

                    <h3>Orders</h3>

                    @include('flash::message')

                    <div class="row">
                        <div class="col-sm-4 col-md-3">
                            <h4> Status List </h4>
                            <div class="box">
                                <div class="box-content status-list-box">

                                    @foreach ($groupedOrderStatuses as $group => $groupedOrderStatus)
                                        <div class="mb-3">
                                            <div class="mb-2">
                                                <strong>
                                                    {{ title_case(str_replace('_', ' ', $group ?? '')) }}
                                                </strong>
                                            </div>
                                            <div class="list-group">
                                                @foreach ($groupedOrderStatus as $status => $statusData)
                                                    <a
                                                        class="list-group-item {{ $statusData['info']->id == $orderStatusId ? 'list-group-item-info' : '' }}"
                                                        href="{{ route('users.schools.orders.index', [$school->id, 'order_status_id' => $statusData['info']->id])  }}">

                                                        <span class="mr-2">
                                                            {{ title_case(str_replace('_', ' ', $statusData['info']->key ?? '')) }}
                                                        </span>
                                                        <span class="label label-danger">
                                                            {{ $statusData['count'] }}
                                                        </span>
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8 col-md-9">

                            {{--
                            <div class="box">
                                <div class="box-content">
                                    @foreach ($statuses as $status => $statusValue)
                                        <a class="btn {{ $status == $orderStatus ? 'btn-primary' : 'btn-default'}}"
                                            href="{{ route('users.schools.orders.index', [$school->id, 'status'=>$status]) }}">
                                            {{ ucwords($status) }}
                                            <span class="label label-danger">{{ $statusValue['total'] }}</span>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                            --}}

                            <h4>
                                Status : {{ $selectedOrderStatus->value }}
                            </h4>

                            <div class="box">
                                <div class="box-content">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Options</th>
                                                    <th>Store</th>
                                                    <th>Invoice No</th>
                                                    <th>Status</th>
                                                    <th>Total</th>
                                                    <th>Order Date</th>
                                                    <th>Last Updated</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($orders))
                                                    @php
                                                        $no = $orders->firstItem();
                                                    @endphp
                                                    @foreach ($orders as $order)
                                                        <tr>
                                                            <td>{{ $no }}</td>
                                                            <td>
                                                                <a class="btn btn-xs btn-default" href="{{ route('users.schools.orders.show', [$school->id, $order->id]) }}" title="View">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>

                                                                <a class="btn btn-xs btn-danger"
                                                                    href="{{ route('users.schools.orders.show.download-pdf', [$school->id, $order->id]) }}"
                                                                    title="Download PDF" target="_blank">
                                                                    <i class="fa fa-file-pdf"></i>
                                                                </a>

                                                            </td>
                                                            <td>
                                                                {{ $order->store_name }}
                                                            </td>
                                                            <td>{{ $order->invoice_prefix.$order->invoice_no }}</td>
                                                            <td>{{ $order->order_status }}</td>
                                                            <td>{{ formatNUmber($order->total) }}</td>
                                                            <td>{{ $order->created_at->diffForHumans() }}</td>
                                                            <td>{{ $order->updated_at->diffForHumans() }}</td>
                                                        </tr>
                                                        @php
                                                            $no++;
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="8">No data</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    {{ $orders->appends(request()->only(['status']))->links() }}
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-school')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
