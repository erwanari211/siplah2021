<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Title Page</title>

        <!-- Bootstrap CSS -->
        {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> --}}

        <style type="text/css">
            body {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
                font-size: 14px;
            }

            .container {
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
            }

            @media (min-width: 768px) {
                .container {
                    width: 750px;
                }
            }

            @media (min-width: 992px) {
                .container {
                    width: 970px;
                }
            }

            @media (min-width: 1200px) {
                .container {
                    width: 1170px;
                }
            }

            table {
                background-color: transparent;
            }

            table {
                border-spacing: 0;
                border-collapse: collapse;
            }

            .table {
                width: 100%;
                max-width: 100%;
                margin-bottom: 20px;
            }

            .table>tbody>tr>td,
            .table>tbody>tr>th,
            .table>tfoot>tr>td,
            .table>tfoot>tr>th,
            .table>thead>tr>td,
            .table>thead>tr>th {
                padding: 8px;
                line-height: 1.42857143;
                vertical-align: top;
                border-top: 1px solid #ddd;
            }

            .table-bordered {
                border: 1px solid #ddd;
            }

            .table-bordered>tbody>tr>td,
            .table-bordered>tbody>tr>th,
            .table-bordered>tfoot>tr>td,
            .table-bordered>tfoot>tr>th,
            .table-bordered>thead>tr>td,
            .table-bordered>thead>tr>th {
                border: 1px solid #ddd;
            }

            .text-center {
                text-align: center;
            }

            .text-right {
                text-align: right;
            }

            .text-key {
                font-weight: bold;
                padding-right: 50px;
            }
        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <h2>INVOICE # {{ $order->invoice_prefix.$order->invoice_no }}</h2>

            <table style="width:100%;">
                <tr>
                    <td style="width:100%;">
                        <div class="">
                            @php
                                $shippingMethod = $orderMetas['shipping_method'];
                                $paymentMethod = $orderMetas['payment_method'];
                            @endphp
                            <table class="">
                                <tbody>
                                    <tr>
                                        <td class="text-key">Store</td>
                                        <td>{{ $store->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Invoice No</td>
                                        <td>{{ $order->invoice_prefix.$order->invoice_no }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Date</td>
                                        <td>{{ $order->created_at }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Payment Method</td>
                                        <td>{{ $paymentMethod['label'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Shipping Method</td>
                                        <td>{{ $shippingMethod['name'].' '.$shippingMethod['service'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Order Note</td>
                                        <td>{{ $paymentMethod['note'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>

            <table style="width:100%;">
                <tr>
                    <td style="width:50%;">
                        <div class="">
                            <h3>Payment Address</h3>
                            @php
                                $address = $orderMetas['payment_address'];
                            @endphp
                            <table class="">
                                <tbody>
                                    <tr>
                                        <td class="text-key">Full Name</td>
                                        <td>{{ $address['full_name'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Company</td>
                                        <td>
                                            @if ($address['company'])
                                                {{ $address['company'] }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Phone</td>
                                        <td>{{ $address['phone'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Address</td>
                                        <td>{{ $address['address'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">City</td>
                                        <td>{{ $address['city'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Province</td>
                                        <td>{{ $address['province'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Post Code</td>
                                        <td>{{ $address['postcode'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                    <td style="width:50%;">
                        <div class="col-sm-6">
                            <h3>Shipping Address</h3>
                            @php
                                $address = $orderMetas['shipping_address'];
                            @endphp
                            <table class="">
                                <tbody>
                                    <tr>
                                        <td class="text-key">Full Name</td>
                                        <td>{{ $address['full_name'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Company</td>
                                        <td>
                                            @if ($address['company'])
                                                {{ $address['company'] }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Phone</td>
                                        <td>{{ $address['phone'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Address</td>
                                        <td>{{ $address['address'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">City</td>
                                        <td>{{ $address['city'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Province</td>
                                        <td>{{ $address['province'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-key">Post Code</td>
                                        <td>{{ $address['postcode'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>

            <div class="row">
                <div class="col-sm-12">
                    <h3>Products</h3>
                    <table class="table table-hover table-bordered mb-0">
                        <thead>
                            <tr>
                                <th style="width: 50%">Product</th>
                                <th style="width: 10%" class="text-center">Qty</th>
                                <th style="width: 20%" class="text-right">Price</th>
                                <th style="width: 20%" class="text-right">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <td>{{ $product->product_name }}</td>
                                    <td class="text-center">{{ $product->qty }} </td>
                                    <td class="text-right">{{ formatNumber($product->price) }}</td>
                                    <td class="text-right">{{ formatNumber($product->total) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            @php
                                $courier = $orderMetas['shipping_method']['name'];
                                $service = $orderMetas['shipping_method']['service'];
                                $shippingCost = $orderMetas['shipping_method']['value'];
                                $subtotal = $orderMetas['order']['subtotal'];
                                $subtotal = floatvalue($subtotal);
                                $uniqueNumber = $orderMetas['payment_method']['unique_number'];
                                $total = $subtotal + $shippingCost + $uniqueNumber;

                                $paymentMethodInfo = $orderMetas['payment_method']['info'];
                            @endphp

                            <tr>
                                <td colspan="3">Subtotal</td>
                                <td class="text-right">{{ formatNumber($subtotal) }}</td>
                            </tr>
                            <tr>
                                <td colspan="3">{{ $courier . ' ' . $service }}</td>
                                <td class="text-right">{{ formatNumber($shippingCost) }}</td>
                            </tr>
                            <tr>
                                <td colspan="3">Unique Number</td>
                                <td class="text-right">{{ formatNumber($uniqueNumber) }}</td>
                            </tr>
                            <tr>
                                <td colspan="3"> <strong>Total</strong> </td>
                                <td class="text-right"><strong>{{ formatNumber($total) }}</strong> </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <p style="text-align: right">
                        <small >Generated at {{ date('Y-m-d H:i') }}</small>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
