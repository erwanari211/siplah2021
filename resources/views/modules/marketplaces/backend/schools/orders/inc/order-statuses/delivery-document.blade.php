<h4>Delivery Document</h4>
<div class="box">
    <div class="box-content">
        <div class="mb-4">
            <div class="d-inline-block mr-3">
                <button class="mb-2 btn btn-sm btn-default">Confirmation</button>
                <button class="mb-2 btn btn-sm btn-default">Packing</button>
                <button class="mb-2 btn btn-sm btn-default">Shipping</button>
                <button class="mb-2 btn btn-sm btn-default">Delivery</button>
                <button class="mb-2 btn btn-sm btn-primary">Delivery Document</button>
                <button class="mb-2 btn btn-sm btn-default">Payment from Customer</button>
                <button class="mb-2 btn btn-sm btn-default">Payment to Store</button>
                <button class="mb-2 btn btn-sm btn-default">Completed</button>
            </div>
            <div class="d-inline-block">
                <button class="mb-2 btn btn-sm btn-default">Complain</button>
                <button class="mb-2 btn btn-sm btn-default">Canceled</button>
                <button class="mb-2 btn btn-sm btn-default">Rejected</button>
            </div>
        </div>

        <hr>

        @php
            $deliveryDocumentStatuses = $groupedOrderStatuses['delivery_document'];

            $deliveryDocumentStatusCompleted = $deliveryDocumentStatuses->where('key', 'completed')->first();
        @endphp

        <div data-id="status-form-wrapper" class="hide">
            <div class="status-btn-group mb-3">
                <button class="btn  {{ $order->status_delivery_document == 'pending' ? 'btn-info' : 'btn-default' }}" data-role="display-status-form" data-target="status-pending">
                    Pending
                </button>

                <button class="btn  {{ $order->status_delivery_document == 'completed' ? 'btn-info' : 'btn-default' }}" data-role="display-status-form" data-target="status-completed">
                    Completed
                </button>
            </div>

            <div class="well" data-role="status-form" data-id="status-completed">
                <div class="mb-3">
                    <strong>Completed</strong>
                </div>

                {!! Form::open([
                    'route' => ['users.schools.order.statuses.delivery-document.store', $school->id, $order->id],
                    'method' => 'POST',
                    'files' => true,
                    'style' => 'display: inline-block;'
                ]) !!}
                    {!! Form::hidden('group', 'delivery_document') !!}
                    {!! Form::hidden('status', $deliveryDocumentStatusCompleted->key) !!}

                    {!! Form::bs3Select('notify', $dropdown['yes_no'], 0) !!}
                    {!! Form::bs3Textarea('note') !!}
                    {!! Form::bs3File('file') !!}

                    <button class="btn {{ $order->status_packing == $deliveryDocumentStatusCompleted->key ? 'btn-info' : 'btn-default' }}" type="submit" onclick="return confirm('{{ $deliveryDocumentStatusCompleted->value }}')">
                        Submit
                    </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
