@push('css')
    <style>
        .d-inline-block {
            display: inline-block;
        }
    </style>
@endpush


@push('js')
    <script>
        $(document).ready(function () {
            $('[data-role="status-form"]').hide();
            $('[data-id="status-form-wrapper"]').removeClass('hide');

            $('[data-role="display-status-form"]').on('click', function () {
                var target = $(this).attr('data-target');
                var targetElm = $('[data-id="' + target + '"]');
                var isHidden = $(targetElm).is(":hidden");

                $('[data-role="status-form"]').hide();
                if (isHidden) {
                    $(targetElm).show();
                }
            });
        });
    </script>
@endpush

<div class="row">
    <div class="col-md-12">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <span>{{ $error }}</span>
                    <br>
                @endforeach
            </div>
        @endif

        @if ($order->status_order_process == 'pending')
            @include('modules.marketplaces.backend.schools.orders.inc.order-statuses.order-confirmation')
        @endif

        @if ($order->status_order_process == 'processing')
            @if (in_array($order->status_packing, ['pending', 'processing']))
                @include('modules.marketplaces.backend.schools.orders.inc.order-statuses.packing')
            @endif

            @if (in_array($order->status_packing, ['processed']))
                @if (in_array($order->status_shipping, ['pending', 'shipping']))
                    @include('modules.marketplaces.backend.schools.orders.inc.order-statuses.shipping')
                @endif
            @endif

            @if (in_array($order->status_shipping, ['shipped']))
                @if (in_array($order->status_delivery, ['pending']))
                    @include('modules.marketplaces.backend.schools.orders.inc.order-statuses.delivery')
                @endif
            @endif

            @if (in_array($order->status_delivery, ['delivered']))
                @if (in_array($order->status_delivery_document, ['pending']))
                    @include('modules.marketplaces.backend.schools.orders.inc.order-statuses.delivery-document')
                @endif
            @endif

            @if (in_array($order->status_delivery_document, ['completed']))
                @if (in_array($order->status_payment_from_customer, ['unpaid']))
                    @include('modules.marketplaces.backend.schools.orders.inc.order-statuses.payment-from-customer')
                @endif
            @endif

            @if (in_array($order->status_payment_from_customer, ['paid']))
                @if (in_array($order->status_payment_to_store, ['unpaid']))
                    @include('modules.marketplaces.backend.schools.orders.inc.order-statuses.payment-to-store')
                @endif
            @endif

            @if (in_array($order->status_payment_to_store, ['paid']))
                @include('modules.marketplaces.backend.schools.orders.inc.order-statuses.completed')
            @endif
        @endif

        @if ($order->status_order_process == 'completed')
            @include('modules.marketplaces.backend.schools.orders.inc.order-statuses.completed')
        @endif

        @if ($order->status_order_process == 'rejected')
            @include('modules.marketplaces.backend.schools.orders.inc.order-statuses.rejected')
        @endif

    </div>
</div>
