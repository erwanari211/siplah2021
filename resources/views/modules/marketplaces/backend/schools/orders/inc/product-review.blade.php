@push('css')
    <style>
        .yellow-star {
            color: #edb867;
        }
    </style>
@endpush
@push('js')
    <script>
        $(document).ready(function () {
            $('.select-rating').barrating({
                theme: 'fontawesome-stars',
                showSelectedRating: true,
            });

            $('[data-role="add-review-btn"]').on('click', function () {
                var productId = $(this).attr('data-product-id');
                var productName = $(this).attr('data-product-name');
                var rating = $(this).attr('data-review-rating');

                $('[name=product-rating-product-id]').val(productId);
                $('[name=product-rating-product-name]').val(productName);
                $('[name=product-rating-rating]').barrating('set', rating);

            });
        });
    </script>
@endpush

<div class="modal fade" id="modal-add-product-review" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Product Review</h4>
            </div>
            <div class="modal-body">
                {!! Form::open([
                    'route' => ['users.schools.orders.product-reviews.store', $school->id, $order->id],
                    'method' => 'POST',
                    'files' => true,
                    ]) !!}


                    <div class="hide">
                        {!! Form::bs3Text('product-rating-product-id', null, ['label'=>'Product ID']) !!}
                    </div>
                    {!! Form::bs3Text('product-rating-product-name', null, ['label'=>'Product Name', 'readonly']) !!}
                    <select class="select-rating" name="product-rating-rating">
                        <option value="">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    <div class="form-group {{ $errors->has('product-rating-rating') ?  'has-error' : '' }}">
                        @if ($errors->has('product-rating-rating'))
                            <span class="help-block" role="alert">
                                <strong>{{ $errors->first('product-rating-rating') }}</strong>
                            </span>
                        @endif
                    </div>
                    {!! Form::bs3Textarea('product-rating-content', null, ['label'=>'Comment']) !!}

                    {!! Form::bs3Submit('Submit', ['class'=>'btn btn-primary']); !!}
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
