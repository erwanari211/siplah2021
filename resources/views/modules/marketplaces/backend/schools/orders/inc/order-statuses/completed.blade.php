<h4>Payment from Customer</h4>
<div class="box">
    <div class="box-content">
        <div class="mb-4">
            <div class="d-inline-block mr-3">
                <button class="mb-2 btn btn-sm btn-default">Confirmation</button>
                <button class="mb-2 btn btn-sm btn-default">Packing</button>
                <button class="mb-2 btn btn-sm btn-default">Shipping</button>
                <button class="mb-2 btn btn-sm btn-default">Delivery</button>
                <button class="mb-2 btn btn-sm btn-default">Delivery Document</button>
                <button class="mb-2 btn btn-sm btn-default">Payment from Customer</button>
                <button class="mb-2 btn btn-sm btn-default">Payment to Store</button>
                <button class="mb-2 btn btn-sm btn-primary">Completed</button>
            </div>
        </div>

        <hr>

        <button class="btn  {{ $order->status_order_process == 'completed' ? 'btn-success' : 'btn-default' }}" data-role="display-status-form" data-target="status-completed">
            Completed
        </button>

    </div>
</div>
