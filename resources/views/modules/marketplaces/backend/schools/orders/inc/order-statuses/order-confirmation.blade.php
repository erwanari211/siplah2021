<h4>Order  Confirmation</h4>
<div class="box">
    <div class="box-content">
        <div class="mb-4">
            <div class="d-inline-block mr-3">
                <button class="mb-2 btn btn-sm btn-primary">Confirmation</button>
                <button class="mb-2 btn btn-sm btn-default">Packing</button>
                <button class="mb-2 btn btn-sm btn-default">Shipping</button>
                <button class="mb-2 btn btn-sm btn-default">Delivery</button>
                <button class="mb-2 btn btn-sm btn-default">Delivery Document</button>
                <button class="mb-2 btn btn-sm btn-default">Payment from Customer</button>
                <button class="mb-2 btn btn-sm btn-default">Payment to Store</button>
                <button class="mb-2 btn btn-sm btn-default">Completed</button>
            </div>
            <div class="d-inline-block">
                <button class="mb-2 btn btn-sm btn-default">Complain</button>
                <button class="mb-2 btn btn-sm btn-default">Canceled</button>
                <button class="mb-2 btn btn-sm btn-default">Rejected</button>
            </div>
        </div>

        <hr>

        @php
            $orderConfirmationStatuses = $groupedOrderStatuses['order_confirmation'];

            $orderConfirmationStatusNotConfirmed = $orderConfirmationStatuses->where('key', 'not_confirmed')->first();
            $orderConfirmationStatusConfirmed = $orderConfirmationStatuses->where('key', 'confirmed')->first();
        @endphp

        <div data-id="status-form-wrapper" class="hide">
            <div class="status-btn-group mb-3">
                <button class="btn btn-default" data-role="display-status-form" data-target="status-not-confirmed">
                    Reject
                </button>

                <button class="btn btn-default" data-role="display-status-form" data-target="status-confirmed">
                    Confirmed
                </button>
            </div>
        </div>
    </div>
</div>
