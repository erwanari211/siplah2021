@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Order Detail')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.schools.home', [$school->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.schools.orders.index', [$school->id]) }}">Orders</a>
                        </li>
                        <li class="active">
                            Detail
                        </li>
                    </ol>

                    <h3>Order Detail</h3>

                    @include('flash::message')

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <span>{{ $error }}</span>
                                <br>
                            @endforeach
                        </div>
                    @endif


                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Click
                        <strong>
                            <a href="{{ route('users.schools.orders.show.download-pdf', [$school->id, $order->id]) }}" target="_blank">here</a>
                        </strong>
                        to download as Pdf
                    </div>


                    <h4>Order Detail</h4>
                    <div class="box">
                        <div class="box-content">
                            @php
                                $shippingMethod = $orderMetas['shipping_method'];
                                $paymentMethod = $orderMetas['payment_method'];
                            @endphp
                            <table class="table table-bordered table-hover mb-0">
                                <tbody>
                                    <tr>
                                        <td><strong>Store</strong></td>
                                        <td>
                                            <a href="{{ route('stores.show', [$marketplace->slug, $store->slug]) }}">
                                                {{ $store->name }}
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Invoice No</strong></td>
                                        <td>{{ $order->invoice_prefix.$order->invoice_no }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Date</strong></td>
                                        <td>{{ $order->created_at }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Payment Method</strong></td>
                                        <td>{{ $paymentMethod['label'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Shipping Method</strong></td>
                                        <td>{{ $shippingMethod['name'].' '.$shippingMethod['service'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Order Note</strong></td>
                                        <td>{{ $paymentMethod['note'] }}</td>
                                    </tr>

                                    <tr>
                                        <td><strong>Status</strong></td>
                                        <td>{{ $order->orderStatus->value ?? '' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    @include('modules.marketplaces.backend.schools.orders.inc.product-review')

                    <h4>Products</h4>
                    <div class="box">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered mb-0">
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Product</th>
                                            <th>Qty</th>
                                            <th>Price</th>
                                            <th>Subtotal</th>
                                            @if ($order->status_order_process == 'completed')
                                                <th>Review</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($products as $product)
                                            <tr>
                                                <td>
                                                    <img src="{{ asset($product->product->image) }}" width="50" height="50">
                                                </td>
                                                <td>{{ $product->product_name }}</td>
                                                <td> {{ $product->qty }} </td>
                                                <td>{{ formatNumber($product->price) }}</td>
                                                <td>{{ formatNumber($product->total) }}</td>

                                                @if ($order->status_order_process == 'completed')
                                                    <td>
                                                        @php
                                                            $productRating = $groupedProductReviews[$product->product_id] ?? null;
                                                        @endphp
                                                        <button class="btn btn-sm btn-default"
                                                            title="{{ $productRating ? 'update review' : 'add review' }}"
                                                            data-toggle="modal"
                                                            data-target="#modal-add-product-review"
                                                            data-role="add-review-btn"
                                                            data-order-id="{{ $order->id }}"
                                                            data-product-id="{{ $product->product_id }}"
                                                            data-product-name="{{ $product->product_name }}"
                                                            data-review-rating="{{ $productRating }}" >
                                                            @if ($productRating)
                                                                {{ $productRating }}
                                                                <i class="fa fa-fw fa-star yellow-star"></i>
                                                            @endif
                                                            @if (!$productRating)
                                                                <i class="fa fa-fw fa-star text-muted"></i>
                                                            @endif
                                                        </button>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>

                                    <tfoot>
                                        @php
                                            $courier = $orderMetas['shipping_method']['name'];
                                            $service = $orderMetas['shipping_method']['service'];
                                            $shippingCost = $orderMetas['order']['shipping'];
                                            $subtotal = $orderMetas['order']['subtotal'];
                                            $subtotal = floatvalue($subtotal);
                                            $uniqueNumber = $orderMetas['order']['unique_number'];
                                            $adjustmentPrice = $orderMetas['order']['adjustment_price'];
                                            $total = $subtotal + $shippingCost + $uniqueNumber + $adjustmentPrice;

                                            $paymentMethodInfo = $orderMetas['payment_method']['info'];
                                        @endphp

                                        <tr>
                                            <td colspan="4">Subtotal</td>
                                            <td>{{ formatNumber($subtotal) }}</td>
                                            @if ($order->status_order_process == 'completed')
                                                <td> </td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td colspan="4">{{ $courier . ' ' . $service }}</td>
                                            <td>{{ formatNumber($shippingCost) }}</td>
                                            @if ($order->status_order_process == 'completed')
                                                <td> </td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td colspan="4">Unique Number</td>
                                            <td>{{ formatNumber($uniqueNumber) }}</td>
                                            @if ($order->status_order_process == 'completed')
                                                <td> </td>
                                            @endif
                                        </tr>
                                        @if ($adjustmentPrice != 0)
                                            <tr>
                                                <td colspan="4">Adjustment Price</td>
                                                <td>{{ formatNumber($adjustmentPrice) }}</td>
                                                @if ($order->status_order_process == 'completed')
                                                    <td> </td>
                                                @endif
                                            </tr>
                                        @endif
                                        <tr>
                                            <td colspan="4"> <strong>Total</strong> </td>
                                            <td> <strong>{{ formatNumber($total) }}</strong> </td>
                                            @if ($order->status_order_process == 'completed')
                                                <td> </td>
                                            @endif
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>

                    <h4>Payment Address</h4>
                    <div class="box">
                        <div class="box-content">
                            @php
                                $address = $orderMetas['payment_address'];
                            @endphp
                            <table class="table table-bordered table-hover mb-0">
                                <tbody>
                                    <tr>
                                        <td><strong>Full Name</strong></td>
                                        <td>
                                            {{ $address['full_name'] }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Company</strong></td>
                                        <td>
                                            @if ($address['company'])
                                                {{ $address['company'] }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Phone</strong></td>
                                        <td>{{ $address['phone'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Address</strong></td>
                                        <td>{{ $address['address'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>City</strong></td>
                                        <td>{{ $address['city'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Province</strong></td>
                                        <td>{{ $address['province'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Post Code</strong></td>
                                        <td>{{ $address['postcode'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <h4>Shipping Address</h4>
                    <div class="box">
                        <div class="box-content">
                            @php
                                $address = $orderMetas['shipping_address'];
                            @endphp
                            <table class="table table-bordered table-hover mb-0">
                                <tbody>
                                    <tr>
                                        <td><strong>Full Name</strong></td>
                                        <td>
                                            {{ $address['full_name'] }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Company</strong></td>
                                        <td>
                                            @if ($address['company'])
                                                {{ $address['company'] }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Phone</strong></td>
                                        <td>{{ $address['phone'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Address</strong></td>
                                        <td>{{ $address['address'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>City</strong></td>
                                        <td>{{ $address['city'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Province</strong></td>
                                        <td>{{ $address['province'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Post Code</strong></td>
                                        <td>{{ $address['postcode'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    @include('modules.marketplaces.backend.schools.orders.inc.order-statuses')

                    <h4>Histories</h4>
                    <div class="box">
                        <div class="box-content">
                            <table class="table table-bordered table-hover mb-0">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Note</th>
                                        <th><i class="fa fa-paperclip"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($histories))
                                        @foreach ($histories as $history)
                                            <tr>
                                                <td>{{ $history->created_at }}</td>
                                                <td>{{ $history->order_status }}</td>
                                                <td>{{ $history->notify ? $history->note : '' }}</td>
                                                <td>
                                                    @if ($history->has_attachment)
                                                        <a class="btn btn-default btn-xs"
                                                            href="{{ asset($history->attachments) }}"
                                                            download="{{ get_filename(asset($history->attachments)) }}" target="_blank">
                                                            <i class="fa fa-download"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">No data</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="mb-3">
                        {{--
                        <a class="btn btn-primary" href="{{ route('users.schools.orders.payment-confirmations.create', [$order->id]) }}">
                            Payment Confirmation
                        </a>
                        --}}
                        <a class="btn btn-default" href="{{ route('users.schools.orders.index', $school->id) }}">Back</a>
                    </div>


                    @include('modules.marketplaces.backend.schools.orders.inc.order-negotiations')
                    @include('modules.marketplaces.backend.schools.orders.inc.order-documents')
                    @include('modules.marketplaces.backend.schools.orders.inc.order-payments')
                    @include('modules.marketplaces.backend.schools.orders.inc.order-complains')

                </div>
            </div>

        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-school')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
