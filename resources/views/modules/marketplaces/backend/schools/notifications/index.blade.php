@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Notifications')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.schools.home', [$school->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.schools.notifications.index', [$school->id]) }}">Notifications</a>
                        </li>
                        <li class="active">
                            List
                        </li>
                    </ol>

                    <h3>Notifications</h3>

                    @include('flash::message')

                    <div class="box mt-3">
                        <div class="box-content">
                            <div class="mb-3">
                                <a href="{{ route('users.schools.notifications.index', [$school->id, 'display' => 'all']) }}"
                                    class="btn {{ $display == 'all' ? 'btn-primary' : 'btn-default' }}">
                                    All
                                </a>

                                <a href="{{ route('users.schools.notifications.index', [$school->id, 'display' => 'unread']) }}"
                                    class="btn {{ $display == 'unread' ? 'btn-primary' : 'btn-default' }}">
                                    Unread
                                </a>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Deskripsi</th>
                                            <th>Waktu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($notifications))
                                            @php
                                                $no = $notifications->firstItem();
                                            @endphp
                                            @foreach ($notifications as $notification)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        @if (is_null($notification->read_at))
                                                        {!! Form::open([
                                                            'route' => ['users.schools.notifications.update', $school->id, $notification->id],
                                                            'method' => 'PUT',
                                                            'style' => 'display: inline-block;'
                                                        ]) !!}
                                                            <button class="btn btn-xs btn-default" type="submit">
                                                                <i class="fa fa-fw fa-check text-success" title="Tandai sudah dibaca"></i>
                                                            </button>
                                                        {!! Form::close() !!}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if (is_null($notification->read_at))
                                                            <i class="fa fa-fw fa-circle text-primary" title="Belum dibaca"></i>
                                                        @endif

                                                        @if ($notification->notification_link)
                                                            <a href="{{ $notification->notification_link }}"
                                                                title="{{ $notification->description }}">
                                                                {{ $notification->notification_label }}
                                                            </a>
                                                        @endif

                                                        @if (!$notification->notification_link)
                                                            <span title="{{ $notification->description }}">
                                                                {{ $notification->notification_label }}
                                                            </span>
                                                        @endif
                                                    </td>
                                                    <td>{{ $notification->created_at->diffForHumans() }}</td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="7">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $notifications->links() }}
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-school')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
