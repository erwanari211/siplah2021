@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'School Dashboard')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style>
        .info-box-label {
            display: block;
        }
        .info-box-count {
            display: block;
            font-weight: bold;
            font-size: 32px;
        }
        .info-box-total {
            display: block;
            font-weight: bold;
            color: #777;
        }
    </style>
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.schools.home', [$school->id]) }}">Home</a>
                        </li>
                        <li class="active">
                            Dashboard
                        </li>
                    </ol>

                    <h3>School Dashboard</h3>

                    @include('flash::message')

                    @if (!$school->is_active)
                        @if ($school->status == 'inactive')
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Sekolah belum aktif. Menunggu persetujuan admin.
                            </div>
                        @endif

                        @if ($school->status == 'suspended')
                            @php
                                $schoolStatusNote = $school->getMeta('store_status_note');
                            @endphp
                            <div class="alert alert-danger">
                                Sekolah telah dibekukan.
                                Alasan : {{ $schoolStatusNote }}.
                            </div>
                        @endif
                    @endif

                    @if (!$school->has_addresses)
                        <div class="alert alert-danger">
                            Store dont have address.
                            <a href="{{ route('users.schools.addresses.index', $school->id) }}">
                                Please add address!
                            </a>
                        </div>
                    @endif

                    @if (!$school->has_default_addresses)
                        <div class="alert alert-danger">
                            Store dont have default address.
                            <a href="{{ route('users.schools.addresses.index', $school->id) }}">
                                Please set default address!
                            </a>
                        </div>
                    @endif

                    @include('modules.marketplaces.backend.schools.dashboard.inc.order-data')


                </div>
            </div>

        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-school')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
