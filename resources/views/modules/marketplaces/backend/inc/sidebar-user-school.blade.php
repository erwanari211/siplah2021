@php
    $school->updateSchoolStaffLastOnline();
@endphp

<div class="sidenav sidenav-overlay sidenav-overlay--left" id="user-sidebar">
    <div class="sidenav-overlay-toolbar">
        <button class="btn btn-default btn-xs sidenav-overlay-close"
            data-action="close-sidenav"
            data-target="#user-sidebar">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <div class="sidenav-overlay-container">
        <div class="sidenav-overlay-content slimscroll">
            <nav class="metismenu-vertical-container">
                <ul class="metismenu">
                    <li>
                        <div class="media" style="padding: 10px 15px;">
                            <span class="pull-left" href="#">
                                <img class="media-object img-circle img-thumbnail" src="{{ asset($school->image) }}" alt="Image" width="60" height="60">
                            </span>
                            <div class="media-body">
                                <h4 class="media-heading">
                                    {{ $school->name }}
                                </h4>
                                <p><span class="btn btn-xs btn-default">School</span></p>
                            </div>
                        </div>
                    </li>

                    <li> <span class="divider"><i class="fa fa-fw fa-store"></i> School Menu</span> </li>
                    <li><a href="{{ route('users.schools.home', [$school->id]) }}">Dashboard</a></li>
                    @php
                        $unreadNotification = $school->groupNotifications()->unread()->count();
                    @endphp
                    <li>
                        <a href="{{ route('users.schools.notifications.index', $school->id) }}">
                            Notification
                            @if ($unreadNotification > 0)
                                <span class="label label-danger">{{ $unreadNotification }}</span>
                            @endif
                        </a>
                    </li>
                    <li><a href="{{ route('users.schools.orders.index', $school->id) }}">Orders</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-store"></i> Setting</span> </li>
                    <li><a href="{{ route('users.schools.addresses.index', $school->id) }}"> Addresses </a></li>
                    <li><a href="{{ route('users.schools.staffs.index', $school->id) }}"> Staffs </a></li>


                    <li> <span class="divider"><i class="fa fa-fw fa-history"></i> Logs</span>  </li>
                    <li><a href="{{ route('users.schools.activities.index', $school->id) }}">Activity Logs</a></li>


                    <li> <span class="divider"><i class="fa fa-fw fa-user"></i> Account</span> </li>
                    <li><a href="{{ route('users.accounts.profile.index') }}">View Account</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
