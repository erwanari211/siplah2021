@php
    $supervisorGroup->updateSupervisorStaffLastOnline();
@endphp

<div class="sidenav sidenav-overlay sidenav-overlay--left" id="user-sidebar">
    <div class="sidenav-overlay-toolbar">
        <button class="btn btn-default btn-xs sidenav-overlay-close"
            data-action="close-sidenav"
            data-target="#user-sidebar">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <div class="sidenav-overlay-container">
        <div class="sidenav-overlay-content slimscroll">
            <nav class="metismenu-vertical-container">
                <ul class="metismenu">
                    <li>
                        <div class="media" style="padding: 10px 15px;">
                            <span class="pull-left" href="#">
                                <img class="media-object img-circle img-thumbnail" src="{{ asset($supervisorGroup->image) }}" alt="Image" width="60" height="60">
                            </span>
                            <div class="media-body">
                                <h4 class="media-heading">
                                    {{ $supervisorGroup->name }}
                                </h4>
                                <p><span class="btn btn-xs btn-default">Supervisor</span></p>
                            </div>
                        </div>
                    </li>

                    <li> <span class="divider"><i class="fa fa-fw fa-user-tie"></i> Supervisor Group Menu</span> </li>
                    <li><a href="{{ route('users.supervisor-groups.home', [$supervisorGroup->id]) }}">Dashboard</a></li>
                    <li><a href="{{ route('users.supervisor-groups.orders.index', [$supervisorGroup->id]) }}">Orders</a></li>
                    <li><a href="{{ route('users.supervisor-groups.stores.index', [$supervisorGroup->id]) }}">Stores</a></li>
                    <li><a href="{{ route('users.supervisor-groups.categories.index', [$supervisorGroup->id]) }}">Categories</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-user"></i> Staffs</span>  </li>
                    <li><a href="{{ route('users.supervisor-groups.staffs.index', [$supervisorGroup->id]) }}">Staffs</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-history"></i> Logs</span>  </li>
                    <li><a href="{{ route('users.supervisor-groups.activities.index', [$supervisorGroup->id]) }}">Staff Activity Logs</a></li>
                    <li><a href="{{ route('users.supervisor-groups.user-activities.index', [$supervisorGroup->id]) }}">User Activity Logs</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-user"></i> Account</span> </li>
                    <li><a href="{{ route('users.accounts.profile.index') }}">View Account</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
