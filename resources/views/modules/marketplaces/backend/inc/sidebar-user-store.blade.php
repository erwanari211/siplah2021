@php
    $store->updateStoreStaffLastOnline();
@endphp

<div class="sidenav sidenav-overlay sidenav-overlay--left" id="user-sidebar">
    <div class="sidenav-overlay-toolbar">
        <button class="btn btn-default btn-xs sidenav-overlay-close"
            data-action="close-sidenav"
            data-target="#user-sidebar">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <div class="sidenav-overlay-container">
        <div class="sidenav-overlay-content slimscroll">
            <nav class="metismenu-vertical-container">
                <ul class="metismenu">
                    <li>
                        <div class="media" style="padding: 10px 15px;">
                            <span class="pull-left" href="#">
                                <img class="media-object img-circle img-thumbnail" src="{{ asset($store->image) }}" alt="Image" width="60" height="60">
                            </span>
                            <div class="media-body">
                                <h4 class="media-heading">
                                    {{ $store->name }}
                                </h4>
                                <p><span class="btn btn-xs btn-default">Store</span></p>
                                {{--
                                <p>Marketplace : {{ $store->marketplace->name }}</p>
                                --}}
                            </div>
                        </div>
                    </li>
                    <li> <span class="divider"><i class="fa fa-fw fa-store"></i> Store Menu</span> </li>
                    <li><a href="{{ route('users.stores.index', [$store->id]) }}">Dashboard</a></li>
                    <li><a href="{{ route('users.stores.show', [$store->id]) }}">Profile</a></li>
                    @php
                        $unreadNotification = $store->groupNotifications()->unread()->count();
                    @endphp
                    <li>
                        <a href="{{ route('users.stores.notifications.index', [$store->id]) }}">
                            Notification
                            @if ($unreadNotification > 0)
                                <span class="label label-danger">{{ $unreadNotification }}</span>
                            @endif
                        </a>
                    </li>
                    <li><a href="{{ route('users.stores.collections.index', [$store->id]) }}">Collections</a></li>
                    <li><a href="{{ route('users.stores.products.index', [$store->id]) }}">Products</a></li>
                    <li><a href="{{ route('users.stores.orders.index', [$store->id]) }}">Orders</a></li>
                    <li><a href="{{ route('users.stores.store-sellers.index', [$store->id]) }}"> Store Sellers </a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-store"></i> Promo</span> </li>
                    <li><a href="{{ route('users.stores.banners.index', [$store->id]) }}">Banners</a></li>
                    <li><a href="{{ route('users.stores.banner-groups.index', [$store->id]) }}">Banner Groups</a></li>
                    <li><a href="{{ route('users.stores.featured-products.index', [$store->id]) }}">Products Collections</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-store"></i> CMS</span> </li>
                    <li><a href="{{ route('users.stores.menus.index', [$store->id]) }}">Menu</a></li>
                    <li><a href="{{ route('users.stores.notes.index', [$store->id]) }}">Notes</a></li>
                    <li><a href="{{ route('users.stores.media-libraries.index', [$store->id]) }}">Media Libraries</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-store"></i> Setting</span> </li>
                    <li><a href="{{ route('users.stores.settings.addresses.index', [$store->id]) }}"> Address </a></li>
                    <li><a href="{{ route('users.stores.settings.payment-methods.index', [$store->id]) }}"> Payment Methods </a></li>
                    <li><a href="{{ route('users.stores.settings.shipping-methods.rajaongkir-edit', [$store->id]) }}"> Shipping Methods </a></li>
                    <li><a href="{{ route('users.stores.shipping-methods.index', [$store->id]) }}"> Shipping Methods (Custom)</a></li>
                    <li><a href="{{ route('users.stores.staffs.index', [$store->id]) }}"> Store Staffs </a></li>
                    <li><a href="{{ route('users.stores.settings.store-settings.edit', [$store->id]) }}"> Store Settings </a></li>

                    @if (!$store->active)
                        @if (in_array($store->status, ['inactive', 'rejected']))
                            <li>
                                <a href="{{ route('users.stores.settings.store-registration.edit', [$store->id]) }}">
                                    @if (in_array($store->status, ['rejected']))
                                        <i class="fa fa-fw fa-exclamation-circle text-danger"></i>
                                    @endif
                                    Store Registration
                                </a>
                            </li>
                        @endif
                    @endif

                    <li> <span class="divider"><i class="fa fa-fw fa-history"></i> Logs</span>  </li>
                    <li><a href="{{ route('users.stores.activities.index', [$store->id]) }}">Activity Logs</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-store"></i> Store</span> </li>
                    <li><a class="list-group-item" href="{{ $store->store_url }}"><i class="fa fa-fw  fa-external-link-alt"></i> Visit Store</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-user"></i> Account</span> </li>
                    <li><a href="{{ route('users.accounts.profile.index') }}">View Account</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
