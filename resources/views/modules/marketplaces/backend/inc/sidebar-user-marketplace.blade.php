@php
    $marketplace->updateMarketplaceStaffLastOnline();
@endphp

<div class="sidenav sidenav-overlay sidenav-overlay--left" id="user-sidebar">
    <div class="sidenav-overlay-toolbar">
        <button class="btn btn-default btn-xs sidenav-overlay-close"
            data-action="close-sidenav"
            data-target="#user-sidebar">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <div class="sidenav-overlay-container">
        <div class="sidenav-overlay-content slimscroll">
            <nav class="metismenu-vertical-container">
                <ul class="metismenu">
                    <li>
                        <div class="media" style="padding: 10px 15px;">
                            <span class="pull-left" href="#">
                                <img class="media-object img-circle img-thumbnail" src="{{ asset($marketplace->image) }}" alt="Image" width="60" height="60">
                            </span>
                            <div class="media-body">
                                <h4 class="media-heading">
                                    {{ $marketplace->name }}
                                </h4>
                                <p><span class="btn btn-xs btn-default">Marketplace</span></p>
                            </div>
                        </div>
                    </li>
                    <li> <span class="divider"><i class="fa fa-fw fa-store"></i> Menu</span> </li>
                    <li><a href="{{ route('users.marketplaces.index', [$marketplace->id]) }}">Dashboard</a></li>
                    <li><a href="{{ route('users.marketplaces.edit', [$marketplace->id]) }}">Profile</a></li>
                    @php
                        $unreadNotification = $marketplace->groupNotifications()->unread()->count();
                    @endphp
                    <li>
                        <a href="{{ route('users.marketplaces.notifications.index', [$marketplace->id]) }}">
                            Notification
                            @if ($unreadNotification > 0)
                                <span class="label label-danger">{{ $unreadNotification }}</span>
                            @endif
                        </a>
                    </li>
                    <li><a href="{{ route('users.marketplaces.categories.index', [$marketplace->id]) }}">Categories</a></li>
                    <li><a href="{{ route('users.marketplaces.stores.index', [$marketplace->id]) }}">Stores</a></li>

                    <li><a href="{{ route('users.marketplaces.orders.index', [$marketplace->id]) }}">Orders</a></li>
                    <li><a href="{{ route('users.marketplaces.products.index', [$marketplace->id]) }}">Products</a></li>

                    <li><a href="{{ route('users.marketplaces.users.index', [$marketplace->id]) }}">Users</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-store"></i> Promo</span>  </li>
                    <li><a href="{{ route('users.marketplaces.banners.index', [$marketplace->id]) }}">Banner</a></li>
                    <li><a href="{{ route('users.marketplaces.banner-groups.index', [$marketplace->id]) }}">Banner Group</a></li>
                    <li><a href="{{ route('users.marketplaces.popular-searches.index', [$marketplace->id]) }}">Popular Searches</a></li>

                    <li><a href="{{ route('users.marketplaces.store-collections.index', [$marketplace->id]) }}">Store Collections</a></li>
                    <li><a href="{{ route('users.marketplaces.product-collections.index', [$marketplace->id]) }}">Product Collections</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-store"></i> CMS</span>  </li>
                    <li><a href="{{ route('users.marketplaces.media-libraries.index', [$marketplace->id]) }}">Media Libraries</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-store"></i> Settings</span>  </li>
                    <li><a href="{{ route('users.marketplaces.staffs.index', [$marketplace->id]) }}">Staffs</a></li>
                    <li><a href="{{ route('users.marketplaces.settings.marketplace-settings.edit', [$marketplace->id]) }}">Marketplace Settings</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-user-tie"></i> Supervisors</span>  </li>
                    <li><a href="{{ route('users.marketplaces.supervisor-groups.index', [$marketplace->id]) }}">Supervisor Groups</a></li>
                    <li><a href="{{ route('users.marketplaces.supervisor-staffs.index', [$marketplace->id]) }}">Supervisor Staffs</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-history"></i> Logs</span>  </li>
                    <li><a href="{{ route('users.marketplaces.activities.index', [$marketplace->id]) }}">Activity Logs</a></li>
                    <li><a href="{{ route('users.marketplaces.user-activities.index', [$marketplace->id]) }}">User Activity Logs</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-store"></i> Marketplace</span> </li>
                    <li><a href="{{ $marketplace->marketplace_url }}"><i class="fa fa-fw  fa-external-link-alt"></i> Visit Markeplace</a></li>

                    <li> <span class="divider"><i class="fa fa-fw fa-user"></i> Account</span> </li>
                    <li><a href="{{ route('users.accounts.profile.index') }}">View Account</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
