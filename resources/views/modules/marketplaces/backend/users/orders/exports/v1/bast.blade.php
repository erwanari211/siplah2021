<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>BAST</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <style type="text/css">
            body {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            }

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4,
            .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8,
            .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }
            .col-sm-12 {
                width: 100%;
            }
            .col-sm-11 {
                width: 91.66666667%;
            }
            .col-sm-10 {
                width: 83.33333333%;
            }
            .col-sm-9 {
                width: 75%;
            }
            .col-sm-8 {
                width: 66.66666667%;
            }
            .col-sm-7 {
                width: 58.33333333%;
            }
            .col-sm-6 {
                width: 50%;
            }
            .col-sm-5 {
                width: 41.66666667%;
            }
            .col-sm-4 {
                width: 33.33333333%;
            }
            .col-sm-3 {
                width: 25%;
            }
            .col-sm-2 {
                width: 16.66666667%;
            }
            .col-sm-1 {
                width: 8.33333333%;
            }

            .text-key {
                font-weight: bold;
                padding-right: 50px;
            }
        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">

            <h3 class="text-center">BERITA ACARA SERAH TERIMA BARANG</h3>

            <p>
                Pada hari ini, ..............
                Tanggal ...... Bulan .................... Tahun ...........,
                yang bertanda tangan di bawah ini
            </p>

            <table style="margin-bottom: 20px;">
                @php
                    $address = $storeAddress;
                @endphp
                <tbody>
                    <tr>
                        <td width="20px;">1. </td>
                        <td width="100px;">Nama</td>
                        <td width="20px;">:</td>
                        <td>..................</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td width="100px;">Jabatan</td>
                        <td width="20px;">:</td>
                        <td>..................</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td width="100px;">Perusahaan</td>
                        <td width="20px;">:</td>
                        <td>{{ $store->name }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td width="100px;">Alamat</td>
                        <td width="20px;">:</td>
                        <td>
                            {{ $address['address'] }} - {{ $address['postcode'] }},
                            {{ ucwords(strtolower($address['city']['name'])) }},
                            {{ ucwords(strtolower($address['province']['name'])) }}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3">Selanjutnya disebut sebagai PIHAK PERTAMA,</td>
                    </tr>
                </tbody>
            </table>

            <table style="margin-bottom: 20px;">
                @php
                    $address = $orderMetas['shipping_address'];
                @endphp
                <tbody>
                    <tr>
                        <td width="20px;">2. </td>
                        <td width="100px;">Nama</td>
                        <td width="20px;">:</td>
                        <td>{{ $address['full_name'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td width="100px;">NIP</td>
                        <td width="20px;">:</td>
                        <td>..................</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td width="100px;">Jabatan</td>
                        <td width="20px;">:</td>
                        <td>..................</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td width="100px;">Sekolah</td>
                        <td width="20px;">:</td>
                        <td>{{ $address['full_name'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td width="100px;">Alamat</td>
                        <td width="20px;">:</td>
                        <td>
                            {{ $address['address'] }} - {{ $address['postcode'] }},
                            {{ ucwords(strtolower($address['city'])) }},
                            {{ ucwords(strtolower($address['province'])) }}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3">Selanjutnya disebut sebagai PIHAK KEDUA,</td>
                    </tr>
                </tbody>
            </table>

            <p>
                Selanjutnya berdasarkan PESANAN PEMBELIAN
                Nomor : <strong>{{ $order->invoice_prefix.$order->invoice_no }}</strong>
                pada tanggal {{ date('Y-m-d', strtotime($order->created_at)) }},
                PIHAK PERTAMA menyerahkan barang kepada PIHAK KEDUA,
                dan PIHAK KEDUA telah menerima :
            </p>

            <div class="row" style="margin-top: 20px;">
                <div class="col-sm-12">
                    <table class="table table-hover table-bordered mb-0">
                        <thead>
                            <tr>
                                <th style="vertical-align:middle; width: 5%"
                                    class="text-center"
                                    rowspan="2">
                                    No
                                </th>
                                <th style="vertical-align:middle; width: 12%"
                                    class="text-center"
                                    rowspan="2">
                                    Kode
                                </th>
                                <th style="vertical-align:middle; width: 33%"
                                    class="text-center"
                                    rowspan="2">
                                    Nama Produk
                                </th>
                                <th style="vertical-align:middle; width: 5%"
                                    class="text-center"
                                    rowspan="2">
                                    Qty
                                </th>
                                <th style="vertical-align:middle; width: 10%"
                                    class="text-center"
                                    rowspan="2">
                                    Harga Satuan <br> (Rp)
                                </th>
                                <th style="width: 20%"
                                    class="text-center"
                                    colspan="2">
                                    Diperiksa
                                </th>
                                <th style="vertical-align:middle; width: 15%"
                                    class="text-center"
                                    rowspan="2">
                                    Jumlah <br> (Rp)
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 10%" class="text-center">Baik</th>
                                <th style="width: 10%" class="text-center">Rusak</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($products as $product)
                                <tr>
                                    <td class="text-center">{{ $no }}</td>
                                    <td>
                                        @if (isset($product->product->sku))
                                            {{ $product->product->sku }}
                                        @endif
                                    </td>
                                    <td>{{ $product->product_name }}</td>
                                    <td class="text-center">{{ $product->qty }} </td>
                                    <td class="text-right">{{ formatNumber($product->price) }}</td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right">{{ formatNumber($product->total) }}</td>
                                </tr>
                                @php
                                    $no++;
                                @endphp
                            @endforeach
                            <tr>
                                <td>&nbsp;</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <p>
                Demikian Berita Acara Serah Terima ini dibuat dengan sebenarnya,
                untuk dapat dipergunakan sebagaimana mestinya.
            </p>

            <div style="page-break-inside: avoid;">
                <table style="margin-top: 20px; width: 100%;">
                    <tbody>
                        <tr>
                            <td width="25%">PIHAK KEDUA,</td>
                            <td width="25%"></td>
                            <td width="25%">PIHAK PERTAMA,</td>
                            <td width="25%"></td>
                        </tr>
                        <tr>
                            <td width="25%">...........................</td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                        </tr>
                        <tr>
                            <td width="25%">{{ $order->user_name }}</td>
                            <td width="25%"></td>
                            <td width="25%">{{ $order->store_name }}</td>
                            <td width="25%"></td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="25%">...........................</td>
                            <td width="25%"></td>
                            <td width="25%">...........................</td>
                            <td width="25%"></td>
                        </tr>
                        <tr>
                            <td width="25%">NIP ...........................</td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                        </tr>

                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div style="border-left: 3px solid #ccc; padding-left: 10px;">
                                    Melalui Marketplace <br>
                                    {{ config('app.name') }}
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- jQuery -->
        <script src="//code.jquery.com/jquery.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
         <script src="Hello World"></script>
    </body>
</html>
