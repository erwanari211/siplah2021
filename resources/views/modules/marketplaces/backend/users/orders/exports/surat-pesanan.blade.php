<!DOCTYPE html>
<html lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Surat Pesanan</title>

  <!-- Bootstrap CSS -->
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> --}}

  <style type="text/css">
    body {
      font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
      font-size: 14px;
    }

    .container {
      padding-right: 15px;
      padding-left: 15px;
      margin-right: auto;
      margin-left: auto;
    }

    @media (min-width: 768px) {
      .container {
        width: 750px;
      }
    }

    @media (min-width: 992px) {
      .container {
        width: 970px;
      }
    }

    @media (min-width: 1200px) {
      .container {
        width: 1170px;
      }
    }

    table {
      background-color: transparent;
    }

    table {
      border-spacing: 0;
      border-collapse: collapse;
    }

    .table {
      width: 100%;
      max-width: 100%;
      margin-bottom: 20px;
    }

    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
      padding: 8px;
      line-height: 1.42857143;
      vertical-align: top;
      border-top: 1px solid #ddd;
    }

    .table-bordered {
      border: 1px solid #ddd;
    }

    .table-bordered>tbody>tr>td,
    .table-bordered>tbody>tr>th,
    .table-bordered>tfoot>tr>td,
    .table-bordered>tfoot>tr>th,
    .table-bordered>thead>tr>td,
    .table-bordered>thead>tr>th {
      border: 1px solid #ddd;
    }

    .text-center {
      text-align: center;
    }

    .text-right {
      text-align: right;
    }

    .text-key {
      font-weight: bold;
      padding-right: 50px;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div class="container">
    <h3 class="text-center">SURAT PESANAN</h3>

    <table style="width: 100%; margin-bottom: 20px;">
      <tbody>
        <tr>
          <td style="width: 160px;"><strong>Nama Paket Pekerjaan</strong></td>
          <td style="width: 10px;">:</td>
          <td></td>
        </tr>
        <tr>
          <td style="width: 160px;">Nomor</td>
          <td style="width: 10px;">:</td>
          <td></td>
        </tr>
        <tr>
          <td style="width: 160px;">Tanggal</td>
          <td style="width: 10px;">:</td>
          <td></td>
        </tr>
      </tbody>
    </table>

    <table style="width: 100%; margin-bottom: 20px;">
      <tbody>
        @php
          $address = $orderMetas['shipping_address'];
        @endphp
        <tr>
          <td colspan="4">
            <p>Yang bertanda tangan dibawah ini</p>
          </td>
        </tr>
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 60px;vertical-align: top;">Nama</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;"></td>
        </tr>
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 60px;vertical-align: top;">NIP</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;"></td>
        </tr>
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 60px;vertical-align: top;">Jabatan</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;">
            Kepala {{ $address['full_name'] }}
          </td>
        </tr>
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 60px;vertical-align: top;">Alamat</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;">
            {{ $address['address'] }} - {{ $address['postcode'] }},
            {{ ucwords(strtolower($address['city'])) }},
            {{ ucwords(strtolower($address['province'])) }}
          </td>
        </tr>
        <tr>
          <td style="width: 40px;vertical-align: top;">&nbsp;</td>
          <td style="width: 60px;vertical-align: top;"></td>
          <td style="width: 10px;vertical-align: top;"></td>
          <td style="vertical-align: top;"></td>
        </tr>
        <tr>
          <td colspan="4">
            <p>Selanjutnya disebut sebagai Kuasa Pengguna Anggaran</p>
            <p>
              Berdasarkan Surat Lembar Survey, Klarifikasi Teknis dan Negosiasi Harga ........................
              ........................ tanggal ...... bulan .................. tahun ..........
            </p>
          </td>
        </tr>
      </tbody>

      <tbody>
        @php
          $address = $storeAddress;
        @endphp
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 135px;vertical-align: top;">Nama</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;"></td>
        </tr>
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 135px;vertical-align: top;">Perusahaan / Toko</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;">{{ $store->name }}</td>
        </tr>
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 135px;vertical-align: top;">Alamat</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;">
            {{ $address['address'] }} - {{ $address['postcode'] }},
            {{ ucwords(strtolower($address['city']['name'])) }},
            {{ ucwords(strtolower($address['province']['name'])) }}
          </td>
        </tr>
        <tr>
          <td style="width: 40px;vertical-align: top;">&nbsp;</td>
          <td style="width: 135px;vertical-align: top;"></td>
          <td style="width: 10px;vertical-align: top;"></td>
          <td style="vertical-align: top;"></td>
        </tr>
        <tr>
          <td colspan="4">
            <p>Selanjutnya disebut sebagai Penyedia Barang</p>
          </td>
        </tr>
      </tbody>
    </table>

    <table style="width: 100%; margin-bottom: 20px;">
      <tbody>
        <tr>
          <td colspan="4">
            <p>Untuk mengirimkan barang dengan memperhatikan ketentuan-ketentuan sebagai berikut :</p>
          </td>
        </tr>
        <tr>
          <td class="text-center" style="width: 40px;">1.</td>
          <td style="width: 180px;">Rincian Barang</td>
          <td style="width: 10px;"></td>
          <td></td>
        </tr>
      </tbody>
    </table>

    <table class="table table-hover table-bordered mb-0">
      <thead>
        <tr>
          <th style="vertical-align:middle; width: 8%"
            class="text-center">
            No
          </th>
          <th style="vertical-align:middle; width: 45%"
            class="text-center">
            Nama Produk
          </th>
          <th style="vertical-align:middle; width: 8%"
            class="text-center">
            Qty
          </th>
          <th style="vertical-align:middle; width: 17%"
            class="text-center">
            Harga Satuan <br> (Rp)
          </th>
          <th style="vertical-align:middle; width: 22%"
            class="text-center">
            Jumlah <br> (Rp)
          </th>
        </tr>
      </thead>
      <tbody>
        @php
          $no = 1;
        @endphp
        @foreach ($products as $product)
          <tr>
            <td class="text-center">{{ $no }}</td>
            <td>{{ $product->product_name }}</td>
            <td class="text-center">{{ $product->qty }} </td>
            <td class="text-right">{{ formatNumber($product->price) }}</td>
            <td class="text-right">{{ formatNumber($product->total) }}</td>
          </tr>
          @php
            $no++;
          @endphp
        @endforeach
        <tr>
          <td colspan="4">TOTAL</td>
          <td class="text-right">{{ formatNumber($products->sum('total')) }}</td>
        </tr>
      </tbody>
    </table>

    <table style="width: 100%; margin-bottom: 20px;">
      <tbody>
        <tr>
          <td class="text-center" style="width: 40px;">2.</td>
          <td style="width: 180px;">Tanggal barang diterima</td>
          <td style="width: 10px;">:</td>
          <td></td>
        </tr>
        <tr>
          <td class="text-center" style="width: 40px;">3.</td>
          <td style="width: 180px;">Syarat-syarat pekerjaan</td>
          <td style="width: 10px;">:</td>
          <td>Sesuai dengan persyaratan dan spesifikasi</td>
        </tr>
        <tr>
          <td class="text-center" style="width: 40px;">4.</td>
          <td style="width: 180px;">Waktu penyelesaian</td>
          <td style="width: 10px;">:</td>
          <td></td>
        </tr>
        <tr>
          @php
          $address = $orderMetas['shipping_address'];
          @endphp
          <td class="text-center" style="width: 40px;">5.</td>
          <td style="width: 180px;">Alamat pengiriman barang</td>
          <td style="width: 10px;">:</td>
          <td>
            {{ $address['address'] }} - {{ $address['postcode'] }},
            {{ ucwords(strtolower($address['city'])) }},
            {{ ucwords(strtolower($address['province'])) }}
          </td>
        </tr>
      </tbody>
    </table>

    <div style="page-break-inside: avoid;">
      <table style="width: 100%; margin-bottom: 20px;">
        <tbody>
          <tr>
            <td class="text-center" style="width: 33%">
              Rekanan
            </td>
            <td class="text-center" style="width: 34%"></td>
            <td class="text-center" style="width: 33%">
              PPTK,
            </td>
          </tr>
          <tr>
            <td style="height: 80px;"></td>
            <td style="height: 80px;"></td>
            <td style="height: 80px;"></td>
          </tr>
          <tr>
            <td class="text-center">
              <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                &nbsp;
              </div>
            </td>
            <td class="text-center">
            </td>
            <td class="text-center">
              <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                &nbsp;
              </div>
            </td>
          </tr>
          <tr>
            <td class="text-center">Pimpinan</td>
            <td class="text-center"></td>
            <td class="text-center">NIP. ...............................</td>
          </tr>
        </tbody>
      </table>
    </div>

    <div style="page-break-inside: avoid;">
      <table  style="width: 100%;">
        <tbody>
          @php
            $address = $orderMetas['shipping_address'];
          @endphp
          <tr>
            <td class="text-center" style="width: 25%"> </td>
            <td class="text-center" style="width: 50%">
              Kepala {{ $address['full_name'] }}
            </td>
            <td class="text-center" style="width: 25%"> </td>
          </tr>
          <tr>
            <td class="text-center" style="width: 25%"> </td>
            <td class="text-center" style="width: 50%">
              Selaku Kuasa Pengguna Anggaran
            </td>
            <td class="text-center" style="width: 25%"> </td>
          </tr>
          <tr>
            <td style="height: 80px;"></td>
            <td style="height: 80px;"></td>
            <td style="height: 80px;"></td>
          </tr>
          <tr>
            <td class="text-center"> </td>
            <td class="text-center">
              <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                &nbsp;
              </div>
            </td>
            <td class="text-center"> </td>
          </tr>
          <tr>
            <td class="text-center"></td>
            <td class="text-center">NIP. ...............................</td>
            <td class="text-center"></td>
          </tr>
        </tbody>
      </table>
    </div>

    <table style="margin-top: 20px; width: 100%;">
      <tbody>
        <tr>
          <td width="25%"></td>
          <td width="25%"></td>
          <td width="25%"></td>
          <td width="25%"></td>
        </tr>
        <tr>
          <td colspan="4">
            <div style="border-left: 3px solid #ccc; padding-left: 10px;">
              Melalui Marketplace <br>
              {{ config('app.name') }}
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</body>
</html>
