<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Lampiran BAAPPBJ</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <style type="text/css">
            body {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            }

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4,
            .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8,
            .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }
            .col-sm-12 {
                width: 100%;
            }
            .col-sm-11 {
                width: 91.66666667%;
            }
            .col-sm-10 {
                width: 83.33333333%;
            }
            .col-sm-9 {
                width: 75%;
            }
            .col-sm-8 {
                width: 66.66666667%;
            }
            .col-sm-7 {
                width: 58.33333333%;
            }
            .col-sm-6 {
                width: 50%;
            }
            .col-sm-5 {
                width: 41.66666667%;
            }
            .col-sm-4 {
                width: 33.33333333%;
            }
            .col-sm-3 {
                width: 25%;
            }
            .col-sm-2 {
                width: 16.66666667%;
            }
            .col-sm-1 {
                width: 8.33333333%;
            }

            .text-key {
                font-weight: bold;
                padding-right: 50px;
            }
        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">

            <table style="width: 100%; margin-bottom: 20px; border-bottom: 1px solid black">
                <tbody>
                    <tr>
                        <td style="width: 100px;">Lampiran</td>
                        <td style="width: 10px;">:</td>
                        <td>B.A Pemeriksaan dan Penerimaan Barang/Jasa</td>
                    </tr>
                    <tr>
                        <td style="width: 100px;">Tanggal</td>
                        <td style="width: 10px;">:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="width: 100px;">Nomor</td>
                        <td style="width: 10px;">:</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>

            <div class="text-center" style="margin-bottom: 20px;">
                <strong style="text-decoration: underline;">DAFTAR BARANG YANG DIPERIKSA DAN DITERIMA</strong>
            </div>

            <table class="table table-hover table-bordered mb-0">
                <thead>
                    <tr>
                        <th style="vertical-align:middle; width: 5%;"
                            rowspan="2"
                            class="text-center">
                            No
                        </th>
                        <th style="vertical-align:middle; width: 45%;"
                            rowspan="2"
                            class="text-center">
                            Jenis Barang
                        </th>
                        <th style="vertical-align:middle; width: 25%;"
                            rowspan="2"
                            class="text-center">
                            Spesifikasi
                        </th>
                        <th style="vertical-align:middle; width: 20%;"
                            colspan="2"
                            class="text-center">
                            Keadaan Barang
                        </th>
                        <th style="vertical-align:middle; width: 5%;"
                            rowspan="2"
                            class="text-center">
                            Jumlah Satuan
                        </th>
                    </tr>
                    <tr>
                        <th style="vertical-align:middle; width: 10%;"
                            class="text-center">
                            Baik
                        </th>
                        <th style="vertical-align:middle; width: 10%;"
                            class="text-center">
                            Kurang Baik
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($products as $product)
                        <tr>
                            <td class="text-center">{{ $no }}</td>
                            <td>{{ $product->product_name }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-center">{{ $product->qty }} </td>
                        </tr>
                        @php
                            $no++;
                        @endphp
                    @endforeach
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>

            <div style="page-break-inside: avoid;">
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <td style="width: 35%" class="text-center">&nbsp;</td>
                            <td style="width: 15%"></td>
                            <td style="width: 15%"></td>
                            <td style="width: 35%" class="text-center">
                                .................... , ...... .................. ..........
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 35%" class="text-center">Penyedia Barang</td>
                            <td style="width: 15%"></td>
                            <td style="width: 15%"></td>
                            <td style="width: 35%" class="text-center">
                                Pejabat penerima Hasil Pekerjaan
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 35%" class="text-center">{{ $store->name }}</td>
                            <td style="width: 15%"></td>
                            <td style="width: 15%"></td>
                            <td style="width: 35%;" class="text-center"></td>
                        </tr>
                        <tr>
                            <td style="width: 35%;height: 60px;" class="text-center"></td>
                            <td style="width: 15%"></td>
                            <td style="width: 15%"></td>
                            <td style="width: 35%;height: 60px;" class="text-center"></td>
                        </tr>
                        <tr>
                            <td style="width: 35%" class="text-center">
                                <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                                    &nbsp;
                                </div>
                            </td>
                            <td style="width: 15%"></td>
                            <td style="width: 15%"></td>
                            <td style="width: 35%;" class="text-center">
                                <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                                    &nbsp;
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 35%" class="text-center">Pimpinan</td>
                            <td style="width: 15%"></td>
                            <td style="width: 15%"></td>
                            <td style="width: 35%" class="text-center">NIP. .....................................</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div style="page-break-inside: avoid;">
                <table style="margin-top: 20px; width: 100%;">
                    <tbody>
                        <tr>
                            <td width="25%"></td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div style="border-left: 3px solid #ccc; padding-left: 10px;">
                                    Melalui Marketplace <br>
                                    {{ config('app.name') }}
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- jQuery -->
        <script src="//code.jquery.com/jquery.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
         <script src="Hello World"></script>
    </body>
</html>
