<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Surat Pesanan</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <style type="text/css">
            body {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            }

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4,
            .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8,
            .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }
            .col-sm-12 {
                width: 100%;
            }
            .col-sm-11 {
                width: 91.66666667%;
            }
            .col-sm-10 {
                width: 83.33333333%;
            }
            .col-sm-9 {
                width: 75%;
            }
            .col-sm-8 {
                width: 66.66666667%;
            }
            .col-sm-7 {
                width: 58.33333333%;
            }
            .col-sm-6 {
                width: 50%;
            }
            .col-sm-5 {
                width: 41.66666667%;
            }
            .col-sm-4 {
                width: 33.33333333%;
            }
            .col-sm-3 {
                width: 25%;
            }
            .col-sm-2 {
                width: 16.66666667%;
            }
            .col-sm-1 {
                width: 8.33333333%;
            }

            .text-key {
                font-weight: bold;
                padding-right: 50px;
            }
        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">

            <h3 class="text-center">SURAT PESANAN</h3>

            <table style="width: 100%; margin-bottom: 20px;">
                <tbody>
                    <tr>
                        <td style="width: 160px;"><strong>Nama Paket Pekerjaan</strong></td>
                        <td style="width: 10px;">:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="width: 160px;">Nomor</td>
                        <td style="width: 10px;">:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="width: 160px;">Tanggal</td>
                        <td style="width: 10px;">:</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>

            <table style="width: 100%; margin-bottom: 20px;">
                <tbody>
                    @php
                        $address = $orderMetas['shipping_address'];
                    @endphp
                    <tr>
                        <td colspan="4">
                            <p>Yang bertanda tangan dibawah ini</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 40px;vertical-align: top;"></td>
                        <td style="width: 60px;vertical-align: top;">Nama</td>
                        <td style="width: 10px;vertical-align: top;">:</td>
                        <td style="vertical-align: top;"></td>
                    </tr>
                    <tr>
                        <td style="width: 40px;vertical-align: top;"></td>
                        <td style="width: 60px;vertical-align: top;">NIP</td>
                        <td style="width: 10px;vertical-align: top;">:</td>
                        <td style="vertical-align: top;"></td>
                    </tr>
                    <tr>
                        <td style="width: 40px;vertical-align: top;"></td>
                        <td style="width: 60px;vertical-align: top;">Jabatan</td>
                        <td style="width: 10px;vertical-align: top;">:</td>
                        <td style="vertical-align: top;">
                             Kepala {{ $address['full_name'] }}
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 40px;vertical-align: top;"></td>
                        <td style="width: 60px;vertical-align: top;">Alamat</td>
                        <td style="width: 10px;vertical-align: top;">:</td>
                        <td style="vertical-align: top;">
                            {{ $address['address'] }} - {{ $address['postcode'] }},
                            {{ ucwords(strtolower($address['city'])) }},
                            {{ ucwords(strtolower($address['province'])) }}
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 40px;vertical-align: top;">&nbsp;</td>
                        <td style="width: 60px;vertical-align: top;"></td>
                        <td style="width: 10px;vertical-align: top;"></td>
                        <td style="vertical-align: top;"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <p>Selanjutnya disebut sebagai Kuasa Pengguna Anggaran</p>
                            <p>
                                Berdasarkan Surat Lembar Survey, Klarifikasi Teknis dan Negosiasi Harga ........................
                                ........................ tanggal ...... bulan .................. tahun ..........
                            </p>
                        </td>
                    </tr>
                </tbody>

                <tbody>
                    @php
                        $address = $storeAddress;
                    @endphp
                    <tr>
                        <td style="width: 40px;vertical-align: top;"></td>
                        <td style="width: 135px;vertical-align: top;">Nama</td>
                        <td style="width: 10px;vertical-align: top;">:</td>
                        <td style="vertical-align: top;"></td>
                    </tr>
                    <tr>
                        <td style="width: 40px;vertical-align: top;"></td>
                        <td style="width: 135px;vertical-align: top;">Perusahaan / Toko</td>
                        <td style="width: 10px;vertical-align: top;">:</td>
                        <td style="vertical-align: top;">{{ $store->name }}</td>
                    </tr>
                    <tr>
                        <td style="width: 40px;vertical-align: top;"></td>
                        <td style="width: 135px;vertical-align: top;">Alamat</td>
                        <td style="width: 10px;vertical-align: top;">:</td>
                        <td style="vertical-align: top;">
                            {{ $address['address'] }} - {{ $address['postcode'] }},
                            {{ ucwords(strtolower($address['city']['name'])) }},
                            {{ ucwords(strtolower($address['province']['name'])) }}
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 40px;vertical-align: top;">&nbsp;</td>
                        <td style="width: 135px;vertical-align: top;"></td>
                        <td style="width: 10px;vertical-align: top;"></td>
                        <td style="vertical-align: top;"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <p>Selanjutnya disebut sebagai Penyedia Barang</p>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table style="width: 100%; margin-bottom: 20px;">
                <tbody>
                    <tr>
                        <td colspan="4">
                            <p>Untuk mengirimkan barang dengan memperhatikan ketentuan-ketentuan sebagai berikut :</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center" style="width: 40px;">1.</td>
                        <td style="width: 180px;">Rincian Barang</td>
                        <td style="width: 10px;"></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-hover table-bordered mb-0">
                <thead>
                    <tr>
                        <th style="vertical-align:middle; width: 8%"
                            class="text-center">
                            No
                        </th>
                        <th style="vertical-align:middle; width: 45%"
                            class="text-center">
                            Nama Produk
                        </th>
                        <th style="vertical-align:middle; width: 8%"
                            class="text-center">
                            Qty
                        </th>
                        <th style="vertical-align:middle; width: 17%"
                            class="text-center">
                            Harga Satuan <br> (Rp)
                        </th>
                        <th style="vertical-align:middle; width: 22%"
                            class="text-center">
                            Jumlah <br> (Rp)
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($products as $product)
                        <tr>
                            <td class="text-center">{{ $no }}</td>
                            <td>{{ $product->product_name }}</td>
                            <td class="text-center">{{ $product->qty }} </td>
                            <td class="text-right">{{ formatNumber($product->price) }}</td>
                            <td class="text-right">{{ formatNumber($product->total) }}</td>
                        </tr>
                        @php
                            $no++;
                        @endphp
                    @endforeach
                    <tr>
                        <td colspan="4">TOTAL</td>
                        <td class="text-right">{{ formatNumber($products->sum('total')) }}</td>
                    </tr>
                </tbody>
            </table>

            <table style="width: 100%; margin-bottom: 20px;">
                <tbody>
                    <tr>
                        <td class="text-center" style="width: 40px;">2.</td>
                        <td style="width: 180px;">Tanggal barang diterima</td>
                        <td style="width: 10px;">:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-center" style="width: 40px;">3.</td>
                        <td style="width: 180px;">Syarat-syarat pekerjaan</td>
                        <td style="width: 10px;">:</td>
                        <td>Sesuai dengan persyaratan dan spesifikasi</td>
                    </tr>
                    <tr>
                        <td class="text-center" style="width: 40px;">4.</td>
                        <td style="width: 180px;">Waktu penyelesaian</td>
                        <td style="width: 10px;">:</td>
                        <td></td>
                    </tr>
                    <tr>
                        @php
                            $address = $orderMetas['shipping_address'];
                        @endphp
                        <td class="text-center" style="width: 40px;">5.</td>
                        <td style="width: 180px;">Alamat pengiriman barang</td>
                        <td style="width: 10px;">:</td>
                        <td>
                            {{ $address['address'] }} - {{ $address['postcode'] }},
                            {{ ucwords(strtolower($address['city'])) }},
                            {{ ucwords(strtolower($address['province'])) }}
                        </td>
                    </tr>
                </tbody>
            </table>

            <div style="page-break-inside: avoid;">
                <table style="width: 100%; margin-bottom: 20px;">
                    <tbody>
                        <tr>
                            <td class="text-center" style="width: 33%">
                                Rekanan
                            </td>
                            <td class="text-center" style="width: 34%"></td>
                            <td class="text-center" style="width: 33%">
                                PPTK,
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 80px;"></td>
                            <td style="height: 80px;"></td>
                            <td style="height: 80px;"></td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                                    &nbsp;
                                </div>
                            </td>
                            <td class="text-center">
                            </td>
                            <td class="text-center">
                                <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                                    &nbsp;
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">Pimpinan</td>
                            <td class="text-center"></td>
                            <td class="text-center">NIP. ...............................</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div style="page-break-inside: avoid;">
                <table  style="width: 100%;">
                    <tbody>
                        @php
                            $address = $orderMetas['shipping_address'];
                        @endphp
                        <tr>
                            <td class="text-center" style="width: 25%"> </td>
                            <td class="text-center" style="width: 50%">
                                Kepala {{ $address['full_name'] }}
                            </td>
                            <td class="text-center" style="width: 25%"> </td>
                        </tr>
                        <tr>
                            <td class="text-center" style="width: 25%"> </td>
                            <td class="text-center" style="width: 50%">
                                Selaku Kuasa Pengguna Anggaran
                            </td>
                            <td class="text-center" style="width: 25%"> </td>
                        </tr>
                        <tr>
                            <td style="height: 80px;"></td>
                            <td style="height: 80px;"></td>
                            <td style="height: 80px;"></td>
                        </tr>
                        <tr>
                            <td class="text-center"> </td>
                            <td class="text-center">
                                <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                                    &nbsp;
                                </div>
                            </td>
                            <td class="text-center"> </td>
                        </tr>
                        <tr>
                            <td class="text-center"></td>
                            <td class="text-center">NIP. ...............................</td>
                            <td class="text-center"></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <table style="margin-top: 20px; width: 100%;">
                <tbody>
                    <tr>
                        <td width="25%"></td>
                        <td width="25%"></td>
                        <td width="25%"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div style="border-left: 3px solid #ccc; padding-left: 10px;">
                                Melalui Marketplace <br>
                                {{ config('app.name') }}
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <!-- jQuery -->
        <script src="//code.jquery.com/jquery.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
         <script src="Hello World"></script>
    </body>
</html>
