<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Surat Perintah Kerja</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <style type="text/css">
            body {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            }

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4,
            .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8,
            .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }
            .col-sm-12 {
                width: 100%;
            }
            .col-sm-11 {
                width: 91.66666667%;
            }
            .col-sm-10 {
                width: 83.33333333%;
            }
            .col-sm-9 {
                width: 75%;
            }
            .col-sm-8 {
                width: 66.66666667%;
            }
            .col-sm-7 {
                width: 58.33333333%;
            }
            .col-sm-6 {
                width: 50%;
            }
            .col-sm-5 {
                width: 41.66666667%;
            }
            .col-sm-4 {
                width: 33.33333333%;
            }
            .col-sm-3 {
                width: 25%;
            }
            .col-sm-2 {
                width: 16.66666667%;
            }
            .col-sm-1 {
                width: 8.33333333%;
            }

            .text-key {
                font-weight: bold;
                padding-right: 50px;
            }

            .table-with-border {
                border: 1px solid #ddd;
                table-layout: fixed;
            }

            .table-with-border > tbody > tr > td,
            .table-with-border > tbody > tr > th,
            .table-with-border > tfoot > tr > td,
            .table-with-border > tfoot > tr > th,
            .table-with-border > thead > tr > td,
            .table-with-border > thead > tr > th {
                border: 1px solid #ddd;
                padding: 4px;
            }
        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">

            <table border="1" style="width: 100%; margin-bottom: 10px;">
                <tbody>
                    <tr>
                        <td style="width: 50%; vertical-align: middle;"
                            rowspan="2"
                            class="text-center">
                            <strong>SURAT PERINTAH KERJA (SPK) </strong>
                        </td>
                        <td style="width: 50%; vertical-align: top;"
                            class="text-center">
                            <strong>SATUAN KERJA PERANGKAT DAERAH </strong>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; vertical-align: top;"
                            rowspan="3"
                            class="text-center">
                            <strong>NOMOR DAN TANGGAL SPK</strong> <br>
                            &nbsp; <br>
                            &nbsp; <br>
                            Tanggal : ..............................
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; vertical-align: top;"
                            class="text-center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; vertical-align: top;"
                            rowspan="5"
                            class="text-center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; vertical-align: top;"
                            rowspan="3"
                            class="text-center">
                            <strong>NOMOR DAN TANGGAL SURVEY, KLARIFIKASI DAN NEGOSIASI</strong> <br>
                            &nbsp; <br>
                            &nbsp; <br>
                            Tanggal : ..............................
                        </td>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                        <td style="width: 50%; vertical-align: top; height: 80px;"
                            class="text-center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; vertical-align: top;"
                            colspan="2"
                            class="">
                            <strong>SUMBER DANA :</strong>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; vertical-align: top;"
                            colspan="2"
                            class="">
                            <strong>WAKTU PELAKSANAAN PEKERJAAN :</strong>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div style="border: 1px solid black;" class="text-center">
                <strong>NILAI PEKERJAAN</strong>
            </div>

            <div style="width: 100%; margin-bottom: 10px;">
                <table border="1" style="width: 100%; table-layout:fixed;">
                    <thead>
                        <tr>
                            <th style="vertical-align:middle; width: 5%;"
                                class="text-center">
                                No
                            </th>
                            <th style="vertical-align:middle; width: 45%;"
                                class="text-center">
                                Uraian Pekerjaan
                            </th>
                            <th style="vertical-align:middle; width: 10%;"
                                class="text-center">
                                Kuantitas
                            </th>
                            <th style="vertical-align:middle; width: 10%;"
                                class="text-center">
                                Satuan
                            </th>
                            <th style="vertical-align:middle; width: 15%;"
                                class="text-center">
                                Harga Satuan (Rp)
                            </th>
                            <th style="vertical-align:middle; width: 15%;"
                                class="text-center">
                                Sub Total (Rp)
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @foreach ($products as $product)
                            <tr>
                                <td style="vertical-align: top;" class="text-center">{{ $no }}</td>
                                <td style="vertical-align: top;">{{ $product->product_name }}</td>
                                <td style="vertical-align: top;" class="text-center">{{ $product->qty }} </td>
                                <td style="vertical-align: top;"></td>
                                <td style="vertical-align: top;" class="text-right">{{ formatNumber($product->price) }}</td>
                                <td style="vertical-align: top;" class="text-right">{{ formatNumber($product->total) }}</td>
                            </tr>
                            @php
                                $no++;
                            @endphp
                        @endforeach
                        <tr>
                            <td colspan="2">
                                <strong>Jumlah</strong>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-right">
                                <strong>{{ formatNumber($products->sum('total')) }}</strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div style="border: 1px solid black; margin-bottom: 10px;">
                <p style="margin: 10px 0">
                    <strong>Terbilang :</strong>
                </p>

                <p>
                    <strong>Harga tersebut diatas sudah termasuk pajak</strong>
                </p>

                <p>
                    <span style="text-decoration: underline;">
                        INSTRUKSI KEPADA PENYEDIA :
                    </span>
                </p>

                <p style="padding-right: 20px;">
                    Penagihan hanya dapat dilakukan setelah penyelesaian pekerjaan yang diperintahkan dalam SPK ini
                    dan dibuktikan dengan Berita Acara Serah Terima.
                    Jika pekerjaan tidak dapat diselesaikan dalam jangka waktu pelaksanaan pekerjaan
                    karena kesalahan atau kelalaian penyedia
                    maka penyedia berkewajiban untuk membayar denda kepada KPK sebesar 1/1000 (satu perseribu)
                    dari nilai SPK sebelum PPN setiap hari kalender keterlambatan.
                    Selain tunduk kepada ketentuan dalam SPK ini,
                    penyedia berkewajiban untuk memenuhi Standar Ketentuan dan Syarat Umum SPK terlampir.
                </p>

            </div>

            <div style="page-break-inside: avoid;">
                <table style="width: 100%" border="1">
                    <tbody>
                        <tr>
                            <td style="width: 60%">
                                <div style="padding: 10px 20px;">
                                    Untuk dan atas nama Dinas Pendidikan <br>
                                    Kuasa Pengguna Anggaran Selaku <br>
                                    Pejabat Pembuat Komitmen <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <div style="width: 40%; border-bottom: 1px solid black;">&nbsp;</div>
                                    NIP. ..............................
                                </div>
                            </td>
                            <td style="width: 40%">
                                <div class="text-center" style="padding: 10px 20px;">
                                    Untuk dan atas nama <br>
                                    {{ $store->name }} <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <div style="width: 50%; border-bottom: 1px solid black; margin: 0 auto;">&nbsp;</div>
                                    Pimpinan
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <table style="margin-top: 20px; width: 100%;">
                <tbody>
                    <tr>
                        <td width="25%"></td>
                        <td width="25%"></td>
                        <td width="25%"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div style="border-left: 3px solid #ccc; padding-left: 10px;">
                                Melalui Marketplace <br>
                                {{ config('app.name') }}
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <!-- jQuery -->
        <script src="//code.jquery.com/jquery.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
         <script src="Hello World"></script>
    </body>
</html>
