<!DOCTYPE html>
<html lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Berita Acara Survey</title>

  <!-- Bootstrap CSS -->
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> --}}

  <style type="text/css">
    body {
      font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
      font-size: 14px;
    }

    .container {
      padding-right: 15px;
      padding-left: 15px;
      margin-right: auto;
      margin-left: auto;
    }

    @media (min-width: 768px) {
      .container {
        width: 750px;
      }
    }

    @media (min-width: 992px) {
      .container {
        width: 970px;
      }
    }

    @media (min-width: 1200px) {
      .container {
        width: 1170px;
      }
    }

    table {
      background-color: transparent;
    }

    table {
      border-spacing: 0;
      border-collapse: collapse;
    }

    .table {
      width: 100%;
      max-width: 100%;
      margin-bottom: 20px;
    }

    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
      padding: 8px;
      line-height: 1.42857143;
      vertical-align: top;
      border-top: 1px solid #ddd;
    }

    .table-bordered {
      border: 1px solid #ddd;
    }

    .table-bordered>tbody>tr>td,
    .table-bordered>tbody>tr>th,
    .table-bordered>tfoot>tr>td,
    .table-bordered>tfoot>tr>th,
    .table-bordered>thead>tr>td,
    .table-bordered>thead>tr>th {
      border: 1px solid #ddd;
    }

    .text-center {
      text-align: center;
    }

    .text-right {
      text-align: right;
    }

    .text-key {
      font-weight: bold;
      padding-right: 50px;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div class="container">
    <h3 class="text-center">BERITA ACARA SURVEY HARGA PASAR</h3>

    <p>
      Pada hari ini ................ tanggal .................... ............ bulan ..................
      tahun ................ .................. ........ ,
      Berdasarkan .............................. .............................. ..............................
      .............................. .............................. ..............................
      .............................. .............................. ..............................
      .............................. .............................. ..............................
      .............................. .............................. ..............................
      .............................. ..............................
      telah dilakukan survey harga untuk pekerjaan
      .............................. .............................. ..............................
      dengan spesifikasi terlampir
    </p>

    <table style="width: 100%; margin-bottom: 10px;">
      <tbody>
        <tr>
          <td colspan="4">
            <span>Survey harga dilakukan oleh :</span>
          </td>
        </tr>
        <tr>
          <td style="width: 20px;"></td>
          <td style="width: 150px;">Nama</td>
          <td style="width: 10px;">:</td>
          <td></td>
        </tr>
        <tr>
          <td style="width: 20px;"></td>
          <td style="width: 150px;">NIP</td>
          <td style="width: 10px;">:</td>
          <td></td>
        </tr>
        <tr>
          <td colspan="4">
            <span style="margin: 10px 0; display: inline-block;">
              Survey harga dilakukan di ........................ ,
              dari hasil survey diperoleh referensi harga dari:
            </span>
          </td>
        </tr>
        <tr>
          <td style="width: 20px;"></td>
          <td style="width: 150px;">Nama Pimpinan</td>
          <td style="width: 10px;">:</td>
          <td></td>
        </tr>
        <tr>
          <td style="width: 20px;"></td>
          <td style="width: 150px;">Nama Badan Usaha</td>
          <td style="width: 10px;">:</td>
          <td>{{ $store->name }}</td>
        </tr>
        <tr>
          @php
            $address = $storeAddress;
          @endphp
          <td style="width: 20px;"></td>
          <td style="width: 150px;">Alamat</td>
          <td style="width: 10px;">:</td>
          <td>
            {{ $address['address'] }} - {{ $address['postcode'] }},
            {{ ucwords(strtolower($address['city']['name'])) }},
            {{ ucwords(strtolower($address['province']['name'])) }}
          </td>
        </tr>
        <tr>
          <td colspan="4">
            <span style="margin: 10px 0; display: inline-block;">
              Dengan rincian harga sebagai berikut :
            </span>
          </td>
        </tr>
      </tbody>
    </table>

    <table class="table table-hover table-bordered mb-0">
      <thead>
        <tr>
          <th style="vertical-align:middle; width: 10%"
            class="text-center">
            No
          </th>
          <th style="vertical-align:middle; width: 50%"
            class="text-center">
            Nama Produk
          </th>
          <th style="vertical-align:middle; width: 20%"
            class="text-center">
            Harga Penawaran <br> (Rp)
          </th>
          <th style="vertical-align:middle; width: 20%"
            class="text-center">
            Keterangan
          </th>
        </tr>
      </thead>
      <tbody>
        @php
          $no = 1;
        @endphp
        @foreach ($products as $product)
          <tr>
            <td style="vertical-align: top;" class="text-center">{{ $no }}</td>
            <td style="vertical-align: top;">{{ $product->product_name }}</td>
            <td style="vertical-align: top;" class="text-right">{{ formatNumber($product->price) }}</td>
            <td style="vertical-align: top;" class="text-right"></td>
          </tr>
          @php
            $no++;
          @endphp
        @endforeach
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
      </tbody>
    </table>

    <p>Keterangan : harga diatas sudah termasuk pajak-pajak</p>
    <p>Demikian Berita Acara Survey Harga ini dibuat dengan sesungguhnya</p>

    <div style="page-break-inside: avoid;">
      <table style="width: 100%">
        <tbody>
          <tr>
            <td style="width: 33%" class="text-center">&nbsp;</td>
            <td style="width: 34%"></td>
            <td style="width: 33%" class="text-center">&nbsp;</td>
          </tr>
          <tr>
            <td style="width: 33%" class="text-center">Mengetahui</td>
            <td style="width: 34%"></td>
            <td style="width: 33%;" class="text-center">Pejabat Pengadaan</td>
          </tr>
          <tr>
            <td style="width: 33%;height: 60px;" class="text-center"></td>
            <td style="width: 34%"></td>
            <td style="width: 33%;height: 60px;" class="text-center"></td>
          </tr>
          <tr>
            <td style="width: 33%" class="text-center">
              <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                &nbsp;
              </div>
            </td>
            <td style="width: 34%"></td>
            <td style="width: 33%;" class="text-center">
              <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                &nbsp;
              </div>
            </td>
          </tr>
          <tr>
            <td style="width: 33%" class="text-center">Pimpinan</td>
            <td style="width: 34%"></td>
            <td style="width: 33%" class="text-center">NIP. .....................................</td>
          </tr>
        </tbody>
      </table>
    </div>

    <table style="margin-top: 20px; width: 100%;">
      <tbody>
        <tr>
          <td width="25%"></td>
          <td width="25%"></td>
          <td width="25%"></td>
          <td width="25%"></td>
        </tr>
        <tr>
          <td colspan="4">
            <div style="border-left: 3px solid #ccc; padding-left: 10px;">
              Melalui Marketplace <br>
              {{ config('app.name') }}
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</body>
</html>
