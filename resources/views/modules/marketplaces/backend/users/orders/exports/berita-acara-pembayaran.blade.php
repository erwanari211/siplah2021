<!DOCTYPE html>
<html lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Berita Acara pembayaran</title>

  <!-- Bootstrap CSS -->
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> --}}

  <style type="text/css">
    body {
      font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
      font-size: 14px;
    }

    .container {
      padding-right: 15px;
      padding-left: 15px;
      margin-right: auto;
      margin-left: auto;
    }

    @media (min-width: 768px) {
      .container {
        width: 750px;
      }
    }

    @media (min-width: 992px) {
      .container {
        width: 970px;
      }
    }

    @media (min-width: 1200px) {
      .container {
        width: 1170px;
      }
    }

    table {
      background-color: transparent;
    }

    table {
      border-spacing: 0;
      border-collapse: collapse;
    }

    .table {
      width: 100%;
      max-width: 100%;
      margin-bottom: 20px;
    }

    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
      padding: 8px;
      line-height: 1.42857143;
      vertical-align: top;
      border-top: 1px solid #ddd;
    }

    .table-bordered {
      border: 1px solid #ddd;
    }

    .table-bordered>tbody>tr>td,
    .table-bordered>tbody>tr>th,
    .table-bordered>tfoot>tr>td,
    .table-bordered>tfoot>tr>th,
    .table-bordered>thead>tr>td,
    .table-bordered>thead>tr>th {
      border: 1px solid #ddd;
    }

    .text-center {
      text-align: center;
    }

    .text-right {
      text-align: right;
    }

    .text-key {
      font-weight: bold;
      padding-right: 50px;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div class="container">
    <h3 class="text-center">BERITA ACARA PEMBAYARAN</h3>
    <p class="text-center">Nomor : ............................................................ </p>

    <div>
      <p>
        Pada hari ini ................ tanggal .................... ..........
        bulan .................. tahun .................. ....................
        yang bertanda tangan di bawah ini masing-masing :
      </p>
    </div>

    <table style="width: 100%; margin-bottom: 20px;">
      <tbody>
        @php
          $address = $orderMetas['shipping_address'];
        @endphp
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 20px;vertical-align: top;">1.</td>
          <td style="width: 60px;vertical-align: top;">Nama</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;"></td>
        </tr>
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 20px;vertical-align: top;"></td>
          <td style="width: 60px;vertical-align: top;">NIP</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;"></td>
        </tr>
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 20px;vertical-align: top;"></td>
          <td style="width: 60px;vertical-align: top;">Jabatan</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;">
            Kepala {{ $address['full_name'] }}
          </td>
        </tr>
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 20px;vertical-align: top;"></td>
          <td style="width: 60px;vertical-align: top;">Alamat</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;">
            {{ $address['address'] }} - {{ $address['postcode'] }},
            {{ ucwords(strtolower($address['city'])) }},
            {{ ucwords(strtolower($address['province'])) }}
          </td>
        </tr>
        <tr>
          <td style="width: 40px;"></td>
          <td style="width: 20px;"></td>
          <td colspan="3">
            Yang selanjutnya disebut sebagai PIHAK PERTAMA
          </td>
        </tr>
        <tr>
          <td style="width: 40px;">&nbsp;</td>
          <td style="width: 20px;"></td>
          <td style="width: 60px;"></td>
          <td style="width: 10px;"></td>
          <td></td>
        </tr>
      </tbody>

      <tbody>
        @php
          $address = $storeAddress;
        @endphp
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 20px;vertical-align: top;">2.</td>
          <td style="width: 135px;vertical-align: top;">Nama</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;"></td>
        </tr>
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 20px;vertical-align: top;"></td>
          <td style="width: 135px;vertical-align: top;">Jabatan</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;">Pimpinan {{ $store->name }}</td>
        </tr>
        <tr>
          <td style="width: 40px;vertical-align: top;"></td>
          <td style="width: 20px;vertical-align: top;"></td>
          <td style="width: 135px;vertical-align: top;">Alamat</td>
          <td style="width: 10px;vertical-align: top;">:</td>
          <td style="vertical-align: top;">
            {{ $address['address'] }} - {{ $address['postcode'] }},
            {{ ucwords(strtolower($address['city']['name'])) }},
            {{ ucwords(strtolower($address['province']['name'])) }}
          </td>
        </tr>
        <tr>
          <td style="width: 40px;"></td>
          <td style="width: 20px;"></td>
          <td colspan="3">
            Yang selanjutnya disebut sebagai PIHAK KEDUA
          </td>
        </tr>
      </tbody>
    </table>

    <p style="margin-bottom: 20px;">
      Sehubungan dengan pekerjaan .................... .................... ....................
      .................... .................... ....................
      .................... .................... ....................
      telah sesuai dengan Berita Acara Pemeriksaan dan Penerimaan Barang / Jasa Nomor :
      .................... .................... ....................
    </p>

    <p style="margin-bottom: 20px;">
      Maka Pihak Kedua berhak Menerima pembayaran sebesar {{ formatNumber($order->total) }}
      ( .................... .................... .................... .................... ....................)
    </p>

    <p style="margin-bottom: 20px;">
      Demikian Berita Acara Pembayaran ini dibuat untuk dipergunakan sebagaimana mestinya.
    </p>

    <div style="page-break-inside: avoid;">
      <table style="width: 100%">
        <tbody>
          <tr>
            <td style="width: 35%" class="text-center">&nbsp;</td>
            <td style="width: 15%"></td>
            <td style="width: 15%"></td>
            <td style="width: 35%" class="text-center">&nbsp;</td>
          </tr>
          <tr>
            <td style="width: 35%" class="text-center"> Pihak Pertama </td>
            <td style="width: 15%"></td>
            <td style="width: 15%"></td>
            <td style="width: 35%" class="text-center">Pihak Kedua</td>
          </tr>
          <tr>
            <td style="width: 35%" class="text-center">
              Kepala {{ $order->user_name }}
            </td>
            <td style="width: 15%"></td>
            <td style="width: 15%"></td>
            <td style="width: 35%" class="text-center">Rekanan</td>
          </tr>
          <tr>
            <td style="width: 35%;" class="text-center">
              Selaku Kuasa Pengguna Anggaran
            </td>
            <td style="width: 15%"></td>
            <td style="width: 15%"></td>
            <td style="width: 35%" class="text-center">{{ $store->name }}</td>
          </tr>
          <tr>
            <td style="width: 35%;height: 60px;" class="text-center"></td>
            <td style="width: 15%"></td>
            <td style="width: 15%"></td>
            <td style="width: 35%;height: 60px;" class="text-center"></td>
          </tr>
          <tr>
            <td style="width: 35%" class="text-center">
              <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                &nbsp;
              </div>
            </td>
            <td style="width: 15%"></td>
            <td style="width: 15%"></td>
            <td style="width: 35%;" class="text-center">
              <div style="width: 70%;  margin: 0 auto; border-bottom: 1px solid #ccc;">
                &nbsp;
              </div>
            </td>
          </tr>
          <tr>
            <td style="width: 35%" class="text-center">NIP. .....................................</td>
            <td style="width: 15%"></td>
            <td style="width: 15%"></td>
            <td style="width: 35%" class="text-center">Pimpinan</td>
          </tr>
        </tbody>
      </table>
    </div>

    <div style="page-break-inside: avoid;">
      <table style="margin-top: 20px; width: 100%;">
        <tbody>
          <tr>
            <td width="25%"></td>
            <td width="25%"></td>
            <td width="25%"></td>
            <td width="25%"></td>
          </tr>
          <tr>
            <td colspan="4">
              <div style="border-left: 3px solid #ccc; padding-left: 10px;">
                Melalui Marketplace <br>
                {{ config('app.name') }}
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>
