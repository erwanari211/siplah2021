<!DOCTYPE html>
<html lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>BAST</title>

  <!-- Bootstrap CSS -->
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> --}}

  <style type="text/css">
    body {
      font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
      font-size: 14px;
    }

    .container {
      padding-right: 15px;
      padding-left: 15px;
      margin-right: auto;
      margin-left: auto;
    }

    @media (min-width: 768px) {
      .container {
        width: 750px;
      }
    }

    @media (min-width: 992px) {
      .container {
        width: 970px;
      }
    }

    @media (min-width: 1200px) {
      .container {
        width: 1170px;
      }
    }

    table {
      background-color: transparent;
    }

    table {
      border-spacing: 0;
      border-collapse: collapse;
    }

    .table {
      width: 100%;
      max-width: 100%;
      margin-bottom: 20px;
    }

    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
      padding: 8px;
      line-height: 1.42857143;
      vertical-align: top;
      border-top: 1px solid #ddd;
    }

    .table-bordered {
      border: 1px solid #ddd;
    }

    .table-bordered>tbody>tr>td,
    .table-bordered>tbody>tr>th,
    .table-bordered>tfoot>tr>td,
    .table-bordered>tfoot>tr>th,
    .table-bordered>thead>tr>td,
    .table-bordered>thead>tr>th {
      border: 1px solid #ddd;
    }

    .text-center {
      text-align: center;
    }

    .text-right {
      text-align: right;
    }

    .text-key {
      font-weight: bold;
      padding-right: 50px;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div class="container">
    <h3 class="text-center">BERITA ACARA SERAH TERIMA BARANG</h3>

    <p>
      Pada hari ini, ..............
      Tanggal ...... Bulan .................... Tahun ...........,
      yang bertanda tangan di bawah ini
    </p>

    <table style="margin-bottom: 20px;">
      @php
        $address = $storeAddress;
      @endphp
      <tbody>
        <tr>
          <td width="20px;">1. </td>
          <td width="100px;">Nama</td>
          <td width="20px;">:</td>
          <td>..................</td>
        </tr>
        <tr>
          <td></td>
          <td width="100px;">Jabatan</td>
          <td width="20px;">:</td>
          <td>..................</td>
        </tr>
        <tr>
          <td></td>
          <td width="100px;">Perusahaan</td>
          <td width="20px;">:</td>
          <td>{{ $store->name }}</td>
        </tr>
        <tr>
          <td></td>
          <td width="100px;">Alamat</td>
          <td width="20px;">:</td>
          <td>
            {{ $address['address'] }} - {{ $address['postcode'] }},
            {{ ucwords(strtolower($address['city']['name'])) }},
            {{ ucwords(strtolower($address['province']['name'])) }}
          </td>
        </tr>
        <tr>
          <td></td>
          <td colspan="3">Selanjutnya disebut sebagai PIHAK PERTAMA,</td>
        </tr>
      </tbody>
    </table>

    <table style="margin-bottom: 20px;">
      @php
        $address = $orderMetas['shipping_address'];
      @endphp
      <tbody>
        <tr>
          <td width="20px;">2. </td>
          <td width="100px;">Nama</td>
          <td width="20px;">:</td>
          <td>{{ $address['full_name'] }}</td>
        </tr>
        <tr>
          <td></td>
          <td width="100px;">NIP</td>
          <td width="20px;">:</td>
          <td>..................</td>
        </tr>
        <tr>
          <td></td>
          <td width="100px;">Jabatan</td>
          <td width="20px;">:</td>
          <td>..................</td>
        </tr>
        <tr>
          <td></td>
          <td width="100px;">Sekolah</td>
          <td width="20px;">:</td>
          <td>{{ $address['full_name'] }}</td>
        </tr>
        <tr>
          <td></td>
          <td width="100px;">Alamat</td>
          <td width="20px;">:</td>
          <td>
            {{ $address['address'] }} - {{ $address['postcode'] }},
            {{ ucwords(strtolower($address['city'])) }},
            {{ ucwords(strtolower($address['province'])) }}
          </td>
        </tr>
        <tr>
          <td></td>
          <td colspan="3">Selanjutnya disebut sebagai PIHAK KEDUA,</td>
        </tr>
      </tbody>
    </table>

    <p>
      Selanjutnya berdasarkan PESANAN PEMBELIAN
      Nomor : <strong>{{ $order->invoice_prefix.$order->invoice_no }}</strong>
      pada tanggal {{ date('Y-m-d', strtotime($order->created_at)) }},
      PIHAK PERTAMA menyerahkan barang kepada PIHAK KEDUA,
      dan PIHAK KEDUA telah menerima :
    </p>

    <div class="row" style="margin-top: 20px;">
      <div class="col-sm-12">
        <table class="table table-hover table-bordered mb-0">
          <thead>
            <tr>
              <th style="vertical-align:middle; width: 5%"
                class="text-center"
                rowspan="2">
                No
              </th>
              <th style="vertical-align:middle; width: 12%"
                class="text-center"
                rowspan="2">
                Kode
              </th>
              <th style="vertical-align:middle; width: 33%"
                class="text-center"
                rowspan="2">
                Nama Produk
              </th>
              <th style="vertical-align:middle; width: 5%"
                class="text-center"
                rowspan="2">
                Qty
              </th>
              <th style="vertical-align:middle; width: 10%"
                class="text-center"
                rowspan="2">
                Harga Satuan <br> (Rp)
              </th>
              <th style="width: 20%"
                class="text-center"
                colspan="2">
                Diperiksa
              </th>
              <th style="vertical-align:middle; width: 15%"
                class="text-center"
                rowspan="2">
                Jumlah <br> (Rp)
              </th>
            </tr>
            <tr>
              <th style="width: 10%" class="text-center">Baik</th>
              <th style="width: 10%" class="text-center">Rusak</th>
            </tr>
          </thead>
          <tbody>
            @php
              $no = 1;
            @endphp
            @foreach ($products as $product)
              <tr>
                <td class="text-center">{{ $no }}</td>
                <td>
                  @if (isset($product->product->sku))
                  {{ $product->product->sku }}
                  @endif
                </td>
                <td>{{ $product->product_name }}</td>
                <td class="text-center">{{ $product->qty }} </td>
                <td class="text-right">{{ formatNumber($product->price) }}</td>
                <td></td>
                <td></td>
                <td class="text-right">{{ formatNumber($product->total) }}</td>
              </tr>
              @php
                $no++;
              @endphp
            @endforeach
            <tr>
              <td>&nbsp;</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <p>
      Demikian Berita Acara Serah Terima ini dibuat dengan sebenarnya,
      untuk dapat dipergunakan sebagaimana mestinya.
    </p>

    <div style="page-break-inside: avoid;">
      <table style="margin-top: 20px; width: 100%;">
        <tbody>
          <tr>
            <td width="25%">PIHAK KEDUA,</td>
            <td width="25%"></td>
            <td width="25%">PIHAK PERTAMA,</td>
            <td width="25%"></td>
          </tr>
          <tr>
            <td width="25%">...........................</td>
            <td width="25%"></td>
            <td width="25%"></td>
            <td width="25%"></td>
          </tr>
          <tr>
            <td width="25%">{{ $order->user_name }}</td>
            <td width="25%"></td>
            <td width="25%">{{ $order->store_name }}</td>
            <td width="25%"></td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td width="25%">...........................</td>
            <td width="25%"></td>
            <td width="25%">...........................</td>
            <td width="25%"></td>
          </tr>
          <tr>
            <td width="25%">NIP ...........................</td>
            <td width="25%"></td>
            <td width="25%"></td>
            <td width="25%"></td>
          </tr>

          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4">
              <div style="border-left: 3px solid #ccc; padding-left: 10px;">
                Melalui Marketplace <br>
                {{ config('app.name') }}
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>
