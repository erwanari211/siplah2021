<h4>Payment to Store</h4>
<div class="box">
    <div class="box-content">
        <div class="mb-4">
            <div class="d-inline-block mr-3">
                <button class="mb-2 btn btn-sm btn-default">Confirmation</button>
                <button class="mb-2 btn btn-sm btn-default">Packing</button>
                <button class="mb-2 btn btn-sm btn-default">Shipping</button>
                <button class="mb-2 btn btn-sm btn-default">Delivery</button>
                <button class="mb-2 btn btn-sm btn-default">Delivery Document</button>
                <button class="mb-2 btn btn-sm btn-default">Payment from Customer</button>
                <button class="mb-2 btn btn-sm btn-primary">Payment to Store</button>
                <button class="mb-2 btn btn-sm btn-default">Completed</button>
            </div>
            <div class="d-inline-block">
                <button class="mb-2 btn btn-sm btn-default">Complain</button>
                <button class="mb-2 btn btn-sm btn-default">Canceled</button>
                <button class="mb-2 btn btn-sm btn-default">Rejected</button>
            </div>
        </div>

        <hr>

        @php
            $paymentStatuses = $groupedOrderStatuses['payment_to_store'];

            $paymentStatusPaid = $paymentStatuses->where('key', 'paid')->first();
        @endphp

        <div data-id="status-form-wrapper" class="hide">
            <div class="status-btn-group mb-3">
                <button class="btn  {{ $order->status_payment_to_store == 'unpaid' ? 'btn-info' : 'btn-default' }}" data-role="display-status-form" data-target="status-unpaid">
                    Unpaid
                </button>

                <button class="btn  {{ $order->status_payment_to_store == 'paid' ? 'btn-info' : 'btn-default' }}" data-role="display-status-form" data-target="status-paid">
                    Paid
                </button>
            </div>

            {{--
            <div class="well" data-role="status-form" data-id="status-paid">
                <div class="mb-3">
                    <strong>Paid</strong>
                </div>

                {!! Form::open([
                    'route' => ['users.order.statuses.payment-from-customer.store', $order->id],
                    'method' => 'POST',
                    'files' => true,
                    'style' => 'display: inline-block;'
                ]) !!}
                    {!! Form::hidden('group', 'payment_to_store') !!}
                    {!! Form::hidden('status', $paymentStatusPaid->key) !!}

                    {!! Form::bs3Select('notify', $dropdown['yes_no'], 0) !!}
                    {!! Form::bs3Textarea('note') !!}
                    {!! Form::bs3File('file') !!}

                    <button class="btn {{ $order->status_packing == $paymentStatusPaid->key ? 'btn-info' : 'btn-default' }}" type="submit" onclick="return confirm('{{ $paymentStatusPaid->value }}')">
                        Submit
                    </button>
                {!! Form::close() !!}
            </div>
            --}}

        </div>

    </div>
</div>
