@push('css')
    <style type="text/css">
        .user-photo {
            width: 50px;
        }

        .comment-container {
            overflow-y: auto;
            max-height: 40vh;
        }

        .messages {
          height: auto;
          /*min-height: calc(100% - 93px);*/
          /*max-height: calc(100% - 93px);*/
          max-height: calc(50vh);
          overflow-y: scroll;
          overflow-x: hidden;
        }

        .message-content {
            padding: 0;
        }
        @media screen and (max-width: 735px) {
          .messages {
            /*max-height: calc(100% - 105px);*/
            max-height: calc(40vh);
          }
        }
        .messages::-webkit-scrollbar {
          width: 8px;
          background: transparent;
        }
        .messages::-webkit-scrollbar-thumb {
          background-color: rgba(0, 0, 0, 0.3);
        }
        .messages ul li {
          display: inline-block;
          clear: both;
          float: left;
          margin: 15px 15px 5px 15px;
          width: calc(100% - 25px);
          font-size: 0.9em;
        }
        .messages ul li:nth-last-child(1) {
          margin-bottom: 20px;
        }
        .messages ul li.sent img {
          margin: 6px 8px 0 0;
        }
        .messages ul li.sent .message-content {
          /*background: #435f7a;*/
          /*color: #f5f5f5;*/
          background: #EAEAEA;
        }
        .messages ul li.replies img {
          float: right;
          margin: 6px 0 0 8px;
        }
        .messages ul li.replies .message-content {
          /*background: #f5f5f5;*/
          background: #CCEAFC;
          float: right;
        }
        .messages ul li img {
          width: 22px;
          border-radius: 50%;
          float: left;
        }
        .messages ul li .message-content {
          display: inline-block;
          padding: 10px 15px;
          border-radius: 20px;
          max-width: 205px;
          line-height: 130%;
        }
        @media screen and (min-width: 735px) {
          .messages ul li .message-content {
            max-width: 300px;
          }
        }

        .messages ul li .message-time {
          display: block;
          font-size: smaller;
          margin-top: 1em;
          color: #777;
        }

        .messages ul li.replies .message-time {
          float: right;
        }

        .message-input {
          position: absolute;
          bottom: 0;
          width: 100%;
          z-index: 99;
        }
        .message-input .wrap {
          position: relative;
        }
        .message-input .wrap input {
          font-family: "proxima-nova",  "Source Sans Pro", sans-serif;
          float: left;
          border: none;
          width: calc(100% - 90px);
          padding: 11px 32px 10px 8px;
          font-size: 0.8em;
          color: #32465a;
        }
        @media screen and (max-width: 735px) {
          .message-input .wrap input {
            padding: 15px 32px 16px 8px;
          }
        }
        .message-input .wrap input:focus {
          outline: none;
        }
        .message-input .wrap .attachment {
          position: absolute;
          right: 60px;
          z-index: 4;
          margin-top: 10px;
          font-size: 1.1em;
          color: #435f7a;
          opacity: .5;
          cursor: pointer;
        }
        @media screen and (max-width: 735px) {
          .message-input .wrap .attachment {
            margin-top: 17px;
            right: 65px;
          }
        }
        .message-input .wrap .attachment:hover {
          opacity: 1;
        }
        .message-input .wrap button {
          float: right;
          border: none;
          width: 50px;
          padding: 12px 0;
          cursor: pointer;
          background: #32465a;
          color: #f5f5f5;
        }
        @media screen and (max-width: 735px) {
          .message-input .wrap button {
            padding: 16px 0;
          }
        }
        .message-input .wrap button:hover {
          background: #435f7a;
        }
        .message-input .wrap button:focus {
          outline: none;
        }

    </style>
@endpush

<div class="row">
    <div class="col-md-8">
        <h4>
            <i class="fa fa-handshake"></i>
            Negotation Activities
        </h4>
        <div class="box">
            <div class="box-content">
                <div class="messages">
                    <ul class="message-content">
                        @if (count($activities['negotiation_comments']))
                            @foreach ($activities['negotiation_comments'] as $activity)
                                @php
                                    $photo = $activity->user->photo ? asset($activity->user->photo) : '';
                                    $isOwner = false;
                                    if ($store->user_id == $activity->user_id) {
                                        $isOwner = true;
                                    }
                                @endphp
                                @if (!$isOwner)
                                    <li class="sent">
                                        <img src="{{ $photo }}" alt="" title="{{ $activity->user->name }}">
                                        <div class="message-content">
                                            <p>
                                                {{ $activity->note }}
                                            </p>
                                            <span class="message-time">{{ $activity->created_at }}</span>
                                        </div>
                                    </li>
                                @endif
                                @if ($isOwner)
                                    <li class="replies">
                                        <img src="{{ $photo }}" alt="" title="{{ $activity->user->name }}">
                                        <div class="message-content">
                                            @php
                                                $adjustmentPrice = $activity->getMeta('price');
                                                $priceClass = 'text-success';
                                                if ($adjustmentPrice < 0) {
                                                    $priceClass = 'text-danger';
                                                }

                                                $activityLabel = '';
                                            @endphp

                                            @if ($activity->activity == 'negotiation comment')
                                                <p>
                                                    {{ $activity->note }}
                                                </p>
                                            @endif

                                            @if ($activity->activity == 'adjustment price')
                                                @php
                                                    $activityLabel = 'Adjustment Price :'
                                                @endphp
                                                <p>
                                                    <i class="fa fa-fw fa-info-circle"></i>
                                                    {{ $activityLabel }}
                                                    <strong class="{{ $priceClass }}">
                                                        {{ $adjustmentPrice }}
                                                    </strong>
                                                </p>
                                            @endif

                                            @if ($activity->activity == 'change shipping cost')
                                                @php
                                                    $activityLabel = 'Change shipping cost to :'
                                                @endphp
                                                <p>
                                                    <i class="fa fa-fw fa-info-circle"></i>
                                                    {{ $activityLabel }}
                                                    <strong class="{{ $priceClass }}">
                                                        {{ $adjustmentPrice }}
                                                    </strong>
                                                </p>
                                            @endif

                                            @if ($activity->activity == 'change product price')
                                                @php
                                                    $product_name = $activity->getMeta('product_name');
                                                    $activityLabel = 'Change <strong>'.$product_name.'</strong> price to :'
                                                @endphp
                                                <p>
                                                    <i class="fa fa-fw fa-info-circle"></i>
                                                    {!! $activityLabel !!}
                                                    <strong class="{{ $priceClass }}">
                                                        {{ $adjustmentPrice }}
                                                    </strong>
                                                </p>
                                            @endif

                                            <span class="message-time">{{ $activity->created_at }}</span>
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        @else
                            No data
                        @endif
                    </ul>
                </div>

                <hr>

                {!! Form::open([
                    'route' => ['users.order-activities.negotiation-comments.store', $order->id],
                    'method' => 'POST',
                    'files' => true,
                    ]) !!}

                    <div class="hide">
                        {!! Form::bs3Text('group', 'negotiation') !!}
                        {!! Form::bs3Text('activity', 'negotiation comment') !!}
                    </div>
                    {!! Form::bs3Textarea('note', null, ['label'=>'Comment']) !!}

                    {!! Form::bs3Submit('Submit', ['class'=>'btn btn-primary btn-block']); !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-6">
    </div>
</div>
