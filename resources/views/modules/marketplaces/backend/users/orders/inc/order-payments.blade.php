<div class="row">
    <div class="col-md-8">
        <h4>
            <i class="fa fa-money-bill-alt"></i>
            Payments
        </h4>
        <div class="box">
            <div class="box-content">
                <div class="messages">
                    <ul class="message-content">
                        @if (count($activities['payments']))
                            @foreach ($activities['payments'] as $payment)
                                @php
                                    $photo = $payment->user->photo ? asset($payment->user->photo) : '';
                                    $isOwner = false;
                                    if ($store->user_id == $payment->user_id) {
                                        $isOwner = true;
                                    }
                                    $totalPayment = $payment->getMeta('total_payment');
                                @endphp
                                @if (!$isOwner)
                                    <li class="sent">
                                        <img src="{{ $photo }}" alt="" title="{{ $payment->user->name }}">
                                        <div class="message-content">
                                            <p>
                                                <strong>{{ $payment->user->name }}</strong>
                                                sent
                                                <strong>{{ $payment->activity }}</strong>
                                                : {{ formatNumber($totalPayment) }}
                                            </p>
                                            @if ($payment->has_attachment)
                                                <a class=""
                                                    href="{{ asset($payment->attachments) }}"
                                                    download="{{ get_filename(asset($payment->attachments)) }}" target="_blank">
                                                    <i class="fa fa-download"></i>
                                                    Download
                                                </a>
                                            @endif
                                            @if ($payment->note)
                                              <p>
                                                  {{ $payment->note }}
                                              </p>
                                            @endif
                                            <span class="message-time">{{ $payment->created_at }}</span>
                                        </div>
                                    </li>
                                @endif
                                @if ($isOwner)
                                    <li class="replies">
                                        <img src="{{ $photo }}" alt="" title="{{ $payment->user->name }}">
                                        <div class="message-content">
                                            <p>
                                                <strong>{{ $payment->order->store_name }}</strong>
                                                sent
                                                <strong>{{ $payment->activity }}</strong>
                                                : {{ formatNumber($totalPayment) }}
                                            </p>
                                            @if ($payment->has_attachment)
                                                <a class=""
                                                    href="{{ asset($payment->attachments) }}"
                                                    download="{{ get_filename(asset($payment->attachments)) }}" target="_blank">
                                                    <i class="fa fa-download"></i>
                                                    Download
                                                </a>
                                            @endif
                                            @if ($payment->note)
                                              <p>
                                                  {{ $payment->note }}
                                              </p>
                                            @endif
                                            <span class="message-time">{{ $payment->created_at }}</span>
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        @else
                            No data
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-6">
        <h4>
            <i class="fa fa-money-bill-alt"></i>
            Payments
        </h4>
        <div class="box">
            <div class="box-content">
                {!! Form::open([
                    'route' => ['users.order-activities.payments.store', $order->id],
                    'method' => 'POST',
                    'files' => true,
                    ]) !!}

                    <div class="hide">
                        {!! Form::bs3Text('group', 'payment') !!}
                        {!! Form::bs3Text('activity', 'payment') !!}
                    </div>
                    {!! Form::bs3Text('total_payment') !!}
                    {!! Form::bs3File('file') !!}
                    {!! Form::bs3Textarea('note', null, ['label'=>'Comment']) !!}

                    {!! Form::bs3Submit('Submit', ['class'=>'btn btn-primary btn-block']); !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
