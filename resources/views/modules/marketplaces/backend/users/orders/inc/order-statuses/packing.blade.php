<h4>Packing</h4>
<div class="box">
    <div class="box-content">
        <div class="mb-4">
            <div class="d-inline-block mr-3">
                <button class="mb-2 btn btn-sm btn-default">Confirmation</button>
                <button class="mb-2 btn btn-sm btn-primary">Packing</button>
                <button class="mb-2 btn btn-sm btn-default">Shipping</button>
                <button class="mb-2 btn btn-sm btn-default">Delivery</button>
                <button class="mb-2 btn btn-sm btn-default">Delivery Document</button>
                <button class="mb-2 btn btn-sm btn-default">Payment from Customer</button>
                <button class="mb-2 btn btn-sm btn-default">Payment to Store</button>
                <button class="mb-2 btn btn-sm btn-default">Completed</button>
            </div>
            <div class="d-inline-block">
                <button class="mb-2 btn btn-sm btn-default">Complain</button>
                <button class="mb-2 btn btn-sm btn-default">Canceled</button>
                <button class="mb-2 btn btn-sm btn-default">Rejected</button>
            </div>
        </div>

        <hr>

        @php
            $packingStatuses = $groupedOrderStatuses['packing'];

            $packingStatusProcessing = $packingStatuses->where('key', 'processing')->first();
            $packingStatusProcessed = $packingStatuses->where('key', 'processed')->first();
        @endphp

        <div data-id="status-form-wrapper" class="hide">
            <div class="status-btn-group mb-3">
                <button class="btn  {{ $order->status_packing == 'pending' ? 'btn-info' : 'btn-default' }}" data-role="display-status-form" data-target="status-pending">
                    Pending
                </button>

                <button class="btn  {{ $order->status_packing == 'processing' ? 'btn-info' : 'btn-default' }}" data-role="display-status-form" data-target="status-processing">
                    Processing
                </button>

                <button class="btn  {{ $order->status_packing == 'processed' ? 'btn-info' : 'btn-default' }}" data-role="display-status-form" data-target="status-processed">
                    Processed
                </button>
            </div>

            {{--
            <div class="well" data-role="status-form" data-id="status-processing">
                <div class="mb-3">
                    <strong>Processing</strong>
                </div>

                {!! Form::open([
                    'route' => ['users.stores.order.statuses.store', $store->id, $order->id],
                    'method' => 'POST',
                    'files' => true,
                    'style' => 'display: inline-block;'
                ]) !!}
                    {!! Form::hidden('group', 'packing') !!}
                    {!! Form::hidden('status', $packingStatusProcessing->key) !!}

                    {!! Form::bs3Select('notify', $dropdown['yes_no'], 0) !!}
                    {!! Form::bs3Textarea('note') !!}
                    {!! Form::bs3File('file') !!}

                    <button class="btn {{ $order->status_packing == $packingStatusProcessing->key ? 'btn-info' : 'btn-default' }}" type="submit" onclick="return confirm('{{ $packingStatusProcessing->value }}')">
                        Submit
                    </button>
                {!! Form::close() !!}
            </div>

            <div class="well" data-role="status-form" data-id="status-processed">
                <div class="mb-3">
                    <strong>Processed</strong>
                </div>

                {!! Form::open([
                    'route' => ['users.stores.order.statuses.store', $store->id, $order->id],
                    'method' => 'POST',
                    'files' => true,
                    'style' => 'display: inline-block;'
                ]) !!}
                    {!! Form::hidden('group', 'packing') !!}
                    {!! Form::hidden('status', $packingStatusProcessed->key) !!}

                    {!! Form::bs3Select('notify', $dropdown['yes_no'], 0) !!}
                    {!! Form::bs3Textarea('note') !!}
                    {!! Form::bs3File('file') !!}

                    <button class="btn {{ $order->status_packing == $packingStatusProcessed->key ? 'btn-info' : 'btn-default' }}" type="submit" onclick="return confirm('{{ $packingStatusProcessed->value }}')">
                        Submit
                    </button>
                {!! Form::close() !!}
            </div>
            --}}

        </div>
    </div>
</div>
