@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Store Dashboard')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style>
        .info-box-label {
            display: block;
        }
        .info-box-count {
            display: block;
            font-weight: bold;
            font-size: 32px;
        }
        .info-box-total {
            display: block;
            font-weight: bold;
            color: #777;
        }
    </style>
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li class="active">
                            Dashboard
                        </li>
                    </ol>

                    <h3>Store Dashboard</h3>

                    @include('flash::message')

                    @if (!$store->is_active)
                        @if ($store->status == 'inactive')
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Toko belum aktif. Menunggu persetujuan admin.
                            </div>
                        @endif

                        @if ($store->status == 'rejected')
                            @php
                                $storeStatusNote = $store->getMeta('store_status_note');
                            @endphp
                            <div class="alert alert-danger">
                                Aktivasi toko telah ditolak.
                                Alasan : {{ $storeStatusNote }}.
                            </div>
                        @endif

                        @if ($store->status == 'suspended')
                            @php
                                $storeStatusNote = $store->getMeta('store_status_note');
                            @endphp
                            <div class="alert alert-danger">
                                Toko telah dibekukan.
                                Alasan : {{ $storeStatusNote }}.
                            </div>
                        @endif
                    @endif

                    @if (!$store->has_addresses)
                        <div class="alert alert-danger">
                            Store dont have address.
                            <a href="{{ route('users.stores.settings.addresses.index', $store->id) }}">
                                Please add address!
                            </a>
                        </div>
                    @endif

                    @if (!$store->has_default_addresses)
                        <div class="alert alert-danger">
                            Store dont have default address.
                            <a href="{{ route('users.stores.settings.addresses.index', $store->id) }}">
                                Please set default address!
                            </a>
                        </div>
                    @endif

                    @if (!$store->has_shipping_methods)
                        <div class="alert alert-danger">
                            Store dont have shipping method.
                            <a href="{{ route('users.stores.settings.shipping-methods.rajaongkir-edit', $store->id) }}">
                                Please set shipping method!
                            </a>
                        </div>
                    @endif

                    @if (!$store->has_payment_methods)
                        <div class="alert alert-danger">
                            Store dont have payment method.
                            <a href="{{ route('users.stores.settings.payment-methods.index', $store->id) }}">
                                Please set payment method!
                            </a>
                        </div>
                    @endif


                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="box dashboard-info-box">
                        <div class="box-content">
                            <span class="info-box-label">Belum dikonfirmasi</span>
                            <span class="info-box-count">{{ $orderData['unconfirmed']['count'] }}</span>
                            <span class="info-box-total">{{ number_format($orderData['unconfirmed']['total'], 2, ',', '.') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box dashboard-info-box">
                        <div class="box-content">
                            <span class="info-box-label">Telah dikonfirmasi</span>
                            <span class="info-box-count">{{ $orderData['confirmed']['count'] }}</span>
                            <span class="info-box-total">{{ number_format($orderData['confirmed']['total'], 2, ',', '.') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box dashboard-info-box">
                        <div class="box-content">
                            <span class="info-box-label">Sedang Diproses</span>
                            <span class="info-box-count">{{ $orderData['processing']['count'] }}</span>
                            <span class="info-box-total">{{ number_format($orderData['processing']['total'], 2, ',', '.') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box dashboard-info-box">
                        <div class="box-content">
                            <span class="info-box-label">Sedang Dikirim</span>
                            <span class="info-box-count">{{ $orderData['shipping']['count'] }}</span>
                            <span class="info-box-total">{{ number_format($orderData['shipping']['total'], 2, ',', '.') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box dashboard-info-box">
                        <div class="box-content">
                            <span class="info-box-label">BAST</span>
                            <span class="info-box-count">{{ $orderData['delivery_document']['count'] }}</span>
                            <span class="info-box-total">{{ number_format($orderData['delivery_document']['total'], 2, ',', '.') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box dashboard-info-box">
                        <div class="box-content">
                            <span class="info-box-label">Sudah Dibayar</span>
                            <span class="info-box-count">{{ $orderData['payment_from_customer']['count'] }}</span>
                            <span class="info-box-total">{{ number_format($orderData['payment_from_customer']['total'], 2, ',', '.') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box dashboard-info-box">
                        <div class="box-content">
                            <span class="info-box-label">Selesai</span>
                            <span class="info-box-count">{{ $orderData['completed']['count'] }}</span>
                            <span class="info-box-total">{{ number_format($orderData['completed']['total'], 2, ',', '.') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box dashboard-info-box">
                        <div class="box-content">
                            <span class="info-box-label">Komplain</span>
                            <span class="info-box-count">{{ $orderData['complain']['count'] }}</span>
                            <span class="info-box-total">{{ number_format($orderData['complain']['total'], 2, ',', '.') }}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="box dashboard-info-box">
                        <div class="box-content">
                            <span class="info-box-label">Belum direview</span>
                            <span class="info-box-count">{{ $productData->pending }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box dashboard-info-box">
                        <div class="box-content">
                            <span class="info-box-label">Aktif</span>
                            <span class="info-box-count">{{ $productData->approved }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box dashboard-info-box">
                        <div class="box-content">
                            <span class="info-box-label">Ditolak</span>
                            <span class="info-box-count">{{ $productData->rejected }}</span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <h4>Most Viewed Product</h4>
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs border-bottom-nav">
                            <li class="active">
                                <a href="#tab-most-products-viewed-weekly" data-toggle="tab">Weekly</a>
                            </li>
                            <li>
                                <a href="#tab-most-products-viewed-monthly" data-toggle="tab">Monthly</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in pt-4 active" id="tab-most-products-viewed-weekly">
                                <div class="box">
                                    <div class="box-content">
                                        <table class="table table-bordered table-hover mb-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th>Views</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($mostViewedProductsThisWeek))
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach ($mostViewedProductsThisWeek as $item)
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>
                                                                <img src="{{ asset($item->product->image) }}" width="50">
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $item->product->slug]) }}">
                                                                    {{ $item->product->name }}
                                                                </a>
                                                            </td>
                                                            <td>{{ $item->total_views }}</td>
                                                        </tr>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No Data</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade pt-4" id="tab-most-products-viewed-monthly">
                                <div class="box">
                                    <div class="box-content">
                                        <table class="table table-bordered table-hover mb-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th>Views</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($mostViewedProductsThisMonth))
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach ($mostViewedProductsThisMonth as $item)
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>
                                                                <img src="{{ asset($item->product->image) }}" width="50">
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('products.show', [$marketplace->slug, $store->slug, $item->product->slug]) }}">
                                                                    {{ $item->product->name }}
                                                                </a>
                                                            </td>
                                                            <td>{{ $item->total_views }}</td>
                                                        </tr>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No Data</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h4>Popular Search</h4>
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs border-bottom-nav">
                            <li class="active">
                                <a href="#tab-popular-search-weekly" data-toggle="tab">Weekly</a>
                            </li>
                            <li>
                                <a href="#tab-popular-search-monthly" data-toggle="tab">Monthly</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in pt-4 active" id="tab-popular-search-weekly">
                                <div class="box">
                                    <div class="box-content">
                                        <table class="table table-bordered table-hover mb-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Search</th>
                                                    <th>Views</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($popularSearchThisWeek))
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach ($popularSearchThisWeek as $item)
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $item->search }}</td>
                                                            <td>{{ $item->total_search }}</td>
                                                        </tr>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No Data</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade pt-4" id="tab-popular-search-monthly">
                                <div class="box">
                                    <div class="box-content">
                                        <table class="table table-bordered table-hover mb-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Search</th>
                                                    <th>Views</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($popularSearchThisMonth))
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach ($popularSearchThisMonth as $item)
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $item->search }}</td>
                                                            <td>{{ $item->total_search }}</td>
                                                        </tr>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No Data</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-4 col-sm-6"></div>
                <div class="col-md-4 col-sm-6"></div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
