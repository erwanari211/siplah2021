@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Edit Store Geolocation')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/leaflet/leaflet.css') }}">
    <style>
        #main-map { height: 200px; margin-bottom: 20px; }
    </style>
@endpush

@push('js')
    <script src="{{ asset('assets/plugins/leaflet/leaflet.js') }}"></script>
    <script>
        $(document).ready(function () {
            var defaultLatLng = {
                lat: -6.2088,
                lng: 106.8456,
            }

            var oldLat = $('#latitude').val();
            var oldLng = $('#longitude').val();
            if (oldLat && oldLng) {
                defaultLatLng.lat = oldLat;
                defaultLatLng.lng = oldLng;
            }

            var mainMap = L.map('main-map').setView(defaultLatLng, 13);

            var openstreetmap = {}
            openstreetmap = {
                url : 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                options : {
                    maxZoom: 19,
                    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                }
            }

            L.tileLayer(openstreetmap.url, openstreetmap.options).addTo(mainMap);

            var marker = L.marker(defaultLatLng).addTo(mainMap);
            setMarkerPopup(marker)

            function setMapView(map, lat, lng, zoom = 13) {
                map.setView([lat, lng], zoom);
            }

            function setMarkerPopup(marker) {
                var latlng = marker.getLatLng();
                var lat = latlng.lat.toFixed(2)
                var lng = latlng.lng.toFixed(2)
                marker.bindPopup(`${lat}, ${lng}`);
            }

            function setMarkerPosition(marker, lat, lng) {
                marker.setLatLng([lat, lng]);
            }

            function setGeolocationInput(lat, lng) {
                var lat = lat.toFixed(8);
                var lng = lng.toFixed(8);
                $('#latitude').val(lat);
                $('#longitude').val(lng);
            }

            function onMapClick(e) {
                var latlng = e.latlng;
                setMarkerPosition(marker, latlng.lat, latlng.lng);
                setMarkerPopup(marker);
                setGeolocationInput(latlng.lat, latlng.lng);
            }

            mainMap.on('click', onMapClick);

            $('#get-location-btn').on('click', function () {
                getLocation()
            });

            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else {
                    alert("Geolocation is not supported by this browser.")
                }
            }

            function showPosition(position) {
                var latlng = []
                latlng.lat = position.coords.latitude;
                latlng.lng = position.coords.longitude;

                setMapView(mainMap, latlng.lat, latlng.lng);
                setMarkerPosition(marker, latlng.lat, latlng.lng);
                setMarkerPopup(marker);
                setGeolocationInput(latlng.lat, latlng.lng);
            }


        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li class="active">
                            Store Profile
                        </li>
                    </ol>

                    <h3>Edit Store Geolocation</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($store, [
                                'route' => ['users.stores.geolocation.update', $store->id],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                <div class="map-wrapper">
                                    <div id="main-map"></div>
                                    <div class="mb-3">
                                        <span class="btn btn-sm btn-default" id="get-location-btn">Get your location</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        {!! Form::bs3Number('latitude', null, ['label' => 'Latitude *', 'step' => 'any']) !!}
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Form::bs3Number('longitude', null, ['label' => 'Longitude *', 'step' => 'any']) !!}
                                    </div>
                                </div>


                                {!! Form::bs3Submit('Update'); !!}
                                <a class="btn btn-default" href="{{ route('users.stores.show', [$store->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>



                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
