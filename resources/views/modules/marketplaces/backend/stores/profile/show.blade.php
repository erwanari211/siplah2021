@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Detail Store')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li class="active">
                            Store Profile
                        </li>
                    </ol>

                    <h3>Detail Store</h3>

                    @include('flash::message')

                    <div class="mb-3">
                        <a class="btn btn-success btn-sm" href="{{ route('users.stores.edit', [$store->id]) }}">
                            Edit
                        </a>

                        <a class="btn btn-default btn-sm" href="{{ route('users.stores.geolocation.edit', [$store->id]) }}">
                            Edit Geolocation
                        </a>
                    </div>

                    <div class="box">
                        <div class="box-content">
                            <div class="media mb-3">
                                @if ($store->image)
                                    <span class="pull-left" href="#">
                                        <img class="media-object img-circle img-thumbnail" src="{{ asset($store->image) }}" alt="Image" width="60" height="60">
                                    </span>
                                @endif
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        <a href="{{ route('stores.show', [$store->marketplace_id, $store->id]) }}">
                                            {{ $store->name }}
                                        </a>
                                    </h4>
                                </div>
                            </div>

                            <hr>

                            <table class="table">
                                <tr>
                                    <th colspan="3">
                                        <span class="text-primary">Profile</span>
                                    </th>
                                </tr>
                                <tr>
                                    <td style="width: 180px">Nama Toko</td>
                                    <td style="width: 15px"></td>
                                    <td>{{ $store->name }}</td>
                                </tr>
                                <tr>
                                    <td>Jenis Usaha</td>
                                    <td></td>
                                    <td>{{ $storeMeta['jenis_usaha'] }}</td>
                                </tr>
                                <tr>
                                    <td>Kelas Usaha</td>
                                    <td></td>
                                    <td>{{ $storeMeta['kelas_usaha'] }}</td>
                                </tr>

                                <tr>
                                    @php
                                        $isUmkm = in_array($storeMeta['kelas_usaha'], ['Mikro', 'Kecil', 'Menengah']);
                                    @endphp
                                    <td>Kategori Usaha</td>
                                    <td></td>
                                    <td>{{ $isUmkm ? 'UMKM' : $storeMeta['kelas_usaha'] }}</td>
                                </tr>

                                <tr>
                                    <td>Status Usaha</td>
                                    <td></td>
                                    <td>{{ $storeMeta['status_usaha'] }}</td>
                                </tr>

                                <tr>
                                    <td>NPWP</td>
                                    <td></td>
                                    <td>{{ $storeMeta['npwp'] }}</td>
                                </tr>
                                <tr>
                                    <td>SIUP / NIB</td>
                                    <td></td>
                                    <td>{{ $storeMeta['siup_nib'] }}</td>
                                </tr>
                                <tr>
                                    <td>TDP</td>
                                    <td></td>
                                    <td>{{ $storeMeta['tdp'] }}</td>
                                </tr>

                                <tr>
                                    <td colspan="3"></td>
                                </tr>

                                <tr>
                                    <td>Provinsi</td>
                                    <td></td>
                                    <td>{{ $dropdown['provinces'][$storeMeta['province_id']] ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td>Kabupaten / Kota</td>
                                    <td></td>
                                    <td>{{ $dropdown['cities'][$storeMeta['city_id']] ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td>Kecamatan</td>
                                    <td></td>
                                    <td>{{ $dropdown['districts'][$storeMeta['district_id']] ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td></td>
                                    <td>{{ $storeMeta['alamat'] }}</td>
                                </tr>

                                <tr>
                                    <td>Kode Pos</td>
                                    <td></td>
                                    <td>{{ $storeMeta['kode_pos'] }}</td>
                                </tr>
                                <tr>
                                    <td>Email Toko</td>
                                    <td></td>
                                    <td>{{ $storeMeta['email_toko'] }}</td>
                                </tr>
                                <tr>
                                    <td>No Telepon Kantor</td>
                                    <td></td>
                                    <td>{{ $storeMeta['no_tel_kantor'] }}</td>
                                </tr>

                                <tr>
                                    <td colspan="3"></td>
                                </tr>

                                <tr>
                                    <th colspan="3">
                                        <span class="text-primary">Penanggungjawab / Penandatangan</span>
                                    </th>
                                </tr>

                                <tr>
                                    <td>Nama Penanggungjawab</td>
                                    <td></td>
                                    <td>{{ $storeMeta['nama_lengkap_penanggungjawab'] }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td></td>
                                    <td>{{ $storeMeta['email_penanggungjawab'] }}</td>
                                </tr>
                                <tr>
                                    <td>Jabatan</td>
                                    <td></td>
                                    <td>{{ $storeMeta['jabatan_penanggungjawab'] }}</td>
                                </tr>
                                <tr>
                                    <td>NIK</td>
                                    <td></td>
                                    <td>{{ $storeMeta['nik_penanggungjawab'] }}</td>
                                </tr>

                                <tr>
                                    <td colspan="3"></td>
                                </tr>

                                <tr>
                                    <th colspan="3">
                                        <span class="text-primary">Bank</span>
                                    </th>
                                </tr>

                                <tr>
                                    <td>Nama Bank</td>
                                    <td></td>
                                    <td>{{ $storeMeta['nama_bank'] }}</td>
                                </tr>
                                <tr>
                                    <td>Nama Pemilik Rekening</td>
                                    <td></td>
                                    <td>{{ $storeMeta['nama_pemilik_rekening'] }}</td>
                                </tr>
                                <tr>
                                    <td>No Rekening</td>
                                    <td></td>
                                    <td>{{ $storeMeta['no_rekening'] }}</td>
                                </tr>
                                <tr>
                                    <td>Cabang</td>
                                    <td></td>
                                    <td>{{ $storeMeta['cabang_bank'] }}</td>
                                </tr>

                                <tr>
                                    <td colspan="3"></td>
                                </tr>

                                <tr>
                                    <th colspan="3">
                                        <span class="text-primary">Scan Document</span>
                                    </th>
                                </tr>

                                <tr>
                                    <td>KTP Penanggungjawab</td>
                                    <td></td>
                                    <td>
                                        <a href="{{ asset($storeMeta['scan_ktp_penanggung_jawab']) }}">
                                            Download
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>NPWP</td>
                                    <td></td>
                                    <td>
                                        <a href="{{ asset($storeMeta['scan_npwp']) }}">
                                            Download
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>SIUP</td>
                                    <td></td>
                                    <td>
                                        <a href="{{ asset($storeMeta['scan_siup']) }}">
                                            Download
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>TDP</td>
                                    <td></td>
                                    <td>
                                        <a href="{{ asset($storeMeta['scan_tdp']) }}">
                                            Download
                                        </a>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>



                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
