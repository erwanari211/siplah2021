<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Surat Jalan</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <style type="text/css">
            body {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            }

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4,
            .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8,
            .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }
            .col-sm-12 {
                width: 100%;
            }
            .col-sm-11 {
                width: 91.66666667%;
            }
            .col-sm-10 {
                width: 83.33333333%;
            }
            .col-sm-9 {
                width: 75%;
            }
            .col-sm-8 {
                width: 66.66666667%;
            }
            .col-sm-7 {
                width: 58.33333333%;
            }
            .col-sm-6 {
                width: 50%;
            }
            .col-sm-5 {
                width: 41.66666667%;
            }
            .col-sm-4 {
                width: 33.33333333%;
            }
            .col-sm-3 {
                width: 25%;
            }
            .col-sm-2 {
                width: 16.66666667%;
            }
            .col-sm-1 {
                width: 8.33333333%;
            }

            .text-key {
                font-weight: bold;
                padding-right: 50px;
            }
        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">

            <h3 class="text-center">SURAT JALAN</h3>

            <div class="row">
                <div class="col-sm-4">
                    <table>
                        <tbody>
                            <tr>
                                <td style="width: 128px;">No Surat Jalan</td>
                                <td style="width: 10px;">:</td>
                                <td>....................</td>
                            </tr>
                            <tr>
                                <td style="width: 128px;">Tanggal Pengiriman</td>
                                <td style="width: 10px;">:</td>
                                <td>....................</td>
                            </tr>
                            <tr>
                                <td style="width: 128px;">No Surat Pesanan</td>
                                <td style="width: 10px;">:</td>
                                <td>{{ $order->invoice_prefix.$order->invoice_no }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6">
                    <table>
                        <tbody>
                            <tr>
                                @php
                                    $address = $orderMetas['payment_address'];
                                @endphp
                                <td style="width: 110px; vertical-align: top;">Pemesan</td>
                                <td style="width: 10px; vertical-align: top;">:</td>
                                <td>
                                    {{ $address['full_name'] }} <br>
                                    {{ $address['address'] }} - {{ $address['postcode'] }},
                                    {{ ucwords(strtolower($address['city'])) }},
                                    {{ ucwords(strtolower($address['province'])) }}
                                </td>
                            </tr>
                            <tr>
                                @php
                                    $address = $orderMetas['shipping_address'];
                                @endphp
                                <td style="width: 110px; vertical-align: top;">Alamat Kirim</td>
                                <td style="width: 10px; vertical-align: top;">:</td>
                                <td>
                                    {{ $address['full_name'] }} <br>
                                    {{ $address['address'] }} - {{ $address['postcode'] }},
                                    {{ ucwords(strtolower($address['city'])) }},
                                    {{ ucwords(strtolower($address['province'])) }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row" style="margin-top: 20px;">
                <div class="col-sm-12">
                    <table class="table table-hover table-bordered mb-0">
                        <thead>
                            <tr>
                                <th style="vertical-align:middle; width: 5%;"
                                    class="text-center">
                                    No
                                </th>
                                <th style="vertical-align:middle; width: 10%;"
                                    class="text-center">
                                    Kode
                                </th>
                                <th style="vertical-align:middle; width: 50%;"
                                    class="text-center">
                                    Nama Produk
                                </th>
                                <th style="vertical-align:middle; width: 10%;"
                                    class="text-center">
                                    Qty
                                </th>
                                <th style="vertical-align:middle; width: 25%;"
                                    class="text-center">
                                    Keterangan
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($products as $product)
                                <tr>
                                    <td class="text-center">{{ $no }}</td>
                                    <td>
                                        @if (isset($product->product->sku))
                                            {{ $product->product->sku }}
                                        @endif
                                    </td>
                                    <td>{{ $product->product_name }}</td>
                                    <td class="text-center">{{ $product->qty }} </td>
                                    <td></td>
                                </tr>
                                @php
                                    $no++;
                                @endphp
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="3">TOTAL</td>
                                <td class="text-center">{{ $products->sum('qty') }}</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row" style="margin-top: 5px;">
                <div class="col-sm-12">
                    <table class="table table-hover table-bordered mb-0">
                        <tbody>
                            <tr>
                                <td width="200px">Tanggal dan Jam Diterima</td>
                                <td></td>
                                <td width="150px">No HP</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Nama Sopir / Ekspedisi</td>
                                <td></td>
                                <td>Jenis dan No Mobil</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div style="page-break-inside: avoid;">
                <div class="row" style="margin-top: 20px;">
                    <div class="col-sm-12">
                        <table class="table table-hover table-bordered mb-0">
                            <tbody>
                                <tr>
                                    <td class="text-center" width="20%">Penerima</td>
                                    <td class="text-center" width="20%">Ekspedisi</td>
                                    <td class="text-center" width="20%">Security</td>
                                    <td class="text-center" width="20%">Mengetahui</td>
                                    <td class="text-center" width="20%">Hormat Kami</td>
                                </tr>
                                <tr>
                                    <td height="80px"></td>
                                    <td height="80px"></td>
                                    <td height="80px"></td>
                                    <td height="80px"></td>
                                    <td height="80px"></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>

                        <p>Catatan:</p>
                        <p>
                            Apabila dalam waktu 3 (tiga) hari setelah barang diterima dan tidak ada komplain
                            maka barang yang diterima dianggap sesuai dengan surat jalan
                        </p>

                        <div style="border-left: 3px solid #ccc; padding-left: 10px;">
                            Melalui Marketplace <br>
                            {{ config('app.name') }}
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- jQuery -->
        <script src="//code.jquery.com/jquery.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
         <script src="Hello World"></script>
    </body>
</html>
