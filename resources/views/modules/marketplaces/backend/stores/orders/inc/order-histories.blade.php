<div class="row">
    {{--
    <div class="col-md-4">
        <h4>Add History</h4>
        <div class="box">
            <div class="box-content">
                {!! Form::open([
                    'route' => ['users.stores.order-histories.store', $store->id, $order->id],
                    'method' => 'POST',
                    'files' => true,
                    ]) !!}

                    {!! Form::bs3Select('status', $dropdown['order_statuses']) !!}
                    {!! Form::bs3Select('notify', $dropdown['yes_no'], 0) !!}
                    {!! Form::bs3Textarea('note') !!}
                    {!! Form::bs3File('file') !!}

                    {!! Form::bs3Submit('Save'); !!}
                    <a class="btn btn-default" href="{{ route('users.stores.orders.index', [$store->id]) }}">Back</a>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    --}}
    <div class="col-md-12">
        <h4>Histories</h4>
        <div class="box">
            <div class="box-content">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover mb-0">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Notify</th>
                                <th>Note</th>
                                <th><i class="fa fa-paperclip"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($histories))
                                @foreach ($histories as $history)
                                    <tr>
                                        <td>{{ $history->created_at }}</td>
                                        <td>{{ $history->order_status }}</td>
                                        <td>{{ $history->notify ? 'yes' : 'no' }}</td>
                                        <td>{{ $history->note }}</td>
                                        <td>
                                            @if ($history->has_attachment)
                                                <a class="btn btn-default btn-xs"
                                                    href="{{ asset($history->attachments) }}"
                                                    download="{{ get_filename(asset($history->attachments)) }}" target="_blank">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">No data</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
