<div class="row">
    <div class="col-md-8">
        <h4>
            <i class="fa fa-frown"></i>
            Complain
        </h4>
        <div class="box">
            <div class="box-content">
                <div class="messages">
                    <ul class="message-content">
                        @if (count($activities['complain_comments']))
                            @foreach ($activities['complain_comments'] as $activity)
                                @php
                                    $photo = $activity->user->photo ? asset($activity->user->photo) : '';
                                    $isOwner = false;
                                    if ($store->user_id == $activity->user_id) {
                                        $isOwner = true;
                                    }
                                @endphp
                                @if ($isOwner)
                                    <li class="sent">
                                        <img src="{{ $photo }}" alt="" title="{{ $activity->user->name }}">
                                        <div class="message-content">
                                            <p>
                                                {{ $activity->note }}
                                            </p>
                                            @if ($activity->has_attachment)
                                                <a class=""
                                                    href="{{ asset($activity->attachments) }}"
                                                    download="{{ get_filename(asset($activity->attachments)) }}" target="_blank">
                                                    <i class="fa fa-download"></i>
                                                    Download
                                                </a>
                                            @endif
                                            <span class="message-time">{{ $activity->created_at }}</span>
                                        </div>
                                    </li>
                                @endif
                                @if (!$isOwner)
                                    <li class="replies">
                                        <img src="{{ $photo }}" alt="" title="{{ $activity->user->name }}">
                                        <div class="message-content">
                                            <p>
                                                {{ $activity->note }}
                                            </p>
                                            @if ($activity->has_attachment)
                                                <a class=""
                                                    href="{{ asset($activity->attachments) }}"
                                                    download="{{ get_filename(asset($activity->attachments)) }}" target="_blank">
                                                    <i class="fa fa-download"></i>
                                                    Download
                                                </a>
                                            @endif
                                            <span class="message-time">{{ $activity->created_at }}</span>
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        @else
                            No data
                        @endif
                    </ul>
                </div>

                <hr>

                {!! Form::open([
                    'route' => ['users.stores.order-activities.complain-comments.store', $store->id, $order->id],
                    'method' => 'POST',
                    'files' => true,
                    ]) !!}

                    <div class="hide">
                        {!! Form::bs3Text('group', 'complain') !!}
                        {!! Form::bs3Text('activity', 'complain comment') !!}
                    </div>
                    {!! Form::bs3Textarea('note', null, ['label'=>'Comment']) !!}
                    {!! Form::bs3File('file') !!}

                    {!! Form::bs3Submit('Submit', ['class'=>'btn btn-primary btn-block']); !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
