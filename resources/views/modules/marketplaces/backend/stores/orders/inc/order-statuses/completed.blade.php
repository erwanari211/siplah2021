<h4>Payment from Customer</h4>
<div class="box">
    <div class="box-content">
        <div class="mb-4">
            <div class="d-inline-block mr-3">
                <button class="mb-2 btn btn-sm btn-default">Confirmation</button>
                <button class="mb-2 btn btn-sm btn-default">Packing</button>
                <button class="mb-2 btn btn-sm btn-default">Shipping</button>
                <button class="mb-2 btn btn-sm btn-default">Delivery</button>
                <button class="mb-2 btn btn-sm btn-default">Delivery Document</button>
                <button class="mb-2 btn btn-sm btn-default">Payment from Customer</button>
                <button class="mb-2 btn btn-sm btn-default">Payment to Store</button>
                <button class="mb-2 btn btn-sm btn-primary">Completed</button>
            </div>
        </div>

        <hr>

        @if ($order->status_order_process != 'completed')
            @php
                $orderProcessStatuses = $groupedOrderStatuses['order_process'];

                $orderProcessStatusNotCompleted = $orderProcessStatuses->where('key', 'completed')->first();
            @endphp

            <div data-id="status-form-wrapper" class="hide">
                <div class="status-btn-group mb-3">
                    <button class="btn  {{ $order->status_order_process == 'completed' ? 'btn-info' : 'btn-default' }}" data-role="display-status-form" data-target="status-completed">
                        Completed
                    </button>
                </div>


                <div class="well" data-role="status-form" data-id="status-completed">
                    <div class="mb-3">
                        <strong>Completed</strong>
                    </div>

                    {!! Form::open([
                        'route' => ['users.stores.order.statuses.store', $store->id, $order->id],
                        'method' => 'POST',
                        'files' => true,
                        'style' => 'display: inline-block;'
                    ]) !!}
                        {!! Form::hidden('group', 'order_process') !!}
                        {!! Form::hidden('status', $orderProcessStatusNotCompleted->key) !!}

                        {!! Form::bs3Select('notify', $dropdown['yes_no'], 0) !!}
                        {!! Form::bs3Textarea('note') !!}
                        {!! Form::bs3File('file') !!}

                        <button class="btn btn-default" type="submit" onclick="return confirm('{{ $orderProcessStatusNotCompleted->value }}')">
                            Submit
                        </button>
                    {!! Form::close() !!}
                </div>
            </div>
        @endif

        @if ($order->status_order_process == 'completed')
            <button class="btn btn-success">
                Completed
            </button>
        @endif

    </div>
</div>
