<h4>Payment from Customer</h4>
<div class="box">
    <div class="box-content">
        <div class="mb-4">
            <div class="d-inline-block mr-3">
                <button class="mb-2 btn btn-sm btn-default">Confirmation</button>
                <button class="mb-2 btn btn-sm btn-default">Packing</button>
                <button class="mb-2 btn btn-sm btn-default">Shipping</button>
                <button class="mb-2 btn btn-sm btn-default">Delivery</button>
                <button class="mb-2 btn btn-sm btn-default">Delivery Document</button>
                <button class="mb-2 btn btn-sm btn-primary">Payment from Customer</button>
                <button class="mb-2 btn btn-sm btn-default">Payment to Store</button>
                <button class="mb-2 btn btn-sm btn-default">Completed</button>
            </div>
            <div class="d-inline-block">
                <button class="mb-2 btn btn-sm btn-default">Complain</button>
                <button class="mb-2 btn btn-sm btn-default">Canceled</button>
                <button class="mb-2 btn btn-sm btn-default">Rejected</button>
            </div>
        </div>

        <hr>

        <button class="btn {{ $order->status_payment_from_customer == 'unpaid' ? 'btn-info' : 'btn-default' }}">
            Unpaid
        </button>

        <button class="btn {{ $order->status_payment_from_customer == 'paid' ? 'btn-info' : 'btn-default' }}">
            Paid
        </button>

    </div>
</div>
