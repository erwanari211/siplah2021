@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Show Banner Group')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-banner-id]').on('click', function(event) {
                event.preventDefault();
                var bannerId = $(this).attr('data-banner-id');
                var sortOrder = $(this).attr('data-sort-order');
                console.log(sortOrder);
                $('#banner_id').val(bannerId);
                $('#modal-add-to-banner-group').find('#sort_order').val(sortOrder);
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.banners.index', [$store->id]) }}">
                                Store Banners
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.banner-groups.index', [$store->id]) }}">
                                Groups
                            </a>
                        </li>
                        <li class="active">
                            Detail
                        </li>
                    </ol>

                    <h3>Show Banner Group</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($bannerGroup, [
                                'route' => ['users.stores.banner-groups.update', $store->id, $bannerGroup->id],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('name') !!}
                                {!! Form::bs3Text('display_name') !!}
                                {!! Form::bs3Select('active', $dropdown['yes_no']) !!}
                                {!! Form::bs3Select('type', $dropdown['types']) !!}
                                {!! Form::bs3Number('sort_order') !!}

                                <a class="btn btn-default" href="{{ route('users.stores.banner-groups.index', [$store->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                    <h3>Details</h3>

                    <div class="box">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered mb-0">
                                    <thead>
                                        <tr>
                                            <th>Options</th>
                                            <th>Image</th>
                                            <th>Link</th>
                                            <th>Note</th>
                                            <th>Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($details))
                                            @foreach ($details as $detail)
                                                @php
                                                    $banner = $detail->storeBanner;
                                                @endphp
                                                <tr>
                                                    <td>
                                                        <a class="btn btn-xs btn-success"
                                                            data-toggle="modal"
                                                            data-banner-id="{{ $banner->id }}"
                                                            data-sort-order="{{ $detail->sort_order }}"
                                                            href="#modal-add-to-banner-group" title="Add to group">
                                                            <i class="fa fa-edit"></i>
                                                        </a>

                                                        {!! Form::open([
                                                                'route' => ['users.stores.banner-group-details.destroy', $store->id, $bannerGroup->id, $banner->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td>
                                                        <img class="lazy" src="{{ asset($banner->image) }}" height="50">
                                                    </td>
                                                    <td>
                                                        @if ($banner->link)
                                                            <a href="{{ $banner->link }}">Yes</a>
                                                        @else
                                                            No
                                                        @endif
                                                    </td>
                                                    <td>{{ $banner->note }}</td>
                                                    <td>{{ $detail->sort_order }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5">No Data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    <div class="modal fade" id="modal-add-to-banner-group">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add to group</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open([
                        'route' => ['users.stores.banner-group-details.store', $store->id],
                        'method' => 'POST',
                        'files' => true,
                        ]) !!}


                        <div class="hide">
                            {!! Form::bs3Text('banner_id') !!}
                            {!! Form::bs3Text('group_id', $bannerGroup->id) !!}
                        </div>
                        {!! Form::bs3Number('sort_order') !!}

                        {!! Form::bs3Submit('Save'); !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
