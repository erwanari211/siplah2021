@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Edit Product Collection')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.featured-products.index', [$store->id]) }}">
                                Product Collections
                            </a>
                        </li>
                        <li class="active">
                            Edit
                        </li>
                    </ol>

                    <h3>Edit Product Collection</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($featuredProduct, [
                                'route' => ['users.stores.featured-products.update', $store->id, $featuredProduct->id],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('name') !!}
                                {!! Form::bs3Text('slug') !!}
                                {!! Form::bs3Textarea('description') !!}
                                {!! Form::bs3File('image') !!}
                                @if ($featuredProduct->image)
                                    <div class="form-group ">
                                        <img class="img-thumbnail" src="{{ asset($featuredProduct->image) }}" width="100" height="100">
                                    </div>
                                @endif
                                {!! Form::bs3Select('active', $dropdown['yes_no'], 1) !!}

                                {!! Form::bs3Submit('Update'); !!}
                                <a class="btn btn-default" href="{{ route('users.stores.featured-products.index', [$store->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
