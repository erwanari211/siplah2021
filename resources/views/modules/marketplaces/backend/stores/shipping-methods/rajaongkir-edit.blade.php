@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Edit Shipping Method')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Store Settings</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.settings.shipping-methods.rajaongkir-edit', [$store->id]) }}">
                                Shipping Methods
                            </a>
                        </li>
                        <li class="active">
                            Edit
                        </li>
                    </ol>

                    <h3>Edit Shipping Method</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['users.stores.settings.shipping-methods.rajaongkir-update', $store->id],
                                'method' => 'PUT',
                                'files' => false,
                                ]) !!}

                                @php
                                    $shippingMethodOptions = get_rajaongkir_couriers();
                                @endphp
                                <div class="form-group {{ $errors->has('shipping_methods') ?  'has-error' : '' }}">
                                    {{ Form::label('Shipping Methods') }}
                                    @php
                                        $selectedStoreShippingMethods = $storeShippingMethods->pluck('label')->toArray();
                                    @endphp
                                    @foreach ($shippingMethodOptions as $method => $methodName)
                                        @php
                                            $checked = false;
                                            foreach ($storeShippingMethods as $storeShippingMethod) {
                                                if ($method == $storeShippingMethod->label) {
                                                    if ($storeShippingMethod->active) {
                                                        $checked = true;
                                                    }
                                                    break;
                                                }
                                            }
                                        @endphp
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('shipping_methods[]', $method, $checked); !!}
                                                {{ $methodName }}
                                            </label>
                                        </div>
                                    @endforeach


                                    {{--
                                    @foreach ($storeShippingMethods as $method)
                                        @php
                                            $checked = $method->active;
                                        @endphp
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('shipping_methods[]', $method->label, $checked); !!}
                                                {{ $method->name." ($method->group)" }}
                                            </label>
                                        </div>
                                    @endforeach
                                    --}}

                                    @if ($errors->has('shipping_methods'))
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('shipping_methods') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {!! Form::bs3Submit('Update'); !!}
                                <a class="btn btn-default" href="{{ route('users.stores.index', [$store->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
