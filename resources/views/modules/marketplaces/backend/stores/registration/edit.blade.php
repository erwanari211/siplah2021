@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Store Settings')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <style>
        [data-role="upload-image-wrapper"] [data-role="preview"] img {
            max-height: 150px;
            margin-bottom: 16px;
        }

        .border.border-danger {
            border: 1px solid red;
        }
    </style>
@endpush

@push('js')
    <script>
        $(document).ready(function () {
            $('#nama_lengkap').on('change', function () {
                var value = $(this).val();

                var namaLengkapPenanggungjawabElm = $('#nama_lengkap_penanggungjawab');
                var namaPemilikRekeningElm = $('#nama_pemilik_rekening');

                if (value) {
                    console.log('changed')
                    if (!namaLengkapPenanggungjawabElm.val()) {
                        namaLengkapPenanggungjawabElm.val(value);
                    }

                    if (!namaPemilikRekeningElm.val()) {
                        namaPemilikRekeningElm.val(value);
                    }
                }

                if (value == '') {
                    console.log('changed')
                    namaLengkapPenanggungjawabElm.val('');
                    namaPemilikRekeningElm.val('');
                }
            });

            $('#email').on('change', function () {
                var value = $(this).val();

                var emailPenanggungjawabElm = $('#email_penanggungjawab');

                if (value) {
                    console.log('changed')
                    if (!emailPenanggungjawabElm.val()) {
                        emailPenanggungjawabElm.val(value);
                    }
                }

                if (value == '') {
                    console.log('changed')
                    emailPenanggungjawabElm.val('');
                }
            });

            $('#jabatan').on('change', function () {
                var value = $(this).val();

                var jabatanPenanggungjawabElm = $('#jabatan_penanggungjawab');

                if (value) {
                    console.log('changed')
                    if (!jabatanPenanggungjawabElm.val()) {
                        jabatanPenanggungjawabElm.val(value);
                    }
                }

                if (value == '') {
                    console.log('changed')
                    jabatanPenanggungjawabElm.val('');
                }
            });

            $('#nik').on('change', function () {
                var value = $(this).val();

                var nikPenanggungjawabElm = $('#nik_penanggungjawab');

                if (value) {
                    console.log('changed')
                    if (!nikPenanggungjawabElm.val()) {
                        nikPenanggungjawabElm.val(value);
                    }
                }

                if (value == '') {
                    console.log('changed')
                    nikPenanggungjawabElm.val('');
                }
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('[data-role="upload-image" ]').on('click', function (e) {
                e.preventDefault();
                var target = $(this).attr('data-target');
                $(target).click();
            });

            $('[data-role="upload-image-wrapper"]').on('change', '[type="file"]', function (e) {
                e.preventDefault();
                console.log('changed')

                var input = this;
                var parent = $(this).parents('[data-role="upload-image-wrapper"]');
                // data-role="preview"
                var uploadImgBtn = $(parent).find('[data-role="upload-image"]');
                uploadImgBtn.html('Change Image');

                var previewElm = $(parent).find('[data-role="preview"]');
                readURL(input, previewElm);
            });

            function readURL(input, previewElm) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        var img = e.target.result;

                        var isImage = checkImage(img);
                        if (!isImage) {
                            alert('please upload valid image');
                            return false;
                        }

                        var output = `
                            <img src="${img}" class="img-thumbnail">
                        `;

                        previewElm.html('');
                        previewElm.html(output);
                    }

                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                }
            }

            function checkImage(str) {
                var types = [
                    'data:image/png;base64',
                    'data:image/jpeg;base64',
                ]

                for (let i = 0; i < types.length; i++) {
                    const type = types[i];
                    var isImage = str.includes(type);
                    if (isImage) {
                        return true;
                    }
                }

                return false;
            }

            checkSubmitFormBtn();
            $('[name="setuju_ketentuan_penjual"]').on('change', function () {
                checkSubmitFormBtn();
            });

            function checkSubmitFormBtn() {
                var elm = $('[name="setuju_ketentuan_penjual"]');
                var submitBtn = $("#submit-form-btn");
                var isChecked = elm.is(":checked");

                if (isChecked) {
                    submitBtn.removeAttr('disabled');
                }
                if (!isChecked) {
                    submitBtn.attr('disabled', true);
                }
            }



        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Store Settings</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.settings.store-settings.edit', [$store->id]) }}">
                                Settings
                            </a>
                        </li>
                    </ol>

                    <h3>Store Settings</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'url' => route('users.stores.settings.store-registration.update', $store->id),
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <span>{{ $error }}</span>
                                            <br>
                                        @endforeach
                                    </div>
                                @endif

                                @include('flash::message')

                                <h3 class="text-primary">
                                    Toko
                                </h3>

                                <h5 class="text-primary">
                                    Profile
                                </h5>

                                {!! Form::bs3Text('nama_toko', $store->name, ['label' => 'Nama Toko *']) !!}
                                {!! Form::bs3Select('jenis_usaha', $dropdown['jenisUsaha'], $storeMeta['jenis_usaha'] ?? null, ['label' => 'Jenis Usaha *']) !!}
                                {!! Form::bs3Select('kelas_usaha', $dropdown['kelasUsaha'], $storeMeta['kelas_usaha'] ?? null, ['label' => 'Kelas Usaha *']) !!}
                                {!! Form::bs3Select('status', $dropdown['statusUsaha'], $storeMeta['status_usaha'] ?? null, ['label' => 'Status *']) !!}

                                {!! Form::bs3Text('npwp', $storeMeta['npwp'] ?? null, ['label' => 'NPWP *']) !!}
                                {!! Form::bs3Text('siup_nib', $storeMeta['siup_nib'] ?? null, ['label' => 'SIP/NIB *']) !!}
                                {!! Form::bs3Text('tdp', $storeMeta['tdp'] ?? null, ['label' => 'TDP *']) !!}

                                <hr>

                                {{-- {!! Form::bs3Text('kab_kota', null, ['label' => 'Kab/Kota *']) !!} --}}
                                {!! Form::bs3Select('province_id', $dropdown['provinces'], $storeMeta['province_id'] ?? null, ['label'=>'Provinsi *']) !!}
                                {!! Form::bs3Select('city_id', $dropdown['cities'], $storeMeta['city_id'] ?? null, ['label'=>'Kab / Kota *']) !!}
                                {!! Form::bs3Select('district_id', $dropdown['districts'], $storeMeta['district_id'] ?? null, ['label'=>'Kecamatan *']) !!}

                                {!! Form::bs3Textarea('alamat', $storeMeta['alamat'] ?? null, ['label' => 'Alamat *']) !!}
                                {{-- {!! Form::bs3Text('geolokasi', $storeMeta['geolokasi'] ?? null, ['label' => 'Geolokasi *']) !!} --}}

                                {!! Form::bs3Text('kode_pos', $storeMeta['kode_pos'] ?? null, ['label' => 'Kode Pos *']) !!}
                                {!! Form::bs3Text('email_toko', $storeMeta['email_toko'] ?? null, ['label' => 'Email Toko *']) !!}
                                {!! Form::bs3Text('no_tel_kantor', $storeMeta['no_tel_kantor'] ?? null, ['label' => 'No Telepon Kantor *']) !!}

                                <hr>

                                <h5 class="text-primary">
                                    Penanggungjawab / Penandatangan
                                </h5>

                                {!! Form::bs3Text('nama_lengkap_penanggungjawab', $storeMeta['nama_lengkap_penanggungjawab'] ?? null, ['label' => 'Nama Lengkap *']) !!}
                                {!! Form::bs3Text('email_penanggungjawab', $storeMeta['email_penanggungjawab'] ?? null, ['label' => 'Email *']) !!}
                                {!! Form::bs3Text('jabatan_penanggungjawab', $storeMeta['jabatan_penanggungjawab'] ?? null, ['label' => 'Jabatan *']) !!}
                                {!! Form::bs3Text('nik_penanggungjawab', $storeMeta['nik_penanggungjawab'] ?? null, ['label' => 'NIK/KTP *']) !!}

                                <br>

                                <h3 class="text-primary">
                                    Bank
                                </h3>

                                {!! Form::bs3Text('nama_bank', $storeMeta['nama_bank'] ?? null, ['label' => 'Nama Bank *']) !!}
                                {!! Form::bs3Text('nama_pemilik_rekening', $storeMeta['nama_pemilik_rekening'] ?? null, ['label' => 'Nama Pemilik Rekening *']) !!}
                                {!! Form::bs3Text('no_rekening', $storeMeta['no_rekening'] ?? null, ['label' => 'Nomor Rekening *']) !!}
                                {!! Form::bs3Text('cabang_bank', $storeMeta['cabang_bank'] ?? null, ['label' => 'Cabang Bank *']) !!}

                                <hr>

                                <h3 class="text-primary">
                                    Scan Document
                                </h3>

                                <div class="mb-3">
                                    <span>
                                        Gunakan file gambar berekstensi <strong>.png</strong> atau <strong>.jpg</strong>, maksimum ukuran 1 MB
                                    </span>
                                </div>

                                <div class="text-center">
                                    <label for="scan_ktp_penanggung_jawab" class="control-label">
                                        KTP Penanggungjawab
                                    </label>
                                    <div class="well text-center {{ $errors->has('scan_ktp_penanggung_jawab') ? ' border border-danger' : '' }}"
                                        data-role="upload-image-wrapper">
                                        <div data-role="preview">
                                            @if ($storeMeta['scan_ktp_penanggung_jawab'])
                                                <img src="{{ asset($storeMeta['scan_ktp_penanggung_jawab']) }}" class="img-thumbnail">
                                            @endif
                                        </div>
                                        <button class="btn btn-secondary"
                                            data-role="upload-image"
                                            data-target="#scan_ktp_penanggung_jawab">
                                            Upload Image
                                        </button>
                                        <div class="hidden">
                                            {!! Form::file('scan_ktp_penanggung_jawab', [
                                                'id' => 'scan_ktp_penanggung_jawab',
                                                'accept' => "image/*"
                                            ]) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <label for="scan_npwp" class="control-label">
                                        NPWP
                                    </label>
                                    <div class="well text-center  {{ $errors->has('scan_npwp') ? ' border border-danger' : '' }}"
                                        data-role="upload-image-wrapper">
                                        <div data-role="preview">
                                            @if ($storeMeta['scan_npwp'])
                                                <img src="{{ asset($storeMeta['scan_npwp']) }}" class="img-thumbnail">
                                            @endif
                                        </div>
                                        <button class="btn btn-secondary"
                                            data-role="upload-image"
                                            data-target="#scan_npwp">
                                            Upload Image
                                        </button>
                                        <div class="hidden">
                                            {!! Form::file('scan_npwp', [
                                                'id' => 'scan_npwp',
                                                'accept' => "image/*"
                                            ]) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <label for="scan_siup" class="control-label">
                                        SIUP/NIB
                                    </label>
                                    <div class="well text-center  {{ $errors->has('scan_siup') ? ' border border-danger' : '' }}"
                                        data-role="upload-image-wrapper">
                                        <div data-role="preview">
                                            @if ($storeMeta['scan_siup'])
                                                <img src="{{ asset($storeMeta['scan_siup']) }}" class="img-thumbnail">
                                            @endif
                                        </div>
                                        <button class="btn btn-secondary"
                                            data-role="upload-image"
                                            data-target="#scan_siup">
                                            Upload Image
                                        </button>
                                        <div class="hidden">
                                            {!! Form::file('scan_siup', [
                                                'id' => 'scan_siup',
                                                'accept' => "image/*"
                                            ]) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <label for="scan_tdp" class="control-label">
                                        TDP
                                    </label>
                                    <div class="well text-center  {{ $errors->has('scan_tdp') ? ' border border-danger' : '' }}"
                                        data-role="upload-image-wrapper">
                                        <div data-role="preview">
                                            @if ($storeMeta['scan_tdp'])
                                                <img src="{{ asset($storeMeta['scan_tdp']) }}" class="img-thumbnail">
                                            @endif
                                        </div>
                                        <button class="btn btn-secondary"
                                            data-role="upload-image"
                                            data-target="#scan_tdp">
                                            Upload Image
                                        </button>
                                        <div class="hidden">
                                            {!! Form::file('scan_tdp', [
                                                'id' => 'scan_tdp',
                                                'accept' => "image/*"
                                            ]) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <label style="font-weight: normal">
                                        <input type="checkbox" name="setuju_ketentuan_penjual">
                                        Saya Setuju terhadap
                                        <a href="#" target="_blank">Ketentuan Penjual</a>
                                    </label>
                                </div>

                                <button class="btn btn-primary" type="submit" id="submit-form-btn" title="Kirim Ulang Data Pendaftaran">
                                    Kirim
                                </button>

                            {!! Form::close() !!}
                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
