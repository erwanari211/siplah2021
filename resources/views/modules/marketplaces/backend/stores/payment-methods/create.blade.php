@php
    $defaultInfo = '
Please Transfer to following bank account : <br>
{Bank Name} <br>
a/n {Name} <br>
{Bank Account Number} <br>
    ';

    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Create Payment Method')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Store Settings</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.settings.payment-methods.index', [$store->id]) }}">
                                Payment Methods
                            </a>
                        </li>
                        <li class="active">
                            Create
                        </li>
                    </ol>

                    <h3>Create Payment Method</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['users.stores.settings.payment-methods.store', $store->id],
                                'method' => 'POST',
                                'files' => false,
                                ]) !!}

                                {!! Form::bs3Text('label', 'Bank Name') !!}
                                {!! Form::bs3Text('name', 'Bank Name') !!}
                                {!! Form::bs3Textarea('info', $defaultInfo) !!}

                                {!! Form::bs3Submit('Save'); !!}
                                <a class="btn btn-default" href="{{ route('users.stores.settings.payment-methods.index', [$store->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
