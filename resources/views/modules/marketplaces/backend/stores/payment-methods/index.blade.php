@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Payment Methods')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Store Settings</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.settings.payment-methods.index', [$store->id]) }}">
                                Payment Methods
                            </a>
                        </li>
                    </ol>

                    <h3>Payment Methods</h3>

                    @include('flash::message')

                    <a class="btn btn-primary" href="{{ route('users.stores.settings.payment-methods.create', [$store->id]) }}">Add Payment Method</a>

                    <div class="box mt-4">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Label</th>
                                            <th>Name</th>
                                            <th>Info</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($paymentMethods))
                                            @php
                                                $no = $paymentMethods->firstItem();
                                            @endphp
                                            @foreach ($paymentMethods as $paymentMethod)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-success"
                                                            href="{{ route('users.stores.settings.payment-methods.edit', [$store->id, $paymentMethod->id]) }}" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['users.stores.settings.payment-methods.destroy', $store->id, $paymentMethod->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                        @php
                                                            $defaultBtnClass = $paymentMethod->default_payment_method ? 'btn-success' : 'btn-default';
                                                        @endphp
                                                        {!! Form::open([
                                                                'route' => [
                                                                    'users.stores.settings.payment-methods.default-addresses.update',
                                                                    $store->id, $paymentMethod->id
                                                                ],
                                                                'method' => 'PUT',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-check"></i>', ['class'=>'btn btn-xs '.$defaultBtnClass, 'onclick'=>'return confirm("Set as default?")']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td>
                                                        {{ $paymentMethod->label }}
                                                        @if ($paymentMethod->default_payment_method)
                                                            <br>
                                                            <span class="label label-success">Default method</span>
                                                        @endif
                                                    </td>
                                                    <td> {{ $paymentMethod->name }} </td>
                                                    <td> {!! $paymentMethod->info !!} </td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $paymentMethods->links() }}
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
