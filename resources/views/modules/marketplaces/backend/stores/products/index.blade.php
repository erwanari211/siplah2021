@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Products')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
@endpush

@push('js')
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-product-id]').on('click', function(event) {
                event.preventDefault();
                var productId = $(this).attr('data-product-id');
                $('#product').val(productId);
            });

            $('[data-action="submit-form"]').on('click', function(event) {
                event.preventDefault();
                /* Act on the event */
                var form = $('#form-add-product-to-collection');
                var url = form.attr('action');
                var collectionId = $('#collection_id').val();
                if (collectionId) {
                    newUrl = url.replace('collection-id', collectionId);
                    form.attr('action', newUrl);
                    form.submit();
                }

            });
            // $('#form-add-product-to-collection').on('submit', function(event) {
            //     event.preventDefault();
            //     var url = $(this).attr('action');
            //     var collectionId = $('#collection_id').val();
            //     newUrl = url.replace('collection-id', collectionId);
            //     $(this).attr('action', newUrl);
            //     $(this).trigger('submit');
            // });

            $.fn.select2.defaults.set( "theme", "bootstrap" );

            $('.select2').select2({
                // placeholder: 'Please Select'
            });

            $('[data-toggle="popover"]').popover();
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li class="active">
                            Store Products
                        </li>
                    </ol>

                    <h3>Products</h3>

                    @include('flash::message')

                    <a class="btn btn-primary" href="{{ route('users.stores.products.create', [$store->id]) }}">Add Product</a>
                    <a class="btn btn-success" href="{{ route('users.stores.import-products.create', [$store->id]) }}">Import Product</a>
                    <br><br>

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['users.stores.products.index', $store->id],
                                'method' => 'GET',
                                'files' => true,
                                ]) !!}

                                <div class="row">
                                    <div class="col-sm-12 hide">
                                        @if (request('active'))
                                            {!! Form::bs3Text('active', request('active')) !!}
                                        @endif
                                        @if (request('status'))
                                            {!! Form::bs3Text('status', request('status')) !!}
                                        @endif
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Form::bs3Text('name', request('name')) !!}
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Form::bs3Text('sku', request('sku')) !!}
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Form::bs3Select('category_id', $dropdown['categories'], request('category_id'), [
                                            'class'=>'form-control select2', 'label'=>'Category'
                                        ]) !!}
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Form::bs3Select('collection_id', $dropdown['collections'], request('collection_id'), [
                                            'class'=>'form-control select2', 'label'=>'Collection'
                                        ]) !!}
                                    </div>

                                </div>

                                {!! Form::bs3Submit('Filter'); !!}
                                <a class="btn btn-default"
                                    href="{{ route('users.stores.products.index', $store->id) }}">
                                    Reset
                                </a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="box">
                        <div class="box-content">
                            <div style="display: inline-block;">
                                <strong>Active</strong>
                                <br>
                                @php
                                    $statuses = [
                                        'true' => 'Active',
                                        'false' => 'Inactive'
                                    ];
                                @endphp
                                @foreach ($statuses as $status => $label)
                                    @php
                                        $routeParams = array_merge($filters, ['active'=>$status]);
                                        $routeParams = [$store->id] + $routeParams;
                                        $class = $filters['active'] == $status ? 'btn btn-primary' : 'btn btn-default';
                                    @endphp
                                    <a class="{{ $class }}"
                                        href="{{ route('users.stores.products.index', $routeParams) }}">
                                        {{ $label }}
                                    </a>
                                @endforeach
                                {!! nbsp(3, true) !!}
                            </div>

                            <div style="display: inline-block;">
                                <strong>Status</strong> <br>
                                @php
                                    $statuses = [
                                        'active' => 'Active',
                                        'inactive' => 'Inactive',
                                        'archived' => 'Archived',
                                    ];
                                @endphp
                                @foreach ($statuses as $status => $label)
                                    @php
                                        $routeParams = array_merge($filters, ['status'=>$status]);
                                        $routeParams = [$store->id] + $routeParams;
                                        $class = $filters['status'] == $status ? 'btn btn-primary' : 'btn btn-default';
                                    @endphp
                                    <a class="{{ $class }}"
                                        href="{{ route('users.stores.products.index', $routeParams) }}">
                                        {{ $label }}
                                    </a>
                                @endforeach
                                {!! nbsp(3, true) !!}
                            </div>

                            <div style="display: inline-block;">
                                <strong>Approved Status</strong> <br>
                                @foreach ($approvedStatuses as $status => $label)
                                    @php
                                        $routeParams = array_merge($filters, ['approved_status'=>$status]);
                                        $routeParams = [$store->id] + $routeParams;
                                        $class = $filters['approved_status'] == $status ? 'btn btn-primary' : 'btn btn-default';
                                    @endphp
                                    <a class="{{ $class }}"
                                        href="{{ route('users.stores.products.index', $routeParams) }}">
                                        {{ $label }}
                                    </a>
                                @endforeach
                                {!! nbsp(3, true) !!}
                            </div>

                            <div style="display: inline-block;">
                                <br>
                                <a class="btn btn-default"
                                    href="{{ route('users.stores.products.index', $store->id) }}">
                                    Reset
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>SKU</th>
                                            <th>Price</th>
                                            <th>Qty</th>
                                            <th>Category</th>
                                            {{-- <th>Collection</th> --}}
                                            <th>Active</th>
                                            <th>Status</th>
                                            <th>Approved</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($products))
                                            @php
                                                $no = $products->firstItem();
                                            @endphp
                                            @foreach ($products as $product)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-default"
                                                            data-toggle="modal"
                                                            data-product-id="{{ $product->id }}"
                                                            href="#modal-add-to-collection" title="Add to collection">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                        <a class="btn btn-xs btn-default"
                                                            href="{{ route('users.stores.products.show', [$store->id, $product->id]) }}" title="View">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                        <a class="btn btn-xs btn-success"
                                                            href="{{ route('users.stores.products.edit', [$store->id, $product->id]) }}" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        @if ($product->active)
                                                            {!! Form::open([
                                                                    'route' => ['users.stores.products.destroy', $store->id, $product->id],
                                                                    'method' => 'DELETE',
                                                                    'style' => 'display: inline-block;'
                                                                ]) !!}
                                                                {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                            {!! Form::close() !!}
                                                        @else
                                                            {!! Form::open([
                                                                    'route' => ['users.stores.products.restore', $store->id, $product->id],
                                                                    'method' => 'PUT',
                                                                    'style' => 'display: inline-block;'
                                                                ]) !!}
                                                                {!! Form::bs3SubmitHtml('<i class="fa fa-sync-alt"></i>', ['class'=>'btn btn-xs btn-info', 'onclick'=>'return confirm("Restore?")']); !!}
                                                            {!! Form::close() !!}
                                                        @endif

                                                        <a class="btn btn-xs btn-default"
                                                            href="{{ route('products.show', [$marketplace->slug, $store->slug, $product->slug]) }}"
                                                            target="_blank">
                                                            <i class="fa fa-external-link-alt"></i>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <img src="{{ $product->image_url }}" width="50" height="50">
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('users.stores.products.show', [$store->id, $product->id]) }}">
                                                            {{ $product->name }}
                                                        </a>
                                                    </td>
                                                    <td>{{ $product->sku }}</td>
                                                    <td>
                                                        {{ formatNumber($product->price) }}
                                                        @if ($product->getMeta('has_kemdikbud_zone_price'))
                                                            <br>
                                                            <i class="fa fa-globe"
                                                                data-toggle="tooltip"
                                                                title="Has zone prices"></i>
                                                        @endif
                                                    </td>
                                                    <td>{{ formatNumber($product->qty) }}</td>
                                                    <td>
                                                        @if (count($product->marketplaceCategories))
                                                            @foreach ($product->marketplaceCategories as $productCategory)
                                                                <span class="btn btn-primary btn-xs">{{ $productCategory->name }}</span>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    {{--
                                                    <td>
                                                        @if (count($product->storeCategories))
                                                            @foreach ($product->storeCategories as $productCategory)
                                                                <span class="btn btn-primary btn-xs">{{ $productCategory->name }}</span>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    --}}
                                                    <td>
                                                        {!! $product->active_button !!}
                                                    </td>
                                                    <td>
                                                        {!! $product->status_button !!}
                                                    </td>
                                                    <td>
                                                        {!! $product->approved_status_button !!}
                                                    </td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="11">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $products->appends(request()->only(['status', 'active', 'name', 'sku', 'collection_id', 'category_id']))->links() }}
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-add-to-collection">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add to collection</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open([
                        'route' => ['users.stores.collections.products.store', $store->id, 'collection-id'],
                        'method' => 'POST',
                        'files' => true,
                        'id' => 'form-add-product-to-collection',
                        ]) !!}

                        <div class="hide">
                            {!! Form::bs3Text('product') !!}
                        </div>
                        {!! Form::bs3Select('collection_id', $dropdown['collections']) !!}

                        <button type="button" class="btn btn-primary" data-action="submit-form">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
