@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Import Product')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">


                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.products.index', [$store->id]) }}">
                                Store Products
                            </a>
                        </li>
                        <li class="active">
                            Import
                        </li>
                    </ol>

                    <h3>Import Products</h3>
                    @include('flash::message')
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs border-bottom-nav">
                            <li role="presentation" class="active">
                                <a href="#tab-create-products" data-toggle="tab">Create products</a>
                            </li>
                            <li role="presentation">
                                <a href="#tab-edit-products" data-toggle="tab">Edit products</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane pt-4 active" id="tab-create-products">
                                <div class="box">
                                    <div class="box-content">
                                        {!! Form::open([
                                            'route' => ['users.stores.import-products.store', $store->id],
                                            'method' => 'POST',
                                            'files' => true,
                                            ]) !!}

                                            {!! Form::bs3File('file') !!}
                                            <a class="help-block" href="{{ asset('assets/upload-templates/sample-product-import.xlsx') }}">
                                                Download excel template
                                            </a>

                                            {!! Form::bs3Submit('Import'); !!}
                                            <a class="btn btn-default" href="{{ route('users.stores.products.index', [$store->id]) }}">Back</a>

                                        {!! Form::close() !!}
                                    </div>
                                </div>

                                <div class="alert alert-warning">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <strong>Note</strong> <br>
                                    In products sheet : <br>
                                    SKU is required and must be unique <br>
                                    Price, qty, and weight must br integer <br>
                                    Weight is in gram <br>
                                    Fill product active column with 1 if active, or 0 if inactive <br>
                                    Image must be valid image url <br>
                                    Sheet have at least 1 row, if no data just fill with " - " <br>
                                    <br>

                                    In product attributes sheet : <br>
                                    SKU and name is required <br>
                                    Sort order must be integer <br>
                                    Sheet have at least 1 row, if no data just fill with " - " <br>
                                </div>
                            </div>
                            <div class="tab-pane pt-4" id="tab-edit-products">
                                {{--
                                <div class="box">
                                    <div class="box-content">
                                        {!! Form::open([
                                            'route' => ['users.stores.import-products.update', $store->id],
                                            'method' => 'PUT',
                                            'files' => true,
                                            ]) !!}

                                            {!! Form::bs3File('file') !!}
                                            <a class="help-block" href="{{ route('users.stores.export-products.download', [$store->id]) }}">
                                                Export your products
                                            </a>

                                            {!! Form::bs3Submit('Import'); !!}
                                            <a class="btn btn-default" href="{{ route('users.stores.products.index', [$store->id]) }}">Back</a>

                                        {!! Form::close() !!}
                                    </div>
                                </div>

                                <div class="alert alert-warning">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <strong>Note</strong> <br>
                                    In products sheet : <br>
                                    SKU is required and must be unique <br>
                                    Price, qty, and weight must br integer <br>
                                    Weight is in gram <br>
                                    Fill product active column with 1 if active, or 0 if inactive <br>
                                    Image must be valid image url <br>
                                    If you dont want to edit product image, leave it blank <br>
                                    If you want to edit category, fill it with category id and category slug (NEW) <br>
                                    You can get category id and category slug in categories sheet (NEW) <br>
                                    Sheet have at least 1 row, if no data just fill with " - " <br>
                                    <br>

                                    In product attributes sheet : <br>
                                    SKU and name is required <br>
                                    Sort order must be integer <br>
                                    If attribute dont exist, it will be created <br>
                                    If attribute already exist, it will be overwrited <br>
                                    If you dont need to edit your product attribute,
                                    you can delete available data to make import faster<br>
                                    Sheet have at least 1 row, if no data just fill with " - " <br>
                                </div>
                                --}}
                                @include('modules.marketplaces.backend.stores.products.export.export-form')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
