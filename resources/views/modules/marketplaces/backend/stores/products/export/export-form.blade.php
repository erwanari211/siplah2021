<div class="box">
    <div class="box-content">
        {!! Form::open([
            'route' => ['users.stores.import-products.update', $store->id],
            'method' => 'PUT',
            'files' => true,
            ]) !!}

            {!! Form::bs3File('file') !!}
            <span class="help-block">
                You can download excel templete by exporting products in form below
            </span>

            {!! Form::bs3Submit('Import'); !!}
            <a class="btn btn-default" href="{{ route('users.stores.products.index', [$store->id]) }}">Back</a>

        {!! Form::close() !!}
    </div>
</div>

<div class="box">
    <div class="box-content">
        <div class="alert alert-info">
            Recommended export maximal 100 products
        </div>

        {!! Form::open([
            'route' => ['users.stores.export-products.download', $store->id],
            'method' => 'POST',
            'files' => true,
            ]) !!}

            {!! Form::bs3Number('start', 1) !!}
            {!! Form::bs3Number('end', $totalProducts) !!}

            {!! Form::bs3Submit('Export Product'); !!}

        {!! Form::close() !!}
    </div>
</div>

<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Note</strong> <br>
    In products sheet : <br>
    SKU is required and must be unique <br>
    Price, qty, and weight must br integer <br>
    Weight is in gram <br>
    Fill product active column with 1 if active, or 0 if inactive <br>
    Image must be valid image url <br>
    If you dont want to edit product image, leave it blank <br>
    If you want to edit category, fill it with category id and category slug (NEW) <br>
    You can get category id and category slug in categories sheet (NEW) <br>
    Sheet have at least 1 row, if no data just fill with " - " <br>
    <br>

    In product attributes sheet : <br>
    SKU and name is required <br>
    Sort order must be integer <br>
    If attribute dont exist, it will be created <br>
    If attribute already exist, it will be overwrited <br>
    If you dont need to edit your product attribute,
    you can delete available data to make import faster<br>
    Sheet have at least 1 row, if no data just fill with " - " <br>
</div>
