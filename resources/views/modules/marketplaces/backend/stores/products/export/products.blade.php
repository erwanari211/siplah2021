<table border="1">
    <thead>
        <tr>
            <th>no</th>
            <th>unique_code</th>
            <th>sku</th>
            <th>name</th>
            <th>description</th>
            <th>price</th>
            <th>qty</th>
            <th>image</th>
            <th>weight</th>
            <th>active</th>
            <th>category_id</th>
            <th>category_slug</th>
            <th>collection_id</th>
            <th>collection_slug</th>
        </tr>
    </thead>
    <tbody>
        @if (count($products))
            @foreach($products as $product)
                <tr>
                    <td>{{ $product['index'] }}</td>
                    <td>{{ $product['unique_code'] }}</td>
                    <td>{{ $product['sku'] }}</td>
                    <td>{{ $product['name'] }}</td>
                    <td>{{ $product['description'] }}</td>
                    <td>{{ $product['price'] }}</td>
                    <td>{{ $product['qty'] }}</td>
                    <td>{{ $product['image'] }}</td>
                    <td>{{ $product['weight'] }}</td>
                    <td>{{ $product['active'] }}</td>

                    @if (count($product['categories']))
                        @php
                            $categoryIds = [];
                            $categorySlugs = [];
                            foreach ($product['categories'] as $category) {
                                $categoryIds[] = $category['id'];
                                $categorySlugs[] = $category['slug'];
                            }
                        @endphp
                        <td>
                            {{ implode(', ', $categoryIds) }}
                        </td>
                        <td>
                            {{ implode(', ', $categorySlugs) }}
                        </td>
                    @else
                        <td></td>
                        <td></td>
                    @endif

                    @if (count($product['collections']))
                        @php
                            $categoryIds = [];
                            $categorySlugs = [];
                            foreach ($product['collections'] as $category) {
                                $categoryIds[] = $category['id'];
                                $categorySlugs[] = $category['slug'];
                            }
                        @endphp
                        <td>
                            {{ implode(', ', $categoryIds) }}
                        </td>
                        <td>
                            {{ implode(', ', $categorySlugs) }}
                        </td>
                    @else
                        <td></td>
                        <td></td>
                    @endif
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
