<table>
    <thead>
        <tr>
            <th>id</th>
            <th>slug</th>
            <th>name</th>
        </tr>
    </thead>
    <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{ $category['id'] }}</td>
                <td>{{ $category['slug'] }}</td>
                <td>{{ $category['name'] }}</td>
            </tr>
            @if (count($category->childs))
                @foreach ($category->childs as $childCategory)
                    <tr>
                        <td>{{ $childCategory['id'] }}</td>
                        <td>{{ $childCategory['slug'] }}</td>
                        <td>{{ $category['name'] }} > {{ $childCategory['name'] }}</td>
                    </tr>
                @endforeach
            @endif
        @endforeach
    </tbody>
</table>
