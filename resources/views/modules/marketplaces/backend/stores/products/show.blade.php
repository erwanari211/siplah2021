@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Detail Product')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
@endpush

@push('js')
    <script src="{{ asset('assets/plugins') }}/tinymce_4.7.9/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            /**
             * Tinymce settings
             */
            var plugins = 'preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern code help spellchecker';
            var toolbar1 = 'formatselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat';
            var toolbar2 = 'undo redo | link unlink image media styleselect | nanospell | fullscreen preview code ';

            tinymce.init({
                    selector: '#description',
                    height: 300,
                    plugins: plugins,
                    toolbar1: toolbar1,
                    toolbar2: toolbar2,
                    image_advtab: true,
                    selection_toolbar: 'bold italic | quicklink h2 h3 blockquote',
                    imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",
                    content_css: [
                        '//tiny.cloud/css/codepen.min.css',
                        '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'
                    ],

                    relative_urls : false,
                    remove_script_host : false,
                    convert_urls : true,
                    image_class_list: [
                        {title: 'Lazyload', value: 'lazy'},
                        {title: 'None', value: ''},
                    ],

                    table_default_attributes: {
                        'class': 'table table-bordered'
                    },
                    table_default_styles: {},
                    table_class_list: [
                        {title: 'None', value: ''},
                        {title: 'Table Bordered', value: 'table table-bordered'},
                    ],
                });

            $.fn.select2.defaults.set( "theme", "bootstrap" );

            $('.select2').select2();
        });
    </script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            var categoryWrapperElm = $('[data-role="category-wrapper"]');
            var url = "{{ route('users.stores.ajax.categories.index', $store->id) }}";
            console.log(url);

            function removeChildrenDropdownCategory(level = null) {
                if (level >= 0) {
                    var dropdowns = $('[data-role="dropdown-category"]');
                    for (let i = 0; i < dropdowns.length; i++) {
                        var dropdown = dropdowns[i];
                        var dropdownDataLevel = $(dropdown).attr('data-level');
                        if (dropdownDataLevel > level) {
                            $(dropdown).parents('.form-group').remove();
                        }
                    }
                }
            }

            $('body').on('change', '[data-role="dropdown-category"]', function () {
                var dropdownValue = $(this).val();
                var level = $(this).attr('data-level');

                removeChildrenDropdownCategory(level);
                if (!dropdownValue) {
                    return false;
                }

                $.ajax({
                    type: "GET",
                    url: url,
                    data: {
                        parent_id: dropdownValue,
                    },
                    dataType: "json",
                    success: function (response) {
                        var categories = response;
                        var isEmpty = $.isEmptyObject(categories);
                        if (!isEmpty) {

                            var options = ``;
                            for (const key in categories) {
                                var category = categories[key];
                                var id = key.replace('id-', '');
                                options += `
                                    <option value="${id}">
                                        ${category}
                                    </option>
                                `;
                            }

                            var newLevel = parseInt(level) + 1;
                            var elm = `
                                <div class="form-group">
                                    <label class="control-label">
                                        Subcategory ${newLevel}
                                    </label>
                                    <select class="form-control new-select2"
                                        style="width:100%"
                                        data-level="${newLevel}"
                                        data-role="dropdown-category"
                                        name="category[]">
                                        <option value="">Please Select</option>
                                        ${options}
                                    </select>
                                </div>
                            `;

                            $(categoryWrapperElm).append(elm);

                            $(categoryWrapperElm)
                                .find('.new-select2')
                                .addClass('.select2')
                                .removeClass('.new-select2')
                                .select2();
                        }

                    }
                });
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            function in_array(needle, haystack = []) {
                if (Array.isArray(haystack)) {
                    return haystack.indexOf(needle) != -1;
                }

                return false;
            }

            $('#type').on('change', function () {
                handleTypeOnChange();
            });

            handleTypeOnChange();

            function handleTypeOnChange() {
                var type = $('#type').val();

                $('[data-id="product-meta-wrapper"]').hide();
                $('[data-id="service-meta-wrapper"]').hide();

                var productTypes = ['product', 'grouped product', 'digital product'];
                var serviceTypes = ['service'];

                if (in_array(type, productTypes)) {
                    $('[data-id="product-meta-wrapper"]').show();
                }

                if (in_array(type, serviceTypes)) {
                    $('[data-id="service-meta-wrapper"]').show();
                }
            }
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.products.index', [$store->id]) }}">
                                Store Products
                            </a>
                        </li>
                        <li class="active">
                            Detail
                        </li>
                    </ol>

                    <h3>Detail Product</h3>

                    @include('flash::message')

                    @php
                        $productMetas = $product->getAllMeta();
                        $originalPrice = isset($productMetas['original_price']) ? $productMetas['original_price'] : 0;
                        $discountAmountPercent = $product->discountAmountPercent;

                        $approvedStatusReason = $productMetas['approved_status_reason'] ?? null;
                    @endphp

                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs border-bottom-nav">
                            <li class="active">
                                <a href="#tab-detail" data-toggle="tab">Detail</a>
                            </li>
                            <li>
                                <a href="#tab-attributes" data-toggle="tab">Attributes</a>
                            </li>
                            <li>
                                <a href="#tab-galleries" data-toggle="tab">Galleries</a>
                            </li>
                            @if ($product->type == 'grouped product')
                                <li>
                                    <a href="#tab-grouped-product" data-toggle="tab">Group Product</a>
                                </li>
                            @endif
                            @if ($hasKemdikbudZonePrice)
                                <li>
                                    <a href="#tab-zone-price" data-toggle="tab">Zone Price</a>
                                </li>
                            @endif
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active pt-4" id="tab-detail">
                                <div class="box">
                                    <div class="box-content">
                                        {!! Form::model($product, [
                                            'route' => ['users.stores.products.update', $store->id, $product->id],
                                            'method' => 'PUT',
                                            'files' => true,
                                            ]) !!}

                                            {!! Form::bs3Select('is_approved', $dropdown['yes_no'], null, ['disabled']) !!}
                                            @if ($product->approved_status == 'rejected')
                                                {!! Form::bs3Textarea('reason', $approvedStatusReason, ['disabled']) !!}
                                            @endif

                                            <hr>

                                            {!! Form::bs3Text('name') !!}
                                            {!! Form::bs3Text('slug') !!}
                                            {!! Form::bs3Text('sku', null, ['label'=>'SKU']) !!}
                                            {!! Form::bs3Textarea('description') !!}
                                            {!! Form::bs3Number('price') !!}
                                            {!! Form::bs3Number('original_price', $originalPrice) !!}
                                            {!! Form::bs3Number('discount', $discountAmountPercent, ['readonly'=>'readonly', 'label'=>'Discount (%)']) !!}

                                            {{--
                                            {!! Form::bs3Select('category[]', $dropdown['marketplaceCategories'], $productMarketplaceCategories, [
                                                'multiple'=>'multiple', 'class'=>'form-control select2', 'label'=>'Category', 'readonly'=>'readonly'
                                            ]) !!}
                                            {!! Form::bs3Select('collection[]', $dropdown['storeCategories'], $productStoreCategories, [
                                                'multiple'=>'multiple', 'class'=>'form-control select2', 'label'=>'Collection'
                                            ]) !!}
                                            --}}

                                            <hr>

                                            <div data-role="category-wrapper">
                                                {!! Form::bs3Select('category[]', $dropdown['marketplaceCategories'], $productMarketplaceCategories, [
                                                    'class'=>'form-control select2', 'label'=>'Category',
                                                    'style' => 'width:100%',
                                                    'data-level' => 0,
                                                    'data-role' => 'dropdown-category',
                                                ]) !!}

                                                @php
                                                    $level = 1;
                                                @endphp
                                                @php
                                                    if (!isset($oldDropdowns['data'])) {
                                                        $oldDropdowns['data'] = [];
                                                    }
                                                @endphp

                                                @foreach ($oldDropdowns['data'] as $oldDropdown)
                                                    @php
                                                        $oldDropdown = collect($oldDropdown['data']);
                                                        $oldDropdownPlucked = $oldDropdown->pluck('name', 'id');
                                                    @endphp
                                                    @if (count($oldDropdownPlucked))
                                                        {!! Form::bs3Select('category[]', $oldDropdownPlucked, $productMarketplaceCategories, [
                                                            'class'=>'form-control select2',
                                                            'label'=>'Subcategory ' . $level,
                                                            'style' => 'width:100%',
                                                            'data-level' => $level,
                                                            'data-role' => 'dropdown-category',
                                                            'id' => 'category-level-'.$level,
                                                        ]) !!}
                                                    @endif
                                                    @php
                                                        $level++;
                                                    @endphp
                                                @endforeach
                                            </div>

                                            <hr>

                                            {!! Form::bs3Number('qty') !!}
                                            {!! Form::bs3Number('weight', null, ['label'=>'Weight (gr)', 'step'=>'0.01']) !!}
                                            {!! Form::bs3File('image') !!}
                                            <div class="form-group ">
                                                <img class="img-thumbnail" src="{{ asset($product->image) }}" width="100" height="100">
                                            </div>

                                            <hr>

                                            {!! Form::bs3Select('type', $dropdown['types']) !!}
                                            {!! Form::bs3Select('active', $dropdown['yes_no']) !!}
                                            {!! Form::bs3Select('status', $dropdown['statuses']) !!}

                                            <hr>

                                            {!! Form::bs3Select('is_taxed', $dropdown['yes_no'], null, ['label' => 'PPN']) !!}
                                            {!! Form::bs3Select('is_local_product', $dropdown['yes_no'], null, ['label' => 'Produk Dalam Negeri']) !!}

                                            <div data-id="product-meta-wrapper">
                                                {!! Form::bs3Select('product_is_msme_product', $dropdown['yes_no'], $product->is_msme_product, ['label' => 'Barang UMKM']) !!}
                                                {!! Form::bs3Select('meta_data[product_is_msme_product]', $dropdown['kelasUsaha'], null, ['label' => 'Kategori Barang UMKM']) !!}
                                                {!! Form::bs3Select('meta_data[product_jaminan_kebaruan]', $dropdown['yes_no'], null, ['label' => 'Jaminan Kebaruan']) !!}
                                                {!! Form::bs3Text('meta_data[product_garansi]', null, ['label' => 'Garansi']) !!}
                                                {!! Form::bs3Select('meta_data[product_status_ketersediaan]', $dropdown['product_status_ketersediaan'], null, ['label' => 'Status Ketersediaan']) !!}
                                                {!! Form::bs3Number('meta_data[product_minimal_order]', null, ['label' => 'Minimal Order']) !!}
                                                {!! Form::bs3Text('meta_data[product_cara_pengiriman]', null, ['label' => 'Cara Pengiriman']) !!}
                                            </div>

                                            <div data-id="service-meta-wrapper">
                                                {!! Form::bs3Select('service_is_msme_product', $dropdown['yes_no'], $product->is_msme_product, ['label' => 'Jasa UMKM']) !!}
                                                {!! Form::bs3Select('meta_data[service_is_msme_product]', $dropdown['kelasUsaha'], null, ['label' => 'Kategori Jasa UMKM']) !!}
                                                {!! Form::bs3Select('meta_data[service_jasa_kemendikbud]', $dropdown['yes_no'], null, ['label' => 'Jasa Kemendikbud']) !!}
                                                {!! Form::bs3Text('meta_data[service_garansi]', null, ['label' => 'Garansi']) !!}
                                                {!! Form::bs3Text('meta_data[service_waktu_cara_pelaksanaan]', null, ['label' => 'Waktu dan Cara Pelaksanaan']) !!}
                                                {!! Form::bs3Text('meta_data[service_jaminan_pelaksanaan]', null, ['label' => 'Jaminan Pelaksanaan']) !!}
                                                {!! Form::bs3Text('meta_data[service_status_verifikasi_jasa]', null, ['label' => 'Status Verifikasi Jasa']) !!}
                                                {!! Form::bs3Text('meta_data[service_jasa_kemendikbud]', null, ['label' => 'Jasa Kemendikbud']) !!}
                                            </div>

                                            <hr>

                                            {!! Form::bs3Select('has_kemdikbud_zone_price', $dropdown['yes_no'], $hasKemdikbudZonePrice, ['label'=>'Zone Price']) !!}

                                            <a class="btn btn-default" href="{{ route('users.stores.products.index', [$store->id]) }}">Back</a>

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane pt-4" id="tab-attributes">
                                <div class="box">
                                    <div class="box-content">
                                        <a class="btn btn-primary" href="{{ route('users.stores.products.attributes.create', [$store->id, $product->id]) }}">Add Attribute</a>

                                        <table class="table table-bordered table-hover mt-4 mb-0">
                                            <tbody>
                                                @if (count($groupedAttributes))
                                                    <tr>
                                                        <th>Options</th>
                                                        <th>Key</th>
                                                        <th>Value</th>
                                                    </tr>
                                                    @foreach ($groupedAttributes as $group => $attributes)
                                                        <tr>
                                                            <td></td>
                                                            <td colspan="2"><strong>{{ $group }}</strong></td>
                                                        </tr>
                                                        @foreach ($attributes as $attribute)
                                                            <tr>
                                                                <td>
                                                                    <a class="btn btn-success btn-xs"
                                                                        href="{{ route('users.stores.products.attributes.edit', [$store->id, $product->id, $attribute->id]) }}" title="Edit">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    {!! Form::open([
                                                                            'route' => ['users.stores.products.attributes.destroy', $store->id, $product->id, $attribute->id],
                                                                            'method' => 'DELETE',
                                                                            'style' => 'display: inline-block;'
                                                                        ]) !!}
                                                                        {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                                    {!! Form::close() !!}
                                                                </td>
                                                                <td>{{ $attribute->key }}</td>
                                                                <td>{{ $attribute->value }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td>No Attributes</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane pt-4" id="tab-galleries">
                                <div class="box">
                                    <div class="box-content">
                                        <a class="btn btn-primary" href="{{ route('users.stores.products.galleries.create', [$store->id, $product->id]) }}">Add Galleries</a>

                                        <table class="table table-bordered table-hover mt-4 mb-0">
                                            <tbody>
                                                @if (count($galleries))
                                                    <tr>
                                                        <th>Options</th>
                                                        <th></th>
                                                        <th>Type</th>
                                                        <th>Group</th>
                                                        <th>Name</th>
                                                        <th>Active</th>
                                                        <th>Sort Order</th>
                                                    </tr>
                                                    @foreach ($galleries as $gallery)
                                                        <tr>
                                                            <td>
                                                                <a class="btn btn-success btn-xs"
                                                                    href="{{ route('users.stores.products.galleries.edit', [$store->id, $product->id, $gallery->id]) }}" title="Edit">
                                                                    <i class="fa fa-edit"></i>
                                                                </a>
                                                                {!! Form::open([
                                                                        'route' => ['users.stores.products.galleries.destroy', $store->id, $product->id, $gallery->id],
                                                                        'method' => 'DELETE',
                                                                        'style' => 'display: inline-block;'
                                                                    ]) !!}
                                                                    {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                                {!! Form::close() !!}
                                                            </td>
                                                            <td>
                                                                @if ($gallery->fileType == 'image')
                                                                    <img src="{{ asset($gallery->url) }}" style="height: 50px;">
                                                                @else
                                                                    <a class="btn btn-default btn-xs"
                                                                        href="{{ asset($gallery->url) }}" target="_blank">Download</a>
                                                                @endif
                                                            </td>
                                                            <td>{{ $gallery->fileType }}</td>
                                                            <td>{{ $gallery->group }}</td>
                                                            <td>{{ $gallery->name }}</td>
                                                            <td>{{ $gallery->active ? 'Yes' : 'No' }}</td>
                                                            <td>{{ $gallery->sort_order }}</td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td>No Galleries</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            @if ($product->type == 'grouped product')
                                <div class="tab-pane pt-4" id="tab-grouped-product">
                                    <div class="box">
                                        <div class="box-content">
                                            <a class="btn btn-primary"
                                                href="{{ route('users.stores.products.grouped-products.create', [$store->id, $product->id]) }}">
                                                Add Grouped Product
                                            </a>

                                            <a class="btn btn-default"
                                                href="{{ route('users.stores.products.grouped-products.import.create', [$store->id, $product->id]) }}">
                                                Import to Grouped Product
                                            </a>

                                            <table class="table table-bordered table-hover mt-4 mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>Options</th>
                                                        <th>Image</th>
                                                        <th>Product</th>
                                                        <th>Qty</th>
                                                        <th>Note</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (count($groupedProductDetails))
                                                        @foreach ($groupedProductDetails as $detail)
                                                            <tr>
                                                                <td>
                                                                    <a class="btn btn-success btn-xs"
                                                                        href="{{ route('users.stores.products.grouped-products.edit', [$store->id, $product->id, $detail->id]) }}" title="Edit">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    {!! Form::open([
                                                                            'route' => ['users.stores.products.grouped-products.destroy', $store->id, $product->id, $detail->id],
                                                                            'method' => 'DELETE',
                                                                            'style' => 'display: inline-block;'
                                                                        ]) !!}
                                                                        {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                                    {!! Form::close() !!}
                                                                </td>
                                                                <td>
                                                                    <img src="{{ asset($detail->product->image) }}" width="50" height="50">
                                                                </td>
                                                                <td>
                                                                    <a href="{{ route('users.stores.products.show', [$store->id, $detail->product->id]) }}">
                                                                        {{ $detail->product->name }}
                                                                    </a>
                                                                </td>
                                                                <td>{{ $detail->qty }}</td>
                                                                <td>{{ $detail->note }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="5">No data</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            @endif

                            @if ($hasKemdikbudZonePrice)
                                <div class="tab-pane pt-4" id="tab-zone-price">
                                    <div class="box">
                                        <div class="box-content">
                                            <a class="btn btn-primary"
                                                href="{{ route('users.stores.products.zone-prices.create', [$store->id, $product->id]) }}">
                                                Update Zone Price
                                            </a>

                                            <a class="btn btn-default"
                                                href="{{ route('users.stores.products.zone-prices.import.create', [$store->id, $product->id]) }}">
                                                Import to Zone Price
                                            </a>

                                            <table class="table table-bordered table-hover mt-4 mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>Zone</th>
                                                        <th>Price</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (count($mappedKemdikbudZonePrices))
                                                        @foreach ($mappedKemdikbudZonePrices as $zone => $zonePrice)
                                                            <tr>
                                                                <td>Zone {{ $zone }}</td>
                                                                <td>{{ $zonePrice }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="5">No data</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
