@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Edit Grouped Product Detail')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.products.index', [$store->id]) }}">
                                Store Products
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.products.show', [$store->id, $product->id]) }}">
                                Detail
                            </a>
                        </li>
                        <li>
                            Grouped Product
                        </li>
                        <li class="active">
                            Edit
                        </li>
                    </ol>

                    <h3>Edit Grouped Product Detail</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            <div class="media">
                                <a class="pull-left" href="{{ route('users.stores.products.show', [$store->id, $product->id]) }}">
                                    <img class="media-object" src="{{ asset($product->image) }}" width="50" height="50">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">{{ $product->name }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($groupedProduct, [
                                'route' => ['users.stores.products.grouped-products.update', $store->id, $product->id, $groupedProduct->id],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('product', $groupedProduct->product->name, ['readonly'=>'readonly']) !!}
                                {!! Form::bs3Text('qty') !!}
                                {!! Form::bs3Textarea('note') !!}

                                {!! Form::bs3Submit('Update'); !!}
                                <a class="btn btn-default" href="{{ route('users.stores.products.show', [$store->id, $product->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
