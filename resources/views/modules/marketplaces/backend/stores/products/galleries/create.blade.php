@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Create Product Gallery')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.products.index', [$store->id]) }}">
                                Store Products
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.products.show', [$store->id, $product->id]) }}">
                                Detail
                            </a>
                        </li>
                        <li>
                            Galleries
                        </li>
                        <li class="active">
                            Create
                        </li>
                    </ol>

                    <h3>Create Product Gallery</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            <div class="media">
                                <a class="pull-left" href="{{ route('users.stores.products.show', [$store->id, $product->id]) }}">
                                    <img class="media-object" src="{{ asset($product->image) }}" width="50" height="50">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">{{ $product->name }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['users.stores.products.galleries.store', $store->id, $product->id],
                                'method' => 'POST',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Select('group', $dropdown['groups']) !!}
                                {!! Form::bs3File('file') !!}
                                {!! Form::bs3Text('name', $product->name.' gallery') !!}
                                {!! Form::bs3Textarea('description') !!}
                                {!! Form::bs3Number('sort_order', 1) !!}
                                {!! Form::bs3Select('active', $dropdown['yes_no'], 1) !!}

                                {!! Form::bs3Submit('Save'); !!}
                                <a class="btn btn-default" href="{{ route('users.stores.products.show', [$store->id, $product->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
