@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Add Grouped Product Detail')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
@endpush

@push('js')
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.fn.select2.defaults.set( "theme", "bootstrap" );

            $('.select2').select2();
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.products.index', [$store->id]) }}">
                                Store Products
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.products.show', [$store->id, $product->id]) }}">
                                Detail
                            </a>
                        </li>
                        <li>
                            Zone Prices
                        </li>
                        <li class="active">
                            Update
                        </li>
                    </ol>

                    <h3>Update Zone Prices</h3>

                    @include('flash::message')

                    @if (!$errors->isEmpty())
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            There are some error. Please check your input.
                        </div>
                    @endif

                    <div class="box">
                        <div class="box-content">
                            <div class="media">
                                <a class="pull-left" href="{{ route('users.stores.products.show', [$store->id, $product->id]) }}">
                                    <img class="media-object" src="{{ asset($product->image) }}" width="50" height="50">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">{{ $product->name }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['users.stores.products.zone-prices.store', $store->id, $product->id],
                                'method' => 'POST',
                                'files' => true,
                                ]) !!}

                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Zone</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($zones as $zone)
                                            <tr>
                                                @php
                                                    $zoneName = $zone->zone;
                                                    $price = isset($mappedKemdikbudZonePrices[$zoneName]) ? $mappedKemdikbudZonePrices[$zoneName] : 0;
                                                @endphp
                                                <tr>
                                                    <td>Zone {{ $zoneName }}</td>
                                                    <td>
                                                        {!! Form::hidden('prices['.$zoneName.'][zone]', $zoneName, ['class'=>'form-control']) !!}
                                                        {!! Form::number('prices['.$zoneName.'][price]', $price, ['class'=>'form-control']) !!}
                                                    </td>
                                                </tr>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>


                                {!! Form::bs3Submit('Save'); !!}
                                <a class="btn btn-default" href="{{ route('users.stores.products.show', [$store->id, $product->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
