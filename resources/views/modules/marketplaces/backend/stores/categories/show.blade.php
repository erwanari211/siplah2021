@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Detail Collection')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/marika-natsuki/plugins') }}/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
@endpush

@push('js')
    <script src="{{ asset('assets/marika-natsuki/plugins') }}/select2/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.fn.select2.defaults.set( "theme", "bootstrap" );

            $('.select2').select2();
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.collections.index', [$store->id]) }}">
                                Store Collections
                            </a>
                        </li>
                        <li class="active">
                            Detail
                        </li>
                    </ol>

                    <h3>Detail Collection</h3>

                    <div class="box">
                        <div class="box-content">
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="{{ asset($collection->image) }}" width="50" height="50">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">{{ $collection->name }}</h4>
                                    <p>{{ $collection->description }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h4>Add Product to Collections</h4>
                    <div class="box">
                        <div class="box-content">
                            @include('flash::message')

                            {!! Form::model($collection, [
                                'route' => ['users.stores.collections.products.store', $store->id, $collection->id],
                                'method' => 'POST',
                                'files' => false,
                                ]) !!}

                                {!! Form::bs3Select('product', $dropdown['products'], null, [
                                    'class'=>'form-control select2'
                                ]) !!}
                                {!! Form::bs3Submit('Add to collection'); !!}
                                <a class="btn btn-default" href="{{ route('users.stores.collections.index', [$store->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                    <h4>Products</h4>
                    <div class="box">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Option</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($products))
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach ($products as $product)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        {!! Form::open([
                                                                'route' => ['users.stores.collections.products.destroy', $store->id, $collection->id, $product->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td>
                                                        <img src="{{ asset($product->image) }}" width="50" height="50">
                                                    </td>
                                                    <td>{{ $product->name }}</td>
                                                    <td>{{ formatNumber($product->price) }}</td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
