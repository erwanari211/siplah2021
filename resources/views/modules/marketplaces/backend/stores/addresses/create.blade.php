@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Create Address')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script>
        $(document).ready(function() {
            var provincesWithDetails = @json($provincesWithDetails);
            console.log(provincesWithDetails);

            $('#province_id').on('change', function(event) {
                event.preventDefault();
                $('#city_id').prop('disabled', 'disabled');
                var provinceId = $(this).val();

                var output = '';
                output += '<option selected="selected" value="">Please Select</option>';
                $('#city_id').html(output);
                $('#district_id').html(output);

                if (provinceId) {
                    var province = provincesWithDetails[provinceId];
                    var cities = province['cities_data'];
                    for (var i in cities) {
                        var city = cities[i];
                        output += `<option value="${city['id']}">${city['name']}</option>`;
                    }
                    $('#city_id').html(output);
                    $('#city_id').prop('disabled', false);
                }
            });

            $('#city_id').on('change', function(event) {
                event.preventDefault();
                $('#district_id').prop('disabled', 'disabled');
                var provinceId = $('#province_id').val();
                var cityId = $(this).val();

                var output = '';
                output += '<option selected="selected" value="">Please Select</option>';
                $('#district_id').html(output);

                if (cityId) {
                    var city = provincesWithDetails[provinceId]['cities_data'][cityId];
                    console.log(city);
                    var districts = city['districts_data'];
                    console.log(districts);
                    for (var i in districts) {
                        var district = districts[i];
                        output += `<option value="${district['id']}">${district['name']}</option>`;
                    }
                    $('#district_id').html(output);
                    $('#district_id').prop('disabled', false);
                }
            });

        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.stores.index', [$store->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="#">Store Settings</a>
                        </li>
                        <li>
                            <a href="{{ route('users.stores.settings.addresses.index', [$store->id]) }}">
                                Addresses
                            </a>
                        </li>
                        <li class="active">
                            Create
                        </li>
                    </ol>

                    <h3>Create Address</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['users.stores.settings.addresses.store', $store->id],
                                'method' => 'POST',
                                'files' => false,
                                ]) !!}


                                {!! Form::bs3Text('label', 'Store address') !!}
                                {!! Form::bs3Text('full_name', $store->name) !!}
                                {!! Form::bs3Text('company') !!}
                                {!! Form::bs3Textarea('address') !!}
                                {!! Form::bs3Text('phone') !!}
                                {!! Form::bs3Text('postcode') !!}
                                {!! Form::bs3Select('province_id', $dropdown['provinces'], null, ['label'=>'Province']) !!}
                                {!! Form::bs3Select('city_id', $dropdown['cities'], null, ['label'=>'City']) !!}
                                {!! Form::bs3Select('district_id', $dropdown['districts'], null, ['label'=>'District']) !!}

                                {!! Form::bs3Submit('Save'); !!}
                                <a class="btn btn-default" href="{{ route('users.stores.settings.addresses.index', [$store->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-store')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
