@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Order Detail')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.supervisor-groups.home', [$supervisorGroup->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.supervisor-groups.orders.index', [$supervisorGroup->id]) }}">Orders</a>
                        </li>
                        <li class="active">
                            Detail
                        </li>
                    </ol>

                    <h3>Order Detail</h3>

                    @include('flash::message')

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="mb-3">
                                <a class="btn btn-default btn-sm" href="{{ route('users.supervisor-groups.orders.index', [
                                    $supervisorGroup->id,
                                    'order_status_id' => request('order_status_id'),
                                    'store_id' => request('store_id'),
                                ]) }}">
                                    Back
                                </a>
                            </div>

                            <h4>Order Detail</h4>
                            <div class="box">
                                <div class="box-content">
                                    @php
                                        $shippingMethod = $orderMetas['shipping_method'];
                                        $paymentMethod = $orderMetas['payment_method'];
                                        $storeSeller = isset($orderMetas['order']['store_seller']) ? $orderMetas['order']['store_seller'] : null;
                                    @endphp
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover mb-0">
                                            <tbody>
                                                <tr>
                                                    <td><strong>Invoice No</strong></td>
                                                    <td>{{ $order->invoice_prefix.$order->invoice_no }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Date</strong></td>
                                                    <td>{{ $order->created_at }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Payment Method</strong></td>
                                                    <td>{{ $paymentMethod['label'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Shipping Method</strong></td>
                                                    <td>{{ $shippingMethod['name'].' '.$shippingMethod['service'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Order Note</strong></td>
                                                    <td>{{ $paymentMethod['note'] }}</td>
                                                </tr>

                                                <tr>
                                                    <td><strong>Status</strong></td>
                                                    <td>{{ $order->orderStatus->value ?? '' }}</td>
                                                </tr>

                                                @if ($storeSeller)
                                                    <tr>
                                                        <td>
                                                            <strong>Seller</strong>
                                                        </td>
                                                        <td>
                                                            <i class="fa fa-user-tie mr-2"></i>
                                                            {{ $storeSeller }}
                                                        </td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Products</h4>
                            <div class="box">
                                <div class="box-content">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered mb-0">
                                            <thead>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Product</th>
                                                    <th>Qty</th>
                                                    <th>Price</th>
                                                    <th>Subtotal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($products as $product)
                                                    <tr>
                                                        <td>
                                                            <img src="{{ asset($product->product->image) }}" width="50" height="50">
                                                        </td>
                                                        <td>{{ $product->product_name }}</td>
                                                        <td> {{ $product->qty }} </td>
                                                        <td>{{ formatNumber($product->price) }}</td>
                                                        <td>{{ formatNumber($product->total) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                @php
                                                    $courier = $orderMetas['shipping_method']['name'];
                                                    $service = $orderMetas['shipping_method']['service'];
                                                    $shippingCost = $orderMetas['order']['shipping'];
                                                    $subtotal = $orderMetas['order']['subtotal'];
                                                    $subtotal = floatvalue($subtotal);
                                                    $uniqueNumber = $orderMetas['order']['unique_number'];
                                                    $adjustmentPrice = $orderMetas['order']['adjustment_price'];
                                                    $total = $subtotal + $shippingCost + $uniqueNumber + $adjustmentPrice;

                                                    $paymentMethodInfo = $orderMetas['payment_method']['info'];
                                                @endphp

                                                <tr>
                                                    <td colspan="4">Subtotal</td>
                                                    <td>{{ formatNumber($subtotal) }}</td>
                                                </tr>


                                                <tr>
                                                    <td colspan="4">{{ $courier . ' ' . $service }}</td>
                                                    <td>{{ formatNumber($shippingCost) }}</td>
                                                </tr>

                                                <tr>
                                                    <td colspan="4">Unique Number</td>
                                                    <td>{{ formatNumber($uniqueNumber) }}</td>
                                                </tr>

                                                @if ($adjustmentPrice != 0)
                                                    <tr>
                                                        <td colspan="4">Adjustment Price</td>
                                                        <td>{{ formatNumber($adjustmentPrice) }}</td>
                                                    </tr>
                                                @endif

                                                <tr>
                                                    <td colspan="4">
                                                        <strong>Total</strong>
                                                    </td>
                                                    <td>
                                                        <strong>{{ formatNumber($total) }}</strong>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <h4>Payment Address</h4>
                            <div class="box">
                                <div class="box-content">
                                    @php
                                        $address = $orderMetas['payment_address'];
                                    @endphp
                                    <table class="table table-bordered table-hover mb-0">
                                        <tbody>
                                            <tr>
                                                <td><strong>Full Name</strong></td>
                                                <td>
                                                    {{ $address['full_name'] }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Company</strong></td>
                                                <td>
                                                    @if ($address['company'])
                                                        {{ $address['company'] }}
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Phone</strong></td>
                                                <td>{{ $address['phone'] }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Address</strong></td>
                                                <td>{{ $address['address'] }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>City</strong></td>
                                                <td>{{ $address['city'] }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Province</strong></td>
                                                <td>{{ $address['province'] }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Post Code</strong></td>
                                                <td>{{ $address['postcode'] }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h4>Shipping Address</h4>
                            <div class="box">
                                <div class="box-content">
                                    @php
                                        $address = $orderMetas['shipping_address'];
                                    @endphp
                                    <table class="table table-bordered table-hover mb-0">
                                        <tbody>
                                            <tr>
                                                <td><strong>Full Name</strong></td>
                                                <td>
                                                    {{ $address['full_name'] }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Company</strong></td>
                                                <td>
                                                    @if ($address['company'])
                                                        {{ $address['company'] }}
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Phone</strong></td>
                                                <td>{{ $address['phone'] }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Address</strong></td>
                                                <td>{{ $address['address'] }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>City</strong></td>
                                                <td>{{ $address['city'] }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Province</strong></td>
                                                <td>{{ $address['province'] }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Post Code</strong></td>
                                                <td>{{ $address['postcode'] }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    @include('modules.marketplaces.backend.supervisor-groups.orders.inc.order-histories')
                </div>
            </div>

        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-supervisor-group')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
