@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Categories')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.supervisor-groups.home', [$supervisorGroup->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.supervisor-groups.categories.index', [$supervisorGroup->id]) }}">Categories</a>
                        </li>
                        <li class="active">
                            List
                        </li>
                    </ol>

                    <h3>Categories</h3>

                    @include('flash::message')

                    @if ($rootCategories)
                        <div class="box">
                            <div class="box-content">
                                @php
                                    $chunkedCategories = $rootCategories->chunk(3);
                                @endphp

                                <div class="row">
                                    @foreach ($chunkedCategories as $chunkedCategoriesItem)
                                        @foreach ($chunkedCategoriesItem as $rootCategory)
                                            <div class="col-sm-4">
                                                <a href="{{ route('users.supervisor-groups.categories.index', [
                                                        $supervisorGroup->id,
                                                        'category_id' => $rootCategory->id,
                                                    ]) }}">
                                                    {{ $rootCategory->name }}
                                                </a>

                                                <span class="label label-default ml-2" title="Children Category">
                                                    {{ $rootCategory->children_count }}
                                                </span>

                                                <span class="label label-success ml-2" title="Products">
                                                    {{ $rootCategory->products_count }}
                                                </span>
                                            </div>
                                        @endforeach
                                    @endforeach
                                </div>

                            </div>
                        </div>
                    @endif

                    @if ($category)
                        <div class="box">
                            <div class="box-content">
                                <a href="{{ route('users.supervisor-groups.categories.index', [
                                        $supervisorGroup->id,
                                        'category_id' => $category->parent_id,
                                    ]) }}">
                                    {{ $category->name }}
                                </a>

                                <span class="label label-default ml-2" title="Children Category">
                                    {{ $category->children_count }}
                                </span>

                                <span class="label label-success ml-2" title="Products">
                                    {{ $category->products_count }}
                                </span>
                            </div>
                        </div>

                        @php
                            $childrenCategories = $category->children()->withCount('children', 'products')->orderBy('name')->get();
                        @endphp
                        @if (count($childrenCategories))
                            <div class="box">
                                <div class="box-content">
                                    @php
                                        $chunkedCategories = $childrenCategories->chunk(3);
                                    @endphp

                                    <div class="row">
                                        @foreach ($chunkedCategories as $chunkedCategoriesItem)
                                            @foreach ($chunkedCategoriesItem as $childrenCategory)
                                                <div class="col-sm-4">
                                                    <a href="{{ route('users.supervisor-groups.categories.index', [
                                                            $supervisorGroup->id,
                                                            'category_id' => $childrenCategory->id,
                                                        ]) }}">
                                                        {{ $childrenCategory->name }}
                                                    </a>

                                                    <span class="label label-default ml-2" title="Children Category">
                                                        {{ $childrenCategory->children_count }}
                                                    </span>

                                                    <span class="label label-success ml-2" title="Products">
                                                        {{ $childrenCategory->products_count }}
                                                    </span>
                                                </div>
                                            @endforeach
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                        @endif
                    @endif

                    @if ($products && count($products))
                        <div class="box">
                            <div class="box-content">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Options</th>
                                                <th>Image</th>
                                                <th>Store</th>
                                                <th>Name</th>
                                                <th>SKU</th>
                                                <th>Price</th>
                                                <th>Category</th>
                                                <th>Approved</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($products))
                                                @php
                                                    $no = $products->firstItem();
                                                @endphp
                                                @foreach ($products as $product)
                                                    <tr>
                                                        <td>{{ $no }}</td>
                                                        <td>
                                                            <a class="btn btn-xs btn-default"
                                                                href="{{ route('products.show', [$marketplace->slug, $product->store_id, $product->slug]) }}"
                                                                target="_blank">
                                                                <i class="fa fa-external-link-alt"></i>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <img src="{{ $product->image_url }}" width="50" height="50">
                                                        </td>
                                                        <td> {{ $product->store->name }} </td>
                                                        <td> {{ $product->name }} </td>
                                                        <td>{{ $product->sku }}</td>
                                                        <td>
                                                            {{ formatNumber($product->price) }}
                                                            @if ($product->getMeta('has_kemdikbud_zone_price'))
                                                                <br>
                                                                <i class="fa fa-globe"
                                                                    data-toggle="tooltip"
                                                                    title="Has zone prices"></i>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (count($product->marketplaceCategories))
                                                                @foreach ($product->marketplaceCategories as $productCategory)
                                                                    <span class="btn btn-primary btn-xs">{{ $productCategory->name }}</span>
                                                                @endforeach
                                                            @endif
                                                        </td>

                                                        <td>
                                                            {!! $product->approved_button !!}
                                                        </td>
                                                        <td>
                                                            {!! $product->approved_status_button !!}
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $no++;
                                                    @endphp
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="11">No data</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>

                                {{ $products->appends(request()->only(['category_id']))->links() }}
                            </div>
                        </div>

                    @endif


                </div>
            </div>

        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-supervisor-group')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
