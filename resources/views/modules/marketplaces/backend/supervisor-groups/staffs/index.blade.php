@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Staffs')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.supervisor-groups.home', [$supervisorGroup->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.supervisor-groups.staffs.index', [$supervisorGroup->id]) }}">Staffs</a>
                        </li>
                        <li class="active">
                            List
                        </li>
                    </ol>

                    <h3>Staffs</h3>

                    @include('flash::message')

                    <div class="mb-3">
                        <a class="btn btn-primary" href="{{ route('users.supervisor-groups.staffs.create', $supervisorGroup->id) }}">
                            Add Staff
                        </a>
                    </div>

                    <div class="box mt-3">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Name</th>
                                            <th>Role</th>
                                            <th>Is Active</th>
                                            <th>Is Online</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($staffs))
                                            @php
                                                $no = $staffs->firstItem();
                                            @endphp
                                            @foreach ($staffs as $staff)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-success"
                                                            href="{{ route('users.supervisor-groups.staffs.edit', [$supervisorGroup->id, $staff->id]) }}" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['users.supervisor-groups.staffs.destroy', $supervisorGroup->id, $staff->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td>
                                                        {{ $staff->user->name }}
                                                    </td>
                                                    <td>{{ $staff->role }}</td>
                                                    <td>{{ $staff->is_active ? 'Yes' : 'No' }}</td>

                                                    <td>
                                                        @php
                                                            $isOnline = $staff->checkIsOnline();
                                                        @endphp
                                                        @if ($isOnline)
                                                            <i class="fa fa-circle text-success"></i>
                                                            Online
                                                        @endif

                                                        @if (!$isOnline)
                                                            <i class="fa fa-circle"></i>
                                                            Offline
                                                        @endif
                                                    </td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="7">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $staffs->links() }}
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-supervisor-group')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
