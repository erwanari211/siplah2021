@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Edit Staff')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.marketplaces.frontend.inc.navbar-top-user')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('users.supervisor-groups.home', [$supervisorGroup->id]) }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('users.supervisor-groups.staffs.index', [$supervisorGroup->id]) }}">Staffs</a>
                        </li>
                        <li class="active">
                            Edit
                        </li>
                    </ol>

                    <h3>Edit Staff</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($staff, [
                                'route' => ['users.supervisor-groups.staffs.update', $supervisorGroup->id, $staff->id],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('staff', $staff->user->name, ['readonly']) !!}
                                {!! Form::bs3Select('role', $dropdown['roles']) !!}
                                {!! Form::bs3Select('is_active', $dropdown['yes_no']) !!}

                                {!! Form::bs3Submit('Save'); !!}
                                <a class="btn btn-default" href="{{ route('users.supervisor-groups.staffs.index', [$supervisorGroup->id]) }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>

    @include('modules.marketplaces.backend.inc.sidebar-user-supervisor-group')
    @include('modules.marketplaces.frontend.inc.sidebar-cart')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
