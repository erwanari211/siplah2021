@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Show Product Collection')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-product-id]').on('click', function(event) {
                event.preventDefault();
                var productId = $(this).attr('data-product-id');
                var sortOrder = $(this).attr('data-sort-order');
                $('#product_id').val(productId);
                $('#modal-add-to-product-collection').find('#sort_order').val(sortOrder);
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.administrators.backend.inc.navbar-top-administrator')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('administrator.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('administrator.product-collections.index') }}">Product Collections</a>
                        </li>
                        <li class="active">Detail</li>
                    </ol>

                    <h3>Show Product Collection</h3>

                    @include('flash::message')

                    <div class="box">
                        <div class="box-content">
                            {!! Form::model($productCollection, [
                                'route' => ['administrator.website.banner-groups.update', $productCollection->id],
                                'method' => 'PUT',
                                'files' => true,
                                ]) !!}

                                {!! Form::bs3Text('name') !!}
                                {!! Form::bs3Text('display_name') !!}
                                {!! Form::bs3Select('active', $dropdown['yes_no']) !!}
                                {!! Form::bs3Select('type', $dropdown['types']) !!}
                                {!! Form::bs3Number('sort_order') !!}

                                <a class="btn btn-default" href="{{ route('administrator.product-collections.index') }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                    <h3>Details</h3>

                    <div class="mb-4">
                        <a class="btn btn-primary" href="{{ route('administrator.product-collections.products.index', ['collection'=>$productCollection->id]) }}">
                            Add Product
                        </a>
                    </div>


                    <div class="box">
                        <div class="box-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered mb-0">
                                    <thead>
                                        <tr>
                                            <th>Options</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Store</th>
                                            <th>Marketplace</th>
                                            <th>Sort Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($details))
                                            @foreach ($details as $detail)
                                                @php
                                                    $product = $detail->product;
                                                @endphp
                                                <tr>
                                                    <td>
                                                        <a class="btn btn-xs btn-success"
                                                            data-toggle="modal"
                                                            data-product-id="{{ $product->id }}"
                                                            data-sort-order="{{ $detail->sort_order }}"
                                                            href="#modal-add-to-product-collection" title="Add to collection">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        {!! Form::open([
                                                                'route' => ['administrator.product-collection-details.destroy', $productCollection->id, $product->id],
                                                                'method' => 'DELETE',
                                                                'style' => 'display: inline-block;'
                                                            ]) !!}
                                                            {!! Form::bs3SubmitHtml('<i class="fa fa-trash"></i>', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'return confirm("Delete?")']); !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td>
                                                        <img class="lazy" src="{{ asset($product->image) }}" height="50">
                                                    </td>
                                                    <td>{{ $product->name }}</td>
                                                    <td>{{ $product->store->name }}</td>
                                                    <td>{{ $product->store->marketplace->name }}</td>
                                                    <td>{{ $detail->sort_order }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6">No Data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>


    <div class="modal fade" id="modal-add-to-product-collection">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add to group</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open([
                        'route' => ['administrator.product-collection-details.store'],
                        'method' => 'POST',
                        'files' => true,
                        ]) !!}


                        <div class="hide">
                            {!! Form::bs3Text('product_id') !!}
                            {!! Form::bs3Text('collection_id', $productCollection->id) !!}
                        </div>
                        {!! Form::bs3Number('sort_order') !!}

                        {!! Form::bs3Submit('Save'); !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    @include('modules.administrators.backend.inc.sidebar-administrator')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
