@php
    /**
     * website settings
     */
    $favicon = null;
    $favicon = isset($websiteSettings['favicon']) && $websiteSettings['favicon'] ? $websiteSettings['favicon'] : false;
@endphp

@extends('themes.marika-natsuki.main')

@section('title', 'Product List')

@push('meta')
    @if ($favicon)
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($favicon) }}">
    @endif
@endpush

@push('css')
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-product-id]').on('click', function(event) {
                event.preventDefault();
                var productId = $(this).attr('data-product-id');
                $('#product_id').val(productId);
            });
        });
    </script>
@endpush

@push('meta')
@endpush

@push('head_js')
@endpush

@section('content')

    @include('modules.administrators.backend.inc.navbar-top-administrator')

    <div class="page-content">

        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">

                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('administrator.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('administrator.product-collections.index') }}">Product Collections</a>
                        </li>
                        <li class="active">Products</li>
                    </ol>

                    <h3>Product List</h3>

                    @include('flash::message')
                    @php
                        $marketplaceId = request('marketplace');
                        $storeId = $marketplaceId ? request('store') : null;
                    @endphp

                    <div class="box">
                        <div class="box-content">
                            {!! Form::open([
                                'route' => ['administrator.product-collections.products.index'],
                                'method' => 'GET',
                                'files' => true,
                                ]) !!}

                                {!! Form::hidden('collection', request('collection')) !!}

                                {!! Form::bs3Select('marketplace', $dropdown['marketplaces'], $marketplaceId) !!}

                                @if ($dropdown['stores'])
                                    {!! Form::bs3Select('store', $dropdown['stores'], $storeId) !!}
                                @endif

                                {!! Form::bs3Text('name', request('name')) !!}
                                {!! Form::bs3Text('sku', request('sku')) !!}


                                {!! Form::bs3Submit('Filter'); !!}
                                <a class="btn btn-default" href="{{ route('administrator.product-collections.index') }}">Back</a>

                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="box">
                        <div class="box-content">
                            <h4>
                                Total Item : {{ $products->total() }}
                            </h4>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Options</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>SKU</th>
                                            <th>Store</th>
                                            <th>Marketplace</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($products))
                                            @php
                                                $no = $products->firstItem();
                                            @endphp
                                            @foreach ($products as $product)
                                                <tr>
                                                    <td>{{ $no }}</td>
                                                    <td>
                                                        <a class="btn btn-xs btn-default"
                                                            data-toggle="modal"
                                                            data-product-id="{{ $product->id }}"
                                                            href="#modal-add-to-product-collection" title="Add to collection">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <img src="{{ $product->image_url }}" width="50" height="50">
                                                    </td>
                                                    <td>
                                                        {{ $product->name }}
                                                    </td>
                                                    <td>
                                                        {{ $product->sku }}
                                                    </td>
                                                    <td>{{ $product->store->name }}</td>
                                                    <td>{{ $product->store->marketplace->name }}</td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8">No data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $products->appends(request()->only(['marketplace', 'store', 'name', 'sku', 'collection']))->links() }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-add-to-product-collection">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add to Collection</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open([
                        'route' => ['administrator.product-collection-details.store'],
                        'method' => 'POST',
                        'files' => true,
                        ]) !!}


                        <div class="hide">
                            {!! Form::bs3Text('product_id') !!}
                        </div>
                        {!! Form::bs3Select('collection_id', $dropdown['collections'], request('collection')) !!}
                        @if (!count($dropdown['collections']))
                            <span class="help-block">
                                <strong>Please create banner group</strong>
                            </span>
                        @endif

                        {!! Form::bs3Number('sort_order', 1) !!}

                        {!! Form::bs3Submit('Save'); !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    @include('modules.administrators.backend.inc.sidebar-administrator')

    <button class="btn-back-to-top btn btn-skin" title="Go to top">
        <i class="fa fa-angle-up"></i>
    </button>

@endsection
