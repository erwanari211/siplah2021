/**
 * Back to Top
 * Sidenav Overlay
 */

/**
 * Back to top
 */
function scrollFunction() {
    var scrollTop = $(document).scrollTop();
    if (scrollTop > 20) {
        $('.btn-back-to-top').css('display', 'block');
    } else {
        $('.btn-back-to-top').css('display', 'none');
    }
}

$(document).on('scroll', function(event) {
    event.preventDefault();
    scrollFunction();
});

scrollFunction();

$('.btn-back-to-top').on('click', function(event) {
    event.preventDefault();
    $("html, body").animate({ scrollTop: 0 }, 600);
});

/**
 * Sidenav Overlay
 */
$('[data-action="open-sidenav"]').on('click', function(event) {
    event.preventDefault();
    var target = $(this).attr('data-target');
    var isOpen = $(target).hasClass('open');
    if (isOpen) {
        $(target).removeClass('open')
        $(target).css('width', '0');
    } else {
        $(target).addClass('open');
        $(target).css('width', '230');
    }
});

$('[data-action="close-sidenav"]').on('click', function(event) {
    event.preventDefault();
    var target = $(this).attr('data-target');
    $(target).removeClass('open')
    $(target).css('width', '0');
});

$('.sidenav-overlay-close').on('click', function(event) {
    event.preventDefault();
    var parent = $(this).parents('.sidenav-overlay');
    $(parent).removeClass('open')
    parent.css('width', '0');
});

/**
 * Bar Rating
 * Lazy load
 * Slimscroll
 * Toastr
 */

/**
 * Bar Rating
 */
$('.rating').each(function(index, el) {
  var $El = $(el);
  var initialRating = $El.attr('data-initial-rating');
  var ratingReadonly = $El.attr('data-rating-readonly');
  var isReadOnly = ratingReadonly == 'false' ? false : true;
  $El.barrating({
    theme: 'fontawesome-stars',
    initialRating: initialRating,
    readonly: isReadOnly
  });
});

$('.product-rating').each(function(index, el) {
  var $El = $(el);
  $El.barrating({
    theme: 'fontawesome-stars',
    initialRating: $El.attr('data-initial-rating'),
  });
});

/**
 * Lazy load
 */
var myLazyLoad = new LazyLoad({
  elements_selector: ".lazy"
});

/**
 * Slimscroll
 */
$('.slimscroll').slimscroll({
  height: 'auto',
  railVisible: true,
  railBorderRadius: 0,
});

/**
 * Toastr
 */
function setDefaultToastrOptions() {
  defaultToastrOptions = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "rtl": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": 300,
    "hideDuration": 1000,
    "timeOut": 5000,
    "extendedTimeOut": 1000,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
  toastr.options = defaultToastrOptions;
}

